<?php
global $system,$userinfo,$db,$settings;

$var = $system->getVar();

$system->importClass('payment');

if(!isset($var['sid'])) $system->invalidRequest();



   if($var['submit']){
   
     if($var['sid'] == '') $errors[] =  "Please select a valid payment option";
     if(is_array($errors) && !empty($errors)) while (list($key,$value) = each($errors)) $return_msg.= $value.'<br>';	
     else
     {
		    $sql = "SELECT * FROM ".PREFIX."_payment_subscriptions WHERE id = '".$var['sid']."' ";
			$res = $db->query_db($sql,$print = DEBUG);
			if ($db->num_rows($res) > 0)
			{ 
			   $row = $db->fetch_db_array($res);
			   switch($var['submit'])
               {
                 case('Activate'):Payment::renewSubscription($row['username'],$row['method'],$row['key']);break;
	             case('Suspend'):Payment::suspendSubscription($row['username'],$row['method'],$row['key']);break;
	             case('Cancel'):Payment::cancelSubscription($row['username'],$row['method'],$row['key']);break;
				 default:break;
				 
			   }		
           }   
     } 
  }
  $per_page = $settings['system']['rows_per_page'];
  
  $sql  = "SELECT * FROM ".PREFIX."_payment_subscriptions ";
  $sql .= "WHERE id = '".$var['sid']."'  ";
  $sql .= "ORDER BY id DESC LIMIT 1";		

  $result = $db->query_db($sql,$print = DEBUG);
  if ($db->num_rows($result) > 0) $subscription = $db->fetch_db_array($result);
  $sql  = "SELECT name FROM ".PREFIX."_payment_processors WHERE id = '". $subscription['method']."'  LIMIT 1";
  $result2 = $db->query_db($sql,$print = DEBUG);
  $prow = $db->fetch_db_array($result2);
  $subscription["method"] = $prow["name"];
  $prefix = "Every ";
  $duration = ($subscription['bill_duration'] == 1) ?" ":$subscription['bill_duration'];
  $period = ($subscription['bill_duration'] == 1) ? rtrim($subscription['bill_period'],"s"):$subscription['bill_period'];
  $subscription['details'] = $prefix.$duration.$period;
  
  
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'subscription_details.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();  
?>

