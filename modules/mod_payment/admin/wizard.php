<?php

   global $system,$db,$settings;

   $system->importClass('account')->checkPrivilege(3);

   $system->importClass('payment');
   
   $var = $system->getVar();
   
   if($var['install'] == 'y' && isset($var['processor']))
   {
	 $installed = Payment::installProcessor($var['processor']);
	 if($installed[0] == true)  $return_msg = "Processor Installed";
	 else  $return_msg = $installed[1];
   }
	
   $to_install = array();
   
   $folders = scanDirectory( ROOT.DS.'modules'.DS.'mod_payment'.DS.'processors' );
   
   foreach($folders as $value) if(!$db->get_row("classdir",$value,"_payment_processors")) $to_install[] = $value;
  
   $data = get_defined_vars();
	
   $loader = new Loader;
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'wizard.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();

?>

