<h1>Payment Gateways</h1>

<p class="info">Here you can edit payment gateways</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

<h2><span><a href="?mod=payment&go=wizard" class="button-alt-sml" >Add Processor</a></span><br /></h2>

<form  method="post" action="">
<table  cellpadding="0" cellspacing="0" class="tableS">
  <thead>
  <tr>
    <th class="left">Processor</th>
    <th class="right">Action</th>
  </tr>
  </thead>

 <?php foreach ($processors as $num => $row){ ?>
 
  <tr>
     <td  align="left"><?php echo $row['name'] ?></td>
     <td  align="right"><a href="?mod=payment&go=edit_processor&pid=<?php echo $row['id'] ?>"><img src="images/edit.png" class="tooltip img-wrap2"  alt="" title="Edit"/></a></td>
  </tr>

   <?php } ?>
 
   <?php if(count($processors) == 0) { ?><tr> <td colspan="2" align="center" >No processor found</td> </tr><?php } ?>
   
 
  </table>
</form>
<script language="javascript">
<!-- Begin
var checkflag = "false";
function check(field) {
var checks = document.getElementsByName('list[]');
if (checkflag == "false") {
for (i = 0; i < checks.length; i++) {
checks[i].checked = true;}
checkflag = "true";
return "Uncheck All"; }
else {
for (i = 0; i < checks.length; i++) {
checks[i].checked = false; }
checkflag = "false";
return "Check All"; }
}
//  End -->
</script>

