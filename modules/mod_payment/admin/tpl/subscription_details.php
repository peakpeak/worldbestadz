<h1>Subscription Details</h1>

<p class="info">Here you can view subscription details</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>
	
	
	<form  action="" method="post">
    <input type="hidden" name="sid" value="<?php echo $var['sid']?>" />
	<table  cellpadding="0" cellspacing="0"  class="tableS">
	<thead>
	<tr>
		<th colspan="2" class="left"><a href="?mod=payment&go=subscriptions">Subscriptions</a> > Subscription Details</th>	
	</tr>
	</thead>
	 <?php if(count($subscription) == 0) : ?>
	 
      <tr>
		<td colspan="2"> - no records found - </td>
	  </tr>
	  <?php else: ?> 
      <tr>
		<td width="30%">Username</td>
        <td><?php echo $subscription["username"]?></td>
    </tr>
    <tr>
		<td><label>Date</label></td>
		<td><?php echo $subscription['date'] ?></td>
	</tr>
	<tr>
		<td><label>Description</label></td>
		<td><?php echo $subscription['description'] ?></td>
	</tr>
	<tr>
		<td><label>Amount</label></td>
		<td><?php  echo $settings['payment']['currencies'][$subscription['currency']]['symbol']." ".number_format($subscription['amount'],2)?> <?php echo $subscription['details'];?></td>
	</tr>
	<!--<tr>
		<td><label>Fee:</label></td>
		<td><?php  echo $settings['payment']['currencies'][$subscription['currency']]['symbol']." ".number_format($subscription['fee'],2)?></td>
	</tr>-->
	<tr>
		<td><label>Payment Method</label> </td>
		<td><?php echo $subscription['method'] ?></td>
	</tr>
	<tr>
		<td><label>Account ID</label> </td>
		<td><?php echo $subscription['accountid'] ?></td>
	</tr>
	<tr>
		<td><label>Creation Date</label> </td>
		<td><?php echo $subscription['date'] ?></td>
	</tr>
	<tr>
		<td><label>Last Billing</label> </td>
		<td><?php echo $subscription['lastbill_date'] ?></td>
	</tr>
	
	<?php if( $subscription['status'] != "Cancelled") { ?>
	<tr>
		<td><label>Next Billing</label> </td>
		<td><?php echo $subscription['nextbill_date'] ?></td>
	</tr>
	<?php } ?>
	<tr>
		<td><label>Status</label> </td>
		<td><?php echo $subscription['status'] ?></td>
	</tr>
	
	<!--<tr>
		<td>Status</td>
        <td>
                           <select name="status">
						       <option value="Active" <?php if($subscription["status"] == 'Active')  echo "selected"?>>Active</option>
                               <option value="Pending" <?php if($subscription["status"] == 'Pending')  echo "selected"?>>Pending</option>
							   <option value="Cancelled" <?php if($subscription["status"] == 'Cancelled')  echo "selected"?>>Cancelled</option>
						 </select>
		</td>
  </tr>-->
  <tr>
            <td colspan="2" align="left">
			<?php if($subscription["status"] == 'Active'){ ?>
			
			 <input type="submit" name="submit" value="Suspend" class="button_link"> 
			 
			 <input type="submit" name="submit" value="Cancel" class="button_link"> 
			 
			 
			 <?php } elseif($subscription["status"] == 'Pending') { ?>
			 
			  <input type="submit" name="submit" value="Activate" class="button_link"> 
			 
			  <input type="submit" name="submit" value="Cancel" class="button_link"> 
			 	
			  <?php }  ?>
			 
			 </td>
  </tr>
     <?php endif; ?> 
</table>
   
</form>

	