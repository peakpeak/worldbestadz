<h1>Deposit Details</h1>

<p class="info">Here you can view deposits made by members</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>
	
	
	<form  action="" method="post">
    <input type="hidden" name="did" value="<?php echo $var['did']?>" />
	<table  cellpadding="0" cellspacing="0"  class="tableS">
	<thead>
	<tr>
		<th colspan="2" class="left"><a href="?mod=payment&go=deposits">Deposits</a> > Deposit Details</th>	
	</tr>
	</thead>
	 <?php if(count($deposit) == 0) : ?>
	 
      <tr>
		<td colspan="2"> - no records found - </td>
	  </tr>
	  <?php else: ?> 
      <tr>
						
						<td width="30%">Username</td>
                        <td nowrap="nowrap"><?php echo $deposit["username"]?></td>
		</tr>
       <tr>
						
						<td>Amount</td>
                        <td nowrap="nowrap"><?php echo $settings['payment']['currencies'][ $deposit['currency']]['symbol']; ?> <?php echo  $deposit['amount'] ?></td>
		</tr>
		 <tr>
						
						<td>Fee</td>
                        <td nowrap="nowrap"><?php echo $settings['payment']['currencies'][ $deposit['currency']]['symbol']; ?> <?php echo  $deposit['fee'] ?></td>
		</tr>
		 <tr>
						
						<td>Reference</td>
                        <td nowrap="nowrap"><?php echo $deposit["reference"]?></td>
		</tr>
        <tr>
						<td>Description</td>
                        <td><?php echo $deposit["description"]?></td>
	   </tr>
	   <tr>
						<td>Payment reference</td>
                        <td><?php echo $deposit["details"]?></td>
	   </tr>
	   <tr>
						<td>Payer's comment</td>
                        <td><?php echo $deposit["payer_comments"]?></td>
	   </tr>
	    <tr>
		
						<td>Status</td>
                        <td>
                           <select name="status">
                               <option value="Pending" <?php if($deposit["status"] == 'Pending')  echo "selected"?>>Pending</option>
                               <option value="Open" <?php if($deposit["status"] == 'Open')  echo "selected"?>>Open</option>
                               <option value="Approved" <?php if($deposit["status"] == 'Approved')  echo "selected"?>>Approved</option>                               
							   <option value="Declined" <?php if($deposit["status"] == 'Declined')  echo "selected"?>>Declined</option>                               
							   <option value="Canceled" <?php if($deposit["status"] == 'Canceled')  echo "selected"?>>Canceled</option>
							   <option value="Failed" <?php if($deposit['status'] == 'Failed')  echo "selected"?>>Failed</option>   						                         </select>
					</td>
	    </tr>
	    <tr>
						<td>Comment</td>
                        <td><textarea rows="2" cols="15" name="comments"><?php echo $deposit["payee_comments"]?></textarea></td>
	    </tr>  
        <tr>
            <td colspan="2" align="left"><input type="submit" name="submit" <?php echo ($deposit["status"] == 'Completed')? "disabled ='disabled'":"" ?>value="Update" class="button_link">	</td>
        </tr>
     <?php endif; ?> 
	</table>
   
</form>

	