<h1>Install a Payment Gateway</h1>

<p class="info">Here you can install a gateway</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

  <table  cellpadding="0" cellspacing="0" class="tableS">
     <thead>
      <tr> 
        <th colspan="2"  class="left"><a href="?mod=payment&go=processors">Payment Gateways</a> > Install Payment Gateway</th>
      </tr>
	  <tr>
	    <td colspan="2">
						<p class="info"><b>Installing a Payment Processor</b></p>				
						<h4>To install a payment processor, just follow these steps:</h4>
						<ol>
						  <li>Upload payment processor to /modules/mod_payment/processors </li>
						  <li>Refresh this page once processor is uploaded to the directory above</li>
						  <li>The processor should appear in the table below if it was uploaded correctly</li>
						  <li>Click on &quot;Install&quot; link on the right to install</li>
					   </ol>
			</td>            
	   </tr>
	 </thead>
	 <?php foreach ($to_install as $value){ ?>
     <tr>
      <td  align="left"><?php echo $value ?></td>
      <td  align="right"><a href="?mod=payment&go=wizard&install=y&processor=<?php echo $value ?>" onclick="return(confirm('Do you really want to install?'))">Install</a></td>
     </tr>
     <?php } ?>
     <?php if(count($to_install) == 0) { ?><tr> <td colspan="2" align="center" >No processor to install</td> </tr><?php } ?>
 </table> 

 
 
 