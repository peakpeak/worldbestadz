<h1>Subscriptions</h1>

<p class="info">Here you can view payment subscriptions to your website</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

<div class="box" style="margin-bottom:10px">
<form action="index.php" method="get" >
<input  type="hidden" name="mod" value="payment" />
<input  type="hidden" name="go" value="subscriptions" />
  <table cellpadding="0" cellspacing="0" class="utility">
	<tr style="background-color:transparent">
     <td  class="border">
	   <script>$(function() { jQueryui( "#datepicker_1" ).datepicker({ dateFormat: 'yy-mm-dd' });});</script>
	   <script>$(function() { jQueryui( "#datepicker_2" ).datepicker({ dateFormat: 'yy-mm-dd' });});</script>
	   
	    Date: 
	    <input type="text" name="time_from" value="<?php echo $var["time_from"]?>" size="10"  id="datepicker_1" />&nbsp;-&nbsp;
	    <input type="text" name="time_to" value="<?php echo $var["time_to"]?>" size="10" id="datepicker_2"/>	  
		Amount: 
	    <input type="text" name="total_from" value="<?php echo $var["total_from"]?>" size="10" />&nbsp;-&nbsp;
	    <input type="text" name="total_to" value="<?php echo $var["total_to"]?>" size="10"  /></td>
     <td align="left" class="border"> 
	 <table cellpadding="0" cellspacing="0" border="0"> 
	 <tr>
	 <td>Status:</td>
     <td><input type="checkbox" class="html-checkboxes" name="status[]" value="Pending"<?php if(in_array('Pending',$var["status"])){ ?> checked="checked" <?php } ?>/>Pending</td>
	 <td><input type="checkbox" class="html-checkboxes" name="status[]" value="Active"<?php if(in_array('Active',$var["status"])){ ?> checked="checked" <?php } ?>/>Active</td>
	 <td><input type="checkbox" class="html-checkboxes" name="status[]" value="Canceled"<?php if(in_array('Canceled',$var["status"])){ ?> checked="checked" <?php } ?>/>Canceled</td>   

</tr></table></td>
    </tr>
	
	
    <tr style="background-color:transparent">
	
     <td  class="border"> 
	  Username: <input type="text" name="username" value="<?php echo $var["username"]?>"  />
	  Processor:
	    <select name="method">
	    <option value="" >All</option>
	    <?php foreach ($processors as $id => $value) {?>
		<option value="<?php echo $id?>" <?php if( $var["method"] == $id){ ?> selected="selected" <?php } ?> ><?php echo $value; ?></option>	 
	    <?php } ?>
	    </select>
	 </td>
     <td align="left" class="border">
	 
	 Items Per Page: 
	 <select name="per_page">
	 <option value="10" <?php if( $var["per_page"] == '10'){ ?> selected="selected" <?php } ?>>10</option>
     <option value="25" <?php if( $var["per_page"] == '25'){ ?> selected="selected" <?php } ?>>25</option>
     <option value="50" <?php if( $var["per_page"] == '50'){ ?> selected="selected" <?php } ?>>50</option>
     <option value="100"<?php if( $var["per_page"] == '100'){ ?> selected="selected" <?php } ?>>100</option>
     </select>
     &nbsp;&nbsp;
    <!-- Go To: 
	 <select class="select" >
	 <option value="1" selected="selected">1</option>
     </select>-->
	 <input  class="button-alt-sml" type="button" name="clear"  value="Clear Filters" onclick="window.location.href='?mod=payment&go=subscriptions'"  />
	 <input  class="button-alt-sml" type="submit" name="submit"  value="Search"  /></td>
    </tr>
  </table>
</form>
</div>

<form  method="post" action="">
<table  cellpadding="0" cellspacing="0"  class="tableS">
 <thead>
  <tr>
    <th class="left">Date</th>
	<th class="left">Username</th>
    <th class="left">Amount</th>
	<th class="left">Method</th>
	<th class="left">Acc ID</th>
	<th class="left">Description</th>
	<th>Status</th>
    <th>Select</th>
  </tr>
  </thead>
  
  <?php foreach ($subscriptions as $num => $row){ ?>
  
  <tr>
        <td align="left"><?php echo $row['date']; ?></td>
		<td align="left"><?php echo $row['username']; ?></td>
		<td align="left"><?php echo $settings['payment']['currencies'][$row['currency']]['symbol']; ?> <?php echo $row['amount']; ?></td>
		<td align="left"><?php echo $row['method']; ?></td>
		<td align="left"><?php echo $row['accountid']; ?></td>
		<td align="left"><?php echo $row['description']; ?></td>
		<td align="center"><?php echo $row['status']; ?></td>
        <td align="center" nowrap="nowrap"><a href="?mod=payment&go=subscription_details&sid=<?php echo $row["id"]?>">Details</a></td>
     
  </tr>
 
   <?php } ?>
 
   <?php if(count($subscriptions) == 0) { ?> <tr><td colspan="8" align="center" >No subscription found</td></tr> <?php } ?>
   
     <tr>
	    <td colspan="8" align="center"> <?php echo $system->getPaginationString($page,$num_rows,$per_page,3,$system->curPageURL(),"page");?></td>
	 </tr>

  </table>
</form>
<script language="javascript">
<!-- Begin

    var checkflag = "false";

    function check(field) {
    var checks = document.getElementsByName('list[]');
    if (checkflag == "false") {
     for (i = 0; i < checks.length; i++){
      checks[i].checked = true;
     }
      checkflag = "true";
      return "Uncheck All";
   }
   else {
     for (i = 0; i < checks.length; i++) {
      checks[i].checked = false; }
      checkflag = "false";
      return "Check All"; 
  }
}
//  End -->
</script>

