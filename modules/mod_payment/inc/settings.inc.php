<?php
global $db,$settings;
$settings['payment']['default_memo'] = 'Miscilenous';
$settings['payment']['default_payment_button'] = SITEURL.'modules/mod_payment/processors/buttons/pay.png';
$settings['payment']['currencies'] =  array(
                                      'USD'=>array('name'=>'United States Dollar','symbol'=>'$','rate'=>1.00),
			                          'GBP'=>array('name'=>'Great Britain Pounds','symbol'=>'&#163;','rate'=>0.62),
									// 'BTC'=>array('name'=>'Bitcoin','symbol'=>' &#163;','rate'=>0.62),
                                    // 'NGN'=>array('name'=>'Nigerian Naira','symbol'=>'&#8358','rate'=>158.00),
								   
								 
			                        );

// dynamic settings
$sql  = "SELECT * FROM ".PREFIX."_payment_processors ";
$payment_result = $db->query_db($sql,$print = DEBUG);
if($db->num_rows($payment_result) > 0 ) 
while($payment_row = $db->fetch_db_array($payment_result)) 
$settings["payment"]["processors"][$payment_row['id']] =  $payment_row["name"]; 

foreach ($settings["payment"]['currencies'] as $key => $data) 
$settings["payment"]["currency_options"][$key] =  $data["name"];


?>