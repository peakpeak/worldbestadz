<?php
/*
	Please follow the instructions below to enable your IPN Status and to set up your Alert URL:

    Login to your AlertPay account.
    Click on "Business Tools".
    Under "Instant Payment Notification � IPN", click on "IPN Set up".
    Enter your Transaction PIN and click on "Access".
    Click on the "Edit" icon for the respective business profile. **This is for Business accounts only. Ignore this step if you only have one business profile on your account**
    Enter the information:
        For IPN Status, select "Enabled".
        For Alert URL, enter the URL of the location of your IPN Handler.
        Generate a security code if necessary and record it.
    Click on "Update".
	
	
	
	API Password

To ensure that a request made to the API comes from you, you must submit a password with your request.

Follow these steps to get your API password:

    Login to your AlertPay.
    Click on "Business Tools".
    Under the "API" section, click on "Setup API".
    Enter your "Transaction PIN" and click the "Access" button.
    Under "Activate API", select "Enabled" for API Status.
    Click on the "Generate New" button to get your password.
    Click "Update".

Please record your API password since you must use it with every API request.

	*/
class Payza extends Payment{
 
	
	var $payee;
	var $securityKey;
	var $apiPassword;
	var $sendMoneyData;
	
	var $makePaymentUrl = "https://secure.payza.com/checkout";//"https://sandbox.Payza.com/sandbox/payprocess.aspx";                 
	var $sendMoneyUrl = "https://api.payza.com/svc/api.svc/sendmoney";
	var $cancelSubUrl = "https://api.payza.com/svc/api.svc/CancelSubscription";
	
	var $currency = 'USD';
	
	var $default_button_buy = '';
	var $default_button_subscribe = '';
	
	var $postdata = array(
	
	
	);

    var $testmode = false;
    var $testdata = array(

                   'eid'=>'W544y',	           
		           'payer'=>'payer@test.com',	          
		           'payee'=>'reciever@test.com',	        
		           'amount'=>'100',	      
		           'currency'=>'USD',	           
		           'description'=>'Account Funding',	          
		           'reference'=>'REF-1234-1234-1234',
				          
		          // 'sub_id'=>'AZDF34355GHDSTYEQQASH',	      
		          // 'sub_status'=>'Active',
				
                  
               );
		         
	
	
	
	/*
	ap_merchant
	ap_purchasetype
	ap_itemname
	ap_amount
	ap_currency
	ap_description
	ap_itemcode
	ap_quantity
	ap_additionalcharges
	ap_shippingcharges
	ap_taxamount
	ap_discountamount
	ap_returnurl
    ap_cancelurl
	apc_1
    apc_2
    apc_3
    apc_4
    apc_5
    apc_6
	ap_alerturl
	ap_ipnversion
    */
	
	
	/*
	ap_timeunit
	ap_periodlength
	ap_periodcount
	ap_setupamount
	ap_trialamount
	ap_trialtimeunit
	ap_trialperiodlength
	*/
	
	/*ap_fname
	ap_lname
	ap_contactemail
	ap_contactphone
	ap_addressline1
	ap_addressline2
	ap_city
	ap_stateprovince
	ap_zippostalcode
	ap_country 	*/
	
	
	//Returned
	
	/*ap_merchant
	ap_securitycode
	ap_status
	ap_purchasetype
	ap_referencenumber
	ap_totalamount
	ap_currency
	ap_test
	ap_description
	ap_itemname
	ap_itemcode
	ap_quantity
	ap_amount
	ap_additionalcharges
	ap_shippingcharges
	ap_taxamount
	ap_discountamount
	ap_netamount
	ap_feeamount
	ap_transactiontype
	ap_transactiondate
	apc_1
    apc_2
    apc_3
    apc_4
    apc_5
    apc_6*/
	
	
	//subscription
	/*ap_subscriptionreferencenumber
	ap_setupamount
	ap_timeunit
	ap_periodlength
	ap_periodcount
	ap_nextrundate
	ap_trialtimeunit
	ap_trialperiodlength
	ap_subscriptionpaymentnumber*/
	

       function button( $data, $type ){ //required

               ///////////////////////////////
               /* $defined = array (
                //USER DEFINED FOR SIMPLE 
                'amount' => , //required
                'discription' => ,
                'reference' => ,
                'success' => ,
                'fail' => ,
				'auto_submit'=>,				
				
				//optional
				'button' => , //  link to image for submit button
				'custom' => array( custom 1, custom 2.... custom n), //  custom data
				
				 //USER DEFINED FOR SUBSCRIBE
				'sub_time' =>'Year', //Day,Week,Month,Year
                'sub_duration' => 1,
                'sub_period' =>5,

  
                //SYSTEM DEFINED
                'payee' => $this->payee, //required
                'currency' => $this->currency, //required
				
 
  
            );*/
             
 
          if(!$this->__checkRequired($data,$type)) return "Parameters are not configured";
		  return $this->makeForm($data,$type);
       
     }


     function makeForm($data,$type){
 
            $data['button'] = ($data['button']) ? $data['button'] : $this->default_button_buy;
			
			switch($type){
			case'simple':
			case'advanced':	
		    $form ="
			       
				   <form  action='".$this->makePaymentUrl."' method='post' name='pro'>
                   <input type='hidden' name='ap_purchasetype' value='item'>
                   <input type='hidden' name='ap_merchant' value='".$this->payee."'>
                   <input type='hidden' name='ap_itemname' value='".$data['discription']."'>
                   <input type='hidden' name='ap_description' value='".$data['discription']."'>
                   <input type='hidden' name='ap_amount' value='".$data['amount']."'>
				   <input type='hidden' name='ap_currency' value='".$this->currency."'>
                   <input type='hidden' name='ap_cancelurl' value='".$data['fail']."'>
				   <input type='hidden' name='ap_returnurl' value='".$data['success']."'>
                  ";
				   
			break;
			case'subscribe':	
			
				
		    $form.="
			
			       <form  action='".$this->makePaymentUrl."' method='post' name='pro'>
                   <input type='hidden' name='ap_purchasetype' value='subscription'>
                   <input type='hidden' name='ap_merchant' value='".$this->payee."'>
                   <input type='hidden' name='ap_itemname' value='".$data['discription']."'>
                   <input type='hidden' name='ap_description' value='".$data['discription']."'>
                   <input type='hidden' name='ap_amount' value='".$data['amount']."'>
				   <input type='hidden' name='ap_currency' value='".$this->currency."'>
                   <input type='hidden' name='ap_cancelurl' value='".$data['fail']."'>
				   <input type='hidden' name='ap_returnurl' value='".$data['success']."'>
			       <input type='hidden' name='ap_timeunit' value='".rtrim($data['sub_time'],"s")."'>
			       <input type='hidden' name='ap_periodlength' value='".$data['sub_duration']."'>
			       <input type='hidden' name='ap_periodcount' value='".$data['sub_period']."'>
			      ";
				   
			break;
			}
			
			$num = 0;	
			$form.="<input  type=\"hidden\" name=\"apc_1\" value=\"".$data['eid']."\" >";	
		    if(!$data['auto_submit'])  $form.="<input type=\"submit\" name=\"submit\" value=\"Submit\"  /></form>";
            else  $form.="</form><script>document.forms.pro.submit();</script>";
            return $form;
		
	 }
	 
	  function __checkRequired($data, $type) {
	  
	  if(!isset($data['id'])) return false;
	  if(!self::setPayee($data['id'])) return false;
	  if($type == "advanced" || $type == "subscribe") if(!self::setSecurityKey($data['id']))return false;
	  if(!isset($data['amount']) && !isset($this->currency)) return false;
	   //  if($type == 'subscribe') {}
	   return true;
	    
	 }
	 
	 
	 function setPayee($id)
	 {
	  
	   global $db;
	   $sql = "SELECT  
	   AES_DECRYPT(payee,'".ENCKEY."') as payee 
	   FROM ".PREFIX."_payment_processors WHERE id = '".$db->sql_quote($id)."' 
	   LIMIT 1 ";
	   $res = $db->query_db($sql,$print = DEBUG);
	   if ($db->num_rows($res) == 0 ) return false;
	   $processor = $db->fetch_db_array($res);
	   if($processor["payee"] == "")return false;
	   $this->payee = $processor["payee"];
	   return true;
	   
     }
	 
	 function setSecurityKey($id)
	 {
	   global $db;
	   $sql = "SELECT *,
	           AES_DECRYPT(payee,'".ENCKEY."') as payee,
			   AES_DECRYPT(var_1,'".ENCKEY."') as var_1
	           FROM ".PREFIX."_payment_processors 
	           WHERE id = '".$db->sql_quote($id)."' LIMIT 1 ";
	   $res = $db->query_db($sql,$print = DEBUG);
	   if ($db->num_rows($res) == 0 ) return false;
	   $processor = $db->fetch_db_array($res);
	   if($processor["var_1"] == "")return false;
	   $this->securityKey = $processor["var_1"];
	   return true;
     }
	 
	 function setAPIKey($id)
	 {
	   global $db;
	   $sql = "SELECT *,
	           AES_DECRYPT(payee,'".ENCKEY."') as payee,
			   AES_DECRYPT(var_2,'".ENCKEY."') as var_2
	           FROM ".PREFIX."_payment_processors 
	           WHERE id = '".$db->sql_quote($id)."' LIMIT 1 ";
	   $res = $db->query_db($sql,$print = DEBUG);
	   if ($db->num_rows($res) == 0 ) return false;
	   $processor = $db->fetch_db_array($res);
	   if($processor["var_2"] == "")return false;
	   $this->apiPassword = $processor["var_2"];
	   return true;
     }
	 /////////////////////////
	 function ipn(){
	 
	     $verified = $this->verify();
	     if(!$verified) return false;
	     return true;
	  
	 }
	  
	 function verify(){
     
	     if ($this->testmode == true) return true;
		 
	     $id = $_GET["id"];
	     self::setPayee($id);
	     self::setSecurityKey($id);
		 
	     $flagged = false;
	     $reason = "";
		 
	     if($_REQUEST['ap_test'] != 0) {$flagged = true;  $reason .= "Test mode is on, ";}
	     if($_REQUEST['ap_securitycode'] != $this->securityKey ) {$flagged = true;  $reason .= "Security code does not match, ";}
	     if(strtoupper($_REQUEST['ap_currency']) != $this->currency){ $flagged = true;  $reason .= "Currency does not match, ";}
	     if($_REQUEST["ap_merchant"] != $this->payee){ $flagged = true;  $reason .= "Merchant does not match, ";}
		 if(!in_array($_REQUEST["ap_status"],array("Success","Subscription-Payment-Success","Subscription-Payment-Rescheduled","Subscription-Payment-Failed","Subscription-Expired","Subscription-Canceled"))){$flagged = true;  $reason .= "Invalid Payment Status, ";}
	 
	      if(!$flagged) return true;
	     self::failedDeposit($id,$reason);
	     return false;
	
	}
	
	function failedDeposit($id,$reason)
	{
	  
	  $this->recordFailedPayment($id,$reason);	
	}
	
	 
	 function defineData(){
	 
       if($this->testmode == true) 
         {
		    foreach ($this->testdata as $data => $value) define($data,$value);
            return;
         }
    
	              
                   define('eid',$_REQUEST['apc_1']);
		           define('payer',$_REQUEST['ap_custemailaddress']); 
				   define('payee',$_REQUEST['ap_merchant']); 
                   define('amount',$_REQUEST['ap_totalamount']); 
				   define('currency',$_REQUEST['ap_currency']); 
                   define('description',$_REQUEST['ap_description']);
				   define('reference',$_REQUEST['ap_referencenumber']);
                   define('sub_id',$_REQUEST['ap_subscriptionreferencenumber']); 
				   
				 
				   switch($_REQUEST['ap_status']){
				   
				     case "Success":break;
					 case "Subscription-Payment-Success":$sub_status = "Active";break;
					 case "Subscription-Payment-Rescheduled":$sub_status = "Pending";break;
					 case "Subscription-Payment-Failed":$sub_status = "Pending";break;
					 case "Subscription-Expired":$sub_status = "Cancelled";break;
					 case "Subscription-Canceled":$sub_status = "Cancelled";break;
					 default:break;
					 
				   }
				   
                   define('sub_status',$sub_status); //Active|Pending|Cancelled
				 
				   	 
	 
	 }
	 
	
	function massPay(){
	    //https://api.alertpay.com/svc/api.svc/executemasspay
	}
	
	function autoPayment($id,$to,$amount,$currency,$memo = null){
	
	  
	  $return_array = array();
	  self::setPayee($id);
	  self::setAPIKey($id);
	  $testmode = ($this->testmode)?1:0;
	  
	  $post = sprintf("USER=%s&PASSWORD=%s&AMOUNT=%s&CURRENCY=%s&RECEIVEREMAIL=%s&SENDEREMAIL=%s&PURCHASETYPE=%s&NOTE=%s&TESTMODE=%s",
              urlencode($this->payee), 
			  urlencode($this->apiPassword), 
			  urlencode((string)$amount), 
			  urlencode($currency),
			  urlencode($to),
			  urlencode($this->payee), 
			  urlencode((string)1),
			  urlencode((string)$memo),
			  urlencode((string)$testmode)
	   );
	   
	    $response = self::sendRequest($this->sendMoneyUrl,$post);
		parse_str($response,$return_array);
		if(count($return_array) == 0)return array(false,"error processing request");
		if($return_array['RETURNCODE'] == 100)return array(true,$return_array['REFERENCENUMBER']);
		return array(false,$return_array['DESCRIPTION']);
	    
	  
	  //https://api.payza.com/svc/api.svc/sendmoney
	  //USER=$from&PASSWORD=$password&AMOUNT=$amount&CURRENCY=$currency&RECEIVEREMAIL=$to&PURCHASETYPE=1&NOTE=$memo&TESTMODE=0
	  //RETURNCODE=100&REFERENCENUMBER=xxxxx-xxxxx-xxxx&DESCRIPTION= Transaction%20was completed%20successfully&TESTMODE=0
	  //RETURNCODE=100&REFERENCENUMBER=&DESCRIPTION= Transaction%20was completed%20successfully&TESTMODE=1
	  //RETURNCODE=221&REFERENCENUMBER=&DESCRIPTION=Invalid%20USER%20or%20PASSWORD&TESTMODE=0
	  
	 
	}
	
	
	function cancelSubscription($id,$key,$memo = null){
	  
	  $return_array = array();
	  self::setPayee($id);
	  self::setAPIKey($id);
	  $testmode = ($this->testmode)?1:0;
	  
	  $post = sprintf("USER=%s&PASSWORD=%s&SUBSCRIPTIONREFERENCE=%s&NOTE=%s&TESTMODE=%s",
              urlencode($this->payee), 
			  urlencode($this->apiPassword), 
			  urlencode((string)$key), 
			  urlencode((string)$memo),
			  urlencode((string)$testmode)
	   );
	   
	    $response = self::sendRequest($this->cancelSubUrl,$post);
		parse_str($response,$return_array);
		if(count($return_array) == 0)return array(false,"error processing request");
		if($return_array['RETURNCODE'] == 100)return array(true,$return_array['REFERENCENUMBER']);
		return array(false,$return_array['DESCRIPTION']);
	    
	//https://api.alertpay.com/svc/api.svc/CancelSubscription
	//USER=$from&PASSWORD=$password&SUBSCRIPTIONREFERENCE=$reference&NOTE=$memo&TESTMODE=$testmode
	//RETURNCODE=100&REFERENCENUMBER=S-xxxxx-xxxxx-xxxx&DESCRIPTION= Transaction%20was completed%20successfully&TESTMODE=0
	//RETURNCODE=100&REFERENCENUMBER=S-xxxxx-xxxxx-xxxx&DESCRIPTION= Transaction%20was completed%20successfully&TESTMODE=1
	//RETURNCODE=221&REFERENCENUMBER=&DESCRIPTION=Invalid%20USER%20or%20PASSWORD&TESTMODE=0

	}
	
	function sendRequest($url,$data)
	{
	   
	   $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        curl_close($ch);
		return $response;
	}
	
	 
		   
};

?>
