<?php
global $system,$db,$userinfo,$settings;

$payment = $system->importClass('payment');

$var = $system->getVar();

if(!isset($var['pid'])) header("location:index.php");


   if($var['submit'])
   {
      
	
	 if($var['values']["email"] == "") $errors[] = "Email address cannot be empty";
	 elseif(!Validate::email($var['values']["email"] )) $errors[] = "Please add a valid email address";
	/* elseif($db->if_row_exist("_payment_user_accounts",array("username"=>$userinfo['username'],"pro_id"=>$var['pid'],"account_id"=>$account_id))$errors[] = "This account already exist";*/
	 if($settings['account']['use_security_question'] == true && $userinfo['question']!="")
	 if($var['answer'] != $userinfo['answer']) {$errors = array(); $errors[] =  "Incorrect answer to security question";}
	 if(is_array($errors) && !empty($errors))while (list($key,$value) = each($errors)) $error_msg.= $value.'<br>';	//$system->logMessage("error",$errors); //
	 else 
	 {
	   $account_id = $var['values']["email"];
	   $added = $payment->addAccount($userinfo['username'],$var['pid'],$account_id,$var['description'],$var['values']);
	   if($added) $success_msg = "Account has been added";
	 }
   
   }
   



   $pageInfo['title'] = 'Manage Accounts';
  
   $pageInfo['map'] = array('Manage Accounts' => '',);
   
   $loader = new Loader;
   
   $data= get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'add.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('private');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('private');
   
   $loader->displayOutput();