<?php
require_once "api/api.php";
require_once "sci/sci.php";
class Egopay extends Payment{
 
	
	var $storeID;
	var $storePass;
	var $apiName;
	var $apiID;
	var $apiPass;
	var $currency = 'USD';
	var $default_button_buy = '';
	var $postdata = array(
	
	
	);

    var $testmode = false;
    var $testdata = array(
	
	               'eid'=>'DcXSJ',	           
		           'payer'=>'payer@test.com',	          
		           'payee'=>'reciever@test.com',	        
		           'amount'=>'100',	      
		           'currency'=>'USD',	           
		           'description'=>'Account Funding',	          
		           'reference'=>'REF-1234-1234-1234',

               );
		         
	
       function button( $data, $type ){ //required

               ///////////////////////////////
               /* $defined = array (
                //USER DEFINED FOR SIMPLE 
                'amount' => , //required
                'discription' => ,
                'reference' => ,
                'success' => ,
                'fail' => ,
				'auto_submit'=>,				
				
				//optional
				'button' => , //  link to image for submit button
				'custom' => array( custom 1, custom 2.... custom n), //  custom data
				
				 //USER DEFINED FOR SUBSCRIBE
				'sub_time' =>'Year', //Day,Week,Month,Year
                'sub_duration' => 1,
                'sub_period' =>5,

  
                //SYSTEM DEFINED
                'payee' => $this->storeID, //required
                'currency' => $this->currency, //required
				
 
  
            );*/
             
 
          if(!$this->__checkRequired($data,$type)) return "Parameters are not configured";
		  return $this->makeForm($data,$type);
       
     }


     function makeForm($data,$type){
 
            $data['button'] = ($data['button']) ? $data['button'] : $this->default_button_buy;
			
			switch($type){
			case'simple':
			case'advanced':	
			
			 $oEgopay = new EgoPaySci(array(
                             'store_id'          => $this->storeID,
                             'store_password'    => $this->storePass
                           ));
    
             $sPaymentHash = $oEgopay->createHash(array(
                             'amount'    => $data['amount'],
	                         'currency'  => $this->currency,
	                         'description' => $data['discription'],
	                         'fail_url'	=> $data['fail'],
	                         'success_url'	=> $data['success'],
	                         'cf_1' => $data['eid'],
						 ));
			
			
			$form ="<form action='".EgoPaySci::EGOPAY_PAYMENT_URL."' method='post' name='pro'>    
                      <input type='hidden' name='hash' value='".$sPaymentHash."' />";
			break;
			}

		    if(!$data['auto_submit'])  $form.="<input type=\"submit\" name=\"submit\" value=\"Submit\"  /></form>";
            else  $form.="</form><script>document.forms.pro.submit();</script>";
            return $form;
		
	 }
	 
	  function __checkRequired($data, $type) {
	  
	  if(!isset($data['id'])) return false;
	  if(!self::setStoreID($data['id'])) return false;
	  if($type == "advanced") if(!self::setStorePass($data['id']))return false;
	  if(!isset($data['amount']) && !isset($this->currency)) return false;
	  return true;
	    
	 }
	 
	 
	 function setStoreID($id)
	 {
	  
	   global $db;
	   $sql = "SELECT  
	   AES_DECRYPT(payee,'".ENCKEY."') as payee 
	   FROM ".PREFIX."_payment_processors WHERE id = '".$db->sql_quote($id)."' 
	   LIMIT 1 ";
	   $res = $db->query_db($sql,$print = DEBUG);
	   if ($db->num_rows($res) == 0 ) return false;
	   $processor = $db->fetch_db_array($res);
	   if($processor["payee"] == "")return false;
	   $this->storeID = $processor["payee"];
	   return true;
	   
     }
	 
	 function setStorePass($id)
	 {
	   global $db;
	   $sql = "SELECT *,
			   AES_DECRYPT(var_1,'".ENCKEY."') as var_1
	           FROM ".PREFIX."_payment_processors 
	           WHERE id = '".$db->sql_quote($id)."' LIMIT 1 ";
	   $res = $db->query_db($sql,$print = DEBUG);
	   if ($db->num_rows($res) == 0 ) return false;
	   $processor = $db->fetch_db_array($res);
	   if($processor["var_1"] == "")return false;
	   $this->storePass = $processor["var_1"];
	   return true;
     }
	 
	 function setApiName($id)
	 {
	   global $db;
	   $sql = "SELECT *,
			   AES_DECRYPT(var_2,'".ENCKEY."') as var_2
	           FROM ".PREFIX."_payment_processors 
	           WHERE id = '".$db->sql_quote($id)."' LIMIT 1 ";
	   $res = $db->query_db($sql,$print = DEBUG);
	   if ($db->num_rows($res) == 0 ) return false;
	   $processor = $db->fetch_db_array($res);
	   if($processor["var_2"] == "")return false;
	   $this->apiName = $processor["var_2"];
	   return true;
     }
	 function setApiID($id)
	 {
	   global $db;
	   $sql = "SELECT *,
			   AES_DECRYPT(var_3,'".ENCKEY."') as var_3
	           FROM ".PREFIX."_payment_processors 
	           WHERE id = '".$db->sql_quote($id)."' LIMIT 1 ";
	   $res = $db->query_db($sql,$print = DEBUG);
	   if ($db->num_rows($res) == 0 ) return false;
	   $processor = $db->fetch_db_array($res);
	   if($processor["var_3"] == "")return false;
	   $this->apiID = $processor["var_3"];
	   return true;
     }
	 function setApiPass($id)
	 {
	   global $db;
	   $sql = "SELECT *,
			   AES_DECRYPT(var_4,'".ENCKEY."') as var_4
	           FROM ".PREFIX."_payment_processors 
	           WHERE id = '".$db->sql_quote($id)."' LIMIT 1 ";
	   $res = $db->query_db($sql,$print = DEBUG);
	   if ($db->num_rows($res) == 0 ) return false;
	   $processor = $db->fetch_db_array($res);
	   if($processor["var_4"] == "")return false;
	   $this->apiPass = $processor["var_4"];
	   return true;
     }
	 /////////////////////////
	 function ipn(){
	 
	     $verified = $this->verify();
	     if(!$verified) return false;
	     return true;
	  
	 }
	  
	 function verify(){
     
	     if ($this->testmode == true) return true;
		 $reason = "";
		 $flagged = false;
	     $id = $_GET["id"];
	     if(!self::setStoreID($id)){$flagged = true;  $reason .= "Store ID is not set, ";}
	     if(!self::setStorePass($id)){$flagged = true;  $reason .= "Store Pass is not set, ";}
	
	     try{
		
		  $oEgopay = new EgoPaySciCallback(array(
                             'store_id'          => $this->storeID,
                             'store_password'    => $this->storePass
                    ));
		  $aResponse = $oEgopay->getResponse($_POST);
          } catch (Exception $e) {
		       
			    $flagged = true;  $reason .= $e->getMessage();
          }
	
	     if ($aResponse["sStatus"] != "Completed"){$flagged = true;  $reason .= " Payment status is not Complete, ";}
		 if ($aResponse["sCurrency"] != 'USD'){$flagged = true;  $reason .= "Currency does not match, ";}
	     if(!$flagged) return true;
	     self::failedDeposit($id,$reason);
	     return false;
	
	}
	
	function failedDeposit($id,$reason)
	{
	  $this->recordFailedPayment($id,$reason);	
	}
	
	 
	 function defineData(){
	 
       if($this->testmode == true) 
         {
		    foreach ($this->testdata as $data => $value) define($data,$value);
            return;
         } 
                   $id = $_GET["id"];
	               self::setStoreID($id);
	               self::setStorePass($id);
				   $oEgopay = new EgoPaySciCallback(array(
                             'store_id'          => $this->storeID,
                             'store_password'    => $this->storePass
                    ));
		           $aResponse = $oEgopay->getResponse($_POST);	 
				  
				  
				   define('eid', $aResponse['cf_1']);
		           define('payer',$aResponse['sEmail']); 
				   define('payee',$this->storeID); 
                   define('amount',$aResponse['fAmount']); 
				   define('currency', $aResponse['sCurrency']); 
                   define('description', $aResponse['sDetails']);
				   define('reference', $aResponse['sId']);
	}
	
	
	function massPay()
	{
	  
	}
	
	function autoPayment($id,$to,$amount,$currency,$memo = null)
	{
	  global $settings;
	  $return_array = array();
	  self::setApiName($id);
	  self::setApiID($id);
	  self::setApiPass($id);
	  $testmode = ($this->testmode)?1:0;
	  $memo = ($memo == null) ? 'Payment from '.$settings['system']['websitename']:$memo;
	
	   try{
		
		  $oAuth = new EgoPayAuth($this->apiName,$this->apiID,$this->apiPass);
		  $oJsonApiAgent = new EgoPayJsonApiAgent($oAuth);
	      $oResponse = $oJsonApiAgent->getTransfer($to,$amount,$currency,$memo);
		  
          } catch (Exception $e) {
		       
			   return array(false,$e->getMessage());
          }
	  
      //var_dump($oResponse);
	  $return_array =  objectToArray($oResponse);
	  if(count($return_array) == 0)return array(false,"error processing request");
	  if($return_array['status'] == 'ok')return array(true,$return_array['transaction']['sId']);
	  return array(false,'error processing request');
	  
	  
	  /*
	  object(stdClass)[5]
  public 'transaction' =>
    object(stdClass)[6]
      public 'sId' => string '55PT1U-ELU4Z8-7RDTJY' (length=20)
      public 'sDate' => string '2012-12-13 17:32:45' (length=19)
      public 'fAmount' => int -10
      public 'sCurrency' => string 'USD' (length=3)
      public 'fFee' => int 0
      public 'sType' => string '(api) Sent to' (length=13)
      public 'iTypeId' => string '12' (length=2)
      public 'sEmail' => string 'receiver@test.com' (length=19)
      public 'sDetails' => string 'Api send test' (length=12)
      public 'sStatus' => string 'Completed' (length=9)
  public 'status' => string 'ok' (length=2)*/    
	}
	
	
	function cancelSubscription($key,$memo = null){
	  
	  $return_array = array();
	  $testmode = ($this->testmode)?1:0;
	  
	 

	}
	
	function sendRequest($url,$data)
	{
	   
	   $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        curl_close($ch);
		return $response;
	}
		   
}


        function objectToArray($d) {
		if (is_object($d)) {
			// Gets the properties of the given object
			// with get_object_vars function
			$d = get_object_vars($d);
		}
 
		if (is_array($d)) {
			/*
			* Return array converted to object
			* Using __FUNCTION__ (Magic constant)
			* for recursive call
			*/
			return array_map(__FUNCTION__, $d);
		}
		else {
			// Return array
			return $d;
		}
	}
	
	
	    function arrayToObject($d) {
		if (is_array($d)) {
			/*
			* Return array converted to object
			* Using __FUNCTION__ (Magic constant)
			* for recursive call
			*/
			return (object) array_map(__FUNCTION__, $d);
		}
		else {
			// Return object
			return $d;
		}
	}

?>
