    <h1>Edit <?php echo $processor['name']?> Details</h1>

    <p class="info">Here you can edit a payment gateway</p>
	
	<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>
	
	<form  action="" method="post">
    <input type="hidden" name="pid" value="<?php echo $var['pid']?>" />
	<table   cellpadding="0" cellspacing="0" class="tableS">
	<thead>
	<tr>
		<th colspan="2" align="left">Processor Details</th>	
	</tr>
	</thead>
	 <?php if(count($processor) == 0) : ?>
	 
      <tr>
		<td colspan="2"> - no records found - </td>
	  </tr>
	  <?php else: ?> 
       <tr>
						<td width="30%">Name</td>
                        <td><input type="text" name="name" value="<?php echo $processor['name'] ?>" /></td>
	   </tr> 
	   <tr>
	     				<td colspan="2">
						<p class="info"><b>Setting Your CallBack URL</b></p>				
						<h4>For instant payment crediting to account, you have to set up your Callback URL on http://www.egopay.com. <br />To set up your Callback URL, just follow these steps:</h4>
						<ol>
						  <li>Login to your Egopay account.</li>
						  <li>Click on &quot;MERCHANT&quot; Menu.</li>
						  <li>Under &quot;Stores&quot;, click on &quot;the edit&quot; icon of your store to edit a store or the &quot;add new&quot; button to add a new store.</li>
						  <li>Enter the information:
						    <ol>
							  <li>For Store Name, enter a desired name.</li>
							  <li>Enter "<b><?php echo SITEURL."/members/";?></b>" for success url and fail url</li>
						      <li>For Callback URL, enter "<b><?php echo SITEURL."/payment/ipn/id/".$var['pid'];//$settings["system"]["websiteurl"]?></b>"</li>
							  <li><b>DO NOT</b> check the Test Mode and Verify Integrity boxes</li>
					        </ol>
					      </li>
						  <li>Click on &quot;Save&quot; button.</li>
						  </ol>	 </td>
                        
	   </tr>
	   <thead>
	   <tr>
		<th colspan="2" align="left">Store Details </th>	
	   </tr>
	   </thead>
	   <tr>
	     				<td>Store ID</td>
                        <td><input type="text" name="store_id" value="<?php echo $processor['payee'] ?>" /><img src="images/tooltip.png" alt="Tip" class="tooltip" title="You can find this by clicking on the info icon on your manage stores page" /></td>
	   </tr>
	    <tr>
						<td>Store Pass</td>
                        <td><input type="password" name="store_pass" value="<?php echo $processor['var_1'] ?>" /><img src="images/tooltip.png" alt="Tip" class="tooltip" title="You can find this by clicking on the info icon on your manage stores page" /></td>
	   </tr>
	   <thead>
	   <tr>
		<th colspan="2" align="left">API details <img src="images/tooltip.png" alt="Tip" class="tooltip" title="Leave this section blank if you do not want to make mass payments with Egopay" /></th>	
	   </tr>
	   </thead>
	    <tr>
						<td>API Name</td>
                        <td><input type="text" name="api_name" value="<?php echo $processor['var_2'] ?>" /><img src="images/tooltip.png" alt="Tip" class="tooltip" title="You can find by clicking on the info icon on your manage API page" /></td>
	   </tr> 
	    <tr>
						<td>API ID</td>
                        <td><input type="text" name="api_id" value="<?php echo $processor['var_3'] ?>" /><img src="images/tooltip.png" alt="Tip" class="tooltip" title="You can find by clicking on the info icon on your manage API page" /></td>
	   </tr> 
	   <tr>
						<td>API Pass</td>
                        <td><input type="password" name="api_pass" value="<?php echo $processor['var_4'] ?>" /><img src="images/tooltip.png" alt="Tip" class="tooltip" title="You can find by clicking on the info icon on your manage API page" /></td>
	   </tr> 
	    <thead>
	   <tr>
		<th colspan="2" align="left">Other Settings</th>	
	   </tr>
	   </thead>
       <tr>
						<td>Currencies</td>
                        <td><?php foreach ($settings['payment']['currencies'] as $id => $currency ) { ?>
	 
	 <?php echo $id; ?> <input name="currencies[]" type="checkbox"  value="<?php echo $id; ?>" <?php if ((stristr($processor['currencies'],",".$id.",")== true)) echo "checked=\"checked\" "; ?>  />
	 
	 
	  <?php } ?></td>
	   </tr>
	    <tr>
		
						<td>Allow Deposit</td>
                        <td><select name="deposit">
                               <option value="Y" <?php if($processor['deposit'] == 'Y')  echo "selected"?>>Yes</option>
                               <option value="N" <?php if($processor['deposit'] == 'N')  echo "selected"?>>No</option>
                           </select> 
                         </td>
	    </tr>
		 <tr>
						<td>Deposit Fee</td>
                        <td><input type="text" name="dfee" value="<?php echo $processor['dfee'] ?>" /></td>
	   </tr> 
	   <tr>
		
						<td>Allow Withdrawal</td>
                        <td><select name="withdraw">
                               <option value="Y" <?php if($processor['withdraw'] == 'Y')  echo "selected"?>>Yes</option>
                               <option value="N" <?php if($processor['withdraw'] == 'N')  echo "selected"?>>No</option>
                           </select> 
                         </td>
	    </tr>
		 <tr>
		
						<td>Enable Auto Withdrawal</td>
                        <td><select name="autopay">
                               <option value="Y" <?php if($processor['autopay'] == 'Y')  echo "selected"?>>Yes</option>
                               <option value="N" <?php if($processor['autopay'] == 'N')  echo "selected"?>>No</option>
                           </select> <img src="images/tooltip.png" alt="Tip" class="tooltip" title="If this is enabled, users will be paid automatcally when they make a withdrawal from their wallet. API details must be set for this to work." />
                         </td>
	    </tr>
		<tr>
						<td>Withdrawal Fee</td>
                        <td><input type="text" name="wfee" value="<?php echo $processor['wfee'] ?>" /></td>
	   </tr> 
		<!--<tr>
		
						<td>Allow Subscriptions</td>
                        <td><select name="subscribe">
                               <option value="Y" <?php if($processor['subscribe'] == 'Y')  echo "selected"?>>Yes</option>
                               <option value="N" <?php if($processor['subscribe'] == 'N')  echo "selected"?>>No</option>
                           </select> 
                         </td>
	    </tr>
		 <tr>
						<td>Subscription setup fee</td>
                        <td><input type="text" name="sfee" value="<?php echo $processor['sfee'] ?>" /></td>
	    </tr> -->
	    <tr>
		
						<td>Status</td>
                        <td><select name="status">
                               <option value="Active" <?php if($processor['status'] == 'Active')  echo "selected"?>>Active</option>
                               <option value="Pending" <?php if($processor['status'] == 'Pending')  echo "selected"?>>Pending</option>
                           </select> 
                         </td>
	    </tr>  
        <tr>
            <td colspan="2" align="left"><input type="submit" name="submit" value="Update" class="button_link">	</td>
        </tr>
     <?php endif; ?> 
	</table>
   
</form>

	