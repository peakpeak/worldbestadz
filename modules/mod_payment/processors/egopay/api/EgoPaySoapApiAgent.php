<?php
require_once 'EgoPayApiAgentInterface.php';

class EgoPaySoapApiAgent implements EgoPayApiAgentInterface
{
    const VERSION = '1.1';
    
    const EGOPAY_API_PAYMENT_WSDL_URL = "https://www.egopay.com/api/soap?wsdl";
    const EGOPAY_API_PAYMENT_HEADER_URL = "https://www.egopay.com/soap/";
    
    private $_oAuth;
    private $_oClient;
    
    public function __construct(EgoPayAuth $a)
    {
        $this->_oAuth = $a;
    }        
    
    public function getBalance($sCurrency = null)
    {
        $this->_setupCredentials();
           
        $oBalance = $this->_oClient->balance();
        $this->_checkError($oBalance);
        
        if ($sCurrency !== null)
            if (isset($oBalance->{$sCurrency}))
                return $oBalance->{$sCurrency};
            else
                return null;
        
        return $oBalance;
    }
    
    public function getFindTransaction($iTransactionId)
    {
        $aData = array(
            'transactionId' => $iTransactionId
        );
        $this->_setupCredentials($aData);
        $oTransaction = $this->_oClient->findTransaction($aData);
        $this->_checkError($oTransaction);
        return $oTransaction;
    }
    
    public function getTransfer($sPayeeEmail, $fAmount, $sCurrency, $sDetails)
    {
        $aData = array(
            'payeeEmail'    => $sPayeeEmail,
            'amount'        => $fAmount,
            'currency'      => $sCurrency,
            'details'       => $sDetails            
        );
        $this->_setupCredentials($aData);
        $oTransfer = $this->_oClient->transfer($aData);
        $this->_checkError($oTransfer);
        return $oTransfer;
    }
       
    public function getHistory($aParams = array())
    {        
        $this->_setupCredentials($aParams);
        $oHistory = $this->_oClient->history($aParams);
        $this->_checkError($oHistory);
        return $oHistory;
    }
    
    private static function _generateId()
    {
        return uniqid();
    }
    
    private function _setupCredentials($aData=array())
    {                
        $this->_oClient = new SoapClient(self::EGOPAY_API_PAYMENT_WSDL_URL);
        $oHeader = new SoapHeader(
            self::EGOPAY_API_PAYMENT_HEADER_URL,
            "authenticate",
            new SoapVar($this->_buildAuthenticationQuery($aData), SOAP_ENC_OBJECT)
        );
        $this->_oClient->__setSoapHeaders(array($oHeader));      
    }
    
    private function _buildAuthenticationQuery($aData)
    {
        $aHeader = array(
            'id'            => self::_generateId(),
            'version'       => self::VERSION,
            'account_name'  => $this->_oAuth->getAccountName(),
            'api_id'        => $this->_oAuth->getApiId(),
            'ts'            => time(),
        );
        $aData = array_merge($aHeader, array_filter($aData));
        ksort($aData);
        $aHeader['h'] = hash('sha256',$this->_oAuth->getApiPass() . '|' . implode('|', $aData));        
        return $aHeader;
    }      
    
    private function _checkError($response)
    {
        if ($response === null)
            throw new EgoPayApiException('Invalid Response Format', (int) 0);
        if (isset($response->status) && $response->status == 'ERROR')
            throw new EgoPayApiException($response->error_message, (int) $response->error_code);
    }        
}