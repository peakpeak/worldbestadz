<?php
/*
PM Class by Charles Owivri
email:amin@wizziweb.net
Website:http://wizziweb.net
*/


class PerfectMoneyAPI {

var $PAYER_ID;
var $PAYER_ACCOUNT;
var $PAYEE_ACCOUNT;
var $PAYMENT_AMOUNT;
//var $PAYMENT_CURRENCY;
var $MEMO;
var $MERCHANT_REF;
var	$SEC_WORD;
var	$isPrivate = false;

function make_spend($amt,$payer_id,$payer,$payee,$cur='',$descr = '',$secret,$mref)
{ 
	//// Assign Variables ///////
	$this->PAYER_ID 		    = $payer_id;
	$this->PAYER_ACCOUNT 		= $payer;
	$this->PAYEE_ACCOUNT 		= $payee;
	$this->PAYMENT_AMOUNT		= $amt;
	//$this->PAYMENT_CURRENCY 	= $cur;
	$this->MERCHANT_REF			= $mref;
	$this->MEMO 				= $descr;
	$this->SEC_WORD				= $secret;
	$this->isPrivate			= $isPrivate;
	
	
	
	if (!$this->PAYER_ACCOUNT || !$this->PAYEE_ACCOUNT || !$this->PAYER_ID)
	{
		return array(false,"Please Provide All Required Filed");
	}
	   else if ($this->valid_Ac($this->PAYER_ACCOUNT ) != true  || $this->valid_Ac($this->PAYER_ACCOUNT ) != true)
	{
	    return array(false,"Please Provide a valid account PerfectMoney Account for Payer and Payee");
	}
	   else if ($this->valid_amount($this->PAYMENT_AMOUNT) != true || $this->PAYMENT_AMOUNT < 0)
	{
	 	return array(false,"Please Provide a valid Amount. Make sure Amount is Grater than 0");
	}
	   else if (!$this->SEC_WORD )
	{
		return array(false,"Please Provide PassPhrase.");
    }
	 else
	{
	   //$this->Rand_id 	= $this->rand_id();		
	   return 	$this->Make_Request($payer,$payer_id,$secret,$payee,$amt,$descr);
										
	}
		
}

function valid_Ac($acct)
{
	return ereg("^(U|E|G)[0-9]{1,}$", $acct);
}

function valid_amount($amount)
{
	return is_numeric($amount); 
}

function rand_id()
{
	return time().rand(0,9999);
}
	
function Make_Request($from,$acct_id,$secret,$to,$amount,$memo)
{
    $url = "https://perfectmoney.is/acct/confirm.asp?AccountID=$acct_id&PassPhrase=$secret&Payer_Account=$from&Payee_Account=$to&Amount=$amount";
    $f = fopen($url,'rb');
    if(!$f)return array(false,"Error opening url");
 

    // getting data
    $response = array();
    $response = "";
    while(!feof($f)) $response .= fgets($f);
 
    fclose($f);
    $result = $this->parseResponse($response);
 
    if (!$result['ERROR'])
    {
		return array(true,$result['PAYMENT_BATCH_NUM']);
	}
	else
	{
	 	return array(false,$result['ERROR']);
    }
  
}


function parseResponse($response)
{
 
  // searching for hidden fields
  if(!preg_match_all("/<input name='(.*)' type='hidden' value='(.*)'>/", $response, $result, PREG_SET_ORDER))
  {
    return array(false,"Invalid Output");
    exit;
  }

  $ar = array();
  $ar = "";

  foreach($result as $item)
  {
    $key = $item[1];
    $ar[$key] = $item[2];
  }
  return $ar;
  /*echo '<pre>';
  print_r($ar);
  echo '</pre>';
  */
 }
 
}
?>