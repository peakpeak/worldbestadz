<?php
class PerfectMoney extends Payment{
 
	
	var $accountID;
	var $accountNum;
	var $accountPass;
	var $accountAltPass;
	var $paymentURL = "https://perfectmoney.is/api/step1.asp";
	var $currency = 'USD';
	var $default_button_buy = '';
	var $postdata = array(
	
	
	);

    var $testmode = false;
    var $testdata = array(

               	   'eid'=>'W544y',	           
		           'payer'=>'payer@test.com',	          
		           'payee'=>'reciever@test.com',	        
		           'amount'=>'100',	      
		           'currency'=>'USD',	           
		           'description'=>'Account Funding',	          
		           'reference'=>'REF-1234-1234-1234',
				          
		        
                  
               );
		         
	
       function button( $data, $type ){ //required

               ///////////////////////////////
               /* $defined = array (
                //USER DEFINED FOR SIMPLE 
                'amount' => , //required
                'discription' => ,
                'reference' => ,
                'success' => ,
                'fail' => ,
				'auto_submit'=>,				
				
				//optional
				'button' => , //  link to image for submit button
				'custom' => array( custom 1, custom 2.... custom n), //  custom data
				
				 //USER DEFINED FOR SUBSCRIBE
				'sub_time' =>'Year', //Day,Week,Month,Year
                'sub_duration' => 1,
                'sub_period' =>5,

  
                //SYSTEM DEFINED
                'payee' => $this->storeID, //required
                'currency' => $this->currency, //required
				
 
  
            );*/
             
 
          if(!$this->__checkRequired($data,$type)) return "Parameters are not configured";
		  return $this->makeForm($data,$type);
       
     }


     function makeForm($data,$type){
            global $settings;
            $data['button'] = ($data['button']) ? $data['button'] : $this->default_button_buy;
			
			switch($type){
			case'simple':
			case'advanced':	
		   
		    $form =  "<form action='".$this->paymentURL."' method='POST' name='pro'>
                         <input type='hidden' name='PAYEE_ACCOUNT' value='".$this->accountNum."'>   
                         <input type='hidden' name='PAYEE_NAME' value='".$settings["system"]["websitename"]."'>   
                         <input type='hidden' name='PAYMENT_ID' value='".$data['eid']."'>   
                         <input type='hidden' name='PAYMENT_AMOUNT' value='".$data["amount"]."'>   
                         <input type='hidden' name='PAYMENT_UNITS' value='USD'>  
                         <input type='hidden' name='STATUS_URL' value='".SITEURL."/payment/ipn/id/".$data['id']."'> 
                         <input type='hidden' name='PAYMENT_URL' value='".$data['success']."'>
                         <input type='hidden' name='NOPAYMENT_URL' value='".$data['fail']."'>
                         <input type='hidden' name='SUGGESTED_MEMO' value='".$data['discription']."'>";
      
			break;
			}

		    if(!$data['auto_submit'])  $form.="<input type=\"submit\" name=\"submit\" value=\"Submit\"  /></form>";
            else  $form.="</form><script>document.forms.pro.submit();</script>";
            return $form;
		
	 }
	 
	  function __checkRequired($data, $type) {
	  
	  if(!isset($data['id'])) return false;
	  if(!self::setAccountNum($data['id'])) return false;
	  if($type == "advanced") if(!self::setAccountAltPass($data['id']))return false;
	  if(!isset($data['amount']) && !isset($this->currency)) return false;
	  return true;
	    
	 }
	 
	 function setAccountID($id)
	 {
	  
	   global $db;
	   $sql = "SELECT  
	   AES_DECRYPT(var_1,'".ENCKEY."') as var_1 
	   FROM ".PREFIX."_payment_processors WHERE id = '".$db->sql_quote($id)."' 
	   LIMIT 1 ";
	   $res = $db->query_db($sql,$print = DEBUG);
	   if ($db->num_rows($res) == 0 ) return false;
	   $processor = $db->fetch_db_array($res);
	   if($processor["var_1"] == "")return false;
	   $this->accountID = $processor["var_1"];
	   return true;
	   
     }
	 
	 function setAccountNum($id)
	 {
	  
	   global $db;
	   $sql = "SELECT  
	   AES_DECRYPT(payee,'".ENCKEY."') as payee 
	   FROM ".PREFIX."_payment_processors WHERE id = '".$db->sql_quote($id)."' 
	   LIMIT 1 ";
	   $res = $db->query_db($sql,$print = DEBUG);
	   if ($db->num_rows($res) == 0 ) return false;
	   $processor = $db->fetch_db_array($res);
	   if($processor["payee"] == "")return false;
	   $this->accountNum = $processor["payee"];
	   return true;
	   
     }
	 function setAccountPass($id)
	 {
	  
	   global $db;
	   $sql = "SELECT  
	   AES_DECRYPT(var_2,'".ENCKEY."') as var_2 
	   FROM ".PREFIX."_payment_processors WHERE id = '".$db->sql_quote($id)."' 
	   LIMIT 1 ";
	   $res = $db->query_db($sql,$print = DEBUG);
	   if ($db->num_rows($res) == 0 ) return false;
	   $processor = $db->fetch_db_array($res);
	   if($processor["var_2"] == "")return false;
	   $this->accountPass = $processor["var_2"];
	   return true;
	   
     }
	 
	 function setAccountAltPass($id)
	 {
	   global $db;
	   $sql = "SELECT *,
			   AES_DECRYPT(var_3,'".ENCKEY."') as var_3
	           FROM ".PREFIX."_payment_processors 
	           WHERE id = '".$db->sql_quote($id)."' LIMIT 1 ";
	   $res = $db->query_db($sql,$print = DEBUG);
	   if ($db->num_rows($res) == 0 ) return false;
	   $processor = $db->fetch_db_array($res);
	   if($processor["var_3"] == "")return false;
	   $this->accountAltPass = $processor["var_3"];
	   return true;
     }
	 
	 
	 /////////////////////////
	 function ipn(){
	 
	     $verified = $this->verify();
	     if(!$verified) return false;
	     return true;
	  
	 }
	  
	 function verify(){
     
	     if ($this->testmode == true) return true;
		 $reason = "";
		 $flagged = false;
	     $id = $_GET["id"];
	     if(!self::setAccountNum($id)){$flagged = true;  $reason .= "Account ID is not set, ";}
		 if(!self::setAccountAltPass($id)){$flagged = true;  $reason .= "Alt Pass is not set, ";}
	   
		 
		 $string = $_REQUEST["PAYMENT_ID"].":".$_REQUEST['PAYEE_ACCOUNT'].":".$_REQUEST['PAYMENT_AMOUNT'].":".$_REQUEST['PAYMENT_UNITS'].":".$_REQUEST['PAYMENT_BATCH_NUM'].":".$_REQUEST['PAYER_ACCOUNT'].":".strtoupper(md5($this->accountAltPass)).":".$_POST['TIMESTAMPGMT'];
	
	     $c_hash = strtoupper(md5($string));
		
		 
		 if($_REQUEST["V2_HASH"] != $c_hash){$flagged = true;  $reason .= "Payment hash does not match, ";}
		 if(strtoupper($_REQUEST["PAYMENT_UNITS"]  )!=	"USD"){$flagged = true;  $reason .= "Currency does not match,";}
		 
	     if(!$flagged) return true;
	     self::failedDeposit($id,$reason);
	     return false;
	
	}
	
	function failedDeposit($id,$reason)
	{
	  
	  $this->recordFailedPayment($id,$reason);	
	}
	
	 
	 function defineData(){
	 
       if($this->testmode == true) 
         {
		    foreach ($this->testdata as $data => $value) define($data,$value);
            return;
         } 
	
                   define('eid',$_REQUEST['PAYMENT_ID']);
		           define('payer',$_REQUEST['PAYER_ACCOUNT']); 
				   define('payee',$_REQUEST['PAYEE_ACCOUNT']); 
                   define('amount',$_REQUEST['PAYMENT_AMOUNT']); 
				   define('currency','USD'); 
                   define('description',$_REQUEST['SUGGESTED_MEMO']);
				   define('reference',$_REQUEST['PAYMENT_BATCH_NUM']);
	}
	 
	
	function massPay()
	{
	  
	}
	
	function autoPayment($id,$to,$amount,$currency,$memo = null)
	{
	  
	  $return_array = array();
	  self::setAccountID($id);
	  self::setAccountNum($id);
	  self::setAccountPass($id);
	  $testmode = ($this->testmode)?1:0;
	  include_once("api.php");
	  $spend  = new PerfectMoneyAPI;
	  $result = $spend->make_spend($amount,$this->accountID,$this->accountNum,$to,'USD',$memo,(trim($this->accountPass)),null);
	  if ($result[0] != true)return array(false,$result[1]); 
	  return array(true,$result[1]);
	  
	
	}
	
	
	function cancelSubscription($key,$memo = null){
	  
	  $return_array = array();
	  $testmode = ($this->testmode)?1:0;
	  
	 

	}
	
	function sendRequest($url,$data)
	{
	   
	   $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        curl_close($ch);
		return $response;
	}
		   
};



?>
