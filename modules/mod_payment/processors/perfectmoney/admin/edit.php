<?php
global $system,$userinfo,$db,$settings;

$var = $system->getVar();

if(!isset($var['pid'])) header("location:index.php");



   if($var['submit'])
   switch($var['submit'])
   {

    
      case('Update'):
      Validate::trim_post();
	  
	  if($var['pid'] == '') $errors[] =  "Please select a valid payment option";
	    
	  if(is_array($errors) && !empty($errors)) while (list($key,$value) = each($errors)) $return_msg.= $value.'<br>';	
		  
	  else 
	  {
	   
		    $sql = "SELECT id FROM ".PREFIX."_payment_processors WHERE id = '".$var['pid']."' ";
			$res = $db->query_db($sql,$print = DEBUG);
			if ($db->num_rows($res) > 0)
			{ 
			  
               $currencies = "";
			   if (count($var['currencies']) > 0)
	           foreach ($var['currencies'] as $currency)$currencies .= ","."$currency".",";
		       else $currencies = "";
			   
			   $sql = "UPDATE ".PREFIX."_payment_processors SET
			      	   name = '".$var['name']."',
					   payee = AES_ENCRYPT('".$var['acc_num']."','".ENCKEY."'),
					   var_1 = AES_ENCRYPT('".$var['acc_id']."','".ENCKEY."'),
					   var_2 = AES_ENCRYPT('".$var['acc_passphrase']."','".ENCKEY."'),
					   var_3 = AES_ENCRYPT('".$var['acc_alt_passphrase']."','".ENCKEY."'),
			      	   status = '".$var['status']."',
					   deposit= '".$var['deposit']."',
					   dfee = '".$var['dfee']."',
					   withdraw = '".$var['withdraw']."',
					   autopay = '".$var['autopay']."',
					   wfee = '".$var['wfee']."',
					   currencies = '".$currencies."'
                       WHERE id = '".$var['pid']."' LIMIT  1";
					
			   $db->query_db($sql,$print = DEBUG);
			   $db->free_result($res);
               $return_msg = "Processor Updated";


	       }	   
			  
	  }
      break;
	  
	  
   }

 
  $per_page = $settings['system']['rows_per_page'];
  
  $sql  = "SELECT *,
           AES_DECRYPT(payee,'".ENCKEY."') as payee,
		   AES_DECRYPT(var_1,'".ENCKEY."') as var_1,
		   AES_DECRYPT(var_2,'".ENCKEY."') as var_2,
		   AES_DECRYPT(var_3,'".ENCKEY."') as var_3
           FROM ".PREFIX."_payment_processors ";
  $sql .= "WHERE id = '".$var['pid']."'  ";
  $sql .= "ORDER BY id DESC LIMIT 1";		

  $result = $db->query_db($sql,$print = DEBUG);
  if ($db->num_rows($result) > 0) $processor = $db->fetch_db_array($result);
  
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'edit.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();
?>