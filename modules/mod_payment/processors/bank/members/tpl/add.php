<span class="form_title">Add Account</span>

<?php if($error_msg){ ?><span class="error_message"><?php echo $error_msg;?></span><? } ?>
<?php if($success_msg){ ?><span class="success_message"><?php echo $success_msg;?></span><? } ?>

<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
  <td width="430" height="200" align="center" valign="top">
  
  <div align="left">
  <a href="{#loader=system::url}/members/payment/accounts">All Accounts</a> > 
  <a href="{#loader=system::url}/members/payment/view_accounts/pid/<?php echo $var["pid"]?>">Manage Bank Accounts</a> >
  New Account
  </div><br /><br />
  
  <div class="white">
	 
	<form action="" method="post" class="form_1">
	
	<table cellspacing=1 cellpadding=2 border=0 width="100%" class="table_1">
	  <thead> 
	  <tr>
         <th colspan="2">Account details</th>
      </tr>
	  </thead>
	  <tr>
          <td>Bank Name</td>
          <td><input type="text" name="values[bank_name]" value="<?php echo $var['values']["bank_name"];?>" />
		      <input type="hidden" name="description[bank_name]" value="Bank Name" />
		  </td>
     </tr>
	 <tr>
          <td>Account Number</td>
          <td><input type="text" name="values[account_number]" value="<?php echo $var['values']["account_number"];?>"  />
		      <input type="hidden" name="description[account_number]" value="Account Number" />
		  </td>
     </tr>
	 <tr>
          <td>Routing Number</td>
          <td><input type="text" name="values[routing_number]" value="<?php echo $var['values']["routing_number"];?>" />
		      <input type="hidden" name="description[routing_number]" value="Routing Number" />
		  </td>
     </tr>
	  <tr>
          <td>First Name</td>
          <td><input type="text" name="values[first_name]" value="<?php echo $var['values']["first_name"];?>"  />
		      <input type="hidden" name="description[first_name]" value="First Name" />
		  </td>
     </tr> 
	  <tr>
          <td>Last Name</td>
          <td><input type="text" name="values[last_name]" value="<?php echo $var['values']["last_name"];?>" />
		      <input type="hidden" name="description[last_name]" value="Last Name" />
		  </td>
     </tr>  
    <?php if($settings['account']['use_security_question'] == true && $userinfo['question']!="") { ?>
	 <tr>
         <td><?php echo $userinfo['question'] ?></td>
         <td><input type="text" name="answer" value=""></td>
     </tr>
     <?php } ?>
     <tr>
		 <td >&nbsp;</td>
		 <td ><input name="submit" type="submit" value="Add"></td>
	 </tr>
	</table>
	</form>  
	
	</div></td></tr></table>