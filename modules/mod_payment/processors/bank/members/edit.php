<?php
global $system,$db,$userinfo,$settings;

$payment = $system->importClass('payment');

$var = $system->getVar();

if(!isset($var['pid']) || !isset($var['aid'])) header("location:index.php");

   if($var['submit'])
   {
     
	 if($var['values']["bank_name"] == "") $errors[] = "Please provide bank name";
	 if($var['values']["account_number"] == "") $errors[] = "Please provide a valid account number";
	 if($var['values']["first_name"] == "") $errors[] = "Please provide first name of account holder";
	 if($var['values']["last_name"] == "") $errors[] = "Please provide last name of account holder";
	 if($settings['account']['use_security_question'] == true && $userinfo['question']!="")
	 if($var['answer'] != $userinfo['answer']) {$errors = array(); $errors[] =  "Incorrect answer to security question";}
	 if(is_array($errors) && !empty($errors))while (list($key,$value) = each($errors)) $error_msg.= $value.'<br>';	//$system->logMessage("error",$errors); //
	 else 
	 {
	   $suffix = substr($var['values']["account_number"], -4);
	   $account_id = $var['values']["bank_name"]." - ".$suffix;
	   $edited = $payment->updateAccount($var['aid'],$userinfo['username'],$account_id,$var['description'],$var['values']);
	   if($edited) $success_msg = "Account has been updated";
	}
   
   }



   $sql = "SELECT *, AES_DECRYPT(field_values,'".ENCKEY."') as field_values
	       FROM ".PREFIX."_payment_user_accounts 
		   WHERE username = '".$userinfo["username"]."' ";
   $sql.= "AND pro_id = '".$var['pid']."' ";
   $sql.= "AND id = '".$var['aid']."' ";
   $res 	= $db->query_db($sql,$print = DEBUG);
   if ($db->num_rows($res) == 0)header("location:index.php");
   $erow = $db->fetch_db_array($res);
   $values = unserialize(html_entity_decode($erow['field_values']));
   
   
  
   
   

   $pageInfo['title'] = 'Manage Accounts';
  
   $pageInfo['map'] = array('Manage Accounts' => '',);
   
   $loader = new Loader;
   
   $data= get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'edit.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('private');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('private');
   
   $loader->displayOutput();