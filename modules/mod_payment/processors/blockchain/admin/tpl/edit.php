    <h1>Edit <?php echo $processor['name']?> Details</h1>

    <p class="info">Here you can edit a payment gateway</p>
	
	<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>
	
	<form  action="" method="post">
    <input type="hidden" name="pid" value="<?php echo $var['pid']?>" />
	<table   cellpadding="0" cellspacing="0" class="tableS">
	<thead>
	<tr>
		<th colspan="2" align="left">Processor Details</th>	
	</tr>
	</thead>
	 <?php if(count($processor) == 0) : ?>
	 
      <tr>
		<td colspan="2"> - no records found - </td>
	  </tr>
	  <?php else: ?> 
       <tr>
						<td width="30%">Name</td>
                        <td><input type="text" name="name" value="<?php echo $processor['name'] ?>" /></td>
	   </tr> 
	   <tr>
	     				<td>Bitcoin Address</td>
                        <td><input type="text" name="payee" value="<?php echo $processor['payee'] ?>" /></td>
	   </tr>
	   <tr>
	     				<td>Secret Key</td>
                        <td><input type="password" name="api_key" value="<?php echo $processor['var_1'] ?>" /><img src="images/tooltip.png" alt="Tip" class="tooltip" title="Create a secret password for validating payments made to your Bitcoin address. This can be any random secured password you come up with" /></td>
	   </tr>
	   <thead>
	   <tr>
		<th colspan="2" align="left">API details <img src="images/tooltip.png" alt="Tip" class="tooltip" title="Required for automatic mass payments. Leave this section blank if you do not want to make automated or mass payments" /></th>	
	   </tr>
	   </thead>
	    <tr>
	     				<td colspan="2">
						<p class="info"><b>Setting Up your Wallet API</b></p>				
						<h4>For automated and mass payment of withdrawals, you will need to create a Blockchain wallet on http://blockchain.info and setup a few details.<br />To set this up, just follow these steps:</h4>
						<ol>
						  <li>Create a Blockchain wallet if you haven't done so already.</li>
						  <li>Login to your Blockchain account.</li>
						  <li>Click on 'Account settings' > 'IP restrictions'</li>
						  <li>Modify the following fields:
						    <ol>
							  <li>Enable 'API access'.</li>
						      <li>In the field 'IP Address Whitelist' enter your website IP address</li>
							  <li>Enable 'Restrict To Whitelisted IP Addresses Only' for security reasons</li>
					        </ol>
					      </li>
						  <li>Click on &quot;Save&quot;</li>
						  <li>Once complete you will need to supply your wallet identifier and wallet password (Same ones you use to login to your Blockchan Wallet) in the fields below</li>
						  </ol>	 </td>
                        
	   </tr>
	   <tr>
	     				<td>Blockchain Wallet ID</td>
                        <td><input type="text" name="wallet_id" value="<?php echo $processor['var_2'] ?>" /><img src="images/tooltip.png" alt="Tip" class="tooltip" title="Your Blockchain Wallet identifier. Usually found in your login page" /></td>
	   </tr>
	   <tr>
	     				<td>Blockchain Password</td>
                        <td><input type="password" name="wallet_password" value="<?php echo $processor['var_3'] ?>" /><img src="images/tooltip.png" alt="Tip" class="tooltip" title="Your Blockchain Wallet password" /></td>
	   </tr>
	   <thead>
	   <tr>
		<th colspan="2" align="left">Other Settings</th>	
	    </tr>
	   </thead>
       <tr>
						<td>Currencies</td>
                        <td><?php foreach ($settings['payment']['currencies'] as $id => $currency ) { ?>
	 
	 <?php echo $id; ?> <input name="currencies[]" type="checkbox"  value="<?php echo $id; ?>" <?php if ((stristr($processor['currencies'],",".$id.",")== true)) echo "checked=\"checked\" "; ?>  />
	 
	 
	  <?php } ?></td>
	   </tr>
	    <tr>
		
						<td>Allow Deposit</td>
                        <td><select name="deposit">
                               <option value="Y" <?php if($processor['deposit'] == 'Y')  echo "selected"?>>Yes</option>
                               <option value="N" <?php if($processor['deposit'] == 'N')  echo "selected"?>>No</option>
                           </select> 
                         </td>
	    </tr>
		 <tr>
						<td>Deposit Fee</td>
                        <td><input type="text" name="dfee" value="<?php echo $processor['dfee'] ?>" /></td>
	   </tr> 
		 <tr>
		
						<td>Allow Withdrawal</td>
                        <td><select name="withdraw">
                               <option value="Y" <?php if($processor['withdraw'] == 'Y')  echo "selected"?>>Yes</option>
                               <option value="N" <?php if($processor['withdraw'] == 'N')  echo "selected"?>>No</option>
                           </select> 
                         </td>
	    </tr>
		<!--<tr>
		
						<td>Enable Auto Withdrawal</td>
                        <td><select name="autopay">
                               <option value="Y" <?php if($processor['autopay'] == 'Y')  echo "selected"?>>Yes</option>
                               <option value="N" <?php if($processor['autopay'] == 'N')  echo "selected"?>>No</option>
                           </select> <img src="images/tooltip.png" alt="Tip" class="tooltip" title="If this is enabled, users will be paid automatcally when they make a withdrawal from their wallet. API details must be set for this to work." />
                         </td>
	    </tr>-->
		 <tr>
						<td>Withdrawal Fee</td>
                        <td><input type="text" name="wfee" value="<?php echo $processor['wfee'] ?>" /></td>
	   </tr> 
		<!--<tr>
		
						<td>Allow Subscriptions</td>
                        <td><select name="subscribe">
                               <option value="Y" <?php if($processor['subscribe'] == 'Y')  echo "selected"?>>Yes</option>
                               <option value="N" <?php if($processor['subscribe'] == 'N')  echo "selected"?>>No</option>
                           </select> 
                         </td>
	    </tr>
		 <tr>
						<td>Subscription setup fee</td>
                        <td><input type="text" name="sfee" value="<?php echo $processor['sfee'] ?>" /></td>
	    </tr> -->
	    <tr>
		
						<td>Status</td>
                        <td><select name="status">
                               <option value="Active" <?php if($processor['status'] == 'Active')  echo "selected"?>>Active</option>
                               <option value="Pending" <?php if($processor['status'] == 'Pending')  echo "selected"?>>Pending</option>
                           </select> 
                         </td>
	    </tr>  
        <tr>
            <td colspan="2" align="left"><input type="submit" name="submit" value="Update" class="button_link">	</td>
        </tr>
     <?php endif; ?> 
	</table>
   
</form>

	