<?php
require_once "api.php";
class Blockchain extends Payment{
 
	var $payee;
	var $secretKey;
	var $walletID;
	var $walletPassword;
	var $rootURL = "https://blockchain.info/"; 
	var $paymentURL ="https://blockchain.info/api/receive";
	var $walletURL ="https://blockchain.info/merchant/";
	var $currency = 'USD';
	var $default_button_buy = '';
	var $default_button_subscribe = '';

	//IMPORTANT!!!
	//remeber to set to false in production mode 
    var $testmode = false; 
	var $postdata = array(
	
	               
	);
    var $testdata = array(
	
	               'eid'=>'W544y',	           
		           'payer'=>'payer@test.com',	          
		           'payee'=>'reciever@test.com',	        
		           'amount'=>'100',	      
		           'currency'=>'USD',	           
		           'description'=>'Account Funding',	          
		           'reference'=>'REF-1234-1234-1234',

     );
	
	

     function button( $data, $type )
	 { //required

               ///////////////////////////////
               /* $defined = array (
                //USER DEFINED FOR SIMPLE
				'id' => ,  //required - id of processor
                'amount' => , //required
                'discription' => ,
                'reference' => ,
                'success' => ,
                'fail' => ,
				'auto_submit'=>,				
				
				//optional
				'button' => , //  link to image for submit button
				'custom' => array( custom 1, custom 2.... custom n), //  custom data
				
				 //USER DEFINED FOR SUBSCRIBE
				'sub_time' =>'Year', //Day,Week,Month,Year
                'sub_duration' => 1,
                'sub_period' =>5,

  
                //SYSTEM DEFINED
                'payee' => $this->payee, //required
                'currency' => $this->currency, //required
            );*/
          if(!$this->__checkRequired($data,$type)) return "Parameters are not configured";
		  return $this->makeForm($data,$type); 
     }


     function makeForm($data,$type)
	 {
            //$callback = SITEURL."/ipn/".$data['id'];
			$callback = SITEURL.FS."payment/ipn/id/".$data['id'];
            $data['button'] = ($data['button']) ? $data['button'] : $this->default_button_buy;
			$unit_btc = file_get_contents($this->rootURL."tobtc?currency=USD&value=1");
			$btc_amount = $unit_btc * $data['amount'];
			switch($type){
				case'simple':
				case'advanced':	
				if(strpos($callback,"?") !== false)
					 $callback .= '&eid='.$data['eid'].'&secret='.$this->secretKey.'&ub='.$unit_btc;
				else $callback .= '?eid='.$data['eid'].'&secret='.$this->secretKey.'&ub='.$unit_btc;
				$parameters = 'method=create&address=' . $this->payee .'&callback='. urlencode($callback);
				$response = file_get_contents($this->paymentURL . '?' . $parameters);
				if($response == null || strpos($response,"Error") !== false) return 'Application Error! '.$response;
				$obj = json_decode($response);
				$form   ="<br><div class =\"white\">
						   BITCOIN INFO:<br>
						   Amount: <b>".$btc_amount." BTC</b><br>
						   Bitcoin Address:<br><b>".$obj->input_address."</b>
						  </div>";
				break;
				case'subscribe':	   
				break;
			
			}
			/*$form .="
<script type=\"text/javascript\">
	  fancyjQuery(document).ready(function() {

			fancyjQuery(\"#pro\").fancybox({
				'width'				: '60%',
				'height'			: '100%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
			
	 });
</script>";	*/
			$num = 0;	
            return $form;
		
	 }
	 
	 function __checkRequired($data, $type) 
	 {
	    if(!isset($data['id'])) return false;
	    if(!self::setPayee($data['id'])) return false;
		if(!self::setSecretKey($data['id'])) return false;
	    if(!isset($data['amount']) && !isset($this->currency)) return false;
	     //  if($type == 'subscribe') {}
	    return true;   
	 }
	 
	 function setPayee($id)
	 {
	  
		   global $db;
		   $sql = "SELECT *,
				   AES_DECRYPT(payee,'".ENCKEY."') as payee
				   FROM ".PREFIX."_payment_processors 
				   WHERE id = '".$db->sql_quote($id)."' LIMIT 1 ";
		   $res = $db->query_db($sql,$print = DEBUG);
		   if ($db->num_rows($res) == 0 ) return false;
		   $processor = $db->fetch_db_array($res);
		   if($processor["payee"] == "")return false;
		   $this->payee = $processor["payee"];
		   return true;
     }
	 
	 function setSecretKey($id)
	 {
		   global $db;
		   $sql = "SELECT *,
				   AES_DECRYPT(var_1,'".ENCKEY."') as var_1
				   FROM ".PREFIX."_payment_processors 
				   WHERE id = '".$db->sql_quote($id)."' LIMIT 1 ";
		   $res = $db->query_db($sql,$print = DEBUG);
		   if ($db->num_rows($res) == 0 ) return false;
		   $processor = $db->fetch_db_array($res);
		   if($processor["var_1"] == "")return false;
		   $this->secretKey = trim($processor["var_1"]);
		   return true;
     }
	 
	 function setWalletDetails($id)
	 {
		   global $db;
		   $sql = "SELECT *,
				   AES_DECRYPT(var_2,'".ENCKEY."') as var_2,
				   AES_DECRYPT(var_3,'".ENCKEY."') as var_3
				   FROM ".PREFIX."_payment_processors 
				   WHERE id = '".$db->sql_quote($id)."' LIMIT 1 ";
		   $res = $db->query_db($sql,$print = DEBUG);
		   if ($db->num_rows($res) == 0 ) return false;
		   $processor = $db->fetch_db_array($res);
		   if($processor["var_2"] == "")return false;
		   if($processor["var_3"] == "")return false;
		   $this->walletID = trim($processor["var_2"]);
		   $this->walletPassword = trim($processor["var_3"]);
		   return true;
     }
	 
	 function ipn()
	 {
	 
	     $verified = $this->verify();
	     if(!$verified) return false;
	     return true;
	  
	 }
	  
	 function verify()
	 {
             if ($this->testmode == true) return true;
		     $reason = "";
		     $flagged = false;
	         $id = $_GET["id"];
			 if($_REQUEST['confirmations'] < 3) return false;
			 if($_REQUEST['ub'] == null) {$flagged = true;  $reason .= "Conversion unit missing <br>";}
	         if(!self::setPayee($id)){$flagged = true;  $reason .= "Account ID is not set<br> ";}
	         if(!self::setSecretKey($id)){$flagged = true;  $reason .= "Secret Key is not set<br> ";}
			 if($_REQUEST['test'] == true) {$flagged = true;  $reason .= "Testmode is active<br>";}
			 if($this->secretKey !=  $_REQUEST['secret']) {$flagged = true;  $reason .= "Secret Key does not match<br>";}
			 if($this->payee != $_REQUEST['destination_address']) {$flagged = true;  $reason .= "Merchant does not match <br>";}
			 if(!$flagged) return true;
			 self::failedDeposit($id,$reason);
			 return false;
	
	 }

	 
     function failedDeposit($id,$reason)
	 {	
             $this->recordFailedPayment($id,$reason);	
	 }
	
	 function defineData()
	 {
         if($this->testmode == true) 
         {
		    foreach ($this->testdata as $data => $value) define($data,$value);
            return;
         }
		
		/*
         $_REQUEST['transaction_hash'];
         $_REQUEST['input_transaction_hash'];
         $_REQUEST['input_address'];
         $_REQUEST['value'];
		 $_REQUEST['confirmations'];
        */
		
		 $custom = parent::extractCustomData($_REQUEST['eid']);
		 $unit_btc = $_REQUEST['ub'];
		 $value_in_satoshi = $_REQUEST['value'];
		 $value_in_btc = $value_in_satoshi / 100000000;
		 $value_in_usd = $value_in_btc / $unit_btc;
		 $value_in_usd = round($value_in_usd, 2);
		
         define('eid',$_REQUEST['eid']);
	     define('payer',$custom['username']); 
	     define('payee',$_REQUEST['destination_address']); 
	     define('amount',$value_in_usd); 
	     define('currency','USD'); 
	     define('description',$custom['payment_description']);
	     define('reference',$_REQUEST['transaction_hash']);		   	 
	 
	 }
	   
	 function autoPayment($id,$to,$amount,$currency,$memo)
	 {
	     
		    if(!self::setWalletDetails($id)) array(false,"error processing request");
			$request_url = $this->walletURL.$this->walletID.'/payment';
			$unit_btc = file_get_contents($this->rootURL."tobtc?currency=".$currency."&value=1");
			$btc_amount = $unit_btc * $amount;
			$btc_in_satoshi = $btc_amount * 100000000;
			$parameters = '';
			$parameters .=   '?password='.$this->walletPassword;
			//$parameters .=   '?second_password='.$second_password;
			$parameters .=   '&to='.$to;
			$parameters .=   '&amount='.$btc_in_satoshi;
			//$parameters .=   '&fee='.$fee;
			
			$parameters .=   '&note='.$memo;
			$request = $request_url . $parameters;
			$response = file_get_contents($request);
			if($response == null || strpos($response,"error") !== false) return array(false,"Error processing request:".$response);
			$obj = json_decode($response);
			return array(true,$obj->tx_hash);
			
	 }
	
	 function massPay()
	 {
	   
	 }
	
  	 function cancelSubscription($key)
	 {
	   
	 }
		   
};

?>
