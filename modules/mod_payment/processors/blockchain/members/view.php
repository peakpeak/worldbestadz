<?php
global $system,$db,$userinfo,$settings;

$payment = $system->importClass('payment');

$var = $system->getVar();

if(!isset($var['pid'])) header("location:index.php");

if($var['delete'] == y && isset($var['aid'])) $success_msg = $payment->deleteAccount($userinfo["username"],$var['aid']);

$accounts = $payment->userAccounts($userinfo["username"],$var['pid']);


   $pageInfo['title'] = 'Accounts';
  
   $pageInfo['map'] = array('Accounts' => '',);
   
   $loader = new Loader;
   
   $data= get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'view.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('private');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('private');
   
   $loader->displayOutput();