<span class="form_title">Manage Accounts</span>

<?php if($error_msg){ ?><span class="error_message"><?php echo $error_msg;?></span><? } ?>
<?php if($success_msg){ ?><span class="success_message"><?php echo $success_msg;?></span><? } ?>

<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
  <td width="430" height="200" align="center" valign="top">
  
  <div align="left">
  <a href="{#loader=system::url}/members/payment/accounts">All Accounts</a> > 
  Manage Payeer Accounts
  </div>
  <div align="right"><a href="{#loader=system::url}/members/payment/add_account/pid/<?php echo $var["pid"]?>">New Account</a></div><br />
  
  <div class="white">

		  <table  cellspacing=1 cellpadding=2 border=0 width="100%" class="table_1">
		    <thead>
			<tr>
		     	<th>Account ID</th>
				<th>&nbsp;</th>
			</tr>
			</thead>
			
	 <?php  foreach ($accounts as $id => $value) { ?>
 
     <tr>
		<td><?php echo $value; ?></td>
		<td align="center"><b><a href="{#loader=system::url}/members/payment/edit_account/pid/<?php echo $var["pid"]?>/aid/<?php echo $id;?>">Edit</a> | <a href="{#loader=system::url}/members/payment/view_accounts/delete/y/pid/<?php echo $var["pid"]?>/aid/<?php echo $id;?>" onclick="return(confirm('Do you really want to delete this account'))">Delete</a></b></td>
      </tr>
     
	  <?php } ?>
	  
	   <?php if(count($accounts) == 0) : ?>
	 
      <tr>
		<td colspan="2" align="center"> - no records found -</td>
	 </tr>
	 
     <?php endif; ?> 
	 
	 <?php if($num_rows > $per_page) : ?>
	 
	 <tr>
	    <td colspan="2" align="center"> <?php echo $system->getPaginationString($page,$num_rows,$per_page,3,$system->curPageURL(),"page");?></td>
	 </tr>
	 
	 <?php endif; ?> 
	 
	 
</table>

</div></td></tr></table>