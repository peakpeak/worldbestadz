<?php
class Payeer extends Payment{
 
	
	var $accountNum;
	var $shopID;
	var $shopKey;
	var $apiID;
	var $apiKey;
	var $paymentURL = "https://payeer.com/api/merchant/m.php";
	var $currency = 'USD';
	var $default_button_buy = '';
	var $postdata = array(
	
	
	);

    var $testmode = false;
    var $testdata = array(

               	  /* 'eid'=>'W544y',	           
		           'payer'=>'payer@test.com',	          
		           'payee'=>'reciever@test.com',	        
		           'amount'=>'100',	      
		           'currency'=>'USD',	           
		           'description'=>'Account Funding',	          
		           'reference'=>'REF-1234-1234-1234',*/
				          
		        
                  
               );
		         
	
       function button( $data, $type ){ //required

               ///////////////////////////////
               /* $defined = array (
                //USER DEFINED FOR SIMPLE 
                'amount' => , //required
                'discription' => ,
                'reference' => ,
                'success' => ,
                'fail' => ,
				'auto_submit'=>,				
				
				//optional
				'button' => , //  link to image for submit button
				'custom' => array( custom 1, custom 2.... custom n), //  custom data
				
				 //USER DEFINED FOR SUBSCRIBE
				'sub_time' =>'Year', //Day,Week,Month,Year
                'sub_duration' => 1,
                'sub_period' =>5,

  
                //SYSTEM DEFINED
                'payee' => $this->storeID, //required
                'currency' => $this->currency, //required
				
 
  
            );*/
             
 
          if(!$this->__checkRequired($data,$type)) return "Parameters are not configured";
		  return $this->makeForm($data,$type);
       
     }


     function makeForm($data,$type){
            global $settings;
            $data['button'] = ($data['button']) ? $data['button'] : $this->default_button_buy;
			$m_shop = $this->shopID;
			$m_key = $this->shopKey;
            $m_orderid = $data['eid'];
            $m_amount = number_format($data["amount"], 2, '.', '');
            $m_curr = 'USD';
            $m_desc = base64_encode($data['discription']);
            
            $arHash = array(
	                  $m_shop,
	                  $m_orderid,
	                  $m_amount,
                      $m_curr,
	                  $m_desc,
	                  $m_key
            );
            $m_sign = strtoupper(hash('sha256', implode(":", $arHash)));
			
			switch($type){
			case'simple':
			case'advanced':	
		   
		  
			$form = "<form method='GET' action='".$this->paymentURL."' name='pro'>
                         <input type='hidden' name='m_shop' value='".$this->shopID."'>
                         <input type='hidden' name='m_orderid' value='".$data['eid']."'>
                         <input type='hidden' name='m_amount' value='".$data["amount"]."'>
                         <input type='hidden' name='m_curr' value='USD'>
                         <input type='hidden' name='m_desc' value='".base64_encode($data['discription'])."'>
                         <input type='hidden' name='m_sign' value='".$m_sign."'>
                         <input type='hidden' name='m_process' value='send' />";
      
			break;
			}

		    if(!$data['auto_submit'])  $form.="<input type=\"submit\" name=\"submit\" value=\"Submit\"  /></form>";
            else  $form.="</form><script>document.forms.pro.submit();</script>";
            return $form;
		
	 }
	 
	  function __checkRequired($data, $type) {
	  
	  if(!isset($data['id'])) return false;
	  if(!self::setShopID($data['id'])) return false;
	  if($type == "advanced") if(!self::setShopKey($data['id']))return false;
	  if(!isset($data['amount']) && !isset($this->currency)) return false;
	  return true;
	    
	 }
	 
	  
	 function setAccountNum($id)
	 {
	   global $db;
	   $sql = "SELECT  
	   AES_DECRYPT(payee,'".ENCKEY."') as payee 
	   FROM ".PREFIX."_payment_processors WHERE id = '".$db->sql_quote($id)."' 
	   LIMIT 1 ";
	   $res = $db->query_db($sql,$print = DEBUG);
	   if ($db->num_rows($res) == 0 ) return false;
	   $processor = $db->fetch_db_array($res);
	   if($processor["payee"] == "")return false;
	   $this->accountNum = $processor["payee"];
	   return true;
	 }
	 
	 function setShopID($id)
	 {
	   global $db;
	   $sql = "SELECT  
	   AES_DECRYPT(var_1,'".ENCKEY."') as var_1 
	   FROM ".PREFIX."_payment_processors WHERE id = '".$db->sql_quote($id)."' 
	   LIMIT 1 ";
	   $res = $db->query_db($sql,$print = DEBUG);
	   if ($db->num_rows($res) == 0 ) return false;
	   $processor = $db->fetch_db_array($res);
	   if($processor["var_1"] == "")return false;
	   $this->shopID = $processor["var_1"];
	   return true;
	 }
	 
	 function setShopKey($id)
	 {
	   global $db;
	   $sql = "SELECT  
	   AES_DECRYPT(var_2,'".ENCKEY."') as var_2 
	   FROM ".PREFIX."_payment_processors WHERE id = '".$db->sql_quote($id)."' 
	   LIMIT 1 ";
	   $res = $db->query_db($sql,$print = DEBUG);
	   if ($db->num_rows($res) == 0 ) return false;
	   $processor = $db->fetch_db_array($res);
	   if($processor["var_2"] == "")return false;
	   $this->shopKey = $processor["var_2"];
	   return true;
	 }
	
	 
	 function setApiID($id)
	 {
	   global $db;
	   $sql = "SELECT  
	   AES_DECRYPT(var_3,'".ENCKEY."') as var_3 
	   FROM ".PREFIX."_payment_processors WHERE id = '".$db->sql_quote($id)."' 
	   LIMIT 1 ";
	   $res = $db->query_db($sql,$print = DEBUG);
	   if ($db->num_rows($res) == 0 ) return false;
	   $processor = $db->fetch_db_array($res);
	   if($processor["var_3"] == "")return false;
	   $this->apiID = $processor["var_3"];
	   return true;
	 }
	 
	 function setApiKey($id)
	 {
	   global $db;
	   $sql = "SELECT  
	   AES_DECRYPT(var_4,'".ENCKEY."') as var_4 
	   FROM ".PREFIX."_payment_processors WHERE id = '".$db->sql_quote($id)."' 
	   LIMIT 1 ";
	   $res = $db->query_db($sql,$print = DEBUG);
	   if ($db->num_rows($res) == 0 ) return false;
	   $processor = $db->fetch_db_array($res);
	   if($processor["var_4"] == "")return false;
	   $this->apiKey = $processor["var_4"];
	   return true;
	 }
	 
	 
	 
	 /////////////////////////
	 function ipn(){
	 
	     $verified = $this->verify();
	     if(!$verified) return false;
	     return true;
	  
	 }
	  
	 function verify(){
     
	     if ($this->testmode == true) return true;
		 $reason = "";
		 $flagged = false;
	     $id = $_GET["id"];
	    // if(!self::setShopID($id)){$flagged = true;  $reason .= "Shop ID is not set, ";}
		 if(!self::setShopKey($id)){$flagged = true;  $reason .= "Shop Key is not set, ";}
		 if(!isset($_POST["m_operation_id"])){$flagged = true;  $reason .= "Operation ID is not set, ";}
		 if(!isset($_POST["m_sign"])){$flagged = true;  $reason .= "Signature is not set, ";}
		 
	     $arHash = array($_POST['m_operation_id'],
			             $_POST['m_operation_ps'],
			             $_POST['m_operation_date'],
			             $_POST['m_operation_pay_date'],
			             $_POST['m_shop'],
			             $_POST['m_orderid'],
			             $_POST['m_amount'],
			             $_POST['m_curr'],
			             $_POST['m_desc'],
			             $_POST['m_status'],
			             $this->shopKey);
						 
	    $sign_hash = strtoupper(hash('sha256', implode(":", $arHash)));
		if($_POST["m_sign"] != $sign_hash ){$flagged = true;  $reason .= "Payment hash does not match, ";}
		if($_POST['m_status'] != "success" ){$flagged = true;  $reason .= "Payment status is not successfull, ";}
        if(strtoupper($_POST['m_curr']) !=	"USD"){$flagged = true;  $reason .= "Currency does not match,";}
	    if(!$flagged) return true;
	    self::failedDeposit($id,$reason);
	    return false;
	
	}
	
	function failedDeposit($id,$reason)
	{
	  $this->recordFailedPayment($id,$reason);	
	}
	
	 
	function defineData(){
	 
       if($this->testmode == true) 
         {
		    foreach ($this->testdata as $data => $value) define($data,$value);
            return;
         } 
	
                   define('eid',$_POST['m_orderid']);
		           define('payer',$_POST['m_operation_id']);  //change
				   define('payee',$_POST['m_shop']); 
                   define('amount',$_POST['m_amount']); 
				   define('currency',$_POST['m_curr']); 
                   define('description',$_POST['m_desc']);
				   define('reference',$_POST['m_operation_id']);
	}
	 
	
	function massPay()
	{
	  
	}
	
	function autoPayment($id,$to,$amount,$currency,$memo = null)
	{
	  
	  $return_array = array();
	  self::setAccountNum($id);
	  self::setApiID($id);
	  self::setApiKey($id);
	  $testmode = ($this->testmode)?1:0;
	  include_once("api.php");  
	 
     $payeer = new CPayeer($this->accountNum,$this->apiID, $this->apiKey);
     if (!$payeer->isAuth()) return array(false,'Authentication Error'); 
	 $arTransfer = $payeer->transfer(array(
		'curIn' => 'USD', // write-off account 
		'sum' => $amount, // Amount to receive 
		'curOut' => 'USD', // receiving currency  
		'to' => $to, // Recipient (email)
		//'to' => '+01112223344',  // Recipient (Phone)
		//'to' => 'P1000000',  // Recipient (Account number)
		'comment' => $memo,
		//'anonim' => 'Y', // anonymous transfer 
		//'protect' => 'Y', // protect transaction 
		//'protectPeriod' => '3', // protection period (from 1 to 30 days) 
		//'protectCode' => '12345', // protection code 
	  ));
	  if(empty($arTransfer["historyId"]))
	  {
	     $errors = $arTransfer["errors"];
		 return array(false,$errors); 
	  }
	  return array(true,$arTransfer["historyId"]);
	}
	
	
	function cancelSubscription($key,$memo = null){
	  
	  $return_array = array();
	  $testmode = ($this->testmode)?1:0;
	}
	
	
		   
};



?>
