<?php
class SolidTrustPay extends Payment{
 
	
	var $accountID;
	var $sciName;
	var $secPass;
	var $apiName;
	var $apiPass;
	var $paymentURL = "https://solidtrustpay.com/handle.php";
	var $sendMoneyUrl = "https://solidtrustpay.com/accapi/process.php";
	var $currency = 'USD';
	var $default_button_buy = '';
	var $postdata = array(
	
	
	);

    var $testmode = false;
    var $testdata = array(

                   'eid'=>'W544y',	           
		           'payer'=>'payer@test.com',	          
		           'payee'=>'reciever@test.com',	        
		           'amount'=>'100',	      
		           'currency'=>'USD',	           
		           'description'=>'Account Funding',	          
		           'reference'=>'REF-1234-1234-1234',
				
                  
               );
		         
	
       function button( $data, $type ){ //required

               ///////////////////////////////
               /* $defined = array (
                //USER DEFINED FOR SIMPLE 
                'amount' => , //required
                'discription' => ,
                'reference' => ,
                'success' => ,
                'fail' => ,
				'auto_submit'=>,				
				
				//optional
				'button' => , //  link to image for submit button
				'custom' => array( custom 1, custom 2.... custom n), //  custom data
				
				 //USER DEFINED FOR SUBSCRIBE
				'sub_time' =>'Year', //Day,Week,Month,Year
                'sub_duration' => 1,
                'sub_period' =>5,

  
                //SYSTEM DEFINED
                'payee' => $this->storeID, //required
                'currency' => $this->currency, //required
				
 
  
            );*/
             
 
          if(!$this->__checkRequired($data,$type)) return "Parameters are not configured";
		  return $this->makeForm($data,$type);
       
     }


     function makeForm($data,$type){
 
            $data['button'] = ($data['button']) ? $data['button'] : $this->default_button_buy;
			
			switch($type){
			case'simple':
			case'advanced':	
		
					   
			$form =	  "<form action='".$this->paymentURL."' method='post' name='pro'>
                      <input type=hidden name='merchantAccount' value='".$this->accountID."' />
                      <input type=hidden name='amount' value='".$data["amount"]."' />
					  <input type=hidden name='currency' value='USD' />
                      <input type=hidden name='item_id' value='".$data['eid']."' />
					  <input type=hidden name='sci_name' value='".$this->sciName."' />";
            /*$form .=  "<input type=hidden name='notify_url' value='".SITEURL."/payment/ipn/id/".$data['id']."' />
                      <input type=hidden name='return_url'  value='".$data['success']."' />
                      <input type=hidden name='cancel_url' value='".$data['fail']."' />";*/
            $form .=  "<input type=hidden name='memo' value='".$data['discription']."' />
                      <input type=hidden name='testmode' value='OFF' />";

			break;
			}

		    if(!$data['auto_submit'])  $form.="<input type=\"submit\" name=\"cartImage\" value=\"Submit\"  /></form>";
            else  $form.="</form><script>document.forms.pro.submit();</script>";
            return $form;
		
	 }
	 
	  function __checkRequired($data, $type) {
	  
	  if(!isset($data['id'])) return false;
	  if(!self::setAccountID($data['id'])) return false;
	  if(!self::setSciName($data['id'])) return false;
	  if($type == "advanced") if(!self::setSecPass($data['id']))return false;
	  if(!isset($data['amount']) && !isset($this->currency)) return false;
	  return true;
	    
	 }
	 
	 function setAccountID($id)
	 {
	  
	   global $db;
	   $sql = "SELECT  
	   AES_DECRYPT(payee,'".ENCKEY."') as payee 
	   FROM ".PREFIX."_payment_processors WHERE id = '".$db->sql_quote($id)."' 
	   LIMIT 1 ";
	   $res = $db->query_db($sql,$print = DEBUG);
	   if ($db->num_rows($res) == 0 ) return false;
	   $processor = $db->fetch_db_array($res);
	   if($processor["payee"] == "")return false;
	   $this->accountID = $processor["payee"];
	   return true;
	   
     }
	 
	 function setSciName($id)
	 {
	  
	   global $db;
	   $sql = "SELECT  
	   AES_DECRYPT(var_4,'".ENCKEY."') as var_4 
	   FROM ".PREFIX."_payment_processors WHERE id = '".$db->sql_quote($id)."' 
	   LIMIT 1 ";
	   $res = $db->query_db($sql,$print = DEBUG);
	   if ($db->num_rows($res) == 0 ) return false;
	   $processor = $db->fetch_db_array($res);
	   if($processor["var_4"] == "")return false;
	   $this->sciName = $processor["var_4"];
	   return true;
	   
     }
	 
	 function setSecPass($id)
	 {
	  
	   global $db;
	   $sql = "SELECT  
	   AES_DECRYPT(var_1,'".ENCKEY."') as var_1 
	   FROM ".PREFIX."_payment_processors WHERE id = '".$db->sql_quote($id)."' 
	   LIMIT 1 ";
	   $res = $db->query_db($sql,$print = DEBUG);
	   if ($db->num_rows($res) == 0 ) return false;
	   $processor = $db->fetch_db_array($res);
	   if($processor["var_1"] == "")return false;
	   $this->secPass = $processor["var_1"];
	   return true;
	   
     }
	 
	
	 
	 function setApiName($id)
	 {
	   global $db;
	   $sql = "SELECT *,
			   AES_DECRYPT(var_2,'".ENCKEY."') as var_2
	           FROM ".PREFIX."_payment_processors 
	           WHERE id = '".$db->sql_quote($id)."' LIMIT 1 ";
	   $res = $db->query_db($sql,$print = DEBUG);
	   if ($db->num_rows($res) == 0 ) return false;
	   $processor = $db->fetch_db_array($res);
	   if($processor["var_2"] == "")return false;
	   $this->apiName = $processor["var_2"];
	   return true;
     }
	
	 function setApiPass($id)
	 {
	   global $db;
	   $sql = "SELECT *,
			   AES_DECRYPT(var_3,'".ENCKEY."') as var_3
	           FROM ".PREFIX."_payment_processors 
	           WHERE id = '".$db->sql_quote($id)."' LIMIT 1 ";
	   $res = $db->query_db($sql,$print = DEBUG);
	   if ($db->num_rows($res) == 0 ) return false;
	   $processor = $db->fetch_db_array($res);
	   if($processor["var_3"] == "")return false;
	   $this->apiPass = $processor["var_3"];
	   return true;
     }
	 /////////////////////////
	 function ipn(){
	 
	     $verified = $this->verify();
	     if(!$verified) return false;
	     return true;
	  
	 }
	  
	 function verify(){
     
	     if ($this->testmode == true) return true;
		 $reason = "";
		 $flagged = false;
	     $id = $_GET["id"];
	     if(!self::setAccountID($id)){$flagged = true;  $reason .= "Account ID is not set<br> ";}
	     if(!self::setSecPass($id)){$flagged = true;  $reason .= "Secondary Pass is not set<br> ";}
		
		 $md5 = md5($this->secPass."s+E_a*");
         $c_hash = MD5($_POST['tr_id'].":".MD5($md5).":".$_POST['amount'].":".$_POST['merchantAccount'].":".$_POST['payerAccount']);

		 if($_POST['testmode'] == 'ON'){$flagged = true;  $reason .= "System is in testomde<br> ";}
		 if($_POST['hash'] != $c_hash){$flagged = true;  $reason .= "Payment hash does not match<br> ";}
		 if(strtoupper($_POST['status']) != 'COMPLETE'){$flagged = true;  $reason .= "Payment Status is not complete<br> ";}

	     if(!$flagged) return true;
	     self::failedDeposit($id,$reason);
	     return false;
	
	}
	
	function failedDeposit($id,$reason)
	{ 
	  $this->recordFailedPayment($id,$reason);	
	}
	
	 
	 function defineData(){
	 
        if($this->testmode == true) 
        {
		    foreach ($this->testdata as $data => $value) define($data,$value);
            return;
        } 		   
		define('eid',$_REQUEST['item_id']);
		define('payer',$_REQUEST['payerAccount']); 
		define('payee',$_REQUEST['merchantAccount']); 
		define('amount',$_REQUEST['amount']); 
		define('currency','USD'); 
		define('description',$_REQUEST['memo']);
		define('reference',$_REQUEST['tr_id']);
	}
	 
	
	function massPay()
	{
	  
	}
	
	function autoPayment($id,$to,$amount,$currency,$memo = null)
	{
	  
	  $return_array = array();
	  self::setApiName($id);
	  self::setApiPass($id);
	  $testmode = ($this->testmode)?1:0;
	  
      $post = sprintf("user=%s&api_id=%s&api_pwd=%s&amount=%s&fee=%s&paycurrency=%s&comments=%s&testmode=%s",
              urlencode($to),
			  urlencode($this->apiName),
			  urlencode(md5($this->apiPass.'s+E_a*')), 
			  urlencode((string)$amount),
			  urlencode((string)$fee), 
			  urlencode($currency),
			  urlencode((string)$memo),
			  urlencode((string)$testmode)
	   );


	  
	  $response = self::sendRequest($this->sendMoneyUrl,$post);
	  $return_array = explode(":",$response);
	  if(count($return_array) == 0)return array(false,"error processing request");
	  elseif($return_array[0] == 'DECLINED')return array(false,$return_array[1]);
	  else return array(true,"");
	
	}
	
	
	function cancelSubscription($key,$memo = null){
	  
	  $return_array = array();
	  $testmode = ($this->testmode)?1:0;
	  
	 

	}
	
	function sendRequest($url,$data)
	{
	   
	   $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        curl_close($ch);
		return $response;
	}
		   
};



?>
