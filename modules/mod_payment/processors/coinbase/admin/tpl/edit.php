    <h1>Edit <?php echo $processor['name']?> Details</h1>

    <p class="info">Here you can edit a payment gateway</p>
	
	<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>
	
	<form  action="" method="post">
    <input type="hidden" name="pid" value="<?php echo $var['pid']?>" />
	<table   cellpadding="0" cellspacing="0" class="tableS">
	<thead>
	<tr>
		<th colspan="2" align="left">Processor Details</th>	
	</tr>
	</thead>
	 <?php if(count($processor) == 0) : ?>
	 
      <tr>
		<td colspan="2"> - no records found - </td>
	  </tr>
	  <?php else: ?> 
       <tr>
						<td width="30%">Name</td>
                        <td><input type="text" name="name" value="<?php echo $processor['name'] ?>" /></td>
	   </tr> 
	   <tr>
	     				<td colspan="2">
						<p class="info"><b>Setting Up your  API </b></p>				
						<h4>For instant payment crediting to account, you have to set up your API info on http://www.coinbase.com. <br />To set this up, just follow these steps:</h4>
						<ol>
						  <li>Login to your Coinbase account.</li>
						  <li>Click on Settings > API Access</li>
						  <li>Click on New API Key</li>
						  <li>Enter the information:
						    <ol>
							  <li>Select Wallet under 'Accounts' section</li>
							  <li>Under 'Permissions' Click on 'show' right next to 'API v1 permissions (legacy)' and select <b>all</b> permissions</li>
							  <li>Leave Notification URL blank </li>
							  <li>Enter <?php echo $_SERVER['SERVER_ADDR']; ?> in 'Allowed IP Addresses'
</li>
					        </ol>
					      </li>
						  <li>Click on 'Create' button once completed</li>
						  <li>Once completed, your API keys will be automatically generated. Enter them below</li>
						  <li>Note: Make sure you complete your merchant profile <a href="https://www.coinbase.com/merchant_profiles" target="_blank">here</a> to avoid error messages</li>
						  
						  </ol>	 </td>
                        
	   </tr>
	   <tr>
	     				<td>API Key</td>
                        <td><input type="text" name="api_key" value="<?php echo $processor['api_key'] ?>" /><img src="images/tooltip.png" alt="Tip" class="tooltip" title="Get this from login into your coinbase account -> Settings -> API access -> New API key" /></td>
	   </tr>
	    <tr>
	     				<td>Secret key</td>
                        <td><input type="password" name="secret_key" value="<?php echo $processor['secret_key'] ?>" /><img src="images/tooltip.png" alt="Tip" class="tooltip" title="Get this from login into your coinbase account -> Settings -> API access -> New API key" /></td>
	   </tr>
       <tr>
						<td>Currencies</td>
                        <td><?php foreach ($settings['payment']['currencies'] as $id => $currency ) { ?>
	 
	 <?php echo $id; ?> <input name="currencies[]" type="checkbox"  value="<?php echo $id; ?>" <?php if ((stristr($processor['currencies'],",".$id.",")== true)) echo "checked=\"checked\" "; ?>  />
	 
	 
	  <?php } ?></td>
	   </tr>
	    <tr>
		
						<td>Allow Deposit</td>
                        <td><select name="deposit">
                               <option value="Y" <?php if($processor['deposit'] == 'Y')  echo "selected"?>>Yes</option>
                               <option value="N" <?php if($processor['deposit'] == 'N')  echo "selected"?>>No</option>
                           </select> 
                         </td>
	    </tr>
		 <tr>
						<td>Deposit Fee</td>
                        <td><input type="text" name="dfee" value="<?php echo $processor['dfee'] ?>" /></td>
	   </tr> 
		 <tr>
		
						<td>Allow Withdrawal</td>
                        <td><select name="withdraw">
                               <option value="Y" <?php if($processor['withdraw'] == 'Y')  echo "selected"?>>Yes</option>
                               <option value="N" <?php if($processor['withdraw'] == 'N')  echo "selected"?>>No</option>
                           </select> 
                         </td>
	    </tr>
		<tr>
		
						<td>Enable Auto Withdrawal</td>
                        <td><select name="autopay">
                               <option value="Y" <?php if($processor['autopay'] == 'Y')  echo "selected"?>>Yes</option>
                               <option value="N" <?php if($processor['autopay'] == 'N')  echo "selected"?>>No</option>
                           </select> <img src="images/tooltip.png" alt="Tip" class="tooltip" title="If this is enabled, users will be paid automatcally when they make a withdrawal from their wallet. API details must be set for this to work." />
                         </td>
	    </tr>
		 <tr>
						<td>Withdrawal Fee</td>
                        <td><input type="text" name="wfee" value="<?php echo $processor['wfee'] ?>" /></td>
	   </tr> 
		<!--<tr>
		
						<td>Allow Subscriptions</td>
                        <td><select name="subscribe">
                               <option value="Y" <?php if($processor['subscribe'] == 'Y')  echo "selected"?>>Yes</option>
                               <option value="N" <?php if($processor['subscribe'] == 'N')  echo "selected"?>>No</option>
                           </select> 
                         </td>
	    </tr>
		 <tr>
						<td>Subscription setup fee</td>
                        <td><input type="text" name="sfee" value="<?php echo $processor['sfee'] ?>" /></td>
	    </tr> -->
	    <tr>
		
						<td>Status</td>
                        <td><select name="status">
                               <option value="Active" <?php if($processor['status'] == 'Active')  echo "selected"?>>Active</option>
                               <option value="Pending" <?php if($processor['status'] == 'Pending')  echo "selected"?>>Pending</option>
                           </select> 
                         </td>
	    </tr>  
        <tr>
            <td colspan="2" align="left"><input type="submit" name="submit" value="Update" class="button_link">	</td>
        </tr>
     <?php endif; ?> 
	</table>
   
</form>

	