<?php

require_once('lib/Coinbase.php');

class CoinbasePayments extends Payment
{

    var $apiKey;
    var $apiSecret;
    var $coinbaseCheckoutUrl = 'https://coinbase.com/checkouts/';

    function setApiKeys($id)
    {

        global $db;
        $sql = "SELECT *,
		AES_DECRYPT(var_1,'" . ENCKEY . "') as api_key,
		AES_DECRYPT(var_2,'" . ENCKEY . "') as api_secret
		FROM " . PREFIX . "_payment_processors 
		WHERE id = '" . $db->sql_quote($id) . "' LIMIT 1 ";
        $res = $db->query_db($sql, $print = DEBUG);
        if ($db->num_rows($res) == 0) return false;
        $processor = $db->fetch_db_array($res);
        if ($processor["api_key"] == "") return false;
        if ($processor["api_secret"] == "") return false;
        $this->apiKey = trim($processor["api_key"]);
        $this->apiSecret = trim($processor["api_secret"]);
        return true;

    }

	function __checkRequired($data, $type) 
	{
		if(!isset($data['id'])) return false;
	    if(!self::setApiKeys($data['id'])) return false;
	    if(!isset($data['amount'])) return false;
	    return true;   
	}

    function button($data, $type) 
    {
    	if(!$this->__checkRequired($data,$type)) return "Parameters are not configured";
		return $this->makeForm($data,$type);
    }

    function makeForm($data, $type)
    {
       
        
        switch ($type) {
            case 'simple':
            case 'advanced':
				 try {
							$coinbase = Coinbase::withApiKey($this->apiKey, $this->apiSecret);
							$coinbaseTransactionId = $coinbase->createButton(
							$data['discription'], // Transaction description, ex. "Account funding by john123"
							$data['amount'],      // Amount in USD
							'USD',                // Currency
							$data['eid'],         // "custom" parameter to identify payment in ipn
							array(                // Array with callback url, success url and cancel url. You can supply your own values, reffering to
												  // official documentation: 
												  // https://developers.coinbase.com/api#create-a-new-payment-button-page-or-iframe
								'callback_url' => SITEURL . "/payment/ipn/id/" . $data['id'] . "?secret=" . md5($this->apiKey . $this->apiSecret . $data['eid']) ,
								'success_url'  => $data['success'],
								'cancel_url'   => $data['fail'],
							))->button->code;       // generate button object and get transaction ID
				 
				  } catch (Exception $e) {
				 
					return $e->getMessage();
				  }

                  $redirectUrl = $this->coinbaseCheckoutUrl . $coinbaseTransactionId;
                  $form = '<form action="' . $redirectUrl . '" method="get">
	              <input type="submit" value="Submit">
	              </form>';
            break;
			
            case 'subscribe':
                break;
        }

        return $form;

    }

    function ipn()
    {

        $verified = $this->verify();
        if (!$verified) return false;
        return true;

    }

    function verify()
    {

        $reason = '';
        $flagged = false;
        $id = $_GET["id"];
        $json = json_decode(file_get_contents('php://input'));
        $order = $json->order;

        if (!self::setApiKeys($id)) {
            $flagged = true;
            $reason .= "API Key and API Secret pair is incorrect. <br>";
        }
        if (md5($this->apiKey . $this->apiSecret . $order->custom) !== $_GET['secret']) {
            $flagged = true;
            $reason .= "Given sign is incorrect. <br>";
        }
        if (strtoupper($order->total_native->currency_iso) != "USD") {
            $flagged = true;
            $reason .= "Currency does not match. <br> ";
        }
        if ($order->status !== 'completed') {
            $flagged = true;
            $reason .= "Transaction is mispaid or expired. ";
        }

        if (!$flagged) return true;
        self::failedDeposit($id, $reason);
        return false;

    }

    function failedDeposit($id, $reason)
    {
        $this->recordFailedPayment($id, $reason);
    }

    function defineData()
    {

        $custom = parent::extractCustomData($_REQUEST['eid']);
        $id = $_GET["id"];
        $json = json_decode(file_get_contents('php://input'));
        $order = $json->order;


        define('eid', $order->custom);
        define('payer', $custom['username']);
        define('payee', $order->receive_address);
        define('amount', number_format($order->total_native->cents / 100, 2, '.', ''));
        define('currency', 'USD');
        define('description', $custom['payment_description']);
        define('reference', $order->transaction->hash);

    }

	function autoPayment($id, $to, $amount, $currency, $memo = null)
	{
        self::setApiKeys($id);
        
        try {
		
		    $coinbase = Coinbase::withApiKey($this->apiKey, $this->apiSecret);
            $response = $coinbase->sendMoney($to, $amount, $memo, null, $currency);
            return array($response->success, $response->transaction->hash);
			
        } catch(Exception $e) {
            return array(false, $e->getMessage());	
        }

	}
}