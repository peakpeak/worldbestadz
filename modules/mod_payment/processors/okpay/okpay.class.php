<?php
require_once "api.php";
class Okpay extends Payment{
 
	var $payee;
	var $APIKey;
	var $paymentUrl ="https://www.okpay.com/process.html";
	var $currency = 'USD';
	var $default_button_buy = '';
	var $default_button_subscribe = '';

	//IMPORTANT!!!
	//remeber to set to false in production mode 
    var $testmode = false; 
	var $postdata = array(
	
	               
	);
    var $testdata = array(
	
	               'eid'=>'W544y',	           
		           'payer'=>'payer@test.com',	          
		           'payee'=>'reciever@test.com',	        
		           'amount'=>'100',	      
		           'currency'=>'USD',	           
		           'description'=>'Account Funding',	          
		           'reference'=>'REF-1234-1234-1234',

     );
	
	

     function button( $data, $type )
	 { //required

               ///////////////////////////////
               /* $defined = array (
                //USER DEFINED FOR SIMPLE
				'id' => ,  //required - id of processor
                'amount' => , //required
                'discription' => ,
                'reference' => ,
                'success' => ,
                'fail' => ,
				'auto_submit'=>,				
				
				//optional
				'button' => , //  link to image for submit button
				'custom' => array( custom 1, custom 2.... custom n), //  custom data
				
				 //USER DEFINED FOR SUBSCRIBE
				'sub_time' =>'Year', //Day,Week,Month,Year
                'sub_duration' => 1,
                'sub_period' =>5,

  
                //SYSTEM DEFINED
                'payee' => $this->payee, //required
                'currency' => $this->currency, //required
            );*/
          if(!$this->__checkRequired($data,$type)) return "Parameters are not configured";
		  return $this->makeForm($data,$type); 
     }


     function makeForm($data,$type)
	 {
 
            $data['button'] = ($data['button']) ? $data['button'] : $this->default_button_buy;
			
			switch($type){
			case'simple':
			case'advanced':	
			$form ="
			      <form  method='post' action='".$this->paymentUrl."' name='pro'>
                  <input type='hidden' name='ok_receiver' value='".$this->payee."'/>
                  <input type='hidden' name='ok_item_1_name' value='".$data['discription']."'/>
                  <input type='hidden' name='ok_item_1_price' value='".$data['amount']."'/>
                  <input type='hidden' name='ok_currency' value='".$this->currency."'/>";	   
			break;
			case'subscribe':	   
			break;
			
			}
			
			$num = 0;	
			$form.="<input type='hidden' name='ok_item_1_custom_1_title' value='custom_1'/>";
            $form.="<input type='hidden' name='ok_item_1_custom_1_value' value='".$data['eid']."'/>";
   	
		    if(!$data['auto_submit'])  $form.="<input type=\"submit\" name=\"submit\" value=\"Submit\"  /></form>";
            else  $form.="</form><script>document.forms.pro.submit();</script>";
            return $form;
		
	 }
	 
	 function __checkRequired($data, $type) 
	 {
	    if(!isset($data['id'])) return false;
	    if(!self::setPayee($data['id'])) return false;
	    if(!isset($data['amount']) && !isset($this->currency)) return false;
	     //  if($type == 'subscribe') {}
	    return true;   
	 }
	 
	 function setPayee($id)
	 {
	  
		   global $db;
		   $sql = "SELECT *,
				   AES_DECRYPT(payee,'".ENCKEY."') as payee
				   FROM ".PREFIX."_payment_processors 
				   WHERE id = '".$db->sql_quote($id)."' LIMIT 1 ";
		   $res = $db->query_db($sql,$print = DEBUG);
		   if ($db->num_rows($res) == 0 ) return false;
		   $processor = $db->fetch_db_array($res);
		   if($processor["payee"] == "")return false;
		   $this->payee = $processor["payee"];
		   return true;
     }
	 
	 function setAPIKey($id)
	 {
		   global $db;
		   $sql = "SELECT *,
				   AES_DECRYPT(var_1,'".ENCKEY."') as var_1
				   FROM ".PREFIX."_payment_processors 
				   WHERE id = '".$db->sql_quote($id)."' LIMIT 1 ";
		   $res = $db->query_db($sql,$print = DEBUG);
		   if ($db->num_rows($res) == 0 ) return false;
		   $processor = $db->fetch_db_array($res);
		   if($processor["var_1"] == "")return false;
		   $this->APIKey = $processor["var_1"];
		   return true;
     }
	 
	 function ipn(){
	 
	     $verified = $this->verify();
	     if(!$verified) return false;
	     return true;
	  
	 }
	  
	 function verify()
	 {
     
			 if ($this->testmode == true) return true;
			 if(!$this->verifyPost()) return false;
			 $id = $_GET["id"];
			 self::setPayee($id);
			 $flagged = false;
			 $reason = "";
			 if(strtoupper($_REQUEST['ok_txn_currency']) != $this->currency) {$flagged = true;  $reason .= "Currency does not match, ";}
			 if($_REQUEST["ok_receiver"] != $this->payee) {$flagged = true;  $reason .= "Merchant does not match, ";}
			 if(!$flagged) return true;
			 self::failedDeposit($id,$reason);
			 return false;
	
	 }

	 function verifyPost()
	 {
			   
		   $request = 'ok_verify=true';
		   foreach ($_POST as $key => $value) {
			   $value = urlencode(stripslashes($value));
			   $request .= "&$key=$value";
		   }
		  
		   $fsocket = false;
		   $result = false;
		  
		   // Try to connect via SSL due sucurity reason
		   if ( $fp = @fsockopen('ssl://www.okpay.com', 443, $errno, $errstr, 30) ) {
			   // Connected via HTTPS
			   $fsocket = true;
		   } elseif ($fp = @fsockopen('www.okpay.com', 80, $errno, $errstr, 30)) {
			   // Connected via HTTP
			   $fsocket = true;
		   }
		  
		   // If connected to OKPAY
		   if ($fsocket == true) {
			   $header = 'POST /ipn-verify.html HTTP/1.0' . "\r\n" .
						 'Host: www.okpay.com'."\r\n" .
						 'Content-Type: application/x-www-form-urlencoded' . "\r\n" .
						 'Content-Length: ' . strlen($request) . "\r\n" .
						 'Connection: close' . "\r\n\r\n";
		  
			   @fputs($fp, $header . $request);
			   $string = '';
			   while (!@feof($fp)) {
				   $res = @fgets($fp, 1024);
				   $string .= $res;
				   // Find verification result in response
				   if ( $res == 'VERIFIED' || $res == 'INVALID' || $res == 'TEST') {
					   $result = $res;
					   break;
				   }
			   }
			   @fclose($fp);
		   }
		  
		   if ($result == 'VERIFIED') return true;
			   // check the "ok_txn_status" is "completed"
			   // check that "ok_txn_id" has not been previously processed
			   // check that "ok_receiver_email" is your OKPAY email
			   // check that "ok_txn_gross"/"ok_txn_currency" are correct
			   // process payment
		   elseif($result == 'INVALID') return false;
			   // If 'INVALID': log for manual investigation.
		   elseif($result == 'TEST');
			   // If 'TEST': do something
		   else  return false;
			   // IPN not verified or connection errors
			   // If status != 200 IPN will be repeated later
			
	  }
	
      function failedDeposit($id,$reason)
	  {	
        $this->recordFailedPayment($id,$reason);	
	  }
	
	  function defineData()
	  {
         if($this->testmode == true) 
         {
		    foreach ($this->testdata as $data => $value) define($data,$value);
            return;
         }
         define('eid',$_REQUEST['ok_item_1_custom_1_value']);
	     define('payer',$_REQUEST['ok_payer_email']); 
	     define('payee',$_REQUEST['ok_receiver']); 
	     define('amount',$_REQUEST['ok_txn_gross']); 
	     define('currency',$_REQUEST['ok_txn_currency']); 
	     define('description',$_REQUEST['ok_item_1_name']);
	     define('reference',$_REQUEST['ok_txn_id']);		   	 
	 
	  }
	   
	  function autoPayment($id,$to,$amount,$currency,$memo)
	  {
		 global $settings;
	     $return_array = array();
		 self::setPayee($id);
		 self::setAPIKey($id);
		 $testmode = ($this->testmode)?1:0;
	     $memo = ($memo == null) ? 'Payment from '.$settings['system']['websitename']:$memo;
		 try{
		
		       $ws = new OkpayApi($this->payee,$this->APIKey);
	           $response = $ws->sendMoney($to,$amount,$currency,$memo);
		  
         } catch (Exception $e) {
		       
			   return array(false,$e->getMessage());
         }
		 
		 //print_r($response);
	  
	     $return_array  =  (array)$response;
	     if(count($return_array) == 0)return array(false,"error processing request");
	     if($return_array['Status'] == 'Completed')return array(true,$return_array['ID']);
	     return array(false,'error processing request');
	  }
	
	  function massPay()
	  {
	   
	  }
	
  	  function cancelSubscription($key)
	  {
	   
	  }
		   
};

?>
