    <h1>Edit <?php echo $processor['name']?> Details</h1>

    <p class="info">Here you can edit a payment gateway</p>
	
	<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>
	
	<form  action="" method="post">
    <input type="hidden" name="pid" value="<?php echo $var['pid']?>" />
	<table   cellpadding="0" cellspacing="0" class="tableS">
	<thead>
	<tr>
		<th colspan="2" align="left">Processor Details</th>	
	</tr>
	</thead>
	 <?php if(count($processor) == 0) : ?>
	 
      <tr>
		<td colspan="2"> - no records found - </td>
	  </tr>
	  <?php else: ?> 
       <tr>
						<td width="30%">Name</td>
                        <td><input type="text" name="name" value="<?php echo $processor['name'] ?>" /></td>
	   </tr> 
	   <tr>
	     				<td colspan="2">
						<p class="info"><b>Setting Your IPN</b></p>				
						<h4>For instant payment crediting to account, you have to set up your IPN on http://www.okpay.com. <br />To set up your IPN, just follow these steps:</h4>
						<ol>
						  <li>Login to your Okpay account.</li>
						  <li>Click on the wallet title under Wallets and Currencies section.</li> 
						  <li>Then, on the Wallet Properties page click on the Integration tab.</li>
						  <li>Enter the information:
						    <ol>
							  <li>Under, Auto Return URLs, enter "<b><?php echo SITEURL."/members/";?></b>" for Success URL and Fail URL</li>
							  <li>Under, Payment Notification (IPN), check 'Enable IPN' and enter "<b><?php echo SITEURL."/payment/ipn/id/".$var['pid'];?></b>" for Notification URL</li>
					        </ol>
					      </li>
						  <li>Click on &quot;Save&quot; button.</li>
						  </ol>	
						  <p class="info"><b>Setting Your API</b></p>				
						<h4>For automated withdrawal or automated mass payment you need to setup your API on http://www.okpay.com. <br />To set up your API, just follow these steps:</h4>
						<ol>
						  <li>Login to your Okpay account.</li>
						  <li>Click on the wallet title under Wallets and Currencies section.</li> 
						  <li>Then, on the Wallet Properties page click on the Integration tab.</li>
                          <li>Scroll down to API Access and provide the required information</li>
						    <ol>
							  <li>Enable the required API function. Activation of certain API features may require additional approval.</li>
                              <li>Provide a suitably strong access password. This password will be used in every request as part of the authentication process.</li>
					        </ol>
					      </li>
						  <li>Click on &quot;Save&quot; button.</li>
						  </ol>	
						   </td>
                        
	   </tr>
	   <tr>
	     				<td>Okpay Wallet ID</td>
                        <td><input type="text" name="payee" value="<?php echo $processor['payee'] ?>" /></td>
	   </tr>
	    <tr>
	     				<td>API Access Password</td>
                        <td><input type="password" name="api_key" value="<?php echo $processor['var_1'] ?>" /><img src="images/tooltip.png" alt="Tip" class="tooltip" title="This is your Okpay API Access Password. Leave blank if you do not want to make auto payments" /></td>
	   </tr>
       <tr>
						<td>Currencies</td>
                        <td><?php foreach ($settings['payment']['currencies'] as $id => $currency ) { ?>
	 
	 <?php echo $id; ?> <input name="currencies[]" type="checkbox"  value="<?php echo $id; ?>" <?php if ((stristr($processor['currencies'],",".$id.",")== true)) echo "checked=\"checked\" "; ?>  />
	 
	 
	  <?php } ?></td>
	   </tr>
	    <tr>
		
						<td>Allow Deposit</td>
                        <td><select name="deposit">
                               <option value="Y" <?php if($processor['deposit'] == 'Y')  echo "selected"?>>Yes</option>
                               <option value="N" <?php if($processor['deposit'] == 'N')  echo "selected"?>>No</option>
                           </select> 
                         </td>
	    </tr>
		 <tr>
						<td>Deposit Fee</td>
                        <td><input type="text" name="dfee" value="<?php echo $processor['dfee'] ?>" /></td>
	   </tr> 
		 <tr>
		
						<td>Allow Withdrawal</td>
                        <td><select name="withdraw">
                               <option value="Y" <?php if($processor['withdraw'] == 'Y')  echo "selected"?>>Yes</option>
                               <option value="N" <?php if($processor['withdraw'] == 'N')  echo "selected"?>>No</option>
                           </select> 
                         </td>
	    </tr>
		<tr>
		
						<td>Enable Auto Withdrawal</td>
                        <td><select name="autopay">
                               <option value="Y" <?php if($processor['autopay'] == 'Y')  echo "selected"?>>Yes</option>
                               <option value="N" <?php if($processor['autopay'] == 'N')  echo "selected"?>>No</option>
                           </select> <img src="images/tooltip.png" alt="Tip" class="tooltip" title="If this is enabled, users will be paid automatcally when they make a withdrawal from their wallet. API details must be set for this to work." />
                         </td>
	    </tr>
		 <tr>
						<td>Withdrawal Fee</td>
                        <td><input type="text" name="wfee" value="<?php echo $processor['wfee'] ?>" /></td>
	   </tr> 
		<tr>
		
						<td>Allow Subscriptions</td>
                        <td><select name="subscribe">
                               <option value="Y" <?php if($processor['subscribe'] == 'Y')  echo "selected"?>>Yes</option>
                               <option value="N" <?php if($processor['subscribe'] == 'N')  echo "selected"?>>No</option>
                           </select> 
                         </td>
	    </tr>
		 <tr>
						<td>Subscription setup fee</td>
                        <td><input type="text" name="sfee" value="<?php echo $processor['sfee'] ?>" /></td>
	    </tr> 
	    <tr>
		
						<td>Status</td>
                        <td><select name="status">
                               <option value="Active" <?php if($processor['status'] == 'Active')  echo "selected"?>>Active</option>
                               <option value="Pending" <?php if($processor['status'] == 'Pending')  echo "selected"?>>Pending</option>
                           </select> 
                         </td>
	    </tr>  
        <tr>
            <td colspan="2" align="left"><input type="submit" name="submit" value="Update" class="button_link">	</td>
        </tr>
     <?php endif; ?> 
	</table>
   
</form>

	