 <span class="form_title">Add Account</span>

<?php if($error_msg){ ?><span class="error_message"><?php echo $error_msg;?></span><? } ?>
<?php if($success_msg){ ?><span class="success_message"><?php echo $success_msg;?></span><? } ?>

<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
  <td width="430" height="200" align="center" valign="top">
  
  <div align="left">
  <a href="{#loader=system::url}/members/payment/accounts">All Accounts</a> > 
  <a href="{#loader=system::url}/members/payment/view_accounts/pid/<?php echo $var["pid"]?>">Manage Okpay Accounts</a> >
  New Account
  </div><br /><br />
  
  <div class="white">
	 
	<form action="" method="post" class="form_1">
	
	 <table cellspacing=1 cellpadding=2 border=0 width="100%" class="table_1"> 
	  <thead> 
	  <tr>
         <th colspan="2">Account details</th>
      </tr>
	  </thead> 
     <tr>
          <td>Okpay Wallet ID</td>
          <td><input type="text" name="values[wallet]" value="<?php echo $var["values"]["wallet"]?>" />
		   <input type="hidden" name="description[wallet]" value="Wallet ID" />
		  </td>
     </tr>
     <?php if($settings['account']['use_security_question'] == true && $userinfo['question']!="") { ?>
	   <tr>
         <td><?php echo $userinfo['question'] ?></td>
         <td><input type="text" name="answer" value=""></td>
     </tr>
     <?php } ?>
     <tr>
		 <td >&nbsp;</td>
		 <td ><input name="submit" type="submit" value="Add"></td>
	 </tr>
	</table>
	</form>
	
	</div></td></tr></table>  