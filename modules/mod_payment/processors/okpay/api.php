<?php

class OkpayApi 
{
	
	
	 const OKPAY_API_PAYMENT_WSDL_URL = "https://api.okpay.com/OkPayAPI?wsdl";
     const OKPAY_API_PAYMENT_HEADER_URL = "";
	 
    
     private $_oWalletID;
	 private $_oSecToken;
     private $_oClient;
	 
	 public function __construct($walletID, $password)
     {
               $this->_oWalletID = $walletID; 
			   $datePart = gmdate("Ymd");
			   $timePart = gmdate("H");
			   $authString = $password.":".$datePart.":".$timePart;
	
			   $sha256 = bin2hex(mhash(MHASH_SHA256, $authString));
			   $this->_oSecToken = strtoupper($sha256);
	
      } 
	  
	  public function getBalance($currency = null)
	  {
	           $this->_setupCredentials();
		       $obj->WalletID = $this->_oWalletID;
		       $obj->SecurityToken = $this->_oSecToken;
		       $obj->Currency = $currency;
			   
			   //$webService = $this->_oClient->Get_Date_Time();
			   //$wsResult = $webService->Get_Date_TimeResult;

			   if($currency != null)
			   {
			      $webService =  $this->_oClient->Wallet_Get_Currency_Balance($obj);
				  $this->_checkError($webService);
				  $wsResult = $webService->Wallet_Get_Currency_BalanceResult;
			   }
			   else
			   {
			      $webService = $this->_oClient->Wallet_Get_Balance($obj);
				  $this->_checkError($webService);
			      $wsResult = $webService1->Wallet_Get_BalanceResult;
			   }
			   return $wsResult;
	   }
	  
	   public function sendMoney($reciever, $amount, $currency, $details, $receiverPaysFees = FALSE)
	   { 
			   $this->_setupCredentials();
		       $obj->WalletID = $this->_oWalletID;
		       $obj->SecurityToken = $this->_oSecToken;
			   $obj->Receiver = $reciever;
			   $obj->Amount = $amount;
			   $obj->Currency = $currency;
			   $obj->Comment = $details;
			   $obj->IsReceiverPaysFees = $receiverPaysFees;
			   $webService = $this->_oClient->Send_Money($obj);
			   $this->_checkError($webService);
			   $wsResult = $webService->Send_MoneyResult;
			   return $wsResult;
			   
	    }
		
		public function getTransaction($transactionID)
	    { 
			   $this->_setupCredentials();
		       $obj->WalletID = $this->_oWalletID;
		       $obj->SecurityToken = $this->_oSecToken;
			   $obj->TxnID = $transactionID;
			   $webService = $this->_oClient->Transaction_Get($obj);
			   $this->_checkError($webService);
			   $wsResult3 = $webService->Transaction_GetResult;
			   return $wsResult;
			   
	    }
	
	    private function _setupCredentials($data =array())
        {                
               $this->_oClient = new SoapClient(self::OKPAY_API_PAYMENT_WSDL_URL);    
        }
		
	    private function _checkError($response)
        {
               if ($response === null)
                    throw new OkpayApiException('Invalid Response Format', (int) 0);
             /*if (isset($response['']) && $response[''] == 'ERROR')
                    throw new OkpayApiException('Message', (int) $response['']);*/
        } 
	
}


class OkpayApiException extends Exception 
{
    
}
?>