<?php


class PayPal extends Payment{
 
	
	
	var $code;
	var $payee;
	var $paymentUrl ="https://www.paypal.com/cgi-bin/webscr";// "https://www.sandbox.paypal.com/cgi-bin/webscr";
	var $currency = 'USD';
	var $default_button_buy = '';
	var $default_button_subscribe = '';
	var $logfile = "ipn_log.txt";
    
	//IMPORTANT!!!
	//remeber to set to false in production mode 
    var $testmode = false; 
	var $postdata = array(
	
	               
	);
    var $testdata = array(
	
	               'eid'=>'JG2Ln',	           
		           'payer'=>'payer@test.com',	          
		           'payee'=>'reciever@test.com',	        
		           'amount'=>'10',	      
		           'currency'=>'USD',	           
		           'description'=>'Account Funding',	          
		           'reference'=>'REF-1234-1234-1234',

               );
		         
	
	/*
	option_selection1
	
	receiver_email
	payer_email
	mc_gross // amount3 // mc_amount3
	mc_currency
	txn_id
	
	subscr_id
	txn_type
	
	period3 2 Y // 2 M // 2 D //
	period3
	
	subscr_effective
	
	*/
	

       function button( $data, $type ){ //required

               ///////////////////////////////
               /* $defined = array (
                //USER DEFINED FOR SIMPLE
				'id' => ,  //required - id of processor
                'amount' => , //required
                'discription' => ,
                'reference' => ,
                'success' => ,
                'fail' => ,
				'auto_submit'=>,				
				
				//optional
				'button' => , //  link to image for submit button
				'custom' => array( custom 1, custom 2.... custom n), //  custom data
				
				 //USER DEFINED FOR SUBSCRIBE
				'sub_time' =>'Year', //Day,Week,Month,Year
                'sub_duration' => 1,
                'sub_period' =>5,

  
                //SYSTEM DEFINED
                'payee' => $this->payee, //required
                'currency' => $this->currency, //required
				
 
  
            );*/
             
 
          if(!$this->__checkRequired($data,$type)) return "Parameters are not configured";
		  return $this->makeForm($data,$type);
       
     }


     function makeForm($data,$type){
 
            $data['button'] = ($data['button']) ? $data['button'] : $this->default_button_buy;
			
			switch($type){
			case'simple':
			case'advanced':	
		    $form ="
			       <form action='".$this->paymentUrl."' method='post' name='pro' class='form_1'>
                   <input type='hidden' name='cmd' value='_xclick'>
                   <input type='hidden' name='business' value='".$this->payee."'>
				   <input type='hidden' name='no_shipping' value='1' />
                   <input type='hidden' name='no_note' value='1' />
                   <input type='hidden' name='currency_code' value='".$this->currency."'>
                   <input type='hidden' name='item_name' value='".$data['discription']."'>
                   <input type='hidden' name='amount' value='".$data['amount']."'>
                   <input type='hidden' name='return' value='".$data['success']."'>
                   <input type='hidden' name='cancel_return' value='".$data['fail']."'>
                   <input type='hidden' name='notify_url' value='".SITEURL."/payment/ipn/id/".$data['id']."'>
                  ";
				   
			break;
			case'subscribe':	
			
				
		    $form.="
			
			       <form action='".$this->paymentUrl."' method='post' name='pro' class='form_1'>
                   <input type='hidden' name='cmd' value='_xclick-subscriptions'>
                   <input type='hidden' name='business' value='".$this->payee."'>
				   <input type='hidden' name='no_shipping' value='1' />
                   <input type='hidden' name='no_note' value='1' />
                   <input type='hidden' name='currency_code' value='".$this->currency."'>
                   <input type='hidden' name='item_name' value='".$data['discription']."'>
                   <input type='hidden' name='amount' value='".$data['amount']."'>
                   <input type='hidden' name='return' value='".$data['success']."'>
                   <input type='hidden' name='cancel_return' value='".$data['fail']."'>
				   <input type='hidden' name='notify_url' value='".SITEURL."/payment/ipn/id/".$data['id']."'>
		
                   <input type='hidden' name='a3' value='".$data['amount']."'>
                   <input type='hidden' name='p3' value='".$data['sub_duration']."'>
                   <input type='hidden' name='t3' value='".$data['sub_time'][0]."'>
                   <input type='hidden' name='src' value='1'>
                   <input type='hidden' name='sra' value='1'>
                   <input type='hidden' name='no_note' value='1'>
			      ";
				   
			break;
			}
			
			$num = 0;	
			$form.="<input  type=\"hidden\" name=\"on0\" value=\"eid\" >\n";
			$form.="<input  type=\"hidden\" name=\"os0\" value=\"".$data['eid']."\" >\n";	
		    if(!$data['auto_submit'])  $form.="<input type=\"submit\" name=\"submit\" value=\"Submit\"  /></form>";
            else  $form.="</form><script>document.forms.pro.submit();</script>";
            return $form;
		
	 }
	 
	  function __checkRequired($data, $type) {
	  
	  if(!isset($data['id'])) return false;
	  if(!self::setPayee($data['id'])) return false;
	  if(!isset($data['amount']) && !isset($this->currency)) return false;
	   //  if($type == 'subscribe') {}
	   return true;
	    
	 }
 
	 function setPayee($id)
	 {
	  
	   global $db;
	   $sql = "SELECT *,
	           AES_DECRYPT(payee,'".ENCKEY."') as payee
	           FROM ".PREFIX."_payment_processors 
	           WHERE id = '".$db->sql_quote($id)."' LIMIT 1 ";
	   $res = $db->query_db($sql,$print = DEBUG);
	   if ($db->num_rows($res) == 0 ) return false;
	   $processor = $db->fetch_db_array($res);
	   if($processor["payee"] == "")return false;
	   $this->payee = $processor["payee"];
	   return true;
	   
     }
	 
	 function ipn()
	 {
	     $verified = $this->verify();
	     if(!$verified) return false;
	     return true;
	 }
	  
	 function verify(){
     
	     if ($this->testmode == true) return true;
		 if(!$this->verifyPost()) return false;
		 
	     $id = $_GET["id"];
	     self::setPayee($id);
	    
	     $flagged = false;
	     $reason = "";
		 
	     if(strtoupper($_REQUEST['mc_currency']) != $this->currency) {$flagged = true;  $reason .= "Currency does not match, ";}
	     if($_REQUEST["receiver_email"] != $this->payee) {$flagged = true;  $reason .= "Merchant does not match, ";}
		 if($_REQUEST['payment_status'] != 'Completed')  {$flagged = true;  $reason .= "Payment status is not completed, ";}
	     if(!$flagged) return true;
	     self::failedDeposit($id,$reason);
	     return false;
	
	}
	

	function verifyPost()
	{
	
	   // STEP 1: Read POST data
 
       // reading posted data from directly from $_POST causes serialization 
       // issues with array data in POST
       // reading raw POST data from input stream instead. 
       $raw_post_data = file_get_contents('php://input');
       $raw_post_array = explode('&', $raw_post_data);
       $myPost = array();
       foreach ($raw_post_array as $keyval) {
	   
               $keyval = explode ('=', $keyval);
               if (count($keyval) == 2)
               $myPost[$keyval[0]] = urldecode($keyval[1]);
       }
       // read the post from PayPal system and add 'cmd'
       $req = 'cmd=_notify-validate';
       if(function_exists('get_magic_quotes_gpc')) $get_magic_quotes_exists = true;


        foreach ($myPost as $key => $value) { 
		       
                if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1)  $value = urlencode(stripslashes($value)); 
                else  $value = urlencode($value);
                $req .= "&$key=$value";
        }
 
 
          // STEP 2: Post IPN data back to paypal to validate
 
         $ch = curl_init($this->paymentUrl);
         curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
         curl_setopt($ch, CURLOPT_POST, 1);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
         curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
         curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
         curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
 
          // In wamp like environments that do not come bundled with root authority certificates,
          // please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path 
          // of the certificate as shown below.
          // curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
          if( !($res = curl_exec($ch)) ) {
          // error_log("Got " . curl_error($ch) . " when processing IPN data");
           curl_close($ch);
           return false;
          }
          curl_close($ch);
 
 
         // STEP 3: Inspect IPN validation result and act accordingly
         if (strcmp ($res, "VERIFIED") == 0) return true;
         else if (strcmp ($res, "INVALID") == 0) return false; 
		 return false;
    

	}
	
    function failedDeposit($id,$reason)
	{	
      $this->recordFailedPayment($id,$reason);	
	}
 
	function defineData()
	{
       if($this->testmode == true) 
       {
		 foreach ($this->testdata as $data => $value) define($data,$value);
         return;
       }
	   define('eid',$_REQUEST['option_selection1']);
	   define('payer',$_REQUEST['payer_email']); 
	   define('payee',$_REQUEST['receiver_email']); 
	   define('amount',$_REQUEST['mc_gross']); 
	   define('currency',$_REQUEST['mc_currency']); 
	   define('description',$_REQUEST['transaction_subject']);
	   define('reference',$_REQUEST['txn_id']);
	   define('sub_id',$_REQUEST['subscr_id']); 
	   switch($_REQUEST['txn_type']){
	  
		 case "subscr_payment":$sub_status = "Active";break;
		 case "subscr_failed":$sub_status = "Pending";break;
		 case "subscr_eot":$sub_status = "Cancelled";break;
		 case "subscr_cancel":$sub_status = "Cancelled";break;
		 default:break;
		 
	   }
	   
	   define('sub_status',$sub_status); //Active|Pending|Cancelled
				
	}
	 
	function autoPayment($to,$amount,$currency,$memo)
	{
	  // return array(true,"PAYPAL-BATCH");
	}
	
	function massPay()
	{
	   
	}
	
	function cancelSubscription($id,$key)
	{
	
	}
	
	
	 
		   
};

?>
