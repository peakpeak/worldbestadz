<?php
global $system,$userinfo,$db,$settings;

$var = $system->getVar();

if(!isset($var['pid'])) $system->invalidRequest();

$sql  = "SELECT * FROM ".PREFIX."_payment_processors ";

$sql .= "WHERE id = '".$db->sql_quote($var['pid'])."' LIMIT 1";
	
$result = $db->query_db($sql,$print = DEBUG);

if ($db->num_rows($result) == 0) $system->invalidRequest();

$processor = $db->fetch_db_array($result);

$include = ROOT.DS.'modules'.DS."mod_payment".DS."processors".DS.$processor['classdir'].DS.'members'.DS.'edit.php';

if(!file_exists($include))  $system->invalidRequest();
	   
if(!include_once($include))  $system->invalidRequest();

?>