<?php
   global $system,$userinfo,$db,$settings;

   $payment = $system->importClass('payment');
   
   $var = $system->getVar();
   
   if(!isset($var['sid'])) $system->invalidRequest();
   
   $sql = "SELECT * FROM ".PREFIX."_payment_subscriptions
		   WHERE username = '".$userinfo['username']."'
		   AND id = '".$db->sql_quote($var['sid'])."' LIMIT 1";
   $res 	= $db->query_db($sql,$print = DEBUG);
   if ($db->num_rows($res) == 0) $system->invalidRequest();
   $row = $db->fetch_db_array($res);
   
   
   $sql = "SELECT `name`,`deposit_type` FROM ".PREFIX."_payment_processors
		   WHERE id = '".$row['method']."' LIMIT 1";
   $res2 = $db->query_db($sql,$print = DEBUG);
   $row2 = $db->fetch_db_array($res2);
   $row['method'] = $row2['name'];
   $prefix = "Every ";
   $duration = ($row['bill_duration'] == 1) ?" ":$row['bill_duration'];
   $period = ($row['bill_duration'] == 1) ? rtrim($row['bill_period'],"s"):$row['bill_period'];
   $row['details'] = $prefix.$duration.$period;
   
   
   
   
   $pageInfo['title']='Subscription Details';
  
   $pageInfo['map'] = array(
	                  'Subscriptions' => SITEURL.FS.'members'.FS.'payment/subscriptions',
					  'Subscription Details' => '',
					  );
   
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'subscription_details.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('popup');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('popup');
   
   $loader->displayOutput();
?>