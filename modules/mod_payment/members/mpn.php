<?php
  global $system,$db,$userinfo,$settings;

  $system->importClass('payment');

  $var = $system->getVar();
  
 // if (!isset($var['processor'])  || !isset($var['amount'])  || !isset($var['item'])) $system->invalidRequest();
  
  if($var['generate'])
  {
	 $table = "_payment_deposits";
	  
	 if(!$db->if_row_exist($table,array("username" => $userinfo["username"],"`mod`"=>$var['caller'],"status"=>"Pending"))){
	  /*addDeposit($username,$mod,$type,$description,$method,$amount,$currency = "USD",$details = null,$reference = null, $status = 'Pending',$eids = array())*/
	 $deposit = Payment::addDeposit($userinfo["username"],$var['caller'],"Manual",$var['item'],$var['processor'],$var['amount'],$var['fee'],$var['currency'],"","","Pending",array($var['eid1'],$var['eid2'],$var['eid3'],$var['eid4']));
	 
	 
	 if($deposit[0]) {
	 
	    /* $eid = $db->get_item("eid_on_generate","id",$deposit[1],$table);
	     Payment::defineCustomData($eid);	
         $file = Payment::getExecuteFile($eid); 
         $system->systemExec($file,get_defined_vars()); 
		 Payment::updateExecutionStatus($eid,'Completed');*/
		 
	     if($deposit[0]) $msg = "Your payment request was generated";
	 }
	    if($var["returnurl"])$system->redirect($var["returnurl"]."/msg/".urlencode($msg));
	 
	 }else  $msg = "You already have a pending payment request. You cannot make a new request at this time";
	  
	 
	
	 
	 
	
   }elseif($var['confirm']){
  
    if($var['details'] == '' || $var['reference'] == ''){$msg = "Please complete all required field.";
	
	}else{
	
	
    $table = "_payment_deposits";
	if($db->if_row_exist($table,array("username" => $userinfo["username"],"id"=>$var['confirm'],"status" => "Pending")) || $db->if_row_exist($table,array("username" => $userinfo["username"],"id"=>$var['confirm'],"status" => "Open"))){
    Payment::confirmDeposit($var['confirm'],$var['details'],$var['reference'],$var['comments']);
	$msg = "Deposit request updated.";
	
	  
   } else { $msg = "Invalid ID"; }}
   
   
   }elseif($var['cancel']){
   
    $table = "_payment_deposits";
    if($db->if_row_exist($table,array("username" => $userinfo["username"],"id"=>$var['cancel'],"status"=>"Pending")))
	{
	  Payment::cancelDeposit($var['cancel']);
	  $msg = "Payment Canceled";
	}
	else $msg = "Invalid ID"; 
   
   
  
   }else{}
  if($var["returnurl"])$system->redirect($var["returnurl"]."/msg/".urlencode($msg));
  

 
   
 ?>

 