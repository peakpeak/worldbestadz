<?php
   global $system,$userinfo,$db,$settings;

   $payment = $system->importClass('payment');
   
   $var = $system->getVar();
   
   if(!isset($var['did'])) $system->invalidRequest();
   
   $sql = "SELECT * FROM ".PREFIX."_payment_deposits
		   WHERE username = '".$userinfo['username']."'
		   AND id = '".$db->sql_quote($var['did'])."' LIMIT 1";
   $res 	= $db->query_db($sql,$print = DEBUG);
   if ($db->num_rows($res) == 0) $system->invalidRequest();
   $row = $db->fetch_db_array($res);
   
   
   $sql = "SELECT `name`,`deposit_type` FROM ".PREFIX."_payment_processors
		   WHERE id = '".$row['method']."' LIMIT 1";
   $res2 = $db->query_db($sql,$print = DEBUG);
   $row2 = $db->fetch_db_array($res2);
   $row['method'] = $row2['name'];
   $row['deposit_type'] = $row2['deposit_type'];
   
   $pageInfo['title']='Confirm Payment';
  
   $pageInfo['map'] = array(
	                  'My Payments' => SITEURL.FS.'members'.FS.'payment/deposits',
					  'Payment Details' => '',
					  );
   
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'deposit_details.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('popup');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('popup');
   
   $loader->displayOutput();
?>