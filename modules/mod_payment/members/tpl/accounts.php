<span class="form_title">Manage Accounts</span>

<?php if($error_msg){ ?>

<span class="error_message"><?php echo $error_msg;?></span>

<? } ?>

<?php if($success_msg){ ?>

<span class="success_message"><?php echo $success_msg;?></span>

<? } ?>

<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">

  <tr>

    <td width="430" height="200" align="center" valign="top"><div class="white">

        <table  cellspacing=1 cellpadding=2 border=0 width="100%" class="table_1">

          <thead>

            <tr>

              <th colspan="2">Manage Account </th>

            </tr>

          </thead>
          <?php $new_icons=array(
                    '2' => '<img src="/images/icons/payza.png" alt="Payza" class="payza" />',
                    '3' => '<img src="/images/icons/stp.png" alt="Solidtrustpay" class="payza" />',
                    '6' => '<img src="/images/icons/bitcoin.png" alt="Bitcoin" class="payza" />',
                    '7' => '<img src="/images/icons/perfectmoney.png" alt="Perfectmoney" class="payza" />'
          );
            ?>
          <?php  foreach ($new_icons as $id => $value) { ?>

          <tr>


            <td><?php echo $value; ?></td>

            <td width="10%" align="center"><a href="{#loader=system::url}/members/payment/view_accounts/pid/<?php echo $id;?>">Manage</a></td>

          </tr>

          <?php }?>

          <?php if(count($processors) == 0) { ?>

          <tr>

            <td colspan="2" align="center"> - no records found -</td>

          </tr>

          <?php }  ?>

          <?php if($num_rows > $per_page) { ?>

          <tr>

            <td colspan="2" align="center"><?php echo $system->getPaginationString($page,$num_rows,$per_page,3,$system->curPageURL(),"page");?></td>

          </tr>

          <?php } ?>

        </table>

      </div></td>

  </tr>

</table>

