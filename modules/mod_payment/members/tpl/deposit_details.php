<span class="form_title">Deposit Details</span>
<?php if($error_msg){ ?>
<span class="error_message"><?php echo $error_msg;?></span>
<? } ?>
<?php if($success_msg){ ?>
<span class="success_message"><?php echo $success_msg;?></span>
<? } ?>
<?php if($var['msg']){ ?>
<span class="success_message"><? echo $var['msg'];?></span>
<?php } ?>
<div class="white">
<table cellpadding="3" cellspacing="0" border="0" width="50%" class="table_1">
  <tr>
    <td><label>Date:</label></td>
    <td><?php echo $row['date'] ?></td>
  </tr>
  <tr>
    <td><label>Amount:</label></td>
    <td><?php  echo $settings['payment']['currencies'][$row['currency']]['symbol']." ".number_format($row['amount'],2)?></td>
  </tr>
  <tr>
    <td><label>Fee:</label></td>
    <td><?php  echo $settings['payment']['currencies'][$row['currency']]['symbol']." ".number_format($row['fee'],2)?></td>
  </tr>
  <tr>
    <td><label>Payment Method:</label>
    </td>
    <td><?php echo $row['method'] ?>
      <?php if( $row['deposit_type'] == 'Manual') { ?>
      [ <a href="{#loader=system::url}/members/payment/info/did/<?php echo $row['id'] ?>" onclick="window.open(this.href,'popupwindow','width=900,height=600,toolbar=yes,status=no,scrollbars=yes,resizable=no,menubar=yes,location=no,direction=no');
 return false;"  class=""  style="text-decoration:none;">Payment Instructions</a> ]
      <?php } ?></td>
  </tr>
  <tr>
    <td><label>Description:</label></td>
    <td><?php echo $row['description'] ?></td>
  </tr>
  <tr>
    <td><label>Comment:</label></td>
    <td><?php echo ($row['payer_comments'] != null) ? $row['payer_comments']: "None"; ?></td>
  </tr>
  <?php if($row['details'] != null) { ?>
  <tr>
    <td><label>Details:</label></td>
    <td><?php echo $row['details'] ?></td>
  </tr>
  <?php } ?>
  <?php if($row['reference'] != null) { ?>
  <tr>
    <td><label>Reference:</label></td>
    <td><?php echo $row['reference'] ?></td>
  </tr>
  <?php } ?>
  <tr>
    <td><label>Status:</label></td>
    <td><?php echo $row['status'] ?>
      <?php if( $row['deposit_type'] == 'Manual' &&  ( $row['status'] == 'Pending' )) { ?>
      [ <a href="#" onclick="if(confirm('Do you really want to cancel this payment?'))document.forms.cancelform.submit()"  style="text-decoration:none;">Cancel Payment</a> ]
      <form action="index.php" name="cancelform">
        <input type="hidden" name ="mod" value="payment" />
        <input type="hidden" name ="go" value="mpn" />
        <input type="hidden" name ="cancel" value="<?php echo $row['id'] ?>" />
        <input type="hidden" name ="returnurl" value="{#loader=system::url}/members/payment/deposit_details/did/<?php  echo $var['did'] ?>" />
      </form>
      <?php } elseif($row['deposit_type'] == 'Auto' && $row['status'] != 'Approved') { ?>
      [ Contact Admin ]
      <?php }?></td>
  </tr>
</table>
