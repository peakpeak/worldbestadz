<span class="form_title">Subscriptions</span>
<?php if($error_msg){ ?>
<span class="error_message"><?php echo $error_msg;?></span>
<? } ?>
<?php if($success_msg){ ?>
<span class="success_message"><?php echo $success_msg;?></span>
<? } ?>
<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td width="430" height="200" align="center" valign="top"><div class="white">
        <table  cellspacing=1 cellpadding=2 border=0 width="100%" class="table_2">
          <thead>
            <tr>
              <th>#</th>
              <th>Date</th>
              <th>Amount</th>
              <th>Method</th>
              <th>Acc ID</th>
              <th>Description</th>
              <th>Status</th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <?php   $i = 0;  foreach ($subscriptions as $num => $data) { $i++; ?>
          <tr>
            <td align="center"><a href="#"><strong>#<?php echo $num; ?></strong></a></td>
            <td><?php echo $data['date']; ?></td>
            <td><?php echo $settings['payment']['currencies'][$data['currency']]['symbol']; ?> <?php echo $data['amount']; ?></td>
            <td><?php echo $data['processor']; ?></td>
            <td><?php echo $data['accountid']; ?></td>
            <td><?php echo $data['description']; ?></td>
            <td><?php echo $data['status']; ?> </td>
            <td nowrap="nowrap"><b><a href="{#loader=system::url}/members/payment/subscription_details/sid/<?php echo $data['id']; ?>"  id="popup<?php echo $i?>">Details</a></b></td>
          </tr>
          <?php } ?>
          <?php if(count($subscriptions) == 0) { ?>
          <tr>
            <td colspan="8" align="center"> - no records found -</td>
          </tr>
          <?php }?>
          <?php if($num_rows > $per_page) : ?>
          <tr>
            <td colspan="8" align="center"><?php echo $system->getPaginationString($page,$num_rows,$per_page,3,$system->curPageURL(),"page");?></td>
          </tr>
          <?php endif; ?>
        </table>
      </div></td>
  </tr>
</table>
<script type="text/javascript">
 
   
	 var first = 0;
	 var last = <?php echo $i;?>;	
	  fancyjQuery(document).ready(function() {
			
			for (i = first; i  <= last; i++) {
			
			fancyjQuery("#popup"+i).fancybox({
				'width'				: '60%',
				'height'			: '100%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
          }
		
		});
  </script>
