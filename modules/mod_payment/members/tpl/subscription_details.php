<span class="form_title">Subscription Details</span>
<?php if($error_msg){ ?>
<span class="error_message"><?php echo $error_msg;?></span>
<? } ?>
<?php if($success_msg){ ?>
<span class="success_message"><?php echo $success_msg;?></span>
<? } ?>
<?php if($var['msg']){ ?>
<span class="success_message"><? echo $var['msg'];?></span>
<?php } ?>
<div class="white">
  <table cellpadding="3" cellspacing="0" border="0" width="50%" class="table_1">
    <tr>
      <td><label>Date:</label></td>
      <td><?php echo $row['date'] ?></td>
    </tr>
    <tr>
      <td><label>Description:</label></td>
      <td><?php echo $row['description'] ?></td>
    </tr>
    <tr>
      <td><label>Amount:</label></td>
      <td><?php  echo $settings['payment']['currencies'][$row['currency']]['symbol']." ".number_format($row['amount'],2)?>
        <?php echo $row['details'];?></td>
    </tr>
    <!--<tr>
		<td><label>Fee:</label></td>
		<td><?php  echo $settings['payment']['currencies'][$row['currency']]['symbol']." ".number_format($row['fee'],2)?></td>
	</tr>-->
    <tr>
      <td><label>Payment Method:</label>
      </td>
      <td><?php echo $row['method'] ?></td>
    </tr>
    <tr>
      <td><label>Account ID:</label>
      </td>
      <td><?php echo $row['accountid'] ?></td>
    </tr>
    <tr>
      <td><label>Creation Date:</label>
      </td>
      <td><?php echo $row['date'] ?></td>
    </tr>
    <tr>
      <td><label>Last Billing:</label>
      </td>
      <td><?php echo $row['lastbill_date'] ?></td>
    </tr>
    <?php if( $row['status'] != "Cancelled") { ?>
    <tr>
      <td><label>Next Billing:</label>
      </td>
      <td><?php echo $row['nextbill_date'] ?></td>
    </tr>
    <?php } ?>
    <tr>
      <td><label>Status:</label></td>
      <td><?php echo $row['status'] ?>
        <?php if( $row['status'] != "Cancelled") { ?>
        <?php } ?></td>
    </tr>
  </table>
</div>
