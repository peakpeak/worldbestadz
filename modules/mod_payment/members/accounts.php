<?php
   global $system,$db,$userinfo,$settings;

   $payment = $system->importClass('payment');

   $processors = $payment->processors('withdraw');

   $var = $system->getVar();
  
   $pageInfo['title'] = 'Accounts';
  
   $pageInfo['map'] = array('Accounts' => '',);
   
   $loader = new Loader;
   
   $data= get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'accounts.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('private');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('private');
   
   $loader->displayOutput();