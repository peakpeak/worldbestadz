<?php
 global $system,$db,$userinfo,$settings;

 $system->importPlugin('dompdf');

 $var = $system->getVar();
 
 $sql = "SELECT * FROM ".PREFIX."_payment_deposits WHERE username = '".$userinfo['username']."' ";
 $sql .="AND id = '".$db->sql_quote($var["did"])."' LIMIT 1";

 $res = $db->query_db($sql,$print = DEBUG);
 if($db->num_rows($res) == 0) header("location:index.php");
 $row = $db->fetch_db_array($res);

 $sql = "SELECT *, AES_DECRYPT(var_1,'".ENCKEY."') as var_1 FROM ".PREFIX."_payment_processors WHERE id = '".$row['method']."' LIMIT 1 ";
 $res = $db->query_db($sql,$print = DEBUG);
 if($db->num_rows($res) > 0)  $prow = $db->fetch_db_array($res);
 
 //$tpl_file =  ROOT.DS.'modules'.DS.'mod_payment'.DS.'processors'.DS.$prow['classdir'].DS.$prow['templatefile'];

 $html = $prow["var_1"];//file_get_contents($tpl_file);
 
 $st_rep = array(
									ucwords($userinfo['firstname']." ".$userinfo['lastname']),
									$userinfo['username'],
									$userinfo['email'],
									$row['date'],
									$row['description'],
									$row['id'],
									$settings['payment']['currencies'][$row['currency']]['symbol'],
									number_format($row['amount'],2),
									$row['status'],
									"",
									);
									
  $st_vari =  array(
									"{Name}",
									"{Username}",
									"{Email}",
									"{Date}",
									"{Item}",
									"{Ref}",
									"{Currency}",
									"{Amount}",
									"{Status}",
									"{Logo}",
									);
									 
  $html  = str_replace($st_vari, $st_rep, $html);
  echo $html;
  /*$dompdf = new DOMPDF();
  $dompdf->load_html($html);
  $dompdf->set_paper("a4","portrait");
  $dompdf->render();
  $dompdf->stream("dompdf_out.pdf", array("Attachment" => false));*/
  exit(0); 
 ?>