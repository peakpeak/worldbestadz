<?php
global $system;
$payment = $system->importClass('payment');
$id = $system->getVar("id");
$payment->ipn($id);
if (!defined('eid') || eid == '') exit('Error occured');//carry out failure action and exit
$payment->logResponse(eid);
if($payment->executionStatus(eid) == 'Completed') exit('Execution already completed');//carry out failure action and exit
$custom = $payment->extractCustomData(eid);
if($id != $custom['method'])  exit('Payment method does not match');
$file = $payment->getExecuteFile(eid); 
$system->systemExec($file,get_defined_vars());
$payment->updateExecutionStatus(eid,'Completed');
