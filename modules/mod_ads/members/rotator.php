<?php
session_start();

global $system,$db,$userinfo,$settings;

$pagetitle = 'Surf Rotator';

$ads = $system->importClass('ads');

$var = $system->getVar();

$ads->startSurfSession($userinfo['username']);

$date = date("Y-m-d");

$time_now = date("H:i:s");

$time = time();

$last_click = $ads->userLastClick($userinfo['username']);

///////////////////////Process Post Data/////////////////////////////
$tmp_xy = explode('-', $_SESSION['sess_data']['xy_postition']);
$x_min = $tmp_xy[0];
$x_max = $tmp_xy[1];

if ($_POST['surf'] == 'ok' )
{
   if($last_click != 'none' && $last_click < $_SESSION['surfdata']['timer'])$_SESSION['surfdata']['msg'] = "You have clicked on the image too fast";
   elseif($_REQUEST['token'] == $x_min ){
   
		$_SESSION['surfdata']['ctoday']+=1;
		$_SESSION['surfdata']['usurftoday']+= 1;
		$_SESSION['surfdata']['hits']+=$_SESSION['surfdata']['cr_per_surf'];
		 ///////////////Check User surfing Record and update it/////// 
	 if ($_SESSION['surfdata']['usurf'] == 'NO' || $_SESSION['surfdata']['usurfid'] == NULL)
		{
			$sql = "SELECT id,surfed,credits FROM ".PREFIX."_ads_surf_statistics
			        WHERE username ='".$userinfo['username']."'
					AND date = '".$date."' LIMIT 1 ";
			$res = $db->query_db($sql,$print = DEBUG);
			
			if ($db->num_rows($res) > 0)
			{
					//// GET user Surfing Data/////////
					$u_surfed 	= $db->db_result($res,0,'surfed');
					$u_surfid   = $db->db_result($res,0,'id');
					$u_credits  = $db->db_result($res,0,'credits');
 					$_SESSION['surfdata']['usurftoday'] = $u_surfed+1 ;
					$_SESSION['surfdata']['usurfid']	= $u_surfid;
					$_SESSION['surfdata']['hits']		= $u_credits+$_SESSION['surfdata']['cr_per_surf'];
					
					/// Update Record 
					$sql = "UPDATE ".PREFIX."_ads_surf_statistics SET
							surfed=surfed+1,credits=credits+'".$db->sql_quote($_SESSION['surfdata']['cr_per_surf'])."',
							time = '".$time_now."' 
							WHERE id = '".$db->sql_quote($u_surfid)."' LIMIT 1";
					$res = $db->query_db($sql,$print = DEBUG);
			 }			
			 else
			 {
					//////Add surf Record/////////////////////
					$sql = "INSERT INTO ".PREFIX."_ads_surf_statistics
					(username,surfed,credits,date,time)
					VALUES 
					('".$userinfo['username']."','1','".$db->sql_quote($_SESSION['surfdata']['cr_per_surf'])."','".$date."','".$time_now."' )"; 
					$res = $db->query_db($sql,$print = DEBUG);
			}
			
			$system->handleEvent('on_surf_verified', get_defined_vars());
			
		 }
		 else
		 {
				///// update record 
				$sql = "UPDATE ".PREFIX."_ads_surf_statistics SET
						surfed=surfed+1,credits=credits+'".$db->sql_quote($_SESSION['surfdata']['cr_per_surf'])."',
						time = '".$time_now."' 
						WHERE id = '".$db->sql_quote($_SESSION['surfdata']['usurfid'])."' LIMIT 1";
						
				$res = $db->query_db($sql,$print = DEBUG);

		 }
			
			//////// UPDATE user Settings////////////////////////////
			$sql = "UPDATE ".PREFIX."_ads_users set 
			       website_credits=website_credits+'".$db->sql_quote($_SESSION['surfdata']['cr_per_surf'])."'
				   WHERE username='".$userinfo['username']."'LIMIT 1 ";
				   
			$res = $db->query_db($sql,$print = DEBUG);
			
			//////////// Assign Msg//////////////////////////////////// 
           $_SESSION['surfdata']['msg'] = "You have earned ".$_SESSION['surfdata']['cr_per_surf']." Credit(s).";
		   
		}else{
		
		
		   $_SESSION['surfdata']['msg'] = "Oops!! You have clicked on the wrong image";
		
		
		}
	
}


   ///////////// Get Site to be show in rotator//////////////

   
   $day = date(",D,");
  
   if($settings['ads']['categorized_ads'] &&  $_SESSION['surfdata']['category'] != "all"){
   
     $user_categories = explode(",",$_SESSION['surfdata']['category']);
     $sql_category = "AND ( ";
     foreach($user_categories as $cat)$sql_category .= " ".$cat." IN (category) OR";
     $sql_category = rtrim($sql_category,"OR");
     $sql_category .= " ) ";
	 
   }else $sql_category = ""; 
   $sql_target =($settings['ads']['geotargeted_ads'] &&  $_SESSION['surfdata']['geo_target'] != "none")? "AND target = '".$_SESSION['surfdata']['geo_target']."' " :" ";
   $pid = $_SESSION['surfdata']['package'];
	
	$sql = "SELECT * ";
    $sql.= "FROM ".PREFIX."_ads_websites as w ";
	$sql.= "WHERE credits > 0 ";
	/*$sql.= "AND (( IF(
	                  (SELECT count(*) FROM ".PREFIX."_ads_website_statistics as s WHERE s.site_id = w.id AND s.date = curdate()) > 0, 
	                  (SELECT totalhits FROM ".PREFIX."_ads_website_statistics as s WHERE s.site_id = w.id AND s.date = curdate()), 0
					 ) < w.credits_per_day 
					 
				 ) OR w.credits_per_day = 0) ";*/
	$sql.= "AND (( w.credits_today < w.credits_per_day OR FROM_UNIXTIME(w.last_visit,'%Y-%m-%d') != '".$date."')  OR w.credits_per_day = 0 )";
	$sql.= "AND status = 'Active' ";
	$sql.= "AND startdate <= curdate() ";
	$sql.= "AND display_days LIKE '%".$day."%' ";
	$sql.= "AND package = '".$db->sql_quote($pid)."' ";
	//$sql.=  $sql_category;
	//$sql.=  $sql_target;
	$sql.= " ORDER BY rand() LIMIT 1";
			
    $res = $db->query_db($sql,$print = DEBUG);
	$_SESSION['surfdata']['psurfed']+=1;
	
	if ($db->num_rows($res) > 0)
    {
	
        $site_show = $db->db_result($res,0,'url');
        $site_id   = $db->db_result($res,0,'id');
        $username = $db->db_result($res,0,'username');
		$last_visit = $db->db_result($res,0,'last_visit');
		$credits_today = $db->db_result($res,0,'credits_today');
        $ads->checkWebsiteCredit($site_id);
		$cr_today = (strftime("%Y/%m/%d",$last_visit) == strftime("%Y/%m/%d")) ? ($credits_today + $_SESSION['surfdata']['cr_per_exposure']):$_SESSION['surfdata']['cr_per_exposure'];
        $_SESSION['surfdata']['site_show_id'] = $site_id;

         ////////Up date site Credits//////////////////////////
        $sql = "UPDATE ".PREFIX."_ads_websites 
		        SET credits=credits-".$_SESSION['surfdata']['cr_per_exposure'].",
				credits_used=credits_used+".$_SESSION['surfdata']['cr_per_exposure'].",
				credits_today=".$cr_today.",
                last_visit='".time()."',
				memhits=memhits+1,
	        	totalhits=totalhits+1
		        WHERE id='".$db->sql_quote($site_id)."' LIMIT 1 "; 	
        $db->query_db($sql,$print = DEBUG);

         ////////////////////////////////////
	    $sql = "SELECT id FROM ".PREFIX."_ads_website_statistics
			    WHERE username='".$username."'
			    AND site_id='".$db->sql_quote($site_id)."'
			    AND date = '".$date."' LIMIT 1 ";
		$res = $db->query_db($sql,$print = DEBUG);
				
		if ($db->num_rows($res) > 0)
		{
					/// Update Record 
					$sql = "UPDATE ".PREFIX."_ads_website_statistics SET
							totalhits=totalhits+1,memhits=memhits+1,credits=credits+'".$db->sql_quote($_SESSION['surfdata']['cr_per_exposure'])."',
							time = '".$time_now."' 
							WHERE site_id='".$db->sql_quote($site_id)."' 
							AND date = '".$date."' LIMIT 1";
					$res = $db->query_db($sql,$print = DEBUG);
		}
		else
		{
					
					$sql = "INSERT INTO ".PREFIX."_ads_website_statistics
					(username,site_id,totalhits,memhits,date,time,credits)
					VALUES 
					('".$username."','".$site_id."','1','1','".$date."','".$time_now."','".$db->sql_quote($_SESSION['surfdata']['cr_per_surf'])."' )"; 
					$res = $db->query_db($sql,$print = DEBUG);
		}
		 
		 
		 
     } else  $site_show = $settings['ads']['default_website'];
       $db->free_result($res);
	   $_SESSION['surfdata']['site_show'] = $site_show;
	   
	  
$ads->surfimages = SITEURL."/modules/mod_ads/img/";

$img_dir = $ads->surfimages;

$click_bar_arrs = $ads->getSurfImages();

$valid_x_position[0] = "1-56";
$valid_x_position[1] = "57-113";
$valid_x_position[2] = "114-170";
$valid_x_position[3] = "171-227";


//=====

$rand_nc_list = array_rand($click_bar_arrs, 10);

$rand_nc_next = array_rand($click_bar_arrs, 1);

while (list($o, $p) = each($click_bar_arrs)) {

 if ($rand_nc_next == $o) 
 {

  $click_img_src = $click_bar_arrs[$o];

  $click_vals = $rand_nc_next;

 }

}

$temp_cv = explode('|', $click_vals);

$c_1 = $temp_cv[0];

$c_2 = $temp_cv[1];

$c_3 = $temp_cv[2];

$c_4 = $temp_cv[3];

$rn = rand(1, 24);

if ($rn == 1 || $rn == 5 || $rn == 9 || $rn == 13 || $rn == 17 || $rn == 21) {

 $zz_zz = $c_1;

 $xy_pos = $valid_x_position[0];

} elseif ($rn == 2 || $rn == 6 || $rn == 10 || $rn == 14 || $rn == 18 || $rn == 22) {

 $zz_zz = $c_2;

 $xy_pos = $valid_x_position[1];

} elseif ($rn == 3 || $rn == 7 || $rn == 11 || $rn == 15 || $rn == 19 || $rn == 23) {

 $zz_zz = $c_3;

 $xy_pos = $valid_x_position[2];

} else {

 $zz_zz = $c_4;

 $xy_pos = $valid_x_position[3];

}


$_SESSION['sess_data']['next_click'] = $zz_zz;

$_SESSION['sess_data']['xy_postition'] = $xy_pos;

$_SESSION['sess_data']['picked_val_was'] = rand(2000, 3000);
//=====

 $tmp_xy = explode('-', $_SESSION['sess_data']['xy_postition']);

 $x_min = $tmp_xy[0];

 $x_max = $tmp_xy[1];
    


/***************************************************************/


   
   $loader = new Loader;
   
   $data= get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'rotator.php';
   
   $loader->setVar($data);
      
   $loader->loadOutput($tpl_file);
   
   $loader->displayOutput();
