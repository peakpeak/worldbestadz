<?php
global $system,$db,$userinfo,$settings;

$ads = $system->importClass('ads');

$userinfo['mid'] = (!$ads->isInvestor($userinfo['username']))?1:2;

$noAds =  $ads->getNoAds($userinfo['username'],"loginads");

$var = $system->getVar();

if($var['submit'])
{

  switch($var['submit'])
  {
	/*Add Website*/
	
    case("add this ad"):
	
    if(($noAds + 1) > $settings["ads"]["max_websites_".$userinfo['mid']])
	{
		$errors[] =  "You are only allowed to add ".$settings["ads"]["max_websites_".$userinfo['mid']]." website(s)";
	}
    elseif($var['title']=='')
	{
		$errors[] =  "Add a title";
	}
	elseif($var['name']=='')
	{
		$errors[] =  "Add a website name";
	}
	elseif($var['url']=='' || !Validate::url($var['url']))
	{
		$errors[] =  "Add a valid URL";
	}
	elseif ($var['agree']!=1)
	{
	    $errors[] =  "You must agree to the terms and conditions";
    }
	else
	{	  
	   $sql="SELECT name FROM ".PREFIX."_ads_loginads  WHERE username = '".$var['username']."' AND name='".$db->sql_quote($var['name'])."' LIMIT 1";
	   $res = $db->query_db($sql,$print = DEBUG);
       if ($db->num_rows($res) > 0)  $errors[] =  "An ad with this name already exist"; 
	}
	if($var['credits'] > 0)
	{  
	   $lcredits = $ads->adsCredits($userinfo['username'],'loginads');
       if($var['credits'] > $lcredits) $errors[] = "Insufficient Login Ad Credit(s)";  
	}
	if(is_array($errors) && !empty($errors))
	{
		while (list($key,$value) = each($errors)) $error_msg.= $value.'<br>';		
	}
	else 
	{
	    //Add Website		
	   if (count($var['day']) > 0)	   
         foreach ($var['day'] as $v)  $display .= ","."$v".",";               
	   else $display = "";
	
		
		$added = $ads->addLoginAd($userinfo['username'],$var['name'],$var['title'],$var['url'],$display,$var['limit']);
		if($added && $var['credits'] > 0) $ads->assignLoginAdCredits($userinfo["username"],$added,$var['credits']); 
		if($added) $success_msg = 'Ad added';	
	}
	

    break;
  }

}

   $userinfo['mid'] = (!$ads->isInvestor($userinfo['username']))?1:2;
   
   $loginads_credits = number_format($ads->adsCredits($userinfo['username'],'loginads'),2);
   
   $num_websites =  $ads->getNoAds($userinfo['username'],"loginads");
   
   $max_websites = $settings["ads"]["max_websites_".$userinfo['mid']];
	
   $loader = new Loader();
   
   $data= get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'add_loginad.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('private');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('private');
   
   $loader->displayOutput();