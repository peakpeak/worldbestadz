<?php
global $system,$db,$userinfo,$settings;

$ads = $system->importClass('ads');

$userinfo['mid'] = (!$ads->isInvestor($userinfo['username']))?1:2;

$noWebsites =  $ads->getNoAds($userinfo['username'],"websites");

$var = $system->getVar();

$packages = $ads->getWebsitePackages();

$categories = $ads->getAdCategories();

if($var['submit'])
{

  switch($var['submit'])
  {
	/*Add Website*/
	
    case("add this website"):
	
    if(($noWebsites + 1) > $settings["ads"]["max_websites_".$userinfo['mid']])
	{
		$errors[] =  "You are only allowed to add ".$settings["ads"]["max_websites_".$userinfo['mid']]." website(s)";
	}
    elseif($var['title']=='')
	{
		$errors[] =  "Add a title";
	}
	elseif($var['name']=='')
	{
		$errors[] =  "Add a website name";
	}
	elseif($var['url']=='' || !Validate::url($var['url']))
	{
		$errors[] =  "Add a valid URL";
	}
	elseif (count($var['categories']) < 1 && $settings["ads"]["categorized_ads"])
	{
		$errors[] =  "You must select one category at least";
	}
	elseif ($var['agree']!=1)
	{
	    $errors[] =  "You must agree to the terms and conditions";
    }
	else
	{	  
	   $sql="SELECT name FROM ".PREFIX."_ads_websites  WHERE username = '".$var['username']."' AND name='".$db->sql_quote($var['name'])."' LIMIT 1";
	   $res = $db->query_db($sql,$print = DEBUG);
       if ($db->num_rows($res) > 0)  $errors[] =  "A website with this name already exist"; 
	   
	   $sql="SELECT url FROM ".PREFIX."_ads_websites  WHERE username = '".$userinfo['username']."' AND url='".$db->sql_quote($var['url'])."' AND package='".$db->sql_quote($var['package'])."' LIMIT 1";
	   $res = $db->query_db($sql,$print = DEBUG);
       if ($db->num_rows($res) > 0)  $errors[] =  "This website already exist in the choosen rotator"; 
	}
	if($var['credits'] > 0)
	{  
	   $wcredits = $ads->adsCredits($userinfo['username'],'website');
       if($var['credits'] > $wcredits) $errors[] = "Insufficient Website Credit(s)";  
	}
	if(is_array($errors) && !empty($errors))
	{
		while (list($key,$value) = each($errors)) $error_msg.= $value.'<br>';		
	}
	else 
	{
	    //Add Website		
	   if (count($var['day']) > 0)	   
         foreach ($var['day'] as $v)  $display .= ","."$v".",";               
	   else $display = "";
	   
	   
	    if (count($var['categories']) > 0) { foreach ($var['categories'] as $v)  $scategories .= $v.","; $scategories = rtrim($scategories,","); }               
	    else $scategories = "";
		
		$added = $ads->addWebsite($userinfo['username'],$var['name'],$var['title'],$var['url'],$scategories,array($var['geo_target'],$var['country']),$var['package'],$display,$var['limit'],$var['start']);
		
		if($added && $var['credits'] > 0) $ads->assignWebsiteCredits($userinfo["username"],$added,$var['credits']); 
		if($added) $success_msg = 'Website added';	
	}
	

    break;
  }

}

   $website_credits = number_format($ads->adsCredits($userinfo['username'],'website'),2);
   
   $num_websites =  $ads->getNoAds($userinfo['username'],"websites");
   
   $max_websites = $settings["ads"]["max_websites_".$userinfo['mid']];
	
   $loader = new Loader();
   
   $data= get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'add_website.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('private');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('private');
   
   $loader->displayOutput();