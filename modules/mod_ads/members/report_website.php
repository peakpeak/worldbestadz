<?php
global $system,$db,$userinfo,$settings;

$pagetitle = 'Report Website';

$traffic = $system->importClass('ads');

$var = $system->getVar();

if(!isset($var['wid']) || !$traffic->isWebsite($var['wid'])) $system->invalidRequest();

 $sql = "SELECT *  FROM ".PREFIX."_ads_websites WHERE id='".$db->sql_quote($var['wid'])."' LIMIT 1";
 
        $res = $db->query_db($sql,$print = DEBUG);
		
        $website = $db->fetch_db_array($res);

if($var['submit'])
{

  switch($var['submit'])
  {
     
	/*Add Website*/
	
    case("report this website"):

    if ($var['reason']=='' || $var['wid']=='' )
	{
		$errors[] =  "All fields are required";
	}
	elseif ($var['agree']!=1)
	{
	    $errors[] =  "You must agree to the terms and conditions";
    }
	
	
	if(is_array($errors) && !empty($errors))
	{
		while (list($key,$value) = each($errors)) $error_msg.= $value.'<br>';		
	}
	else 
	{
	    //Report Website			
		$reported = $traffic->reportWebsite($userinfo['username'],$var['wid'],$var['reason']);
		
		if($reported) $success_msg = 'Website is now under review';	
	}
	

    break;
  }

}

 
   $loader = new Loader;
   
   $data= get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'report_website.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('private');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('private');
   
   $loader->displayOutput();
 ?>

 
 
 