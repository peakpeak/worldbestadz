<?php
global $system,$db,$userinfo,$settings;

$ads = $system->importClass('ads');

$var = $system->getVar();

if (!isset($var['wid']))$system->invalidRequest();


if($var['submit'])
{

	  switch($var['submit'])
	  {
		
			case("edit this ad"):
		
			if($var['title']=='')
			{
				$errors[] =  "Add a title";
			}
			 elseif($var['name']=='')
			{
				$errors[] =  "Add a website name";
			}
			elseif($var['url']=='' || !Validate::url($var['url']))
			{
				$errors[] =  "Add a valid URL";
			}
			elseif ($var['agree']!=1)
			{
			   // $errors[] =  "You must agree to the terms and conditions";
			}
			else
			{	  
			   
			   $sql="SELECT name FROM ".PREFIX."_ads_loginads WHERE id!='".$db->sql_quote($var['wid'])."' AND username = '".$userinfo['username']."' AND name='".$db->sql_quote($var['name'])."' LIMIT 1";
			   $res = $db->query_db($sql,$print = DEBUG);
			   if ($db->num_rows($res) > 0)  $errors[] =  "An ad with this name already exist"; 
				 
			}
			
			if(is_array($errors) && !empty($errors))
			{
				while (list($key,$value) = each($errors)) $error_msg.= $value.'<br>';		
			}
			else 
			{
				//Add Website		
				if (count($var['day']) > 0)	   
				  foreach ($var['day'] as $v)  $display .= ","."$v".",";               
				else $display = "";
				$updated = $ads->updateLoginAd($userinfo['username'],$var['wid'],$var['name'],$var['title'],$var['url'],$display,$var['limit']);
				if($updated) $success_msg = 'Login Ad Edited';	
			}
			
		    break;
	   
	  }

}

   $sql = "SELECT *  FROM ".PREFIX."_ads_loginads WHERE username ='".$userinfo['username']."' AND id='".$db->sql_quote($var['wid'])."' LIMIT 1";
   $res = $db->query_db($sql,$print = DEBUG);
   if ($db->num_rows($res) > 0) $loginad = $db->fetch_db_array($res);
   
   $check_sun =((stristr($loginad['display_days'],",Sun,")))? "checked=\"checked\" ": ""; 
   $check_mon =((stristr($loginad['display_days'],",Mon,")))? "checked=\"checked\" ": ""; 
   $check_tue =((stristr($loginad['display_days'],",Tue,")))? "checked=\"checked\" ": ""; 
   $check_wed =((stristr($loginad['display_days'],",Wed,")))? "checked=\"checked\" ": ""; 
   $check_thur =((stristr($loginad['display_days'],",Thu,")))? "checked=\"checked\" ": ""; 
   $check_fri =((stristr($loginad['display_days'],",Fri,")))? "checked=\"checked\" ": ""; 
   $check_sat =((stristr($loginad['display_days'],",Sat,")))? "checked=\"checked\" ": ""; 
  
   $loader = new Loader;
   
   $data= get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'edit_loginad.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('private');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('private');
   
   $loader->displayOutput();