<?php


	session_start();
	
	global $system,$db,$userinfo,$settings;
	
	$pagetitle = 'Login Ads Rotator';
	
	$ads = $system->importClass('ads');
	
	$var = $system->getVar();
	
	
	if ($_POST['viewed'] == 'ok' )
	{
		if($_REQUEST['token'] == $_SESSION['ads_loginads_token'] )$system->redirect(SITEURL.FS.'members');
	}
	else
	{
		$token = md5(uniqid().time());
		$_SESSION['ads_loginads_token'] = $token;
	}

   
    $day = date(",D,");
    $date = date('Y-m-d');
	$credits_per_exposure = $settings['ads']['loginads_credits_per_exposure'];
    $rotator_timer = $settings['ads']['loginads_rotator_timer'];
	$sql = "SELECT * ";
    $sql.= "FROM ".PREFIX."_ads_loginads as w ";
	$sql.= "WHERE credits > 0 ";
	$sql.= "AND (( w.credits_today < w.credits_per_day OR FROM_UNIXTIME(w.last_visit,'%Y-%m-%d') != '".$date."')  OR w.credits_per_day = 0 )";
	$sql.= "AND status = 'Active' ";
	$sql.= "AND display_days LIKE '%".$day."%' ";
	$sql.= " ORDER BY rand() LIMIT 1";		
    $res = $db->query_db($sql,$print = DEBUG);
	
	
	if ($db->num_rows($res) > 0)
    {
	
        $site_show = $db->db_result($res,0,'url');
        $site_id   = $db->db_result($res,0,'id');
        $username = $db->db_result($res,0,'username');
		$last_visit = $db->db_result($res,0,'last_visit');
		$credits_today = $db->db_result($res,0,'credits_today');
        $ads->checkWebsiteCredit($site_id);
		$cr_today = (strftime("%Y/%m/%d",$last_visit) == strftime("%Y/%m/%d")) ? ($credits_today + $credits_per_exposure):$credits_per_exposure;
 
         ////////Up date site Credits//////////////////////////
        $sql = "UPDATE ".PREFIX."_ads_loginads SET 
				credits=credits-".$credits_per_exposure.",
				credits_used=credits_used+".$credits_per_exposure.",
				credits_today=".$cr_today.",
                last_visit='".time()."',
				hits=hits+1
		        WHERE id='".$db->sql_quote($site_id)."' LIMIT 1 "; 	
        $db->query_db($sql,$print = DEBUG);

         ////////////////////////////////////
	    $sql = "SELECT id FROM ".PREFIX."_ads_loginads_statistics
			    WHERE username='".$username."'
			    AND site_id='".$db->sql_quote($site_id)."'
			    AND date = '".$date."' LIMIT 1 ";
		$res = $db->query_db($sql,$print = DEBUG);
				
		if ($db->num_rows($res) > 0)
		{
					/// Update Record 
					$sql = "UPDATE ".PREFIX."_ads_loginads_statistics SET
							hits=hits+1,
							credits=credits+'".$db->sql_quote($credits_per_exposure)."'
							WHERE site_id='".$db->sql_quote($site_id)."' 
							AND date = '".$date."' LIMIT 1";
					$res = $db->query_db($sql,$print = DEBUG);
		}
		else
		{
					
					$sql = "INSERT INTO ".PREFIX."_ads_loginads_statistics
					(username,site_id,hits,date,credits)
					VALUES 
					('".$username."','".$site_id."','1','".$date."','".$db->sql_quote($credits_per_exposure)."' )"; 
					$res = $db->query_db($sql,$print = DEBUG);
		}
		 
		 
		 
     } else  $site_show = $settings['ads']['default_website'];
       $db->free_result($res);
	   
	     
   $loader = new Loader;
   
   $data= get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'loginads_rotator.php';
   
   $loader->setVar($data);
   
   //$loader->mainHeader('private');
   
   $loader->loadOutput($tpl_file);
   
  // $loader->mainFooter('private');
   
   $loader->displayOutput();
   
   
?>

