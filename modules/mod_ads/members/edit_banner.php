<?php

global $system,$db,$userinfo,$settings;

$ads = $system->importClass('ads');

$categories = $ads->getAdCategories();

$packages = $ads->getBannerPackages();

$var = $system->getVar();

if (!isset($var['bid']))$system->invalidRequest();

if($var['submit'])
{

  switch($var['submit'])
  {
    
		case("edit this banner"):
		
	
		if($var['title']=='')
		{
			$errors[] =  "Add a title";
		}
		elseif($var['name']=='')
		{
			$errors[] =  "Add a banner name";
		}
		elseif($var['url']=='' || !Validate::url($var['url']))
		{
			$errors[] =  "Add a valid URL";
		}
		elseif($var['image'] == '' || !Validate::url($var['image']) || !is_array(getimagesize($var['image'])))
		{
			$errors[] =  "Add a valid Image URL";
		}
		elseif (count($var['categories']) < 1 && $settings["ads"]["categorized_ads"])
		{
			$errors[] =  "You must select one category at least";
		}
		elseif ($var['agree']!=1)
		{
			$errors[] =  "You must agree to the terms and conditions";
		}
		else
		{	  
		   $sql="SELECT * FROM ".PREFIX."_ads_banners  WHERE id = '".$db->sql_quote($var['bid'])."'  LIMIT 1";
		   $res = $db->query_db($sql,$print = DEBUG);
		   if ($db->num_rows($res) > 0) 
		   {
				 $row = $db->fetch_db_array($res);
				 if(($var['package'] !=  $row['package'])
				 &&($packages[$var['package']]['type'] == 'PPP' 
				 || $packages[$row['package']]['type'] == 'PPP'))
				 {$errors[] =  "You cannot choose the selected banner section";} 
			 
			
		   }
		  
		   $sql="SELECT name FROM ".PREFIX."_ads_banners WHERE id!='".$db->sql_quote($var['bid'])."' AND username = '".$userinfo['username']."' AND name='".$db->sql_quote($var['name'])."' LIMIT 1";
		   $res = $db->query_db($sql,$print = DEBUG);
		   if ($db->num_rows($res) > 0)  $errors[] =  "A banner with this name already exist"; 
		   
		   $sql="SELECT image FROM ".PREFIX."_ads_banners  WHERE id!='".$var['bid']."' AND username = '".$userinfo['username']."' AND image='".$db->sql_quote($var['image'])."' AND package='".$db->sql_quote($var['package'])."' LIMIT 1";
		   $res = $db->query_db($sql,$print = DEBUG);
		   if ($db->num_rows($res) > 0)  $errors[] =  "This banner already exist";  
		   
		  
		}
		
		if(is_array($errors) && !empty($errors))
		{
			while (list($key,$value) = each($errors)) $error_msg.= $value.'<br>';		
		}
		else 
		{
			//Add Banner		
		   if (count($var['day']) > 0)	   
			 foreach ($var['day'] as $v)  $display .= ","."$v".",";               
		   else $display = "";
		   
			if (count($var['categories']) > 0) { foreach ($var['categories'] as $v)  $scategories .= $v.","; $scategories = rtrim($scategories,","); }               
			else $scategories = "";
			
			$updated = $ads->updateBanner($userinfo['username'],$var['bid'],$var['name'],$var['title'],$var['image'],$var['url'],$scategories,array($var['geo_target'],$var['country']),$var['package'],$display,$var['limit'],$var['start']);
			
			if($updated) $success_msg = 'Banner Edited';		
		}
		
	    break;
   
  }

}

   $sql = "SELECT *  FROM ".PREFIX."_ads_banners WHERE username ='".$userinfo['username']."' AND id='".$db->sql_quote($var['bid'])."' LIMIT 1";
   $res = $db->query_db($sql,$print = DEBUG);
   if ($db->num_rows($res) > 0) $banner = $db->fetch_db_array($res);
   
   $check_sun =((stristr($banner['display_days'],",Sun,")))? "checked=\"checked\" ": ""; 
   $check_mon =((stristr($banner['display_days'],",Mon,")))? "checked=\"checked\" ": ""; 
   $check_tue =((stristr($banner['display_days'],",Tue,")))? "checked=\"checked\" ": ""; 
   $check_wed =((stristr($banner['display_days'],",Wed,")))? "checked=\"checked\" ": ""; 
   $check_thu =((stristr($banner['display_days'],",Thu,")))? "checked=\"checked\" ": ""; 
   $check_fri =((stristr($banner['display_days'],",Fri,")))? "checked=\"checked\" ": ""; 
   $check_sat =((stristr($banner['display_days'],",Sat,")))? "checked=\"checked\" ": ""; 
   
   $selected_categories = explode(",",$banner['category']);

   $loader = new Loader;
   
   $data= get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'edit_banner.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('private');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('private');
   
   $loader->displayOutput();
