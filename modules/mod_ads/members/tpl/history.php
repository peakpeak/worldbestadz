<span  class="form_title">Credit History</span>
<?php if($error_msg){ ?>
<span class="error_message"><?php echo $error_msg;?></span>
<? } ?>
<?php if($success_msg){ ?>
<span class="success_message"><?php echo $success_msg;?></span>
<? } ?>
<div class="white">
  <table cellpadding="0" cellspacing="0" border="0" class="table_2" width="100%">
    <thead>
      <tr>
        <th>#</th>
        <th>Date</th>
        <th>Description</th>
        <th>Type</th>
        <th>Amount</th>
        <th>Balance</th>
      </tr>
    </thead>
    <?php  foreach ($history as $num => $data) : ?>
    <tr>
      <td ><?php echo $num; ?></td>
      <td><?php echo $data['date']; ?></td>
      <td><?php echo $data['transaction']; ?></td>
      <td><?php echo $data['type'];?></td>
      <td><?php echo $data['sign']; ?> <?php echo number_format($data['amount'],2);//sprintf("%01.2f",$data['amount']); ?></td>
      <td><?php echo number_format($data['post_balance'],2);//sprintf("%01.2f",$data['post_balance']); ?></td>
    </tr>
    <?php endforeach; ?>
    <?php if(count($history) == 0) : ?>
    <tr>
      <td colspan="6" align="center"> - no records found -</td>
    </tr>
    <?php endif; ?>
    <?php if($num_rows > $per_page) : ?>
    <tr>
      <td colspan="6" align="center"><?php echo $system->getPaginationString($page,$num_rows,$per_page,3,$system->curPageURL(),"page");?></td>
    </tr>
    <?php endif; ?>
  </table>
</div>
