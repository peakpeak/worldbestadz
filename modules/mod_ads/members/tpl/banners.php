<span  class="form_title">Manage Banners</span>
<?php if($error_msg){ ?>
<span class="error_message"><?php echo $error_msg;?></span>
<? } ?>
<?php if($success_msg){ ?>
<span class="success_message"><?php echo $success_msg;?></span>
<? } ?>
<div align="right"><a href="{#loader=system::url}/members/ads/add_banner">New Banner</a></div>
<br />
<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td width="430" height="200" align="center" valign="top"><table cellspacing=1 cellpadding=2 border=0 width="100%">
        <tr>
          <td><div class="white">
  <table cellspacing=0 cellpadding=0 border=0 width="100%" class="table_1">
    <thead>
      <tr>
        <th colspan="2">Banner Ads Summary</th>
      </tr>
    </thead>
    <tr>
      <td  width="30%">Available Banner Credits</td>
      <td><?php echo $banner_credits;?></td>
    </tr>
    <tr>
      <td>Allowed Banners</td>
      <td><?php echo $max_banners;?></td>
    </tr>
    <tr>
      <td>Total Banners Added</td>
      <td><?php echo $num_banners;?></td>
    </tr>
  </table>
</div>
            <?php  $i = 0;foreach ($banners as $num => $banner) { $i++; ?>
            <div class="white">
              <table cellspacing=1 cellpadding=2 border=0 width="100%" class="table_1">
                <tr>
                  <td style="width:300px; background:#fcfcfc;"><?php echo $banner["name"];?> (<?php echo $packages[$banner["package"]]["name"];?>)</td>
                  <td nowrap style="background:#fcfcfc;"><a href="{#loader=system::url}/members/ads/banner_statistics/bid/<?php echo $banner["id"];?>" id="popup<?php echo $i?>">stats</a> | <a href="{#loader=system::url}/members/ads/edit_banner/bid/<?php echo $banner["id"];?>">edit</a> | <a href="{#loader=system::url}/members/ads/banners/delete/1/bid/<?php echo $banner["id"];?>" onclick="if(!confirm('Do you really want to delete this banner')) return false">delete</a> </td>
                </tr>
                <tr>
                  <td nowrap colspan="2" style="background:#fcfcfc;"><iframe src="<?php echo $banner["image"];?>" width="100%" height="150" align="middle" frameborder="0"  scrolling="auto"></iframe></td>
                </tr>
                <?php  if($packages[$banner["package"]]["type"]  != 'PPP') { ?>
                <tr>
                  <td  nowrap colspan="2" style="background:#fcfcfc;"><a href="<?php echo $banner["url"];?>" target="_blank">&raquo; <?php echo $banner["url"];?></a>
                    <form action="{#loader=system::url}/members/ads/banners" method="post"  class="form_2">
                      <input type="hidden" name="bid" value="<?php echo $banner["id"];?>" />
                      <input type="text" name="credits" value="" size="5"  />
                      <input type="submit" name="submit" value="assign credits" onclick="if(!confirm('Do you really want to assign these impression(s)')) return false"/>
                    </form></td>
                </tr>
                <?php } ?>
                <tr>
                  <td colspan="2" style="border-bottom:1px solid #CCC;"><table cellpadding="0" cellspacing="0" border="0" width="100%">
                      <tr>
                        <?php  if($packages[$banner["package"]]["type"]  == 'PPP') { ?>
                        <td style="background:#DDD;">Days Remaining</td>
                        <td style="background:#DDD;">Days Displayed</td>
                        <?php } else { ?>
                        <td style="background: <?php if($banner["credits"] > 10){ ?>#b6d996<?php }else{?>#FF3300<?php }  ?>; font-weight:bold;">Credits <br />
                          Remaining</td>
                        <td style="background:#DDD;">Credits <br />
                          Used</td>
                        <?php } ?>
                        <td style="background:#DDD;">Total Views</td>
                        <td style="background:#DDD;">Total Clicks</td>
                        <td style="background:#DDD;">Status</td>
                      </tr>
                      <tr>
                        <?php  if($packages[$banner["package"]]["type"]  == 'PPP') { ?>
                        <td><?php echo $banner["rem_days"];?></td>
                        <td><?php echo $banner["run_days"];?></td>
                        <?php } else { ?>
                        <td style="background: <?php if($banner["credits"] > 10){ ?>#d4f4b7<?php }else{?>#febfaf<?php } ?>; font-weight:bold;"><?php echo $banner["credits"];?></td>
                        <td><?php echo $banner["credits_used"];?></td>
                        <?php } ?>
                        <td><?php echo $banner["views"];?></td>
                        <td><?php echo $banner["hits"];?></td>
                        <td><?php echo $banner["status"];?></td>
                      </tr>
                    </table></td>
                </tr>
              </table>
            </div>
            <?php } ?>
          </td>
        </tr>
        <?php if(count($banners) == 0) : ?>
        <tr>
          <td align="center"><div class="white"> - no records found - </div></td>
        </tr>
        <?php endif; ?>
        <?php if($num_rows > $per_page) : ?>
        <tr>
          <td align="center"><?php echo $system->getPaginationString($page,$num_rows,$per_page,3,$system->curPageURL(),"page");?></td>
        </tr>
        <?php endif; ?>
      </table></td>
  </tr>
</table>
<script type="text/javascript">
 
   
	 var first = 0;
	 var last = <?php echo $i;?>;	
	  fancyjQuery(document).ready(function() {
			
			for (i = first; i  <= last; i++) {
			
			fancyjQuery("#popup"+i).fancybox({
				'width'				: '70%',
				'height'			: '100%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
          }
		
		});
  </script>
