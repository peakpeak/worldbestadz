<span  class="form_title">Buy Static Banners</span>

<?php if($error_msg){ ?><span class="error_message"><?php echo $error_msg;?></span><? } ?>
<?php if($success_msg){ ?><span class="success_message"><?php echo $success_msg;?></span><? } ?>

<div align="left">
  <a href="{#loader=system::url}/members/ads/banners">Banners</a> > 
  Buy Static Banners
</div><br />


<div class="white">
		 
        <table cellspacing=0 cellpadding=0 border=0 width="100%" class="table_1">
	      <thead>
          <tr>
            <th colspan="2">Banner Ads Summary</th>
          </tr> 
	     </thead>
	    <tr>
		  <td width="30%">Allowed Banners</td>
		  <td><?php echo $max_banners;?></td>
	    </tr>
		<tr>
		  <td>Total Banners Added</td>
		  <td><?php echo $num_banners;?></td>
	    </tr>
	   </table>
	   
</div>

 <br />

<div class="white">
	
		<form action="" method="post" class="form_1">

			<table cellpadding="0" cellspacing="0" border="0" class="table_1">
			    <thead>
			       <tr><th colspan="2">Buy Static Banner Ads</th></tr>
			    </thead>
				<tr>
					<td  width="30%">Select Ad</td>
					<td><select name="package" id="package" onchange="checklimit();">
					<option value="">-select-</option>
					<?php  foreach($packages as $id => $package){ ?>
					
					<option value="<?php echo $id?>" <?php if($var["package"] == $id) { ?> selected="selected" <?php }?>><?php echo $package["name"];?> - <?php echo $package["length"];?> x <?php echo $package["height"];?> (<?php echo $package["summary"];?>)</option>
					
					<?php } ?></select>  <span id="package_details"></span></td>
				</tr>
				<tr id="b1" class="hidden">
					<td width="30%">Banner Name</td>
					<td><input type="text" name="name"  size="15" maxlength="20" value="<?php echo $var["name"]?>"></td>
				</tr>
				<tr id="b2" class="hidden">
					<td>Banner Title</td>
					<td><input type="text" name="title" size="40" maxlength="150" value="<?php echo $var["title"]?>"> </td>
				</tr>
				<tr id="b3" class="hidden">
					<td>Banner File URL</td>
					<td><input type="text" name="image" size="40" maxlength="150" value="<?php echo $var["image"]?>"> </td>
				</tr>
				<tr id="b4" class="hidden">
					<td>Target URL</td>
					<td><input type="text" name="url" size="40" maxlength="150" value="<?php echo $var["url"]?>"></td>
				</tr>
				
				<?php  if($settings["ads"]["categorized_ads"]) { ?>
				<tr id="b5" class="hidden">
					<td>Category</td>
					<td>
					 <?php $num_data = count($categories);
	                 if($num_data > 0){
	                 $num = 0;
	                 $sort_in = 5;
	                 $tds = ($num_data%$sort_in);
	                 if($tds != 0) $tds = $sort_in - $tds;
	                 ?>
	                 <table cellpadding="0" cellspacing="0"  width="100%">
	                 <tr>
					 <?php  foreach($categories as $id => $value) {
	    
		            if($num%$sort_in == 0) { ?> </tr> <tr><? } ?> 
		 
		             <td><input type="checkbox" name="categories[]" value="<?php echo $id;?>" <?php if($var['categories'][$num] == $id) { ?> checked="checked"<?php }?> onclick='check_categories()'/> &nbsp;<?php echo $value;?></td>
		  
                     <?php $num++;} for($i=0;$i<$tds;$i++){ ?><td >&nbsp; </td><?php } ?>
		 
		             </tr>
					 </table>
	                 <?php } else { ?> No categories found <?php }?>
		 
		          </td>
				</tr>
				<?php } ?>
				<tr id="p1" class="hidden">
						<td >Payment Option</td>
						<td >
						    <select name ="pro">
                            <option value="">- select -</option>
					        <?php  foreach ( $processors as $id => $name) { ?>
					        <option value="<?php echo $id; ?>" <?php if($var['pro'] == $id){?> selected="selected" <?php }?>><?php echo $name;?> - $<?php echo $wallet->userPurchaseBal($userinfo['username'],$id,"USD");?></option> 
					        <?php } ?>	
					        </select>
						</td>
				</tr>
				
				<tr id="b6" class="hidden">
					<td colspan="2">
					    <input type="checkbox"  name="agree" value="1"> I Agree that the above URL (Website) conforms to our rules 
					</td>
				</tr>
				
				<tr id="b7" class="hidden">
					<td colspan="2" align="center"><input type="submit" name="submit" value="Buy banner"></td>
				</tr>
			</table>

		</form>
		<br><br><br>
		
	</div>


<script type="text/javascript"> 
function check_categories() {

 var categories = document.getElementsByName('categories[]');
 var total = 0;
 for (var i = 0; i < categories.length; i++) if(categories[i].checked)total = total +1;
 
 if(total >= 3)
 {
    for (i = 0; i < categories.length; i++){
    if(categories[i].checked == false){categories[i].disabled = true;}
    }
 }
 else
 {
   for (i = 0; i < categories.length; i++)categories[i].disabled = false;   
 }
 
  
}

function checklimit()
{
	
	var packages = new Array();
	var time_period = new Array();
	
	time_period['H'] = 'Hour(s)';
	time_period['D'] = 'Day(s)';
	time_period['W'] = 'Week(s)';
	time_period['M'] = 'Month(s)';
	time_period['Y'] = 'Year(s)';
	
	<?php foreach($packages as $id => $data) { ?>
	packages[<?php echo $id?>] = new Array();
	packages[<?php echo $id?>]['cost_per_period'] = <?php echo $data["cost_per_period"]?>;
	packages[<?php echo $id?>]['duration'] = <?php echo $data["duration"]?>;
	packages[<?php echo $id?>]['period'] = time_period['<?php echo $data["period"]?>'];
	packages[<?php echo $id?>]['total_spots'] = <?php echo $data["spots"]?>;
	packages[<?php echo $id?>]['available_spots'] = '<?php echo $data["available_spots"]?>';
	<?php } ?>
	
	var index = document.getElementById('package').selectedIndex;
    var pid = document.getElementById('package').options[index].value;
    var details = ''; // = '$'+packages[pid]['cost_per_period']+' for '+packages[pid]['duration']+' '+packages[pid]['period']+'. ';

  
	if(packages[pid]['total_spots'] > 0){
	
	   details += packages[pid]['available_spots']+' spot(s) left';
	   
	   if(packages[pid]['available_spots'] > 0){
	
	      document.getElementById('p1').className = "";
		  document.getElementById('b1').className = "";
		  document.getElementById('b2').className = "";
		  document.getElementById('b3').className = "";
		  document.getElementById('b4').className = "";
		 // document.getElementById('b5').className = "";
		  document.getElementById('b6').className = "";
		  document.getElementById('b7').className = "";
		  
	   }
	  
	   
	}else{
	
	      document.getElementById('p1').className = "";
		  document.getElementById('b1').className = "";
		  document.getElementById('b2').className = "";
		  document.getElementById('b3').className = "";
		  document.getElementById('b4').className = "";
		//  document.getElementById('b5').className = "";
		  document.getElementById('b6').className = "";
		  document.getElementById('b7').className = "";
	  
	
	}
	document.getElementById('package_details').innerHTML = details;
	
}

checklimit();
</script>





