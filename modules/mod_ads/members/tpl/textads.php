<span  class="form_title">Manage Text Ads</span>
<?php if($error_msg){ ?>
<span class="error_message"><?php echo $error_msg;?></span>
<? } ?>
<?php if($success_msg){ ?>
<span class="success_message"><?php echo $success_msg;?></span>
<? } ?>
<div align="right"><a href="{#loader=system::url}/members/ads/add_textad">New Text Ad</a></div>
<br />
<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td width="430" height="200" align="center" valign="top"><table cellspacing=1 cellpadding=2 border=0 width="100%">
        <tr>
          <td><div class="white">
			  <table cellspacing=0 cellpadding=0 border=0 width="100%" class="table_1">
				<thead>
				  <tr>
					<th colspan="2">Text Ads Summary</th>
				  </tr>
				</thead>
				<tr>
				  <td  width="30%">Available Text Ad Credits</td>
				  <td><?php echo $textad_credits;?></td>
				</tr>
				<tr>
				  <td>Allowed Text Ads</td>
				  <td><?php echo $max_textads;?></td>
				</tr>
				<tr>
				  <td>Total Text Ads Added</td>
				  <td><?php echo $num_textads;?></td>
				</tr>
			  </table>
			</div>
            <?php  $i = 0;foreach ($textads as $num => $textad) { $i++; ?>
            <div class="white">
              <table cellspacing=1 cellpadding=2 border=0 width="100%" class="table_1">
                <tr>
                  <td style="width:300px; background:#fcfcfc;"><b><?php echo $textad["name"];?></b></td>
                  <td nowrap style="background:#fcfcfc;"><a href="{#loader=system::url}/members/ads/textad_statistics/tid/<?php echo $textad["id"];?>" id="popup<?php echo $i?>">stats</a> | <a href="{#loader=system::url}/members/ads/edit_textad/tid/<?php echo $textad["id"];?>">edit</a> | <a href="{#loader=system::url}/members/ads/textads/delete/1/tid/<?php echo $textad["id"];?>" onclick="if(!confirm('Do you really want to delete this text ad')) return false">delete</a> </td>
                </tr>
                <tr>
                  <td nowrap colspan="2"  style="background:#fcfcfc;"><?php echo $textad["title"];?> <br />
                    <?php echo $textad["body"];?> </td>
                </tr>
                <tr>
                  <td  nowrap colspan="2" style="background:#fcfcfc;"><a href="<?php echo $textad["url"];?>" target="_blank">&raquo; <?php echo $textad["url"];?></a>
                    <form action="{#loader=system::url}/members/ads/textads" method="post" class="form_2">
                      <input type="hidden" name="tid" value="<?php echo $textad["id"];?>" />
                      <input type="text" name="credits" value="" size="5"  />
                      <input type="submit" name="submit" value="assign credits" onclick="if(!confirm('Do you really want to assign these impression(s)')) return false"/>
                    </form></td>
                </tr>
                <tr>
                  <td colspan="2" style="border-bottom:1px solid #CCC;"><table cellpadding="0" cellspacing="0" border="0" width="100%">
                      <tr>
                        <td style="background:<?php if($textad["credits"] > 10){ ?>#b6d996<?php }else{?>#FF3300<?php } ?>; font-weight:bold;">Remaining</td>
                        <td style="background:#DDD;">Used</td>
                        <td style="background:#DDD;">Today</td>
                        <td style="background:#DDD;">Exposures</td>
                        <td style="background:#DDD;">Clicks</td>
                        <td style="background:#DDD;">Status</td>
                      </tr>
                      <tr>
                        <td style="background:<?php if($textad["credits"] > 10){ ?>#d4f4b7<?php }else{?>#febfaf<?php }  ?>; font-weight:bold;"><?php echo $textad["credits"];?></td>
                        <td><?php echo $textad["credits_used"];?></td>
                        <td><?php echo $textad["todayhits"];?></td>
                        <td><?php echo $textad["views"];?></td>
                        <td><?php echo $textad["hits"];?></td>
                        <td><?php echo $textad["status"];?></td>
                      </tr>
                    </table></td>
                </tr>
              </table>
            </div>
            <?php } ?>
          </td>
        </tr>
        <?php if(count($textads) == 0) : ?>
        <tr>
          <td align="center"><div class="white"> - no records found - </div></td>
        </tr>
        <?php endif; ?>
        <?php if($num_rows > $per_page) : ?>
        <tr>
          <td align="center"><?php echo $system->getPaginationString($page,$num_rows,$per_page,3,$system->curPageURL(),"page");?></td>
        </tr>
        <?php endif; ?>
      </table></td>
  </tr>
</table>
<script type="text/javascript">
 
   
	 var first = 0;
	 var last = <?php echo $i;?>;	
	  fancyjQuery(document).ready(function() {
			
			for (i = first; i  <= last; i++) {
			
			fancyjQuery("#popup"+i).fancybox({
				'width'				: '70%',
				'height'			: '100%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
          }
		
		});
  </script>
