<span class="form_title">Directory Listings</span>
<?php if($error_msg){ ?>
<span class="error_message"><?php echo $error_msg;?></span>
<? } ?>
<?php if($success_msg){ ?>
<span class="success_message"><?php echo $success_msg;?></span>
<? } ?>
<div align="right"><a href="{#loader=system::url}/members/ads/add_listing">New Listing</a></div>
<br />
<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td width="430" height="200" align="center" valign="top"><div class="white">
        <table  cellspacing=1 cellpadding=2 border=0 width="100%" class="table_2">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>URL</th>
              <th>Views</th>
              <th>Clicks</th>
              <th>Status</th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <?php  $i = 0; foreach ($listings as $num => $data) { $i++;?>
          <tr>
            <td height="25" valign="middle" align="center"><?php echo $num; ?></td>
            <td height="25" valign="middle"><?php echo $data['name']; ?></td>
            <td height="25" valign="middle"><a href="<?php echo $data['url']; ?>" target="_blank"><?php echo $data['url']; ?></a></td>
            <td height="25" valign="middle"><?php echo $data['views']; ?></td>
            <td height="25" valign="middle"><?php echo $data['hits']; ?></td>
            <td height="25" valign="middle"><?php echo $data['status']; ?> </td>
            <td height="25" align="middle" nowrap="nowrap"><b><a href="{#loader=system::url}/members/ads/edit_listing/lid/<?php echo $data['id']; ?>">Edit</a></b> | <b><a href="{#loader=system::url}/members/ads/listings/delete/1/lid/<?php echo $data['id']; ?>" onclick="if(!confirm('Do you really want to delete this listing')) return false">Delete</a></b></td>
          </tr>
          <?php } ?>
          <?php if(count($listings) == 0) : ?>
          <tr>
            <td colspan="8" height="25" valign="middle"  align="center" > - no records found - </td>
          </tr>
          <?php endif; ?>
          <?php if($num_rows > $per_page) : ?>
          <tr>
            <td colspan="8" height="25" valign="middle" align="center" ><?php echo $system->getPaginationString($page,$num_rows,$per_page,3,$system->curPageURL(),"page");?></td>
          </tr>
          <?php endif; ?>
        </table>
      </div></td>
  </tr>
</table>
<script type="text/javascript">
 
   
	 var first = 0;
	 var last = <?php echo $i;?>;	
	  fancyjQuery(document).ready(function() {
			
			for (i = first; i  <= last; i++) {
			
			fancyjQuery("#popup"+i).fancybox({
				'width'				: '60%',
				'height'			: '100%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
          }
		
		});
  </script>
