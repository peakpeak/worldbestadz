<span  class="form_title">Report Website</span>
<?php if($error_msg){ ?>
<span class="error_message"><?php echo $error_msg;?></span>
<? } ?>
<?php if($success_msg){ ?>
<span class="success_message"><?php echo $success_msg;?></span>
<? } ?>
<div class="white">
  <form action="" method="post" class="form_1">
    <input type="hidden" name="wid" value="<?php echo $website["id"];?>" />
    <table cellpadding="0" cellspacing="0" border="0" class="table_1">
      <thead>
        <tr>
          <th colspan="2">Report Website</th>
        </tr>
      </thead>
      <tr>
        <td>Website</td>
        <td><b><?php echo $website["title"];?></b> (<?php echo $website["url"];?>)</td>
      </tr>
      <tr>
        <td>Additional Comments</td>
        <td><textarea name="reason" cols="45" rows="10"></textarea></td>
      </tr>
      <tr>
        <td colspan="2"><input type="checkbox"  name="agree" value="1">
          I Agree to terms of reporting this website </td>
      </tr>
      <tr>
        <td colspan="2" align="center"><input type="submit" name="submit" value="report this website"></td>
      </tr>
    </table>
  </form>
</div>
