<span  class="form_title">Add Banner</span>
<?php if($error_msg){ ?>
<span class="error_message"><?php echo $error_msg;?></span>
<? } ?>
<?php if($success_msg){ ?>
<span class="success_message"><?php echo $success_msg;?></span>
<? } ?>
<div align="left"> <a href="{#loader=system::url}/members/ads/banners">Banners</a> > 
  New Banner </div>
<br />
<div class="white">
  <table cellspacing=0 cellpadding=0 border=0 width="100%" class="table_1">
    <thead>
      <tr>
        <th colspan="2">Banner Ads Summary</th>
      </tr>
    </thead>
    <tr>
      <td  width="30%">Available Banner Credits</td>
      <td><?php echo $banner_credits;?></td>
    </tr>
    <tr>
      <td>Allowed Banners</td>
      <td><?php echo $max_banners;?></td>
    </tr>
    <tr>
      <td>Total Banners Added</td>
      <td><?php echo $num_banners;?></td>
    </tr>
  </table>
</div>
<br />
<div class="white">
  <form action="" method="post" class="form_1">
    <table cellpadding="0" cellspacing="0" border="0" class="table_1">
      <thead>
        <tr>
          <th colspan="2">Add Banner</th>
        </tr>
      </thead>
      <tr>
        <td width="30%">Banner Name</td>
        <td><input type="text" name="name"  size="15" maxlength="20" value="<?php echo $var["name"]?>"></td>
      </tr>
      <tr>
        <td>Banner Title</td>
        <td><input type="text" name="title" size="40" maxlength="150" value="<?php echo $var["title"]?>">
        </td>
      </tr>
      <tr>
        <td>Banner File URL</td>
        <td><input type="text" name="image" size="40" maxlength="150" value="<?php echo $var["image"]?>">
        </td>
      </tr>
      <tr>
        <td>Target URL</td>
        <td><input type="text" name="url" size="40" maxlength="150" value="<?php echo $var["url"]?>"></td>
      </tr>
      <tr>
        <td>Banner Section</td>
        <td><select name="package" id="package" onchange="checklimit();">
            <option value="">-select-</option>
            <?php  foreach($packages as $id => $package){ ?>
            <option value="<?php echo $id?>" <?php if($var["package"] == $id) { ?> selected="selected" <?php }?>><?php echo $package["name"];?> - <?php echo $package["length"];?> x <?php echo $package["height"];?></option>
            <?php } ?>
          </select>
          <span id="package_details"></span></td>
      </tr>
      <tr id="r_ppp">
        <td >Payment Option</td>
        <td ><select name ="pro">
            <option value="">- select -</option>
            <?php  foreach ( $processors as $id => $name) { ?>
            <option value="<?php echo $id; ?>" <?php if($var['pro'] == $id){?> selected="selected" <?php }?>><?php echo $name;?> - $<?php echo $wallet->userPurchaseBal($userinfo['username'],$id,"USD");?></option>
            <?php } ?>
          </select>
        </td>
      </tr>
      <?php  if($settings["ads"]["categorized_ads"]) { ?>
      <tr>
        <td>Category</td>
        <td><?php $num_data = count($categories);
	                 if($num_data > 0){
	                 $num = 0;
	                 $sort_in = 5;
	                 $tds = ($num_data%$sort_in);
	                 if($tds != 0) $tds = $sort_in - $tds;
	                 ?>
          <table cellpadding="0" cellspacing="0"  width="100%">
            <tr>
              <?php  foreach($categories as $id => $value) {
	    
		            if($num%$sort_in == 0) { ?>
            </tr>
            <tr>
              <? } ?>
              <td><input type="checkbox" name="categories[]" value="<?php echo $id;?>" <?php if($var['categories'][$num] == $id) { ?> checked="checked"<?php }?> onclick='check_categories()'/>
                &nbsp;<?php echo $value;?></td>
              <?php $num++;} for($i=0;$i<$tds;$i++){ ?>
              <td >&nbsp;</td>
              <?php } ?>
            </tr>
          </table>
          <?php } else { ?>
          No categories found
          <?php }?>
        </td>
      </tr>
      <?php } ?>
      <!--<tr>
					<td>Start date</td>
					<td><script>$(function() { $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });});</script><input type="text" name="start" size="15" id="datepicker" maxlength="20" value=""></td>
				</tr>-->
      <tr id="r_ppvc_1">
        <td>Days to display ads</td>
        <td><table border="0" cellpadding="0" cellspacing="2" width="250">
            <tbody>
              <tr>
                <td align="center" width="14%">Sun<br>
                  <input name="day[]" type="checkbox"  value="Sun" <?php if(in_array('Sun',$var['day'])){?> checked="checked" <?php }?>/></td>
                <td align="center" width="14%">Mon<br>
                  <input name="day[]" type="checkbox"  value="Mon" <?php if(in_array('Mon',$var['day'])){?> checked="checked" <?php }?>/></td>
                <td align="center" width="14%">Tue<br>
                  <input name="day[]" type="checkbox"  value="Tue"<?php if(in_array('Tue',$var['day'])){?> checked="checked" <?php }?> /></td>
                <td align="center" width="14%">Wed<br>
                  <input name="day[]" type="checkbox"  value="Wed" <?php if(in_array('Wed',$var['day'])){?> checked="checked" <?php }?>/></td>
                <td align="center" width="14%">Thu<br>
                  <input name="day[]" type="checkbox"  value="Thu" <?php if(in_array('Thu',$var['day'])){?> checked="checked" <?php }?>/></td>
                <td align="center" width="14%">Fri<br>
                  <input name="day[]" type="checkbox"  value="Fri" <?php if(in_array('Fri',$var['day'])){?> checked="checked" <?php }?>/></td>
                <td align="center" width="14%">Sat<br>
                  <input name="day[]" type="checkbox"  value="Sat"  <?php if(in_array('Sat',$var['day'])){?> checked="checked" <?php }?>/></td>
              </tr>
            </tbody>
          </table></td>
      </tr>
      <tr id="r_ppvc_2">
        <td>Assign credits</td>
        <td><input type="text" name="credits" size="15" maxlength="20" value="<?php echo $var["credits"] ?>"></td>
      </tr>
      <tr id="r_ppvc_3">
        <td>Credit limit per day</td>
        <td><input type="text" name="limit" size="15" maxlength="20" value="<?php echo $var["limit"] ?>">
          Leave blank for no limit</td>
      </tr>
      <tr>
        <td colspan="2"><input type="checkbox"  name="agree" value="1">
          I Agree that the above URL (Website) conforms to our rules </td>
      </tr>
      <tr>
        <td colspan="2" align="center"><input type="submit" name="submit" value="add this banner"></td>
      </tr>
    </table>
  </form>
  <br>
  <br>
  <br>
</div>
<script type="text/javascript"> 
function check_categories() {

 var categories = document.getElementsByName('categories[]');
 var total = 0;
 for (var i = 0; i < categories.length; i++) if(categories[i].checked)total = total +1;
 
 if(total >= 3)
 {
    for (i = 0; i < categories.length; i++){
    if(categories[i].checked == false){categories[i].disabled = true;}
    }
 }
 else
 {
   for (i = 0; i < categories.length; i++)categories[i].disabled = false;   
 }
 
  
}

function checklimit()
{
	
	var packages = new Array();
	var time_period = new Array();
	
	time_period['H'] = 'Hour(s)';
	time_period['D'] = 'Day(s)';
	time_period['W'] = 'Week(s)';
	time_period['M'] = 'Month(s)';
	time_period['Y'] = 'Year(s)';
	
	<?php foreach($packages as $id => $data) { ?>
	packages[<?php echo $id?>] = new Array();
	packages[<?php echo $id?>]['type'] = '<?php echo $data["type"]?>';
	packages[<?php echo $id?>]['imp_per_view'] = <?php echo $data["credits_per_exposure"]?>;
	packages[<?php echo $id?>]['imp_per_click'] = <?php echo $data["credits_per_click"]?>;
	packages[<?php echo $id?>]['cost_per_period'] = <?php echo $data["cost_per_period"]?>;
	packages[<?php echo $id?>]['duration'] = <?php echo $data["duration"]?>;
	packages[<?php echo $id?>]['period'] = time_period['<?php echo $data["period"]?>'];
	packages[<?php echo $id?>]['total_spots'] = <?php echo $data["spots"]?>;
	packages[<?php echo $id?>]['available_spots'] = '<?php echo $data["available_spots"]?>';
	<?php } ?>
	var details;
	var index = document.getElementById('package').selectedIndex;
    var pid = document.getElementById('package').options[index].value;
	if(pid == '') {
	   document.getElementById('r_ppp').className = "hidden";
	   return;
	} 
	else if(packages[pid]['type'] == 'PPV')
	{
	   document.getElementById('r_ppvc_1').className = "";
	   document.getElementById('r_ppvc_2').className = "";
	   document.getElementById('r_ppvc_3').className = "";
	   document.getElementById('r_ppp').className = "hidden";
	   details = packages[pid]['imp_per_view']+' Credit(s) Per Exposure. ';
	}
	else if(packages[pid]['type'] == 'PPC')
	{
	  document.getElementById('r_ppvc_1').className = "";
	  document.getElementById('r_ppvc_2').className = "";
	  document.getElementById('r_ppvc_3').className = "";
	  document.getElementById('r_ppp').className = "hidden";
	  details = packages[pid]['imp_per_click']+' Credit(s) Per Click. ';
	}
	else if(packages[pid]['type'] == 'PPP')
	{
       details = '$'+packages[pid]['cost_per_period']+' for '+packages[pid]['duration']+' '+packages[pid]['period']+'. ';
	   document.getElementById('r_ppp').className = "";
	   document.getElementById('r_ppvc_1').className = "hidden";
	   document.getElementById('r_ppvc_2').className = "hidden";
	   document.getElementById('r_ppvc_3').className = "hidden";
	}
	

	
	
	
	if(packages[pid]['total_spots'] > 0)
	{
	  details += packages[pid]['available_spots']+' spot(s) left';
	}
	
	document.getElementById('package_details').innerHTML = details;
	
}

checklimit();
</script>
