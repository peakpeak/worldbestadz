<span  class="form_title">Buy Credits</span>
<?php if($error_msg){ ?>
<span class="error_message"><?php echo $error_msg;?></span>
<? } ?>
<?php if($success_msg){ ?>
<span class="success_message"><?php echo $success_msg;?></span>
<? } ?>
<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td width="430" height="200" align="center" valign="top"><div class="white">
        <form class="form_2">
          <table cellspacing=1 cellpadding=2 border=0 width="100%" class="table_2">
            <thead>
              <tr>
                <th>Package</th>
                <th>Credits</th>
                <!--<th># Website <br />Credits</th>
						<th># Banner <br />Credits</th>
						<th># Text Ads <br />Credits</th>-->
                <th>Cost</th>
                <th>Payment Method</th>
                <th>Action</th>
              </tr>
            </thead>
            <?php  foreach ($packages as $pnum => $pdata) { 
					        $cost = Ads::findCreditPackageCost($pdata['id'],$userinfo['mid']);
							$buy_link = '<a href="#" onclick="buy_credit_package('.$pdata["id"].')">Buy Now</a>';
							$upgrade_link = '<a href="#">Upgrade to Buy</a>';
					?>
            <tr>
              <td><?php echo $pdata['name']?></td>
              <td><?php 
							    if($pdata['website_credits'] > 0) echo $pdata['website_credits'].' website credit(s)<br>';
							    if($pdata['banner_credits'] > 0) echo $pdata['banner_credits'].' banner credit(s)<br>';
							    if($pdata['textad_credits'] > 0) echo $pdata['textad_credits'].' text ads credit(s)<br>';
							    if($pdata['ptc_credits'] > 0) echo $pdata['ptc_credits'].' ptc credit(s)<br>';
								if($pdata['loginads_credits'] > 0) echo $pdata['loginads_credits'].' login ads credit(s)<br>';
				?>
              </td>
              <td>$<?php echo $pdata['price']?></td>
              <td valign="bottom"><select name ="processor[<?php echo $pdata['id']?>]" id="pro_<?php echo $pdata['id']?>" >
                  <option value="">- select -</option>
                  <?php  foreach ( $pdata['processors'] as $pid) { ?>
                  <option value="<?php echo $pid; ?>"><?php echo $processors[$pid];?> - $<?php echo $wallet->userPurchaseBal($userinfo['username'],$pid,"USD");?></option>
                  <?php } ?>
                </select>
              </td>
              <td><strong><?php echo (Ads::findCreditPackageCost($pdata['id'],$userinfo['mid']))?$buy_link:$upgrade_link;?></strong></td>
            </tr>
            <?php } ?>
            <?php if(count($packages) == 0) { ?>
            <tr>
              <td colspan="7" align="center"> no records found </td>
            </tr>
            <?php } ?>
          </table>
        </form>
      </div></td>
  </tr>
</table>
<script type="text/javascript">

function buy_credit_package(id)
{
   var proID = document.getElementById('pro_'+id).value;
   if(proID == '') {alert('Choose a valid payment option');return false;}
   if(confirm('Do you really want to purchase this credit package?'))
   post_to_url('{#loader=system::url}/members/ads/buy_credits',{'buy_now':'y','pid':id,'proid':proID});
   
  
}

function post_to_url(path, params, method) {
    method = method || "post";
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);
            form.appendChild(hiddenField);
         }
    }

    document.body.appendChild(form);
    form.submit();
}


</script>
