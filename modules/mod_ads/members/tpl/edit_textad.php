<span  class="form_title">Edit Text Ad</span>
<?php if($error_msg){ ?>
<span class="error_message"><?php echo $error_msg;?></span>
<? } ?>
<?php if($success_msg){ ?>
<span class="success_message"><?php echo $success_msg;?></span>
<? } ?>
<div align="left"> <a href="{#loader=system::url}/members/ads/textads">Text Ads</a> > 
  Edit Text Ad (<?php echo $textad["name"];?>) </div>
<br />
<div class="white">
  <form action="" method="post" class="form_1">
    <input type="hidden" name="tid" value="<?php echo $textad["id"];?>" />
    <table cellpadding="0" cellspacing="0" border="0" class="table_1">
      <thead>
        <tr>
          <th colspan="2">Edit Text Ad</th>
        </tr>
      </thead>
      <tr>
        <td>Text Ad Name</td>
        <td><input type="text" name="name"  size="15" maxlength="20" value="<?php echo $textad["name"];?>"></td>
      </tr>
      <tr>
        <td>Text Ad Title</td>
        <td><input type="text" id="title" name="title" size="40" maxlength="150" value="<?php echo $textad["title"];?>" onkeyup="checklimit();" onkeydown="checklimit();">
          <input readonly type="text" id="title_count" size="3" value="">
          characters left.</td>
      </tr>
      <tr>
        <td>Text Ad Body</td>
        <td><textarea id="body" name="body"  cols="37" onkeyup="checklimit();" onkeydown="checklimit();"><?php echo $textad["body"];?></textarea>
          <input readonly type="text" id="body_count" size="3" value="">
          characters left.</td>
      </tr>
      <tr>
        <td>Target URL</td>
        <td><input type="text" name="url" size="40" maxlength="150" value="<?php echo $textad["url"];?>"></td>
      </tr>
      <tr>
        <td>Text Ad Section</td>
        <td><select name="package" id="package" onchange="checklimit();">
            <?php  foreach($packages as $id => $package){ ?>
            <option value="<?php echo $id?>" <?php if($textad["package"]==  $id){ ?>  selected="selected" <?php } ?>><?php echo $package["name"];?></option>
            <?php } ?>
          </select>
          <span id="package_details"></span> </td>
      </tr>
      <?php  if($settings["ads"]["categorized_ads"]) { ?>
      <tr>
        <td>Category</td>
        <td><?php $num_data = count($categories);
	                 if($num_data > 0){
	                 $num = 0;
	                 $sort_in = 5;
	                 $tds = ($num_data%$sort_in);
	                 if($tds != 0) $tds = $sort_in - $tds;
	                 ?>
          <table cellpadding="0" cellspacing="0"  width="100%">
            <tr>
              <?php  foreach($categories as $id => $value) {
	    
		            if($num%$sort_in == 0) { ?>
            </tr>
            <tr>
              <? } ?>
              <td><input type="checkbox" name="categories[]" value="<?php echo $id;?>" <?php if(in_array($id,$selected_categories)) { ?> checked="checked"<?php }?> onclick='check_categories()'/>
                &nbsp;<?php echo $value;?></td>
              <?php $num++;} for($i=0;$i<$tds;$i++){ ?>
              <td >&nbsp;</td>
              <?php } ?>
            </tr>
          </table>
          <?php } else { ?>
          No categories found
          <?php }?>
        </td>
      </tr>
      <?php } ?>
      <!--<tr>
					<td>Start date</td>
					<td><script>$(function() { $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });});</script><input type="text" name="start" size="15" id="datepicker" maxlength="20" value="<?php echo $textad["startdate"];?>"></td>
				</tr>-->
      <tr>
        <td>Days to display ads</td>
        <td><table border="0" cellpadding="0" cellspacing="2" width="250">
            <tbody>
              <tr>
                <td align="center" width="14%">Sun<br>
                  <input name="day[]" type="checkbox"  value="Sun" <?php echo $check_sun; ?>/></td>
                <td align="center" width="14%">Mon<br>
                  <input name="day[]" type="checkbox"  value="Mon" <?php echo $check_mon; ?>/></td>
                <td align="center" width="14%">Tue<br>
                  <input name="day[]" type="checkbox"  value="Tue" <?php echo $check_tue; ?>/></td>
                <td align="center" width="14%">Wed<br>
                  <input name="day[]" type="checkbox"  value="Wed" <?php echo $check_wed; ?>/></td>
                <td align="center" width="14%">Thu<br>
                  <input name="day[]" type="checkbox"  value="Thu" <?php echo $check_thur; ?> /></td>
                <td align="center" width="14%">Fri<br>
                  <input name="day[]" type="checkbox"  value="Fri" <?php echo $check_fri; ?>/></td>
                <td align="center" width="14%">Sat<br>
                  <input name="day[]" type="checkbox"  value="Sat" <?php echo $check_sat; ?>/></td>
              </tr>
            </tbody>
          </table></td>
      </tr>
      <tr>
        <td>Credit limit per day</td>
        <td><input type="text" name="limit" size="15" maxlength="20" value="<?php echo $textad["credits_per_day"];?>">
          <span class="sub_note_2">enter 0 for no limit</span></td>
      </tr>
      <tr>
        <td colspan="2"><input type="checkbox"  name="agree" value="1">
          I Agree that the above URL (Website) conforms to our rules </td>
      </tr>
      <tr>
        <td colspan="2" align="center"><input type="submit" name="submit" value="edit this text ad"></td>
      </tr>
    </table>
  </form>
</div>
<script type="text/javascript"> 
function check_categories() {

 var categories = document.getElementsByName('categories[]');
 var total = 0;
 for (var i = 0; i < categories.length; i++) if(categories[i].checked)total = total +1;
 
 if(total >= 3)
 {
    for (i = 0; i < categories.length; i++){
    if(categories[i].checked == false){categories[i].disabled = true;}
    }
 }
 else
 {
   for (i = 0; i < categories.length; i++)categories[i].disabled = false;   
 }
 
  
}
function checklimit()
{
	
	var packages = new Array();
	<?php foreach($packages as $id => $data) { ?>
	packages[<?php echo $id?>] = new Array();
	packages[<?php echo $id?>]['title_limit'] = <?php echo $data["max_title"]?>;
	packages[<?php echo $id?>]['body_limit'] = <?php echo $data["max_body"]?>;
	packages[<?php echo $id?>]['credits'] = <?php echo $data["credits_per_exposure"]?>;
	<?php } ?>
	var index = document.getElementById('package').selectedIndex;
    var pid = document.getElementById('package').options[index].value;
	var t_title = document.getElementById('title');
	var t_body = document.getElementById('body');
	
	//alert(packages[pid]['title_limit']);
	//alert(field);
	
	if (t_title.value.length > packages[pid]['title_limit']) {
		t_title.value = t_title.value.substring(0, packages[pid]['title_limit']);
	} else {
		document.getElementById('title_count').value = packages[pid]['title_limit'] - t_title.value.length;
	}
	
	if (t_body.value.length > packages[pid]['body_limit']) {
		t_body.value = t_body.value.substring(0, packages[pid]['body_limit']);
	} else {
		document.getElementById('body_count').value = packages[pid]['body_limit'] - t_body.value.length;
	}
	document.getElementById('package_details').innerHTML = packages[<?php echo $id?>]['credits']+' Credit(s) Per Exposure';
	
}
check_categories();
</script>
