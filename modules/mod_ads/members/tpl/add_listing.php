<span  class="form_title">Add Listing</span>
<?php if($error_msg){ ?>
<span class="error_message"><?php echo $error_msg;?></span>
<? } ?>
<?php if($success_msg){ ?>
<span class="success_message"><?php echo $success_msg;?></span>
<? } ?>
<div align="left"> <a href="{#loader=system::url}/members/ads/listings">Directory Listings</a> > 
  New Listing </div>
<br />
<div class="white">
  <form action="" method="post" class="form_1">
    <table cellpadding="0" cellspacing="0" border="0" class="table_1">
      <thead>
        <tr>
          <th colspan="2">Add Directory Listing</th>
        </tr>
      </thead>
      <tr>
        <td>Website Name</td>
        <td><input type="text" name="name"  size="15" maxlength="20" value="<?php echo $var["name"]?>"></td>
      </tr>
      <tr>
        <td>Title</td>
        <td><input type="text" id="title" name="title" size="40" maxlength="150" value="<?php echo $var["title"]?>"  onkeyup="checklimit();" onkeydown="checklimit();">
          <input readonly type="text" id="title_count" size="3" value="">
          characters left.</td>
      </tr>
      <tr>
        <td>Short Decription</td>
        <td><textarea id="description" name="description" cols="37" onkeyup="checklimit();" onkeydown="checklimit();"><?php echo $var["description"]?></textarea>
          <input readonly type="text" id="description_count" size="3" value="">
          characters left.</td>
      </tr>
      <?php  if($settings["ads"]["categorized_ads"]) { ?>
      <tr>
        <td>Category</td>
        <td><?php $num_data = count($categories);
	                 if($num_data > 0){
	                 $num = 0;
	                 $sort_in = 5;
	                 $tds = ($num_data%$sort_in);
	                 if($tds != 0) $tds = $sort_in - $tds;
	                 ?>
          <table cellpadding="0" cellspacing="0"  width="100%">
            <tr>
              <?php  foreach($categories as $id => $value) {
	    
		            if($num%$sort_in == 0) { ?>
            </tr>
            <tr>
              <? } ?>
              <td><input type="checkbox" name="categories[]" value="<?php echo $id;?>" <?php if($var['categories'][$num] == $id) { ?> checked="checked"<?php }?> onclick='check_categories()'/>
                &nbsp;<?php echo $value;?></td>
              <?php $num++;} for($i=0;$i<$tds;$i++){ ?>
              <td >&nbsp;</td>
              <?php } ?>
            </tr>
          </table>
          <?php } else { ?>
          No categories found
          <?php }?>
        </td>
      </tr>
      <?php }  ?>
      <tr>
        <td>Website URL</td>
        <td><input type="text" name="url" size="40" maxlength="150" value="<?php echo $var["url"]?>"></td>
      </tr>
      <tr>
        <td>Banner URL (468X60)</td>
        <td><input type="text" name="image" size="40" maxlength="150" value="<?php echo $var["image"]?>"></td>
      </tr>
      <tr>
        <td colspan="2"><input type="checkbox"  name="agree" value="1" >
          I Agree that the above website conforms to our rules </td>
      </tr>
      <tr>
        <td colspan="2" align="center"><input type="submit" name="submit" value="add this listing"></td>
      </tr>
    </table>
  </form>
</div>
<script type="text/javascript"> 
function check_categories() {

 var categories = document.getElementsByName('categories[]');
 var total = 0;
 for (var i = 0; i < categories.length; i++) if(categories[i].checked)total = total +1;
 
 if(total >= 3)
 {
    for (i = 0; i < categories.length; i++){
    if(categories[i].checked == false){categories[i].disabled = true;}
    }
 }
 else
 {
   for (i = 0; i < categories.length; i++)categories[i].disabled = false;   
 }
 
  
}


function checklimit()
{
	
	var packages = new Array();

	packages[0] = new Array();
	packages[0]['title_limit'] = <?php echo $listing["max_title"];?>;
	packages[0]['description_limit'] = <?php echo $listing["max_description"];?>;

	
	
    var pid = 0;
	var t_title = document.getElementById('title');
	var t_description = document.getElementById('description');
	
	//alert(packages[pid]['title_limit']);
	//alert(field);
	
	if (t_title.value.length > packages[pid]['title_limit']) {
		t_title.value = t_title.value.substring(0, packages[pid]['title_limit']);
	} else {
		document.getElementById('title_count').value = packages[pid]['title_limit'] - t_title.value.length;
	}
	
	if (t_description.value.length > packages[pid]['description_limit']) {
		t_description.value = t_description.value.substring(0, packages[pid]['description_limit']);
	} else {
		document.getElementById('description_count').value = packages[pid]['description_limit'] - t_description.value.length;
	}
	//document.getElementById('package_details').innerHTML = packages[<?php //echo $id?>]['credits']+' Credit(s) Per Exposure';
	
}
</script>
