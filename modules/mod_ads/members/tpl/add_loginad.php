<span  class="form_title">Add Login Ad</span>
<?php if($error_msg){ ?>
<span class="error_message"><?php echo $error_msg;?></span>
<? } ?>
<?php if($success_msg){ ?>
<span class="success_message"><?php echo $success_msg;?></span>
<? } ?>
<div align="left"> <a href="{#loader=system::url}/members/ads/loginads">Login Ads</a> > 
  New Login Ad </div>
<br />
<div class="white">
  <table cellspacing=0 cellpadding=0 border=0 width="100%" class="table_1">
    <thead>
      <tr>
        <th colspan="2">Login Ads Summary</th>
      </tr>
    </thead>
    <tr>
      <td  width="30%">Available Login Ads Credits</td>
      <td><?php echo $loginads_credits;?></td>
    </tr>
    <tr>
      <td>Allowed Websites</td>
      <td><?php echo $max_websites;?></td>
    </tr>
    <tr>
      <td>Total Website Added</td>
      <td><?php echo $num_websites;?></td>
    </tr>
  </table>
</div>
<br />
<div class="white">
  <form action="" method="post" class="form_1">
    <table cellpadding="0" cellspacing="0" border="0" class="table_1">
      <thead>
        <tr>
          <th colspan="2">Edit Login Ad</th>
        </tr>
      </thead>
      <tr>
        <td>Website Name</td>
        <td><input type="text" name="name"  size="15" maxlength="20" value="<?php echo $var["name"];?>"></td>
      </tr>
      <tr>
        <td>Website Title</td>
        <td><input type="text" name="title" size="40" maxlength="150" value="<?php echo $var["title"];?>"></td>
      </tr>
      <tr>
        <td>URL</td>
        <td><input type="text" name="url" size="40" maxlength="150" value="<?php echo $var["url"];?>"></td>
      </tr>
      <tr>
        <td>Days to display ads</td>
        <td><table border="0" cellpadding="0" cellspacing="2" width="250">
            <tbody>
              <tr>
                <td align="center" width="14%">Sun<br>
                  <input name="day[]" type="checkbox"  value="Sun" <?php if(in_array('Sun',$var['day'])){?> checked="checked" <?php }?>/></td>
                <td align="center" width="14%">Mon<br>
                  <input name="day[]" type="checkbox"  value="Mon" <?php if(in_array('Mon',$var['day'])){?> checked="checked" <?php }?>/></td>
                <td align="center" width="14%">Tue<br>
                  <input name="day[]" type="checkbox"  value="Tue"<?php if(in_array('Tue',$var['day'])){?> checked="checked" <?php }?> /></td>
                <td align="center" width="14%">Wed<br>
                  <input name="day[]" type="checkbox"  value="Wed" <?php if(in_array('Wed',$var['day'])){?> checked="checked" <?php }?>/></td>
                <td align="center" width="14%">Thu<br>
                  <input name="day[]" type="checkbox"  value="Thu" <?php if(in_array('Thu',$var['day'])){?> checked="checked" <?php }?>/></td>
                <td align="center" width="14%">Fri<br>
                  <input name="day[]" type="checkbox"  value="Fri" <?php if(in_array('Fri',$var['day'])){?> checked="checked" <?php }?>/></td>
                <td align="center" width="14%">Sat<br>
                  <input name="day[]" type="checkbox"  value="Sat"  <?php if(in_array('Sat',$var['day'])){?> checked="checked" <?php }?>/></td>
              </tr>
            </tbody>
          </table></td>
      </tr>
      <tr id="r_ppvc_2">
        <td>Assign credits</td>
        <td><input type="text" name="credits" size="15" maxlength="20" value="<?php echo $var["credits"] ?>"></td>
      </tr>
      <tr id="r_ppvc_3">
        <td>Credit limit per day</td>
        <td><input type="text" name="limit" size="15" maxlength="20" value="<?php echo $var["limit"] ?>">
          enter 0 for no limit</td>
      </tr>
      <tr>
        <td colspan="2"><input type="checkbox"  name="agree" value="1">
          I Agree that the above URL (Website) conforms to our rules </td>
      </tr>
      <tr>
        <td colspan="2" align="center"><input type="submit" name="submit" value="add this ad"></td>
      </tr>
    </table>
  </form>
  <br>
  <br>
  <br>
</div>
<script type="text/javascript"> 
function check_categories() {

 var categories = document.getElementsByName('categories[]');
 var total = 0;
 for (var i = 0; i < categories.length; i++) if(categories[i].checked)total = total +1;
 
 if(total >= 3)
 {
    for (i = 0; i < categories.length; i++){
    if(categories[i].checked == false){categories[i].disabled = true;}
    }
 }
 else
 {
   for (i = 0; i < categories.length; i++)categories[i].disabled = false;   
 }
 
  
}
function checklimit()
{
	
	var packages = new Array();
	<?php foreach($packages as $id => $data) { ?>
	packages[<?php echo $id?>] = new Array();
	packages[<?php echo $id?>]['credits'] = <?php echo $data["cr_per_exposure"]?>;
	<?php } ?>
	var index = document.getElementById('package').selectedIndex;
    var pid = document.getElementById('package').options[index].value;
	document.getElementById('package_details').innerHTML = packages[<?php echo $id?>]['credits']+' Credit(s) Per Exposure';
	
}
checklimit();
check_categories();
</script>