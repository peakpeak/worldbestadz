<span  class="form_title">Edit Login Ad</span>
<?php if($error_msg){ ?>
<span class="error_message"><?php echo $error_msg;?></span>
<? } ?>
<?php if($success_msg){ ?>
<span class="success_message"><?php echo $success_msg;?></span>
<? } ?>
<div align="left"> <a href="{#loader=system::url}/members/ads/loginads">Login Ads</a> > 
  Edit Login Ad (<?php echo $loginad["name"];?>) </div>
<br />
<div class="white">
  <form action="" method="post" class="form_1">
    <input type="hidden" name="wid" value="<?php echo $loginad["id"];?>" />
    <table cellpadding="0" cellspacing="0" border="0" class="table_1">
      <thead>
        <tr>
          <th colspan="2">Edit Login Ad</th>
        </tr>
      </thead>
      <tr>
        <td>Website Name</td>
        <td><input type="text" name="name"  size="15" maxlength="20" value="<?php echo $loginad["name"];?>"></td>
      </tr>
      <tr>
        <td>Website Title</td>
        <td><input type="text" name="title" size="40" maxlength="150" value="<?php echo $loginad["title"];?>"></td>
      </tr>
      <tr>
        <td>URL</td>
        <td><input type="text" name="url" size="40" maxlength="150" value="<?php echo $loginad["url"];?>"></td>
      </tr>
     
      <tr>
        <td>Days to display ads</td>
        <td><table border="0" cellpadding="0" cellspacing="2" width="250">
            <tbody>
              <tr>
                <td align="center" width="14%">Sun<br>
                  <input name="day[]" type="checkbox"  value="Sun" <?php echo $check_sun; ?>/></td>
                <td align="center" width="14%">Mon<br>
                  <input name="day[]" type="checkbox"  value="Mon" <?php echo $check_mon; ?>/></td>
                <td align="center" width="14%">Mon<br>
                  <input name="day[]" type="checkbox"  value="Tue" <?php echo $check_tue; ?>/></td>
                <td align="center" width="14%">Mon<br>
                  <input name="day[]" type="checkbox"  value="Wed" <?php echo $check_wed; ?>/></td>
                <td align="center" width="14%">Mon<br>
                  <input name="day[]" type="checkbox"  value="Thu" <?php echo $check_thur; ?> /></td>
                <td align="center" width="14%">Mon<br>
                  <input name="day[]" type="checkbox"  value="Fri" <?php echo $check_fri; ?>/></td>
                <td align="center" width="14%">Sat<br>
                  <input name="day[]" type="checkbox"  value="Sat" <?php echo $check_sat; ?>/></td>
              </tr>
            </tbody>
          </table></td>
      </tr>
      <tr>
        <td>Credit limit per day</td>
        <td><input type="text" name="limit" size="15" maxlength="20" value="<?php echo $loginad["credits_per_day"];?>">
          <span class="sub_note_2">enter 0 for no limit</span></td>
      </tr>
      <tr>
        <td colspan="2"><input type="checkbox"  name="agree" value="1">
          I Agree that the above URL (website) conforms to our rules </td>
      </tr>
      <tr>
        <td colspan="2" align="center">
			<input type="hidden" name="submit" value="edit this ad">
			<input type="submit" value="edit this website">
		</td>
      </tr>
    </table>
  </form>
  <br>
  <br>
  <br>
</div>
<script type="text/javascript"> 
function check_categories() {

 var categories = document.getElementsByName('categories[]');
 var total = 0;
 for (var i = 0; i < categories.length; i++) if(categories[i].checked)total = total +1;
 
 if(total >= 3)
 {
    for (i = 0; i < categories.length; i++){
    if(categories[i].checked == false){categories[i].disabled = true;}
    }
 }
 else
 {
   for (i = 0; i < categories.length; i++)categories[i].disabled = false;   
 }
 
  
}
function checklimit()
{
	
	var packages = new Array();
	<?php foreach($packages as $id => $data) { ?>
	packages[<?php echo $id?>] = new Array();
	packages[<?php echo $id?>]['credits'] = <?php echo $data["cr_per_exposure"]?>;
	<?php } ?>
	var index = document.getElementById('package').selectedIndex;
    var pid = document.getElementById('package').options[index].value;
	document.getElementById('package_details').innerHTML = packages[<?php echo $id?>]['credits']+' Credit(s) Per Exposure';
	
}
checklimit();
check_categories();
</script>
