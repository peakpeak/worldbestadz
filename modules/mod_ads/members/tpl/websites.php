<span  class="form_title">Manage Websites</span>
<?php if($error_msg){ ?>
<span class="error_message"><?php echo $error_msg;?></span>
<? } ?>
<?php if($success_msg){ ?>
<span class="success_message"><?php echo $success_msg;?></span>
<? } ?>
<div align="right"><a href="{#loader=system::url}/members/ads/add_website">New Website</a></div>
<br />
<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td width="430" height="200" align="center" valign="top"><table cellspacing=1 cellpadding=2 border=0 width="100%">
        <tr>
		<td>
		<div class="white">
		  <table cellspacing=0 cellpadding=0 border=0 width="100%" class="table_1">
			<thead>
			  <tr>
				<th colspan="2">Website Ads Summary</th>
			  </tr>
			</thead>
			<tr>
			  <td  width="30%">Available Website Credits</td>
			  <td><?php echo $website_credits;?></td>
			</tr>
			<tr>
			  <td>Allowed Websites</td>
			  <td><?php echo $max_websites;?></td>
			</tr>
			<tr>
			  <td>Total Website Added</td>
			  <td><?php echo $num_websites;?></td>
			</tr>
		  </table>
	     </div>
         <?php  $i = 0;foreach ($websites as $num => $website) { $i++; ?>
         <div class="white">
              <table cellspacing=1 cellpadding=2 border=0 width="100%" class="table_1">
                <tr>
                  <td rowspan="2" style="width:70px; background:#fcfcfc;"><a href="<?php echo $website["url"];?>" target="_blank"><img src="http://api.webthumbnail.org?width=125&height=125&format=png&screen=1024&url=<?php echo $website["url"];?>" alt="<?php echo $website["name"];?>"  border="0"/></a> </td>
                  <td style="width:300px; background:#fcfcfc;"><?php echo $website["name"];?></td>
                  <td nowrap style="background:#fcfcfc;"><a href="{#loader=system::url}/members/ads/website_statistics/wid/<?php echo $website["id"];?>" id="popup<?php echo $i?>">stats</a> | <a href="{#loader=system::url}/members/ads/edit_website/wid/<?php echo $website["id"];?>">edit</a> | <a href="{#loader=system::url}/members/ads/websites/delete/1/wid/<?php echo $website["id"];?>" onclick="if(!confirm('Do you really want to delete this website')) return false">delete</a> </td>
                </tr>
                <tr>
                  <td  nowrap colspan="2" style="background:#fcfcfc;"><a href="<?php echo $website["url"];?>" target="_blank">&raquo; <?php echo $website["url"];?></a>
                    <form action="{#loader=system::url}/members/ads/websites" method="post"  class="form_2">
                      <input type="hidden" name="wid" value="<?php echo $website["id"];?>" />
                      <input type="text" name="credit" value="" size="5"  />
                      <input type="submit" name="submit" value="assign credits" onclick="if(!confirm('Do you really want to assign these credit(s)')) return false"/>
                    </form></td>
                </tr>
                <tr>
                  <td colspan="3" style="border-bottom:1px solid #CCC;"><table cellpadding="0" cellspacing="0" border="0" width="100%">
                      <tr>
                        <td style="background:<?php if($website["credits"] > 10){ ?>#b6d996<?php }else{?>#FF3300<?php } ?>; font-weight:bold;">Remaining</td>
                        <td style="background:#DDD;">Lifetime Hits </td>
                        <td style="background:#DDD;">Today Hits </td>
                        <td style="background:#DDD;">Credit limit per day </td>
                        <td style="background:#DDD;">Status</td>
                      </tr>
                      <tr>
                        <td style="background:<?php if($website["credits"] > 10){ ?>#d4f4b7<?php }else{?>#febfaf<?php } ?>; font-weight:bold;"><?php echo $website["credits"];?></td>
                        <td><?php echo $website["totalhits"];?></td>
                        <td><?php echo $website["todayhits"];?></td>
                        <td><?php echo $website["credits_per_day"];?></td>
                        <td><?php echo $website["status"];?></td>
                      </tr>
                    </table></td>
                </tr>
              </table>
            </div>
            <?php } ?>
          </td>
        </tr>
        <?php if(count($websites) == 0) : ?>
        <tr>
          <td align="center"><div class="white"> - no records found -</div></td>
        </tr>
        <?php endif; ?>
        <?php if($num_rows > $per_page) : ?>
        <tr>
          <td align="center"><?php echo $system->getPaginationString($page,$num_rows,$per_page,3,$system->curPageURL(),"page");?></td>
        </tr>
        <?php endif; ?>
      </table></td>
  </tr>
</table>
<script type="text/javascript">
 
   
	 var first = 0;
	 var last = <?php echo $i;?>;	
	  fancyjQuery(document).ready(function() {
			
			for (i = first; i  <= last; i++) {
			
			fancyjQuery("#popup"+i).fancybox({
				'width'				: '70%',
				'height'			: '100%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
          }
		
		});
  </script>
