<?php

global $system,$db,$userinfo,$settings;

$payment = $system->importClass('payment');

$processors = $payment->processors(null,"USD");

$wallet = $system->importClass('wallet');

$ads = $system->importClass('ads');

$userinfo['mid'] = (!$ads->isInvestor($userinfo['username']))?1:2;

$no_banners =  $ads->getNoAds($userinfo['username'],"banners");

$packages = $ads->getBannerPackages();

$categories = $ads->getAdCategories();

$var = $system->getVar();



if($var['submit'])
 
{

  switch($var['submit'])
  {
	/*Add Banner*/
	
    case("add this banner"):
	
	
    if(($no_banners + 1) > $settings["ads"]["max_banners_".$userinfo['mid']])
	{
		$errors[] =  "You are only allowed to add ".$settings["ads"]["max_banners_".$userinfo['mid']]." banner(s)";
	}
    elseif($var['title']=='')
	{
		$errors[] =  "Add a title";
	}
	 elseif($var['name']=='')
	{
		$errors[] =  "Add a banner name";
	}
	elseif($var['url']=='' || !Validate::url($var['url']))
	{
		$errors[] =  "Add a valid target URL";
	}
	elseif($var['image'] == '' || !Validate::url($var['image']) || !is_array(getimagesize($var['image'])))
	{
		$errors[] =  "Add a valid image URL";
	}
	
	
	///////////
	if((($packages[$var['package']]['available_spots'] - 1 ) < 0) && $packages[$var['package']]['spots'] != 0)
	{
	    $errors[] =  "There are no available spots in this package at the moment";
	}
	elseif($packages[$var['package']]['type'] == 'PPP')
	{
	    
		$balance = $wallet->userPurchaseBal($userinfo['username'],$var["pro"],"USD");
		if($var["pro"] == '') $errors[] =  "Please choose payment option";
		elseif($balance < $packages[$var['package']]['cost_per_period'])$errors[] = "Insufficient account balance for this purchase";
	}
	
	///////////
	elseif (count($var['categories']) < 1 && $settings["ads"]["categorized_ads"])
	{
		$errors[] =  "You must select one category at least";
	}
	elseif ($var['agree']!=1)
	{
	    $errors[] =  "You must agree to the terms and conditions";
    }
	else
	{	  
	   $sql="SELECT name FROM ".PREFIX."_ads_banners  WHERE username = '".$userinfo['username']."' AND name='".$db->sql_quote($var['name'])."' LIMIT 1";
	   $res = $db->query_db($sql,$print = DEBUG);
       if ($db->num_rows($res) > 0)  $errors[] =  "A banner with this name already exist"; 
	   
	   $sql="SELECT image FROM ".PREFIX."_ads_banners  WHERE username = '".$userinfo['username']."' AND image='".$db->sql_quote($var['image'])."' AND package='".$db->sql_quote($var['package'])."' LIMIT 1";
	   $res = $db->query_db($sql,$print = DEBUG);
       if ($db->num_rows($res) > 0)  $errors[] =  "This banner already exist"; 
	}
	if($var['credits'] > 0)
	{  
	   $bcredits = $ads->adsCredits($userinfo["username"],'banner');
       if($var['credits'] > $bcredits) $errors[] = "Insufficient Banner Credit(s)";  
	}
	if(is_array($errors) && !empty($errors))
	{
		while (list($key,$value) = each($errors)) $error_msg.= $value.'<br>';		
	}
	else 
	{
	    //Add Banner		
	   if (count($var['day']) > 0)	   
         foreach ($var['day'] as $v)  $display .= ","."$v".",";               
	   else $display = "";
	   
	    if (count($var['categories']) > 0) { foreach ($var['categories'] as $v)  $scategories .= $v.","; $scategories = rtrim($scategories,","); }               
	    else $scategories = "";
		
		$description = $userinfo['username']." purchases banner ads ".$packages[$var['package']]["name"];
	    if($packages[$var['package']]['type'] == 'PPP' && $packages[$var['package']]['cost_per_period'] > 0) 
	    $wallet->debitUserPurchaseBal($userinfo['username'],$var["pro"],$packages[$var['package']]['cost_per_period'],0.00,"USD",$description);
		$added = $ads->addBanner($userinfo['username'],$var['name'],$var['title'],$var['image'],$var['url'],$scategories,array($var['geo_target'],$var['country']),$var['package'],$display,$var['limit'],$var['start']);
		
		if($added && $var['credits'] > 0) $ads->assignBannerCredits($userinfo["username"],$added,$var['credits']);   
		if($added) $success_msg = 'Banner added ';
		//if($added && $packages[$var['package']]['type'] != 'PPP') $success_msg .= 'You can now assign credits for your banner to be displayed';	
	}
	

    break;
  }

}

   $banner_credits = number_format($ads->adsCredits($userinfo["username"],'banner'),2);
   
   $num_banners =  $ads->getNoAds($userinfo['username'],"banners");
   
   $max_banners = $settings["ads"]["max_banners_".$userinfo['mid']];
 
   $loader = new Loader();
   
   $data= get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'add_banner.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('private');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('private');
   
   $loader->displayOutput();
