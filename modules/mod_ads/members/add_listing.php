<?php

global $system,$db,$userinfo,$settings;

$ads = $system->importClass('ads');

$userinfo['mid'] = (!$ads->isInvestor($userinfo['username']))?1:2;

$noListings =  $ads->getNoAds($userinfo['username'],"listings");

$listing = array();

$listing["max_title"] = 20;

$listing["max_description"] = 225;

$categories = $ads->getAdCategories();

$var = $system->getVar();



if($var['submit'])
 
{

  switch($var['submit'])
  {
	/*Add Listing*/
	
    case("add this listing"):
	
	if(($noListings + 1) > $settings["ads"]["max_listings_".$userinfo['mid']])
	{
		$errors[] =  "You are only allowed to add ".$settings["ads"]["max_listings_".$userinfo['mid']]." listings in our directory";
	}
    elseif($var['name']=='')
	{
		$errors[] =  "Add a name";
	}
    elseif($var['title']=='')
	{
		$errors[] =  "Add a title";
	}
	elseif($var['description']=='')
	{
		$errors[] =  "Add description";
	}
	elseif($var['url']=='' || !Validate::url($var['url']))
	{
		$errors[] =  "Add a valid website URL";
	}
	elseif($var['image']!='' && !Validate::url($var['image']))
	{
		$errors[] =  "Add a valid banner URL";
	}
	elseif (count($var['categories']) < 1 && $settings["ads"]["categorized_ads"])
	{
		$errors[] =  "You must select one category at least";
	}
	elseif (strlen($var['title']) > $listing['max_title'])
	{
		$errors[] =  "Maximum character allowed for listing title is ".$listing['max_title'];
	}
	elseif (strlen($var['description']) > $listing['max_description'])
	{
		$errors[] =  "Maximum character allowed for listing description is ".$listing['max_description'];
	}
	elseif ($var['agree']!= 1)
	{
	    $errors[] =  "You must agree to the terms and conditions";
    }
	else
	{	  
	   $sql="SELECT name FROM ".PREFIX."_ads_directory  WHERE username = '".$userinfo['username']."' AND name='".$db->sql_quote($var['name'])."' LIMIT 1";
	   $res = $db->query_db($sql,$print = DEBUG);
       if ($db->num_rows($res) > 0)  $errors[] =  "A listing with this name already exist for this user"; 
	   
	   $sql="SELECT url FROM ".PREFIX."_ads_directory  WHERE username = '".$userinfo['username']."' AND url='".$db->sql_quote($var['url'])."' LIMIT 1";
	   $res = $db->query_db($sql,$print = DEBUG);
       if ($db->num_rows($res) > 0)  $errors[] =  "This listing already exist in the directory"; 
	}
	
	if(is_array($errors) && !empty($errors))
	{
		while (list($key,$value) = each($errors)) $error_msg.= $value.'<br>';		
	}
	else 
	{
	    if (count($var['categories']) > 0) { foreach ($var['categories'] as $v)  $scategories .= $v.","; $scategories = rtrim($scategories,","); }               
	    else $scategories = "";
		$added = $ads->addListing($userinfo['username'],$var['name'],$var['title'],$var['description'],$var['url'],$var['image'],$scategories);
		if($added) $success_msg = 'Directory listing added';	
	}
	

    break;
  }

}


 
   $loader = new Loader();
   
   $data= get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'add_listing.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('private');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('private');
   
   $loader->displayOutput();
 ?>

 
 
 