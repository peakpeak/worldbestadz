<?php

global $system,$db,$userinfo,$settings;

$ads = $system->importClass('ads');

$packages = $ads->getBannerPackages();

$pageInfo['title'] = 'Manage Banners';

$var = $system->getVar();

if($var['submit'])
{

  switch($var['submit'])
  {
     
		case("assign credits"):
	
			  if ($var['credits'] == ''  || $var['credits'] < 1)
			  {
				  $errors[] =  "All fields are required";
			  }
			  else
			  {  
				  $bid = $var['bid'];  
				  $credits = $var['credits'];
				  $bcredits = $ads->adsCredits($userinfo["username"],'banner');
				  if($credits > $bcredits) $errors[] = "Insufficient Banner Impression(s)";  
			  }
			
			 if(is_array($errors) && !empty($errors))
			 {
				while (list($key,$value) = each($errors)) $error_msg.= $value.'<br>';	
			 }
			 else 
			 {
				$ads->assignBannerCredits($userinfo["username"],$bid,$credits);   
				$success_msg = 'Banner Credits Assigned';
			 }
		 
		break;
    
  
  }

}


/*Delete Banner*/

 if($var['delete'] == 1)
 {  
      $deleted = $ads->deleteBanner($userinfo['username'],$var['bid']);
      if ($deleted) $success_msg = "Banner Deleted";  
 }
    
	
/*Get all user banners */

     $per_page = $settings['system']['rows_per_page'];
     $today = date("Y-m-d");
     $banners = array();
     $sql = "SELECT *  FROM ".PREFIX."_ads_banners WHERE username ='".$userinfo['username']."' ORDER by id ASC ";
     $result = $db->query_db($sql,$print = DEBUG);


			$page		 = $system->getVar('page');	
			$num_rows 	 = $db->num_rows($result);
			if(!empty($page)) 
			{
				$start = ($page - 1) * $settings['system']['rows_per_page']; 
			}			
			else
			{
				$start 	= 0;
				$page	= 1;
			}				

			
    $sql .= " LIMIT $start, $per_page "; 
    $result = $db->query_db($sql,$print = DEBUG);
	
	
	 if ($db->num_rows($result) > 0)
	 {
	 	$rows = $start;
	 	while ($row = $db->fetch_db_array($result))
		{	
				$sql = "SELECT * FROM ".PREFIX."_ads_banner_statistics WHERE banner_id = '".$row['id']."' ";
				$sql.= "AND date = DATE('".$today."') LIMIT 1";  
				$res = $db->query_db($sql,$print = DEBUG);
				
				$rows++;
				$row["max_days"] = $ads->getAdDays($row['id'],"banners","max");	
				$row["rem_days"] = $ads->getAdDays($row['id'],"banners","rem");	
				$row["run_days"] = $ads->getAdDays($row['id'],"banners","run");	
				$row["run_days"] = ($row["run_days"] >= $row["max_days"])?$row["max_days"]:$row["run_days"];		
				$banners[$rows] = $row;			
		}
	  }
	
   $userinfo['mid'] = (!$ads->isInvestor($userinfo['username']))?1:2;
   
   $banner_credits = number_format($ads->adsCredits($userinfo['username'],'banner'),2);
   
   $num_banners =  $ads->getNoAds($userinfo['username'],"banners");
   
   $max_banners = $settings["ads"]["max_banners_".$userinfo['mid']];
	
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'banners.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('private');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('private');
   
   $loader->displayOutput();