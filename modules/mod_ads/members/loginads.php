<?php

global $system,$db,$userinfo,$settings;

$ads = $system->importClass('ads');

$pageInfo['title'] = 'Manage Login Ads';

$pageInfo['map'] = array();

$var = $system->getVar();

if($var['submit'])
{

  switch($var['submit'])
  {
     
		 case("assign credits"):
		  
			  /*Check Credit*/
			  
			  if ($var['credit']==''  || $var['credit'] < 1)
			  {
				  $errors[] =  "All fields are required";
			  }
			  else
			  {  
				  $wid = $var['wid'];  
				  $credit = $var['credit'];
				  $ucredits = $ads->adsCredits($userinfo['username'],'loginads');
				  if($credit > $ucredits) $errors[] = "Insufficient credit(s)";  
			  }
			
			  if(is_array($errors) && !empty($errors))
			  {
				while (list($key,$value) = each($errors)) $error_msg.= $value.'<br>';	
			  }
			  else 
			  {
				$ads->assignLoginAdCredits($userinfo["username"],$wid,$credit);   
				$success_msg = 'Credits Assigned';
			  }
		 
		 break;
    
  
  }

}


/*Delete Website*/

 if($var['delete'] == 1)
 {  
      $deleted = $ads->deleteLoginAd($userinfo['username'],$var['wid']);
      if ($deleted) $return_msg = "Ad Deleted";
	  
 }
    
	
/*Get all user websites */

     $per_page = $settings['system']['rows_per_page'];
     $today = date("Y-m-d");
     $loginads = array();
     $sql = "SELECT *  FROM ".PREFIX."_ads_loginads WHERE username ='".$userinfo['username']."' ORDER by id ASC ";
     $result = $db->query_db($sql,$print = DEBUG);


			$page		 = $system->getVar('page');	
			$num_rows 	 = $db->num_rows($result);
			if(!empty($page)) 
			{
				$start = ($page - 1) * $settings['system']['rows_per_page']; 
			}			
			else
			{
				$start 	= 0;
				$page	= 1;
			}				

			
    $sql .= " LIMIT $start, $per_page "; 
    $result = $db->query_db($sql,$print = DEBUG);
	
	
	 if ($db->num_rows($result) > 0)
	 {
	 	$rows = $start;
	 	while ($row = $db->fetch_db_array($result))
		{	
			
			$sql = "SELECT * FROM ".PREFIX."_ads_loginads_statistics WHERE site_id = '".$row['id']."' ";
		    $sql.= "AND date = DATE('".$today."') LIMIT 1";  
			$res = $db->query_db($sql,$print = DEBUG);
		    if ($db->num_rows($res) > 0) { $srow = $db->fetch_db_array($res); $row['todayhits'] = $srow["hits"];}else{  $row['todayhits'] = 0.00;}
			
			$rows++;			
			$loginads[$rows] = $row;	
			    		
		}
	  }

	
   $userinfo['mid'] = (!$ads->isInvestor($userinfo['username']))?1:2;
   
   $loginads_credits = number_format($ads->adsCredits($userinfo['username'],'loginads'),2);
   
   $num_websites =  $ads->getNoAds($userinfo['username'],"loginads");
   
   $max_websites = $settings["ads"]["max_websites_".$userinfo['mid']];
   
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'loginads.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('private');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('private');
   
   $loader->displayOutput();