<?php

global $system,$db,$userinfo,$settings;

$ads = $system->importClass('ads');

$userinfo['mid'] = (!$ads->isInvestor($userinfo['username']))?1:2;

$noTextads =  $ads->getNoAds($userinfo['username'],"textads");

$packages = $ads->getTextAdPackages();

$categories = $ads->getAdCategories();

$var = $system->getVar();

if($var['submit'])
{

  switch($var['submit'])
  {
	/*Add Text Ad*/
	
    case("add this text ad"):
	
	if(($noTextads + 1) > $settings["ads"]["max_textads_".$userinfo['mid']])
	{
		$errors[] =  "You are only allowed to add ".$settings["ads"]["max_textads_".$userinfo['mid']]." text ad(s)";
	}
    elseif($var['name']=='')
	{
		$errors[] =  "Add a textad name";
	}
    elseif($var['title']=='')
	{
		$errors[] =  "Add a title";
	}
	elseif($var['body']=='')
	{
		$errors[] =  "Add a body content";
	}
	elseif($var['url']=='' || !Validate::url($var['url']))
	{
		$errors[] =  "Add a valid URL";
	}
	elseif (count($var['categories']) < 1 && $settings["ads"]["categorized_ads"])
	{
		$errors[] =  "You must select one category at least";
	}
	elseif (strlen($var['title']) > $packages[$var['package']]['max_title'])
	{
		$errors[] =  "Maximum character allowed for text ad title is ".$packages[$var['package']]['max_title'];
	}
	elseif (strlen($var['body']) > $packages[$var['package']]['max_body'])
	{
		$errors[] =  "Maximum character allowed for text ad body is ".$packages[$var['package']]['max_body'];
	}
	elseif ($var['agree']!=1)
	{
	    $errors[] =  "You must agree to the terms and conditions";
    }
	else
	{	  
	   $sql="SELECT name FROM ".PREFIX."_ads_textads  WHERE username = '".$userinfo['username']."' AND name='".$db->sql_quote($var['name'])."' LIMIT 1";
	   $res = $db->query_db($sql,$print = DEBUG);
       if ($db->num_rows($res) > 0)  $errors[] =  "A text ad with this name already exist for this user"; 
	   
	   $sql="SELECT url FROM ".PREFIX."_ads_textads  WHERE username = '".$userinfo['username']."' AND url='".$db->sql_quote($var['url'])."' AND package='".$db->sql_quote($var['package'])."' LIMIT 1";
	   $res = $db->query_db($sql,$print = DEBUG);
       if ($db->num_rows($res) > 0)  $errors[] =  "This text ad already exist in the choosen rotator"; 
	}
	if($var['credits'] > 0)
	{  
	   $tcredits = $ads->adsCredits($userinfo["username"],'textad');
       if($var['credits'] > $tcredits) $errors[] = "Insufficient Text Ad Credit(s)";  
	}
	
	if(is_array($errors) && !empty($errors))
	{
		while (list($key,$value) = each($errors)) $error_msg.= $value.'<br>';		
	}
	else 
	{
	    //Add Text Ad		
	   if (count($var['day']) > 0)	   
         foreach ($var['day'] as $v)  $display .= ","."$v".",";               
	   else $display = "";
	   
	    if (count($var['categories']) > 0) { foreach ($var['categories'] as $v)  $scategories .= $v.","; $scategories = rtrim($scategories,","); }               
	    else $scategories = "";
		
		$added = $ads->addTextAd($userinfo['username'],$var['name'],$var['title'],$var['body'],$var['url'],$scategories,array($var['geo_target'],$var['country']),$var['package'],$display,$var['limit'],$var['start']);
		if($added && $var['credits'] > 0) $ads->assignTextAdCredits($userinfo["username"],$added,$var['credits']); 
		if($added) $success_msg = 'Text Ad added';	
	}
	

    break;
  }

}

   $textad_credits = number_format($ads->adsCredits($userinfo["username"],'textad'),2);
   
   $num_textads =  $ads->getNoAds($userinfo['username'],"textads");
   
   $max_textads = $settings["ads"]["max_textads_".$userinfo['mid']];
 
   $loader = new Loader();
   
   $data= get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'add_textad.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('private');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('private');
   
   $loader->displayOutput();
 
 