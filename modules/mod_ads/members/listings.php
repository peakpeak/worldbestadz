<?php
global $system,$db,$userinfo,$settings;

$ads = $system->importClass('ads');

$pageInfo['title'] = 'Manage Listings';

$pageInfo['meta'] = array(); 

$pageInfo['map'] = array();

$var = $system->getVar();


/*Delete Listing*/

 if($var['delete'] == 1 && is_numeric($var['lid']))
 {  
      $deleted = $ads->deleteListing($userinfo['username'],$var['lid']);
      if ($deleted) $success_msg = "Listing Deleted";
 }
    
	
/*Get all user lisitings*/

     $per_page = $settings['system']['rows_per_page'];
     $today = date("Y-m-d");
     $listings = array();
     $sql = "SELECT *  FROM ".PREFIX."_ads_directory WHERE username ='".$userinfo['username']."' ORDER by id ASC ";
     $result = $db->query_db($sql,$print = DEBUG);


			$page		 = $system->getVar('page');	
			$num_rows 	 = $db->num_rows($result);
			if(!empty($page)) 
			{
				$start = ($page - 1) * $settings['system']['rows_per_page']; 
			}			
			else
			{
				$start 	= 0;
				$page	= 1;
			}				

			
    $sql .= " LIMIT $start, $per_page "; 
    $result = $db->query_db($sql,$print = DEBUG);
	
	
	if ($db->num_rows($result) > 0)
	{
	 	$rows = $start;
	 	while ($row = $db->fetch_db_array($result))
		{	
			$rows++;			
			$listings[$rows] = $row;			
		}
    }
 
 
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'listings.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('private');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('private');
   
   $loader->displayOutput();
 