<?php  
   global $system,$db,$userinfo,$settings;

   $traffic = $system->importClass('ads');
   $pageInfo['title'] = 'Login Ads Statistics';
   $pageInfo['map'] = array();

   $var = $system->getVar();
   if($var['wid'] == null)$system->invalidRequest();   
   
   
   $sql = "SELECT *  FROM ".PREFIX."_ads_loginads ";
   $sql .= "WHERE username ='".$userinfo['username']."' ";
   $sql .= "AND id ='".$db->sql_quote($var['wid'])."' LIMIT 1";
   $result = $db->query_db($sql,$print = DEBUG);
   if($db->num_rows($result) ==  0) $system->invalidRequest();
   $loginad = $db->fetch_db_array($result);
   
   $stats = $traffic->getLoginAdsStatistics($var["wid"],10);
   $system->importPlugin("openflash");

   //get data
   $data_x_axis = $stats[0];
   $data_y_axis = $stats[1];


   $x_range = 10;
   $x_data_count = count($data_x_axis);
   if($x_data_count < $x_range){$x_data_count = $x_range; $x_step = 1;}
   else{$r = $x_data_count % $x_range;
     if ($r != 0)$x_data_count = $x_data_count + $x_range - $r; 
     $x_step = intval($x_data_count/$x_range);	
   }

   $y_range = 10;
   $y_data_count = max($data_y_axis);
   if($y_data_count < $y_range){$y_data_count = $y_range; $y_step = 1;}
   else{$r = $y_data_count % $y_range;
     if ($r != 0)$y_data_count = $y_data_count + $y_range - $r; 
     $y_step = intval($y_data_count/$y_range);	
   }


   //creating a title
   $title = new title("Traffic Stats");

   //y axis
   $y = new y_axis(); 
   $y->set_range( 0, $y_data_count, $y_step );

   //add x labels
   $x_labels = new x_axis_labels();
   $x_labels->set_steps($x_step);
   $x_labels->set_vertical();
   $x_labels->set_labels( $data_x_axis );

   //x axis
   $x = new x_axis();
   $x->set_steps($x_step);
   $x->set_labels( $x_labels );


   //create dot for lines
   $dot = new hollow_dot();
   $dot->size(5)->halo_size(0)->colour('#3D5C56')->tooltip( '#val# reach on #x_label#' );

   //creating legend
   $x_legend = new x_legend( 'Date' );
   $x_legend->set_style( '{font-size: 12px; color: #778877}' );

   $y_legend = new y_legend( 'Reach' );
   $y_legend->set_style( '{font-size: 12px; color: #778877}' );


   //creating lines
   $line_1 = new line();
   $line_1->set_width( 2 );
   $line_1->set_default_dot_style($dot);
   $line_1->on_show(new line_on_show('pop-up',0.5,1));
   $line_1->set_values( $data_y_axis );
   $line_1->set_key( 'Exposures', 10 );

   //creating the chart
   $chart = new open_flash_chart();
   $chart->set_title( $title );
   $chart->set_bg_colour('#FFFFFF');
   $chart->set_x_legend( $x_legend );
   $chart->set_y_legend( $y_legend );
   $chart->add_element($line_1);
   $chart->set_y_axis( $y ); 
   $chart->set_x_axis( $x );
                    

	 $w = 500;
	 $h = 350;
	 $elementName = 'chart';
	 $graph ='
     <script type="text/javascript" src="'.SITEURL.FS.$settings['system']['plugins_directory'].'/openflash/js/swfobject.js"></script>
     <script type="text/javascript">swfobject.embedSWF("'.SITEURL.FS.$settings['system']['plugins_directory'].'/openflash/open-flash-chart.swf","'.$elementName.'","'.$w.'","'.$h.'", "9.0.0");</script>
	 <script type="text/javascript">
     function ofc_ready()
     {
       // alert(\'ofc_ready\');
     }
     function open_flash_chart_data()
     {
       // alert( \'reading data\' );
       return JSON.stringify(data);
     }
     function findSWF(movieName)
	 {
      if (navigator.appName.indexOf("Microsoft")!= -1) {
      return window[movieName];
      } else {
      return document[movieName];}
     }
     var data = '.$chart->toPrettyString().';
     </script>
	 <div id="'.$elementName.'"></div>';
	
			 
   
  // $system->importPlugin('alexa');
  // $domain = "";
   
  // $AlexaRank = new AlexaRank($domain);
   
  // echo "'s Rank : " . $AlexaRank->get('rank') . '<br />';
  // echo " is created : " . $AlexaRank->get('created') . '<br />';
  // echo " have links in : " . $AlexaRank->get('linksin') . '<br />';
  // echo "'s Reach : " . $AlexaRank->get('reach') . '<br />';
  // echo "'s title : " . $AlexaRank->get('title') . '<br />';
  // echo "'s description : " . $AlexaRank->get('description') . '<br />'; 
 
   $loader = new Loader;
	
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'loginads_statistics.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('popup');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('popup');
   
   $loader->displayOutput();
 ?>

 
 
 