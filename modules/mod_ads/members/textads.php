<?php
global $system,$db,$userinfo,$settings;

$ads = $system->importClass('ads');

$pageInfo['title'] = 'Manage Text Ads';

$pageInfo['map'] = array();

$var = $system->getVar();

if($var['submit'])
{

	  switch($var['submit'])
	  {
		 
			 case("assign credits"):
			  
				  /*Check Credit*/
				  
				  if ($var['credits']=='' || $var['credits'] < 1)
				  {
					  $errors[] =  "All fields are required";
				  }
				  else
				  {  
					  $tid = $var['tid'];  
					  $credits = $var['credits'];
					  $bcredits = $ads->adsCredits($userinfo["username"],'textad');
					  if($credits > $bcredits) $errors[] = "Insufficient Text Ad Credit(s)";  
				  }
				
				  if(is_array($errors) && !empty($errors))
				  {
					while (list($key,$value) = each($errors)) $error_msg.= $value.'<br>';	
				  }
				  else 
				  {
					$ads->assignTextAdCredits($userinfo["username"],$tid,$credits);   
					$success_msg = 'Text Ad Credits Assigned';
				  }
			 
			  break;
		
	  
	  }

}


/*Delete Text Ad*/

 if($var['delete'] == 1)
 {  
      $deleted = $ads->deleteTextAd($userinfo['username'],$var['tid']);
      if ($deleted) $return_msg = "Text Ad Deleted";  
 }
    
	
/*Get all user textads */

     $per_page = $settings['system']['rows_per_page'];
     $today = date("Y-m-d");
     $textads = array();
     $sql = "SELECT *  FROM ".PREFIX."_ads_textads WHERE username ='".$userinfo['username']."' ORDER by id ASC ";
     $result = $db->query_db($sql,$print = DEBUG);


			$page		 = $system->getVar('page');	
			$num_rows 	 = $db->num_rows($result);
			if(!empty($page)) 
			{
				$start = ($page - 1) * $settings['system']['rows_per_page']; 
			}			
			else
			{
				$start 	= 0;
				$page	= 1;
			}				

			
    $sql .= " LIMIT $start, $per_page "; 
    $result = $db->query_db($sql,$print = DEBUG);
	
	
	 if ($db->num_rows($result) > 0)
	 {
	 	$rows = $start;
	 	while ($row = $db->fetch_db_array($result))
		{	
			$sql = "SELECT * FROM ".PREFIX."_ads_textads_statistics WHERE textad_id = '".$row['id']."' ";
		    $sql.= "AND date = DATE('".$today."') LIMIT 1";  
			$res = $db->query_db($sql,$print = DEBUG);
		    if ($db->num_rows($res) > 0) { $srow = $db->fetch_db_array($res); $row['todayhits'] = $srow["views"];}else{  $row['todayhits'] = 0.00;}
			
			$rows++;			
			$textads[$rows] = $row;			
		}
	  }

	
   $userinfo['mid'] = (!$ads->isInvestor($userinfo['username']))?1:2;
   
   $textad_credits = number_format($ads->adsCredits($userinfo['username'],'textad'),2);
   
   $num_textads =  $ads->getNoAds($userinfo['username'],"textads");
   
   $max_textads = $settings["ads"]["max_textads_".$userinfo['mid']];
	
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'textads.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('private');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('private');
   
   $loader->displayOutput();