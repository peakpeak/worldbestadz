<?php
	global $system,$userinfo,$db,$settings;
	
	$ads = $system->importClass("ads");
	$w_credits = number_format($ads->adsCredits($userinfo['username'],'website'),2);
	$b_credits = number_format($ads->adsCredits($userinfo['username'],'banner'),2);
	$t_credits = number_format($ads->adsCredits($userinfo['username'],'textad'),2);
	$l_credits = number_format($ads->adsCredits($userinfo['username'],'loginads'),2);
   
  
	$sql = "SELECT SUM(surfed) as total ";
	$sql.= "FROM " . PREFIX . "_ads_surf_statistics ";
	$sql.= "WHERE username =  '".$userinfo['username']."' ";
	$sql.= "AND DATE(date) = DATE('".date("Y-m-d")."') ";
	$sql.= "LIMIT 1";
	$res = $db->query_db($sql, $print = DEBUG);
	$surfed_this_day = floatval($db->db_result($res, 0, 'total'));
	
	$sql = "SELECT SUM(surfed) as total ";
	$sql.= "FROM " . PREFIX . "_ads_surf_statistics ";
	$sql.= "WHERE username =  '".$userinfo['username']."' ";
	$sql.= "AND WEEK(date) = WEEK('".date("Y-m-d")."') ";
	$sql.= "AND MONTH(date) = MONTH('".date("Y-m-d")."') ";
	$sql.= "AND YEAR(date) = YEAR('".date("Y-m-d")."') ";
	$sql.= "LIMIT 1";
	$res = $db->query_db($sql, $print = DEBUG);
	$surfed_this_week = floatval($db->db_result($res, 0, 'total'));
	
	$sql = "SELECT SUM(surfed) as total ";
	$sql.= "FROM " . PREFIX . "_ads_surf_statistics ";
	$sql.= "WHERE username =  '".$userinfo['username']."' ";
	$sql.= "AND MONTH(date) = MONTH('".date("Y-m-d")."') ";
	$sql.= "AND YEAR(date) = YEAR('".date("Y-m-d")."') ";
	$sql.= "LIMIT 1";
	$res = $db->query_db($sql, $print = DEBUG);
	$surfed_this_month = floatval($db->db_result($res, 0, 'total'));
	
	$sql = "SELECT SUM(surfed) as total ";
	$sql.= "FROM " . PREFIX . "_ads_surf_statistics ";
	$sql.= "WHERE username =  '".$userinfo['username']."' ";
	$sql.= "AND YEAR(date) = YEAR('".date("Y-m-d")."') ";
	$sql.= "LIMIT 1";
	$res = $db->query_db($sql, $print = DEBUG);
	$surfed_this_year = floatval($db->db_result($res, 0, 'total'));
	
	$sql = "SELECT SUM(surfed) as total ";
	$sql.= "FROM " . PREFIX . "_ads_surf_statistics ";
	$sql.= "WHERE username =  '".$userinfo['username']."' ";
	$sql.= "LIMIT 1";
	$res = $db->query_db($sql, $print = DEBUG);
	$surfed_total = floatval($db->db_result($res, 0, 'total'));
   
	/*$sql = "SELECT * FROM ".PREFIX."_ads_surf_statistics ";
	$sql.= "WHERE username = '".$userinfo['username']."' ";
	$sql.= "ORDER BY date DESC LIMIT 0,5  "; 
	$surf_statistics = array();
	$res = $db->query_db($sql,$print = DEBUG);
	if ($db->num_rows($res) > 0)
	while ($row = $db->fetch_db_array($res)) 
	$surf_statistics[] = $row;*/
   
	$loader = new Loader;
	
	$data = get_defined_vars();
	
	$tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'user_dashboard.php';
	
	$loader->setVar($data);
	
	$loader->loadOutput($tpl_file);
	
	$loader->displayOutput();
?>