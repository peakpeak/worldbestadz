<?php
global $system,$db,$settings;
$ads = $system->importClass('ads');
$var = $system->getVar();
$date = date("Y-m-d");
$time_now = date("H:i:s");


if($var['type'] && $var['id']){ 
switch($var['type']){ 
 
          case 'banner':
	      $sql = "SELECT * FROM ".PREFIX."_ads_banners WHERE id = '".$db->sql_quote($var['id'])."' LIMIT 1";
	      $res = $db->query_db($sql,$print = DEBUG);
	      if ($db->num_rows($res) > 0)
	      {
		      
			   $banner_id  = $db->db_result($res,0,'id');
	           $title  = $db->db_result($res,0,'title');
               $url = $db->db_result($res,0,'url');
		       $image  = $db->db_result($res,0,'image');
               $username = $db->db_result($res,0,'username');
			   $pid = $db->db_result($res,0,'package');
			   $package = $ads->getBannerPackages($pid);
			  
			   
	           $sql 	= "UPDATE ".PREFIX."_ads_banners SET 
			               hits=hits+1 
		                   WHERE id='".$banner_id."'LIMIT 1";
		       $res 	= $db->query_db($sql,$print = DEBUG);
			   
			   ///////////////////////////////////////////////
			   if($package['type'] == 'PPC'){
			   
			   $sql  = "UPDATE ".PREFIX."_ads_banners SET ";
		       $sql .= "credits=credits-".$package["credits_per_click"].", ";
		       $sql .= "credits_used=credits_used+".$package["credits_per_click"]." ";
		       $sql .= "WHERE id='".$banner_id."' LIMIT 1 "; 	
               $db->query_db($sql,$print = DEBUG); }
			   
			    ////////////////////////////////////
	           $sql = "SELECT id FROM ".PREFIX."_ads_banner_statistics
			           WHERE username='".$username."'
			           AND banner_id='".$banner_id."'
			           AND date = '".$date."' LIMIT 1 ";
		       $res = $db->query_db($sql,$print = DEBUG);
				
		      if ($db->num_rows($res) > 0)
		      {
					/// Update Record 
					$sql  = "UPDATE ".PREFIX."_ads_banner_statistics SET ";
				    $sql .= "hits=hits+1,";
					$sql .= "time = '".$time_now."' ";
					$sql .= "WHERE banner_id='".$banner_id."' ";
					$sql .= "AND date = '".$date."' LIMIT 1";
					$res = $db->query_db($sql,$print = DEBUG);
		      }
		      else
		      {
					
					$sql = "INSERT INTO ".PREFIX."_ads_banner_statistics
					(username,banner_id,hits,date,time)
					VALUES 
					('".$username."','".$banner_id."','1','".$date."','".$time_now."')"; 
					$res = $db->query_db($sql,$print = DEBUG);
		      }
		
		
		   ///// Redirect to url//////////////
		   //echo $url;
		      $db->close_db();
		      session_write_close();
		      $system->redirect($url);	

		
	
	      } else {
		  
		     $db->close_db();
		     session_write_close();
		     $system->invalidRequest();
		  }
		  
		  break;
		  case 'textad':	
		  $sql = "SELECT * FROM ".PREFIX."_ads_textads WHERE id = '".$db->sql_quote($var['id'])."' LIMIT 1";
	      $res = $db->query_db($sql,$print = DEBUG);
	      if ($db->num_rows($res) > 0)
	      {
		      
			   $textad_id  = $db->db_result($res,0,'id');
	           $title  = $db->db_result($res,0,'title');
               $url = $db->db_result($res,0,'url');
               $username = $db->db_result($res,0,'username');
			   
	           $sql 	= "UPDATE ".PREFIX."_ads_textads SET hits=hits+1 
		                   WHERE id='".$textad_id."'LIMIT 1";
		       $res 	= $db->query_db($sql,$print = DEBUG);
			   
			    ////////////////////////////////////
	           $sql = "SELECT id FROM ".PREFIX."_ads_textads_statistics
			           WHERE username='".$username."'
			           AND textad_id='".$textad_id."'
			           AND date = '".$date."' LIMIT 1 ";
		       $res = $db->query_db($sql,$print = DEBUG);
				
		      if ($db->num_rows($res) > 0)
		      {
					/// Update Record 
					$sql = "UPDATE ".PREFIX."_ads_textads_statistics SET
							hits=hits+1,
							time = '".$time_now."' 
							WHERE textad_id='".$textad_id."' 
							AND date = '".$date."' LIMIT 1";
					$db->query_db($sql,$print = DEBUG);
					
		      }
		      else
		      {
					
					$sql = "INSERT INTO ".PREFIX."_ads_textads_statistics
					(username,textad_id,hits,date,time)
					VALUES 
					('".$username."','".$textad_id."','1','".$date."','".$time_now."')"; 
					$db->query_db($sql,$print = DEBUG);
					
		       }
	
		
		   ///// Redirect to url//////////////
		   //echo $url;
		      $db->close_db();
		      session_write_close();
		      $system->redirect($url);
		
	
	      } else {
		  
		     $db->close_db();
		     session_write_close();
		     $system->invalidRequest();
		  }
		  break;
		  case 'listing':	
		  $sql = "SELECT * FROM ".PREFIX."_ads_directory WHERE id = '".$db->sql_quote($var['id'])."' LIMIT 1";
	      $res = $db->query_db($sql,$print = DEBUG);
	      if ($db->num_rows($res) > 0)
	      {
		      
			   $listing_id  = $db->db_result($res,0,'id');
	           $title  = $db->db_result($res,0,'title');
               $url = $db->db_result($res,0,'url');
               $username = $db->db_result($res,0,'username');
			   
			   
	           $sql 	= "UPDATE ".PREFIX."_ads_directory SET hits=hits+1 
		                   WHERE id='".$listing_id."'LIMIT 1";
		       $res 	= $db->query_db($sql,$print = DEBUG);
			   
			    ////////////////////////////////////
	           $sql = "SELECT id FROM ".PREFIX."_ads_textads_statistics
			           WHERE username='".$username."'
			           AND textad_id='".$textad_id."'
			           AND date = '".$date."' LIMIT 1 ";
		       $res = $db->query_db($sql,$print = DEBUG);
				
		       $db->close_db();
		       session_write_close();
		       $system->redirect($url);	
		      
	
	
	      } else {
		  
		      $db->close_db();
		      session_write_close();
		      $system->invalidRequest();
		  }
		  break;   	   
	} 
	
}else{
$db->close_db();
session_write_close();
$system->invalidRequest();     
}
  
?>

 
 
 