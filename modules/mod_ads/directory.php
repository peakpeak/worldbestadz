<?php
  global $system,$db,$settings;

  $var = $system->getVar();
  
  $per_page = $settings['system']['rows_per_page'];

  $sql  = "SELECT * FROM ".PREFIX."_ads_directory ";
  
  $sql .= "WHERE status = 'Active' ";
  
  $sql .= "ORDER BY `added` DESC";
 
  $result = $db->query_db($sql,$print = DEBUG);


			$page		 = $system->getVar('page');	
			$num_rows 	 = $db->num_rows($result);
			if(!empty($page)) 
			{
				$start = ($page - 1) * $per_page;
			}			
			else
			{
				$start 	= 0;
				$page	= 1;
			}				

		
  $sql .= " LIMIT $start, $per_page "; 
  
  $result = $db->query_db($sql,$print = DEBUG);
  if ($db->num_rows($result) > 0)
  {
	 	$rows = $start;
	 	while ($row = $db->fetch_db_array($result))
		{	
			$rows++;
			//$row["image"] = Account::userImageSmall($row["username"]);
		    $listings[$rows] = $row;	
			$sql 	= "UPDATE ".PREFIX."_ads_directory SET views=views+1 
		               WHERE id='".$row["id"]."' LIMIT 1";
		    $db->query_db($sql,$print = DEBUG);
					
		}
  }
  
   $pageInfo['title'] = 'Directory Listings';
  
   $pageInfo['map'] = array('Directory Listings' => '',);
   
   $loader = new Loader;
   
   $data= get_defined_vars();
   
   $loader->setVar($data);
   
   $loader->mainHeader();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'directory.php';
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter();
   
   $loader->displayOutput();
   
 ?>

 