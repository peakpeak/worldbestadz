<?php
global $system,$db,$settings,$admininfo;

$system->importClass('account')->checkPrivilege(2);

$var = $system->getVar();

$traffic = $system->importClass('ads');

$packages = $traffic->getRotatorPackages();

$categories = $traffic->getAdCategories();

switch($var['Submit'])
{
		
	case"Add":
	if($var['username'] == null || !Account::isUser($var['username']))
    {
        $errors[] =  "Add a valid username";
    }
    elseif($var['title']=='')
	{
		$errors[] =  "Add a title";
	}
	 elseif($var['name']=='')
	{
		$errors[] =  "Add a website name";
	}
	elseif($var['url']=='' || !Validate::url($var['url']))
	{
		$errors[] =  "Add a valid URL";
	}
	elseif (count($var['categories']) < 1 && $settings["ads"]["categorized_ads"])
	{
		$errors[] =  "You must select one category at least";
	}
	else
	{	  
	   $sql="SELECT name FROM ".PREFIX."_ads_websites  WHERE username = '".$var['username']."' AND name='".$var['name']."' LIMIT 1";
	   $res = $db->query_db($sql,$print = DEBUG);
       if ($db->num_rows($res) > 0)  $errors[] =  "A website with this name already exist for this user"; 
	   
	   $sql="SELECT url FROM ".PREFIX."_ads_websites  WHERE username = '".$var['username']."' AND url='".$var['url']."' AND package='".$var['package']."' LIMIT 1";
	   $res = $db->query_db($sql,$print = DEBUG);
       if ($db->num_rows($res) > 0)  $errors[] =  "This website already exist in the choosen rotator"; 
	}
	
	if(is_array($errors) && !empty($errors))while (list($key,$value) = each($errors)) $return_msg.= $value.'<br>';		
	else 
	{
	    //Add Website		
	   if (count($var['day']) > 0)	   
         foreach ($var['day'] as $v)  $display .= ","."$v".",";               
	   else $display = "";
	   
	   
	    if (count($var['categories']) > 0) { foreach ($var['categories'] as $v)  $scategories .= $v.","; $scategories = rtrim($scategories,","); }               
	    else $scategories = "";
		
		$added = $traffic->addWebsite($var['username'],$var['name'],$var['title'],$var['url'],$scategories,array($var['geo_target'],$var['country']),$var['package'],$display,$var['limit'],$var['start']);
		
		if ($added && is_numeric($var['credits']) && $var['credits']> 0)
		{
		      $wid = $db->id_db();
			  $update_array = array('credits' => $var['credits']);
			  $db->update_db($update_array,"_ads_websites","id",$wid);
		}
		
		if($added) $return_msg = 'Website added';	
	}
	break;
		
}

   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'add_website.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();
	


?>