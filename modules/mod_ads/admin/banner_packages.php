<?php
global $system,$db,$settings;

$system->importClass('account')->checkPrivilege(3);

$system->importClass('ads');

$var = $system->getVar();

if($var['delete'] == 'y' && $var['pid']) $db->delete_from_db($var['pid'],"_ads_banner_packages");

    switch($var['Submit'])
	{
		case"Add":
        if(empty($var['name']))
        {
                $errors[] =  "Add a valid name";
        }
		elseif($db->if_row_exist("_ads_banner_packages",array('name' => $var['name'])))$errors[] =  "Banner package already exist";
        if(!is_numeric($var['length']))
        {
                $errors[] =  "Add a valid banner length";
        } 
		if(!is_numeric($var['height']))
        {
                $errors[] =  "Add a valid banner height";
        }
		if($var['type'] == 'PPV' && !is_numeric($var['credits_per_exposure']))
        {
                $errors[] =  "Add credits per exposure";
        }
		elseif($var['type'] == 'PPC' && !is_numeric($var['credits_per_click']))
        {
                $errors[] =  "Add credits per click";
        }
		elseif($var['type'] == 'PPP' && !is_numeric($var['cost_per_period']))
        {
                $errors[] =  "Add cost of banner space";
        }
		if($var['type'] == 'PPP' && !is_numeric($var['duration']))
		{
		        $errors[] =  "Add a duration for this ad package";
		}
		if($var['default_url'] !='' && !Validate::url($var['default_url']))
	    {
		        $errors[] =  "Add a valid target URL for default banner";
	    }
	    if($var['default_image'] !='' &&  !Validate::url($var['default_image']))
	    {
		        $errors[] =  "Add a valid image URL for default banner";
	    }
        if(is_array($errors) && !empty($errors)) while (list($key,$value) = each($errors)) $return_msg.= $value.'<br>';	
        else 
	    {   
		      // $added = Ads::addCreditPackage($var['name'],$var['website_credits'],$var['banner_credits'],$var['textad_credits'],$var['cost_for_membership']);
			  $table = "_ads_banner_packages";
			  $insert_array = array(
			                  'name' => $var['name'],
							  'length' => $var['length'],
							  'height' => $var['height'],
							  'type' => $var['type'],
							  'credits_per_exposure' => $var['credits_per_exposure'],
							  'credits_per_click' => $var['credits_per_click'],
							  'cost_per_period' => $var['cost_per_period'],
							  'duration' => $var['duration'],
							  'period' => $var['period'],
							  'spots' => $var['spots'],
							  'default_image' => $var['default_image'],
							  'default_url' => $var['default_url'],
							  );
			  $added = $db->insert_into_db($insert_array,$table); 
              if($added) $return_msg = "Banner package added";	
	    }

        break;
		
		case"Edit":
		$sql = "SELECT id,name FROM ".PREFIX."_ads_banner_packages WHERE id = '".$var['pid']."' LIMIT 1";
		$res = $db->query_db($sql,$print = DEBUG);
		if ($db->num_rows($res) > 0) {
		
		
	    $row = $db->fetch_db_array($res);
        if(empty($var['name']))
        {
                $errors[] =  "Add a valid name";
        }
		elseif($var['name'] !=  $row['name'] && $db->if_row_exist("_ads_banner_packages",array('name' => $var['name'])))$errors[] =  "Banner package name already exists";
        if(!is_numeric($var['length']))
        {
                $errors[] =  "Add a valid banner length";
        } 
		if(!is_numeric($var['height']))
        {
                $errors[] =  "Add a valid banner height";
        } 
		if($var['type'] == 'PPV' && !is_numeric($var['credits_per_exposure']))
        {
                $errors[] =  "Add credits per exposure";
        }
		elseif($var['type'] == 'PPC' && !is_numeric($var['credits_per_click']))
        {
                $errors[] =  "Add credits per click";
        }
		elseif($var['type'] == 'PPP' && !is_numeric($var['cost_per_period']))
        {
                $errors[] =  "Add cost of banner space";
        }
		if($var['type'] == 'PPP' && !is_numeric($var['duration']))
		{
		        $errors[] =  "Add a duration for this ad package";
		}
		if($var['default_url'] !='' && !Validate::url($var['default_url']))
	    {
		        $errors[] =  "Add a valid target URL for default banner";
	    }
	    if($var['default_image'] !='' &&  !Validate::url($var['default_image']))
	    {
		        $errors[] =  "Add a valid image URL for default banner";
	    } 
        if(is_array($errors) && !empty($errors)) while (list($key,$value) = each($errors)) $return_msg.= $value.'<br>';	
        else 
	    {   
		       $table = "_ads_banner_packages";
			   $update_array = array(
			                  'name' => $var['name'],
							  'length' => $var['length'],
							  'height' => $var['height'],
							  'type' => $var['type'],
							  'credits_per_exposure' => $var['credits_per_exposure'],
							  'credits_per_click' => $var['credits_per_click'],
							  'cost_per_period' => $var['cost_per_period'],
							  'duration' => $var['duration'],
							  'period' => $var['period'],
							  'spots' => $var['spots'],
							  'default_image' => $var['default_image'],
							  'default_url' => $var['default_url']);
			   $edited = $db->update_db($update_array,$table,"id",$var['pid']);
               if($edited) $return_msg = "Banner package updated";	
	    }  } else $return_msg  =  "Invalid package";
		
        break;
		
	}
	
if($var['edit'] == 'y' && isset($var['pid'])) {

$sql = "SELECT * FROM ".PREFIX."_ads_banner_packages WHERE id = '".$var['pid']."' LIMIT 1";
$res = $db->query_db($sql,$print = DEBUG);
if ($db->num_rows($res) > 0) $erow = $db->fetch_db_array($res);

}

 
$sql = "SELECT * FROM ".PREFIX."_ads_banner_packages ORDER BY id ASC ";
$res = $db->query_db($sql,$print = DEBUG);
if ($db->num_rows($res) > 0)while ($row = $db->fetch_db_array($res))$packages[$row["id"]] = $row;
   



	
	
   $data = get_defined_vars();
   
   $loader = new Loader;
   
   $loader->setVar($data);
   
   $tpl_add_file = dirname(__FILE__).DS.'tpl'.DS.'add_banner_package.php';
   
   $tpl_edit_file = dirname(__FILE__).DS.'tpl'.DS.'edit_banner_package.php';
   
   $loader->placeHolder['form'] =  ($var['edit'] == 'y' && isset($var['pid'])) ?  $tpl_edit_file :  $tpl_add_file;
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'banner_packages.php';
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();

?>
