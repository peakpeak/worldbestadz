<?php
global $system,$db,$settings,$admininfo;

$system->importClass('account')->checkPrivilege(2);

$traffic = $system->importClass('ads');

$var = $system->getVar();

$r = count ($var['list']);
$list = $var['list'];

    switch($var['Submit'])
	{
		
		case"Activate":
		for ($i=0; $i < $r; $i++)
		{
			
			$n 	= $i+1;
			$tid = $list[$i];
			
			$sql = "SELECT * FROM ".PREFIX."_ads_loginads WHERE id = '".$tid."'";
			$res = $db->query_db($sql,$print = DEBUG);
			if ($db->num_rows($res) > 0)
			{ 
			  
			   $row = $db->fetch_db_array($res)	;	   
			   $sql = "UPDATE ".PREFIX."_ads_loginads SET
                       status = 'Active'
                       WHERE id = '".$tid."' LIMIT  1";
					
			   $db->query_db($sql,$print = DEBUG);
			   $db->free_result($res);
               //send update email
               //$system->email($row['username'],"");
	       }	
		  
		}
        if($n>0) $return_msg ="Ad Activated";
		break;
		case"Suspend":
		for ($i=0; $i < $r; $i++)
		{
			
			$n 	= $i+1;
			$tid = $list[$i];
			
			$sql = "SELECT * FROM ".PREFIX."_ads_loginads WHERE id = '".$tid."'";
			$res = $db->query_db($sql,$print = DEBUG);
			if ($db->num_rows($res) > 0)
			{ 
			  
			   $row = $db->fetch_db_array($res)	;	   
			   $sql = "UPDATE ".PREFIX."_ads_loginads SET
                       status = 'Suspended'
                       WHERE id = '".$tid."' LIMIT  1";
					
			   $db->query_db($sql,$print = DEBUG);
			   $db->free_result($res);
               //send update email
               //$system->email($row['username'],"");
	       }	
		  
		}
        if($n>0) $return_msg ="Ads Suspended";
		break;
		case"Delete":
		for ($i=0; $i < $r; $i++)
		{
			
			$n 	= $i+1;
			$tid = $list[$i];
			
			$sql = "SELECT * FROM ".PREFIX."_ads_loginads WHERE id = '".$tid."'";
			$res = $db->query_db($sql,$print = DEBUG);
			if ($db->num_rows($res) > 0)
			{ 
			  
             $row = $db->fetch_db_array($res);
			 $traffic->deleteLoginAd($row['username'],$tid);
			 //  $system->email($row['username'],"");
	        }	
		  
		}
        if($n>0) $return_msg ="Ad Deleted";
		break;
		
	}	
	
  $sql  = "SELECT COUNT(*) as total FROM ".PREFIX."_ads_loginads ";

  $sql .= "WHERE id != '' ";
	
  if($var['submit'] == 'Search')
  {


      if(is_numeric($var['total_from']) || is_numeric($var['total_to'])){
	  
	  if(!is_numeric($var['total_to'])) $sql .= "AND credits >= '".$var['total_from']."'  ";
	   
	  elseif(!is_numeric($var['total_from'])) $sql .= "AND credits <= '".$var['total_to']."'  ";
	   
	  else $sql .= "AND credits BETWEEN '".$var['total_from']."' AND '".$var['total_to']."' "; 
      }
	   
      if(isset($var['status']) && count($var['status'])> 0)
	  { 
	    $sql .= "AND ( ";
	    foreach($var['status'] as $status) { $sql .= "status = '".$status."' "; if($status != end($var['status'])) $sql .= "OR ";}
	    $sql .= ") ";
	  }
    
	  if(!empty($var['username']))$sql .= "AND username = '".$var['username']."' ";
	  
	   if(!empty($var['url']))$sql .= "AND url LIKE '%".$var['url']."%' ";

  }
  $sql .= "ORDER BY id DESC";
  
  $result = $db->query_db($sql,$print = DEBUG);
  $num_rows = $db->db_result($result, 0, 'total');
  $page = $var['page'];
  $per_page = (is_numeric($var['per_page']) 
	             && $var['per_page'] <= $settings['system']['rows_per_page'])?
				 $var['per_page']:$settings['system']['rows_per_page'];		 	
  if(!empty($page)){$start = ($page - 1) * $per_page;}
  else { $start 	= 0; $page	= 1; }		
  $sql = str_replace("COUNT(*) as total", " * ",$sql);
  $sql .= " LIMIT $start, $per_page "; 
  $result = $db->query_db($sql,$print = DEBUG);
  
  if ($db->num_rows($result) > 0)
  {
	 	$rows = $start;
	 	while ($row = $db->fetch_db_array($result))
		{	
			$rows++;
		    $loginads[$rows] = $row;			
		}
  }
 
	
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'loginads.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();

?>
