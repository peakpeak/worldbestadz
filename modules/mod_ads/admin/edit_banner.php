<?php
global $system,$db,$settings,$admininfo;

$system->importClass('account')->checkPrivilege(2);

$var = $system->getVar();

if(!isset($var['bid']))$system->invalidRequest();

$traffic = $system->importClass('ads');

$packages = $traffic->getBannerPackages();

$categories = $traffic->getAdCategories();
   
switch($var['Submit'])
{
		
	case"Edit":
	if($var['username'] == null || !Account::isUser($var['username']))
    {
        $errors[] =  "Add a valid username";
    }
    elseif($var['title']=='')
	{
		$errors[] =  "Add a title";
	}
	elseif($var['name']=='')
	{
		$errors[] =  "Add a banner name";
	}
	elseif($var['url']=='' || !Validate::url($var['url']))
	{
		$errors[] =  "Add a valid URL";
	}
	elseif($var['image']=='' || !Validate::url($var['image']))
	{
		$errors[] =  "Add a valid Image URL";
	}
	elseif (count($var['categories']) < 1 && $settings["ads"]["categorized_ads"])
	{
		$errors[] =  "You must select one category at least";
	}
	else
	{	  
	   
	   $sql="SELECT name FROM ".PREFIX."_ads_banners WHERE id!='".$var['bid']."' AND username = '".$var['username']."' AND name='".$var['name']."' LIMIT 1";
	   $res = $db->query_db($sql,$print = DEBUG);
       if ($db->num_rows($res) > 0)  $errors[] =  "A banner with this name already exist"; 
	   
	   $sql="SELECT url FROM ".PREFIX."_ads_banners  WHERE id!='".$var['bid']."' AND username = '".$var['username']."' AND url='".$var['image']."' AND package='".$var['package']."' LIMIT 1";
	   $res = $db->query_db($sql,$print = DEBUG);
       if ($db->num_rows($res) > 0)  $errors[] =  "This banner already exist";   
	}
	
	if(is_array($errors) && !empty($errors))
	{
		while (list($key,$value) = each($errors)) $return_msg.= $value.'<br>';		
	}
	else 
	{
	    //Edit Banner		
	   if (count($var['day']) > 0)	   
         foreach ($var['day'] as $v)  $display .= ","."$v".",";               
	   else $display = "";
	   
	    if (count($var['categories']) > 0) { foreach ($var['categories'] as $v)  $scategories .= $v.","; $scategories = rtrim($scategories,","); }               
	    else $scategories = "";
		
		$updated = $traffic->updateBanner($var['username'],$var['bid'],$var['name'],$var['title'],$var['image'],$var['url'],$scategories,array($var['geo_target'],$var['country']),$var['package'],$display,$var['limit'],$var['start']);
		
		if (is_numeric($var['credits']) && $var['credits']> 0)
		{
		      $update_array = array('credits' => $var['credits']);
			  $db->update_db($update_array,"_ads_banners","id",$var['bid']);
		}
		
		if($updated) $return_msg = 'Banner Edited';	
	}
		break;
		
	}
	
	
   $sql = "SELECT *  FROM ".PREFIX."_ads_banners WHERE id='".$var['bid']."' LIMIT 1";
   $res = $db->query_db($sql,$print = DEBUG);
   if ($db->num_rows($res) > 0) $banner = $db->fetch_db_array($res);
   
   $check_sun =((stristr($banner['display_days'],",Sun,")))? "checked=\"checked\" ": ""; 
   $check_mon =((stristr($banner['display_days'],",Mon,")))? "checked=\"checked\" ": ""; 
   $check_tue =((stristr($banner['display_days'],",Tue,")))? "checked=\"checked\" ": ""; 
   $check_wed =((stristr($banner['display_days'],",Wed,")))? "checked=\"checked\" ": ""; 
   $check_thur =((stristr($banner['display_days'],",Thu,")))? "checked=\"checked\" ": ""; 
   $check_fri =((stristr($banner['display_days'],",Fri,")))? "checked=\"checked\" ": ""; 
   $check_sat =((stristr($banner['display_days'],",Sat,")))? "checked=\"checked\" ": ""; 
   
   
   $selected_categories = explode(",",$banner['category']);

   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'edit_banner.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();
	


?>
