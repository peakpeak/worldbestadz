<?php
global $system,$db,$settings,$admininfo;

$system->importClass('account')->checkPrivilege(2);

$traffic = $system->importClass('ads');

$categories = $traffic->getAdCategories();

$var = $system->getVar();

$r = count ($var['list']);

$list = $var['list'];

    switch($var['Submit'])
	{
		
		case"Activate":
		for ($i=0; $i < $r; $i++)
		{
			
			$n 	= $i+1;
			$tid = $list[$i];
			
			$sql = "SELECT * FROM ".PREFIX."_ads_directory WHERE id = '".$tid."'";
			$res = $db->query_db($sql,$print = DEBUG);
			if ($db->num_rows($res) > 0)
			{ 
			  
			   $row = $db->fetch_db_array($res)	;	   
			   $sql = "UPDATE ".PREFIX."_ads_directory SET
                       status = 'Active'
                       WHERE id = '".$tid."' LIMIT  1";
					
			   $db->query_db($sql,$print = DEBUG);
			   $db->free_result($res);
               //send update email
               //$system->email($row['username'],"");
	       }	
		  
		}
        if($n>0) $return_msg ="Listing Activated";
		break;
		case"Suspend":
		for ($i=0; $i < $r; $i++)
		{
			
			$n 	= $i+1;
			$tid = $list[$i];
			
			$sql = "SELECT * FROM ".PREFIX."_ads_directory WHERE id = '".$tid."'";
			$res = $db->query_db($sql,$print = DEBUG);
			if ($db->num_rows($res) > 0)
			{ 
			  
			   $row = $db->fetch_db_array($res)	;	   
			   $sql = "UPDATE ".PREFIX."_ads_directory SET
                       status = 'Suspended'
                       WHERE id = '".$tid."' LIMIT  1";
					
			   $db->query_db($sql,$print = DEBUG);
			   $db->free_result($res);
               //send update email
               //$system->email($row['username'],"");
	       }	
		  
		}
        if($n>0) $return_msg ="Listing Suspended";
		break;
		case"Delete":
		for ($i=0; $i < $r; $i++)
		{
			
			$n 	= $i+1;
			$tid = $list[$i];
			
			$sql = "SELECT * FROM ".PREFIX."_ads_directory WHERE id = '".$tid."'";
			$res = $db->query_db($sql,$print = DEBUG);
			if ($db->num_rows($res) > 0)
			{ 
			     $row = $db->fetch_db_array($res);
			     $traffic->deleteListing($row['username'],$tid);
             //  $system->email($row['username'],"");
	        }	
		  
		}
        if($n>0) $return_msg ="Listing Deleted";
		break;
		
	}
	
  $sql  = "SELECT COUNT(*) as total FROM ".PREFIX."_ads_directory ";

  $sql .= "WHERE id != '' ";
	
  if($var['submit'] == 'Search')
  {


     
      if(isset($var['category']) && $var['category']!='A') $sql .= "AND category = '".$var['category']."' ";
	   
      if(isset($var['status']) && count($var['status'])> 0)
	  { 
	    $sql .= "AND ( ";
	    foreach($var['status'] as $status) { $sql .= "status = '".$status."' "; if($status != end($var['status'])) $sql .= "OR ";}
	    $sql .= ") ";
	  }
    
	  if(!empty($var['username']))$sql .= "AND username = '".$var['username']."' ";
	  
	  if(!empty($var['url']))$sql .= "AND url LIKE '%".$var['url']."%' ";

  }

  $sql .= "ORDER BY id DESC";
  
  $result = $db->query_db($sql,$print = DEBUG);
  $num_rows = $db->db_result($result, 0, 'total');
  $page = $var['page'];
  $per_page = (is_numeric($var['per_page']) 
	             && $var['per_page'] <= $settings['system']['rows_per_page'])?
				 $var['per_page']:$settings['system']['rows_per_page'];		 	
  if(!empty($page)){$start = ($page - 1) * $per_page;}
  else { $start 	= 0; $page	= 1; }		
  $sql = str_replace("COUNT(*) as total", " * ",$sql);
  $sql .= " LIMIT $start, $per_page "; 
  $result = $db->query_db($sql,$print = DEBUG);
	
  if ($db->num_rows($result) > 0)
  {
	 	$rows = $start;
	 	while ($row = $db->fetch_db_array($result))
		{	
			$rows++;
		    $listings[$rows] = $row;			
		}
  }
 
	
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'listings.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();

?>
