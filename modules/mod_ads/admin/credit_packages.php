<?php
global $system,$db,$settings;

$system->importClass('account')->checkPrivilege(3);

$system->importClass('ads');

$var = $system->getVar();

if($var['delete'] == 'y' && $var['pid']) Ads::deleteCreditPackage($var['pid']);

    switch($var['Submit'])
	{
		case"Add":
        if(empty($var['name']))
        {
                $errors[] =  "Add a valid name";
        }
		elseif($db->if_row_exist("_ads_credit_packages",array('name' => $var['name'])))$errors[] =  "Credit package already exist";
        /*if(!is_numeric($var['website_credits']))
        {
                $errors[] =  "Add a valid amount of credits";
        } */
        if(is_array($errors) && !empty($errors)) while (list($key,$value) = each($errors)) $return_msg.= $value.'<br>';	
        else 
	    {   
		       $added = Ads::addCreditPackage($var['name'],
			                                   $var['website_credits'],
											   $var['banner_credits'],
											   $var['textad_credits'],
											   $var['loginads_credits'],
											   $var['ptc_credits'],
											   $var['cost_for_membership'],
											   $var['hidden']);
               if($added) $return_msg = "Credit package added";	
	    }

        break;
		
		case"Edit":
		$sql = "SELECT id,name FROM ".PREFIX."_ads_credit_packages WHERE id = '".$var['pid']."' LIMIT 1";
		$res = $db->query_db($sql,$print = DEBUG);
		if ($db->num_rows($res) > 0) {
		
		
	    $row = $db->fetch_db_array($res);
        if(empty($var['name']))
        {
                $errors[] =  "Add a valid name";
        }
		elseif($var['name'] !=  $row['name'] && $db->if_row_exist("_ads_credit_packages",array('name' => $var['name'])))$errors[] =  "Credit package name already exists";
        /*if(!is_numeric($var['website_credits']))
        {
                $errors[] =  "Add a valid amount of credits";
        } */
        if(is_array($errors) && !empty($errors)) while (list($key,$value) = each($errors)) $return_msg.= $value.'<br>';	
        else 
	    {   
		       $edited = Ads::editCreditPackage($var['pid'],
			                                    $var['name'],
											    $var['website_credits'],
												$var['banner_credits'],
												$var['textad_credits'],
												$var['loginads_credits'],
											    $var['ptc_credits'],
												$var['cost_for_membership'],
											    $var['hidden']);
               if($edited) $return_msg = "Credit package updated";	
	    }  } else $return_msg  =  "Invalid package";
		
        break;
		case"Delete":
		break;
	}
	
	

if($var['edit'] == 'y' && isset($var['pid'])) {

$sql = "SELECT * FROM ".PREFIX."_ads_credit_packages WHERE id = '".$var['pid']."' LIMIT 1";
$res = $db->query_db($sql,$print = DEBUG);
if ($db->num_rows($res) > 0) $erow = $db->fetch_db_array($res);

}
/*$sql = "SELECT * FROM ".PREFIX."_membership_plans ORDER BY id ASC ";
$res = $db->query_db($sql,$print = DEBUG);
if ($db->num_rows($res) > 0) while ($row = $db->fetch_db_array($res)) $plans[$row["id"]] = $row;*/

$mplans = array(1=>"Free Members",2=>"Upgraded Members");

$sql = "SELECT * FROM ".PREFIX."_ads_credit_packages ORDER BY id ASC ";
$res = $db->query_db($sql,$print = DEBUG);
$prices = array();
if ($db->num_rows($res) > 0) while ($row = $db->fetch_db_array($res))
{
   $membership_costs =  explode(",",$row["costs"]);
    foreach ($membership_costs as $costs)
    {
       $cost = explode(":",$costs);
	   $prices[$row["id"]][$cost[0]] = $cost[1];
    }
    $packages[$row["id"]] = $row;
 }

   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $loader->setVar($data);
   
   $tpl_add_file = dirname(__FILE__).DS.'tpl'.DS.'add_credit_package.php';
   
   $tpl_edit_file = dirname(__FILE__).DS.'tpl'.DS.'edit_credit_package.php';
   
   $loader->placeHolder['form'] =  ($var['edit'] == 'y' && isset($var['pid'])) ?  $tpl_edit_file :  $tpl_add_file;
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'credit_packages.php';
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();
   
 ?>