<?php
global $system,$db,$settings;

$system->importClass('account')->checkPrivilege(3);

$system->importClass('ads');

$var = $system->getVar();

if($var['delete'] == 'y' && $var['cid']) $db->delete_from_db($var['cid'],"_ads_ad_categories");

    switch($var['Submit'])
	{
		case"Add":
        if(empty($var['name']))
        {
                $errors[] =  "Add a valid name";
        }
		elseif($db->if_row_exist("_ads_ad_categories",array('name' => $var['name'])))$errors[] =  "Website category already exist";
      
        if(is_array($errors) && !empty($errors)) while (list($key,$value) = each($errors)) $return_msg.= $value.'<br>';	
        else 
	    {   
		    
			  $table = "_ads_ad_categories";
			  $insert_array = array('name' => $var['name'],'order' => $var['order']);
			  $added = $db->insert_into_db($insert_array,$table); 
              if($added) $return_msg = "Website category added";	
	    }

        break;
		
		case"Edit":
		$sql = "SELECT id,name FROM ".PREFIX."_ads_ad_categories WHERE id = '".$var['cid']."' LIMIT 1";
		$res = $db->query_db($sql,$print = DEBUG);
		if ($db->num_rows($res) > 0) {
		
		
	    $row = $db->fetch_db_array($res);
        if(empty($var['name']))
        {
                $errors[] =  "Add a valid name";
        }
		elseif($var['name'] !=  $row['name'] && $db->if_row_exist("_ads_ad_categories",array('name' => $var['name'])))$errors[] =  "Website category name already exists";
      
        if(is_array($errors) && !empty($errors)) while (list($key,$value) = each($errors)) $return_msg.= $value.'<br>';	
        else 
	    {   
		       $table = "_ads_ad_categories";
			   $update_array = array('name' => $var['name'],'order' => $var['order']);
			   $edited = $db->update_db($update_array,$table,"id",$var['cid']);
               if($edited) $return_msg = "Website category updated";	
	    }  } else $return_msg  =  "Invalid category";
		
        break;
		case"Save":
		
        $r = count ($var['list']);
        $list = $var['list'];

		for ($i=0; $i < $r; $i++)
		{
			
			$n 	= $i+1;
			$tid = $list[$i];
			
			$sql = "SELECT id,name FROM ".PREFIX."_ads_ad_categories WHERE id = '".$tid."' LIMIT 1";
			$res = $db->query_db($sql,$print = DEBUG);
			if ($db->num_rows($res) > 0)
			{ 
			    $row = $db->fetch_db_array($res);
				$flagged = false;
                if(empty($var['name'][$tid]))
                 {
                    $errors[] =  "Add a valid name for id ".$tid;
					$flagged = true;
                 }
		         elseif($var['name'][$tid] !=  $row['name'] && $db->if_row_exist("_ads_ad_categories",array('name' => $var['name'][$tid]))){
				 $errors[] =  "Website category name already exists for id ".$tid;
				 $flagged = true;
				 
				 }
               
				  
				 if(!$flagged){
				 
			     $sql = "UPDATE ".PREFIX."_ads_ad_categories SET
			           name = '".$var['name'][$tid]."',
					   `order` = '".$var['order'][$tid]."'
                       WHERE id = '".$tid."' Limit 1";
					
			     $db->query_db($sql,$print = DEBUG);
			     $db->free_result($res);
				 
				 }
	       }	
		  
		}
        if(is_array($errors) && !empty($errors)) while (list($key,$value) = each($errors)) $return_msg.= $value.'<br>';	else $return_msg ="Website Category Updated";
		break;
		case"Delete":
		break;
	}
	
if($var['edit'] == 'y' && isset($var['cid'])) {

$sql = "SELECT * FROM ".PREFIX."_ads_ad_categories WHERE id = '".$var['cid']."' LIMIT 1";
$res = $db->query_db($sql,$print = DEBUG);
if ($db->num_rows($res) > 0) $erow = $db->fetch_db_array($res);

}
 
$sql = "SELECT * FROM ".PREFIX."_ads_ad_categories ORDER BY `order` ASC ";
$res = $db->query_db($sql,$print = DEBUG);
if ($db->num_rows($res) > 0) while ($row = $db->fetch_db_array($res))$categories[$row["id"]] = $row;
   
   $data = get_defined_vars();
   
   $loader = new Loader;
   
   $loader->setVar($data);
   
   $tpl_add_file = dirname(__FILE__).DS.'tpl'.DS.'add_ad_category.php';
   
   $tpl_edit_file = dirname(__FILE__).DS.'tpl'.DS.'edit_ad_category.php';
   
   $loader->placeHolder['form'] =  ($var['edit'] == 'y' && isset($var['cid'])) ?  $tpl_edit_file :  $tpl_add_file;
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'ad_categories.php';
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
    $loader->displayOutput();
?>
