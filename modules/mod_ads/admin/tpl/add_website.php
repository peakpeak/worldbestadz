<h1>Manage Websites</h1>

<p class="info">Here you can add a new website</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>


       <form action=""  method="post">
       <input type="hidden" name="add" value="y" />
       <table  cellpadding="0" cellspacing="0"  class="tableS">
                <thead>
                  <tr>
                  <th colspan="2"  class="left"><a href="?mod=ads&go=websites">Manage Websites</a> > New Website</th>
                  </tr>
                 </thead>
                 <tr>
                    <td width="30%">Username</td>
                    <td><input type="text" name="username"  value="<?php echo (isset($var["username"]))?$var["username"]:$admininfo["username"];?>" /></td>
                 </tr>
                 <tr>
					<td width="30%">Website Name</td>
					<td><input type="text" name="name" value="<?php echo $var["name"];?>"></td>
				</tr>
				<tr>
					<td>Website Title</td>
					<td><input type="text" name="title" value="<?php echo $var["title"];?>" size="60"> </td>
				</tr>
				<tr>
					<td>URL</td>

					<td><input type="text" name="url" value="<?php echo (isset($var["url"]))?$var["url"]:'http://';?>" size="60"></td>
				</tr>
				<tr>
					<td>Choose rotator</td>
					<td><?php echo $db->make_select("package",$packages,NULL,NULL);  ?></td>
				</tr>
				<!--<tr>
					<td>Category</td>
					<td>
					 <?php $num_data = count($categories);
	                 if($num_data > 0){
	                 $num = 0;
	                 $sort_in = 5;
	                 $tds = ($num_data%$sort_in);
	                 if($tds != 0) $tds = $sort_in - $tds;
	                 ?>
	                 <table cellpadding="0" cellspacing="0"  width="100%">
	                 <tr>
					 <?php  foreach($categories as $id => $value) {
	    
		            if($num%$sort_in == 0) { ?> </tr> <tr><? } ?> 
		 
		             <td><input type="checkbox" name="categories[]" value="<?php echo $id;?>" <?php if($var['categories'][$num] == $id) { ?> checked="checked"<?php }?> onclick='check_categories()'/> &nbsp;<?php echo $value;?></td>
		  
                     <?php $num++;} for($i=0;$i<$tds;$i++){ ?><td >&nbsp; </td><?php } ?>
		 
		             </tr>
					 </table>
	                 <?php } else { ?> No categories found <?php }?>
		 
		         </td>
				</tr>-->
				<tr>
					<td>Credits</td>
					<td><input type="text" name="credits" value="<?php echo $var["credits"];?>"></td>
				</tr>
				<tr>
					<td>Start date</td>
					<td><script>jQueryui(function() { jQueryui( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });});</script><input type="text" name="start" id="datepicker" value="<?php echo $var[""];?>"></td>
				</tr>
				
				<tr>
                  <td>Days to display ads</td>
                  <td>
                   <table border="0" cellpadding="0" cellspacing="2" width="250">
                     <tbody>
					  <tr>
					     <td align="center" width="14%">Sun<br><input name="day[]" type="checkbox"  value="Sun" <?php if(in_array('Sun',$var['day'])){?> checked="checked" <?php }?>/></td>
					     <td align="center" width="14%">Mon<br><input name="day[]" type="checkbox"  value="Mon" <?php if(in_array('Mon',$var['day'])){?> checked="checked" <?php }?>/></td>
					     <td align="center" width="14%">Tue<br><input name="day[]" type="checkbox"  value="Tue"<?php if(in_array('Tue',$var['day'])){?> checked="checked" <?php }?> /></td>
					     <td align="center" width="14%">Wed<br><input name="day[]" type="checkbox"  value="Wed" <?php if(in_array('Wed',$var['day'])){?> checked="checked" <?php }?>/></td>
					     <td align="center" width="14%">Thu<br><input name="day[]" type="checkbox"  value="Thu" <?php if(in_array('Thu',$var['day'])){?> checked="checked" <?php }?>/></td>
					     <td align="center" width="14%">Fri<br><input name="day[]" type="checkbox"  value="Fri" <?php if(in_array('Fri',$var['day'])){?> checked="checked" <?php }?>/></td>
					     <td align="center" width="14%">Sat<br><input name="day[]" type="checkbox"  value="Sat"  <?php if(in_array('Sat',$var['day'])){?> checked="checked" <?php }?>/></td>
					   </tr>
                  </tbody>
				 </table>
                 </td>
			  </tr>
				<tr>
					<td>Credit limit per day</td>
					<td><input type="text" name="limit" value="<?php echo $var["limit"];?>"> enter 0 for no limit</td>
				</tr>
	
	            <!--<tr>
                 <td>Status</td>
                <td>
	            <select name="status" >
                 <option value="Active">Active</option>
                 <option value="Pending">Pending</option>
                 <option value="Suspended">Suspended</option>
                </select>
			    </td>
             </tr>-->
             <tr>
                <td colspan="2" align="left"><input type="submit" name="Submit" value="Add" /></td>
             </tr>
             </table>
           </form>



