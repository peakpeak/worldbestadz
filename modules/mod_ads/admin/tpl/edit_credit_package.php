
<form  method="post" action="">
<input type="hidden" name="edit" value="y" />
<input type="hidden" name="pid" value="<?php echo $var['pid']?>" />
<table cellpadding="0" cellspacing="0"   class="tableS">
<thead>
  <tr >
    <th colspan="2"  class="left"><a href="?mod=ads&go=credit_packages">Credit Packages</a> > Edit Credit Package</th>
  </tr>
  </thead>
  <tr>
     <td width="30%">Name</td>
     <td><input type="text" name="name" value="<?php echo $erow['name'] ?>"></td>
  </tr>
  <tr>
     <td>Website Credits</td>
     <td><input type="text" name="website_credits" value="<?php echo $erow['website_credits'] ?>"></td>
  </tr>
  <tr>
     <td>Banner Ads Credits</td>
     <td><input type="text" name="banner_credits" value="<?php echo $erow['banner_credits'] ?>"></td>
  </tr>
  <tr>
     <td>Text Ads Credits</td>
     <td><input type="text" name="textad_credits" value="<?php echo $erow['textad_credits'] ?>"></td>
  </tr>
  <tr>
     <td>Login Ads Credits</td>
     <td><input type="text" name="loginads_credits" value="<?php echo $erow['loginads_credits'] ?>"></td>
  </tr>
  
  <?php foreach ($mplans as $id => $name){ ?> 
    <tr>
      <td >Cost for <?php echo $name?> ($)</td>
      <td ><input name="cost_for_membership[<?php echo $id ?>]" type="text" value="<?php echo $prices[$erow["id"]][$id] ?>" /></td>
    </tr>
   <?php } ?>
   <?php if(count($mplans) == 0) { ?>  <tr> <td colspan="2"><p class="info">You have to add a membership plan to add a credit package</p></td></tr><?php } ?>
   <tr>
   <td>Hidden</td>
   <td><select name="hidden">
           <option value="Y" <?php if($erow['hidden'] == 'Y'){?> selected="selected" <?php }?>>Yes</option>
           <option value="N" <?php if($erow['hidden'] == 'N'){?> selected="selected" <?php }?>>No</option>
        </select> <img src="images/tooltip.png" alt="Tip" class="tooltip" title="Select hidden if you do not want this package to be displayed in members area" />
     </td>
  </tr>
  <tr>
    <td colspan="2" class="left"><input type="submit" name="Submit" value="Edit"  <?php if(count($mplans) == 0) { ?> disabled="disabled" <?php } ?> /> </td>
  </tr>
  </table>
</form>
