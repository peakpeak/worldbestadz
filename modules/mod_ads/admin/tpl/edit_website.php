<h1>Websites</h1>

<p class="info">Here you can edit a website</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>


       <form action=""  method="post">
       <input type="hidden" name="wid" value="<?php echo $website["id"];?>" />
       <table  cellpadding="0" cellspacing="0"  class="tableS">
                <thead>
                  <tr>
                  <th colspan="2"  class="left"><a href="?mod=ads&go=websites">Manage Websites</a> > Edit Website > <?php echo $website["name"];?></th>
                  </tr>
                 </thead>
                 <tr>
                    <td width="30%">Username</td>
                    <td><input type="text" name="username"  value="<?php echo $website["username"];?>" /> </td>
                 </tr>
                <tr>
					<td>Website name</td>
					<td><input type="text" name="name"  value="<?php echo $website["name"];?>"></td>
				</tr>
				<tr>
					<td>Website title</td>
					<td><input type="text" name="title" size="60" maxlength="150" value="<?php echo $website["title"];?>"></td>
				</tr>
				<tr>
					<td>URL</td>

					<td><input type="text" name="url" size="60" maxlength="150" value="<?php echo $website["url"];?>"></td>
				</tr>			
				<tr>
					<td>Choose rotator</td>
					<td><?php echo $db->make_select("package",$packages,$website["package"],NULL);  ?></td>
				</tr>
				<!--<tr>
					<td>Website category</td>
					<td>
					 <?php $num_data = count($categories);
	                 if($num_data > 0){
	                 $num = 0;
	                 $sort_in = 5;
	                 $tds = ($num_data%$sort_in);
	                 if($tds != 0) $tds = $sort_in - $tds;
	                 ?>
	                 <table cellpadding="0" cellspacing="0"  width="100%">
	                 <tr>
					 <?php  foreach($categories as $id => $value) {
	    
		            if($num%$sort_in == 0) { ?> </tr> <tr><? } ?> 
		 
		             <td><input type="checkbox" name="categories[]" value="<?php echo $id;?>" <?php if(in_array($id,$selected_categories)) { ?> checked="checked"<?php }?> onclick='check_categories()'/> &nbsp;<?php echo $value;?></td>
		  
                     <?php $num++;} for($i=0;$i<$tds;$i++){ ?><td >&nbsp; </td><?php } ?>
		 
		             </tr>
					 </table>
	                 <?php } else { ?> No categories found <?php }?>
		 
		         </td>
				
				</tr>-->
				<tr>
					<td>Credits</td>
					<td><input type="text" name="credits" value="<?php echo $website["credits"];?>"></td>
				</tr>
				<tr>
					<td>Start date</td>
					<td><script>jQueryui(function() { jQueryui( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });});</script><input type="text" name="start"  id="datepicker"  value="<?php echo $website["startdate"];?>"></td>
				</tr>
				<tr>
                  <td>Days to display ads</td>
                  <td>
                   <table border="0" cellpadding="0" cellspacing="2" width="250">
                     <tbody>
					  <tr>
					     <td align="center" width="14%">Sun<br><input name="day[]" type="checkbox"  value="Sun" <?php echo $check_sun; ?>/></td>
					     <td align="center" width="14%">Mon<br><input name="day[]" type="checkbox"  value="Mon" <?php echo $check_mon; ?>/></td>
					     <td align="center" width="14%">Tue<br><input name="day[]" type="checkbox"  value="Tue" <?php echo $check_tue; ?>/></td>
					     <td align="center" width="14%">Wed<br><input name="day[]" type="checkbox"  value="Wed" <?php echo $check_wed; ?>/></td>
					     <td align="center" width="14%">Thu<br><input name="day[]" type="checkbox"  value="Thu" <?php echo $check_thur; ?> /></td>
					     <td align="center" width="14%">Fri<br><input name="day[]" type="checkbox"  value="Fri" <?php echo $check_fri; ?>/></td>
					     <td align="center" width="14%">Sat<br><input name="day[]" type="checkbox"  value="Sat" <?php echo $check_sat; ?>/></td>
					   </tr>
                  </tbody>
				 </table>
                 </td>
			  </tr>
				<tr>
					<td>Credit limit per day</td>
					<td><input type="text" name="limit"  value="<?php echo $website["credits_per_day"];?>"> enter 0 for no limit</td>
				</tr>
	            <!--<tr>
                 <td>Status</td>
                <td>
	            <select name="status" >
                 <option value="Active">Active</option>
                 <option value="Pending">Pending</option>
                 <option value="Suspended">Suspended</option>
                </select>
			    </td>
             </tr>-->
             <tr>
                <td colspan="2" align="left"><input type="submit" name="Submit" value="Edit" /></td>
             </tr>
             </table>
           </form>



