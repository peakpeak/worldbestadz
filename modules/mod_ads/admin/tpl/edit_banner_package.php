<form  method="post" action="">
<input type="hidden" name="edit" value="y" />
<input type="hidden" name="pid" value="<?php echo $erow['id']?>" />
<table cellpadding="0" cellspacing="0"   class="tableS">
<thead>
   <tr >
    <th colspan="2"  class="left">Edit Banner Package</th>
  </tr>
  </thead>
  <tr>
     <td width="30%">Name</td>
     <td><input type="text" name="name" value="<?php echo $erow["name"]?>"></td>
  </tr>
  <tr>
     <td>Dimension</td>
     <td><input type="text" name="length" value="<?php echo $erow['length'] ?>" size="5"> x <input type="text" name="height" value="<?php echo $erow['height'] ?>" size="5"><img src="images/tooltip.png" alt="Tip" class="tooltip" title="Add dimension of your ad in format ( W X H ) example 25 x 125, 468 x 60, 728 x 90" /></td>
  </tr>
   <tr>
   <td>Ad Type</td>
   <td><select name="type" id="type" onchange="toggle_type();">
           <option value="PPV" <?php if($erow["type"] == "PPV") { ?> selected="selected" <?php } ?> >Pay Per View</option>
           <option value="PPC" <?php if($erow["type"] == "PPC") { ?> selected="selected" <?php } ?> >Pay Per Click</option>
		   <option value="PPP" <?php if($erow["type"] == "PPP") { ?> selected="selected" <?php } ?> >Pay Per Period</option>
        </select><img src="images/tooltip.png" alt="Tip" class="tooltip" title="Pay Per View - Banner credits will be deducted for each ad exposure
Pay Per Click - Banner Credits will be deducted for each ad click
Pay per Period - This will cost members money to add banners in this section for a period of time" />
     </td>
   </tr>
  
  <tr id="r_ppv">
     <td>Credits Per Exposure</td>
     <td><input type="text" name="credits_per_exposure" value="<?php echo $erow["credits_per_exposure"]?>"><img src="images/tooltip.png" alt="Tip" class="tooltip" title="This is the amount of text credit that will be debited from a users account every time their ad is displayed" /></td>
  </tr>
   <tr id="r_ppc">
     <td>Credits Per Click</td>
     <td><input type="text" name="credits_per_click" value="<?php echo $erow["credits_per_click"]?>"><img src="images/tooltip.png" alt="Tip" class="tooltip" title="This is the amount of text credit that will be debited from a users account every time their ad is clicked" /></td>
  </tr>
   <tr id="r_ppp_1">
     <td>Banner Space Cost ($)</td>
     <td><input type="text" name="cost_per_period" value="<?php echo $erow["cost_per_period"]?>"><img src="images/tooltip.png" alt="Tip" class="tooltip" title="This is the amount that will be debited from a users account for adding their ad to this package. Note that the revenue from this package won't be distributed amongst members" /></td>
  </tr>
  <tr id="r_ppp_2">
   <td>Ads last for</td>
   <td><input type="text" name="duration" value="<?php echo $erow["duration"] ?>"  size="5">
        <select name="period">
           <option value="H" <?php if($erow["period"] == "H") { ?> selected="selected" <?php } ?>>Hours</option>
           <option value="D" <?php if($erow["period"] == "D") { ?> selected="selected" <?php } ?>>Days</option>
		   <option value="W" <?php if($erow["period"] == "W") { ?> selected="selected" <?php } ?>>Weeks</option>
		   <option value="M" <?php if($erow["period"] == "M") { ?> selected="selected" <?php } ?>>Months</option>
		   <option value="Y" <?php if($erow["period"] == "Y") { ?> selected="selected" <?php } ?>>Years</option>
        </select>
     </td>
	</tr>
  <tr>
     <td>Spots available</td>
     <td><input type="text" name="spots" value="<?php echo $erow["spots"]?>"><img src="images/tooltip.png" alt="Tip" class="tooltip" title="This is the number of ad spots that will be in rotation for this package - for fixed spots set to 1. If you dont want to limit the number of ads that can be added to this package set this to 0" /></td>
  </tr>
  <thead>
   <tr >
    <th colspan="2"  class="left">Default Banner <img src="images/tooltip.png" alt="Tip" class="tooltip" title="Add a banner to be displayed in absence of any banner in this package" /></th>
  </tr>
  </thead>
   <tr>
     <td>Banner Image URL</td>
     <td><input type="text" name="default_image" value="<?php echo $erow["default_image"]?>"></td>
  </tr>
   <tr>
     <td>Target URL</td>
     <td><input type="text" name="default_url" value="<?php echo $erow["default_url"]?>"></td>
  </tr>
  <tr>
    <td colspan="2" class="left"> <input type="submit" name="Submit" value="Edit" id="button"/> </td>
  </tr>
  </table>
</form>
