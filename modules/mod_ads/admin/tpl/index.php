<table class="tableS" cellpadding="0" cellspacing="0">
 <!--<thead>
  <tr>
    <th colspan="2" align="left">HYIP Manager Statistics</th>
  </tr>
  </thead>
  <tr>
    <td width="30%">Stat 1</td>
    <td >Stat 1 value</td>
  </tr>
  <thead>
  <tr>
    <th colspan="2"  align="left">Investment Statistics</th>
  </tr>
  </thead>-->
  

 <tr>
   <td colspan="2">
   <table  class="tableS" cellpadding="0" cellspacing="0" width="100%">
    <thead>
  <tr>
    <th colspan="2"  align="left">Investment Statistics</th>
  </tr>
  </thead>
   <tr>
    <td valign="top" width="30%">
	 <table cellpadding="0" cellspacing="0"  width="100%">
	  <tr>
       <td>Total Investors</td>
       <td><?php echo $n_investors;?></td>
      </tr>
	  <tr>
       <td>Active Investments</td>
       <td>$<?php echo $total_active_investment;?></td>
      </tr>
	  <tr>
       <td>Expired Investments</td>
       <td>$<?php echo $total_expired_investment;?></td>
      </tr>
	  <tr>
       <td align="right"><b>Total</b></td>
       <td><b>$<?php echo $total_investment;?></b></td>
      </tr>
	  <tr>
       <td>Principal Returned</td>
       <td>$<?php echo $total_principal_returned;?></td>
      </tr>
	  <tr>
       <td>Total Earnings</td>
       <td>$<?php echo $total_earned;?></td>
      </tr>
	   <tr>
       <td>Total Commissions</td>
       <td>$<?php echo $total_commissions;?></td>
      </tr>
	   <tr>
       <td align="right"><b>Equity</b></td>
       <td><b>$<?php echo $equity;?></b></td>
      </tr>
	  <tr>
       <td>Total Wallet Balance</td>
       <td>$<?php echo $total_wallet_balance;?></td>
      </tr>
	  <tr>
       <td>Withdrawable Wallet Balance</td>
       <td>$<?php echo $withdrawable_wallet_balance;?></td>
      </tr>
	  <tr>
       <td>Pending Withdrawals</td>
       <td>$<?php echo $pending_withdrawals['total'];?></td>
      </tr>
	   <tr>
       <td>Paid Withdrawals</td>
       <td>$<?php echo $paid_withdrawals['total'];?></td>
      </tr>
	 </table>
	</td>
    <td><?php echo $graph;?></td>
  </tr>
 </table>
 </td>
 </tr>
 <tr>
    <td colspan="2">
	<table class="tableS" cellpadding="0" cellspacing="0">
 <thead>
  <tr>
    <th align="left">&nbsp;</th>
	<th align="left">Wallet Deposits</th>
    <th align="left">Investments</th>
    <th align="left">Earnings</th>
	<th align="left">Commissions</th>
	<th align="left">Pending Withdrawals</th>
	<th align="left">Paid Withdrawals</th>
  </tr>
  </thead>
  <tr>
    <td align="left">Today</td>
    <td align="left">$<?php echo $wallet_deposits['day'];?></td>
	<td align="left">$<?php echo $shares_purchase['day'];?></td>
    <td align="left">$<?php echo $shares_rebates['day'];?></td>
	<td align="left">$<?php echo $shares_commissions['day'];?></td>
	<td align="left">$<?php echo $pending_withdrawals['day'];?></td>
	<td align="left">$<?php echo $paid_withdrawals['day'];?></td>
  </tr>
   <tr>
    <td align="left">This Week</td>
	<td align="left">$<?php echo $wallet_deposits['week'];?></td>
    <td align="left">$<?php echo $shares_purchase['week'];?></td>
    <td align="left">$<?php echo $shares_rebates['week'];?></td>
	<td align="left">$<?php echo $shares_commissions['week'];?></td>
	<td align="left">$<?php echo $pending_withdrawals['week'];?></td>
	<td align="left">$<?php echo $paid_withdrawals['week'];?></td>
  </tr>
   <tr>
    <td align="left">This Month</td>
	<td align="left">$<?php echo $wallet_deposits['month'];?></td>
    <td align="left">$<?php echo $shares_purchase['month'];?></td>
    <td align="left">$<?php echo $shares_rebates['month'];?></td>
	<td align="left">$<?php echo $shares_commissions['month'];?></td>
	<td align="left">$<?php echo $pending_withdrawals['month'];?></td>
	<td align="left">$<?php echo $paid_withdrawals['month'];?></td>
  </tr>
   <tr>
    <td align="left">This Year</td>
	<td align="left">$<?php echo $wallet_deposits['year'];?></td>
    <td align="left">$<?php echo $shares_purchase['year'];?></td>
    <td align="left">$<?php echo $shares_rebates['year'];?></td>
	<td align="left">$<?php echo $shares_commissions['year'];?></td>
	<td align="left">$<?php echo $pending_withdrawals['year'];?></td>
	<td align="left">$<?php echo $paid_withdrawals['year'];?></td>
  </tr>
  </table>
   </td>
  </tr>
  
  <tr>
    <td width="50%">
	<table class="tableS" cellpadding="0" cellspacing="0">
    <thead>
  <tr>
    <th  colspan="2" align="left">Top 10 investors</th>
  </tr>
  </thead>
  <?php foreach($top_investors as $investor) { ?>
  <tr>
    <td align="left"><?php echo $investor["username"] ?></td>
    <td align="left">$<?php echo $investor["invest"] ?></td>
  </tr>
  <?php } ?>
  </table>
   </td>
   
   
   <td>
	<table class="tableS" cellpadding="0" cellspacing="0">
    <thead>
  <tr>
    <th  colspan="2" align="left">Top 10 earners</th>
  </tr>
  </thead>
  <?php foreach($top_earners as $earners) { ?>
  <tr>
    <td align="left"><?php echo $earners["username"] ?></td>
    <td align="left">$<?php echo $earners["earned"] ?></td>
  </tr>
  <?php } ?>
  </table>
   </td>
  </tr>
</table>