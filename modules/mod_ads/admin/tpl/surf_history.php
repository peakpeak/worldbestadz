<h1>Surf History</h1>

<p class="info">Here you can view a users surf history</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

<div class="box" style="margin-bottom:10px">
<form action="index.php" method="get" >
<input  type="hidden" name="mod" value="ads" />
<input  type="hidden" name="go" value="surf_history" />
  <table cellpadding="0" cellspacing="0" class="utility">
  
  
    <tr style="background-color:transparent">
      <td style="position:relative" colspan="2">
	   <script>$(function() { jQueryui( "#datepicker_1" ).datepicker({ dateFormat: 'yy-mm-dd' });});</script>
	   <script>$(function() { jQueryui( "#datepicker_2" ).datepicker({ dateFormat: 'yy-mm-dd' });});</script>
	   
	    Date: 
	    <input type="text" name="time_from" value="<?php echo $var["time_from"]?>" size="12"  id="datepicker_1" />&nbsp;-&nbsp;
	    <input type="text" name="time_to" value="<?php echo $var["time_to"]?>" size="12" id="datepicker_2"/>	  
		Sites: 
	    <input type="text" name="total_from" value="<?php echo $var["total_from"]?>" size="10" />&nbsp;-&nbsp;
	    <input type="text" name="total_to" value="<?php echo $var["total_to"]?>" size="10"  />
				 
	 </td>
     
    </tr>
    <tr style="background-color:transparent">
	
     <td  class="border"> Username: <input type="text" name="username" value="<?php echo $var["username"]?>"  /></td>
     <td align="left" class="border">
	 
	 Items Per Page: 
	 <select name="per_page">
	 <option value="10" <?php if( $var["per_page"] == '10'){ ?> selected="selected" <?php } ?>>10</option>
     <option value="25" <?php if( $var["per_page"] == '25'){ ?> selected="selected" <?php } ?>>25</option>
     <option value="50" <?php if( $var["per_page"] == '50'){ ?> selected="selected" <?php } ?>>50</option>
     <option value="100"<?php if( $var["per_page"] == '100'){ ?> selected="selected" <?php } ?>>100</option>
     </select>
     &nbsp;&nbsp;
    <!-- Go To: 
	 <select class="select" >
	 <option value="1" selected="selected">1</option>
     </select>-->
	 <input  class="button-alt-sml" type="button" name="clear"  value="Clear Filters" onclick="window.location.href='?mod=ads&go=surf_history'"  />
	 <input  class="button-alt-sml" type="submit" name="submit"  value="Search"  /></td>
    </tr>
  </table>
</form>
</div>


<table  cellpadding="0" cellspacing="0"  class="tableS">
 <thead>
  <tr>
    <th class="left">No</th>
	<th class="left">Date</th>
	<th class="left">Last Surfed Time</th>
    <th class="left">Username</th>
	<th class="left">Site(s)</th>
  </tr>
  </thead>
  
  <?php  $i = 0; foreach ($history as $id => $row){ $i++; ?>
   
  <tr>
     
    <td align="left"><?php echo $id; ?></td>
	<td align="left"><?php echo $row['date']; ?></td>
	<td align="left"><?php echo $row['time']; ?></td>
    <td align="left"><?php echo $row['username']; ?></td>
	<td align="left"><?php echo $row['surfed']; ?></td>
	
  </tr>
	   <?php } ?>
 
       <?php if(count($history) == 0) { ?><tr> <td colspan="5"  align="center">No records found</td></tr><?php } ?>

     <tr>
	    <td colspan="10" align="center"> <?php echo $system->getPaginationString($page,$num_rows,$per_page,3,$system->curPageURL(),"page");?></td>
	 </tr>

  </table>

<script language="javascript">
<!-- Begin

    var checkflag = "false";

    function check(field) {
    var checks = document.getElementsByName('list[]');
    if (checkflag == "false") {
     for (i = 0; i < checks.length; i++){
      checks[i].checked = true;
     }
      checkflag = "true";
      return "Uncheck All";
   }
   else {
     for (i = 0; i < checks.length; i++) {
      checks[i].checked = false; }
      checkflag = "false";
      return "Check All"; 
  }
}


</script>
<script type="text/javascript">
 
   
	 var first = 0;
	 var last = <?php echo $i;?>;	
	  fancyjQuery(document).ready(function() {
			
			for (i = first; i  <= last; i++) {
			
			fancyjQuery("#popup"+i).fancybox({
				'width'				: '90%',
				'height'			: '100%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
          }
		
		});
  </script>