<h1>Reported Websites</h1>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>
	
	
	<form  action="" method="post">
    <input type="hidden" name="rid" value="<?php echo $var['rid']?>" />
	<table cellpadding="0" cellspacing="0"   class="tableS">
	<thead>
	<tr>
		<th colspan="2" align="left"><a href="?mod=ads&go=reports">Reported Websites</a> > Report Details</th>	
	</tr>
	</thead>
	 <?php if(count($report) == 0) : ?>
	 
      <tr>
		<td colspan="2"> - no records found - </td>
	  </tr>
	  <?php else: ?> 
        <tr>
						
						<td width="30%">Reporter</td>
                        <td nowrap="nowrap"><?php echo $report["reporter"]?></td>
		</tr>
		<tr>
						
						<td>Website Owner</td>
                        <td nowrap="nowrap"><?php echo $report["website_owner"]?></td>
		</tr>
		<tr>
						
						<td>Website</td>
                        <td nowrap="nowrap"><?php echo $report["url"]?></td>
		</tr>
		<tr>
						
						<td>Website Status</td>
                        <td nowrap="nowrap"><?php echo $report["website_status"]?></td>
		</tr>
		<tr>
						
						<td>Review Status</td>
                        <td nowrap="nowrap"><?php echo $report["review_status"]?></td>
		</tr>
		<tr>
						
						<td>Reporter Comment</td>
                        <td nowrap="nowrap"><?php echo $report["user_comment"]?></td>
	  </tr>
	   <tr>
	   
	   <tr>
						<td>Comments</td>
                        <td><textarea rows="2" cols="15"  name="admin_comment" ><?php echo $report["admin_comment"]?></textarea></td>
	   </tr>
	   
	   <tr>
	    <td>Action</td>
        <td>
		
	           <input type="submit" name="Submit" value="Suspend" id="button" onclick="return(confirm('Do you really want to suspend?'))" <?php echo ($report["review_status"] == 'Completed')? "disabled ='disabled'":"" ?>/>
	           <input type="submit" name="Submit" value="Delete" id="button" onclick="return(confirm('Do you really want to delete?'))" <?php echo ($report["review_status"] == 'Completed')? "disabled ='disabled'":"" ?>/>
	           <input type="submit" name="Submit" value="Ignore" id="button" <?php echo ($report["review_status"] == 'Completed')? "disabled ='disabled'":"" ?>/>
         
		 
       </td>
     </tr>
       
     <?php endif; ?> 
	</table>
   
</form>

	