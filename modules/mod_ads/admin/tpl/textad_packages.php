<h1>Text Ad Sections</h1>

<p class="info">Here you can manage text ad packages</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

<br />

{#include=form}

<br />


<table  cellpadding="0" cellspacing="0"   class="tableS">
  <thead>
  <tr>
    <th class="left">Name</th>
	<th class="left">Type</th>
	<th class="left">Text Ad Dimensions</th>
	<th class="left">Code</th>
    <th class="right">Action</th>
  </tr>
  </thead>
  <?php $i = 0; foreach ($packages as $num => $row){ $i++; ?> 
  <tr>
     <td align="left"><?php echo $row['name'] ?></td>
	 <td align="left"><?php echo $row['type'] ?></td>
	 <td align="left"><?php echo $row['length'] ?> x <?php echo $row['height'] ?></td>
	 <td align="left"><input  type="text" value="{&#35;widget=ads::textad?pid:<?php echo $row["id"]?>&display:vertical&limit:1}" size="60"/>
	 <img src="images/tooltip.png" alt="Tip" class="tooltip" title="Copy this code and place anywhere in your html template for ads to be displayed.
You can change the display behaviour by changing the parameters below:
 
'display'  - This can either be set to vertical or horizontal
'limit' - Set this to the number of ads you want to be displayed. Example: setting this to 3 will display 3 text ads from this package ( Note that if you the spots available for this package is 1, Only 1 text ad will be displayed 3 times )" /></td>
     <td align="right">
	 <a href="?mod=ads&go=textad_packages&edit=y&pid=<?php echo $row["id"]?>"><img src="images/edit.png" class="tooltip img-wrap2"  alt="" title="Edit"/></a>
	 <?php if($row['default'] != 'Y') { ?>
	 <a href="?mod=ads&go=textad_packages&delete=y&pid=<?php echo $row["id"]?>" onclick="return(confirm('Do you really want to delete?'))"><img src="images/delete.png" class="tooltip img-wrap2"  alt="" title="Delete"/></a></td>
	 <?php } else { ?>
     <a href="#"><img src="images/delete2.png" class="tooltip img-wrap2"  alt="" title=""/></a>
	 <?php } ?>
	
  </tr>

  <?php } ?>
 
  <?php if(count($packages) == 0) { ?><tr><td colspan="5"  align="center">No packages found</td></tr><?php } ?> 
  	 
     <tr>
	    <td colspan="5" align="center"> <?php echo $system->getPaginationString($page,$num_rows,$per_page,3,$system->curPageURL(),"page");?></td>
	 </tr>

  </table>
