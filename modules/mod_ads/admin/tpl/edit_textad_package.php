<form  method="post" action="">
<input type="hidden" name="edit" value="y" />
<input type="hidden" name="pid" value="<?php echo $erow['id']?>" />
<table cellpadding="0" cellspacing="0"   class="tableS">
<thead>
   <tr >
    <th colspan="2"  class="left">Edit Text Ad Package</th>
  </tr>
  </thead>
 <tr>
     <td width="30%">Name</td>
     <td><input type="text" name="name" value="<?php echo $erow['name'] ?>"></td>
  </tr>
   <tr>
     <td>Dimension</td>
     <td><input type="text" name="length" value="<?php echo $erow['length'] ?>" size="5"> x <input type="text" name="height" value="<?php echo $erow['height'] ?>" size="5"><img src="images/tooltip.png" alt="Tip" class="tooltip" title="Add dimension of your ad in format ( W X H ) example 25 x 125, 468 x 60, 728 x 90" /></td>
  </tr>
   <tr>
     <td>Maximum characters of text ad title </td>
     <td><input type="text" name="max_title" value="<?php echo $erow['max_title'] ?>"></td>
  </tr>
   <tr>
     <td>Maximum characters of text ad body </td>
     <td><input type="text" name="max_body" value="<?php echo $erow['max_body'] ?>"></td>
  </tr>
  <tr>
     <td>Credits Per Exposure</td>
     <td><input type="text" name="credits_per_exposure" value="<?php echo $erow['credits_per_exposure'] ?>"><img src="images/tooltip.png" alt="Tip" class="tooltip" title="This is the amount of text credit that will be debited from a users account every time their ad is displayed" /></td>
  </tr>
  <thead>
   <tr >
    <th colspan="2"  class="left">Default Text <img src="images/tooltip.png" alt="Tip" class="tooltip" title="Add a text to be displayed in absence of any text ad in this package" /></th>
  </tr>
  </thead>
   <tr>
     <td>Text</td>
     <td><textarea name="default_text" cols="17" rows="4"><?php echo $erow["default_text"]?></textarea></td>
  </tr>
   <tr>
     <td>Target URL</td>
     <td><input type="text" name="default_url" value="<?php echo $erow["default_url"]?>"></td>
  </tr>
  <tr>
    <td colspan="2" class="left"> <input type="submit" name="Submit" value="Edit" id="button"/> </td>
  </tr>
  </table>
</form>
