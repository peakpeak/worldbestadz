<form  method="post" action="">
<table cellpadding="0" cellspacing="0"   class="tableS">
  <thead>
  <tr >
    <th colspan="2"  class="left"><a href="?mod=ads&go=rotator_packages">Manage Rotators</a> > New Rotator</th>
  </tr>
  </thead>
 <tr>
     <td width="30%">Name</td>
     <td><input type="text" name="name" value=""></td>
  </tr>
   <tr>
     <td>Timer</td>
     <td><input type="text" name="surf_timer" value=""><img src="images/tooltip.png" alt="Tip" class="tooltip" title="This is time in seconds a website will be displayed on the surf rotator before viewers can proceed to view another website" /></td>
  </tr>
  <tr>
     <td>Credits Per Exposure</td>
     <td><input type="text" name="cr_per_exposure" value=""><img src="images/tooltip.png" alt="Tip" class="tooltip" title="This is the amount of website credits that will be deducted from a user's account every time their website is displayed on this rotator" /></td>
  </tr>
  <tr>
     <td>Credits Per View</td>
     <td><input type="text" name="cr_per_view" value=""><img src="images/tooltip.png" alt="Tip" class="tooltip" title="This is the amount of website credits a user earns for each website they view on this rotator" /></td>
  </tr>
 
  <tr >
    <td colspan="2" class="left"><input type="submit" name="Submit" value="Add" id="button"/></td>
  </tr>
  </table>
</form>
