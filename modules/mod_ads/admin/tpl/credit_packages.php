<h1>Credit Packages</h1>

<p class="info">Here you can manage different credit packages for members to purchase</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

<br />

{#include=form}

<br />


<table   cellpadding="0" cellspacing="0"  class="tableS">
<thead>
 <tr>
    <th class="left">Name</th>
    <th class="left">Website Credits </th>
	<th class="left">Banner Ads Credits </th>
	<th class="left">Text Ads Credits </th>
	<th class="left">Login Ads Credits </th>
    <th class="right">Action</th>
  </tr>
  </thead>
 
 <?php $i = 0; foreach ($packages as $num => $row){ $i++; ?> 
  
  <tr>
     <td ><?php echo $row['name'] ?></td>
	 <td ><?php echo $row['website_credits'] ?></td>
	 <td ><?php echo $row['banner_credits'] ?></td>
	 <td ><?php echo $row['textad_credits'] ?></td>
	 <td ><?php echo $row['loginads_credits'] ?></td>
     <td  align="right"><a href="?mod=ads&go=credit_packages&edit=y&pid=<?php echo $row["id"]?>"><img src="images/edit.png" class="tooltip img-wrap2"  alt="" title="Edit"/></a> <a href="?mod=ads&go=credit_packages&delete=y&pid=<?php echo $row["id"]?>" onclick="return(confirm('Do you really want to delete?'))"><img src="images/delete.png" class="tooltip img-wrap2"  alt="" title="Delete"/></a></td>
  </tr>
  <?php } ?>
 
  <?php if(count($packages) == 0) { ?><tr><td colspan="6"  align="center">No packages found</td></tr><?php } ?> 
  	 
 
  </table>

