<h1>Manage Login Ads</h1>

<p class="info">Here you can manage login ads in the system</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

<div class="box" style="margin-bottom:10px">
<form action="index.php" method="get">
<input  type="hidden" name="mod" value="ads" />
<input  type="hidden" name="go" value="loginads" />
  <table cellpadding="0" cellspacing="0" class="utility">
    <tr style="background-color:transparent">
      <td style="position:relative">
	    Username: <input type="text" name="username" value="<?php echo $var["username"]?>"  />
		Credits: 
	    <input type="text" name="total_from" value="<?php echo $var["total_from"]?>" size="5" class="input-text-short" />&nbsp;-&nbsp;<input type="text" name="total_to" value="<?php echo $var["total_to"]?>" size="5" class="input-text-short" />
		
	  </td>
       <td align="left"><table cellpadding="0" cellspacing="0" border="0"> <tr>
	   <td>Status:</td>
	   <td><input type="checkbox" class="html-checkboxes" name="status[]" value="Active"<?php if(in_array('Active',$var["status"])){ ?> checked="checked" <?php } ?>/>Active&nbsp;</td>
<td><input type="checkbox" class="html-checkboxes" name="status[]" value="Pending"<?php if(in_array('Pending',$var["status"])){ ?> checked="checked" <?php } ?>/>Pending</td>

<td><input type="checkbox" class="html-checkboxes" name="status[]" value="Suspended"<?php if(in_array('Suspended',$var["status"])){ ?> checked="checked" <?php } ?>/>Suspended&nbsp;</td>
</tr></table></td>
    </tr>
	<tr style="background-color:transparent">
     <td class="border" colspan="2"> Website URL: <input type="text" name="url" value="<?php echo $var["url"]?>" size="60"  /></td>
    </tr>
    <tr style="background-color:transparent">
     <td align="left" class="border" colspan="2">
	 Items Per Page: 
	<select name="per_page">
	 <option value="10" <?php if( $var["per_page"] == '10'){ ?> selected="selected" <?php } ?>>10</option>
     <option value="25" <?php if( $var["per_page"] == '25'){ ?> selected="selected" <?php } ?>>25</option>
     <option value="50" <?php if( $var["per_page"] == '50'){ ?> selected="selected" <?php } ?>>50</option>
     <option value="100"<?php if( $var["per_page"] == '100'){ ?> selected="selected" <?php } ?>>100</option>
     </select>
     &nbsp;&nbsp;
    <!-- Go To: 
	 <select class="select" >
	 <option value="1" selected="selected">1</option>
     </select>-->
	 <input  class="button-alt-sml" type="button" name="clear"  value="Clear Filters" onclick="window.location.href='?mod=ads&go=loginads'"  />
	 <input  class="button-alt-sml" type="submit" name="submit"  value="Search"  />
</td>
    </tr>
  </table></form>
</div>


<form  method="post" action="">
<table  cellpadding="0" cellspacing="0"  class="tableS">
 <thead>
  <tr>
    <th >No</th>
    <th class="left">Username</th>
    <th class="left">Website</th>
    <th class="left">Credits</th>
	<th class="left">Status</th>
    <!--<th class="center">Action</th>-->
	<th >Select</th>
 
  </tr>
  </thead>
   <?php $i = 0; foreach ($loginads as $num => $row){ $i++; ?> 
  <tr>
     
    <td align="center"><?php echo $num; ?></td>
    <td ><?php echo $row['username']; ?></td>
    <td ><a href="<?php echo $row['url']; ?>" id="popup<?php echo $i?>"><?php echo $row['url']; ?></a></td>
	<td ><?php echo $row['credits']; ?></td>
	<td ><?php echo $row['status'];?></td>
	<!--<td align="center"><a href="?mod=ads&go=edit_loginad&lid=<?php echo $row["id"]?>" ><img src="images/edit.png" class="tooltip img-wrap2"  alt="" title="Edit"/></a>    <a href="?mod=ads&go=loginads&lid=<?php echo $row["id"]?>" onclick="return(confirm('Do you really want to delete?'))"><img src="images/delete.png" class="tooltip img-wrap2"  alt="" title="Delete"/></a></td>-->
	<td align="center"><input type="checkbox" name="list[]" value="<? echo $row["id"]?>"></td>
  </tr>


  <?php } ?>
 
  <?php if(count($loginads) == 0) { ?><tr><td colspan="6"  align="center">No ads found</td></tr><?php } ?> 
  	 
     <tr>
	    <td colspan="6" align="center"> <?php echo $system->getPaginationString($page,$num_rows,$per_page,3,$system->curPageURL(),"page");?></td>
	 </tr>

        

 
  <tr>
    <td colspan="6" ><div align="center">
      <input type=button value="Check All" onclick="this.value=check(this.form.checkbox)" id="button" />
	  <input type="submit" name="Submit" value="Activate" id="button"/>
	  <input type="submit" name="Submit" value="Suspend" id="button"/>
	  <input type="submit" name="Submit" value="Delete" id="button" onclick="return(confirm('Do you really want to delete?'))"/>
	  
     </div>
   </td>
  </tr>
  </table>
</form>
<script language="javascript">
<!-- Begin

    var checkflag = "false";

    function check(field) {
    var checks = document.getElementsByName('list[]');
    if (checkflag == "false") {
     for (i = 0; i < checks.length; i++){
      checks[i].checked = true;
     }
      checkflag = "true";
      return "Uncheck All";
   }
   else {
     for (i = 0; i < checks.length; i++) {
      checks[i].checked = false; }
      checkflag = "false";
      return "Check All"; 
  }
}
//  End -->
</script>

<script type="text/javascript">
 
   
	 var first = 0;
	 var last = <?php echo $i;?>;	
	  fancyjQuery(document).ready(function() {
			
			for (i = first; i  <= last; i++) {
			
			fancyjQuery("#popup"+i).fancybox({
				'width'				: '100%',
				'height'			: '100%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
          }
		
		});
  </script>




