<h1>Manage Website Rotators</h1>

<p class="info">Here you can manage website rotators</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

<br />

{#include=form}

<br />


<table  cellpadding="0" cellspacing="0"   class="tableS">
  <thead>
  <tr>
    <th class="left">Name</th>
	<th class="left">Surf Timer</th>
	<th class="left">Exchange Ratio</th>
    <th class="right">Action</th>
  </tr>
  </thead>
  <?php $i = 0; foreach ($packages as $num => $row){ $i++; ?> 
  <tr>
     <td align="left"><a href="{#loader=system::url}/members/ads/rotator/rotator/<?php echo $row['id'] ?>" target="_blank"><?php echo $row['name'] ?></a></td>
	 <td align="left"><?php echo $row['surf_timer'] ?></td>
	 <td align="left"><?php echo $row['cr_per_exposure'] ?>:<?php echo $row['cr_per_view'] ?></td>
     <td align="right">
	 <a href="?mod=ads&go=rotator_packages&edit=y&pid=<?php echo $row["id"]?>"><img src="images/edit.png" class="tooltip img-wrap2"  alt="" title="Edit"/></a>
	 <?php if($row['default'] != 'Y') { ?>
	  <a href="?mod=ads&go=rotator_packages&delete=y&pid=<?php echo $row["id"]?>" onclick="return(confirm('Do you really want to delete?'))"><img src="images/delete.png" class="tooltip img-wrap2"  alt="" title="Delete"/></a>
	 <?php } else { ?>
     <a href="#"><img src="images/delete2.png" class="tooltip img-wrap2"  alt="" title=""/></a>
	 <?php } ?>
	</td>
  </tr>

  <?php } ?>
 
  <?php if(count($packages) == 0) { ?><tr><td colspan="5"  align="center">No rotators found</td></tr><?php } ?> 
  	 
     <tr>
	    <td colspan="5" align="center"> <?php echo $system->getPaginationString($page,$num_rows,$per_page,3,$system->curPageURL(),"page");?></td>
	 </tr>

  </table>
