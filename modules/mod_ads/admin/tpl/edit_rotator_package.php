<form  method="post" action="">
<input type="hidden" name="edit" value="y" />
<input type="hidden" name="pid" value="<?php echo $erow['id']?>" />
<table cellpadding="0" cellspacing="0"   class="tableS">
<thead>
  <tr>
    <th colspan="2" align="left" ><a href="?mod=ads&go=rotator_packages">Manage Rotators</a> > Edit Rotator</th>
  </tr>
  </thead>
  <tr>
     <td width="30%">Name</td>
     <td><input type="text" name="name" value="<?php echo $erow['name'] ?>"></td>
  </tr>
   <tr>
     <td>Timer</td>
     <td><input type="text" name="surf_timer" value="<?php echo $erow['surf_timer'] ?>"><img src="images/tooltip.png" alt="Tip" class="tooltip" title="This is time in seconds a website will be displayed on the surf rotator before viewers can proceed to view another website" /></td>
  </tr>
  <tr>
     <td>Credits Per Exposure</td>
     <td><input type="text" name="cr_per_exposure" value="<?php echo $erow['cr_per_exposure'] ?>"><img src="images/tooltip.png" alt="Tip" class="tooltip" title="This is the amount of website credits that will be deducted from a user's account every time their website is displayed on this rotator" /></td>
  </tr>
  <tr>
     <td>Credits Per View</td>
     <td><input type="text" name="cr_per_view" value="<?php echo $erow['cr_per_view'] ?>"><img src="images/tooltip.png" alt="Tip" class="tooltip" title="This is the amount of website credits a user earns for each website they view on this rotator" /></td>
  </tr>
 
 
  <tr>
    <td colspan="2" class="left"> <input type="submit" name="Submit" value="Edit" id="button"/> </td>
  </tr>
  </table>
</form>
