<?php
global $system,$db,$settings,$admininfo;

$system->importClass('account')->checkPrivilege(2);

$var = $system->getVar();

if(!isset($var['wid'])) $system->invalidRequest();

$traffic = $system->importClass('ads');

$packages = $traffic->getRotatorPackages();

$categories = $traffic->getAdCategories();




   
switch($var['Submit'])
{
		
	case"Edit":
	if($var['username'] == null || !Account::isUser($var['username']))
    {
        $errors[] =  "Add a valid username";
    }
    elseif($var['title']=='')
	{
		$errors[] =  "Add a title";
	}
	 elseif($var['name']=='')
	{
		$errors[] =  "Add a website name";
	}
	elseif($var['url']=='' || !Validate::url($var['url']))
	{
		$errors[] =  "Add a valid URL";
	}
	elseif (count($var['categories']) < 1 && $settings["ads"]["categorized_ads"])
	{
		$errors[] =  "You must select one category at least";
	}
	else
	{	  
	   
	   $sql="SELECT name FROM ".PREFIX."_ads_websites WHERE id!='".$var['wid']."' AND username = '".$var['username']."' AND name='".$var['name']."' LIMIT 1";
	   $res = $db->query_db($sql,$print = DEBUG);
       if ($db->num_rows($res) > 0)  $errors[] =  "A website with this name already exist"; 
	   
	   $sql="SELECT url FROM ".PREFIX."_ads_websites  WHERE id!='".$var['wid']."' AND username = '".$var['username']."' AND url='".$var['url']."' AND package='".$var['package']."' LIMIT 1";
	   $res = $db->query_db($sql,$print = DEBUG);
       if ($db->num_rows($res) > 0)  $errors[] =  "This website already exist in the choosen rotator";   
	}
	
	if(is_array($errors) && !empty($errors))
	{
		while (list($key,$value) = each($errors)) $return_msg.= $value.'<br>';		
	}
	else 
	{
	    //Add Website		
	   if (count($var['day']) > 0)	   
         foreach ($var['day'] as $v)  $display .= ","."$v".",";               
	   else $display = "";
	   
	    if (count($var['categories']) > 0) { foreach ($var['categories'] as $v)  $scategories .= $v.","; $scategories = rtrim($scategories,","); }               
	    else $scategories = "";
		
		$updated = $traffic->updateWebsite($var['username'],$var['wid'],$var['name'],$var['title'],$var['url'],$scategories,array($var['geo_target'],$var['country']),$var['package'],$display,$var['limit'],$var['start']);
		
		if (is_numeric($var['credits']) && $var['credits']> 0)
		{
		      $update_array = array('credits' => $var['credits']);
			  $db->update_db($update_array,"_ads_websites","id",$var['wid']);
		}
		
		if($updated) $return_msg = 'Website Edited';	
	}
		break;
		
	}
	
	
   $sql = "SELECT *  FROM ".PREFIX."_ads_websites WHERE id='".$var['wid']."' LIMIT 1";
   $res = $db->query_db($sql,$print = DEBUG);
   if ($db->num_rows($res) > 0) $website = $db->fetch_db_array($res);
   
   $check_sun =((stristr($website['display_days'],",Sun,")))? "checked=\"checked\" ": ""; 
   $check_mon =((stristr($website['display_days'],",Mon,")))? "checked=\"checked\" ": ""; 
   $check_tue =((stristr($website['display_days'],",Tue,")))? "checked=\"checked\" ": ""; 
   $check_wed =((stristr($website['display_days'],",Wed,")))? "checked=\"checked\" ": ""; 
   $check_thur =((stristr($website['display_days'],",Thu,")))? "checked=\"checked\" ": ""; 
   $check_fri =((stristr($website['display_days'],",Fri,")))? "checked=\"checked\" ": ""; 
   $check_sat =((stristr($website['display_days'],",Sat,")))? "checked=\"checked\" ": ""; 
   
   
   $selected_categories = explode(",",$website['category']);

   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'edit_website.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();
	


?>
