<?php
global $system,$db,$settings,$admininfo;

$system->importClass('account')->checkPrivilege(2);

$var = $system->getVar();

if(!isset($var['tid']))$system->invalidRequest();

$traffic = $system->importClass('ads');

$packages = $traffic->getTextAdPackages();

$categories = $traffic->getAdCategories();
   
switch($var['Submit'])
{
		
	case"Edit":
	if($var['username'] == null || !Account::isUser($var['username']))
    {
        $errors[] =  "Add a valid username";
    }
    elseif($var['name']=='')
	{
		$errors[] =  "Add a textad name";
	}
    elseif($var['title']=='')
	{
		$errors[] =  "Add a title";
	}
	elseif($var['body']=='')
	{
		$errors[] =  "Add a body content";
	}
	
	elseif($var['url']=='' || !Validate::url($var['url']))
	{
		$errors[] =  "Add a valid URL";
	}
	elseif (count($var['categories']) < 1 && $settings["ads"]["categorized_ads"])
	{
		$errors[] =  "You must select one category at least";
	}
	elseif (strlen($var['title']) > $packages[$var['package']]['max_title'])
	{
		$errors[] =  "Maximum character allowed for text ad title is ".$packages[$var['package']]['max_title'];
	}
	elseif (strlen($var['body']) > $packages[$var['package']]['max_body'])
	{
		$errors[] =  "Maximum character allowed for text ad body is ".$packages[$var['package']]['max_title'];
	}
	else
	{	  
	   
	   $sql="SELECT name FROM ".PREFIX."_ads_textads WHERE id!='".$var['tid']."' AND username = '".$var['username']."' AND name='".$var['name']."' LIMIT 1";
	   $res = $db->query_db($sql,$print = DEBUG);
       if ($db->num_rows($res) > 0)  $errors[] =  "A text ad with this name already exist"; 
	   
	   $sql="SELECT url FROM ".PREFIX."_ads_textads  WHERE id!='".$var['tid']."' AND username = '".$var['username']."' AND url='".$var['url']."' AND package='".$var['package']."' LIMIT 1";
	   $res = $db->query_db($sql,$print = DEBUG);
       if ($db->num_rows($res) > 0)  $errors[] =  "This text ad already exist";   
	}
	
	if(is_array($errors) && !empty($errors))
	{
		while (list($key,$value) = each($errors)) $return_msg.= $value.'<br>';		
	}
	else 
	{
	    //Edit Text Ad		
	   if (count($var['day']) > 0)	   
         foreach ($var['day'] as $v)  $display .= ","."$v".",";               
	   else $display = "";
	   
	    if (count($var['categories']) > 0) { foreach ($var['categories'] as $v)  $scategories .= $v.","; $scategories = rtrim($scategories,","); }               
	    else $scategories = "";
		
		$updated = $traffic->updateTextAd($var['username'],$var['tid'],$var['name'],$var['title'],$var['body'],$var['url'],$scategories,array($var['geo_target'],$var['country']),$var['package'],$display,$var['limit'],$var['start']);
		
		if (is_numeric($var['credits']) && $var['credits']> 0)
		{
		      $update_array = array('credits' => $var['credits']);
			  $db->update_db($update_array,"_ads_textads","id",$var['tid']);
		}
		
		if($updated) $return_msg = 'Text Ad Edited';	
	}
		break;
		
	}
	
	
   $sql = "SELECT *  FROM ".PREFIX."_ads_textads WHERE id='".$var['tid']."' LIMIT 1";
   $res = $db->query_db($sql,$print = DEBUG);
   if ($db->num_rows($res) > 0) $textad = $db->fetch_db_array($res);
   
   $check_sun =((stristr($textad['display_days'],",Sun,")))? "checked=\"checked\" ": ""; 
   $check_mon =((stristr($textad['display_days'],",Mon,")))? "checked=\"checked\" ": ""; 
   $check_tue =((stristr($textad['display_days'],",Tue,")))? "checked=\"checked\" ": ""; 
   $check_wed =((stristr($textad['display_days'],",Wed,")))? "checked=\"checked\" ": ""; 
   $check_thur =((stristr($textad['display_days'],",Thu,")))? "checked=\"checked\" ": ""; 
   $check_fri =((stristr($textad['display_days'],",Fri,")))? "checked=\"checked\" ": ""; 
   $check_sat =((stristr($textad['display_days'],",Sat,")))? "checked=\"checked\" ": ""; 
   
   
   $selected_categories = explode(",",$textad['category']);

   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'edit_textad.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();
	


?>
