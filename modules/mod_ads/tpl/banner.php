<?php if($package) { ?>
<style type="text/css">
#bannerads_<?php echo $pid?> {
    background-color:transparent;
	font-family:verdana,arial,sans-serif;
	font-size:10px;
	margin:5px 5px 20px 5px;
	}
#bannerads_<?php echo $pid?> table{
    margin:0px;
    padding:0px;
    table-layout:fixed;
    width:100%;
    height:100%;
    border-spacing:0px;
    border-collapse:collapse
   }
#bannerads_<?php echo $pid?> td{
   /* padding:0px 6px 5px 0px;*/
    vertical-align:middle
	}
#bannerads_<?php echo $pid?> .w{
    width:<?php echo $w;?>px;
	height:<?php echo $h;?>px
  }
#bannerads_<?php echo $pid?> .h{
    position:absolute;
    display:none;
    background-color:#fffff0;
    color:#222;
    left:5px;
    bottom:1px;
    font-family:arial;
    font-size:9px;
   /* padding:1px 3px 0px 0px;*/
    cursor:pointer
  }
#bannerads_<?php echo $pid?> .w:hover #bannerads_<?php echo $pid?> .h{
    display:block
}
#bannerads_<?php echo $pid?> .c{
  margin:0px;height:<?php echo $h;?>px;
  overflow:hidden;
  border-left:1px solid #999999;
  border-right:1px solid #999999;
  /*padding-left:6px;
  padding-bottom:9px;*/
  background-color:#fffff0
  }
#bannerads_<?php echo $pid?> .t{
   font-size:12px;
   font-weight:bold;
   overflow:hidden;
  }
#bannerads_<?php echo $pid?> .t #bannerads_<?php echo $pid?> a{
   color:#335599;
   font-size:13px;
  }
#bannerads_<?php echo $pid?> .d{
   color:#555555;font-size:12px;
   display:block;
   overflow:hidden
   }
#bannerads_<?php echo $pid?> .u{font-size:10px;
   line-height:12px;
   overflow:hidden;
   white-space:nowrap
   }
#bannerads_<?php echo $pid?> .u a{
   color:#339955;
   text-decoration:none
   }
#bannerads_<?php echo $pid?> .r1,#bannerads_<?php echo $pid?> .r2{
   border-color:transparent #999999;
   border-style:none solid;
   background-color:#fffff0
  }
#bannerads_<?php echo $pid?> .b{margin:0px 4px;height:0px;border-top:1px solid #999999}
#bannerads_<?php echo $pid?> .r1{margin:0px 2px;height:1px;border-width:0px 2px}
#bannerads_<?php echo $pid?> .r2{margin:0px 1px;height:2px;border-width:0px 1px}


</style>


<div id="bannerads_<?php echo $pid?>">
<div class="w">
   <div class="b"></div>
   <div class="r1"></div>
   <div class="r2"></div>
   <div class="c">
   <table>
   <tbody>
   
	   <?php if($banner == ""){ ?>
	 
	      <tr>
	         <td style="width:100%;" align="center"><div class="t"><a target="_blank" href="{#loader=system::url}/members/ads/banners">PUT YOUR BANNER AD HERE</a></div></td>
	      </tr>
		
	   <?php } else { ?>
	   
	      <tr>
	         <td style="width:100%;" align="center"><?php echo $banner;?></td>
	      </tr>
	   
	   <?php } ?>
    
	</tbody></table>
	</div>
	
	
	 <div class="r2"></div>
	 <div class="r1"></div>
	 <div class="b"></div>
	 </div>
	 </div>
<?php } ?>