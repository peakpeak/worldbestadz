<span  class="form_title">Referral Contests</span>
<?php if($error_msg){ ?>
<span class="error_message"><?php echo $error_msg;?></span>
<? } ?>
<?php if($success_msg){ ?>
<span class="success_message"><?php echo $success_msg;?></span>
<? } ?>
<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td width="430" height="200" align="center" valign="top"><div class="white">
        <table  cellspacing=1 cellpadding=2 border=0 width="100%" class="table_2">
          <thead>
            <tr>
              <th>Name</th>
              <th>Start Date</th>
              <th>End Date</th>
              <th>Referrals</th>
              <th>Status</th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <?php  $i=0; foreach ($contests as $pnum => $pdata) {  $i++; ?>
          <tr>
            <td><?php echo $pdata['name']?></td>
            <td><?php echo date("D, d M Y", strtotime($pdata['start_date']));?></td>
            <td><?php echo date("D, d M Y", strtotime($pdata['end_date']));?></td>
            <td><?php echo $pdata['referrals']?></td>
            <td><?php echo $pdata['status']?></td>
            <td align="center"><b><a href="{#loader=system::url}/promo/contest_details/cid/<?php echo $pdata['id']?>" id="popup<?php echo $i?>">Details</a></b></td>
          </tr>
          <?php } ?>
          <?php if(count($contests) == 0) { ?>
          <tr>
            <td colspan="6" align=center > no records found </td>
          </tr>
          <?php } ?>
        </table>
      </div></td>
  </tr>
</table>
<script type="text/javascript">
 
   
	 var first = 0;
	 var last = <?php echo $i;?>;	
	  fancyjQuery(document).ready(function() {
			
			for (i = first; i  <= last; i++) {
			
			fancyjQuery("#popup"+i).fancybox({
				'width'				: '60%',
				'height'			: '100%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
          }
		
		});
  </script>
