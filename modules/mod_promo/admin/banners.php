<?php
global $system,$db,$userinfo,$settings,$admininfo;

$system->importClass('account')->checkPrivilege(3);

$system->importClass('traffic_exchange');

$var = $system->getVar();

if ($var['add'] == 'y') {
       
	  $finfo = new finfo(FILEINFO_MIME_TYPE);
      $fileContents = file_get_contents($_FILES['upfile']['tmp_name']);
      $mimeType = $finfo->buffer($fileContents);
	  
	  if(!($mimeType == "image/gif" || $mimeType == "image/jpeg" || $mimeType == "image/pjpeg")) $errors[] =  "Only JPEG or GIF image type accepted";
      elseif($_FILES['upfile']['size'] >= $settings['promo']['maximum_upload_size']) $errors[] =  "Maximum upload size is ".$settings['promo']['maximum_upload_size'];
      if(is_array($errors) && !empty($errors)) while (list($key,$value) = each($errors)) $return_msg.= $value.'<br>';	
      else{
              switch ($_FILES['upfile']['type'])
			  {
                  case "image/gif":$ext = ".gif";break;
                  case "image/pjpeg":case "image/jpeg":$ext = ".jpg";break;
              }
         
			  $url = $system->pathToUrl($settings['promo']['banner_directory']).$_FILES['upfile']['name']; //$simgurl = '/banners/' . $_FILES['upfile']['name'];
			  $size = getimagesize($_FILES['upfile']['tmp_name']);
			  $db->query_db("INSERT INTO ".PREFIX."_promo_banners (url,width,height) VALUES ('".$url."','".$size[0]."','".$size[1]."')");
			  $lastid = $db->id_db();
			  if(copy($_FILES['upfile']['tmp_name'],$settings['promo']['banner_directory']."ban".$lastid.$ext))
			  {
			     $url = $system->pathToUrl($settings['promo']['banner_directory'])."ban".$lastid.$ext;
			     $db->query_db("UPDATE ".PREFIX."_promo_banners SET url='".$url."' WHERE id='$lastid'");
				 
		      } else {
			  
			     $db->query_db("DELETE FROM ".PREFIX."_promo_banners WHERE id='$lastid'");
				 $return_msg = 'Could not upload banner';
			  }
			  
        }
    } 
	
	
	if ($var['delete'] == 'y' && is_numeric($var['bid']))
	{
	
     
	 
		$sql = "SELECT url FROM ".PREFIX."_promo_banners WHERE id = '".$var['bid']."' LIMIT 1";
		$result = $db->query_db($sql,$print = DEBUG);
		if($db->num_rows($result) > 0)
		{
		  $row = $db->fetch_db_array($result);
		  $bform = $var['bid'];
          $dext = $row["url"];
		  $dext = substr($dext, -4);
          unlink($settings['promo']['banner_directory']."ban".$bform.$dext);
          $db->query_db("DELETE from ".PREFIX."_promo_banners WHERE id='$bform' LIMIT 1");
		}
	}
	
    $sql = "SELECT id,url,width,height FROM ".PREFIX."_promo_banners ORDER BY id ASC";
	$result = $db->query_db($sql,$print = DEBUG);
    if($db->num_rows($result) > 0)while ($row = $db->fetch_db_array($result)) $banners[$row["id"]] = $row;
	
		
		
	
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'banners.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();
	
?>
  
 