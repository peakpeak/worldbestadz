<?php
global $system,$db,$settings;

$system->importClass('account')->checkPrivilege(3);

$var = $system->getVar();

   

    switch($var['Submit'])
	{
		case"Add":
        if(empty($var['name']))
        {
                $errors[] =  "Add a valid name";
        }
		elseif($db->if_row_exist("_promo_referral_contests",array('name' => $var['name'])))$errors[] =  "Contest name already exist";
		
		//dates
		if($var['start_date'] == '' || $var['end_date'] == '') $errors[] =  "Invalid dates";
		else 
		{
		    $time1 = explode("-",$var['start_date']);
		    $time_from = $time1[0]."-".$time1[1]."-".$time1[2];
		
		    $time2 = explode("-",$var['end_date']);
		    $time_to = $time2[0]."-".$time2[1]."-".$time2[2];
			
			if(!checkdate($time1[1],$time1[2],$time1[0]) || !checkdate($time2[1],$time2[2],$time2[0]))  $errors[] =  "Invalid dates";
			elseif(strtotime($var['start_date']) >= strtotime($var['end_date']))$errors[] =  "Invalid dates";
		}
		
	
        if(!is_numeric($var['positions']))
        {
                $errors[] =  "Add a valid position entry";
        } 
        if(is_array($errors) && !empty($errors)) while (list($key,$value) = each($errors)) $return_msg.= $value.'<br>';	
        else 
	    {   
		       $table = "_promo_referral_contests";
               $insert_array = array(
                'name' => $var['name'],
                'start_date' => $var['start_date'],
				'end_date' => $var['end_date'],
                'positions' => $var['positions'],
				'prize_description'=>$var['safe_input']['prize_description'],
                'disallowed_usernames'=>$var['disallowed_usernames'],
                'status' => $var['status'],
               );
               $added = $db->insert_into_db($insert_array, $table);
               if($added) $return_msg = "Contest added";	
	    }

        break;
		
		case"Edit":
		$sql = "SELECT id,name FROM ".PREFIX."_promo_referral_contests WHERE id = '".$var['cid']."' LIMIT 1";
		$res = $db->query_db($sql,$print = DEBUG);
		if ($db->num_rows($res) > 0) {
		
		
	    $row = $db->fetch_db_array($res);
        if(empty($var['name']))
        {
                $errors[] =  "Add a valid name";
        }
		elseif($var['name'] !=  $row['name'] && $db->if_row_exist("_promo_referral_contests",array('name' => $var['name'])))$errors[] =  "Contest name already exist";
        if(!is_numeric($var['positions']))
        {
                $errors[] =  "Add a valid position entry";
        } 
		if($var['start_date'] == '' || $var['end_date'] == '') $errors[] =  "Invalid dates";
		else 
		{
		    $time1 = explode("-",$var['start_date']);
		    $time_from = $time1[0]."-".$time1[1]."-".$time1[2];
		
		    $time2 = explode("-",$var['end_date']);
		    $time_to = $time2[0]."-".$time2[1]."-".$time2[2];
			
			if(!checkdate($time1[1],$time1[2],$time1[0]) || !checkdate($time2[1],$time2[2],$time2[0]))  $errors[] =  "Invalid dates";
			elseif(strtotime($var['start_date']) >= strtotime($var['end_date']))$errors[] =  "Invalid dates";
		}
        if(is_array($errors) && !empty($errors)) while (list($key,$value) = each($errors)) $return_msg.= $value.'<br>';	
        else 
	    {   
		       $table = "_promo_referral_contests";
               $update_array = array(
                'name' => $var['name'],
                'start_date' => $var['start_date'],
				'end_date' => $var['end_date'],
                'positions' => $var['positions'],
				'prize_description'=>$var['safe_input']['prize_description'],
                'disallowed_usernames'=>$var['disallowed_usernames'],
                'status' => $var['status'],
               );
			   $edited = $db->update_db($update_array, $table , "id", $var['cid']);
               if($edited) $return_msg = "Contest edited";	
	    } } else $return_msg  =  "Invalid package";
		
        break;
		case"Delete":
		break;
	}
	
	
if($var['delete'] == 'y' && isset($var['cid'])) 
{
	    $sql = "DELETE FROM ".PREFIX."_promo_referral_contests ";
        $sql.= "WHERE id ='".$var['cid']."' LIMIT 1 ";
        $db->query_db($sql,$print = DEBUG);
		
		$sql = "DELETE FROM ".PREFIX."_promo_referral_contests_winners ";
        $sql.= "WHERE cid ='".$var['cid']."' LIMIT 1 ";
        $db->query_db($sql,$print = DEBUG);
}
if($var['edit'] == 'y' && isset($var['cid'])) 
{
       $sql = "SELECT * FROM ".PREFIX."_promo_referral_contests WHERE id = '".$var['cid']."' LIMIT 1";
       $res = $db->query_db($sql,$print = DEBUG);
       if ($db->num_rows($res) > 0) $erow = $db->fetch_db_array($res);
}
/*$sql = "SELECT * FROM ".PREFIX."_membership_plans ORDER BY id ASC ";
$res = $db->query_db($sql,$print = DEBUG);
if ($db->num_rows($res) > 0) while ($row = $db->fetch_db_array($res)) $plans[$row["id"]] = $row;*/

$mplans = array(1=>"Free Members",2=>"Upgraded Members");

$sql = "SELECT * FROM ".PREFIX."_promo_referral_contests ORDER BY id ASC ";
$res = $db->query_db($sql,$print = DEBUG);
$prices = array();
if ($db->num_rows($res) > 0) while ($row = $db->fetch_db_array($res))
{
    $row['summary'] = 'Period '.$row['start_date'].' - '.$row['end_date'].' <br> ';	
	$contests[$row["id"]] = $row;
}

   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $loader->setVar($data);
   
   $tpl_add_file = dirname(__FILE__).DS.'tpl'.DS.'add_referral_contest.php';
   
   $tpl_edit_file = dirname(__FILE__).DS.'tpl'.DS.'edit_referral_contest.php';
   
   $loader->placeHolder['form'] =  ($var['edit'] == 'y' && isset($var['cid'])) ?  $tpl_edit_file :  $tpl_add_file;
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'referral_contests.php';
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();
   
 ?>