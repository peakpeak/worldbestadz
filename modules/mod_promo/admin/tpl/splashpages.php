<h1>Splash Pages</h1>

<p class="info">Here you can manage splashpages for members to use as a promotion tool for your website</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

<br />

{#include=form}

<br />

<h2>View Splash Pages</h2>

<form  method="post" action="">
<table cellpadding="0" cellspacing="0"   class="tableS">
<thead>
  <tr>
    <th class="left">Splashpage URL </th>
    <th >Status</th>
    <th class="right">Action</th>
  </tr>
  </thead>
  
 <?php $i = 0; foreach ($splashpages as $num => $row){ $i++; ?>
 
  <tr >
	 <td align="left" ><a href="<?php echo $row['url']; ?>" id="popup<?php echo $i?>"><?php echo $row['url']; ?></td>
     <td align="center"><?php echo $row['status'] ?></td>
     <td align="right"><a href="?mod=promo&go=splashpages&edit=y&pid=<?php echo $row["id"]?>"><img src="images/edit.png" class="tooltip img-wrap2"  alt="" title="Edit"/></a></td>
  </tr>
 
  <?php } ?>
 
  <?php if(count($splashpages) == 0) { ?> <tr> <td colspan="4"  align="center">No splash pages found</td> </tr> <?php } ?>
  
  </table>
</form>
<script language="javascript">
<!-- Begin
var checkflag = "false";
function check(field) {
var checks = document.getElementsByName('list[]');
if (checkflag == "false") {
for (i = 0; i < checks.length; i++) {
checks[i].checked = true;}
checkflag = "true";
return "Uncheck All"; }
else {
for (i = 0; i < checks.length; i++) {
checks[i].checked = false; }
checkflag = "false";
return "Check All"; }
}
//  End -->
</script>
<script type="text/javascript">
 
   
	 var first = 0;
	 var last = <?php echo $i;?>;	
	  fancyjQuery(document).ready(function() {
			
			for (i = first; i  <= last; i++) {
			
			fancyjQuery("#popup"+i).fancybox({
				'width'				: '100%',
				'height'			: '100%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
			
			
          }
		
		});
  </script>

