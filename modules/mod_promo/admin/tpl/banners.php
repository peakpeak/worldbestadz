 <h1>Banners</h1>

<p class="info">Here you can manage banners for members to use as a promotion tool for your website</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>
 
 
 <form enctype="multipart/form-data" action="" method="post">
 <input type="hidden" name="add" value="y">
 
 <table cellpadding="0" cellspacing="0"  class="tableS">
 <thead>
  <tr>
   <th colspan="2" class="left">Upload A New Banner</td>
  </tr>
 </thead>
 <tr>
   <td align=left>Only GIF & JPEG files can be uploaded.</td>
 </tr>
 <tr>
   <td align=left><input type="file" name="upfile"></td>
 </tr>
 <tr>
   <td class="left"><input type=submit value="Upload"  ></td>
 </tr>
</table></form>

<br />

<?php $i = 0; foreach ($banners as $num => $row){ $i++; ?>

 <form enctype="multipart/form-data" action="" method="post">
 <input type="hidden" name="delete" value="y">
 <input type="hidden" name="bid" value="<?php echo $row["id"];?>">
 <table cellpadding="0" cellspacing="0" class="tableS">
   <thead>
   <tr>
     <th colspan="2" class="left">Banner #<?php echo $row["id"];?></td>
   </tr>
  </thead>
 <tr>
  <td align=left><div style="padding:1px; width:<?php echo $row["width"];?>px; height:<?php echo $row["height"];?>px;border:solid 1px #CCC;"><img src="<?php echo $row["url"];?>" border="0"></div></td>
 </tr>
 <tr>
    <td align="left"><?php echo $row["url"];?></td>
 </tr>
 <tr>
    <td align="left"><textarea name="textarea" cols="75" rows="5"><a href="<?php echo SITEURL."?ref=".$admininfo["username"];?>"><img src="<?php echo $row["url"];?>" border="0"></a></textarea></td>
 </tr>
 <tr>
   <td class="left"><input type="submit" value="Delete" onclick="return(confirm('Do you really want to delete?'))"></td>
</tr>
</table>
</form>
<br />
<?php } ?>