<form  method="post" action="">
<table  cellpadding="0" cellspacing="0"  class="tableS">
<thead>
  <tr >
    <th colspan="2" class="left" >New Referral Contest</th>
  </tr>
  </thead>
  <tr>
     <td width="30%">Name</td>
     <td><input type="text" name="name" value="<?php echo $var["name"] ?>"></td>
  </tr>
  <tr>
	  <td>Start date</td>
	  <td><script>jQueryui(function() { jQueryui( "#start_date" ).datepicker({ dateFormat: 'yy-mm-dd' });});</script><input type="text" name="start_date" id="start_date" value="<?php echo $var["start_date"];?>"></td>
 </tr>
 <tr>
	 <td>End date</td>
	 <td><script>jQueryui(function() { jQueryui( "#end_date" ).datepicker({ dateFormat: 'yy-mm-dd' });});</script><input type="text" name="end_date" id="end_date" value="<?php echo $var["end_date"];?>"></td>
 </tr>
 <tr>
     <td># of Winners</td>
     <td><input type="text" name="positions" value="<?php echo $var['positions'] ?>"></td>
 </tr>
  <tr>
    <td>Prize Description</td>
    <td align="left">
      <textarea name="safe_input[prize_description]" rows="20" cols="20" style="width:100%; height:550px" class="ckeditor" id="ckeditor" ><?php echo  $var['safe_input']['prize_description']?></textarea>
   </td>
 </tr>
 <tr>
    <td>Disallowed username</td>
    <td>
      <textarea name="disallowed_usernames"><?php echo  $var['disallowed_usernames']?></textarea>
   </td>
  </tr>
  <tr>
      <td>Status</td>
       <td>
	    <select name="status" >
                 <option value="Active" <?php if($var['status'] == 'Active'){?>  selected="selected" <?php } ?>>Active</option>
                 <option value="Pending" <?php if($var['status'] == 'Pending'){?>  selected="selected" <?php } ?>>Pending</option>
                 <option value="Completed" <?php if($var['status'] == 'Completed'){?>  selected="selected" <?php } ?>>Completed</option>
         </select>
	    </td>
  </tr>
  <tr>
    <td colspan="2" class="left"><input type="submit" name="Submit" value="Add" /></td>
  </tr>
  </table>
</form>
<script src="<?php echo SITEURL.FS.$settings['system']['plugins_directory']?>/ckeditor/ckeditor.js"></script>
<script src="<?php echo SITEURL.FS.$settings['system']['plugins_directory']?>/ckeditor/custom.js"></script>
<script type="text/javascript">CKEDITOR.replace( 'ckeditor', {toolbar: advanced});</script>