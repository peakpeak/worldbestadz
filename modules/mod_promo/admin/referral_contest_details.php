<?php
global $system,$db,$settings,$admininfo;

$system->importClass('account')->checkPrivilege(2);

$promo = $system->importClass('promo');

$var = $system->getVar();

if(!is_numeric($var['cid'])) $system->invalidRequest();


if($var['update'] == 'y')
{
 
        for ($i = 0; $i < count ($var['list']); $i++)
		{
			
			$n 	= $i+1;
			$tid = $var['list'][$i];
			$sql = "SELECT * FROM ".PREFIX."_promo_referral_contests_winners 
			        WHERE id = '".$tid."' LIMIT 1";
			$res = $db->query_db($sql,$print = DEBUG);
			if ($db->num_rows($res) > 0)
			{ 
                $row = $db->fetch_db_array($res);	
			    switch($var['submit'])
	            {
				   case"Set as Paid":
				   $sql = "UPDATE ".PREFIX."_promo_referral_contests_winners SET
                           prize_status = 'Paid' WHERE id = '".$tid."' LIMIT  1";
				   $paid = $db->query_db($sql,$print = DEBUG);
				   if($paid) $success[] = "Winner #".$tid." has been paid";
				   break; 
			     }
			     $db->free_result($res);
	       }	
		  
		}
		
		if(is_array($errors) && !empty($errors)) while (list($key,$value) = each($errors)) $return_msg.= $value.'<br>';	
		elseif(is_array($success) && !empty($success)) while (list($key,$value) = each($success)) $return_msg.= $value.'<br>';	
	
}


$sql = "SELECT * FROM ".PREFIX."_promo_referral_contests WHERE id = '".$var['cid']."' LIMIT 1";
$res = $db->query_db($sql,$print = DEBUG);
if ($db->num_rows($res) == 0) $system->invalidRequest();
$contest = $db->fetch_db_array($res);
$exploded = explode(',', $contest['disallowed_usernames']);
foreach ($exploded as $user) $disallowed.= "'".$user."',";
$contest['disallowed'] = rtrim($disallowed,',');


$sql  = "SELECT COUNT(*) as total  FROM ".PREFIX."_promo_referral_contests_winners ";
$sql .= "WHERE cid = '".$contest['id']."' ";
$sql .= "ORDER BY id ASC ";
 
  $result = $db->query_db($sql,$print = DEBUG);
  $num_rows_1 = $db->db_result($result, 0, 'total');
  $page_1 = $var['page_1'];
  $per_page = (is_numeric($var['per_page']) 
	             && $var['per_page'] <= $settings['system']['rows_per_page'])?
				 $var['per_page']:$settings['system']['rows_per_page'];		 	
  if(!empty($page_1)){$start = ($page_1 - 1) * $per_page;}
  else { $start 	= 0; $page_1	= 1; }		
  $sql = str_replace("COUNT(*) as total", " * ",$sql);
  $sql .= " LIMIT $start, $per_page "; 
  $result = $db->query_db($sql,$print = DEBUG);
  if ($db->num_rows($result) > 0)
  {
	 	$rows = $start;
	 	while ($row = $db->fetch_db_array($result))
		{	
			$rows++;
			if($row["source"] == "")$row["source"] = " -- ";		
		    $winners[$rows] = $row;
				
		}
  }

	
$sql  = "SELECT COUNT(*) as total, username, COUNT(`referral`) as referrals, date(refdate) as date FROM ".PREFIX."_promo_referrals ";
$sql .= "WHERE id != '' ";
$sql .= "AND DATE(refdate) BETWEEN date('".$contest['start_date']."') AND date('".$contest['end_date']."') ";
if($contest['disallowed'] != null)$sql .= "AND username NOT IN (".$contest['disallowed'].")";
$sql .= "GROUP BY username ";
$sql .= "ORDER BY count(`referral`) DESC, id ASC ";
 
 
 
  $result = $db->query_db($sql,$print = DEBUG);
  $num_rows_2 = $db->db_result($result, 0, 'total');
  $page_2 = $var['page_2'];
  $per_page = (is_numeric($var['per_page']) 
	             && $var['per_page'] <= $settings['system']['rows_per_page'])?
				 $var['per_page']:$settings['system']['rows_per_page'];		 	
  if(!empty($page_2)){$start = ($page_2 - 1) * $per_page;}
  else { $start 	= 0; $page_2	= 1; }		
  $sql = str_replace("COUNT(*) as total", " * ",$sql);
  $sql .= " LIMIT $start, $per_page "; 
  $result = $db->query_db($sql,$print = DEBUG);
  if ($db->num_rows($result) > 0)
  {
	 	$rows = $start;
	 	while ($row = $db->fetch_db_array($result))
		{	
			$rows++;
			if($row["source"] == "")$row["source"] = " -- ";		
		    $records[$rows] = $row;
				
		}
  }
 
 
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'referral_contest_details.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();  

?>
