<span  class="form_title">Contest Details</span>
<?php if($error_msg){ ?>
<span class="error_message"><?php echo $error_msg;?></span>
<?php } ?>
<?php if($success_msg){ ?>
<span class="success_message"><?php echo $success_msg;?></span>
<?php } ?>
<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td width="430" height="200" align="center" valign="top"><div class='white'>
        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="table_1">
          <tr>
            <td width="20%">Name</td>
            <td><?php echo $contest['name'];?></td>
          </tr>
          <tr>
            <td>Period</td>
            <td><?php echo date("D, d M Y", strtotime($contest['start_date']));?> through <?php echo date("D, d M Y", strtotime($contest['end_date']));?></td>
          </tr>
          <tr>
            <td>Prizes</td>
            <td><?php echo $contest['prize_description'];?>
              </th>
          </tr>
          <tr>
            <td>Status</td>
            <td><?php echo $contest['status'];?></td>
          </tr>
        </table>
      </div>
      <div class="white">
        <?php if($contest['status'] == 'Completed') { ?>
        <h2>Contest Winners</h2>
        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="table_2">
          <thead>
            <tr>
              <th>Position</th>
              <th>Username</th>
              <th>Total Referrals</th>
            </tr>
          </thead>
          <?php  $i = 0; foreach ($winners as $num => $row){ $i++; ?>
          <tr>
            <td><?php echo $num; ?></td>
            <td><?php echo $row['username']; ?></td>
            <td><?php echo $row['referrals']; ?></td>
          </tr>
          <?php } ?>
          <?php if(count($winners) == 0) { ?>
          <tr>
            <td colspan="3"  align="center">No winner found</td>
          </tr>
          <?php } ?>
          <tr>
            <td colspan="3" align="center"><?php echo $system->getPaginationString($page_1,$num_rows_1,$per_page,3,$system->curPageURL(),"page_1");?></td>
          </tr>
        </table>
        <?php } else { ?>
        <br />
        <h2>Leader Board</h2>
        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="table_2">
          <thead>
            <tr>
              <th>No</th>
              <th>Username</th>
              <th>Total Referrals</th>
            </tr>
          </thead>
          <?php  $i = 0; foreach ($records as $num => $row){ $i++; ?>
          <tr>
            <td><?php echo $num; ?></td>
            <td><?php echo $row['username']; ?></td>
            <td><?php echo $row['referrals']; ?></td>
          </tr>
          <?php } ?>
          <?php if(count($records) == 0) { ?>
          <tr>
            <td colspan="3"  align="center">No records found</td>
          </tr>
          <?php } ?>
          <tr>
            <td colspan="3" align="center"><?php echo $system->getPaginationString($page_2,$num_rows_2,$per_page,3,$system->curPageURL(),"page_2");?></td>
          </tr>
        </table>
        <?php } ?>
      </div></td>
  </tr>
</table>
