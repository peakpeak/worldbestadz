<?php 
  global $system,$db,$settings;	

  $var = $system->getVar();

  $top_promoters = array();
 
  $per_page = 10;

  $sql = "SELECT username, COUNT(`referral`) as total 
  FROM ".PREFIX."_promo_referrals
  GROUP BY `username` 
  ORDER BY `total` DESC  LIMIT  $per_page";
  $res = $db->query_db($sql,$print = DEBUG);
  if ($db->num_rows($res) > 0)
  while($row = $db->fetch_db_array($res))$top_promoters[] = $row;
  $db->free_result($res);
  

   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'leader_board.php';
   
   $loader->setVar($data);
   
   $loader->loadOutput($tpl_file);
   
   $loader->displayOutput();
?>
