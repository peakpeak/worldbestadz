<?php
global $system,$db,$settings;

$var = $system->getVar();

if(!is_numeric($var["cid"])) $system->invalidRequest();
			
   
$sql = "SELECT * FROM ".PREFIX."_promo_referral_contests WHERE id = '".$db->sql_quote($var['cid'])."' LIMIT 1";
$res = $db->query_db($sql,$print = DEBUG);
if ($db->num_rows($res) == 0) $system->invalidRequest();
$contest = $db->fetch_db_array($res);
$exploded = explode(',', $contest['disallowed_usernames']);
foreach ($exploded as $user) $disallowed.= "'".$user."',";
$contest['disallowed'] = rtrim($disallowed,',');

$sql  = "SELECT COUNT(*) as total  FROM ".PREFIX."_promo_referral_contests_winners ";
$sql .= "WHERE cid = '".$contest['id']."' ";
$sql .= "ORDER BY id ASC ";
 
  $result = $db->query_db($sql,$print = DEBUG);
  $num_rows_1 = $db->db_result($result, 0, 'total');
  $page_1 = $var['page_1'];
  $per_page = (is_numeric($var['per_page']) 
	             && $var['per_page'] <= $settings['system']['rows_per_page'])?
				 $var['per_page']:$settings['system']['rows_per_page'];		 	
  if(!empty($page_1)){$start = ($page_1 - 1) * $per_page;}
  else { $start 	= 0; $page_1	= 1; }		
  $sql = str_replace("COUNT(*) as total", " * ",$sql);
  $sql .= " LIMIT $start, $per_page "; 
  $result = $db->query_db($sql,$print = DEBUG);
  if ($db->num_rows($result) > 0)
  {
	 	$rows = $start;
	 	while ($row = $db->fetch_db_array($result))
		{	
			$rows++;
			if($row["source"] == "")$row["source"] = " -- ";		
		    $winners[$rows] = $row;
				
		}
  }

	
$sql  = "SELECT COUNT(*) as total, username, COUNT(`referral`) as referrals, date(refdate) as date FROM ".PREFIX."_promo_referrals ";
$sql .= "WHERE id != '' ";
$sql .= "AND DATE(refdate) >= DATE('".$contest['start_date']."') ";
$sql .= "AND DATE(refdate) <= DATE('".$contest['end_date']."') ";
if($contest['disallowed'] != null)$sql .= "AND username NOT IN (".$contest['disallowed'].")";
$sql .= "GROUP BY username ";
$sql .= "ORDER BY count(`referral`) DESC, id ASC ";
 
 
 
  $result = $db->query_db($sql,$print = DEBUG);
  $num_rows_2 = $db->db_result($result, 0, 'total');
  $page_2 = $var['page_2'];
  $per_page = (is_numeric($var['per_page']) 
	             && $var['per_page'] <= $settings['system']['rows_per_page'])?
				 $var['per_page']:$settings['system']['rows_per_page'];		 	
  if(!empty($page_2)){$start = ($page_2 - 1) * $per_page;}
  else { $start 	= 0; $page_2	= 1; }		
  $sql = str_replace("COUNT(*) as total", " * ",$sql);
  $sql .= " LIMIT $start, $per_page "; 
  $result = $db->query_db($sql,$print = DEBUG);
  if ($db->num_rows($result) > 0)
  {
	 	$rows = $start;
	 	while ($row = $db->fetch_db_array($result))
		{	
			$rows++;
			if($row["source"] == "")$row["source"] = " -- ";		
		    $records[$rows] = $row;
				
		}
  }
 

  
   $pageInfo['title'] = '';
  
   $pageInfo['map'] = array('' => '');
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'contest_details.php';
   
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $loader->setVar($data);
   
   $loader->mainHeader('popup');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('popup');
   
   $loader->displayOutput();
   
 
?>