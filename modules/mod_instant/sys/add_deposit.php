<?php
($SYSEXE) or die("No Access");
//Data available to use : payer,payee,amount,currency,description,reference,sub_id,sub_status,sub_duration,sub_period,sub_paycount,sub_nextdate
//Custom Variable Passed available as custom1,custom2...customN

//Custom from script : username,credit_account,method
$in = $system->importClass("instant");
if($custom["payment_type"] == "deposit"){

   if($custom["payment_amount"] != amount) exit('Amount does not match');
   if($custom["payment_currency"]  != currency) exit('Currency Does not match');
   //if(reference exist in deposits)

$in->addDeposit($custom['username'],$custom['credit_account'],$custom['method']);
$in->addTransaction($custom['username'],"Deposit",$custom["payment_description"],$custom['method'],$custom['credit_account'],$custom["payment_fee"]);
Payment::addDeposit($custom['username'],"Payment","Auto",$custom["payment_description"],$custom['method'],$custom["payment_amount"],$custom["payment_fee"],$custom["payment_currency"],payer,reference,'Approved');
$proInfo = Payment::processorInfo($custom['method']);
$info = array('{date}'=>date("Y-m-d"),'{amount}'=>$custom['credit_account'],'{method}'=>$proInfo['name']);
$system->email($custom['username'],"New Deposit",$info);


}
?>
