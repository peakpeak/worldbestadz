<?php

global $system, $db, $settings;

$in = $system->importClass('instant');

$var = $system->getVar();

$in_settings = $in->getSettings();

$pageInfo['title'] = '';

$pageInfo['map'] = array('' => '');

$tpl_file = dirname(__FILE__) . DS . 'tpl' . DS . 'calculate.php';

$loader = new Loader;

$data = get_defined_vars();

$loader->setVar($data);

$loader->mainHeader('popup');

$loader->loadOutput($tpl_file);

$loader->mainFooter('popup');

$loader->displayOutput();
?>