<?php

   global $system,$userinfo,$db,$settings;
   
   $feed_url = SITEURL.FS.'instant/feed_data';
   
   if(empty($data)) $data = 'statistics'; 
   
   $feed_url.='&data='.$data;
  
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'feed.php';
   
   $loader->setVar($data);
   
   $loader->loadOutput($tpl_file);
   
   $loader->displayOutput();
?>


