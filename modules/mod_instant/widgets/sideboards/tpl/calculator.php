<form class="form_1">
  <table  cellspacing=2 cellpadding=2 border=0 width="100%">
    <tr>
      <td>Amount ($)</td>
      <td><input type="text" id="amount"  onkeyup="calcEarning();" ></td>
    </tr>
    <tr>
      <td>Earn Per Seconds ($)</td>
      <td><input type="text" id="earn_per_sec" readonly="true"></td>
    </tr>
    <tr>
      <td>Earn Per Minute ($)</td>
      <td><input type="text" id="earn_per_min" readonly="true"></td>
    </tr>
    <tr>
      <td>Earn Per Hour ($)</td>
      <td><input type="text" id="earn_per_hour" readonly="true"></td>
    </tr>
    <tr>
      <td>Earn Per Day ($)</td>
      <td><input type="text" id="earn_per_day" readonly="true"></td>
    </tr>
    <tr>
      <td>Max Earning ($)</td>
      <td><input type="text" id="earn_max" readonly="true"></td>
    </tr>
  </table>
</form>
<script>
    function calcEarning()
    {
        //var elm_1 = document.getElementById('plan');
       // var plan = elm_1.options[elm_1.selectedIndex].value;
        var amount = document.getElementById('amount').value;
        var roi = '<?php echo $in_settings['roi_per_sec'];?>'; //
        var max_roi = '<?php echo $in_settings['max_roi'];?>'; //
        
        var earn_per_sec =    (amount * (roi / 100));
        var earn_per_min =    (earn_per_sec * 60);
		var earn_per_hour =   (earn_per_sec * 60 * 60);
		var earn_per_day =    (earn_per_sec * 60 * 60 * 24);
		var earn_max =        (amount * (max_roi / 100));
		
		//alert(earn_per_min);
       
        //document.getElementById('amount').value = amount.toFixed(2);
        document.getElementById('earn_per_sec').value = (earn_per_sec > earn_max) ? earn_max.toFixed(4) : earn_per_sec.toFixed(4);
        document.getElementById('earn_per_min').value = (earn_per_min > earn_max) ? earn_max.toFixed(4) : earn_per_min.toFixed(4);
        document.getElementById('earn_per_hour').value = (earn_per_hour > earn_max) ? earn_max.toFixed(4) : earn_per_hour.toFixed(4);
        document.getElementById('earn_per_day').value = (earn_per_day > earn_max) ? earn_max.toFixed(4) : earn_per_day.toFixed(4);
        document.getElementById('earn_max').value = earn_max.toFixed(4);

    }



    calcEarning();
</script>