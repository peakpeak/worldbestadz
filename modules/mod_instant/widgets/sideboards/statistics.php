<?php

   global $system,$userinfo,$db,$settings;
   
 
   $sql = "SELECT COUNT(id) from ".PREFIX."_account_users ";
   $res = $db->query_db($sql,$print = DEBUG);
   $total_users = $db->db_result($res,0,'count(id)');
   $db->free_result($res);
   
   $sql = "SELECT COUNT(id) from ".PREFIX."_account_users ";
   $sql.= "WHERE date(regdate) = CURDATE()  ";
   $res = $db->query_db($sql,$print = DEBUG);
   $today_users = $db->db_result($res,0,'count(id)');
   $db->free_result($res);
  
   
   $sql = "SELECT COUNT(*) FROM ".PREFIX."_instant_deposits ";
   $sql.= "GROUP BY username  ";
   $res = $db->query_db($sql,$print = DEBUG);
   $total_investors = intval($db->num_rows($res));
   $db->free_result($res);
   
   
   $sql = "SELECT SUM(deposit_amount) as amount FROM ".PREFIX."_instant_deposits ";
   $res = $db->query_db($sql,$print = DEBUG);
   $total_investment = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);
   
   $sql = "SELECT count(id) as amount FROM ".PREFIX."_instant_deposits ";
   $res = $db->query_db($sql,$print = DEBUG);
   $n_investment = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);

   
   
   $sql = "SELECT SUM(amount) as amount FROM ".PREFIX."_wallet_withdrawals ";
   $sql.= "WHERE status = 'Paid'  ";
   $res = $db->query_db($sql,$print = DEBUG);
   $total_withdrawals = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);
   
  
  
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'statistics.php';
   
   $loader->setVar($data);
   
   $loader->loadOutput($tpl_file);
   
   $loader->displayOutput();
?>


