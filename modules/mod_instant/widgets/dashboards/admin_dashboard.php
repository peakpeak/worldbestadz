<?php

   global $system,$userinfo,$db,$settings;

   //top purchasers
   $top_purchasers = array();
   
   $sql = "SELECT username, SUM(deposit_amount) as total 
   FROM ".PREFIX."_instant_deposits
   GROUP BY `username` 
   ORDER BY `total` DESC  LIMIT 10";
   $res = $db->query_db($sql,$print = DEBUG);
   if ($db->num_rows($res) > 0)
   while($row = $db->fetch_db_array($res))$top_purchasers[] = $row;
   $db->free_result($res);
	
	
   //top  earners
   $top_earners = array();
   
   $sql = "SELECT username, SUM(earned_amount) as  total
   FROM ".PREFIX."_instant_deposits
   GROUP BY `username` 
   ORDER BY `earned_amount` DESC  LIMIT 10";
   $res = $db->query_db($sql,$print = DEBUG);
   if ($db->num_rows($res) > 0)
   while($row = $db->fetch_db_array($res))$top_earners[] = $row;
   $db->free_result($res);
  
   
   //shares purchase
   
   $sql = "SELECT COUNT(id) FROM ".PREFIX."_instant_deposits ";
   $sql.= "GROUP BY username  ";
   $res = $db->query_db($sql,$print = DEBUG);
   $n_purchasers = intval($db->num_rows($res));
   $db->free_result($res);
   
   $sql = "SELECT SUM(deposit_amount) as amount FROM ".PREFIX."_instant_deposits ";
   $sql.= "WHERE status = 'Active'  ";
   $res = $db->query_db($sql,$print = DEBUG);
   $total_active_purchase = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);

   $sql = "SELECT SUM(deposit_amount) as amount FROM ".PREFIX."_instant_deposits ";
   $sql.= "WHERE status = 'Expired'  ";
   $res = $db->query_db($sql,$print = DEBUG);
   $total_expired_purchase = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);
   
   $total_purchase = $total_active_purchase + $total_expired_purchase;
 
   $sql = "SELECT SUM(earned) as amount FROM ".PREFIX."_instant_shares ";
   $res = $db->query_db($sql,$print = DEBUG);
   $total_earned = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);
   
   //flagged
   $sql = "SELECT SUM(amount) as amount FROM ".PREFIX."_instant_transactions ";
   $sql.= "WHERE type = 'Commission'  ";
   $res = $db->query_db($sql,$print = DEBUG);
   $total_commissions = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);
   
  /* $sql = "SELECT SUM(balance) as amount FROM ".PREFIX."_wallet_balance ";
   $sql.= "WHERE currency = 'USD'  ";
   $res = $db->query_db($sql,$print = DEBUG);
   $total_wallet_balance = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);*/
   
   $range = 7;
   $today = date("Y-m-d");
   $days = array();
   $shares_purchase = array();
   $wallet_deposit = array();
   $pending_wallet_withdrawals = array();
   $paid_wallet_withdrawals = array();
   $timestamp = mktime(0,0,0,date("m",strtotime($today)),date("d",strtotime($today))-($range-1),date("Y",strtotime($today)));			 
   $past_range_days = date("Y-m-d",$timestamp);
   for($i = 0; $i < $range; $i++)
   { 
          $timestamp = mktime(0,0,0,date("m",strtotime($past_range_days)),date("d",strtotime($past_range_days))+$i,date("Y",strtotime($past_range_days)));			
	      $days[$i] = date("Y-m-d",$timestamp);
		  
		 
		  //flagged
		  $sql = "";
		  $sql = "SELECT SUM(amount) as amount FROM ".PREFIX."_instant_transactions ";
		  $sql.= "WHERE type = 'Deposit'  ";
		  $sql.= "AND DATE(date) = DATE('".$days[$i]."') LIMIT 1";
		  $res = $db->query_db($sql,$print = DEBUG);
		  $shares_purchase[$days[$i]] = floatval($db->db_result($res,0,'amount'));
		  $db->free_result($res);
		  
		  $sql = "";
		  $sql = "SELECT SUM(amount) as amount FROM ".PREFIX."_wallet_transactions ";
		  $sql.= "WHERE type = 'Deposit'  ";
		  $sql.= "AND DATE(date) = DATE('".$days[$i]."') LIMIT 1 ";
		  $res = $db->query_db($sql,$print = DEBUG);
		  $wallet_deposits[$days[$i]] = floatval($db->db_result($res,0,'amount'));
		  $db->free_result($res);
		  
		  $sql = "";
		  $sql = "SELECT SUM(amount) as amount FROM ".PREFIX."_wallet_withdrawals ";
          $sql.= "WHERE status = 'Pending'  ";
		  $sql.= "AND DATE(adddate) = DATE('".$days[$i]."') LIMIT 1";
          $res = $db->query_db($sql,$print = DEBUG);
          $pending_wallet_withdrawals[$days[$i]] = floatval($db->db_result($res,0,'amount'));
          $db->free_result($res);
		  
		  $sql = "";
		  $sql = "SELECT SUM(amount) as amount FROM ".PREFIX."_wallet_withdrawals ";
          $sql.= "WHERE status = 'Paid'  ";
		  $sql.= "AND DATE(adddate) = DATE('".$days[$i]."') LIMIT 1 ";
          $res = $db->query_db($sql,$print = DEBUG);
          $paid_wallet_withdrawals[$days[$i]] = floatval($db->db_result($res,0,'amount'));
          $db->free_result($res);

	  
   } 

   $days = array_reverse ( $days );

   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'admin_dashboard.php';
   
   $loader->setVar($data);
   
   $loader->loadOutput($tpl_file);
   
  $loader->displayOutput();
?>