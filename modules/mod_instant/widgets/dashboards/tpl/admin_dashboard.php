<table class="tableS" cellpadding="0" cellspacing="0">
   <tr>
     <td colspan="2">
	<table class="tableS" cellpadding="0" cellspacing="0"> 
	  <thead>
		 <tr>
		   <th colspan="2"  align="left">Statistics</th>
		 </tr>
	   </thead>
		  <tr>
		   <td width="30%">Total Purchasers</td>
		   <td><?php echo $n_purchasers;?></td>
		  </tr>
		  <tr>
		   <td>Active Purchase</td>
		   <td>$<?php echo $total_active_purchase;?></td>
		  </tr>
		  <tr>
		   <td>Expired Purchase</td>
		   <td>$<?php echo $total_expired_purchase;?></td>
		  </tr>
		  <tr>
		   <td><b>Total</b></td>
		   <td><b>$<?php echo $total_purchase;?></b></td>
		  </tr>
		  <tr>
		   <td>Total Commissions</td>
		   <td>$<?php echo $total_commissions;?></td>
		  </tr>
	   </table>
	   </td>
     </tr>
    <tr>
    <td colspan="2">
	<table class="tableS" cellpadding="0" cellspacing="0">
 <thead>
  <tr>
    <th align="left">&nbsp;</th>
	<th align="left">Wallet Deposits</th>
    <th align="left">Purchases</th>
	<th align="left">Pending Withdrawals</th>
	<th align="left">Paid Withdrawals</th>
  </tr>
  </thead>
  
  <?php foreach($days as $id => $day) { ?>
  <tr>
    <td align="left"><?php echo $day;?></td>
    <td align="left">$<?php echo $wallet_deposits[$day];?></td>
	<td align="left">$<?php echo $shares_purchase[ $day];?></td>
	<td align="left">$<?php echo $pending_wallet_withdrawals[ $day];?></td>
	<td align="left">$<?php echo $paid_wallet_withdrawals[ $day];?></td>
  </tr>
  <?php } ?>
  </table>
   </td>
  </tr>
  
  <tr>
    <td width="50%">
	<table class="tableS" cellpadding="0" cellspacing="0">
    <thead>
  <tr>
    <th  colspan="2" align="left">Top 10 purchasers</th>
  </tr>
  </thead>
  <?php foreach($top_purchasers as $purchasers) { ?>
  <tr>
    <td align="left"><?php echo $purchasers["username"] ?></td>
    <td align="left">$<?php echo $purchasers["total"] ?></td>
  </tr>
  <?php } ?>
  </table>
   </td>
   
   
   <td>
	<table class="tableS" cellpadding="0" cellspacing="0">
    <thead>
  <tr>
    <th  colspan="2" align="left">Top 10 earners (Excludes micro accruals accumulated)</th>
  </tr>
  </thead>
  <?php foreach($top_earners as $earners) { ?>
  <tr>
    <td align="left"><?php echo $earners["username"] ?></td>
    <td align="left">$<?php echo $earners["total"] ?></td>
  </tr>
  <?php } ?>
  </table>
   </td>
  </tr>
</table>