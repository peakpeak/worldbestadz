<?php
   global $system,$userinfo,$db,$settings;
   
   $sql = "SELECT SUM(deposit_amount) as amount FROM ".PREFIX."_instant_deposits ";
   $sql.= "WHERE username = '".$userinfo['username']."'  ";
   $sql.= "AND status = 'Active'  ";
   $res = $db->query_db($sql,$print = DEBUG);
   $active_deposit = number_format($db->db_result($res,0,'amount'),2);
   $db->free_result($res);
   
   $sql = "SELECT SUM(deposit_amount) as amount FROM ".PREFIX."_instant_deposits ";
   $sql.= "WHERE username = '".$userinfo['username']."'  ";
   $sql.= "AND status = 'Expired'  ";
   $res = $db->query_db($sql,$print = DEBUG);
   $expired_deposit = number_format($db->db_result($res,0,'amount'),2);
   $db->free_result($res);
   
   $total_deposit = $active_deposit + $expired_deposit;
  
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'user_dashboard.php';
   
   $loader->setVar($data);
   
   $loader->loadOutput($tpl_file);
   
   $loader->displayOutput();
?>