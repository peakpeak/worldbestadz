<?php
  global $system,$db,$settings;
 
  $var = $system->getVar();
  
  $per_page = $settings['system']['rows_per_page'];
  $sql  = "SELECT *,date(purchase_date) as date FROM ".PREFIX."_instant_deposits ";
  $sql .= "ORDER BY `purchase_date` DESC";
  $result = $db->query_db($sql,$print = DEBUG);


			$page		 = $system->getVar('page');	
			$num_rows 	 = $db->num_rows($result);
			if(!empty($page)) 
			{
				$start = ($page - 1) * $settings['system']['rows_per_page']; 
			}			
			else
			{
				$start 	= 0;
				$page	= 1;
			}				

		
  $sql .= " LIMIT $start, $per_page "; 
  
  $result = $db->query_db($sql,$print = DEBUG);
  if ($db->num_rows($result) > 0)
  {
	 	$rows = $start;
	 	while ($row = $db->fetch_db_array($result))
		{	
			$rows++;
			$sql  = "SELECT name FROM ".PREFIX."_payment_processors WHERE id = '".$row['deposit_method']."'  LIMIT 1";
            $result2 = $db->query_db($sql,$print = DEBUG);
            $prow = $db->fetch_db_array($result2);
			$row["processor"] = $prow["name"];
		    $deposits[$rows] = $row;			
		}
  }
   $pageInfo['title'] = 'Deposits';
  
   $pageInfo['map'] = array('Deposits' => '',);
   
   $loader = new Loader;
   
   $data= get_defined_vars();
   
   $loader->setVar($data);
   
   $loader->mainHeader();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'deposits.php';
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter();
   
   $loader->displayOutput();
   
 ?>

 