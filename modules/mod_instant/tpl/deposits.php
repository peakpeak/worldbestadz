<span class="form_title">Recent Deposits</span>
<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td width="430" height="200" align="center" valign="top"><div class="white">
        <table  cellspacing=1 cellpadding=2 border=0 width="100%" class="table_2">
          <thead>
            <tr>
              <th>#</th>
              <th>Date</th>
              <th>Username</th>
              <th>Method</th>
              <th>Amount</th>
            </tr>
          </thead>
          <?php  $i = 0; foreach ($deposits as $num => $data) { $i++;?>
          <tr>
            <td height="25" valign="middle" align="center"><?php echo $num; ?></td>
            <td height="25" valign="middle"><?php echo $data['date']; ?></td>
            <td height="25" valign="middle"><?php echo $data['username']; ?></td>
            <td height="25" valign="middle"><?php echo $data['processor']; ?></td>
            <td height="25" valign="middle">$<?php echo $data['deposit_amount']; ?></td>
          </tr>
          <?php } ?>
          <?php if(count($deposits) == 0) : ?>
          <tr>
            <td colspan="5" height="25" valign="middle"  align="center" > - no records found - </td>
          </tr>
          <?php endif; ?>
          <?php if($num_rows > $per_page) : ?>
          <tr>
            <td colspan="5" height="25" valign="middle" align="center" ><?php echo $system->getPaginationString($page,$num_rows,$per_page,3,$system->curPageURL(),"page");?></td>
          </tr>
          <?php endif; ?>
        </table>
      </div></td>
  </tr>
</table>
<script type="text/javascript">
 
   
	 var first = 0;
	 var last = <?php echo $i;?>;	
	  fancyjQuery(document).ready(function() {
			
			for (i = first; i  <= last; i++) {
			
			fancyjQuery("#popup"+i).fancybox({
				'width'				: '60%',
				'height'			: '100%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
          }
		
		});
  </script>
