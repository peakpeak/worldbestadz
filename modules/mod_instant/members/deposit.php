<?php
global $system,$db,$userinfo,$settings;

$in = $system->importClass('instant');

$wallet = $system->importClass('wallet');

$payment = $system->importClass('payment');

$processors = $payment->processors('deposit',"USD");

$in_settings = $in->getSettings();

$var = $system->getVar();

$tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'deposit.php';


   if($var['submit'])
   {
   

      Validate::trim_post();
	  if($var['processor'] == '') $errors[] =  "Please select a valid processor";
	  elseif(!is_numeric($var["units"]) || $var["units"] < 0)  $errors[] =  "Please add a valid quantity";
	  else if($var['units'] < $in_settings['min_unit']) $errors[] =  "Minimum unit allowed is ".$in_settings['min_unit'];
	  elseif($var['units'] > $in_settings['max_unit']) $errors[] =  "Maximum units allowed is ".$in_settings['max_unit'];
	  $amount =  $var['units'] * $in_settings['unit_cost'];
	  $balance = $wallet->userPurchaseBal($userinfo['username'],$var['processor'],"USD");
	  if($balance < $amount)$errors[] = "Insufficient account balance for this purchase";
	  if(is_array($errors) && !empty($errors))
	  {

		while (list($key,$value) = each($errors)) $error_msg.= $value.'<br>';	
		
	  }
	  else 
	  {
	           $description = 'Adshare purchase by '.$userinfo['username'];
			   $wallet->debitUserPurchaseBal($userinfo['username'],$var['processor'],$amount,0.00,"USD",$description);
			   $in->addDeposit($userinfo['username'],$amount,$var['processor']);
               $in->addTransaction($userinfo['username'],"Deposit",$description,$var['processor'],$amount,0.00);
			   $success_msg = "Your Adshare purchase was successful";
		      
	    }  
   
   }


   $pageInfo['title'] = '';
  
   $pageInfo['map'] = array('' => '');
   
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $loader->setVar($data);
   
   $loader->mainHeader('private');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('private');
   
   $loader->displayOutput();