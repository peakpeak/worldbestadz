<span class="form_title"Confirm & Pay</span>
<?php if($error_msg){ ?>
<span class="error_message"><?php echo $error_msg;?></span>
<? } ?>
<?php if($success_msg){ ?>
<span class="success_message"><?php echo $success_msg;?></span>
<? } ?>
<center>
  <table width="60%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
    <tr>
      <td width="430" height="200" align="center" valign="top"><div class="white">
          <table cellspacing=0 cellpadding=0 border=0 width="100%" class="table_1">
            <thead>
              <tr>
                <th colspan="2">Confirm</th>
              </tr>
            </thead>
            <tr>
              <td>Item:</td>
              <td><?php echo $item ?></td>
            </tr>
            <tr>
              <td>Amount:</td>
              <td>$<?php echo sprintf("%01.2f",$amount); ?></td>
            </tr>
            <tr>
              <td>Fee:</td>
              <td>$<?php echo sprintf("%01.2f",$fee); ?></td>
            </tr>
            <tr>
              <td>Total:</td>
              <td>$<?php echo sprintf("%01.2f",$total); ?></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td><?php echo $button ?></td>
            </tr>
          </table>
        </div></td>
    </tr>
  </table>
</center>
