<span class="form_title">New Deposit</span>
<?php if($error_msg){ ?>
<span class="error_message"><?php echo $error_msg;?></span>
<? } ?>
<?php if($success_msg){ ?>
<span class="success_message"><?php echo $success_msg;?></span>
<? } ?>
<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td width="430" height="200" align="center" valign="top"><div class="white">
        <form action="" method="post" class="form_1">
          <table cellspacing=0 cellpadding=0 border=0 width="100%"  class="table_1">
            <thead>
              <tr>
                <th colspan="2">Buy Adshare (Each unit cost $<?php echo $in_settings['unit_cost']?>)</th>
              </tr>
            </thead>
            <tr>
              <td>Enter Units</td>
              <td><input type="text" name="units" value="<? echo $var['units'] ?>"></td>
            </tr>
            <tr>
              <td>Payment option</td>
              <td><select name="processor">
                  <option value="" >- Select -</option>
                  <?php foreach($processors as $id => $value) { ?>
                  <option value="<?php echo $id?>"  <?php if($var["processor"] == $id){?> selected="selected" <?php } ?>><?php echo $value?> - $<?php echo $wallet->userPurchaseBal($userinfo['username'],$id,"USD");?></option>
                  <?php } ?>
                </select></td>
            </tr>
            <tr>
              <td colspan="2" align="center"><input type="submit" name="submit" value="Make Deposit"></td>
            </tr>
          </table>
        </form>
      </div></td>
  </tr>
</table>
