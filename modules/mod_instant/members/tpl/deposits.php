<span  class="form_title">Deposits</span>
<?php if($error_msg){ ?>
<span class="error_message"><?php echo $error_msg;?></span>
<? } ?>
<?php if($success_msg){ ?>
<span class="success_message"><?php echo $success_msg;?></span>
<? } ?>
<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td width="430" height="200" align="center" valign="top">
	
	<div align="right"><strong>[ <a href="#"  <?php if($to_surf > 0){ ?> onclick="surf('<?php echo $to_surf ?>');" <?php } else { ?> onclick="withdrawAll();" <?php } ?>>Withraw All</a></strong> ]</div><br />
	
	<div class="white">
        <form class="form_1">
		 <table cellspacing=1 cellpadding=2 border=0 width="100%" class="table_2">
            <thead>
              <tr>
                <th>#</th>
                <th>Method</th>
                <th>Desposit</th>
                <th>Accrued</th>
                <th>Withdrawn</th>
                <th>Balance</th>
                <th>Status</th>
				<th>Action</th>
              </tr>
            </thead>
          <?php  foreach ($deposits as $pnum => $data) { ?>
         
            <tr>
              <td>#<?php echo $data['id']?></td>
              <td><?php echo $data['processor']?></td>
              <td>$<?php echo $data['deposit_amount']?></td>
              <td>$<?php echo round($data['earned'],4)?></td>
              <td>$<?php echo round($data['paid_amount'],4)?></td>
              <td>$<?php echo round($data['balance'],4,PHP_ROUND_HALF_DOWN)?></td>
              <td><?php echo $data['status']?></td>
			  <td><input type="hidden" id="amount_<?php echo $data['id']?>" value="<?php echo round($data['balance'],4,PHP_ROUND_HALF_DOWN)?>"/><strong><?php echo $data['link']?></strong></td>
            </tr>
          <?php } ?>
          <?php if(count($deposits) == 0) {?>
          <tr><td colspan="8" align="center" >No Deposits Found</td></tr>
          <?php } ?>
		  <?php if($num_rows > $per_page) : ?>
          <tr>
              <td colspan="8" align="center" ><?php echo $system->getPaginationString($page,$num_rows,$per_page,3,$system->curPageURL(),"page");?></td>
          </tr>
          <?php endif; ?>
		  </table>
        </form>
      </div></td>
  </tr>
</table>
<script type="text/javascript">

function minimum(amount)
{ 
   alert('Balance must be at least $'+amount+' to withdraw to wallet');
}

function withdraw(id)
{ 
   var amount = document.getElementById('amount_'+id).value;
   if(amount == '') {alert('Please enter a valid amount');return false;}
   if(confirm('Do you really want to withdraw?'))
   post_to_url('{#loader=system::url}/members/instant/deposits',{'withdraw':'y','did':id,'amount':amount});
}

function withdrawAll()
{ 
   if(confirm('Do you really want to withdraw?'))
   post_to_url('{#loader=system::url}/members/instant/deposits',{'withdrawall':'y'});
}

function surf(sites)
{ 
   alert('You need to surf '+sites+' site(s) to qualify for daily withdrawal');
}

function post_to_url(path, params, method) {
    method = method || "post";
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);
            form.appendChild(hiddenField);
         }
    }

    document.body.appendChild(form);
    form.submit();
}


</script>
