<span class="form_title">Referrals</span>
<?php if($error_msg){ ?>
<span class="error_message"><?php echo $error_msg;?></span>
<? } ?>
<?php if($success_msg){ ?>
<span class="success_message"><?php echo $success_msg;?></span>
<? } ?>
<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td width="430" height="200" align="center" valign="top"><!--<div class="white">
    <table  cellspacing=1 cellpadding=2 border=0 width="100%" class="table_2">
	<thead>
     <tr>
      <th colspan="2">Referral Commissions</th>
    </tr>
	</thead>
    <?php foreach($commissions[$userinfo['mid']] as $k => $v) { ?>
    <tr>
      <td width="30%" >Level <?php echo $k+1; ?></td>
      <td ><?php echo $v ?>%</td>
    </tr>
   <?php } ?>
	
   <?php if(count($commissions[$userinfo['mid']]) == 0){ ?>
   <tr>
      <td colspan="2" align="center"> - no commissions found -</td>
   </tr>
  <?php } ?>
  </table></div>-->
      <?php if($depth > 0) { for ($level = 0; $level < $depth; $level++) { $data = $referrals[$level]; $sn = $start[$level] ; ?>
      <div class="white">
        <h3> Level <?php echo $level+1 ?> Referrals</h3>
        <table cellspacing=1 cellpadding=2 border=0 width="100%" class="table_2">
          <thead>
            <tr>
              <th>#</th>
              <th>Join Date</th>
              <th>Username</th>
              <th>Upline</th>
            </tr>
          </thead>
          <?php  foreach ($data as $num => $subdata) { $sn++; ?>
          <tr>
            <td><a href="#"><strong>#<?php echo $sn; ?></strong></a></td>
            <td><?php echo $subdata['refdate']; ?></td>
            <td><?php echo $subdata['referral']; ?></td>
            <td><?php echo $subdata['username']; ?></td>
          </tr>
          <?php } ?>
          <?php if(count($data) == 0) { ?>
          <tr >
            <td colspan="4" align="center"> - no records found - </td>
          </tr>
          <?php } ?>
          <?php if($referrals[$level] > $per_page) : ?>
          <tr >
            <td colspan="4"><?php echo $system->getPaginationString($page[$level],$count[$level],$per_page,3,$system->curPageURL(),"page$level");?></td>
          </tr>
          <?php endif; ?>
        </table>
      </div>
      <?php } } ?>
    </td>
  </tr>
</table>
