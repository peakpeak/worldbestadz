			<div class="contentTopACLeft">
				<div class="contentTopACCrued">
					<p>
						<div style="float:left;line-height: 24px;margin-top: 6px;">Balance Accrued: <br/>
					 <a class="button-withdraw" href="{#loader=system::url}/members/instant/deposits">Withdraw</a>
					 </div>
					
					<span id="accrual"></span>
					
					</p>
				</div>
				<div class="contentTopACSpent">
					<p>spent: <span class="ctnTopACspan1">$<?php echo number_format( $deposits['deposit_amount'], 2) ?></span></p>
					<p>todays roi:  <span class="ctnTopACspan2"><?php echo number_format( $in_settings['roi_per_day'], 2) ?>% x <?php echo $multiplier?> = <?php echo ($in_settings['roi_per_day'] * $multiplier) ?>%</span></p>
				</div>
				<div class="ctnchartACC">
					
					
					
  
<script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1','packages':['corechart']}]}"></script>
<script type='text/javascript'>

      google.setOnLoadCallback(drawChart);
      function drawChart() {
		 var data = google.visualization.arrayToDataTable([
		 
          ['Date', 'ROI'],
	      <?php
		  $i = 1; $element = count($roi_data);
		  foreach ($roi_data as $n => $data) {  if ($i < $element){  ?> ['<?php  echo $data['date']?>', <?php  echo ($data['roi'] * $multiplier)?>],  <?php }else{ ?> ['<?php  echo $data['date']?>', <?php  echo $data['roi']?>]  <?php } $i++; } 
		  ?>  
		  
        ]);

		var options = {
          chart: {title: 'Title here', subtitle: 'Subtitle'},
          width:800,
          height: 300,
        };
		

        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
        chart.draw(data, options);


      }
	  
 </script>

<div id='chart_div'></div>


<script language="javascript"> 

$.fn.addAccrual = function(){
      
	  var earned = <?php echo $deposits['earned_amount'];?>;
	  var paid = <?php echo $deposits['paid_amount'];?>;
	  var incm = <?php echo $deposits['earning'];?>;
	  var max_amount = <?php echo $deposits['max_earning'];?>;
	  
	  
	  var d_earned = earned - paid;
	  d_earned = d_earned.toFixed(4)
      var container = $(this[0]).html(d_earned);
	  var addAccrual = setInterval(function () {
	  
	  
      if (earned < max_amount) {
	        if(earned + incm > max_amount) incm = max_amount - earned;
	        earned = earned + incm;
			d_earned = earned - paid;
			d_earned = d_earned.toFixed(4)
            container.html(d_earned);
       } else {
            clearInterval(addAccrual);
			
       }
    }, 1000);

}



	 var first = 0;
	 var last = <?php echo $i;?>;	
	  fancyjQuery(document).ready(function() {
			
			for (i = first; i  <= last; i++) {
			
			fancyjQuery("#popup"+i).fancybox({

				'width'				: '100%',
				'height'			: '100%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
          }
		
		});
--> 
</script>
<script>$("#accrual").addAccrual();</script>

				</div>
			</div>
			<div class="contentTopACRight">
				<div class="contentTopACRightPart">
					
					{#widget=ads::textad?pid:2&display:vertical&limit:1}

				</div>
				<div class="contentTopACRightPart">
					{#widget=ads::textad?pid:2&display:vertical&limit:1}
				</div>
				<div class="contentTopACRightPart contentTopACRightPart3">
					{#widget=ads::textad?pid:2&display:vertical&limit:1}
				</div>
			</div>

<div style="clear:both"></div>


	<div class="advertiseContainer">
		<div class="advertiseInner">
			{#widget=ads::banner?pid:2&display:vertical&limit:1}
			{#widget=ads::banner?pid:2&display:vertical&limit:1}
		</div>
	</div> <!-- end advertiseContainer -->
	
	
	<div class="contentBotACContainer">
		
		<div class="contentBotACInner">
			<div class="contentBotACLeft">
				<div class="contentBotACTop">
					<a class="ctnACdeposit" href="{#loader=system::url}/members/instant/deposit">deposit</a>
					<a class="ctnACwithdraw" href="{#loader=system::url}/members/instant/deposits">withdraw</a>
					<a class="ctnACsettings" href="{#loader=system::url}/members/wallet/fund">fund wallet</a>
					<a class="ctnACreferrals" href="{#loader=system::url}/members/instant/deposits">my deposits</a>
				</div>
				<div class="contentBotACBot">
					
					{#widget=instant::user_dashboard}
					
					<div class="ctnBotACBPart ctnBotACBPart4 ">
					
					<h3>$<?php echo number_format( $pending_withdrawals ,4) ?></h3>
						<p>Pending payment</p>
					
					</div>
					
					
					{#widget=ads::user_dashboard}
					
					
				</div>
			</div>
			<div class="contentBotACRight">
				<div class="ctnBotACRightPart">
					
					{#widget=content::announcements?type:private}
					

				</div>
			</div>
			<div class="recentdepositsACC">
				
				<h3>Deposits</h3>
				  <table  cellspacing=1 cellpadding=2 border=0 width="100%" class="table_2">
					<thead>
					  <tr>
						<th>#</th>
						<th>Method</th>
						<th>Desposit</th>
						<th>Accrued</th>
						<th>Withdrawn</th>
						<th>Balance</th>
						<th>Status</th>
					  </tr>
					</thead>
					<?php  $i = 0; foreach ($udeposits as $num => $data) { $i++;?>
					<tr>
					  <td>#<?php echo $data['id']?></td>
					  <td><?php echo $data['processor']?></td>
					  <td>$<?php echo $data['deposit_amount']?></td>
					  <td>$<?php echo round($data['earned'],4)?></td>
					  <td>$<?php echo round($data['paid_amount'],4)?></td>
					  <td>$<?php echo round($data['balance'],4,PHP_ROUND_HALF_DOWN)?></td>
					  <td><?php echo $data['status']?></td>
					</tr>
					<?php } ?>
					<?php if(count($udeposits) == 0) : ?>
					<tr>
					  <td colspan="7" height="25" valign="middle"  align="center" > - no records found - </td>
					</tr>
					<?php endif; ?>
				  </table>
				  <div align="right">[ <strong><a href="{#loader=system::url}/members/instant/deposits">View All</a></strong> ]</div>
								
			</div>
		</div>
	</div><!-- end contentBotACContainer -->	



 
