<span class="form_title">Apply to become a representative</span>
<?php if ($error_msg) { ?>
<span class="error_message"><?php echo $error_msg; ?></span>
<?php } ?>
<?php if ($success_msg) { ?>
<span class="success_message"><?php echo $success_msg; ?></span>
<?php } ?>
<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td width="430" height="200" align="center" valign="top"><div class="white">
        <?php if(!empty($arow)){?>
        <p><b>Your application status is <?php echo $arow['status']?></b></p>
        <?php } ?>
        <table cellspacing=0 cellpadding=0 border=0 width="100%" class="table_1">
          <thead>
            <tr>
              <th colspan="2">Requirements</th>
            </tr>
          <thead>
            <tr>
              <td colspan="2">To apply as a representative in our program you have to meet the following criterias <br />
                <ol>
                  <li>You must provide a phone number and email address that will be made public to members</li>
                  <li>You must understand and be able to describe how our program works</li>
                </ol></td>
            </tr>
        </table>
      </div>
      <form method="post" action="" class="form_1">
        <div class="white">
          <p>Please confirm your personal info to continue</p>
          <table cellspacing=0 cellpadding=0 border=0 width="100%" class="table_1">
            <thead>
              <tr>
                <th colspan="2">Personal Info</th>
              </tr>
            <thead>
              <tr>
                <td width="200" height="25">First Name</td>
                <td><input type="text" name="fname" value="<?php echo $userinfo['firstname'] ?>" size="30"></td>
              </tr>
              <tr>
                <td>Last Name</td>
                <td ><input type="text" name="lname" value="<?php echo $userinfo['lastname'] ?>" size="30"></td>
              </tr>
              <tr>
                <td>Email</td>
                <td ><input type="text" name="email" value="<?php echo $userinfo['email'] ?>" size="30"></td>
              </tr>
              <tr>
                <td>Phone #</td>
                <td><input type="text" name="telephone" value="<?php echo $userinfo["telephone"] ?>" size="30"></td>
              </tr>
              <tr>
                <td>Address 1</td>
                <td><input type="text" name="address1" value="<?php echo $userinfo["address1"] ?>" size="30"></td>
              </tr>
              <tr>
                <td>Address 2</td>
                <td><input type="text" name="address2" value="<?php echo $userinfo["address2"] ?>" size="30"></td>
              </tr>
              <tr>
                <td>City</td>
                <td><input type="text" name="city" value="<?php echo $userinfo["city"] ?>" size="30"></td>
              </tr>
              <tr>
                <td>State</td>
                <td><input type="text" name="state" value="<?php echo $userinfo["state"] ?>" size="30"></td>
              </tr>
              <tr>
                <td>Country</td>
                <td ><?php echo $db->make_select("country", $countries, $userinfo["country_code"], "inpts") ?></td>
              </tr>
              <?php if ($settings['account']['use_security_question'] == true && $userinfo['question'] != "") { ?>
              <tr>
                <td><?php echo $userinfo['question'] ?></td>
                <td><input type="text" name="answer" value="" size="30"></td>
              </tr>
              <?php } ?>
              <tr>
                <td colspan="2" align="center" valign="bottom" height="35"><input type="submit" name="submit" value="Apply Now"></td>
              </tr>
          </table>
        </div>
      </form></td>
  </tr>
</table>
