<?php
global $system,$db,$userinfo,$settings;

$payment = $system->importClass('payment');

$wallet = $system->importClass('wallet');

$in = $system->importClass('instant');

$in_settings = $in->getSettings();

$processors = $payment->processors(null,"USD");

$proFees = $payment->processorsFee();

$var = $system->getVar();

$tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'deposits.php';

   if($settings['instant']['surf_to_withdraw']){
   
	     $ads = $system->importClass('ads');
		 $sf = $settings['instant']['surf_frequency'];
		 $sts = $settings['instant']['sites_to_surf'];
		 $surfed = $ads->surfedAds($userinfo['username'],$sf);
		 $to_surf = ($surfed < $sts) ? ($sts - $surfed) : 0;	
		 	 
   }else $to_surf = 0;

   if($var['withdraw'] == 'y' && $var['did'])
   {
      
	  
      Validate::trim_post();
	  if($var['did'] == '') $errors[] =  "Please add a valid deposit";
	  if($var['amount'] == '') $errors[] =  "Please add amount";
	  if($to_surf > 0) $errors[] =  "You need to surf ".$to_surf." site(s) to qualify for daily withdrawal";
	  
	  $sql = "SELECT * FROM ".PREFIX."_instant_deposits 
	          WHERE username = '".$userinfo["username"]."'
			  AND id = '".$var["did"]."'   
			  LIMIT 1";
      $res = $db->query_db($sql,$print = DEBUG);
	  $drow = $db->fetch_db_array($res);
	  
	  $earned = $in->earned($var['did']);
	  $balance = $earned - $drow["paid_amount"];
	  $amount = $var['amount'];
	  
	  if($balance < $in_settings['min_withdrawal']) $errors[] = "Amount must be up to \$".$in_settings['min_withdrawal']; 
	  elseif($amount > $balance)$errors[] = "Insufficient balance requested";
	  
	  if(is_array($errors) && !empty($errors))while (list($key,$value) = each($errors)) $error_msg.= $value.'<br>';	
	  else 
	  {
	         $result = $in->withdrawEarnings($var['did']);
			 if($result)$success_msg = "Your accruals have been transfered to your wallet"; 
			 else $error_msg = "Insufficient balance"; 
	  }  
   
 
   }
   
   if($var['withdrawall'] == 'y')
   {
	  $result = $in->withdrawAllEarnings($userinfo['username']);
	  if($result)$success_msg = "Your qualified accruals have been transfered to your wallet";
	  else $error_msg = "Nothing to withdraw";      
   }


  $per_page = $settings['system']['rows_per_page'];
  
  $sql  = "SELECT * FROM ".PREFIX."_instant_deposits ";
  $sql .= "WHERE username = '".$userinfo["username"]."' ";
 // $sql .= "AND status = 'Active' ";
  $sql .= "ORDER BY id DESC";
 
  $result = $db->query_db($sql,$print = DEBUG);


			$page		 = $system->getVar('page');	
			$num_rows 	 = $db->num_rows($result);
			if(!empty($page)) 
			{
				$start = ($page - 1) * $settings['system']['rows_per_page']; 
			}			
			else
			{
				$start 	= 0;
				$page	= 1;
			}				

			
  $sql .= " LIMIT $start, $per_page "; 
  $result = $db->query_db($sql,$print = DEBUG);
  if ($db->num_rows($result) > 0)
  {
	 	$rows = $start;
	 	while ($row = $db->fetch_db_array($result))
		{	
			$rows++;
			$sql  = "SELECT name FROM ".PREFIX."_payment_processors WHERE id = '".$row['deposit_method']."'  LIMIT 1";
            $result2 = $db->query_db($sql,$print = DEBUG);
            $prow = $db->fetch_db_array($result2);
			$row["processor"] = $prow["name"];
			$row['accounts'] = $payment->userAccounts($userinfo['username'],$row['deposit_method']);
			
			$row['earned'] = $in->earned($row['id']);
	        $row['balance'] = $row['earned'] - $row["paid_amount"];
	      
	        if($to_surf > 0)$action_link = '<a href="#" onclick="surf('.$to_surf.')">Withdraw</a>';
			elseif($row['balance'] < $in_settings['min_withdrawal'])$action_link = '<a href="#" onclick="minimum('.$in_settings['min_withdrawal'].')">Withdraw</a>';
		    else $action_link = "<a href='#' onclick='withdraw(".$row["id"].")'>Withdraw</a>";
	        $row['link'] = $action_link;
			$deposits[$rows] = $row;	
						
		}
  }


  
   $pageInfo['title'] = 'Withdraw';
  
   $pageInfo['map'] = array('Withdraw' => '');
   
   $loader = new Loader;
   
   $data= get_defined_vars();
   
   $loader->setVar($data);
   
   $loader->mainHeader('private');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('private');
   
   $loader->displayOutput();
   
 ?>

 