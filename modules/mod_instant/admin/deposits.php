<?php
global $system,$db,$settings,$admininfo;

$system->importClass('account')->checkPrivilege(2);

$payment = $system->importClass('payment');

$wallet = $system->importClass('wallet');

$in = $system->importClass('instant');

$var = $system->getVar();

$processors = $payment->processors();



if($var['update'] == 'y'){
	
		
		for ($i = 0; $i < count ($var['list']); $i++)
		{
			
			$n 	= $i+1;
			$tid = $var['list'][$i];
			$sql = "SELECT * FROM ".PREFIX."_instant_deposits WHERE id = '".$tid."'";
			$res = $db->query_db($sql,$print = DEBUG);
			if ($db->num_rows($res) > 0)
			{ 
                $row = $db->fetch_db_array($res);	
			    switch($var['submit'])
	            {
		          /* case"Activate":
					   $updated = $in->activateDeposit($tid);
					   if($updated)  $success[] = "Deposit #".$tid." has been updated ";
				   break;
				   case"Deactivate":
					   $updated = $in->deactivateDeposit($tid);
					   if($updated)  $success[] = "Deposit #".$tid." has been updated ";
				   break;*/
				   case"Delete":
				       $updated = $in->deleteDeposit($tid);
					   if($updated)  $success[] = "Deposit #".$tid." has been deleted ";
				   break;
				   
				   
			     }
			     $db->free_result($res);
	       }	
		  
		}
		
		if(is_array($errors) && !empty($errors)) while (list($key,$value) = each($errors)) $return_msg.= $value.'<br>';	
		elseif(is_array($success) && !empty($success)) while (list($key,$value) = each($success)) $return_msg.= $value.'<br>';	
	
	}






$per_page = $settings['system']['rows_per_page'];	
	
$sql  = "SELECT COUNT(*) as total,DATE(purchase_date) as date FROM ".PREFIX."_instant_deposits ";

$sql .= "WHERE id != '' ";
	
if($var['submit'] == 'Search'){


         if(is_numeric($var['total_from']) || is_numeric($var['total_to'])){
	  
	     if(!is_numeric($var['total_to'])) $sql .= "AND deposit_amount > '".$var['total_from']."'  ";
	   
	     elseif(!is_numeric($var['total_from'])) $sql .= "AND deposit_amount < '".$var['total_to']."'  ";
	   
	     else $sql .= "AND deposit_amount BETWEEN '".$var['total_from']."' AND '".$var['total_to']."' "; 
		 
         }
		   
        // if(isset($var['currency']) && $var['currency']!='A') $sql .= "AND currency = '".$var['currency']."' ";
	  

         if(isset($var['status']) && count($var['status'])> 0)
	     { 
	        $sql .= "AND ( ";
	        foreach($var['status'] as $status) { $sql .= "status = '".$status."' "; if($status != end($var['status'])) $sql .= "OR ";}
	        $sql .= ") ";
	      }
		  
         if(!empty($var['time_from']) || !empty($var['time_to']))
	     { 
	    
		    $time1 = explode("-",$var['time_from']);
		    $time_from = $time1[0]."-".$time1[1]."-".$time1[2];
		
		    $time2 = explode("-",$var['time_to']);
		    $time_to = $time2[0]."-".$time2[1]."-".$time2[2];
		
		    if(!checkdate($time2[1],$time2[2],$time2[0]) && checkdate($time1[1],$time1[2],$time1[0])) $sql .= "AND DATE(purchase_date) = date('".$time_from."')  ";
	   
	        elseif(checkdate($time2[1],$time2[2],$time2[0]) && !checkdate($time1[1],$time1[2],$time1[0])) $sql .= "AND DATE(purchase_date) < date('".$time_to."')  ";
	   
	        else  $sql .= "AND DATE(purchase_date) BETWEEN date('".$time_from."') AND date('".$time_to."') ";
	 
	     }
     
          
		   
          if(!empty($var['deposit_method']))$sql .= "AND deposit_method = '".$var['deposit_method']."' ";
	  
	      if(!empty($var['username']))$sql .= "AND username = '".$var['username']."' ";

}


  
  $sql .= "ORDER BY id DESC";
  $result = $db->query_db($sql,$print = DEBUG);
  $num_rows = $db->db_result($result, 0, 'total');
  $page = $var['page'];
  $per_page = (is_numeric($var['per_page']) 
	             && $var['per_page'] <= $settings['system']['rows_per_page'])?
				 $var['per_page']:$settings['system']['rows_per_page'];		 	
  if(!empty($page)){$start = ($page - 1) * $per_page;}
  else { $start 	= 0; $page	= 1; }		
  $sql = str_replace("COUNT(*) as total", " * ",$sql);
  $sql .= " LIMIT $start, $per_page "; 
  $result = $db->query_db($sql,$print = DEBUG);
  if ($db->num_rows($result) > 0)
  {
	 	$rows = $start;
	 	while ($row = $db->fetch_db_array($result))
		{	
			$rows++;
			$sql  = "SELECT name FROM ".PREFIX."_payment_processors WHERE id = '".$row['deposit_method']."'  LIMIT 1";
            $result2 = $db->query_db($sql,$print = DEBUG);
            $prow = $db->fetch_db_array($result2);
			$row["processor"] = $prow["name"];
			//$row['earned'] = $in->earned($row['id']);
	        //$row['balance'] = $row['earned_amount'] - $row["paid_amount"];
		    $deposits[$rows] = $row;
				
		}
  }
 
 
	
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'deposits.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();  

?>
