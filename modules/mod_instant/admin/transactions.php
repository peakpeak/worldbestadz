<?php
global $system,$db,$settings,$admininfo;

$system->importClass('account')->checkPrivilege(2);

$payment = $system->importClass('payment');

$wallet = $system->importClass('wallet');

$var = $system->getVar();

$processors = $payment->processors();

$per_page = $settings['system']['rows_per_page'];	
	
$sql  = "SELECT COUNT(*) as total,DATE(date) as date FROM ".PREFIX."_instant_transactions ";

$sql .= "WHERE id != '' ";
	
if($var['submit'] == 'Search'){


         if(is_numeric($var['total_from']) || is_numeric($var['total_to'])){
	  
	     if(!is_numeric($var['total_to'])) $sql .= "AND amount > '".$var['total_from']."'  ";
	   
	     elseif(!is_numeric($var['total_from'])) $sql .= "AND amount < '".$var['total_to']."'  ";
	   
	     else $sql .= "AND amount BETWEEN '".$var['total_from']."' AND '".$var['total_to']."' "; 
		 
         }
		   
        // if(isset($var['currency']) && $var['currency']!='A') $sql .= "AND currency = '".$var['currency']."' ";
	  

         if(isset($var['status']) && count($var['status'])> 0)
	     { 
	        $sql .= "AND ( ";
	        foreach($var['status'] as $status) { $sql .= "status = '".$status."' "; if($status != end($var['status'])) $sql .= "OR ";}
	        $sql .= ") ";
	      }
		  
         if(!empty($var['time_from']) || !empty($var['time_to']))
	     { 
	    
		    $time1 = explode("-",$var['time_from']);
		    $time_from = $time1[0]."-".$time1[1]."-".$time1[2];
		
		    $time2 = explode("-",$var['time_to']);
		    $time_to = $time2[0]."-".$time2[1]."-".$time2[2];
		
		    if(!checkdate($time2[1],$time2[2],$time2[0]) && checkdate($time1[1],$time1[2],$time1[0])) $sql .= "AND DATE(date) = date('".$time_from."')  ";
	   
	        elseif(checkdate($time2[1],$time2[2],$time2[0]) && !checkdate($time1[1],$time1[2],$time1[0])) $sql .= "AND DATE(date) < date('".$time_to."')  ";
	   
	        else  $sql .= "AND DATE(date) BETWEEN date('".$time_from."') AND date('".$time_to."') ";
	 
	     }
     
          if(!empty($var['type']))$sql .= "AND type = '".$var['type']."' ";
		   
          if(!empty($var['method']))$sql .= "AND method = '".$var['method']."' ";
	  
	      if(!empty($var['username']))$sql .= "AND username = '".$var['username']."' ";

}


  
  $sql .= "ORDER BY id DESC";
  $result = $db->query_db($sql,$print = DEBUG);
  $num_rows = $db->db_result($result, 0, 'total');
  $page = $var['page'];
  $per_page = (is_numeric($var['per_page']) 
	             && $var['per_page'] <= $settings['system']['rows_per_page'])?
				 $var['per_page']:$settings['system']['rows_per_page'];		 	
  if(!empty($page)){$start = ($page - 1) * $per_page;}
  else { $start 	= 0; $page	= 1; }		
  $sql = str_replace("COUNT(*) as total", " * ",$sql);
  $sql .= " LIMIT $start, $per_page "; 
  $result = $db->query_db($sql,$print = DEBUG);
  if ($db->num_rows($result) > 0)
  {
	 	$rows = $start;
	 	while ($row = $db->fetch_db_array($result))
		{	
			$rows++;
			$sql  = "SELECT name FROM ".PREFIX."_payment_processors WHERE id = '".$row['method']."'  LIMIT 1";
            $result2 = $db->query_db($sql,$print = DEBUG);
            $prow = $db->fetch_db_array($result2);
			$row["processor"] = $prow["name"];		
		    $transactions[$rows] = $row;
				
		}
  }
 
 
  
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'transactions.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();  

?>
