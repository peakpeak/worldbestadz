<?php
global $system,$db,$settings,$validate;

$system->importClass('account')->checkPrivilege(3);

$var = $system->getVar();

if ($var['submit'])
{
	
	//Validate
	switch($var['submit']){
	
		case 'Add':
			$date = explode('-',$var['date']);
			$sql = "SELECT * FROM ".PREFIX."_instant_roi_override WHERE date = '".$db->sql_quote($var['date'])."' LIMIT 1";
			$result = $db->query_db($sql,$print = DEBUG);
			if ($var['date'] == null) $return_msg = "Please add a valid date ";	
			elseif(!checkdate($date[1],$date[2],$date[0]))$return_msg = "Enter a valid date ";
			elseif($db->num_rows($result) > 0)  $return_msg = "A record for this date already exist ";
			elseif (!is_numeric($var['roi'])) $return_msg = "Please add a valid percentage ";	
			else
			{	
				 $table = "_instant_roi_override ";
				 $insert_array = array(
					 'date'=>$var['date'],
					 'roi'=>$var['roi'],	 
				 );
				 $added = $db->insert_into_db($insert_array,$table);
				 if($added) $return_msg = "ROI added";   
			}
		
		break;
		case 'Edit':
			$date = explode('-',$var['date']);
			$sql = "SELECT * FROM ".PREFIX."_instant_roi_override WHERE date = '".$db->sql_quote($var['date'])."' AND id != '".$var['oid']."' LIMIT 1";
			$result = $db->query_db($sql,$print = DEBUG);
			if ($var['date'] == null) $return_msg = "Please add a valid date ";	
			elseif(!checkdate($date[1],$date[2],$date[0]))$return_msg = "Enter a valid date ";
			elseif($db->num_rows($result) > 0)  $return_msg = "A record for this date already exist ";
			elseif (!is_numeric($var['roi'])) $return_msg = "Please add a valid percentage ";
			else
			{		
					
				 $table = "_instant_roi_override ";
				 $update_array = array(
					 'date'=>$var['date'],
					 'roi'=>$var['roi'],	 
				 );
				 $edited = $db->update_db($update_array, $table, "id", $var['oid']);
				 if($edited) $return_msg = "ROI edited";   
			   
			}
		
		break;
	}
	
	
}


if($var['delete'] == 'y' && $var['oid'] != '')
{
    $sql = "DELETE FROM ".PREFIX."_instant_roi_override WHERE id = '".$var['oid']."' LIMIT 1";
	$deleted = $db->query_db($sql,$print = DEBUG);
	if($deleted)$return_msg = "ROI deleted";  
}


$sql = "SELECT * FROM ".PREFIX."_instant_roi_override ORDER by date DESC ";
$result = $db->query_db($sql,$print = DEBUG);
if($db->num_rows($result) > 0) while ($row = $db->fetch_db_array($result)) $rois[$row['id']] = $row;			

if($var['edit'] == 'y' && isset($var['oid'])) 
{

	$sql = "SELECT * FROM ".PREFIX."_instant_roi_override WHERE id = '".$var['oid']."' LIMIT 1";
	$res = $db->query_db($sql,$print = DEBUG);
	if ($db->num_rows($res) > 0) $erow = $db->fetch_db_array($res);

}

   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $loader->setVar($data);
   
   $tpl_add_file = dirname(__FILE__).DS.'tpl'.DS.'add_roi.php';
   
   $tpl_edit_file = dirname(__FILE__).DS.'tpl'.DS.'edit_roi.php';
   
   $loader->placeHolder['form'] =  ($var['edit'] == 'y' && isset($var['oid'])) ?  $tpl_edit_file :  $tpl_add_file;
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'roi_override.php';
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();

