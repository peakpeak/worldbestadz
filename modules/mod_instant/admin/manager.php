<?php
global $system,$db,$settings;

$system->importClass('account')->checkPrivilege(3);

$payment = $system->importClass('payment');

$processors = $payment->processors();

$wallet = $system->importClass('wallet');

$in = $system->importClass('instant');

$in_settings = $in->getSettings();

$var = $system->getVar();



   switch($var['Submit'])
   {
   
		
		case"Add":

        if($var['username'] == null || !Account::isUser($var['username']))$errors[] = "Add a valid username";
		elseif($var['processor'] == '') $errors[] =  "Select a valid processor";
	    elseif($var['amount'] == '') $errors[] =  "Please add a valid amount";
	    else if($var['amount'] < $in_settings['min_deposit']) $errors[] =  "Minimum deposit allowed is $".$in_settings['min_deposit'];
	    elseif($var['amount'] > $in_settings['max_deposit']) $errors[] =  "Maximum deposit allowed is $".$in_settings['max_deposit'];
        if(is_array($errors) && !empty($errors)) while (list($key,$value) = each($errors)) $return_msg.= $value.'<br>';	
        else 
	    {
	         $add_commission = ($var["add_commission"] == null)? false:true; 
	         $description = ($var["memo"] == null)? $var["amount"]." deposit by ".$var['username']:$var["memo"];
			 $in->addDeposit($var['username'],$var["amount"],$var["processor"],$add_commission);
             $in->addTransaction($var['username'],"Deposit",$description,$var["processor"],$var["amount"],0.00);
	         $return_msg = "Deposit sucessfully added";
	    }

        break;
 
   
   
   }
  
   
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'manager.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();
?>
