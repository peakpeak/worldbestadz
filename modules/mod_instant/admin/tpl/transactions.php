<h1>Transactions</h1>

<p class="info">Here you can view transactions in the system</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

<div class="box" style="margin-bottom:10px">
<form action="index.php" method="get" >
<input  type="hidden" name="mod" value="instant" />
<input  type="hidden" name="go" value="transactions" />
  <table cellpadding="0" cellspacing="0" class="utility">
  
  
    <tr style="background-color:transparent">
      <td style="position:relative" colspan="2">
	   <script>$(function() { $( "#datepicker_1" ).datepicker({ dateFormat: 'yy-mm-dd' });});</script>
	   <script>$(function() { $( "#datepicker_2" ).datepicker({ dateFormat: 'yy-mm-dd' });});</script>
	   
	    Date: 
	    <input type="text" name="time_from" value="<?php echo $var["time_from"]?>" size="12"  id="datepicker_1" />&nbsp;-&nbsp;
	    <input type="text" name="time_to" value="<?php echo $var["time_to"]?>" size="12" id="datepicker_2"/>	  
		Amount: 
	    <input type="text" name="total_from" value="<?php echo $var["total_from"]?>" size="10" />&nbsp;-&nbsp;
	    <input type="text" name="total_to" value="<?php echo $var["total_to"]?>" size="10"  />
				 
	 </td>
     
    </tr>
	<tr style="background-color:transparent">
     <td  class="border">
	 Type
	    <select name="type">
	    <option value="" >All</option>
		<option value="Purchase" <?php if( $var["type"] == 'Purchase'){ ?> selected="selected" <?php } ?> >Purchase</option>	
		<option value="Rebate" <?php if( $var["type"] == 'Rebate'){ ?> selected="selected" <?php } ?> >Rebate</option>
		<option value="Commission" <?php if( $var["type"] == 'Commission'){ ?> selected="selected" <?php } ?> >Commission</option>
		<option value="Principal Return" <?php if( $var["type"] == 'Principal Return'){ ?> selected="selected" <?php } ?> >Principal Return</option> 
	 
	    </select>
     Processor:
	    <select name="method">
	    <option value="" >All</option>
	    <?php foreach ($processors as $id => $value) {?>
		<option value="<?php echo $id?>" <?php if( $var["method"] == $id){ ?> selected="selected" <?php } ?> ><?php echo $value; ?></option>	 
	    <?php } ?>
	    </select></td>
     <td align="left" class="border"> <table cellpadding="0" cellspacing="0" border="0"> 
	 <tr>
	 <td>Status:</td>
     <td><input type="checkbox" class="html-checkboxes" name="status[]" value="Completed"<?php if(in_array('Completed',$var["status"])){ ?> checked="checked" <?php } ?>/>Completed</td>
     <td><input type="checkbox" class="html-checkboxes" name="status[]" value="Pending"<?php if(in_array('Pending',$var["status"])){ ?> checked="checked" <?php } ?>/>Pending</label>&nbsp;</td>

</tr></table></td>
    </tr>
	
	
    <tr style="background-color:transparent">
	
     <td  class="border"> Username: <input type="text" name="username" value="<?php echo $var["username"]?>"  /></td>
     <td align="left" class="border">
	 
	 Items Per Page: 
	 <select class="select" ><option selected="selected" value="10">10</option>
     <option value="25">25</option>
     <option value="50">50</option>
     <option value="100">100</option>
     </select>
     &nbsp;&nbsp;
     Go To: 
	 <select class="select" >
	 <option value="1" selected="selected">1</option>
     </select>
	 <input  class="button-alt-sml" type="button" name="clear"  value="Clear Filters" onclick="window.location.href='?mod=instant&go=transactions'"  />
	 <input  class="button-alt-sml" type="submit" name="submit"  value="Search"  /></td>
    </tr>
  </table>
</form>
</div>


<form  method="post" action="" id="transactions">
<input type="hidden" name="update" value="y" />
<table  cellpadding="0" cellspacing="0"  class="tableS">
 <thead>
  <tr>
    <th class="left">No</th>
	<th class="left">Date</th>
    <th class="left">Username</th>
	<th class="left">Type</th>
	<th class="left">Description</th>
    <th class="left">Amount</th>
    <th class="left">Fee</th>
    <th class="left">Processor</th>
	<th >Status</th>
   
 
  </tr>
  </thead>
  
  <?php  $i = 0; foreach ($transactions as $num => $row){ $i++; ?>
   
  <tr>
     
    <td align="center"><?php echo $num; ?></td>
	<td align="left"><?php echo $row['date']; ?></td>
    <td align="left"><?php echo $row['username']; ?></td>
	<td align="left"><?php echo $row['type']; ?></td>
	<td align="left"><?php echo $row['description']; ?></td>
    <td align="left">$<?php echo number_format($row['amount'],2); ?></td>
    <td align="left">$<?php echo number_format($row['fee'],2); ?></td>
    <td align="left"><?php echo $row['processor']; ?></td>
	<td align="center"><?php echo $row['status']; ?></td>
	
  </tr>
	   <?php } ?>
 
       <?php if(count($transactions) == 0) { ?><tr> <td colspan="9"  align="center">No transactions found</td></tr><?php } ?>

     <tr>
	    <td colspan="9" align="center"> <?php echo $system->getPaginationString($page,$num_rows,$per_page,3,$system->curPageURL(),"page");?></td>
	 </tr>

  </table>
</form>
<script language="javascript">
<!-- Begin

    var checkflag = "false";

    function check(field) {
    var checks = document.getElementsByName('list[]');
    if (checkflag == "false") {
     for (i = 0; i < checks.length; i++){
      checks[i].checked = true;
     }
      checkflag = "true";
      return "Uncheck All";
   }
   else {
     for (i = 0; i < checks.length; i++) {
      checks[i].checked = false; }
      checkflag = "false";
      return "Check All"; 
  }
}


</script>
<script type="text/javascript">
 
   
	 var first = 0;
	 var last = <?php echo $i;?>;	
	  fancyjQuery(document).ready(function() {
			
			for (i = first; i  <= last; i++) {
			
			fancyjQuery("#popup"+i).fancybox({
				'width'				: '90%',
				'height'			: '100%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
          }
		
		});
  </script>