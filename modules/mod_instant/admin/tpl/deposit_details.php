<h1>Deposit Details</h1>

<p class="info">Here you can view deposit details</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>
	
	
	<form  action="" method="post">
    <input type="hidden" name="sid" value="<?php echo $var['did']?>" />
	<table cellpadding="0" cellspacing="0"   class="tableS">
	<thead>
	<tr>
		<th colspan="2" align="left">Deposit Details</th>	
	</tr>
	</thead>
	 <?php if(count($deposit) == 0) : ?>
	 
      <tr>
		<td colspan="2"> - no records found - </td>
	  </tr>
	  <?php else: ?>
	   <tr>
						
						<td width="30%">Username</td>
                        <td nowrap="nowrap"><?php echo $deposit["username"]?></td>
		</tr> 
       <tr>
						
						<td>Purchase date</td>
                        <td nowrap="nowrap"><?php echo $deposit["purchase_date"]?></td>
		</tr>
		<tr>
						
						<td>Amount</td>
                        <td nowrap="nowrap">$<?php echo  $deposit['deposit_amount'] ?></td>
	   </tr>
	    <tr>
						
						<td>Earned</td>
                        <td nowrap="nowrap">$<?php echo  $deposit['earned_amount'] ?></td>
	  </tr>
       <tr>
						
						<td>Withdrawn (to wallet)/td>
                        <td nowrap="nowrap">$<?php echo  $deposit['paid_amount'] ?></td>
	  </tr>
	  <tr>
						<td>Payment Method</td>
                        <td><?php echo $deposit["processor"]?></td>
	   </tr>
       <tr>
						<td>Status</td>
                        <td><?php echo $deposit["status"]?> <?php if($deposit["status"] == "Expired"){?> on <?php echo $deposit["expire_date"]; }?></td>
	   </tr>
	</table>
   
</form>

	