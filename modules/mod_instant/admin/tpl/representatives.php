<h1>Representatives Applications</h1>

<p class="info">Here you can manage application requests</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

<div class="box" style="margin-bottom:10px">
<form action="index.php" method="get">
<input  type="hidden" name="mod" value="instant" />
<input  type="hidden" name="go" value="representatives" />
  <table cellpadding="0" cellspacing="0" class="utility">
    <tr style="background-color:transparent">
      <td style="position:relative">
	    Username: <input type="text" name="username" value="<?php echo $var["username"]?>"  />
		<!--Request Date: 
	    <input type="text" name="date_from" value="<?php echo $var["date_from"]?>" size="8" class="input-text-short" />&nbsp;-&nbsp;<input type="text" name="date_to" value="<?php echo $var["date_to"]?>" size="8" class="input-text-short" />-->
		
	  </td>
       <td align="left"><table cellpadding="0" cellspacing="0" border="0"> <tr>
	   <td>Status:</td>
	   <td><input type="checkbox" class="html-checkboxes" name="status[]" value="Pending"<?php if(in_array('Pending',$var["status"])){ ?> checked="checked" <?php } ?>/>Pending</td>
	   <td><input type="checkbox" class="html-checkboxes" name="status[]" value="Approved"<?php if(in_array('Approved',$var["status"])){ ?> checked="checked" <?php } ?>/>Approved</td>      <td><input type="checkbox" class="html-checkboxes" name="status[]" value="Disapproved"<?php if(in_array('Disapproved',$var["status"])){ ?> checked="checked" <?php } ?>/>Disapproved</td>

</tr></table></td>
    </tr>
    <tr style="background-color:transparent">
     <td  class="border">  &nbsp;  </td>
     <td align="left" class="border">
	 Items Per Page: 
	 <select class="select" ><option selected="selected" value="10">10</option>
     <option value="25">25</option>
     <option value="50">50</option>
     <option value="100">100</option>
     </select>
     &nbsp;&nbsp;
     Go To: 
	 <select class="select" >
	 <option value="1" selected="selected">1</option>
     </select>
	 <input  class="button-alt-sml" type="button" name="clear"  value="Clear Filters" onclick="window.location.href='?mod=instant&go=representatives'"  />
	 <input  class="button-alt-sml" type="submit" name="submit"  value="Search"  />
</td>
    </tr>
  </table></form>
</div>


<form  method="post" action="">
<input  type="hidden" name="update" value="y" />
<table  cellpadding="0" cellspacing="0"  class="tableS">
 <thead>
  <tr>
    <th >No</th>
    <th align="left">Username</th>
	<th align="left">Phone</th>
	<th align="left">Email</th>
	<th align="left">Country</th>
	<th align="left">Deposit total</th>
	<th >Status</th>
    <th >Select</th>
 
  </tr>
  </thead>
  <?php foreach ($applications as $num => $row){?> 
  <tr>
     
    <td align="center"><?php echo $num; ?></td>
    <td ><?php echo $row['username']; ?></td>
	<td ><?php echo $row['info']['telephone']; ?></td>
	<td ><?php echo $row['info']['email']; ?></td>
	<td ><?php echo $row['info']['country']; ?></td>
	<td >$<?php echo $row['deposits']; ?></td>
	<td align="center"><?php echo $row['status']; ?> </td>
	<td align="center"><input type="checkbox" name="list[]" value="<? echo $row["id"]?>"></td>
  </tr>
  
 <?php } ?>
 
 <?php if(count($applications) == 0) { ?> <tr> <td colspan="8"  align="center">No applications found</td></tr><?php } ?>
	
     <tr>
	    <td colspan="8" align="center"> <?php echo $system->getPaginationString($page,$num_rows,$per_page,3,$system->curPageURL(),"page");?></td>
	 </tr>

  <tr>
    <td colspan="8"  align="center">
	  
      <input type=button value="Check All" onclick="this.value=check(this.form.checkbox)" id="button" />
	  <input type="submit" name="submit" value="Pend" id="button"/>
	  <input type="submit" name="submit" value="Approve" id="button"/>
	  <input type="submit" name="submit" value="Disapprove" id="button"/>
    
     
   </td>
  </tr>
  </table>
</form>
<script language="javascript">
<!-- Begin

    var checkflag = "false";

    function check(field) {
    var checks = document.getElementsByName('list[]');
    if (checkflag == "false") {
     for (i = 0; i < checks.length; i++){
      checks[i].checked = true;
     }
      checkflag = "true";
      return "Uncheck All";
   }
   else {
     for (i = 0; i < checks.length; i++) {
      checks[i].checked = false; }
      checkflag = "false";
      return "Check All"; 
  }
}
//  End -->
</script>

