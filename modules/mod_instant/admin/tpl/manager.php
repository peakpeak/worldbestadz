<h1>Deposits Manager</h1>

<p class="info">Here you can add deposit(s) to a members account</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

<form  method="post" action="">
<table cellpadding="0" cellspacing="0" class="tableS">
<thead>
  <tr >
    <th colspan="2" class="left" >Add Deposit</th>
  </tr>
  </thead>
   <tr>
     <td width="30%">Username</td>
     <td><input type="text" name="username" value="<?php echo $var['username'] ?>"></td>
  </tr>
  <tr>
     <td>Amount</td>
     <td><input type="text" name="amount" value="<?php echo $var['amount'] ?>"></td>
  </tr>
  
  <tr>
    <td>Payment Method</td>
    <td><select name="processor">
		  <option value="" >- Select -</option>
		  <?php foreach($processors as $id => $value) { ?>
		  <option value="<?php echo $id?>"  <?php if($var["processor"] == $id){?> selected="selected" <?php } ?>><?php echo $value?></option>
		  <?php } ?>
	 </select>
  </td>
  </tr>
   <tr>
     <td></td>
     <td><input type="checkbox" name="add_commission">Add Commission</td>
  </tr>
  <tr>
     <td>Memo</td>
     <td><textarea name="memo"><?php echo $var['memo'] ?></textarea></td>
  </tr>
  <tr>
    <td colspan="2" class="left" ><input type="submit" name="Submit" value="Add"/></td>
  </tr>
  </table>
</form>


