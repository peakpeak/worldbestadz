<h1>Instant AdShare Settings</h1>

<p class="info">Here you can setup system plan</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

<form  method="post" action="">
<table  cellpadding="0" cellspacing="0"  class="tableS">
<thead>
  <tr >
    <th colspan="2" class="left" >Edit Settings</th>
  </tr>
  </thead>
  <tr>
     <td width="30%">Unit cost ($)</td>
     <td><input type="text" name="unit_cost" value="<?php echo $in_settings["unit_cost"] ?>"></td>
  </tr>
  <tr>
     <td>Minimum units</td>
     <td><input type="text" name="min_unit" value="<?php echo $in_settings["min_unit"] ?>"> <img src="images/tooltip.png" alt="Tip" class="tooltip" title="Minimum units a user can purchase per transaction" /></td>
  </tr>
  <tr>
     <td>Maximum units</td>
     <td><input type="text" name="max_unit" value="<?php echo $in_settings["max_unit"] ?>"> <img src="images/tooltip.png" alt="Tip" class="tooltip" title="Maximum units a user can purchase per transaction" /></td>
  </tr>
   <tr>
     <td>Minimum withdrawal ($)</td>
     <td><input type="text" name="min_withdrawal" value="<?php echo $in_settings["min_withdrawal"] ?>"> <img src="images/tooltip.png" alt="Tip" class="tooltip" title="This is the minimum amount a pack should have accumulated before a user can transfer/withdraw the earnings to their wallet" /></td>
  </tr>
  <tr>
     <td>Default ROI per day</td>
     <td><input type="text" name="roi_per_day" id="roi_per_day" onkeyup="calcRoi('roi_per_day')" value="<?php echo $in_settings["roi_per_day"] ?>"> % <img src="images/tooltip.png" alt="Tip" class="tooltip" title="(Return on investment) This is the percentage of user's deposit amount that will be paid to user daily. You can override this value for specific dates from the 'ROI Override' menu" /></td>
  </tr>
   <tr>
     <td>ROI per hour</td>
     <td><input type="text" id="roi_per_hour"  onkeyup="calcRoi('roi_per_hour')" value="" disabled="disabled"> % </td>
  </tr>
  <tr>
     <td>ROI per minute</td>
     <td><input type="text" id="roi_per_min"  onkeyup="calcRoi('roi_per_min')" value="" disabled="disabled"> % </td>
  </tr>
   <tr>
     <td>ROI per second</td>
     <td><input type="text" id="roi_per_sec" onkeyup="calcRoi('roi_per_sec')" value=""  disabled="disabled"> % </td>
  </tr>
   <tr>
     <td>Max ROI</td>
     <td><input type="text" name="max_roi" value="<?php echo $in_settings["max_roi"] ?>"> % </td>
  </tr>
  <tr>
     <td>Repurchase percentage</td>
     <td><input type="text" name="repurchase" value="<?php echo $in_settings["repurchase"] ?>"> % <img src="images/tooltip.png" alt="Tip" class="tooltip" title="This is the percentage of user earnings that will be reserved in repurchase balance. Repurchase balance is only used to purchase new shares" /></td>
  </tr>
  <tr>
     <td>Referral commission</td>
     <td><input type="text" name="referral_commission" value="<?php echo $in_settings["referral_commission"] ?>"><img src="images/tooltip.png" alt="Tip" class="tooltip" title="Commission paid to members when their referrals make a deposit. Add numbers seperated by commas. Example 3,2,1 means 3% on 1st level,2% on second level and 1% on third level or enter a single number for 1st level only" /></td>
  </tr>
   <tr>
      <td>Ads credit package per unit</td>
      <td><select name="ads_packages">
          <option value="" >- Select -</option>
          <?php foreach ($adpacks as $id => $data){ ?>
          <option value="<?php echo $id?>"  <?php if($in_settings["ads_packages"] == $id){?> selected="selected" <?php } ?>><?php echo $data['name'];?></option>
          <?php } ?>
        </select> <img src="images/tooltip.png" alt="Tip" class="tooltip" title="Choose what ads credit package to add to user's account for each $ deposit. You have to add credit package from 'Ads Manager' -> 'Ad Credit Package' from admin menu to see credit options here. Ads Module must be installed to use this feature. " />
        <?php if(count($adpacks) == 0) { ?>
        <p class="info">You have to add ads module to use this feature </p>
        <?php } ?>
      </td>
  </tr>
  <tr>
   <td>Status</td>
   <td><select name="status">
           <option value="Active"  <?php if($in_settings["status"] == "Active") { ?> selected="selected" <?php } ?>>Active</option>
           <option value="Inactive" <?php if($in_settings["status"] == "Inactive") { ?> selected="selected" <?php } ?>>Inactive</option>
        </select> 
     </td>
	</tr>
	
    <tr>
      <td colspan="2" class="left"><input type="submit" name="Submit" value="Update" /> </td>
    </tr>
  </table>
</form>

<script>

function calcRoi(period)
{

 var roi =  document.getElementById(period).value;
 //if(roi < 0) return false;
 
 switch(period)
 {
   case 'roi_per_sec':baseRate = roi; break;
   case 'roi_per_min':baseRate = (roi/60); break;
   case 'roi_per_hour':baseRate = (roi/(60*60)); break;
   case 'roi_per_day':baseRate = (roi/(24*60*60)); break;
 
 }
 
 var second = baseRate * 1; 
 var minute = baseRate * (60);
 var hour = baseRate * (60*60);
 var day = baseRate * (24*60*60);
 
 document.getElementById('roi_per_sec').value = second.toFixed(8);
 document.getElementById('roi_per_min').value = minute.toFixed(8);
 document.getElementById('roi_per_hour').value = hour.toFixed(8);
 //document.getElementById('roi_per_day').value = day;//.toFixed(8);
 

}


calcRoi('roi_per_day');
</script>
