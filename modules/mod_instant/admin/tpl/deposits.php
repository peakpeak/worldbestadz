<h1>Deposits</h1>

<p class="info">Here you can view deposits in the system</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

<div class="box" style="margin-bottom:10px">
<form action="index.php" method="get" >
<input  type="hidden" name="mod" value="instant" />
<input  type="hidden" name="go" value="deposits" />
  <table cellpadding="0" cellspacing="0" class="utility">
  
  
    <tr style="background-color:transparent">
      <td style="position:relative" >
	   <script>$(function() { $( "#datepicker_1" ).datepicker({ dateFormat: 'yy-mm-dd' });});</script>
	   <script>$(function() { $( "#datepicker_2" ).datepicker({ dateFormat: 'yy-mm-dd' });});</script>
	   
	    Purchase Date: 
	    <input type="text" name="time_from" value="<?php echo $var["time_from"]?>" size="12"  id="datepicker_1" />&nbsp;-&nbsp;
	    <input type="text" name="time_to" value="<?php echo $var["time_to"]?>" size="12" id="datepicker_2"/>	  
		Amount: 
	    <input type="text" name="total_from" value="<?php echo $var["total_from"]?>" size="10" />&nbsp;-&nbsp;
	    <input type="text" name="total_to" value="<?php echo $var["total_to"]?>" size="10"  />
				 
	 </td>
     <td><table cellpadding="0" cellspacing="0" border="0"> 
	 <tr>
	 <td>Status:</td>
     <td><input type="checkbox" class="html-checkboxes" name="status[]" value="Active"<?php if(in_array('Active',$var["status"])){ ?> checked="checked" <?php } ?>/>Active</td>
     <td><input type="checkbox" class="html-checkboxes" name="status[]" value="Expired"<?php if(in_array('Expired',$var["status"])){ ?> checked="checked" <?php } ?>/>Expired</label>&nbsp;</td>

</tr></table></td>
    </tr>
	
    <tr style="background-color:transparent">
	
     <td  class="border"> 
	  Processor:
	    <select name="deposit_method">
	    <option value="" >All</option>
	    <?php foreach ($processors as $id => $value) {?>
		<option value="<?php echo $id?>" <?php if( $var["deposit_method"] == $id){ ?> selected="selected" <?php } ?> ><?php echo $value; ?></option>	 
	    <?php } ?>
	    </select>
	 Username: <input type="text" name="username" value="<?php echo $var["username"]?>"  /></td>
     <td align="left" class="border">
	 
	 Items Per Page: 
	 <select class="select" >
	 <option value="10">10</option>
     <option value="25">25</option>
     <option value="50">50</option>
     <option value="100">100</option>
     </select>
     &nbsp;&nbsp;
	 <input  class="button-alt-sml" type="button" name="clear"  value="Clear Filters" onclick="window.location.href='?mod=instant&go=deposits'"  />
	 <input  class="button-alt-sml" type="submit" name="submit"  value="Search"  /></td>
    </tr>
  </table>
</form>
</div>


<form  method="post" action="" id="deposits">
<input type="hidden" name="update" value="y" />
<table  cellpadding="0" cellspacing="0"  class="tableS">
 <thead>
  <tr>
    <th class="left">ID</th>
	<th class="left">Date</th>
    <th class="left">Username</th>
    <th class="left">Amount</th>
    <th class="left">Earned *</th>
	<th class="left">Withdrawn</th>
    <th class="left">Processor</th>
	<th >Status</th>
    <th > </th>
    <th > </th>
  </tr>
  </thead>
  
  <?php  $i = 0; foreach ($deposits as $num => $row){ $i++; ?>
   
  <tr>
     
    <td align="left"><?php echo $row['id']; ?></td>
	<td align="left"><?php echo $row['date']; ?></td>
    <td align="left"><?php echo $row['username']; ?></td>
    <td align="left">$<?php echo number_format($row['deposit_amount'],2); ?></td>
    <td align="left">$<?php echo number_format($row['earned_amount'],2); ?></td>
	<td align="left">$<?php echo number_format($row['paid_amount'],2); ?></td>
    <td align="left"><?php echo $row['processor']; ?></td>
	<td align="center"><?php echo $row['status']; ?></td>
	<td align="center"><a href="?mod=instant&go=deposit_details&did=<?php echo $row["id"]?>" id="popup<?php echo $i?>">Details</a></td>
	<td align="center"><input type="checkbox" name="list[]" value="<? echo $row["id"]?>"></td>

  </tr>
	   <?php } ?>
 
       <?php if(count($deposits) == 0) { ?><tr> <td colspan="10"  align="center">No deposits found</td></tr><?php } ?>

     <tr>
	    <td colspan="10" align="center"> <?php echo $system->getPaginationString($page,$num_rows,$per_page,3,$system->curPageURL(),"page");?></td>
	 </tr>
     <tr>
        <td colspan="10"  align="center">
		  <input type=button value="Check All" onclick="this.value=check(this.form.checkbox)" id="button" />
		  <!--<input type="submit" name="submit" value="Activate" id="button"/>
		  <input type="submit" name="submit" value="Deactivate" id="button"/>-->
		  <input type="submit" name="submit" value="Delete" id="button" onclick="return(confirm('Do you really want to delete?'))"/>
       </td>
  </tr>    
 

  </table>
</form>
<script language="javascript">
<!-- Begin

    var checkflag = "false";

    function check(field) {
    var checks = document.getElementsByName('list[]');
    if (checkflag == "false") {
     for (i = 0; i < checks.length; i++){
      checks[i].checked = true;
     }
      checkflag = "true";
      return "Uncheck All";
   }
   else {
     for (i = 0; i < checks.length; i++) {
      checks[i].checked = false; }
      checkflag = "false";
      return "Check All"; 
  }
}


</script>
<script type="text/javascript">
 
   
	 var first = 0;
	 var last = <?php echo $i;?>;	
	  fancyjQuery(document).ready(function() {
			
			for (i = first; i  <= last; i++) {
			
			fancyjQuery("#popup"+i).fancybox({
				'width'				: '90%',
				'height'			: '100%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
          }
		
		});
  </script>