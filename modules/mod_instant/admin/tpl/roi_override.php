<h1>ROI override</h1>

<p class="info">Here you can override default ROI for a particular date</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

{#include=form}

<br />

<form  method="post" action="">
<table  cellpadding="0" cellspacing="0"  class="tableS">
 <thead>
 <tr>
    <th class="left">Date</th>
    <th class="left">ROI</th>
	<th></th>
  </tr>
  </thead>
  
  <?php foreach ($rois as $id => $roi){ ?>
  
  <tr >
     <td width="30%"><?php echo $roi['date'] ?></td>
	 <td><?php echo $roi['roi'] ?>%</td>
	 <td align="right">
	 <a href="?mod=instant&go=roi_override&edit=y&oid=<?php echo $roi["id"]?>"><img src="images/edit.png" class="tooltip img-wrap2"  alt="" title="Edit"/></a> 
	 <a href="?mod=instant&go=roi_override&delete=y&oid=<?php echo $roi["id"]?>" onclick="return(confirm('Do you really want to delete?'))"><img src="images/delete.png" class="tooltip img-wrap2"  alt="" title="Delete"/></a> </td>
  </tr>
 	 
 <?php } ?>


  <?php if(count($rois) == 0) { ?> <tr> <td colspan="3"  align="center">No data found</td> </tr> <?php } ?>

  
  </table>
</form>
<script> jQueryui(function() { jQueryui( "#datepicker_1" ).datepicker({ dateFormat: 'yy-mm-dd' });});</script>