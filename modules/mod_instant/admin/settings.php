<?php
global $system,$db,$settings;

$system->importClass('account')->checkPrivilege(3);

$payment = $system->importClass('payment');

$in = $system->importClass('instant');


$var = $system->getVar();


     switch($var['Submit'])
	 {
		
		case"Update":
		
			if(!is_numeric($var['roi_per_day']))
			{
					 $errors[] =  "Please enter a valid ROI (Returns on investment) ";
			}
			elseif(!is_numeric($var['max_roi']) || $var['roi_per_day'] > $var['max_roi'])
			{
					 $errors[] =  "Please enter a valid maximum ROI(Returns on investment)  ";
			}
			if(is_array($errors) && !empty($errors)) while (list($key,$value) = each($errors)) $return_msg.= $value.'<br>';	
			else 
			{   
				   
					 $sql = "UPDATE ".PREFIX."_instant_settings SET 
							 unit_cost = '".$var['unit_cost']."',
							 min_unit = '".$var['min_unit']."',
							 max_unit = '".$var['max_unit']."',
							 roi_per_day = '".$var['roi_per_day']."',
							 max_roi = '".$var['max_roi']."',
							 min_withdrawal = '".$var['min_withdrawal']."',
							 repurchase = '".$var['repurchase']."',
							 referral_commission = '".$var['referral_commission']."',
							 ads_packages = '".$var['ads_packages']."'
							 LIMIT 1";
					 $edited = $db->query_db($sql,$print = DEBUG);
					 if($edited) $return_msg = "Settings updated";	
			} 
			
        break;
	}

   

$in_settings = $in->getSettings();

if($system->moduleInstalled('membership'))
{
  $sql = "SELECT * FROM ".PREFIX."_membership_plans ORDER BY id ASC ";
  $res = $db->query_db($sql,$print = DEBUG);
  if ($db->num_rows($res) > 0) while ($row = $db->fetch_db_array($res)) $memberships[$row["id"]] = $row;
}

if($system->moduleInstalled('ads'))
{
  $sql = "SELECT * FROM ".PREFIX."_ads_credit_packages ORDER BY id ASC ";
  $res = $db->query_db($sql,$print = DEBUG);
  if ($db->num_rows($res) > 0) while ($row = $db->fetch_db_array($res)) $adpacks[$row["id"]] = $row;
}


$pros = $payment->processors(null,"USD");



   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $loader->setVar($data);
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'settings.php';
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();
   
 ?>