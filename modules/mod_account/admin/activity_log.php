<?php
global $system,$db,$settings,$admininfo,$countries;

$system->importClass('account')->checkPrivilege(2);

$var = $system->getVar();
	
	
   $sql  = "SELECT COUNT(*) as total FROM ".PREFIX."_account_activity  ";
  
   $sql .= "WHERE id != '' ";
	
   if($var['search'] == 'y')
   {

    
        if(!empty($var['username']))$sql .= "AND username = '".$var['username']."' ";
		
		if(!empty($var['ip']))$sql .= "AND ipaddress = '".$var['ip']."' ";
	  
	    if(!empty($var['time_from']) || !empty($var['time_to']))
	     { 
	    
		    $time1 = explode("-",$var['time_from']);
		    $time_from = $time1[0]."-".$time1[1]."-".$time1[2];
		
		    $time2 = explode("-",$var['time_to']);
		    $time_to = $time2[0]."-".$time2[1]."-".$time2[2];
		
		    if(!checkdate($time2[1],$time2[2],$time2[0]) && checkdate($time1[1],$time1[2],$time1[0])) $sql .= "AND DATE(date) = DATE('".$time_from."')  ";
	   
	        elseif(checkdate($time2[1],$time2[2],$time2[0]) && !checkdate($time1[1],$time1[2],$time1[0])) $sql .= "AND DATE(date) < DATE('".$time_to."')  ";
	   
	        else  $sql .= "AND DATE(date) BETWEEN DATE('".$time_from."') AND DATE('".$time_to."') ";
	 
	     }
     
	  
	 
    }
  
    $sql .= "ORDER BY id DESC";
	
    $result = $db->query_db($sql,$print = DEBUG);
	$num_rows = $db->db_result($result, 0, 'total');
	$page = $var['page'];
    $per_page = (is_numeric($var['per_page']) 
	             && $var['per_page'] <= $settings['system']['rows_per_page'])?
				 $var['per_page']:$settings['system']['rows_per_page'];		 	
	if(!empty($page)){$start = ($page - 1) * $per_page;}
	else { $start 	= 0; $page	= 1; }		
    $sql = str_replace("COUNT(*) as total", " * ",$sql);
	$sql .= " LIMIT $start, $per_page "; 
    $result = $db->query_db($sql,$print = DEBUG);
	 
    if ($db->num_rows($result) > 0)
    {
	 	$rows = $start;
	 	while ($row = $db->fetch_db_array($result))
		{	
			$rows++;
		    $activities[$rows] = $row;
					
		}
     }
 
	
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'activity_log.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();


?>

