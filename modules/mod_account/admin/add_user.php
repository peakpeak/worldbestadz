<?php
global $system,$db,$settings,$validate,$countries;
$system->importClass('account');
Account::checkPrivilege(3);
$var = $system->getVar();

if ($var['add'] == 'y')
{
	
	Validate::trim_post();
	
	$user = $var['username'];
	
	$email = $var['email'];
	
	$question = $var['question'];
	
	$answer = $var['answer'];
	
	$referrer = $var['referrer'];
	
	//Username
	if ($user == '' )
	{
		$errors[] =  "Please add a valid username";
	}
	elseif (Validate::alpha_numeric($user) == false)
	{
		$errors[] =  "Please add a valid username";
	}
	elseif (strlen($user) < $settings['account']['min_username_length'] || strlen($user) > $settings['account']['max_username_length'])
	{
		$errors[] =  "Username must be between ".$settings['account']['min_username_length']." to ".$settings['account']['max_username_length']." characters";
	}
	elseif($db->if_exist_in_db("username",$user,"_account_users"))
	{
		$errors[] =  "Username already taken";
	}
	
	
	//Email 
	if ($email=='')
	{
		$errors[] = "Please add an email";
	}
	elseif(!Validate::email($email))
	{
	   $errors[] = "Please add a valid email";
	}
	elseif($db->if_exist_in_db("email",$email,"_account_users"))
	{
	    $errors[] = "Email already taken";		
	}
	
	//referrer
	if(!empty($var['referrer']) && !Account::isUser($var['referrer']))
	{
	    $errors[] =  "Referrer must be a valid user";
	}
	
	//Password
	if ($var['password'] == '' || strlen($var['password']) <  $settings['account']['min_password_length'])
	{
		$errors[] =  "Please provide valid password of length ".$settings['account']['min_password_length']." or more";
	}
	
    //security question
    if($settings['account']['use_security_question'] == true) 
	{
	   if ($question == "" ) $errors[] =  "Please provide a security question";
	   if ($answer == "" ) $errors[] =  "Please provide an answer ";
	}
	
	if(is_array($errors) && !empty($errors))
	{
		while (list($key,$value) = each($errors)) $return_msg.= $value.'<br>';		
	}
	else
	{
	
		  $activation = ($var['status'] == 'Pending')? $system->randomString(15):'';
		  $table = "_account_users";
		  $date = date("Y-m-d H:i:s");
		  $ip = $system->getRealIP();
		  $insert_array = array(
			
				'username'=> $var['username'],
				'password'=>md5($var['password']),
				'email' => $var['email'],
				'firstname' => $var['firstname'],
				'lastname' => $var['lastname'],
				'gender' => $var['gender'],
				'email' => $var['email'],
				'address1' => $var['address1'],
				'address2' => $var['address2'],
				'city' => $var['city'],
				'state' => $var['state'],
				'country' => $countries[$var['country_code']],
				'country_code' => $var['country_code'],
				'zipcode' => $var['zipcode'],
				//'telephone' => $var['telephone'],
				'question' => $var['question'],
				'answer' => $var['answer'],
				'usertype'=>$var['usertype'],	  	  	  
				'activation'=> $activation,
				'status'=> $var['status'],
				'regdate' => $date,
				'regip' => $ip,
				'lastip' => $ip,
		  
		 );
		 
		 $db->insert_into_db($insert_array,$table);
		 $system->handleEvent('on_register', get_defined_vars());
		 unset($var);
		 $return_msg = "User added";
	
	}
  }



   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'add_user.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();


?>
