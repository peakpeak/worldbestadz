<?php
global $db,$countries,$system;

$system->importClass('account');
Account::checkPrivilege(3);
$var = $system->getVar();


if (!isset($var['uid']) || !Account::isUser($var['uid'])) header('location: index.php');


    if ($var['activate_user'] == 'y' && $var['uid']!='')
	{
     	$user = Account::getUserData($var['uid']);
		if($user['activation']!='' && $user['status']!='Active')
		{
		   $activated = Account::activate($user['username'],$user['activation']);  
		   if($activated[0] == false) $return_msg = $activated[1]; else $return_msg = "Account Activated";
		}
	
	}
   if ($var['send_activation_link'] == 'y' && $var['uid']!='')
	{
     	Account::sendActivationLink($var['uid']);
	    $return_msg = "Activation link sent";
	
	}
	if ($var['update'] == 'y' && $var['uid']!='')
	{
	
			 $update_array = array(
			
				
				'email' => $var['email'],
				'firstname' => $var['firstname'],
				'lastname' => $var['lastname'],
				'gender' => $var['gender'],
				'email' => $var['email'],
				'address1' => $var['address1'],
				'address2' => $var['address2'],
				'city' => $var['city'],
				'state' => $var['state'],
				'country' => $countries[$var['country_code']],
				'country_code' => $var['country_code'],
				'zipcode' => $var['zipcode'],
				//'telephone' => $var['telephone'],
				'question' => $var['question'],
				'answer' => $var['answer'],
				'usertype' => $var['usertype'],
				'status' => $var['status'],
				
			);
		    if(!empty($var['password'])) $update_array['password'] = md5($var['password']);
	        $db->update_db($update_array,"_account_users","username",$var['uid']);
			if(!empty($var['referrer']) && Account::isUser($var['referrer']) && $var['referrer'] != $var['uid'])
			{
				  $update_array = array('referrer'=> $var['referrer']);
				  $db->update_db($update_array,"_account_users","username",$var['uid']);
				  
				  //remove eventually
				  $update_array = array('username'=> $var['referrer']);
				  $db->update_db($update_array,"_promo_referrals","referral",$var['uid']);
				  
			}

	 
	      /*  $db->key_f = "username";
	        $db->table_n = "_account_contact_details";
	        $db->keyid  = $var['uid'];
	        $db->fields = array ('firstname','lastname','gender','email','question','answer','country_code','state','city','zipcode','address1','address2','country');
		
	        array_push($var['values'],$countries[$var['values'][4]]);
			$db->values = $var['values'];
	        $db->up_record();*/

	        $return_msg = "User information updated";
					
	}

  $user = Account::getUserData($var['uid']);
  
  ////////////////////////wallet////////////////////////////////////////////

  $wallet = $system->importClass('wallet');
  
  $payment = $system->importClass('payment');
  
  $processors = $payment->processors(null,"USD");
  
  ////////////////////////hyip///////////////////////////////////////////////
  
   $sql = "SELECT SUM(deposit_amount) as amount FROM ".PREFIX."_instant_deposits ";
   $sql.= "WHERE username = '".$user["username"]."' LIMIT 1 ";
   $res = $db->query_db($sql,$print = DEBUG);
   $user_total_investment = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);
   
   $sql = "SELECT SUM(deposit_amount) as amount FROM ".PREFIX."_instant_deposits ";
   $sql.= "WHERE username = '".$user["username"]."'  ";
   $sql.= "AND status = 'Active'  ";
   $res = $db->query_db($sql,$print = DEBUG);
   $user_active_investment = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);
   
   $sql = "SELECT SUM(deposit_amount) as amount FROM ".PREFIX."_instant_deposits ";
   $sql.= "WHERE username = '".$user["username"]."'  ";
   $sql.= "AND status = 'Expired'  ";
   $res = $db->query_db($sql,$print = DEBUG);
   $user_expired_investment = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);
   
  /* $sql = "SELECT SUM(earned) as amount FROM ".PREFIX."_instant_deposits ";
   $sql.= "WHERE username = '".$user["username"]."' LIMIT 1 ";
   $res = $db->query_db($sql,$print = DEBUG);
   $user_investment_earnings = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);*/
   
   $sql = "SELECT SUM(amount) as amount FROM ".PREFIX."_instant_transactions ";
   $sql.= "WHERE username = '".$user["username"]."' ";
   $sql.= "AND type = 'Commission' LIMIT 1 ";
   $res = $db->query_db($sql,$print = DEBUG);
   $user_commission_earnings = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);
   
   $user_total_earnings =   $user_investment_earnings + $user_commission_earnings;
   
   /*$sql = "SELECT SUM(amount) as amount FROM ".PREFIX."_instant_transactions ";
   $sql.= "WHERE username = '".$user["username"]."' ";
   $sql.= "AND type = 'Principal' LIMIT 1 ";
   $res = $db->query_db($sql,$print = DEBUG);
   $user_returned_principals = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);*/
  
  
   
    if($system->moduleInstalled("Promo"))
	{
	  $promo = $system->importClass('Promo');
	  $user['upline'] = $promo->getUpline($user["username"]); 
	  $user['upline'] = ($user['upline'] == "")?" -- ":"<a href='?mod=account&go=edit_user&uid=".$user['upline']."'>".$user['upline']."</a>";
	  $user['referrals'] = $promo->totalReferrals($user["username"]); 
	  $user['referrals'] = ($user['referrals'] == 0)?" -- ":$user['referrals'];
	}
	
	
	if(!empty($user["regip"]))
	{  
	  $ip_location = $system->ipToLocation($user["regip"]);
	  $user['regip_country'] = ($ip_location['country_name'] != null) ? $ip_location['country_name'] : 'None';
	  $user['regip_region'] = ($ip_location['region_name'] != null) ? $ip_location['region_name'] : 'None';
	  $user['regip_city'] = ($ip_location['city'] != null) ? $ip_location['city'] : 'None';
	} else $user["regip"] = $user['regip_country'] = $user['regip_region'] = $user['regip_city'] = 'None';
	if(!empty($user["lastip"]))
	{  
	  $ip_location = $system->ipToLocation($user["lastip"]);
	  $user['lastip_country'] = ($ip_location['country_name'] != null) ? $ip_location['country_name'] : 'None';
	  $user['lastip_region'] = ($ip_location['region_name'] != null) ? $ip_location['region_name'] : 'None';
	  $user['lastip_city'] = ($ip_location['city'] != null) ? $ip_location['city'] : 'None';
	}else $user["lastip"] = $user['lastip_country'] = $user['lastip_region'] = $user['lastip_city'] = 'None';
  
  

   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'edit_user.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();

?>
