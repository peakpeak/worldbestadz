<?php
global $system,$db,$settings,$admininfo,$countries;

$system->importClass('account')->checkPrivilege(2);

$var = $system->getVar();

    if($var['update'] == 'y')
	{
	
	   for ($i = 0; $i < count ($var['list']); $i++)
		{
		   
			$tid = $var['list'][$i];
			$sql = "SELECT * FROM ".PREFIX."_account_users 
			        WHERE id = '".$tid."'
					AND usertype != 'Super Admin' 
					LIMIT 1 ";
			$res = $db->query_db($sql,$print = DEBUG);
			if ($db->num_rows($res) > 0)
			{ 
			    $row = $db->fetch_db_array($res);
			    switch($var['submit'])
	            {
				   case"Delete":    
				   $deleted = Account::deleteUser($row['username']);	   
				   break;
				   
				   case"Suspend": 
				    	   
			               $sql = "UPDATE ".PREFIX."_account_users SET
                           status = 'Suspended'
                           WHERE id = '".$tid."'
						   AND usertype != 'Super Admin' 
						   LIMIT  1";
				           $db->query_db($sql,$print = DEBUG);
						   
				   break;
				   
				   case"Activate": 
				    	   
			               $sql = "UPDATE ".PREFIX."_account_users SET
                           status = 'Active'
                           WHERE id = '".$tid."' LIMIT  1";
				           $db->query_db($sql,$print = DEBUG);
						   
				   break;
				}
			}	 	 
		}
	}	
	
   $sql  = "SELECT COUNT(*) as total,u.id as id FROM ".PREFIX."_account_users as u ";
   
   //$sql .= "INNER JOIN ".PREFIX."_account_contact_details as cd ON u.username = cd.username ";

   $sql .= "WHERE u.id != '' ";
	
    if($var['search'] == 'y')
    {

		  if(isset($var['status']) && count($var['status'])> 0)
		  { 
			$sql .= "AND ( ";
			foreach($var['status'] as $status) { $sql .= "u.status = '".$status."' "; if($status != end($var['status'])) $sql .= "OR ";}
			$sql .= ") ";
		  }
		
		  if(!empty($var['username']))$sql .= "AND u.username = '".$var['username']."' ";
		  if(!empty($var['email']))$sql .= "AND u.email = '".urldecode($var['email'])."' ";
		  if(!empty($var['country']))$sql .= "AND u.country_code = '".$var['country']."' ";
	  
    }
  
    $sql .= "ORDER BY u.id DESC";
  
    $result = $db->query_db($sql,$print = DEBUG);
	$num_rows = $db->db_result($result, 0, 'total');
	$page = $var['page'];
    $per_page = (is_numeric($var['per_page']) 
	             && $var['per_page'] <= $settings['system']['rows_per_page'])?
				 $var['per_page']:$settings['system']['rows_per_page'];	 	
	if(!empty($page)){$start = ($page - 1) * $per_page;}
	else { $start 	= 0; $page	= 1; }		
    $sql = str_replace("COUNT(*) as total", " * ",$sql);
	$sql .= " LIMIT $start, $per_page "; 
    $result = $db->query_db($sql,$print = DEBUG);
	 
    if ($db->num_rows($result) > 0)
    {
	 	$rows = $start;
	 	while ($row = $db->fetch_db_array($result))
		{	
			$rows++;
		    $members[$rows] = $row;//Account::getUserData($row['username']);
					
		}
     }
 
	
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'users.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();
