<?php
global $system,$db,$settings,$admininfo,$countries;

//$db->query_db('SET OPTION SQL_BIG_SELECTS=1');

$system->importClass('account')->checkPrivilege(2);

$var = $system->getVar();

    if($var['update'] == 'y')
	{
	
	   for ($i = 0; $i < count ($var['list']); $i++)
		{
		   
			$tid = $var['list'][$i];
			$sql = "SELECT id  FROM ".PREFIX."_account_users WHERE id = '".$tid."' ";
			$res = $db->query_db($sql,$print = DEBUG);
			if ($db->num_rows($res) > 0)
			{ 
			    $row = $db->fetch_db_array($res);
			    switch($var['submit'])
	            {
				   case"Delete": 
				    	   
			               $sql = "UPDATE ".PREFIX."_account_users SET
                           status = 'Deleted'
                           WHERE id = '".$tid."' 
						   AND usertype != 'Super Admin' 
						   LIMIT  1";
				           $db->query_db($sql,$print = DEBUG);
						   
				   break;
				   
				   case"Suspend": 
				    	   
			               $sql = "UPDATE ".PREFIX."_account_users SET
                           status = 'Suspended'
                           WHERE id = '".$tid."'
						   AND usertype != 'Super Admin' 
						   LIMIT  1";
				           $db->query_db($sql,$print = DEBUG);
						   
				   break;
				   
				   case"Activate": 
				    	   
			               $sql = "UPDATE ".PREFIX."_account_users SET
                           status = 'Active'
                           WHERE id = '".$tid."' LIMIT  1";
				           $db->query_db($sql,$print = DEBUG);
						   
				   break;
				}
			}	 	 
		}
	}
	

   
   $members = array();
   if($var['search'] == 'y')
   {
	 
	 switch ($var['operator'])
	 {
	   case 'equals' : $operator = "="; break;
	   case 'not_equal' : $operator = "!="; break;
	   case 'less_than' : $operator = "<"; break;
	   case 'greater_than' : $operator = ">"; break;
	   case 'contains' : $operator = "LIKE"; break;
	   case 'not_contain' : $operator = "NOT LIKE"; break;
	   default: break;
	 }
	
	 $field = ($var['field'] == 'username' || $var['field'] == 'email')? "u.".$var['field']: $var['field'];
	 
	 $value = ($operator == 'LIKE' OR $operator == 'NOT LIKE' ) ? "%".urldecode($var['value'])."%":urldecode($var['value']);
	 
     $sql  = "SELECT COUNT(*) as total,u.id as id,u.username as username,  u.status as status,u.usertype as usertype, ".$field." as `field`  FROM ".PREFIX."_account_users as u ";     //$sql .= "INNER JOIN ".PREFIX."_account_contact_details as cd ON u.username = cd.username ";

     $sql .= "WHERE ".$field." $operator '".$value."' ";
	 
	 $sql .= "ORDER BY u.id DESC ";
  
     $result = $db->query_db($sql,$print = DEBUG);
	 $num_rows = $db->db_result($result, 0, 'total');
	 $page = $var['page'];
     $per_page = (is_numeric($var['per_page']) 
	             && $var['per_page'] <= $settings['system']['rows_per_page'])?
				 $var['per_page']:$settings['system']['rows_per_page'];		 	
	 if(!empty($page)){$start = ($page - 1) * $per_page;}
	 else { $start 	= 0; $page	= 1; }		
     $sql = str_replace("COUNT(*) as total,","  ",$sql);
	 $sql .= " LIMIT $start, $per_page "; 
     $result = $db->query_db($sql,$print = DEBUG);
	  
     if ($db->num_rows($result) > 0)
     {
	 	$rows = $start;
	 	while ($row = $db->fetch_db_array($result))
		{	
			$rows++;
		    $members[$rows] = $row;		
		}
     }
 
	
	  
    } elseif($var['duplicate'] == 'y') {
	
	 $field = $var['field'];
	 
     $sql  = "SELECT COUNT(*) as total, `".$field."` as `field` , COUNT(`".$field."`) as num  FROM ".PREFIX."_account_users as u ";
	 //$sql .= "INNER JOIN ".PREFIX."_account_contact_details as cd ON u.username = cd.username ";
	 
	 $sql .= "WHERE `".$field."` != '' ";

     $sql .= "GROUP BY `field` HAVING num > 1 ";
	 
	 $sql .= "ORDER BY u.id DESC ";
	 
	 $result = $db->query_db($sql,$print = DEBUG);
	 $num_rows = $db->db_result($result, 0, 'total');
	 $page = $var['page'];
     $per_page = (is_numeric($var['per_page']) 
	             && $var['per_page'] <= $settings['system']['rows_per_page'])?
				 $var['per_page']:$settings['system']['rows_per_page'];		 	
	 if(!empty($page)){$start = ($page - 1) * $per_page;}
	 else { $start 	= 0; $page	= 1; }		
     $sql = str_replace("COUNT(*) as total,", "  ",$sql);
	 $sql .= " LIMIT $start, $per_page "; 
     $result = $db->query_db($sql,$print = DEBUG);
	 
     if ($db->num_rows($result) > 0)
     {
	 	$rows = $start;
	 	while ($row = $db->fetch_db_array($result))
		{	
			$rows++;
			//$row['field'] = ($field == 'password')? " ** hidden ** ":$row['field'];
		    $members[$rows] = $row;
					
		}
     }
 
	
	  
    
	}
  
    
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'search.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();


?>

