<h1>Activity Log</h1>

<p class="info">Here you can view users activities</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

<div class="box" style="margin-bottom:10px">

<form action="index.php" method="get">
<input  type="hidden" name="mod" value="account" />
<input  type="hidden" name="go" value="activity_log" />
<input  type="hidden" name="search" value="y" />

  <table cellpadding="0" cellspacing="0" class="utility">
    <tr style="background-color:transparent">
      <td style="position:relative">
	  Username: <input type="text" name="username" value="<?php echo $var["username"]?>"  size="15" />
		IP Address: <input type="text" name="ip" value="<?php echo $var["ip"]?>"  size="15" />
		<script>$(function() { jQueryui( "#datepicker_1" ).datepicker({ dateFormat: 'yy-mm-dd' });});</script>
	    <script>$(function() { jQueryui( "#datepicker_2" ).datepicker({ dateFormat: 'yy-mm-dd' });});</script>
	   
	  Date: 
	  <input type="text" name="time_from" value="<?php echo $var["time_from"]?>" size="10"  id="datepicker_1" />&nbsp;-&nbsp;
	  <input type="text" name="time_to" value="<?php echo $var["time_to"]?>" size="10" id="datepicker_2"/>	  
	  Items Per Page: 
	 <select name="per_page">
	 <option value="10" <?php if( $var["per_page"] == '10'){ ?> selected="selected" <?php } ?>>10</option>
     <option value="25" <?php if( $var["per_page"] == '25'){ ?> selected="selected" <?php } ?>>25</option>
     <option value="50" <?php if( $var["per_page"] == '50'){ ?> selected="selected" <?php } ?>>50</option>
     <option value="100"<?php if( $var["per_page"] == '100'){ ?> selected="selected" <?php } ?>>100</option>
     </select>
   
	 <input  class="button-alt-sml" type="button" name="clear"  value="Clear Filters" onclick="window.location.href='?mod=account&go=activity_log'"  />
	 <input  class="button-alt-sml" type="submit" name="submit"  value="Search"  /></td>
	  </td>
       
    </tr>
    
  </table></form>
</div>


<table  cellpadding="0" cellspacing="0"  class="tableS">
 <thead>
  <tr>
    
    <th >No</th>
    <th class="left">Username</th>
    <th class="left">Date</th>
    <th class="left">Description</th>
	<th class="left">IP Address</th>
  </tr>
  </thead>
  
  <?php foreach ($activities as $num => $row){ ?>
  
  <tr>
    
    <td align="center"><?php echo $num; ?></td>
    <td ><?php echo $row['username']; ?></td>
    <td ><?php echo $row['date']; ?></td>
    <td ><?php echo $row['description']; ?></td>
	<td ><?php echo $row['ipaddress']; ?></td>
   
  </tr>
	  
	 
 <?php } ?>


  <?php if(count($activities) == 0) { ?> <tr> <td colspan="9"  align="center">No result found</td> </tr> <?php } ?>
  

   <tr>
	    <td colspan="9" align="center"> <?php echo $system->getPaginationString($page,$num_rows,$per_page,3,$system->curPageURL(),"page");?></td>
   </tr>
	
  
  </table>



 