<h1>Manage Users</h1>

<p class="info">Here you can manage users in the system</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

<h2><span><a href="?mod=account&go=add_user" class="button-alt-sml" >Add User</a></span><br /></h2>

<div class="box" style="margin-bottom:10px">

<form action="index.php" method="get">
<input  type="hidden" name="mod" value="account" />
<input  type="hidden" name="go" value="users" />
<input  type="hidden" name="search" value="y" />

  <table cellpadding="0" cellspacing="0" class="utility">
    <tr style="background-color:transparent">
      <td style="position:relative">
	    Username: <input type="text" name="username" value="<?php echo $var["username"]?>"  />
		Email: <input type="text" name="email" value="<?php echo urldecode($var["email"])?>"  />
		
	  </td>
       <td align="left"><table cellpadding="0" cellspacing="0" border="0"> <tr>
	  <td>Status:</td>
       <td><input type="checkbox" class="html-checkboxes" name="status[]" value="Pending"<?php if(in_array('Pending',$var["status"])){ ?> checked="checked" <?php } ?>/>Pending</td>
       <td><input type="checkbox" class="html-checkboxes" name="status[]" value="Suspended"<?php if(in_array('Suspended',$var["status"])){ ?> checked="checked" <?php } ?>/>Suspended</td>
       <td><input type="checkbox" class="html-checkboxes" name="status[]" value="Active"<?php if(in_array('Active',$var["status"])){ ?> checked="checked" <?php } ?>/>Active</td>
      
</tr></table></td>
    </tr>
    <tr style="background-color:transparent">
     <td  class="border">
	 &nbsp;
	 <!--Country:
	    <select name="country">
	     <?php foreach ($countries as $id => $name) {?>
		 <option value="<?php echo $id?>" <?php if( $var["country"] == $id){ ?> selected="selected" <?php } ?> ><?php echo $name?></option>	
	     <?php } ?>
	    </select>-->
     </td>
     <td align="left" class="border">
	 Items Per Page: 
	 <select name="per_page">
	 <option value="10" <?php if( $var["per_page"] == '10'){ ?> selected="selected" <?php } ?>>10</option>
     <option value="25" <?php if( $var["per_page"] == '25'){ ?> selected="selected" <?php } ?>>25</option>
     <option value="50" <?php if( $var["per_page"] == '50'){ ?> selected="selected" <?php } ?>>50</option>
     <option value="100"<?php if( $var["per_page"] == '100'){ ?> selected="selected" <?php } ?>>100</option>
     </select>
     &nbsp;&nbsp;
    <!-- Go To: 
	 <select class="select" >
	 <option value="1" selected="selected">1</option>
     </select>-->
	 <input  class="button-alt-sml" type="button" name="clear"  value="Clear Filters" onclick="window.location.href='?mod=account&go=users'"  />
	 <input  class="button-alt-sml" type="submit" name="submit"  value="Search"  />
</td>
    </tr>
  </table></form>
</div>


<form   action="" method="post">
<input  type="hidden" name="update" value="y" />
<table  cellpadding="0" cellspacing="0"  class="tableS">
 <thead>
  <tr>
    
    <th >No</th>
    <th class="left">Username</th>
    <th class="left">First Name</th>
    <th class="left">Last Name</th>
	<th class="left">Email</th>
    <!--<th class="left">Country</th>-->
	<th class="left">Status</th>
	<th >Action</th>
    <th >Select</th>
  </tr>
  </thead>
  
  <?php foreach ($members as $num => $row){ ?>
  
   
  <tr>
    
    <td align="center"><?php echo $num; ?></td>
    <td ><?php echo $row['username']; ?></td>
    <td ><?php echo $row['firstname']; ?></td>
    <td ><?php echo $row['lastname']; ?></td>
	<td ><?php echo $row['email']; ?></td>
    <!--<td ><?php //echo $row['country']; ?></td>-->
	<td ><?php echo $row['status']; ?></td>
	<td align="center">
	<a href="?mod=account&go=edit_user&uid=<?php echo $row["username"]?>" ><img src="images/edit.png" class="tooltip img-wrap2"  alt="" title="Edit"/></a> 
	<?php if($row['usertype'] == 'Super Admin') { ?> <img src="images/delete2.png" class="tooltip img-wrap2"  alt="" title="Super Admin"/> <?php } else {?> 
	<!--<a href="?mod=account&go=users&uid=<?php echo $row["username"]?>&delete=y" onclick="return(confirm('Do you really want to delete?'))"><img src="images/delete.png" class="tooltip img-wrap2"  alt="" title="Delete"/></a>-->
	<a href="#"><img src="images/delete.png" class="tooltip img-wrap2"  alt="" title="Delete"/></a> 
	<?php } ?></td>
	<td align="center"><input type="checkbox" name="list[]" value="<? echo $row["id"]?>"></td>
	
  </tr>
	  
	 
 <?php } ?>


  <?php if(count($members) == 0) { ?> <tr> <td colspan="8"  align="center">No users found</td> </tr> <?php } ?>
  

   <tr>
	    <td colspan="8" align="center"> <?php echo $system->getPaginationString($page,$num_rows,$per_page,3,$system->curPageURL(),"page");?></td>
   </tr>
	
  <tr>
    <td colspan="8"  align="center">
      <input type="button" value="Check All" onclick="this.value = check(this.form.checkbox)" />
      <input type="submit" name="submit" value="Delete" id="button" onclick="return(confirm('Do you really want to delete?'))"/>
	  <input type="submit" name="submit" value="Suspend" id="button"/>
	  <input type="submit" name="submit" value="Activate" id="button"/>
   </td>
  </tr>
  </table>
</form>
<script language="javascript">
<!-- Begin

    var checkflag = "false";

    function check(field) {
    var checks = document.getElementsByName('list[]');
    if (checkflag == "false") {
     for (i = 0; i < checks.length; i++){
      checks[i].checked = true;
     }
      checkflag = "true";
      return "Uncheck All";
   }
   else {
     for (i = 0; i < checks.length; i++) {
      checks[i].checked = false; }
      checkflag = "false";
      return "Check All"; 
  }
}
//  End -->
</script>

 