<script type="text/javascript">

      var currenttime = '<? print date("F d, Y H:i:s", time())?>' //PHP method of getting server date
      var montharray = new Array("January","February","March","April","May","June","July","August","September","October","November","December")
      var serverdate = new Date(currenttime)

      function padlength(what)
	  {
           var output = (what.toString().length == 1)? "0"+what : what
           return output
      }

      function displaytime()
	  {
           serverdate.setSeconds(serverdate.getSeconds()+1)
           var datestring=montharray[serverdate.getMonth()]+" "+padlength(serverdate.getDate())+", "+serverdate.getFullYear()
           var timestring=padlength(serverdate.getHours())+":"+padlength(serverdate.getMinutes())+":"+padlength(serverdate.getSeconds())
           document.getElementById("servertime").innerHTML=datestring+" "+timestring
      }

      window.onload=function()
	  {
         setInterval("displaytime()", 1000)
      }

</script>

<table cellpadding="0" cellspacing="0"  class="tableS" >
 <thead>
  <tr>
    <th colspan="2" align="left">General Information </th>
  </tr>
  </thead>
  <tr>
    <td width="30%">Server Time</td>
    <td ><span id="servertime"></span></td>
  </tr>
  <tr>
    <td>Your Current I.P</td>
    <td><?php echo $system->getRealIP();?></td>
  </tr>
 <thead>
  <tr>
    <th colspan="2" align="left">Server Details</th>
  </tr>
  </thead>
  <tr>
    <td>PHP VERSION</td>
    <td><?php echo phpversion(); ?></td>
  </tr>
  <tr>
    <td>MYSQL VERSION</td>
    <td><?php echo mysql_get_client_info();  ?></td>
  </tr>
  <tr>
    <td>SERVER NAME</td>
    <td><?php echo $_SERVER['SERVER_NAME']; ?></td>
  </tr>
</table> <br />

{#widget=account::admin_dashboard} <br />

{#widget=instant::admin_dashboard} <br />
 
