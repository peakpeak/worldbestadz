<h1>Manage Users</h1>

<p class="info">Here you can edit a user's information</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

<form action="" name="edituser" method="post" >
<input type="hidden" name="uid" value="<?php echo $var['uid']; ?>" />
<input type="hidden" name="update" value="y" />
<table  cellpadding="0" cellspacing="0" class="tableS">
 <thead>
  <tr>
    <th colspan="2" class="left"><a href="?mod=account&go=users">Manage Users</a> > <?php echo $var['uid']; ?></th>
  </tr>
  </thead>
  <tr>
    <td width="30%">First Name</td>
    <td><input type="text" name="firstname" value="<?php echo $user['firstname']; ?>"/></td>
  </tr>
  <tr>
    <td>Last Name</td>
    <td><input type="text" name="lastname" value="<?php echo $user['lastname']; ?>"/></td>
  </tr>
  <tr>
    <td>Referrer</td>
    <td><input type="text" name="referrer" value="<?php  echo $user['referrer'] ?>" /></td>
  </tr>
    <tr>
    <td>Gender</td>
    <td><? echo $db->make_select("gender",array('Male'=>'Male','Female'=>'Female'),$user["gender"],"input")?></td>
  </tr>
  <tr>
    <td>Email</td>
    <td><input type="text" name="email" value="<?php  echo $user['email'];?>"/> &nbsp;  <a href="index.php?mod=admin&go=send_email&reciever=<?php echo $user['username']?>" target="_blank">Send email</a></td>
  </tr>
  <tr>
    <td>Password</td>
    <td><input type="password" name="password" value="" /><img src="images/tooltip.png" alt="Tip" class="tooltip" title="Leave blank if you do not want to change password" /></td>
  </tr>
  <tr>
    <td>Security Question</td>
    <td><input type="text" name="question" value="<?php  echo $user['question'];?>"  size="45"/></td>
  </tr>
  <tr>
    <td>Answer</td>
    <td><input type="text" name="answer" value="<?php  echo $user['answer'];?>"  size="45"/></td>
  </tr>
    <tr>
    <td>Country</td>
    <td><? echo $db->make_select("country_code",$countries,$user["country_code"],"input")?></td>
  </tr>
    <tr>
      <td>State</td>
      <td><input type="text" name="state" value="<?php echo $user['state'];?>"/></td>
    </tr>
    <tr>
      <td>City</td>
      <td><input type="text" name="city" value="<?php echo $user['city'];?>"/></td>
    </tr>
    <tr>
      <td>ZIP Code</td>
      <td><input type="text" name="zipcode" value="<?php echo $user['zipcode'];?>"/></td>
    </tr>
    <tr>
      <td>Address 1</td>
      <td><input type="text" name="address1" value="<?php echo $user['address1'];?>" size="45"/></td>
    </tr>
    <tr>
      <td>Address 2</td>
      <td><input type="text" name="address2" value="<?php echo $user['address2'];?>" size="45"/></td>
    </tr>
	<?php if($user['usertype'] != 'Super Admin') { ?>
	<tr>
    <td>User Type</td>
    <td><?php echo  $db->make_select("usertype",array("Basic User"=>"Basic User","Co Admin"=>"Co Admin"),$user['usertype'],"input");  ?> <img src="images/tooltip.png" alt="Tip" class="tooltip" title="Making a user Co-Admin gives them admin access with limited privileges " /></td>
   </tr>
   <tr>
    <td>Status</td>
    <td><?php echo  $db->make_select("status",array("Active"=>"Active","Suspended"=>"Suspended","Pending"=>"Pending"),$user['status'],"input");  ?> &nbsp;  <?php if($user['activation'] != '') { ?> <a href="?mod=account&go=edit_user&edit=y&uid=<?php echo $user['username']?>&send_activation_link=y">Resend Activation email</a> &nbsp; <a href="?mod=account&go=edit_user&edit=y&uid=<?php echo $user['username']?>&activate_user=y">Activate Account</a><?php } ?></td>
  </tr>
   <?php } else { ?> 
   <tr>
    <td>User Type</td>
    <td><input type="hidden" name="usertype" value="<?php echo $user['usertype'];  ?>"/><?php echo $user['usertype'];  ?></td>
   </tr>
   <tr>
    <td>Status</td>
    <td><input type="hidden" name="status" value="<?php echo $user['status'];  ?>"/><?php echo $user['status'];  ?></td>
   </tr>
   <?php } ?>
  <tr>
    <td colspan="2" align="left"><input type="submit" name="Submit" value="update" /></td>
  </tr>
  </table>
  </form>

  <br />
   <table  cellpadding="0" cellspacing="0" class="tableS">
    <thead>
    <tr>
      <th colspan="2" class="left">General  Information</th>
    </tr>
   </thead>
  <tr>
    <td  width="30%">Registration Date</td>
    <td><?php echo $user['regdate'];?></td>
  </tr>
  
  <tr>
    <td>Last Access Date</td>
    <td><?php echo $user['lastvisit'];?> - <a href="?mod=account&go=activity_log&search=y&username=<?php echo $user['username'];?>">Activity Log</a><?php if($user['usertype'] != 'Super Admin' && $user['username'] != $admininfo['username']) { ?> | <a href="?mod=account&go=stealth&uid=<?php echo $user['username'];?>" onclick="return(confirm('You are about to login in stealth mode. You will be logged out of admin account. Do you want to proceed?'))" >Access account</a><?php } ?></td>
  </tr>
   <tr>
    <td>Registration IP</td>
    <td><?php echo $user['regip'];?> - <?php echo $user['regip_country'];?>,<?php echo $user['regip_region'];?>,<?php echo $user['regip_city'];?></td>
  </tr>
  <tr>
    <td>Last Access IP</td>
    <td><?php echo $user['lastip'];?> - <?php echo $user['lastip_country'];?>,<?php echo $user['lastip_region'];?>,<?php echo $user['lastip_city'];?></td>
  </tr>
  <?php if($system->moduleInstalled("Promo")) { ?>
  <tr>
    <td  width="30%">Upline</td>
    <td><?php echo $user['upline'];?></td>
  </tr>
  <tr>
    <td  width="30%">Referrals</td>
    <td><?php echo $user['referrals'];?> - <a href='?mod=promo&go=referral_tree&username=<?php echo $user['username']?>&submit=Search'>See All</a></td>
  </tr>
  <?php } ?>
  </table>
  
  <br />
  
  
   <table  cellpadding="0" cellspacing="0" class="tableS">
   <thead>
    <tr>
       <th colspan="2" class="left" >Deposits</th>
    </tr>
  </thead>
   <tr>
    <td width="30%">Active Deposits</td>
    <td>$<?php echo $user_active_investment;?></td>
  </tr>
   <tr>
    <td>Expired Deposits</td>
    <td>$<?php echo $user_expired_investment;?></td>
  </tr>
  <tr>
    <td ><b>Total Deposits</b></td>
    <td><b>$<?php echo $user_total_investment;?></b> - <a href="?mod=instant&go=deposits&username=<?php echo $user["username"]; ?>&submit=Search">See all</a></td>
  </tr>
 <!-- <tr>
    <td >Investment Earnings</td>
    <td>$<?php //echo $user_investment_earnings;?></td>
  </tr>-->
  <!-- <tr>
    <td >Returned Principals</td>
    <td>$<?php //echo $user_returned_principals;?></td>
  </tr>-->
  <tr>
    <td >Commission Earnings</td>
    <td>$<?php echo $user_commission_earnings;?></td>
  </tr>
 <!-- <tr>
    <td ><b>Total Earnings</b></td>
    <td><b>$<?php echo $user_total_earnings;?></b> - <a href="?mod=hyip&go=transactions&username=<?php echo $user["username"]; ?>&submit=Search">See all</a></td>
  </tr>-->
</table>

<br />

   <table  cellpadding="0" cellspacing="0" class="tableS">
   <thead>
    <tr>
       <th colspan="2" class="left" >Wallet Balance</th>
    </tr>
   </thead>
  <?php foreach($processors as $id => $name){ ?>
	<tr>
		<td width="30%"><?php echo $name;?> Balance</td>
		<td>$<?php echo $wallet->userBal($user['username'],$id,"USD")?></td>
	</tr>
	<?php }  ?>
	 <tr>
		<td ><b>Total Balance</b></td>
		<td><b>$<?php echo $wallet->userBal($user['username'])?></b> - <a href="?mod=wallet&go=manager&username=<?php echo $user["username"]; ?>">Manage</a> - <a href="?mod=wallet&go=transactions&username=<?php echo $user["username"]; ?>&submit=Search">History</a></td>
	</tr>
	
	</table>
  