<h1>Advanced Search</h1>

<p class="info">Here you can search users in the system</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

<h2><span><a href="?mod=account&go=add_user" class="button-alt-sml" >Add User</a></span><br /></h2>

<div class="box" style="margin-bottom:10px">




  <table cellpadding="0" cellspacing="0" class="utility">
    <tr style="background-color:transparent">
      <td style="position:relative" colspan="2">
	  <form action="index.php" method="get">
      <input  type="hidden" name="mod" value="account" />
      <input  type="hidden" name="go" value="search" />
      <input  type="hidden" name="search" value="y" />
	   Search Users Where:
	    <select name="field">
	     <option value="username" <?php if( $var["field"] == 'username'){ ?> selected="selected" <?php } ?> >Username</option>	
		 <option value="email" <?php if( $var["field"] == 'email'){ ?> selected="selected" <?php } ?> >Email</option>
		 <option value="firstname" <?php if( $var["field"] == 'firstname'){ ?> selected="selected" <?php } ?> >First Name</option>	
		 <option value="lastname" <?php if( $var["field"] == 'lastname'){ ?> selected="selected" <?php } ?> >Last Name</option>
		 <option value="address1" <?php if( $var["field"] == 'address1'){ ?> selected="selected" <?php } ?> >Address 1</option>
		 <option value="address2" <?php if( $var["field"] == 'address2'){ ?> selected="selected" <?php } ?> >Address 2</option>
		 <option value="city" <?php if( $var["field"] == 'city'){ ?> selected="selected" <?php } ?> >City</option>	
		 <option value="state" <?php if( $var["field"] == 'state'){ ?> selected="selected" <?php } ?> >State</option>	
		 <option value="country" <?php if( $var["field"] == 'country'){ ?> selected="selected" <?php } ?> >Country</option>
		 <option value="zipcode" <?php if( $var["field"] == 'zipcode'){ ?> selected="selected" <?php } ?> >Zip Code</option>	
		 <option value="telephone" <?php if( $var["field"] == 'telephone'){ ?> selected="selected" <?php } ?> >Telephone</option>
		 <option value="regip" <?php if( $var["field"] == 'regip'){ ?> selected="selected" <?php } ?> >Signup IP</option>
		 <option value="lastip" <?php if( $var["field"] == 'lastip'){ ?> selected="selected" <?php } ?> >Last Access IP</option>		
	    </select>
		
		<select name="operator">
		 <option value="equals"  <?php if( $var["operator"] == 'equals'){ ?> selected="selected" <?php } ?>>Is</option>
		 <option value="less_than" <?php if( $var["operator"] == 'less_than'){ ?> selected="selected" <?php } ?>>Is less than</option>
		 <option value="greater_than"<?php if( $var["operator"] == 'greater_than'){ ?> selected="selected" <?php } ?> >Is greater than</option>
		 <option value="not_equal" <?php if( $var["operator"] == 'not_equal'){ ?> selected="selected" <?php } ?>>Is not</option>
		 <option value="contains" <?php if( $var["operator"] == 'contains'){ ?> selected="selected" <?php } ?>>Contains</option>
		 <option value="not_contain" <?php if( $var["operator"] == 'not_contain'){ ?> selected="selected" <?php } ?>>Does not contain</option>
	    </select>
	    <input type="text" name="value" value="<?php echo urldecode($var["value"])?>"  />
    
		 Items Per Page: 
	   <select name="per_page">
	     <option value="10" <?php if( $var["per_page"] == '10'){ ?> selected="selected" <?php } ?>>10</option>
         <option value="25" <?php if( $var["per_page"] == '25'){ ?> selected="selected" <?php } ?>>25</option>
         <option value="50" <?php if( $var["per_page"] == '50'){ ?> selected="selected" <?php } ?>>50</option>
         <option value="100"<?php if( $var["per_page"] == '100'){ ?> selected="selected" <?php } ?>>100</option>
        </select>
     &nbsp;&nbsp;
   
	    <input  class="button-alt-sml" type="button" name="clear"  value="Clear Filters" onclick="window.location.href='?mod=account&go=search'"  />
	    <input  class="button-alt-sml" type="submit" name="submit"  value="Search"  />
		</form>
	  </td>
    </tr>
    <tr style="background-color:transparent">
      <td style="position:relative">
	  <form action="index.php" method="get">
        <input  type="hidden" name="mod" value="account" />
        <input  type="hidden" name="go" value="search" />
        <input  type="hidden" name="duplicate" value="y" />
	    Search Users With:
	    <select name="field">
		 <option value="firstname" <?php if( $var["field"] == 'firstname'){ ?> selected="selected" <?php } ?> >Same First Name</option>	
		 <option value="lastname" <?php if( $var["field"] == 'lastname'){ ?> selected="selected" <?php } ?> >Same Last Name</option>
		 <option value="address1" <?php if( $var["field"] == 'address1'){ ?> selected="selected" <?php } ?> >Same Address 1</option>
		 <option value="address2" <?php if( $var["field"] == 'address2'){ ?> selected="selected" <?php } ?> >Same Address 2</option>
		 <option value="city" <?php if( $var["field"] == 'city'){ ?> selected="selected" <?php } ?> >Same City</option>	
		 <option value="state" <?php if( $var["field"] == 'state'){ ?> selected="selected" <?php } ?> >Same State</option>	
		 <option value="country" <?php if( $var["field"] == 'country'){ ?> selected="selected" <?php } ?> >Same Country</option>
		 <option value="zipcode" <?php if( $var["field"] == 'zipcode'){ ?> selected="selected" <?php } ?> >Same Zip Code</option>	
		 <option value="telephone" <?php if( $var["field"] == 'telephone'){ ?> selected="selected" <?php } ?> >Same Telephone</option>	
		 <option value="regip" <?php if( $var["field"] == 'regip'){ ?> selected="selected" <?php } ?> >Same Signup IP</option>
		 <option value="lastip" <?php if( $var["field"] == 'lastip'){ ?> selected="selected" <?php } ?> >Same Last Access IP</option>
		<!-- <option value="password" <?php if( $var["field"] == 'password'){ ?> selected="selected" <?php } ?> >Same Password</option>-->
	    </select>
		
		
	  Items Per Page: 
	   <select name="per_page">
	     <option value="10" <?php if( $var["per_page"] == '10'){ ?> selected="selected" <?php } ?>>10</option>
         <option value="25" <?php if( $var["per_page"] == '25'){ ?> selected="selected" <?php } ?>>25</option>
         <option value="50" <?php if( $var["per_page"] == '50'){ ?> selected="selected" <?php } ?>>50</option>
         <option value="100"<?php if( $var["per_page"] == '100'){ ?> selected="selected" <?php } ?>>100</option>
        </select>
     &nbsp;&nbsp;
   
	    <input  class="button-alt-sml" type="button" name="clear"  value="Clear Filters" onclick="window.location.href='?mod=account&go=search'"  />
	    <input  class="button-alt-sml" type="submit" name="submit"  value="Search"  />
		</form>
	  </td>
    </tr>
  </table>
</div>

<?php if($var['search'] == 'y'){ ?>

<form   action="" method="post">
<input  type="hidden" name="update" value="y" />
<table  cellpadding="0" cellspacing="0"  class="tableS">
 <thead>
  <tr>
    
    <th >No</th>
    <th class="left">Username</th>
    <th class="left">Search Results</th>
	<th class="left">User Status</th>
	<th >Action</th>
  </tr>
  </thead>
  
  <?php foreach ($members as $num => $row){ ?>
  
   
  <tr>
    
    <td align="center"><?php echo $num; ?></td>
    <td ><?php echo $row['username']; ?></td>
	<td ><?php echo $row['field']; ?></td>
	<td ><?php echo $row['status']; ?></td>
	<td align="center" width="10%"><a href="?mod=account&go=edit_user&uid=<?php echo $row["username"]?>" ><img src="images/edit.png" class="tooltip img-wrap2"  alt="" title="Edit"/></a> <?php if($row['usertype'] == 'Super Admin') { ?> <img src="images/delete2.png" class="tooltip img-wrap2"  alt="" title="Super Admin"/> <?php } else {?> <a href="?mod=account&go=search&uid=<?php echo $row["username"]?>&delete=y" onclick="return(confirm('Do you really want to delete?'))"><img src="images/delete.png" class="tooltip img-wrap2"  alt="" title="Delete"/></a> <?php } ?><input type="checkbox" name="list[]" value="<? echo $row["id"]?>"></td>
	
	
  </tr>
	  
	 
 <?php } ?>


  <?php if(count($members) == 0) { ?> <tr> <td colspan="4"  align="center">No users found</td> </tr> <?php } ?>
  

   <tr>
	    <td colspan="5" align="center"> <?php echo $system->getPaginationString($page,$num_rows,$per_page,3,$system->curPageURL(),"page");?></td>
   </tr>
	
  <tr>
    <td colspan="5"  align="center">
      <input type="button" value="Check All" onclick="this.value = check(this.form.checkbox)" />
      <input type="submit" name="submit" value="Delete" id="button"/>
	  <input type="submit" name="submit" value="Suspend" id="button"/>
	  <input type="submit" name="submit" value="Activate" id="button"/>
   </td>
  </tr>
  </table>
</form>

<?php } elseif($var['duplicate'] == 'y'){ ?>



<form   action="" method="post">
<input  type="hidden" name="update" value="y" />
<table  cellpadding="0" cellspacing="0"  class="tableS">
 <thead>
  <tr>
    
    <th >No</th>
    <th class="left">Result</th>
    <th class="left">Duplicate Accounts</th>
	
  </tr>
  </thead>
  
  <?php foreach ($members as $num => $row){ ?>
  
   
  <tr>
    
    <td align="center"><?php echo $num; ?></td>
    <td ><?php echo $row['field']; ?></td>
	<td ><?php echo $row['num']; ?> <a href="?mod=account&go=search&search=y&field=<?php echo $var['field'] ?>&operator=equals&value=<?php echo $row['field']; ?>">View All</a></td>
	
  </tr>
	  
	 
 <?php } ?>


  <?php if(count($members) == 0) { ?> <tr> <td colspan="3"  align="center">No users found</td> </tr> <?php } ?>
  

   <tr>
	    <td colspan="3" align="center"> <?php echo $system->getPaginationString($page,$num_rows,$per_page,3,$system->curPageURL(),"page");?></td>
   </tr>
  </table>
</form>

<?php } ?>

<script language="javascript">
<!-- Begin

    var checkflag = "false";

    function check(field) {
    var checks = document.getElementsByName('list[]');
    if (checkflag == "false") {
     for (i = 0; i < checks.length; i++){
      checks[i].checked = true;
     }
      checkflag = "true";
      return "Uncheck All";
   }
   else {
     for (i = 0; i < checks.length; i++) {
      checks[i].checked = false; }
      checkflag = "false";
      return "Check All"; 
  }
}
//  End -->
</script>

 