<h1>Manage Users</h1>

<p class="info">Here you can add a new user</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

<form action="" method="post">
<input type="hidden" name="add" value="y" />

<table cellpadding="0" cellspacing="0" class="tableS">
 <thead>
  <tr>
    <th colspan="2" class="left"><a href="?mod=account&go=users">Manage Users</a> > Add User </th>
  </tr>
  </thead>
   <tr>
	  <td width="30%">Username</td>
	  <td><input type="text" name="username" value="<?php  echo $var['username'] ?>" /></td>
	 </tr>
  <tr>
    <td>First Name</td>
    <td><input type="text" name="firstname" value="<?php  echo $var['firstname'] ?>" /></td>
  </tr>
  <tr>
    <td>Last Name</td>
    <td><input type="text" name="lastname" value="<?php  echo $var['lastname'] ?>" /></td>
  </tr>
  <tr>
    <td>Referrer</td>
    <td><input type="text" name="referrer" value="<?php  echo $var['referrer'] ?>" /></td>
  </tr>
   <tr>
    <td>Gender</td>
    <td><? echo $db->make_select("gender",array('Male'=>'Male','Female'=>'Female'),$var['gender'],"input")?></td>
  </tr>
  <tr>
    <td>Email</td>
    <td><input type="text" name="email" value="<?php  echo $var['email'] ?>" /> </td>
  </tr>
  <tr>
    <td>Password</td>
    <td><input type="password" name="password" value="<?php  echo $var['password'] ?>" /></td>
  </tr>
   <tr>
    <td>Country</td>
    <td><? echo $db->make_select("country_code",$countries,$var['country_code'],"input")?></td>
  </tr>
    <tr>
      <td>State</td>
      <td><input type="text" name="state" value="<?php  echo $var['state'] ?>" /> </td>
    </tr>
    <tr>
      <td>City</td>
      <td><input type="text" name="city" value="<?php  echo $var['city'] ?>" /></td>
    </tr>
    <tr>
      <td>ZIP Code</td>
      <td><input type="text" name="zipcode" value="<?php  echo $var['zipcode'] ?>" /></td>
    </tr>
    <tr>
      <td>Address 1</td>
      <td><input type="text" name="address1" value="<?php  echo $var['address1'] ?>" /></td>
    </tr>
    <tr>
      <td>Address 2</td>
      <td><input type="text" name="address2" value="<?php  echo $var['address2'] ?>" /></td>
    </tr>
	<tr>
      <td>Security Question</td>
      <td><input type="text" name="question" value="<?php  echo $var['question'] ?>" /></td>
    </tr>
    <tr>
      <td>Answer</td>
      <td><input type="text" name="answer" value="<?php  echo $var['answer'] ?>" /></td>
    </tr>
	<tr>
    <td>User Type</td>
    <td><?php echo  $db->make_select("usertype",array("Basic User"=>"Basic User","Co Admin"=>"Co Admin"),$var['usertype'],"input");  ?></td>
  </tr>
 <tr>
    <td>Status</td>
    <td><?php echo  $db->make_select("status",array("Active"=>"Active","Suspended"=>"Suspended","Pending"=>"Pending"),$var['status'],"input");  ?></td>
  </tr>

  <tr>
    <td colspan="2" class="left"><input type="submit" name="Add" value="Add" /></td>
    </tr>
</table>
</form>
