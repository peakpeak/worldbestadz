<?php
   global $system,$db,$settings,$plugins;
   
   $system->importClass('account');	
  
   $var = $system->getVar();

   if($var['send_message'])
   {

      Validate::trim_post();
	  
	  switch($var['send_message']) {
	  
	  case('Send'):
	  
	  if($settings['account']['use_captcha'] == true)  if($system->validateCaptcha($var["captcha"]) == false) $errors[] = "Security code deos not match";
	  
	  if($var['from_email']=='') $errors[] =  "Please add a valid email";
	   
	  if($var['message']=='') $errors[] =  "Message field cannot be empty";
	  
	  if(is_array($errors) && !empty($errors)) while (list($key,$value) = each($errors)) $error_msg.= $value.'<br>';	 
	  else 
	  { 
		   $from_name = ($var['from_name']=='')? "Anonymous" : $var['from_name'];
		   $from_email = $var['from_email'];
		   $subject = "Message via ".$settings['system']['websitename']." contact form";
		   $content = $var['message'];
		   
		   $to_name = 'Administrator';
		   $to_email = $settings['system']['admin_email'];
	
		  
		  if(!DEMO) $system->send_email(array($to_email,$to_name),array($from_email,$from_name),$subject,$content ,$settings['system']['websiteurl'],true);
		  if(DEBUG) echo $subject."\n\n".$content; 
		  $success_msg = "Message Sent";
	  }
	  break;
	 }
   }
 
   $pageInfo['title'] = "Customer Support";
   
   $pageInfo['meta'] = array(
                       'description'=> "Questions and help",
		               'keywords'=>'customer support, help, faq, contact us, live chat, mobile support,'.$settings['system']['default_meta_keywords'], ); 
   $pageInfo['map'] = array('Support'=>SITEURL.FS.'account/support');
   
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'support.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader();
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter();
   
   $loader->displayOutput();

?>