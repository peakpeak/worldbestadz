<?php
global $system,$db,$settings;

$system->importClass('account');	

$var = $system->getVar();

if($var['submit']) 
{
  
      
   if ($var['email']=='' )
	{
		$error_msg =  "Please provide your email";
	}
	else
	{	   
	      $sent = Account::sendResetLink($var['email']);
	      if ($sent) $success_msg =  "Email has been sent to the email provided";
	      else $error_msg =  "Email was  not found";	   
	}
}

  
   $pageInfo['title']='Reset Password';
   
   $pageInfo['meta'] = array(
                       'description'=> $settings['system']['default_meta_description'],
		               'keywords'=>$settings['system']['default_meta_keywords']); 
   $pageInfo['map'] = array('Reset Password' => SITEURL.FS.'account/reset');
					  
   $loader = new Loader;
  
   $data= get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'reset.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader();
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter();
   
   $loader->displayOutput();
   
?>