<?php
global $settings;
$settings['account']['image_extention'] = 'jpg';
$settings['account']['image_directory'] = ROOT.DS."images".DS."profiles".DS;
$settings['account']['default_image'] = 'default';
$settings['account']['contact_info_options'] = array('Required' => "Required",'Passive' => "Passive",'Remove' => "Remove from form");