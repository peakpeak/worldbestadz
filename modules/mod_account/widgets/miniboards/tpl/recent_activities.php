<table border="0" cellpadding="0" cellspacing="0" width="100%">
 <tr>
	            <th>#</th>
				<th>Date</th>
				<th>Event</th>
				<th>IP</th>
				
 </tr>

 <?php  foreach ($activities as $num => $activity) : ?>
 
<tr>

	    <td><a href="#"><strong>#<?php echo $num; ?></strong></a></td>
		<td><?php echo $activity['date']; ?></td>
		<td><?php echo $activity['description']; ?></td>
		<td><?php echo $activity['ipaddress']; ?></td>
	   
      	 
</tr>

 <?php endforeach; ?>
 <?php if(count($activities) == 0) : ?>

<tr class="">
	<td colspan="4" align="center"> No Result Found </td>
 </tr>

 <?php endif; ?> 
	 
<?php if($num_rows > $per_page) : ?>

  <tr>
	<td colspan="4"><?php echo $system->getPaginationString($page,$num_rows,$per_page,3,$system->curPageURL(),"page");?></td>
  </tr>
	 
 <?php endif; ?> 

</table>