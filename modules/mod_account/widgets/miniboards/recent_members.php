<?php 
global $system,$db,$userinfo,$settings;	

$pagetitle = 'Recent Members';

$system->importClass('account');

$var = $system->getVar();

  $members = array();
 
  $per_page = 28;
  
  $sql  = "SELECT * FROM ".PREFIX."_account_users ";
  $sql .= "ORDER BY regdate DESC ";		
  $sql .= "LIMIT $per_page "; 
  $result = $db->query_db($sql,$print = DEBUG);
	
	
	 if ($db->num_rows($result) > 0)
	 {
	 	$rows = $start;
	 	while ($row = $db->fetch_db_array($result))
		{	
			  $rows++;		
		      $members[$rows] = $row;		
			  $members[$rows]['thumbnail'] = Account::userImageSmall($row['username']);				
		}
	  }

   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'recent_members.php';
   
   $loader->setVar($data);
   
   $loader->loadOutput($tpl_file);
   
   $loader->displayOutput();
?>
