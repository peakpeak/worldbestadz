<?php
   global $system,$userinfo,$db,$settings;
   $system->importPlugin("openflash");
  
   
   $sql = "SELECT COUNT(id) from ".PREFIX."_account_users ";
   $res = $db->query_db($sql,$print = DEBUG);
   $n_total_users = $db->db_result($res,0,'count(id)');
   $db->free_result($res);
   
   $sql = "SELECT COUNT(id) from ".PREFIX."_account_users ";
   $sq.= "WHERE status = 'Active' ";
   $res = $db->query_db($sql,$print = DEBUG);
   $n_active_users = $db->db_result($res,0,'count(id)');
   $d_active_users = ($n_active_users/$n_total_users)*100;
   $db->free_result($res);
   
   $sql = "SELECT COUNT(id) from ".PREFIX."_account_users ";
   $sql.= "WHERE status = 'Pending' ";
   $res = $db->query_db($sql,$print = DEBUG);
   $n_pending_users = $db->db_result($res,0,'count(id)');
   $d_pending_users = ($n_pending_users/$n_total_users)*100;
   $db->free_result($res);
   
   
   $sql = "SELECT COUNT(id) from ".PREFIX."_account_users ";
   $sql.= "WHERE status = 'Suspended' ";
   $res = $db->query_db($sql,$print = DEBUG);
   $n_banned_users = $db->db_result($res,0,'count(id)');
   $d_banned_users = ($n_banned_users/$n_total_users)*100;
   $db->free_result($res);
   
	
   $data_1 = array();
   $colors_1 = array();
   $data_1[] = new pie_value($d_active_users,"Active Users");$colors_1[] = "#77CC6D";
   $data_1[] = new pie_value($d_pending_users,"Pending Users");$colors_1[] = "#FF5973";
   $data_1[] = new pie_value($d_banned_users,"Banned Users");$colors_1[] = "#6D86CC";
   
   $pie_1 = new pie();
   $pie_1->set_animate( true );
   $pie_1->add_animation( new pie_fade() );
   $pie_1->set_label_colour( '#432BAF' );
   $pie_1->set_alpha( 0.75 );
   $pie_1->radius(75);
   
   $pie_1->set_tooltip( '#label#(#percent#)' );
   //$pie->set_no_labels();
   $pie_1->set_values($data_1);
   $pie_1->set_colours($colors_1);

 
   //creating the chart
   $chart_1 = new open_flash_chart();
   $chart_1->set_title( new title('User Statistics') );
   $chart_1->set_bg_colour('#F5F5F5');
   $chart_1->add_element($pie_1);
   $user_statistics = $chart_1->toPrettyString();
   
   
  
    //get data
   $stats = Account::getSignupStatistics(10);
   $data_x_axis = $stats[0];
   $data_y_axis = $stats[1];


   $x_range = 10;
   $x_data_count = count($data_x_axis);
   if($x_data_count < $x_range){$x_data_count = $x_range; $x_step = 1;}
   else{$r = $x_data_count % $x_range;
     if ($r != 0)$x_data_count = $x_data_count + $x_range - $r; 
     $x_step = intval($x_data_count/$x_range);	
   }

   $y_range = 10;
   $y_data_count = max($data_y_axis);
   if($y_data_count < $y_range){$y_data_count = $y_range; $y_step = 1;}
   else{$r = $y_data_count % $y_range;
     if ($r != 0)$y_data_count = $y_data_count + $y_range - $r; 
     $y_step = intval($y_data_count/$y_range);	
   }


   //y axis
   $y = new y_axis(); 
   $y->set_range( 0, $y_data_count, $y_step );

   //add x labels
   $x_labels = new x_axis_labels();
   $x_labels->set_steps($x_step);
   $x_labels->set_vertical();
  // $x_labels->rotate(30);
   $x_labels->set_labels( $data_x_axis );

   //x axis
   $x = new x_axis();
   $x->set_steps($x_step);
   $x->set_labels( $x_labels );


   //create dot for lines
   $dot = new hollow_dot();
   $dot->size(5)->halo_size(0)->colour('#3D5C56')->tooltip( '#val# signups on #x_label#' );

   //creating legend
   $x_legend = new x_legend( 'Days' );
   $x_legend->set_style( '{font-size: 12px; color: #000000}' );

   $y_legend = new y_legend( 'Signups' );
   $y_legend->set_style( '{font-size: 12px; color: #000000}' );


   //creating lines
   $line_1 = new line();
   $line_1->set_width( 2 );
   $line_1->set_default_dot_style($dot);
   $line_1->on_show(new line_on_show('pop-up',0.5,1));
   $line_1->set_values( $data_y_axis );
   $line_1->set_key( 'Signups', 10 );

   //creating the chart
   $chart_2 = new open_flash_chart();
   $chart_2->set_title( new title("10 days signup statistics") );
   $chart_2->set_bg_colour('#F5F5F5');
   $chart_2->set_x_legend( $x_legend );
   $chart_2->set_y_legend( $y_legend );
   $chart_2->add_element($line_1);
   $chart_2->set_y_axis( $y ); 
   $chart_2->set_x_axis( $x );
   
   
   $element_1 = 'account_pie_1';
   $element_1_data = $chart_1->toPrettyString();
   
   $element_2 = 'account_line_1';
   $element_2_data = $chart_2->toPrettyString();
   
   $w = 300;
   $h = 300;
   $graph ='
   
     <script type="text/javascript" src="'.SITEURL.FS.$settings['system']['plugins_directory'].'/openflash/js/json/json2.js"></script>
	 <script type="text/javascript" src="'.SITEURL.FS.$settings['system']['plugins_directory'].'/openflash/js/swfobject.js"></script>
     <script type="text/javascript">
	';
	
   $graph.=' 
	 swfobject.embedSWF("'.SITEURL.FS.$settings['system']['plugins_directory'].'/openflash/open-flash-chart.swf",
	 "'.$element_1.'",
	 "'.$w.'","'.$h.'",
	 "9.0.0",
	 "'.SITEURL.FS.$settings['system']['plugins_directory'].'/openflash/expressInstall.swf",
	 {"get-data":"get_'.$element_1.'_data"}
	 );';
	 
   $graph.=' 
	 swfobject.embedSWF("'.SITEURL.FS.$settings['system']['plugins_directory'].'/openflash/open-flash-chart.swf",
	 "'.$element_2.'",
	 "'.$w.'","'.$h.'",
	 "9.0.0",
	 "'.SITEURL.FS.$settings['system']['plugins_directory'].'/openflash/expressInstall.swf",
	 {"get-data":"get_'.$element_2.'_data"}
	 );';
	 
  
   $graph.='</script>';
   $graph.='<script type="text/javascript">
     function ofc_ready()
     {
       // alert(\'ofc_ready\');
     }';
  
   $graph.= 'function get_'.$element_1.'_data()
     {
	   return JSON.stringify('.$element_1.'_data);
     }'; 
   
   $graph.= 'function get_'.$element_2.'_data()
     {
	   return JSON.stringify('.$element_2.'_data);
     }'; 
   
   $graph.='var '.$element_1.'_data = '.$element_1_data.';';
   $graph.='var '.$element_2.'_data = '.$element_2_data.';';
   $graph.='</script>';
   $graph.='<div id="'.$element_1.'"></div>';
   $graph.='<div id="'.$element_2.'"></div>';
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'admin_dashboard.php';
   
   $loader->setVar($data);
   
   $loader->loadOutput($tpl_file);
   
   $loader->displayOutput();
?>