<?php

global $system, $db, $settings, $plugins, $countries;

$system->importClass('account');

$default_redirect = SITEURL . FS . 'members' . FS . 'account';

if (Account::userLoggedIn())$system->redirect($default_redirect);
  
Validate::trim_post();

$var = $system->getVar();

$pageInfo['title'] = 'Register';
$pageInfo['meta'] = array(
    'description' => 'Create an Account',
    'keywords' => 'register, signup now, create account now,' . $settings['system']['default_meta_keywords'],);
$pageInfo['map'] = array('Register' => SITEURL.FS.'account/register',);

if ($var['submit']) {

    
    $ip = $system->getRealIP();

    $disallowed_ips = explode(",", $settings['account']['disallowed_ip_adresses']);

    $disallowed_usernames = explode(",", $settings['account']['disallowed_usernames']);

    $disallowed_emails_domains = explode(",", $settings['account']['disallowed_email_domains']);

    if (!$settings['account']['allow_registration']) {
        $errors[] = "Registration is not allowed at this time";
    }

    //restrictions
    if ($settings['account']['disallow_duplicate_ip'] && $db->if_exist_in_db("regip", $ip, "_account_users")) {
        $errors[] = "You cannot create more than one account from the same IP address";
    }

    if (in_array($ip, $disallowed_ips)) {
        $errors[] = "You cannot create an account from this IP address";
    }

    //bio
    if ($var['fname'] == '') {
        $errors[] = "Please add a firstname";
    }


    if ($var['lname'] == '') {
        $errors[] = "Please add a lastname";
    }
    if ($var['gender'] == '') {
        //$errors[] =  "Please choose your gender";
    }

    //email
    if ($var['email'] == '') {
        $errors[] = "Please add an email";
    } elseif (!Validate::email($var['email'])) {
        $errors[] = "Please add a valid email";
    } elseif ($db->if_exist_in_db("email", $var['email'], "_account_users")) {
        $errors[] = "Email already taken";
    } elseif (in_array(end(explode('@', $var['email'])), $disallowed_emails_domains)) {
        $errors[] = "This Email domain is not allowed";
    }


    //username
    if ($var['username'] == '') {
        $errors[] = "Please add a valid username";
    } elseif (!Validate::alpha_numeric($var['username'])) {
        $errors[] = "Please add a valid username";
    } elseif (strlen($var['username']) < $settings['account']['min_username_length'] || strlen($var['username']) > $settings['account']['max_username_length']) {
        $errors[] = "Username must be between " . $settings['account']['min_username_length'] . " to " . $settings['account']['max_username_length'] . " characters";
    } elseif ($db->if_exist_in_db("username", $var['username'], "_account_users")) {
        $errors[] = "Username already taken";
    } elseif (in_array($var['username'], $disallowed_usernames)) {
        $errors[] = "Username already taken";
    }

    //password	
    if ($var['password'] == '') {
        $errors[] = "Please provide password";
    } elseif (strlen($var['password']) < $settings['account']['min_password_length']) {
        $errors[] = "Password too short";
    } elseif ($var['password'] != $var['password2']) {
        $errors[] = "Passwords do not match";
    }
    
	//captcha
    if ($settings['account']['use_captcha'] == true) {

        if ($system->validateCaptcha($var["captcha"]) == false) {

            $errors[] = "Security code deos not match";
        }
    }
    //security question
    if ($settings['account']['use_security_question'] == true) {

        if ($var['question'] == "") {
            $errors[] = "Please provide a security question";
        }

        if ($var['answer'] == "") {
            $errors[] = "Please provide an answer ";
        }
    }

    //contact info
    if ($settings['account']['user_contact_info'] != "Remove") {

        if ($var['address1'] == '' && $settings['account']['user_contact_info'] != "Passive") {
            $errors[] = "Please provide address";
        }
        if ($var['city'] == '' && $settings['account']['user_contact_info'] != "Passive") {
            $errors[] = "Please provide city";
        }
        if ($var['state'] == '' && $settings['account']['user_contact_info'] != "Passive") {
            $errors[] = "Please add your province/state";
        }
        if ($var['country'] == '' && $settings['account']['user_contact_info'] != "Passive") {
            $errors[] = "Please choose a country";
        }
    }

    //sponsor
	if($settings['account']['referrer_required']){
		if ($var['sponsor'] == '' || !Account::isUser($var['sponsor'])) {
			$errors[] = "Please add a valid sponsor";
		} else $_SESSION['ref'] = $var['sponsor'];
	}elseif ($var['sponsor'] != '' && !Account::isUser($var['sponsor'])) {
	        $errors[] = "Please add a valid sponsor";
	}

    //terms
    if ($var['agree'] != 1) {
        $errors[] = "Please agree to terms";
    }

    if (is_array($errors) && !empty($errors)) {

        while (list($key, $value) = each($errors))
            $error_msg.= $value . '<br>'; //$system->logMessage("error",$errors); //
    } else {


        $country = $countries[$var['country']];
        $status = ($settings['account']['confirm_email']) ? "Pending" : "Active";
        $activation = ($settings['account']['confirm_email']) ? $system->randomString(15) : "";
        $usertype = "Basic User";
        $date = date("Y-m-d H:i:s");
        /////////////////////////////////////////////////
        $table = "_account_users";
        $insert_array = array(
		
            'username' => $var['username'],
            'password' => md5($var['password']),
            'email' => $var['email'],
			'firstname' => $var['fname'],
            'lastname' => $var['lname'],
            'gender' => $var['gender'],
            'email' => $var['email'],
            'address1' => $var['address1'],
            'address2' => $var['address2'],
            'city' => $var['city'],
            'state' => $var['state'],
            'country' => $countries[$var['country']],
            'country_code' => $var['country'],
            'zipcode' => '',
            'telephone' => '',
            'question' => $var['question'],
            'answer' => $var['answer'],
            'usertype' => $usertype,
            'activation' => $activation,
			'referrer' => $var['sponsor'],
            'regdate' => $date,
            'lastvisit' => $date,
            'regip' => $ip,
            'lastip' => $ip,
            'status' => $status,
			
        );
        $result = $db->insert_into_db($insert_array, $table);
   
        /*$user_id = $db->id_db();

        $db->noquote = array();

        $table = "_account_contact_details";

        $insert_array = array(
            'user_id' => $user_id,
            'username' => $var['username'],
            'firstname' => $var['fname'],
            'lastname' => $var['lname'],
            'gender' => $var['gender'],
            'email' => $var['email'],
            'address1' => $var['address1'],
            'address2' => $var['address2'],
            'city' => $var['city'],
            'state' => $var['state'],
            'country' => $countries[$var['country']],
            'country_code' => $var['country'],
            'zipcode' => '',
            'telephone' => '',
            'question' => $var['question'],
            'answer' => $var['answer'],
        );

        if ($result)
            $result = $db->insert_into_db($insert_array, $table);*/

        if ($result)
            $success_msg = ($status == 'Active') ? "Registration Successful. You may now login to your account" : "Please check your email for a verification link to complete your registration.";


        /*if ($var['newsletter'] == 1) {

            Account::updateSettings($var['username'], "mail", "receive_newsletter", "true");
            if ( $status == 'Active' && $plugins['phplist']['active'] ) {

                $system->importPlugin('phplist-subscribe');
      	  
            }
			if ( $status == 'Active' && $plugins['mailchimp']['active'] ) {

                $system->importPlugin('mailchimp-subscribe');
                mc_subscribe($var['fname'], $var['lname'], $var['email']); 
            }
        } else
            Account::updateSettings($var['username'], "mail", "receive_newsletter", "false");
        */
		
        if ($settings['account']['confirm_email'])
            Account::sendActivationLink($var['username']);
        else
            $system->email($var['username'], "Welcome Email");

        $system->handleEvent('on_register', get_defined_vars());
        unset($var);
    }
}

//  $error_message =  $system->displayMessage("error");

$loader = new Loader;

$data = get_defined_vars();

$loader->setVar($data);

$tpl_file = dirname(__FILE__) . DS . 'tpl' . DS . 'register.php';

$loader->setVar($data);

$loader->mainHeader();

$loader->loadOutput($tpl_file);

$loader->mainFooter();

$loader->displayOutput();