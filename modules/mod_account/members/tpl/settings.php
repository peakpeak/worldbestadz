<span class="form_title">Account Settings</span>
<?php if ($error_msg) { ?>
<span class="error_message"><?php echo $error_msg; ?></span>
<?php } ?>
<?php if ($success_msg) { ?>
<span class="success_message"><?php echo $success_msg; ?></span>
<?php } ?>
<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td width="430" height="200" align="center" valign="top"><!--<form method="post" action="" class="form_1" enctype="multipart/form-data">
        <div class="white">
          <table cellspacing=0 cellpadding=0 border=0 width="100%" class="table_1">
            <thead>
              <tr>
                <th colspan="2">Profile picture</th>
              </tr>
            <thead>
              <tr>
                <td valign="middle" align="center"><img src="{#data=account::large_image}" border="0" ></td>
                <td valign="bottom" >upload your photo <br />
                  Inappropriate photos will lead to your account being deleted<br />
                  <input type="file" name="imagefile" size="30"></td>
              </tr>
              <tr>
                <td colspan="2" align="center" valign="bottom" height="35"><input type="submit" name="submit" value="Upload photo" ></td>
              </tr>
          </table>
        </div>
      </form>-->
      <form method="post" action="" class="form_1">
        <div class="white">
          <table cellspacing=0 cellpadding=0 border=0 width="100%" class="table_1" style="width: 700px; margin: 0px auto;">
            <thead>
              <tr>
                <th colspan="2">Personal Info</th>
              </tr>
            <thead>
              <tr>
                <td width="200" height="25">First Name</td>
                <td><input type="text" name="fname" value="<?php echo $userinfo['firstname'] ?>" size="30"></td>
              </tr>
              <tr>
                <td>Last Name</td>
                <td ><input type="text" name="lname" value="<?php echo $userinfo['lastname'] ?>" size="30"></td>
              </tr>
              <tr>
                <td>Email</td>
                <td ><input type="text" name="email" value="<?php echo $userinfo['email'] ?>" size="30"></td>
              </tr>
              <?php if ($settings['account']['user_contact_info'] != "Remove") { ?>
              <tr>
                <td>Address 1</td>
                <td><input type="text" name="address1" value="<?php echo $userinfo["address1"] ?>" size="30"></td>
              </tr>
              <tr>
                <td>Address 2</td>
                <td><input type="text" name="address2" value="<?php echo $userinfo["address2"] ?>" size="30"></td>
              </tr>
              <tr>
                <td>City</td>
                <td><input type="text" name="city" value="<?php echo $userinfo["city"] ?>" size="30"></td>
              </tr>
              <tr>
                <td>State</td>
                <td><input type="text" name="state" value="<?php echo $userinfo["state"] ?>" size="30"></td>
              </tr>
              <tr>
                <td>Country</td>
                <td ><?php echo $db->make_select("country", $countries, $userinfo["country_code"], "inpts") ?></td>
              </tr>
              <?php } ?>
              <?php if ($settings['account']['use_security_question'] == true && $userinfo['question'] != "") { ?>
              <tr>
                <td><?php echo $userinfo['question'] ?> ?</td>
                <td><input type="text" name="answer" value="" size="30"></td>
              </tr>
              <?php } ?>
              <tr>
                <td colspan="2" align="center" valign="bottom" height="35"><input type="submit" name="submit" value="Update contact info"></td>
              </tr>
          </table>
        </div>
      </form>
      <form method="post" action="" class="form_1">
        <div class="white">
          <table cellspacing=0 cellpadding=0 border=0 width="100%" class="table_1" style="width: 700px; margin: 0px auto;">
            <thead>
              <tr>
                <th colspan="2">Password</th>
              </tr>
            <thead>
              <tr>
                <td width="200">Old (current) Password</td>
                <td ><input type="password" name="password_old" value="" size="30"></td>
              </tr>
              <tr>
                <td>New Password</td>
                <td ><input type="password" name="password_new" value="" size="30"></td>
              </tr>
              <tr>
                <td>Retype New Password</td>
                <td ><input type="password" name="password_new2" value="" size="30"></td>
              </tr>
              <?php if ($settings['account']['use_security_question'] == true && $userinfo['question'] != "") { ?>
              <tr>
                <td><?php echo $userinfo['question'] ?> ? </td>
                <td><input type="text" name="answer" value="" size="30"></td>
              </tr>
              <?php } ?>
              <tr>
                <td colspan="2" align="center" valign="bottom" height="35"><input name="submit" type="submit" value="Update password"></td>
              </tr>
          </table>
        </div>
      </form>
      
      <div class="white">
        <form  method="post" action="" class="form_1">
          <table cellpadding="0" cellspacing="0"  width="100%" class="table_1" style="width: 700px; margin: 0px auto;">
            <thead>
              <tr>
                <th colspan="2">Other Settings</th>
              </tr>
            <thead>
                <?php
                            foreach ($user_settings as $id => $user_setting) {
                            
                            $opt = explode("::", $user_setting["options"]);
                            $options = ($user_setting["options"] == "") ? $settings['system']['bool_no_yes'] : array(''=>'Select') + $settings[$opt[0]][$opt[1]];
                            $value = Account::getSettings($userinfo['username'], $user_setting['mod'], $user_setting['name']);
                ?>
              <tr >
                <?php if ($user_setting["input_type"] == "select") { ?>
				
				
				
                <td valign="middle" ><?php echo $user_setting['description'] ?></td>
                <td valign="middle" ><?php echo $db->make_select("user_settings[" . $user_setting['mod'] . "][" . $user_setting['name'] . "]", $options, $value, "inpts") ?> <?php //echo $user_setting["hint"]; ?></td>
				
				
				
                <?php } elseif ($user_setting["input_type"] == "multi_select") { ?>
				
				
				
				
                <td width ="40%"valign="middle"><?php echo $user_setting['description'] ?></td>
                <td valign="middle" ><input type="hidden" name="user_settings[<?php echo $user_setting['mod']; ?>][<?php echo $user_setting['name']; ?>][]" value="" size="30"/>
                      <?php
                                    $num_data = count($options);
                                    if ($num_data > 0) {
                                    $num = 0;
                                    $sort_in = 4;
                                    $tds = ($num_data % $sort_in);
                                    if ($tds != 0)
                                    $tds = $sort_in - $tds;
                      ?>
                  <table cellpadding="0" cellspacing="0"  width="100%">
                    <tr>
                      <?php
                                            foreach ($options as $skey => $svalue) {

                                            if ($num % $sort_in == 0) {
                                            ?>
                    </tr>
                    <tr>
                      <? } ?>
                      <td><input type="checkbox" name="user_settings[<?php echo $user_setting['mod']; ?>][<?php echo $user_setting['name']; ?>][]" value="<?php echo $skey; ?>" <?php if (stristr($value, ",$skey,")) { ?> checked="checked"<?php } ?>/>
                        &nbsp;<?php echo $svalue; ?></td>
                      <?php $num++;
                                    } for ($i = 0;
                                    $i < $tds;
                                    $i++) {
                                    ?>
                      <td >&nbsp;</td>
                      <?php } ?>
                    </tr>
                  </table>
                  <?php  } else { ?>
                  no option found
                  <?php } ?></td>
				  
				 
				  
                <?php } elseif ($user_setting["input_type"] == "checkbox") { ?>
				
				
				
                <td  colspan="2"><input type="hidden" name="user_settings[<?php echo $user_setting['mod']; ?>][<?php echo $user_setting['name']; ?>]" value="0" />
                  <input type="checkbox" name="user_settings[<?php echo $user_setting['mod']; ?>][<?php echo $user_setting['name']; ?>]" value="1" <?php if ($value) { ?> checked="checked"<?php } ?>/>
                  <?php //echo $user_setting["hint"]; ?><?php echo $user_setting['description'] ?></td>
				  
				  
				  
                <?php } elseif ($user_setting["input_type"] == "textarea") { ?>
				
				
				
                <td width ="40%" valign="middle"><?php echo $user_setting['description'] ?></td>
                <td valign="middle"><textarea name="user_settings[<?php echo $user_setting['mod']; ?>][<?php echo $user_setting['name']; ?>]" size="30"><?php echo $value ?></textarea>
                  <?php //echo $user_setting["hint"]; ?></td>
				  
				  
                <?php } else { ?>
				
				
				
                <td width ="40%" height="25" valign="middle"><?php echo $user_setting['description'] ?></td>
                <td valign="middle"><input type="text" name="user_settings[<?php echo $user_setting['mod']; ?>][<?php echo $user_setting['name']; ?>]" value="<?php echo $value; ?>" size="30">
                  <?php //echo $user_setting["hint"]; ?></td>
                <?php } ?>
              </tr>
              <?php } ?>
			  
			  
              <?php if ($settings['account']['use_security_question'] == true && $userinfo['question'] != "") { ?>
              <tr>
                <td><?php echo $userinfo['question'] ?> ?</td>
                <td><input type="text" name="answer" value="" size="30"></td>
              </tr>
              <?php } ?>
              <tr>
                <td colspan="2" align="middle"><input name="submit" type="submit" value="Update Settings"></td>
              </tr>
          </table>
        </form>
      </div></td>
  </tr>
</table>
