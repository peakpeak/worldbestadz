<span class="form_title">Verify Login</span>
<?php if ($error_msg) { ?>
<span class="error_message"><?php echo $error_msg; ?></span>
<?php } ?>
<?php if ($success_msg) { ?>
<span class="success_message"><?php echo $success_msg; ?></span>
<?php } ?>
<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td width="430" height="200" align="center" valign="top">
	
       <div class="white">
	    <form method="post" action="{#loader=system::url}/members/account/code" class="form_1">
		  <input type="hidden" name="redirect" value="<?php echo $redirect ?>">
          <table cellspacing=0 cellpadding=0 border=0 width="100%" class="table_1">
            <thead>
              <tr>
                <th colspan="2">Login code sent to your email</th>
              </tr>
            <thead>
              <tr>
                <td width="200">Enter Login Code</td>
                <td ><input type="text" name="code" value="" size="30"></td>
              </tr>
               <?php if ($settings['account']['use_captcha'] == true) { ?>
			  <tr>
				<td>Captcha</td>
				<td><input type="text" name="captcha" value="" size="30">
				</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td><img src="<?php echo $system->showCaptcha(); ?>" id='captcha' width="150" height="60" onclick="this.src='<?php echo $system->showCaptcha(); ?>?'+Math.random();"></td>
			  </tr>
			  <?php } ?>
			  <tr>
				<td colspan="2" align="center"><input type="submit" name="submit" value="Login">
				  <br /><a href="{#loader=system::url}/logout.php">Logout</a></td>
			  </tr>
          </table>
       </form>
	  </div>
      </td>
  </tr>
</table>
