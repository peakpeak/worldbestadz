<?php

global $system, $db, $userinfo, $settings, $countries;

$system->importClass('account');

if(!($userinfo['question'] == null || $userinfo['answer'] == null)) $system->redirect(SITEURL.FS.'members');

Account::checkPrivilege(1);

Validate::trim_request();

$var = $system->getVar();

switch ($var['submit']) {

    case("Update Security"):

        if ($var['question'] == null) $errors[] = 'Please provide security question';
		if ($var['answer'] == null) $errors[] = 'Provide answer to security question';
 

        if (is_array($errors) && !empty($errors)) {
            while (list($key, $value) = each($errors)) $error_msg.= $value . '<br>';
        } else {

            $table = "_account_users ";
            $update_array = array(
                'question' => $var['question'],
				'answer' => $var['answer'],
            );

            if ($db->update_db($update_array, $table, 'username', $userinfo['username'])) {
                Account::logActivity("account", $userinfo['username'], "settings", "updated security question");
                $success_msg = "Security question has been updated";
            }
        }
        break;
}

$userinfo = $system->importClass('account')->islogged(true);

$pageInfo['title'] = 'Account Settings';

$pageInfo['map'] = array('Settings' => SITEURL.FS.'account/security');

$loader = new Loader;

$data = get_defined_vars();

$tpl_file = dirname(__FILE__) . DS . 'tpl' . DS . 'security.php';

$loader->setVar($data);

$loader->mainHeader('private');

$loader->loadOutput($tpl_file);

$loader->mainFooter('private');

$loader->displayOutput();
?>
