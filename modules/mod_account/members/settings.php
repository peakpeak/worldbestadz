<?php

global $system, $db, $userinfo, $settings, $countries;

$system->importClass('account');

Account::checkPrivilege(1);

Validate::trim_request();

$var = $system->getVar();

switch ($var['submit']) {

    case("Update Bio"):
	
        $table = "_account_users";
        $update_array = array( 'bio' => $var['bio']);
        $db->update_db($update_array, $table, "username", $userinfo['username']);
        $success_msg = "You Bio has been updated";
		
    break;
    case("Update Interests"):

        if (count($var['interests']) > 0)
            foreach ($var['interests'] as $v) $user_interests .= "," . "$v" . ",";
        else $user_interests = "";
			
        $table = "_account_users";
        $update_array = array( 'interests' => $user_interests);
        $db->update_db($update_array, $table, "username", $userinfo['username']);
        $success_msg = "You interests have been updated";
		
    break;
    case("Update Settings"):
	
        $data_array = array();

        foreach ($var['user_settings'] as $mod => $mod_setting)
            foreach ($mod_setting as $name => $value) {

                $sql = "SELECT * FROM " . PREFIX . "_account_settings_details WHERE `mod` = '" . $mod . "' AND name = '" . $name . "' ";
                $res = $db->query_db($sql, $print = DEBUG);
                if ($db->num_rows($res) == 0)
                    $errors[] = "An error occured";
                else
                    $row = $db->fetch_db_array($res);

                if($row['required']== 'Yes' && $value == ""){$error[] = "Value for \"".$row['description']."\" is required ";continue;}
                //prepare html value
                switch ($row['input_type']) {

                    case"multi_select":
                        if (count($value) > 0)
                            foreach ($value as $v)
                                $uvalue .= ($v != "") ? "," . "$v" . "," : "";
                        else
                            $uvalue = "";
                        $value = $uvalue;
                        break;
                    default:break;
                }


                //validate data
                switch ($row['data_type']) {
                    case'integer':
                        if (!is_numeric($value) && $value != "")
                            $error[] = "Value for \"" . $row['description'] . "\" must be a number";
                        break;
                    case'string':
                        if (!is_string($value) && $value != "")
                            $error[] = "Value for \"" . $row['description'] . "\" must a string";
                        break;
                }
                $data_array[$mod][$name] = $value;
            }

        //security question
        if ($settings['account']['use_security_question'] == true && $userinfo['question'] != "") {

            if ($var['answer'] != $userinfo['answer']) {
                $errors = array();
                $errors[] = "Incorrect answer to security question";
            }
        }
        if (is_array($errors) && !empty($errors))
            while (list($k, $v) = each($errors))
                $error_msg.= $v . '<br>';
        else {
            foreach ($data_array as $mod => $mod_settings)
                foreach ($mod_settings as $name => $value)
                    Account::updateSettings($userinfo['username'], $mod, $name, $value);
            $success_msg = "Settings have been updated";
        }

    break;
    case("Upload photo"):

        try {
		
            $system->importSystemClass("jpg.upload.class.php");
            $dir = $settings['account']['image_directory'];
            $img_name = time();
            $img = new img('imagefile');
            $img->setUploadFolder($dir);
            $img->setFilename($img_name);
            $img->setDimensions(200, 200);
            $img->cropToSquare();
            $img->upload();
            $img->thumb(40, 40);
            $upload_error = $img->getErrors();
			
        } catch (Exception $e) {
            $upload_error = $e->getMessage();
        }

        if (empty($upload_error)) {

            $table = "_account_users";
            $update_array = array('image' => $img_name);
            $db->update_db($update_array, $table, "username", $userinfo['username']);
            $success_msg = "Image uploaded succesfully";
            Account::logActivity("account", $userinfo['username'], "settings", "updated profile picture");
            $unlink = $settings['account']['image_directory'] . $userinfo['image'] . ".jpg";

            if (is_file($unlink))unlink($unlink);
            $unlink = $settings['account']['image_directory'] . $userinfo['image'] . "_thumb.jpg";
            if (is_file($unlink)) unlink($unlink);
			
        } else $error_msg = $upload_error;
        


    break;
    case("Update contact info"):

        if ($var['fname'] == '')  $errors[] = "Please add a firstname";
        if ($var['lname'] == '') $errors[] = "Please add a lastname";
		
        //email
        if ($var['email'] == '') {
            $errors[] = "Please add an email";
        } elseif (!Validate::email($var['email'])) {
            $errors[] = "Please add a valid email";
        } else {
            $sql = "SELECT id from " . PREFIX . "_account_users WHERE email = '" . $db->sql_quote($var['email']) . "' and username !='" . $userinfo['username'] . "' limit 1";
            $res = $db->query_db($sql, $print = DEBUG);

            if ($db->num_rows($res) > 0)
                $errors[] = "Email already taken";
        }

        //contact info
        if ($settings['account']['user_contact_info'] != "Remove") {

            if ($var['address1'] == '' && $settings['account']['user_contact_info'] != "Passive") {
                $errors[] = "Please provide address";
            }
            if ($var['city'] == '' && $settings['account']['user_contact_info'] != "Passive") {
                $errors[] = "Please provide city";
            }
            if ($var['state'] == '' && $settings['account']['user_contact_info'] != "Passive") {
                $errors[] = "Please add your province/state";
            }
            if ($var['country'] == '' && $settings['account']['user_contact_info'] != "Passive") {
                $errors[] = "Please choose a country";
            }
        }

        //security question
        if ($settings['account']['use_security_question'] == true && $userinfo['question'] != "") {

            if ($var['answer'] != $userinfo['answer']) {
                $errors[] = "Incorrect answer to security question";
            }
        }

        if (is_array($errors) && !empty($errors)) {
            while (list($key, $value) = each($errors))$error_msg.= $value . '<br>';
        } else {

            $table = "_account_users";
            $update_array = array(
                'firstname' => $var['fname'],
                'lastname' => $var['lname'],
                'email' => $var['email'],
                'address1' => $var['address1'],
                'address2' => $var['address2'],
                'city' => $var['city'],
                'state' => $var['state'],
                'country' => $countries[$var['country']],
                'country_code' => $var['country'],
            );

            $db->update_db($update_array, $table, "username", $userinfo['username']);
           // if ($settings['mail']['use_phplist_plugin'])$system->importClass('mail')->updatePhplistInfo($userinfo['username']);
            Account::logActivity("account", $userinfo['username'], "settings", "updated personal info");
            $success_msg = "You personal info has been updated";
        }

        $system->handleEvent('on_update_profile', $userinfo);
    break;
    /////////////////////////////////////
    case("Update password"):

        if ($var['password_old'] == NULL) 
		{
            $errors[] = 'Please provide present passowrd';
        } 
		else 
		{
            $data = $system->secure("dc", array($_SESSION['pass']), PRIVATEKEY, PUBLICKEY);
            if ($data[0] != md5($var['password_old'])) $errors[] = 'Password provided is not correct'; 
        }

        if (empty($var['password_new'])) {
            $errors[] = "Please provide password";
        } elseif (strlen($var['password_new']) < $settings['account']['min_password_length']) {
            $errors[] = "Password too short";
        } elseif ($var['password_new'] != $var['password_new2']) {
            $errors[] = "New passwords do not match";
        }
        //security question
        if ($settings['account']['use_security_question'] == true && $userinfo['question'] != "") {

            if ($var['answer'] != $userinfo['answer']) 
			{
                $errors[] = "Incorrect answer to security question";
            }
        }

        if (is_array($errors) && !empty($errors)) {
            while (list($key, $value) = each($errors)) $error_msg.= $value . '<br>';
        } else {

            $table = "_account_users";
            $key_f = "username";
            $key_id = $userinfo['username'];
            $password = md5($var['password_new']);
            $update_array = array('password' => $password);
            if ($db->update_db($update_array, $table, $key_f, $key_id))
			{
                Account::setUserSession($userinfo['username'], $password);
                Account::logActivity("account", $userinfo['username'], "settings", "updated password");
                $success_msg = "Password has been updated";
            }
        }
        break;
}

$userinfo = $system->importClass('account')->islogged(true);

$sql = "SELECT * FROM " . PREFIX . "_account_settings_details ORDER BY `id` ASC ";
$res = $db->query_db($sql, $print = DEBUG);
if ($db->num_rows($res) > 0)
    while ($row = $db->fetch_db_array($res))
        $user_settings[$row['id']] = $row;


/* $sql = "SELECT *  FROM ".PREFIX."_account_interests ORDER BY `order` ASC ";
  $res = $db->query_db($sql,$print = DEBUG);
  if($db->num_rows($res) > 0) while ($row = $db->fetch_db_array($res)) $interests[$row['id']] = $row;
 */



$pageInfo['title'] = 'Account Settings';

$pageInfo['map'] = array('Settings' => SITEURL.FS.'account/settings');

$loader = new Loader;

$data = get_defined_vars();

$tpl_file = dirname(__FILE__) . DS . 'tpl' . DS . 'settings.php';

$loader->setVar($data);

$loader->mainHeader('private');

$loader->loadOutput($tpl_file);

$loader->mainFooter('private');

$loader->displayOutput();
?>
