<?php
global $system, $db, $userinfo, $settings;

$system->importClass('account');

Account::checkPrivilege(1);

Validate::trim_request();

$var = $system->getVar();

$default_redirect = SITEURL . FS . 'members' . FS . 'account';

$redirect = ($var['redirect'] == null || !isset($var['redirect']))?$default_redirect:$var['redirect'];

if($_SESSION['lcode'] == null) $system->redirect($redirect);

if($var['submit']) 
{
  
     
	if ($settings['account']['use_captcha'] == true && $system->validateCaptcha($var["captcha"]) == false) 
	{
         $errors[] = "Captcha code deos not match"; 
    }
    elseif($var['code'] == '') 
	{
	    $errors[] = "All fields are required";
	}

	if (is_array($errors) && !empty($errors))
	{
          while (list($key, $value) = each($errors)) $error_msg.= $value . '<br>'; 	
    }
	else
	{
	     if(!Account::validateLoginCode($userinfo['username'],$var['code'],$redirect)) $error_msg = "Login validation failed";
	}
	
	
	
}


$pageInfo['title'] = 'Login Code';

$pageInfo['map'] = array('Settings' => SITEURL.FS.'account/code');

$loader = new Loader;

$data = get_defined_vars();

$tpl_file = dirname(__FILE__) . DS . 'tpl' . DS . 'code.php';

$loader->setVar($data);

$loader->mainHeader('private');

$loader->loadOutput($tpl_file);

$loader->mainFooter('private');

$loader->displayOutput();
?>
