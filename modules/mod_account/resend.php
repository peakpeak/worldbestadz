<?php
global $system,$db,$settings;

$system->importClass('account');	

$var = $system->getVar();

if($var['submit']) 
{
  
      
    if ($var['username'] == '' )
	{
		$error_msg =  "Please provide your username";
	}
	elseif(!Account::isUser($var['username']))
	{
		$error_msg =  "Invalid username provided";
	}
	else
	{	   
	      $sent = Account::sendActivationLink($var['username']);
	      if ($sent) $success_msg =  "Activation link has been sent to email on file";
	      else $error_msg =  "Problem sending link";	   
	}
}

  
   $pageInfo['title']='Resend Activation Link';
   $pageInfo['meta'] = array(
                       'description'=> $settings['system']['default_meta_description'],
		               'keywords'=>$settings['system']['default_meta_keywords']); 
   $pageInfo['map'] = array('Resend Link' => SITEURL.FS.'account/resend');
					  
   $loader = new Loader;
  
   $data= get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'resend.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader();
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter();
   
   $loader->displayOutput();
   
?>
	
		

