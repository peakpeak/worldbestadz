<span class="form_title">Support</span>
<?php if ($error_msg) { ?>
<span class="error_message"><?php echo $error_msg; ?></span>
<?php } ?>
<?php if ($success_msg) { ?>
<span class="success_message"><?php echo $success_msg; ?></span>
<?php } ?>
<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td valign="top"><div align="justify">Please use the form below to send your message to the appropriate department so that we can get back to you as quickly as possible.</div>
      <br>
      <div class="white">
        <form action="" method="post" class="form_1">
          <table cellspacing=0 cellpadding=0 border=0 width="350" class="table_1" style="width: 550px; margin: 0px auto;">
            <thead>
              <tr>
                <th colspan="2">Support Form</th>
              </tr>
            </thead>
            <tr>
              <td width="150">Name</td>
              <td><input type="text" name="from_name" size="30"></td>
            </tr>
            <tr>
              <td>Email</td>
              <td><input type="text" name="from_email" size="30"></td>
            </tr>
            <tr>
              <td>Message:
              <td><textarea name="message" cols=25 rows=8 style="width:100%;"></textarea>
            </tr>
            <?php if ($settings['account']['use_captcha'] == true) { ?>
            <tr>
              <td>&nbsp;</td>
              <td><img src="<?php echo $system->showCaptcha(); ?>" id='captcha' width="150" height="60" onclick="this.src='<?php echo $system->showCaptcha(); ?>?'+Math.random();"></td>
            </tr>
            <tr>
              <td>Security Code</td>
              <td><input type="text" name="captcha" value="" size="30">
              </td>
            </tr>
            <?php } ?>
            <tr>
              <td colspan="2" align="center"><input type="submit" name="send_message" value="Send"></td>
            </tr>
          </table>
        </form>
      </div></td>
  </tr>
</table>
