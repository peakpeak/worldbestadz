<span class="form_title">Register Account</span>


	<div class="registercontentContainer">
		<div class="registercontentInner">
			<div class="register_form">
				
				<?php if ($error_msg) { ?>
				<span class="error_message"><?php echo $error_msg; ?></span>
				<?php } ?>
				<?php if ($success_msg) { ?>
				<span class="success_message"><?php echo $success_msg; ?></span>
				<?php } ?>
				
				<form action="" method="post" class="form_1">
					<div class="registerformTitle">
						<h3>PERSONAL INFORMATION</h3>
						<h3 class="registerformTitleh3">LOGIN INFO</h3>
					</div>
					<?php if($settings['account']['referrer_required'] || isset($_SESSION["ref"])) { ?>
		            <div class="form_group">
		            	<label>Sponsor:</label>
		                <input type="text" name="sponsor" value="<?php echo ($var["sponsor"]) ? $var["sponsor"] : $_SESSION["ref"] ?>" size="30">
		            </div>
		            <?php } ?>
		            
		            <div class="form_group">
		            	<label>Username:</label>
		                <input type="text" name="username" value="<?php echo $var["username"] ?>" size="30">
						<!--between <?php echo $settings['account']['min_username_length'] ?> and <?php echo $settings['account']['max_username_length'] ?> characters-->
		            </div>
		            <div class="form_group">
		            	<label>First Name:</label>
						<input type="text" name="fname" value="<?php echo $var["fname"] ?>" size="30">
		            </div>
		            <div class="form_group">
		            	<label>Password:</label>
		                <input type="password" name="password" value="" size="30">
						<!--at least <?php echo $settings['account']['min_password_length'] ?> characters-->
		            </div>
		            <div class="form_group">
		            	<label>Last Name:</label>
		                <input type="text" name="lname" value="<?php echo $var["lname"] ?>" size="30">
		            </div>
		            <div class="form_group">
		            	<label>Retype Password:</label>
                        <input type="password" name="password2" value="" size="30">
		            </div>
		            <div class="form_group">
		            	<label>Email:</label>
		                <input type="text" name="email" value="<?php echo $var["email"] ?>" size="30">
		            </div>
		            
		            
		             <?php if ($settings['account']['use_security_question'] == true) { ?>
		            <div class="form_group">
		            	<label>Security Question:</label>
		                <input name="question" type="text" value="<?php echo $var["question"] ?>" size="30">
		            </div>
		            <?php } ?>
		            <div class="form_group">
		            	<label>Retype Email:</label>
                        <input type="text" name="email2" value="<?php echo $var["email2"] ?>" size="30">
		            </div>
		            <?php if ($settings['account']['use_security_question'] == true) { ?>
		            <div class="form_group">
		            	<label>Answer:</label>
                       <input name="answer" type="text" value="<?php echo $var["answer"] ?>" size="30">
		            </div>
		            <?php } ?>
		            <div class="registerformTitle registerformTitle1  ">
		            	<h3>CONTACT INFORMATION</h3>
		            </div>
		            
		            
		            
		              <?php if ($settings['account']['user_contact_info'] != "Remove") { ?>   
		            
							<div class="form_group">
								<label>Address1:</label>
								<input type="text" name="address1" value="<?php echo $var["address1"] ?>" size="30">
							</div>
							<div class="form_group">
								<label>City:</label>
								<input type="text" name="city" value="<?php echo $var["city"] ?>" size="30">
							</div>
							<div class="form_group">
								<label>Address2:</label>
							   <input type="text" name="address2" value="<?php echo $var["address2"] ?>" size="30">
							</div>
							<div class="form_group">
								<label>State:</label>
								<input type="text" name="state" value="<?php echo $var["state"] ?>" size="30">
							</div>
							<div class="form_group">
								<label>Country:</label>
								<?php echo $db->make_select("country", $countries, $var["country"], "") ?>
							</div>
							
		                   <?php } ?>
		                   
		                   
		                      <?php if ($settings['account']['use_captcha'] == true) { ?>
								  	<div class="form_group">
										<img src="<?php echo $system->showCaptcha(); ?>" id='captcha' width="150" height="60" onclick="this.src='<?php echo $system->showCaptcha(); ?>?'+Math.random();">
										<input type="text" name="captcha" value="" size="30" placeholder="Captcha">	
									</div>
							<?php } ?>

		            <div class="form_group checkbox ">
                        <input type="checkbox" name="agree" value="1">
                        <p>I agree with <a href="{#loader=system::url}/pages/terms" target="_blank">Terms and Conditions</a></p>
                         
                        
		            </div>

		            <div class="form_group">
		            	<label></label>
		                <input type="submit" name="submit" value="REGISTER" />
		            </div>
		        </form>
			</div>
		</div>
	</div><!-- end logincontentContainer -->

