<span class="form_title">Resend Activation Link</span>
<?php if ($error_msg) { ?>
<span class="error_message"><?php echo $error_msg; ?></span>
<?php } ?>
<?php if ($success_msg) { ?>
<span class="success_message"><?php echo $success_msg; ?></span>
<?php } ?>
<table width="60%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td width="430" height="200" align="center" valign="top"><form action="" method="post" class="form_1">
        <div class="white">
          <table cellspacing=0 cellpadding=0 border=0 width="100%" class="table_1">
            <thead>
              <tr>
                <th colspan="2">Resend Activation Link</th>
              </tr>
            </thead>
            <tr>
              <td width="200">Username</td>
              <td><input type="text" name="username" value="" size="30"></td>
            </tr>
            <tr>
              <td colspan="2" align="center"><input type="submit" name="submit" value="Send"></td>
            </tr>
          </table>
        </div>
      </form></td>
  </tr>
</table>
