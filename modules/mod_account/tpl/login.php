<span class="form_title">Login</span>
		<?php if ($error_msg) { ?>
				<span class="error_message"><?php echo $error_msg; ?></span>
				<?php } ?>
				<?php if ($success_msg) { ?>
				<span class="success_message"><?php echo $success_msg; ?></span>
				<?php } ?>
							

<div class="logincontentContainer">
		<div class="logincontentInner">
			<div class="login_form">
				<img class="lgTitle" src="{#loader=template::url}styles/images/logintitle.png">
				
			
				
				 <form method="post" action="{#loader=system::url}/account/login" class="form_1">
					<input type="hidden" name="redirect" value="<?php echo $redirect ?>">
		            <div class="form_group">
						<input type="text" name="username" value="" size="30" placeholder="username" >
		            </div>
		            <div class="form_group">
		                <input type="password" name="password" value=""  placeholder="password">
		            </div>
		            
		              <?php if ($settings['account']['use_captcha'] == true) { ?>
						<div class="form_group">
							<img src="<?php echo $system->showCaptcha(); ?>" id='captcha' width="150" height="60" onclick="this.src='<?php echo $system->showCaptcha(); ?>?'+Math.random();">
							<input type="text" name="captcha" id="captcha_login" size="40" placeholder="captcha" />
						</div>
					<?php } ?>
		            
		            <div class="form_group">
		            	<a href="{#loader=system::url}/account/reset">Forgot your password?</a>
		                <input type="submit" name="submit" value="LOGIN" />
		            </div>
		        </form>
		        <div class="footerLogin">
		        	<a href="{#loader=system::url}/account/reset">New member?</a>&nbsp;&nbsp;
		        	<a href="{#loader=system::url}/account/register">Signup here</a>
		        </div>
			</div>
		</div>
	</div><!-- end logincontentContainer -->


