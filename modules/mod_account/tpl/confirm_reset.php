<span class="form_title">Confirm Reset</span>
<?php if ($error_msg) { ?>
<span class="error_message"><?php echo $error_msg; ?></span>
<?php } ?>
<?php if ($success_msg) { ?>
<span class="success_message"><?php echo $success_msg; ?></span>
<?php } ?>
<table width="60%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td width="430" height="200" align="center" valign="top"><form action="" method="post" class="form_1">
        <input type="hidden" name="username" value="<?php echo $var['username'] ?>" />
        <input type="hidden" name="uniq" value="<?php echo $var['uniq'] ?>" />
        <div class="white">
          <table cellspacing=0 cellpadding=0 border=0 width="100%"  class="table_1">
            <thead>
              <tr>
                <th colspan="2">Confirm Password Reset</th>
              </tr>
            </thead>
            <tr>
              <td>New password</td>
              <td><input type="password" name="password1" value="" <?php if ($settings['account']['virtual_keyboard']) { ?>  class ="keyboardInput" size="24"  <?php } else { ?> size="30"<?php } ?>></td>
            </tr>
            <tr>
              <td>Confirm password</td>
              <td><input type="password" name="password2" value="" <?php if ($settings['account']['virtual_keyboard']) { ?>  class ="keyboardInput" size="24"  <?php } else { ?> size="30"<?php } ?>></td>
            </tr>
            <tr>
              <td colspan="2" align="center"><input type="submit" name="submit" value="Reset"></td>
            </tr>
          </table>
        </div>
      </form></td>
  </tr>
</table>
