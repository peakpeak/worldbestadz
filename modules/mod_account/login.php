<?php
global $system,$db,$settings;

$system->importClass('account');	

//$default_redirect = SITEURL . FS . 'members' . FS . 'account';

$default_redirect = SITEURL . FS . 'members' . FS . 'ads'.FS.'loginads_rotator';

if (Account::userLoggedIn())$system->redirect($default_redirect);

$var = $system->getVar();

$redirect = ($var['redirect'] == NULL || !isset($var['redirect']))?$default_redirect:$var['redirect'];

if($var['submit']) 
{
  
    if ($settings['account']['use_captcha'] == true && $system->validateCaptcha($var["captcha"]) == false) 
	{
         $errors[] = "Captcha code deos not match"; 
    }
    elseif ($var['username'] == '' || $var['password'] == '')
	{
		 $errors[] =  "All fields are required";
	}
	if (is_array($errors) && !empty($errors))
	{
         while (list($key, $value) = each($errors)) $error_msg.= $value . '<br>'; 	
    }
	else
	{	
	    $error_msg = Account::login($var['username'],$var['password'],$redirect);	  
	}
	
}

   if(array_key_exists('activated',$var)) $success_msg = 'Your account was activated successfully. You may now login.';
   
   if(array_key_exists('reset',$var)) $success_msg = 'Your password has been reset successfully. You may now login.';
   
   $pageInfo['title'] = 'Login';
   
   $pageInfo['meta'] = array(
                       'description'=> "Login to My Account",
		               'keywords'=>'login, my account, members area,'.$settings['system']['default_meta_keywords'] ); 
   $pageInfo['map'] = array('Login' => SITEURL.FS.'account/login');
					  
   $loader = new Loader;
   
   $data= get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'login.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader();
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter();
   
   $loader->displayOutput();
   
?>
	
		

