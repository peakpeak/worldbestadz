<?php
global $system,$db,$settings;

$system->importClass('account');	

$var = $system->getVar();

    if ($var['acode']=='')
	{
		$error_msg =  "Provide Activation Code";
	}
	else
	{	   
	    $activated = Account::activate($var['username'],$var['acode']);  
		if($activated[0] == false) $error_msg = $activated[1]; 
		else $system->redirect(SITEURL.FS.'account/login?activated');
	}


   
   $pageInfo['title']='Activate Account';
   $pageInfo['meta'] = array(
                       'description'=> $settings['system']['default_meta_description'],
		               'keywords'=>$settings['system']['default_meta_keywords'], ); 
   $pageInfo['map'] = array('Activate Account' => SITEURL.FS.'account/activate');
   $loader = new Loader;
  
   $data= get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'activate.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader();
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter();
   
   $loader->displayOutput();
   
?>
	
		

