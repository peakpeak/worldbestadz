<?php
global $system,$db,$settings;

$system->importClass('account');	

$var = $system->getVar();

if(!isset($var['uniq']) || !Account::validateUniq($var['username'],$var['uniq']) || !Account::isUser($var['username'])) $system->invalidRequest();

if($var['submit']) 
{
  
	if ($var['password1'] == '' || strlen($var['password1']) < $settings['account']['min_password_length'])
	{
		$error_msg =  "Minimun password length : ".$settings['account']['min_password_length'];
	}
	elseif ($var['password1'] != $var['password2'] )
	{
		$error_msg =  "Passwords do not match";
	}
	else
	{	   
	     $reset = Account::resetPassword($var['username'],$var['password1']);  
		 if($reset[0] == false) $error_msg = $reset[1];  
		 else $system->redirect(SITEURL.FS.'account/login?reset');
	}
}

   $pageInfo['title'] = 'Reset Password';
   
   $pageInfo['meta'] = array(
                       'description'=> $settings['system']['default_meta_description'],
		               'keywords'=>$settings['system']['default_meta_keywords'], ); 
					   
   $pageInfo['map'] = array('Reset Password' => SITEURL.FS.'account/confirm_reset');
   
   $loader = new Loader;
  
   $data= get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'confirm_reset.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader();
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter();
   
   $loader->displayOutput();
   
?>
	
		

