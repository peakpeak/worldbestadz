<?php
  global $system,$db,$userinfo,$settings;
  
  $system->importClass('wallet');
  
  $system->importClass('payment');

  $var = $system->getVar();
  
  $per_page = $settings['system']['rows_per_page'];

  $sql  = "SELECT *,date(adddate) as date FROM ".PREFIX."_wallet_withdrawals ";
  
  $sql .= "WHERE username = '".$userinfo['username']."' ";
  
  $sql .= "ORDER BY id DESC";
 
  $result = $db->query_db($sql,$print = DEBUG);


			$page		 = $system->getVar('page');	
			$num_rows 	 = $db->num_rows($result);
			if(!empty($page)) 
			{
				$start = ($page - 1) * $settings['system']['rows_per_page']; 
			}			
			else
			{
				$start 	= 0;
				$page	= 1;
			}				

		
  $sql .= " LIMIT $start, $per_page "; 
  
  $result = $db->query_db($sql,$print = DEBUG);
  if ($db->num_rows($result) > 0)
  {
	 	$rows = $start;
	 	while ($row = $db->fetch_db_array($result))
		{	
			$rows++;
			$sql  = "SELECT name FROM ".PREFIX."_payment_processors WHERE id = '".$row['proid']."'  LIMIT 1";
            $result2 = $db->query_db($sql,$print = DEBUG);
            $prow = $db->fetch_db_array($result2);
			$row["processor"] = $prow["name"];
			
			$sql  = "SELECT account_id FROM ".PREFIX."_payment_user_accounts WHERE id = '".$row['proacc']."'  LIMIT 1";
            $result2 = $db->query_db($sql,$print = DEBUG);
            $prow = $db->fetch_db_array($result2);
			$row["account"] = $prow["account_id"];
			
		    $withdrawals[$rows] = $row;	
					
		}
  }
   $pageInfo['title'] = 'My Withdrawals';
  
   $pageInfo['map'] = array('My Withdrawals' => '',);
   
   $loader = new Loader;
   
   $data= get_defined_vars();
   
   $loader->setVar($data);
   
   $loader->mainHeader('private');
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'withdrawals.php';
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('private');
   
   $loader->displayOutput();
   
 ?>

 