<?php
global $system,$userinfo,$db,$settings;

$tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'transfer_balance.php';

$wallet = $system->importClass('wallet');

$payment = $system->importClass('payment');

$processors = $payment->processors('withdraw',"USD");

$to = array("" => " - select - ");

$var = $system->getVar();



   if($var['submit'])
   {

      $db->lock_transaction($userinfo['username']."_transfer_balance",10) or exit();
	  
	  Validate::trim_post();
	 
	  //if(!$settings['wallet']['allow_transfer']) $errors[] =  "Transfer is not enabled";
	  
	  if($var['amount'] == '') $errors[] =  "Please add a valid amount";
	  
	  elseif($var['from'] == '') $errors[] =  "Please select a valid processor";
	  
	  elseif($var['amount'] < $settings['wallet']['min_transfer_allowed']) $errors[] =  "Minimum transfer allowed is ".$settings['wallet']['min_transfer_allowed'];
	  
	  elseif($var['amount'] > $settings['wallet']['max_transfer_allowed']) $errors[] =  "Maximum transfer allowed is ".$settings['wallet']['max_transfer_allowed'];
	  
	  elseif($var['amount'] > $wallet->userAvailableBal($userinfo['username'],$var['from'],"USD")) $errors[] =  "Insufficient Wallet Balance";
	   
	  if(is_array($errors) && !empty($errors))
	  {

		while (list($key,$value) = each($errors)) $error_msg.= $value.'<br>';	
		
	  }
	  else 
	  {
	            $amount = $var['amount'];
				$fee = '0.00';
	            $debit_description = '$'.$amount.' transfer to repurchase balance from withdrawable balance';
				$credit_description = '$'.$amount.' transfer from withdrawable balance to repurchase balance';
				$wallet->updateUserBal($userinfo['username'],'Debit',$var['from'],$amount,$fee,"USD",$debit_description);
				$wallet->updateUserBal($userinfo['username'],'Credit',$var['from'],$amount,$fee,"USD",$credit_description,"repurchase");
				$wallet->addTransaction($userinfo['username'],"Transfer",$debit_description,$var['from'],$amount,$fee);
				$success_msg = "Transfer Successfull";	
	  }  
   }
   
   $pageInfo['title'] = 'Transfer';
  
   $pageInfo['map'] = array('Transfer' => '',);
   
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $loader->setVar($data);
   
   $loader->mainHeader('private');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('private');
   
   $loader->displayOutput();
?>