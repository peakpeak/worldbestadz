<?php
global $system,$userinfo,$db,$settings;

$tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'transfer.php';

$wallet = $system->importClass('wallet');

$payment = $system->importClass('payment');

$processors = $payment->processors(null,"USD");

$to = array("" => " - select - ");

$var = $system->getVar();

   if($var['submit'])
   {

      $db->lock_transaction($userinfo['username']."_transfer",10) or exit();
	  
	  Validate::trim_post();
	 
	  if(!$settings['wallet']['allow_transfer']) $errors[] =  "Transfer is not enabled";
	  
	  elseif($var['to'] == '' ) $errors[] =  "Please add a reciepient";
	  
	  elseif(!Account::isUser($var['to'])) $errors[] =  "You must add a valid user";
	  
	  elseif($var['processor'] == '') $errors[] =  "Please select a valid processor";
	  
	  elseif($var['amount'] == '' || !is_numeric($var['amount']) || $var['amount'] < 0) $errors[] =  "Please add a valid amount";
	  
	  elseif($var['amount'] < $settings['wallet']['min_transfer_allowed']) $errors[] =  "Minimum transfer allowed is ".$settings['wallet']['min_transfer_allowed'];
	  
	  elseif($var['amount'] > $settings['wallet']['max_transfer_allowed']) $errors[] =  "Maximum transfer allowed is ".$settings['wallet']['max_transfer_allowed'];
	  
	  elseif($var['amount'] > $wallet->userAvailableBal($userinfo['username'],$var['processor'],"USD")) $errors[] =  "Insufficient Wallet Balance";
	     
	  if(is_array($errors) && !empty($errors))
	  {

		while (list($key,$value) = each($errors)) $error_msg.= $value.'<br>';	
		
	  }
	  else 
	  {
				
						$amount = $var['amount'];
						$tfees = $settings['wallet']['transfer_fee'];
						$fee = ($tfees/100)* $amount;
						
						//if($settings['wallet']['reciever_pays_transfer_fee']){
						
						  $debit_sender = $amount;
						  $debit_fee = 0.00;
						  $credit_receiver = $amount - $fee;
						  $credit_fee = $fee;
						  
						/*}else{
						
						  $debit_sender = $amount + $fee;
						  $debit_fee = $fee;
						  $credit_receiver = $amount;
						  $credit_fee = 0.00;
						}*/
						
						$sender_description = '$'.$amount.' transfer to '.$var['to'].' fee=$'.$debit_fee.' ';
						$reciever_description = '$'.$amount.' transfer from '.$userinfo['username'].' fee=$'.$credit_fee.' ';
						$wallet->updateUserBal($userinfo['username'],'Debit',$var['processor'],$debit_sender,$debit_fee,"USD",$sender_description);
						$wallet->updateUserBal($var['to'],'Credit',$var['processor'],$credit_receiver,$credit_fee,"USD",$reciever_description);
						$wallet->addTransaction($userinfo['username'],"Transfer",$sender_description,$var['processor'],$amount,$fee);
						$success_msg = "Transfer Successfull";
						
				
				
				 
			
	  }  
   }
   
   $pageInfo['title'] = 'Transfer';
  
   $pageInfo['map'] = array('Transfer' => '',);
   
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $loader->setVar($data);
   
   $loader->mainHeader('private');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('private');
   
   $loader->displayOutput();
?>