<span class="form_title">Transfer To Repurchase Balance</span>
<?php if($error_msg){ ?>
<span class="error_message"><?php echo $error_msg;?></span>
<? } ?>
<?php if($success_msg){ ?>
<span class="success_message"><?php echo $success_msg;?></span>
<? } ?>
<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td width="430" height="200" align="center" valign="top"><div class="white">
        <form action="" method="post" class="form_1">
          <table cellspacing=0 cellpadding=0 border=0 width="100%" class="table_1">
            <thead>
              <tr>
                <th colspan="2">Transfer Money</th>
              </tr>
            </thead>
            <tr>
              <td width="30%">From Withdrawable Balance</td>
              <td><select name="from" id="from" onchange="setBalance();">
                  <option value="" >- Select -</option>
                  <?php foreach($processors as $id => $value) { ?>
                  <option value="<?php echo $id?>"  <?php if($var["from"] == $id){?> selected="selected" <?php } ?>><?php echo $value?> - $<?php echo $wallet->userAvailableBal($userinfo['username'],$id,"USD")?></option>
                  <?php } ?>
                </select>
              </td>
            </tr>
            <tr>
              <td width="30%">To Repurchase Balance</td>
              <td><select name="to" id="to" onchange="setBalance();">
                  <option value="" >- Select -</option>
                  <?php foreach($processors as $id => $value) { ?>
                  <option value="<?php echo $id?>" <?php if($var["to"] == $id){?> selected="selected" <?php } ?>><?php echo $value?> - $<?php echo $wallet->userRepurchaseBal($userinfo['username'],$id,"USD")?></option>
                  <?php } ?>
                </select>
              </td>
            </tr>
            <tr>
              <td>Amount</td>
              <td><input type="text" name="amount"  id="amount"  value="<? echo $var['amount'] ?>" onkeyup="details();" onkeydown="details();">
                <span id="details"></span></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td><input type="submit" name="submit" value="Transfer"></td>
            </tr>
          </table>
        </form>
      </div></td>
  </tr>
</table>
<script type="text/javascript">
function setBalance(){
	
	var index = document.getElementById('from').selectedIndex;
    var from = document.getElementById('from').options[index].value;
	
	if(from != '') 
	{
	  document.getElementById('to').value = from;
	}
	else
	{ 
	 document.getElementById('to').value = '';
	 document.getElementById('to').disabled = true;
	}
	
 }
 
 function details()
 {
 
 }
</script>
