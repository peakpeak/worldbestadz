<span class="form_title">Wallet Summary</span>

<?php if($error_msg){ ?>

<span class="error_message"><?php echo $error_msg;?></span>

<? } ?>

<?php if($success_msg){ ?>

<span class="success_message"><?php echo $success_msg;?></span>

<? } ?>

<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">

  <tr>

    <td width="430" height="200" align="center" valign="top"><div class="white">

        <table cellspacing=0 cellpadding=0 border=0 width="100%" class="table_1">

          <thead>

            <tr>

              <th colspan="6">Wallet Summary</th>

            </tr>

            <tr>

              <td><b>Account Type</b></td>

              <td><b>Spendable</b></td>

              <td><b>Withdrawable</b></td>

              <td><b>Repurchase</b></td>

              <td><b>Pending</b></td>

              <td><b>Total</b></td>

            </tr>

          </thead>
          <?php
          /*$new_name=array(
            'payza'   =>'<img src="../images/icons/payza.png" class="payza"/>',
            'stp'     =>'<img src="../images/icons/stp.png" class="stp"/>',
            'btc'     =>'<img src="../images/icons/bitcoin.png" class="btc"/>',
            'pm'      =>'<img src="../images/icons/perfectmoney.png" class="pm" />'

          );*/
        
        $new_name=array(
          '2' =>'<img src="../images/icons/payza.png" class="payza"/>',
          '3' =>'<img src="../images/icons/stp.png" class="stp"/>',
          '6' =>'<img src="../images/icons/bitcoin.png" class="btc"/>',
          '7' =>'<img src="../images/icons/perfectmoney.png" class="pm" />'
          );
          
          ?>
          <?php foreach($new_name as $id => $name ){ ?>

          <tr>

            <td><?php echo $name;?></td>

            <td>$<?php echo number_format( $wallet->userPurchaseBal($userinfo['username'],$id,"USD"),4) ?> </td>

            <td>$<?php echo number_format( $wallet->userAvailableBal($userinfo['username'],$id,"USD"),4) ?> </td>

            <td>$<?php echo number_format( $wallet->userRepurchaseBal($userinfo['username'],$id,"USD"),4) ?> </td>

            <td>$<?php echo number_format( $wallet->userPendingBal($userinfo['username'],$id,"USD"),4) ?> </td>

            <td>$<?php echo number_format( $wallet->userBal($userinfo['username'],$id,"USD"),4) ?> </td>

          </tr>

          <?php };
 ?>


          <tr>

            <td><b>Total</b></td>

            <td><b>$<?php echo number_format( $wallet->userPurchaseBal($userinfo['username'],null,"USD"),4) ?></b></td>

            <td><b>$<?php echo number_format( $wallet->userAvailableBal($userinfo['username'],null,"USD"),4) ?></b> </td>

            <td><b>$<?php echo number_format( $wallet->userRepurchaseBal($userinfo['username'],null,"USD"),4) ?></b></td>

            <td><b>$<?php echo number_format( $wallet->userPendingBal($userinfo['username'],null,"USD"),4) ?></b> </td>

            <td><b>$<?php echo number_format( $wallet->userBal($userinfo['username'],null,"USD"),4) ?></b> </td>

          </tr>

        </table>

      </div></td>

  </tr>

</table>

