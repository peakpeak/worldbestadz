<span class="form_title">Wallet History</span>
<?php if($error_msg){ ?>
<span class="error_message"><?php echo $error_msg;?></span>
<? } ?>
<?php if($success_msg){ ?>
<span class="success_message"><?php echo $success_msg;?></span>
<? } ?>
<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td width="430" height="200" align="center" valign="top"><div class="white">
        <table  cellspacing=1 cellpadding=2 border=0 width="100%" class="table_2">
          <thead>
            <tr>
              <th>#</th>
              <th>Date</th>
              <th>Method</th>
              <th>Description</th>
              <th>Amount</th>
              <th>Balance</th>
            </tr>
          </thead>
          <?php  foreach ($history as $num => $data) { ?>
          <tr>
            <td><?php echo $num; ?></td>
            <td><?php echo $data['date']; ?></td>
            <td><?php echo $data['processor']; ?></td>
            <td><?php echo $data['transaction']; ?></td>
            <td nowrap="nowrap"><?php echo $data['sign']; ?> $<?php echo number_format($data['amount'],2);//sprintf("%01.2f",$data['amount']); ?></td>
            <td nowrap="nowrap">$<?php echo number_format($data['post_balance'],2);//sprintf("%01.2f",$data['post_balance']); ?></td>
          </tr>
          <?php } ?>
          <?php if(count($history) == 0) { ?>
          <tr>
            <td colspan="6" align="center"> - no records found -</td>
          </tr>
          <?php } ?>
          <?php if($num_rows > $per_page) { ?>
          <tr>
            <td colspan="6" align="center"><?php echo $system->getPaginationString($page,$num_rows,$per_page,3,$system->curPageURL(),"page");?></td>
          </tr>
          <?php } ?>
        </table>
      </div></td>
  </tr>
</table>
