<span class="form_title">Transfer Money</span>
<?php if($error_msg){ ?>
<span class="error_message"><?php echo $error_msg;?></span>
<? } ?>
<?php if($success_msg){ ?>
<span class="success_message"><?php echo $success_msg;?></span>
<? } ?>
<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td width="430" height="200" align="center" valign="top"><div class="white">
        <form action="" method="post" class="form_1">
          <table cellspacing=0 cellpadding=0 border=0 width="100%" class="table_1">
            <thead>
              <tr>
                <th colspan="2">Transfer Money</th>
              </tr>
            </thead>
            <tr>
              <td width="30%">From Balance</td>
              <td><select name="processor" id="processor" onchange="details();">
                  <option value="" >- Select -</option>
                  <?php foreach($processors as $id => $value) { ?>
                  <option value="<?php echo $id?>"  <?php if($var["processor"] == $id){?> selected="selected" <?php } ?>><?php echo $value?> - $<?php echo $wallet->userAvailableBal($userinfo['username'],$id,"USD")?></option>
                  <?php } ?>
                </select>
              </td>
            </tr>
            <tr>
              <td>Amount</td>
              <td><input type="text" name="amount"  id="amount"  value="<? echo $var['amount'] ?>" onkeyup="details();" onkeydown="details();">
                <span id="details"></span></td>
            </tr>
            <tr>
              <td>Reciever</td>
              <td><input type="to" name="to" value="<? echo $var['to'] ?>"></td>
            </tr>
            <tr>
              <td>Memo</td>
              <td><textarea  name="description" cols="30" rows="5"><?php echo $var['description']?></textarea></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td><input type="submit" name="submit" value="Transfer"></td>
            </tr>
          </table>
        </form>
      </div></td>
  </tr>
</table>
<script type="text/javascript">
function details(){
	

	var tfee = '<?php echo $settings['wallet']['transfer_fee'];?>';
	var index = document.getElementById('processor').selectedIndex;
    var pro = document.getElementById('processor').options[index].value;
    var amt = document.getElementById('amount').value;
	var details;
	
	if(pro != '' && amt > 0)
	{
	  var fee = (tfee/100) * amt ;
	  var total = amt - fee;
	  details = ' '+tfee+'% fee ($'+fee+'). Reciever gets $'+total+' ';
	}else details = '';
	document.getElementById('details').innerHTML = details;
 }
</script>
