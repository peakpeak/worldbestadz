<span class="form_title">Withdraw Money</span>
<?php if($error_msg){ ?>
<span class="error_message"><?php echo $error_msg;?></span>
<? } ?>
<?php if($success_msg){ ?>
<span class="success_message"><?php echo $success_msg;?></span>
<? } ?>
<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td width="430" height="200" align="center" valign="top"><div class="white">
        <form action="" method="post" class="form_1">
          <table cellspacing=0 cellpadding=0 border=0 width="100%" class="table_1">
            <thead>
              <tr>
                <th colspan="2">Withdraw Money</th>
              </tr>
            </thead>
            <tr>
              <td width="30%">From Balance</td>
              <td><select name="processor" id="processor" onchange="set_account(this.value);details();" >
                  <option value="" >- Select -</option>
                  <?php foreach($processors as $id => $value) { ?>
                  <option value="<?php echo $id?>"  <?php if($var["processor"] == $id){?> selected="selected" <?php } ?>><?php echo $value?> - $<?php echo $wallet->userAvailableBal($userinfo['username'],$id,"USD")?></option>
                  <?php } ?>
                </select>
              </td>
            </tr>
            <tr>
              <td>Amount</td>
              <td><input type="text" name="amount" id="amount" value="<? echo $_POST['amount'] ?>" onkeyup="details();" onkeydown="details();">
                <span id="details"></span></td>
            </tr>
            <tr>
              <td>To account</td>
              <td><select name="account" id="acc">
                  <option value="" >- Select -</option>
                </select>
                &nbsp;<a href="{#loader=system::url}/members/payment/accounts">Add</a> </td>
            </tr>
            <tr>
              <td>Comments</td>
              <td><textarea  name="comments" cols="30" rows="5" ><?php echo $_POST['comments']?></textarea></td>
            </tr>
			<?php if($settings['account']['use_security_question'] == true && $userinfo['question']!="") { ?>
			 <tr>
			   <td><?php echo $userinfo['question'] ?> ?</td>
			   <td><input type="text" name="answer" value=""></td>
			 </tr>
			 <?php } ?>
            <tr>
              <td>&nbsp;</td>
              <td><input type="submit" name="submit" value="Withdraw"></td>
            </tr>
          </table>
        </form>
      </div></td>
  </tr>
</table>
<script language="javascript">

function set_account(method) {
  
      var processors = new Array();
      <?php foreach($processors as $pid => $processor){ ?>
      processors[<?php echo $pid; ?>] = new Array();
	  <?php $accounts = $payment->userAccounts($userinfo['username'],$pid); ?> 
	  <?php if(count($accounts) > 0)foreach($accounts as $aid => $account) { ?>
	  processors[<?php echo $pid; ?>][<?php echo $aid; ?>] = '<?php echo $account; ?>';
      <?php } } ?>
		 
		
		if(method == '') {
		
		   document.getElementById('acc').options.length=0;
		   document.getElementById('acc').options[0] = new Option('-Select-',"", false, false);
		   document.getElementById('acc').disabled = true;
		   
		} else {
		
          if(processors[method].length > 0){
		  
		    document.getElementById('acc').disabled = false;
		    document.getElementById('acc').options.length=0;
		    document.getElementById('acc').options[0] = new Option('-Select-',"", false, false);
			var i =0;
			for(var key in processors[method]){
			document.getElementById('acc').options[i+1] = new Option(processors[method][key],key, false, false);
			i++;}
			
		   
		  } else {
		  
		   document.getElementById('acc').options.length=0;
		   document.getElementById('acc').options[0] = new Option('-Select-',"", false, false);
		   document.getElementById('acc').disabled = true;
		   
		  }	
	   } 
	  
    }
	
function details(){
	
	var fees = new Array();
	<?php foreach($proFees as $id => $fees) { ?>
	fees[<?php echo $id?>] = '<?php echo $fees?>';
	<?php } ?>
	
	var details;
	var fee;
	var index = document.getElementById('processor').selectedIndex;
    var pro = document.getElementById('processor').options[index].value;
    var amt = document.getElementById('amount').value;
	
	
	if(pro != '' && amt > 0)
	{
	  var fee = (fees[pro]/100) * amt ;
	  var total = amt - fee;
	  details = ' - '+fees[pro]+'% fee ($'+fee+') = $'+total+' ';
	}else details = '';
	document.getElementById('details').innerHTML = details;
 }
</script>
