<span class="form_title">Withdrawals</span>
<?php if($error_msg){ ?>
<span class="error_message"><?php echo $error_msg;?></span>
<? } ?>
<?php if($success_msg){ ?>
<span class="success_message"><?php echo $success_msg;?></span>
<? } ?>
<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td width="430" height="200" align="center" valign="top"><div class="white">
        <table  cellspacing=1 cellpadding=2 border=0 width="100%" class="table_2">
          <thead>
            <tr>
              <th>#</th>
              <th>Date</th>
              <th>Method</th>
              <th>Acc ID</th>
              <th>Amount</th>
              <th>Fee</th>
              <th>Status</th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <?php  $i = 0; foreach ($withdrawals as $num => $data) { $i++;?>
          <tr>
            <td height="25" valign="middle" align="center"><?php echo $num; ?></td>
            <td height="25" valign="middle"><?php echo $data['date']; ?></td>
            <td height="25" valign="middle"><?php echo $data['processor']; ?></td>
            <td height="25" valign="middle"><?php echo $data['account']; ?></td>
            <td height="25" valign="middle"><?php echo $settings['payment']['currencies'][$data['currency']]['symbol']; ?> <?php echo $data['amount']; ?></td>
            <td height="25" valign="middle"><?php echo $settings['payment']['currencies'][$data['currency']]['symbol']; ?> <?php echo $data['fee']; ?></td>
            <td height="25" valign="middle"><?php echo $data['status']; ?> </td>
            <td height="25" valign="middle" nowrap="nowrap"><b><a id="popup<?php echo $i?>" href="{#loader=system::url}/members/wallet/withdrawal_details/wid/<?php echo $data['id']; ?>">Details</a></b></td>
          </tr>
          <?php } ?>
          <?php if(count($withdrawals) == 0) : ?>
          <tr>
            <td colspan="8" height="25" valign="middle"  align="center" > - no records found - </td>
          </tr>
          <?php endif; ?>
          <?php if($num_rows > $per_page) : ?>
          <tr>
            <td colspan="8" height="25" valign="middle" align="center" ><?php echo $system->getPaginationString($page,$num_rows,$per_page,3,$system->curPageURL(),"page");?></td>
          </tr>
          <?php endif; ?>
        </table>
      </div></td>
  </tr>
</table>
<script type="text/javascript">
 
   
	 var first = 0;
	 var last = <?php echo $i;?>;	
	  fancyjQuery(document).ready(function() {
			
			for (i = first; i  <= last; i++) {
			
			fancyjQuery("#popup"+i).fancybox({
				'width'				: '60%',
				'height'			: '100%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
          }
		
		});
  </script>
