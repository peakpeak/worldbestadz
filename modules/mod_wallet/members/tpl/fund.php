<span class="form_title">Add Money</span>
<?php if($error_msg){ ?>
<span class="error_message"><?php echo $error_msg;?></span>
<? } ?>
<?php if($success_msg){ ?>
<span class="success_message"><?php echo $success_msg;?></span>
<? } ?>
<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td width="430" height="200" align="center" valign="top"><div class="white">
        <form action="" method="post" class="form_1">
          <table cellspacing=0 cellpadding=0 border=0 width="100%"  class="table_1">
            <thead>
              <tr>
                <th colspan="2">Add Money</th>
              </tr>
            </thead>
            <tr>
              <td>Amount</td>
              <td><input type="text" name="amount" value="<? echo $_POST['amount'] ?>"></td>
            </tr>
            <tr>
              <td>Payment option</td>
              <td><select name="processor">
                  <option value="" >- Select -</option>
                  <?php foreach($processors as $id => $value) { ?>
                  <option value="<?php echo $id?>"  <?php if($var["processor"] == $id){?> selected="selected" <?php } ?>><?php echo $value?></option>
                  <?php } ?>
                </select></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td><input type="submit" name="submit" value="Fund Account"></td>
            </tr>
          </table>
        </form>
      </div></td>
  </tr>
</table>
