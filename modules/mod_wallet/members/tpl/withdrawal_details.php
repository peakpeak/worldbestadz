<span class="form_title">Withdrawal Details</span>
<?php if($error_msg){ ?>
<span class="error_message"><?php echo $error_msg;?></span>
<? } ?>
<?php if($success_msg){ ?>
<span class="success_message"><?php echo $success_msg;?></span>
<? } ?>
<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td width="430" height="200" align="center" valign="top"><div class="white">
        <table cellpadding="0" cellspacing="0" border="0" width="100%"  class="table_1">
          <tr>
            <td>Date:</td>
            <td><?php echo $row['adddate'] ?></td>
          </tr>
          <tr>
            <td>Amount:</td>
            <td><?php  echo $settings['payment']['currencies'][$row['currency']]['symbol']." ".number_format($row['amount'],2)?></td>
          </tr>
          <tr>
            <td>Fee:</td>
            <td><?php  echo $settings['payment']['currencies'][$row['currency']]['symbol']." ".number_format($row['fee'],2)?></td>
          </tr>
          <tr>
            <td>Request Method: </td>
            <td><?php echo $row['processor'] ?> - <?php echo $row['account'] ?></td>
          </tr>
          <tr>
            <td>Description:</td>
            <td><?php echo $row['description'] ?></td>
          </tr>
          <tr>
            <td>Comment:</td>
            <td><?php echo ($row['payee_comments'] != null) ? $row['payee_comments']: "None"; ?></td>
          </tr>
          <?php if($row['details'] != null) { ?>
          <tr>
            <td>Details:</td>
            <td><?php echo $row['details'] ?></td>
          </tr>
          <?php } ?>
          <tr>
            <td>Status:</td>
            <td><?php echo $row['status'] ?></td>
          </tr>
        </table>
      </div></td>
  </tr>
</table>
