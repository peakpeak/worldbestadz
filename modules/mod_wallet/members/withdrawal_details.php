<?php
   global $system,$userinfo,$db,$settings;

   $wallet = $system->importClass('wallet');
   
   $payment = $system->importClass('payment');
   
   $var = $system->getVar();
   
   if(!isset($var['wid'])) $system->invalidRequest();
   
   $sql = "SELECT * FROM ".PREFIX."_wallet_withdrawals
		   WHERE username = '".$userinfo['username']."'
		   AND id = '".$db->sql_quote($var['wid'])."' LIMIT 1";
   $res 	= $db->query_db($sql,$print = DEBUG);
   if ($db->num_rows($res) == 0) $system->invalidRequest();
   $row = $db->fetch_db_array($res);
   
   $sql = "SELECT `name`,`deposit_type` FROM ".PREFIX."_payment_processors
		   WHERE id = '".$row['proid']."' LIMIT 1";
   $res2 = $db->query_db($sql,$print = DEBUG);
   $row2 = $db->fetch_db_array($res2);
   $row['processor'] = $row2['name'];
   $row['deposit_type'] = $row2['deposit_type'];
   
   $sql  = "SELECT account_id FROM ".PREFIX."_payment_user_accounts 
           WHERE id = '".$row['proacc']."'  LIMIT 1";
   $res2 = $db->query_db($sql,$print = DEBUG);
   $row2 = $db->fetch_db_array($res2);
   $row["account"] = $row2["account_id"];
   
   
  
   
   $pageInfo['title']='Confirm Payment';
  
   $pageInfo['map'] = array(
	                  'Withdrawals' => SITEURL.FS.'members'.FS.'wallet/withdrawals',
					  'Withdrawal Details' => '',
					  );
   
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'withdrawal_details.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('popup');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('popup');
   
   $loader->displayOutput();
?>