<?php
global $system,$userinfo,$db,$settings;

$tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'fund.php';

$wallet = $system->importClass('wallet');

$payment = $system->importClass('payment');

$processors = $payment->processors('deposit',"USD");

$proFees = $payment->processorsFee('deposit');

$var = $system->getVar();

   if($var['submit'])
   {

      Validate::trim_post();
	 
	  if(!$settings['wallet']['allow_deposit']) $errors[] =  "Deposit is not enabled";
	  
	  elseif($var['processor'] == '') $errors[] =  "Please select a valid processor";
	  
	  if($var['amount'] == '') $errors[] =  "Please add a valid amount";
	  
	  else if($var['amount'] < $settings['wallet']['min_deposit_allowed']) $errors[] =  "Minimum deposit allowed is ".$settings['wallet']['min_deposit_allowed'];
	  
	  elseif($var['amount'] > $settings['wallet']['max_deposit_allowed']) $errors[] =  "Maximum deposit allowed is ".$settings['wallet']['max_deposit_allowed'];
	    
	  if(is_array($errors) && !empty($errors))
	  {

		while (list($key,$value) = each($errors)) $error_msg.= $value.'<br>';	
		
	  }
	  else 
	  {
	    
		        $item = 'Account funding by '.$userinfo['username'];
				$amount = $var['amount'];
			    $amount = round($amount, 2);
				$fee = ($proFees[$var['processor']]/100) * $amount;
				$fee = round($fee, 2);
				$total = $fee + $amount;
				$total = round($total, 2);
				$successurl = SITEURL.FS.'members';
				$failurl = SITEURL.FS.'members'.FS.'wallet/fund';
				
				$sql = "SELECT * FROM ".PREFIX."_payment_processors WHERE id = '".$var['processor']."' LIMIT 1 ";
                $res = $db->query_db($sql,$print = DEBUG);
                if($db->num_rows($res) > 0)  $row = $db->fetch_db_array($res);
               
				if($row['deposit_type'] == 'Auto'){
				
		        $custom = array('username'=>$userinfo['username'],'credit_account'=>$amount,'method'=>$var['processor']);
				
				
				$custom["payment_type"] = "deposit";
			    $custom["payment_amount"] = $total;
			    $custom["payment_fee"] = $fee;
			    $custom["payment_currency"] = "USD";
				$custom["payment_description"] = $item;
				
				
                $execute_file = "modules\\".DS."mod_wallet\\".DS."sys\\".DS."add_funds.php";
                $module = "wallet";
                $eid = $payment->executeId($module,$execute_file,$custom);

                $data = array (
				'id' => $row['id'], 
                'amount' => $total, 
                'discription' => $item,
                'success' => $successurl,
                'fail' => $failurl,
				'auto_submit' => false,
				'eid' => $eid, 
	
                );
				
				$button = $payment->generateButton($var['processor'],$data,'advanced');
				
		
                }else{
				
				     $module = "wallet";
				     $custom = array('username'=>$userinfo['username'],'credit_account'=>$amount,'method'=>$var['processor']);
                     $execute_file = "modules\\".DS."mod_wallet\\".DS."sys\\".DS."add_funds.php";
                     $eid = $payment->executeId($module,$execute_file,$custom);
					 
				     $data = array (
				     
					 'caller' => "wallet",
					 'type' => "Fund",
                     'amount' => $var["amount"], 
					 'fee' =>  $var["fee"], 
					 'currency' => $var["currency"], 
					 'processor' => $var["processor"],
                     'description' => $item,
					 'itemID' => 'RANDOM',
					 'currency' =>'USD',
                     'returnurl' => SITEURL.FS.'members'.FS.'payment/deposits',
				     'auto_submit' => false,
				     'eid' => $eid, 
	
                );
				 $button = $payment->generateManualDepositButton($data);
				 
			} $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'confirm.php';		  
	    }  
   }
  
   $success_msg = ($var["msg"]) ?$var["msg"] : $success_msg;
	
   $pageInfo['title'] = 'Fund Your Account';
  
   $pageInfo['map'] = array('Fund Your Account' => '',);
   
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $loader->setVar($data);
   
   $loader->mainHeader('private');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('private');
   
   $loader->displayOutput();
?>