<?php
global $system,$userinfo,$db,$settings;

$tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'withdraw.php';

$wallet = $system->importClass('wallet');

$payment = $system->importClass('payment');

$processors = $payment->processors('withdraw',"USD");

$proFees = $payment->processorsFee('withdraw',"USD");

$wallet->createUserBal($userinfo['username']);

$num_withdrawals_today = $wallet->getNumWithdrawalToday($userinfo['username']);

$var = $system->getVar();

   if($var['submit'])
   {
      $db->lock_transaction($userinfo['username']."_withdraw",10) or exit();
	  
	  Validate::trim_post();
	   
	  if(!$settings['wallet']['allow_withdrawal']) $errors[] =  "Withdrawal is not enabled";
	  
	  elseif($settings['account']['use_security_question'] == true && $userinfo['question'] != "" && $var['answer'] != $userinfo['answer']) $errors[] = "Incorrect answer to security question. Please try again";
	  elseif(stristr($settings['wallet']['withdrawal_days'],date(",D,")) != TRUE) $errors[] =  "Withdrawal is not enabled today";
	  
	  elseif($var['processor'] == '') $errors[] =  "Please select a valid processor";
	  
	  elseif($num_withdrawals_today >= $settings['wallet']['num_withdraw_allowed'])$errors[] = "Only ".$settings['wallet']['num_withdraw_allowed']." withdrawal(s) allowed per day.";
	  elseif($var['amount'] == '' || !is_numeric($var['amount']) || $var['amount'] < 0) $errors[] =  "Please add a valid amount";
	  
	  elseif($var['amount'] < $settings['wallet']['min_withdraw_allowed']) $errors[] =  "Minimum withdrawal allowed is ".$settings['wallet']['min_withdraw_allowed'];
	  
	  elseif($var['amount'] > $settings['wallet']['max_withdraw_allowed']) $errors[] =  "Maximum withdrawal allowed is ".$settings['wallet']['max_withdraw_allowed'];
	  
	  elseif($var['amount'] > $wallet->userAvailableBal($userinfo['username'],$var['processor'],"USD")) $errors[] =  "Insufficient Wallet Balance";
	  
	  elseif($var['account'] == "") $errors[] =  "Please choose a ".$processors[$var['processor']]." account or add one";
	    
	  if(is_array($errors) && !empty($errors))
	  {

		while (list($key,$value) = each($errors)) $error_msg.= $value.'<br>';	
		
	  }
	  else 
	  {
	     		
				$description = 'Withdrawal from wallet by '.$userinfo['username'];
				$amount = $var['amount'];
				$fee = ($proFees[$var['processor']]/100)* $amount;
				$total = $amount - $fee;
				$wallet->updateUserBal($userinfo['username'],'Debit',$var['processor'],$amount,$fee,"USD",$description);
				$winfo = $wallet->addCashout($userinfo['username'],$description,$var['comments'],$var['processor'],$var['account'],$total,$fee);
				$proinfo = $payment->processorInfo($var['processor']);
				
				if($settings['wallet']['allow_auto_withdrawal'] && $proinfo['autopay'] == 'Y' && $amount <= $settings['wallet']['max_auto_withdraw_allowed'])
				{
				    $memo = "Withdrawal from ".$settings['system']['websitename'];
					$sql = "SELECT account_id FROM ".PREFIX."_payment_user_accounts 
					        WHERE id = '".$db->sql_quote($var['account'])."' 
							AND username = '".$db->sql_quote($userinfo['username'])."'   
							LIMIT 1";
                    $res2 = $db->query_db($sql,$print = DEBUG);
                    $acc = $db->fetch_db_array($res2);
				   
					
				    $result = $payment->autoPayment($var['processor'],$acc["account_id"],$total,'USD',$memo);
				    if($result[0] == true)
					{
					    $sql = "UPDATE ".PREFIX."_wallet_withdrawals SET
                                status = 'Paid',
								paydate = now(),
					            details = '".$result[1]."'
                                WHERE id = '".$winfo[1]."' LIMIT  1";
			            $updated = $db->query_db($sql,$print = DEBUG);
					    $info = array(
					           '{date}'=>date("Y-m-d"),
					           '{amount}'=>$amount,
					           '{method}'=>$proinfo['name'],
					           '{reference}'=>$result[1]);
					    $system->email($userinfo['username'],"Withdrawal Request Completed",$info);
					    if($updated)  $success_msg= "Withdrawal request has been completed";   
					}
					else
					{
					   $info = array(
					           '{date}'=>date("Y-m-d"),
					           '{amount}'=>$amount,
					           '{method}'=>$proinfo['name']);
					   $system->email($userinfo['username'],"New Withdrawal Request",$info);
				       $success_msg= "Withdrawal request has been initiated";
					}
				
				}
				else
				{
				   $system->email($userinfo['username'],"New Withdrawal Request",array('{date}'=>date("Y-m-d"),'{amount}'=>$amount));
				   $success_msg= "Withdrawal request has been initiated";
				}
				
				$wallet->addTransaction($userinfo['username'],"Withdrawal",$description,$var['processor'],$amount,$fee);
				
	  }  
   }
   
   $pageInfo['title'] = 'Withdraw From Wallet';
  
   $pageInfo['map'] = array('Withdraw From Wallet' => '',);
   
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $loader->setVar($data);
   
   $loader->mainHeader('private');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('private');
   
   $loader->displayOutput();
?>