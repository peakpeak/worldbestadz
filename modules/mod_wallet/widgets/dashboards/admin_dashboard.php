<?php

   global $system,$userinfo,$db,$settings;
   
   $system->importPlugin("openflash");
   
   $top_balances = array();
   $top_withdrawals = array();
   
    //top balances
   
   $sql = "SELECT username, (SUM(available_balance) + SUM(pending_balance) + SUM(internal_balance) + SUM(repurchase_balance)) as total 
   FROM ".PREFIX."_wallet_balance
   GROUP BY `username` 
   ORDER BY `total` DESC  LIMIT 10";
   $res = $db->query_db($sql,$print = DEBUG);
   if ($db->num_rows($res) > 0)
   while($row = $db->fetch_db_array($res))$top_balances[] = $row;
   $db->free_result($res);
	
   //top  withdrawals
   
   $sql = "SELECT username, SUM(amount) as  total
   FROM ".PREFIX."_wallet_withdrawals
   GROUP BY `username` 
   ORDER BY `total` DESC  LIMIT 10";
   $res = $db->query_db($sql,$print = DEBUG);
   if ($db->num_rows($res) > 0)
   while($row = $db->fetch_db_array($res))$top_withdrawals[] = $row;
   $db->free_result($res);
   
   $sql = "SELECT SUM(available_balance)+SUM(internal_balance) as amount FROM ".PREFIX."_wallet_balance ";
   $sql.= "WHERE currency = 'USD'  ";
   $res = $db->query_db($sql,$print = DEBUG);
   $spendable_wallet_balance = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);
   
   $sql = "SELECT SUM(available_balance) as amount FROM ".PREFIX."_wallet_balance ";
   $sql.= "WHERE currency = 'USD'  ";
   $res = $db->query_db($sql,$print = DEBUG);
   $withdrawable_wallet_balance = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);
   
   $sql = "SELECT SUM(repurchase_balance) as amount FROM ".PREFIX."_wallet_balance ";
   $sql.= "WHERE currency = 'USD'  ";
   $res = $db->query_db($sql,$print = DEBUG);
   $repurchase_wallet_balance = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);
   
   $sql = "SELECT (SUM(available_balance)+SUM(pending_balance)+SUM(internal_balance)+SUM(repurchase_balance)) as amount FROM ".PREFIX."_wallet_balance ";
   $sql.= "WHERE currency = 'USD'  ";
   $res = $db->query_db($sql,$print = DEBUG);
   $total_wallet_balance = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);
   
  
   $pending_withdrawals = array();
   
   $sql = "SELECT SUM(amount) as amount FROM ".PREFIX."_wallet_withdrawals ";
   $sql.= "WHERE status = 'Pending'  ";
   $res = $db->query_db($sql,$print = DEBUG);
   $pending_withdrawals['total'] = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);
   
   $sql = "SELECT SUM(amount) as amount FROM ".PREFIX."_wallet_withdrawals ";
   $sql.= "WHERE status = 'Pending'  ";
   $sql.= "AND DATE(adddate) = DATE('".date("Y-m-d H:i:s")."') ";
   $res = $db->query_db($sql,$print = DEBUG);
   $pending_withdrawals['day'] = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);
   
   $sql = "SELECT SUM(amount) as amount FROM ".PREFIX."_wallet_withdrawals ";
   $sql.= "WHERE status = 'Pending'  ";
   $sql.= "AND WEEK(adddate) = WEEK('".date("Y-m-d H:i:s")."') ";
   $sql.= "AND YEAR(adddate) = YEAR('".date("Y-m-d H:i:s")."') ";
   $res = $db->query_db($sql,$print = DEBUG);
   $pending_withdrawals['week'] = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);
   
   $sql = "SELECT SUM(amount) as amount FROM ".PREFIX."_wallet_withdrawals ";
   $sql.= "WHERE status = 'Pending'  ";
   $sql.= "AND MONTH(adddate) = MONTH('".date("Y-m-d H:i:s")."') ";
   $sql.= "AND YEAR(adddate) = YEAR('".date("Y-m-d H:i:s")."') ";
   $res = $db->query_db($sql,$print = DEBUG);
   $pending_withdrawals['month'] = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);
   
   $sql = "SELECT SUM(amount) as amount FROM ".PREFIX."_wallet_withdrawals ";
   $sql.= "WHERE status = 'Pending'  ";
   $sql.= "AND YEAR(adddate) = YEAR('".date("Y-m-d H:i:s")."') ";
   $res = $db->query_db($sql,$print = DEBUG);
   $pending_withdrawals['year'] = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);
   
   $sql = "SELECT SUM(amount) as amount FROM ".PREFIX."_wallet_withdrawals ";
   $sql.= "WHERE status = 'Pending'  ";
   $res = $db->query_db($sql,$print = DEBUG);
   $pending_withdrawals['total'] = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);
   
   $paid_withdrawals = array();
   
   $sql = "SELECT SUM(amount) as amount FROM ".PREFIX."_wallet_withdrawals ";
   $sql.= "WHERE status = 'Paid'  ";
   $res = $db->query_db($sql,$print = DEBUG);
   $paid_withdrawals['total'] = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);
   
   $sql = "SELECT SUM(amount) as amount FROM ".PREFIX."_wallet_withdrawals ";
   $sql.= "WHERE status = 'Paid'  ";
   $sql.= "AND DATE(adddate) = DATE('".date("Y-m-d H:i:s")."') ";
   $res = $db->query_db($sql,$print = DEBUG);
   $paid_withdrawals['day'] = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);
   
   $sql = "SELECT SUM(amount) as amount FROM ".PREFIX."_wallet_withdrawals ";
   $sql.= "WHERE status = 'Paid'  ";
   $sql.= "AND WEEK(adddate) = WEEK('".date("Y-m-d H:i:s")."') ";
   $sql.= "AND YEAR(adddate) = YEAR('".date("Y-m-d H:i:s")."') ";
   $res = $db->query_db($sql,$print = DEBUG);
   $paid_withdrawals['week'] = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);
   
   $sql = "SELECT SUM(amount) as amount FROM ".PREFIX."_wallet_withdrawals ";
   $sql.= "WHERE status = 'Paid'  ";
   $sql.= "AND MONTH(adddate) = MONTH('".date("Y-m-d H:i:s")."') ";
   $sql.= "AND YEAR(adddate) = YEAR('".date("Y-m-d H:i:s")."') ";
   $res = $db->query_db($sql,$print = DEBUG);
   $paid_withdrawals['month'] = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);
   
   $sql = "SELECT SUM(amount) as amount FROM ".PREFIX."_wallet_withdrawals ";
   $sql.= "WHERE status = 'Paid'  ";
   $sql.= "AND YEAR(adddate) = YEAR('".date("Y-m-d H:i:s")."') ";
   $res = $db->query_db($sql,$print = DEBUG);
   $paid_withdrawals['year'] = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);
   
   $sql = "SELECT SUM(amount) as amount FROM ".PREFIX."_wallet_withdrawals ";
   $sql.= "WHERE status = 'Paid'  ";
   $res = $db->query_db($sql,$print = DEBUG);
   $paid_withdrawals['total'] = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);
   
   $total_withdrawals = $pending_withdrawals['total'] + $paid_withdrawals['total'];
   
    //wallet deposits
   
   $wallet_deposits = array();
   
   $sql = "SELECT SUM(amount) as amount FROM ".PREFIX."_wallet_transactions ";
   $sql.= "WHERE type = 'Deposit'  ";
   $sql.= "AND DATE(date) = DATE('".date("Y-m-d H:i:s")."') ";
   $res = $db->query_db($sql,$print = DEBUG);
   $wallet_deposits['day'] = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);
   
   $sql = "SELECT SUM(amount) as amount FROM ".PREFIX."_wallet_transactions ";
   $sql.= "WHERE type = 'Deposit'  ";
   $sql.= "AND WEEK(date) = WEEK('".date("Y-m-d H:i:s")."') ";
   $sql.= "AND YEAR(date) = YEAR('".date("Y-m-d H:i:s")."') ";
   $res = $db->query_db($sql,$print = DEBUG);
   $wallet_deposits['week'] = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);
   
   $sql = "SELECT SUM(amount) as amount FROM ".PREFIX."_wallet_transactions ";
   $sql.= "WHERE type = 'Deposit'  ";
   $sql.= "AND MONTH(date) = MONTH('".date("Y-m-d H:i:s")."') ";
   $sql.= "AND YEAR(date) = YEAR('".date("Y-m-d H:i:s")."') ";
   $res = $db->query_db($sql,$print = DEBUG);
   $wallet_deposits['month'] = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);
   
   $sql = "SELECT SUM(amount) as amount FROM ".PREFIX."_wallet_transactions ";
   $sql.= "WHERE type = 'Deposit'  ";
   $sql.= "AND YEAR(date) = YEAR('".date("Y-m-d H:i:s")."') ";
   $res = $db->query_db($sql,$print = DEBUG);
   $wallet_deposits['year'] = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);
   
   $sql = "SELECT SUM(amount) as amount FROM ".PREFIX."_wallet_transactions ";
   $sql.= "WHERE type = 'Deposit'  ";
   $res = $db->query_db($sql,$print = DEBUG);
   $wallet_deposits['total'] = floatval($db->db_result($res,0,'amount'));
   $db->free_result($res);
   
   
   $deposit_count = array();
   $deposit_degree = array();
   $withdrawal_count = array();
   $withdrawal_degree = array();
   $sql  = "SELECT * FROM ".PREFIX."_payment_processors ";
  // $sql .= "WHERE status = 'Active' ";
   $result = $db->query_db($sql,$print = DEBUG);
   if($db->num_rows($result) > 0) 
   while($method = $db->fetch_db_array($result)){
   
      
   
      $sql = "SELECT SUM(amount) as amount FROM ".PREFIX."_wallet_transactions ";
      $sql.= "WHERE method = '".$method["id"]."' ";
	  $sql.= "AND type = 'Deposit' ";
      $res = $db->query_db($sql,$print = DEBUG);
      $deposit_count[$method["name"]] = $deposit = floatval($db->db_result($res,0,'amount'));
      $deposit_degree[$method["name"]] = ($deposit/$wallet_deposits['total'])*100;
	  
	  
	  $sql = "SELECT SUM(amount) as amount FROM ".PREFIX."_wallet_withdrawals ";
      $sql.= "WHERE proid = '".$method["id"]."' ";
      $res = $db->query_db($sql,$print = DEBUG);
      $withdrawal_count[$method["name"]] = $withdrawal = floatval($db->db_result($res,0,'amount'));
      $withdrawal_degree[$method["name"]] = ($withdrawal/$total_withdrawals)*100;
	 
   
   }
   
   $data_1 = array();
   $colors_1 = array();
   $colors = array("#77CC6D","#B048B5","#FF5973","#6D86CC","#FFFF00");
   $last_color = end($colors);reset($colors);
   foreach($deposit_degree as $name => $degree)
   {
          $data_1[] = new pie_value($degree,$name." Deposits <br> $".$deposit_count[$name]);
		  $color = current($colors);
		  if($color == $last_color)reset($colors); 
		  else next($colors);
		  $colors_1[] = $color;
   }
   $pie_1 = new pie();
   $pie_1->set_animate( true );
   $pie_1->add_animation( new pie_fade() );
   $pie_1->set_label_colour( '#432BAF' );
   $pie_1->set_alpha( 0.75 );
   $pie_1->radius(75);
   $pie_1->set_tooltip( '#label#(#percent#)' );
   
   $pie_1->set_no_labels();
   $pie_1->set_values($data_1);
   $pie_1->set_colours($colors_1);
   
   //creating the chart
   $chart_1 = new open_flash_chart();
   $chart_1->set_title( new title('Deposits by Processors') );
   $chart_1->set_bg_colour('#F5F5F5');
   $chart_1->add_element($pie_1);
   
   $element_1 = 'adshare_pie_1';
   $element_1_data = $chart_1->toPrettyString();
   
   
  //withdrawal
   $data_2 = array();
   $colors_2 = array();
   $colors = array("#77CC6D","#B048B5","#FF5973","#6D86CC","#FFFF00");
   $last_color = end($colors);reset($colors);
   foreach($withdrawal_degree as $name => $degree)
   {
          $data_2[] = new pie_value($degree, $name." Withdrawals $".$withdrawal_count[$name]);
		  $color = current($colors);
		  if($color == $last_color)reset($colors); 
		  else next($colors);
		  $colors_2[] = $color;
   }
   $pie_2 = new pie();
   $pie_2->set_animate( true );
   $pie_2->add_animation( new pie_fade() );
   $pie_2->set_label_colour( '#432BAF' );
   $pie_2->set_alpha( 0.75 );
   $pie_2->radius(75);
   $pie_2->set_tooltip( '#label# (#percent#)' );
   
   $pie_2->set_no_labels();
   $pie_2->set_values($data_2);
   $pie_2->set_colours($colors_2);
   
   //creating the chart
   $chart_2 = new open_flash_chart();
   $chart_2->set_title( new title('Withdrawals by Processors') );
   $chart_2->set_bg_colour('#F5F5F5');
   $chart_2->add_element($pie_2);
   
   $element_2 = 'adshare_pie_2';
   $element_2_data = $chart_2->toPrettyString();
   
   
 

 
   $w = 300;
   $h = 300;
   $graph ='
   
     <script type="text/javascript" src="'.SITEURL.FS.$settings['system']['plugins_directory'].'/openflash/js/json/json2.js"></script>
	 <script type="text/javascript" src="'.SITEURL.FS.$settings['system']['plugins_directory'].'/openflash/js/swfobject.js"></script>
     <script type="text/javascript">
	';
	
   //add graph elements
   $graph.=' 
	 swfobject.embedSWF("'.SITEURL.FS.$settings['system']['plugins_directory'].'/openflash/open-flash-chart.swf",
	 "'.$element_1.'",
	 "'.$w.'","'.$h.'",
	 "9.0.0",
	 "'.SITEURL.FS.$settings['system']['plugins_directory'].'/openflash/expressInstall.swf",
	 {"get-data":"get_'.$element_1.'_data"}
	 );';
	 $graph.=' 
	 swfobject.embedSWF("'.SITEURL.FS.$settings['system']['plugins_directory'].'/openflash/open-flash-chart.swf",
	 "'.$element_2.'",
	 "'.$w.'","'.$h.'",
	 "9.0.0",
	 "'.SITEURL.FS.$settings['system']['plugins_directory'].'/openflash/expressInstall.swf",
	 {"get-data":"get_'.$element_2.'_data"}
	 );'; 
 
   $graph.='</script>';
   $graph.='<script type="text/javascript">
     function ofc_ready()
     {
       // alert(\'ofc_ready\');
     }';
  
   $graph.= 'function get_'.$element_1.'_data()
     {
	   return JSON.stringify('.$element_1.'_data);
     }'; 
   $graph.= 'function get_'.$element_2.'_data()
     {
	   return JSON.stringify('.$element_2.'_data);
     }';

   $graph.='var '.$element_1.'_data = '.$element_1_data.';';
   $graph.='var '.$element_2.'_data = '.$element_2_data.';';
   
   $graph.='</script>';
   $graph.='<div id="'.$element_1.'"></div>';
   $graph.='<div id="'.$element_2.'"></div>';
 

   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'admin_dashboard.php';
   
   $loader->setVar($data);
   
   $loader->loadOutput($tpl_file);
   
   $loader->displayOutput();
?>