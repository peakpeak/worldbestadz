<table class="tableS" cellpadding="0" cellspacing="0">


 <tr>
   <td colspan="2">
   <table  class="tableS" cellpadding="0" cellspacing="0" width="100%">
    <thead>
  <tr>
    <th colspan="2"  align="left">Wallet Statistics</th>
  </tr>
  </thead>
   <tr>
    <td valign="top" width="30%">
	 <table cellpadding="0" cellspacing="0"  width="100%">
	  <tr>
       <td>Deposits</td>
       <td>$<?php echo $wallet_deposits['total'];?></td>
      </tr>
	   <tr>
       <td align="right"><b>Total</b></td>
       <td><b>$<?php echo $wallet_deposits['total'];?></b></td>
      </tr>
	  <tr>
       <td>Spendable Balance</td>
       <td>$<?php echo $spendable_wallet_balance;?></td>
      </tr>
	  <tr>
       <td>Withdrawable Balance</td>
       <td>$<?php echo $withdrawable_wallet_balance;?></td>
      </tr>
	  <tr>
       <td>Repurchase Balance</td>
       <td>$<?php echo $repurchase_wallet_balance;?></td>
      </tr>
	  <tr>
       <td align="right"><b>Total</b></td>
       <td><b>$<?php echo $total_wallet_balance;?></b></td>
      </tr>
	  <tr>
       <td>Pending Withdrawals</td>
       <td>$<?php echo $pending_withdrawals['total'];?></td>
      </tr>
	  <tr>
       <td>Paid Withdrawals</td>
       <td>$<?php echo $paid_withdrawals['total'];?></td>
      </tr>
	  <tr>
       <td align="right"><b>Total</b></td>
       <td><b>$<?php echo $total_withdrawals;?></b></td>
      </tr>
	 </table>
	</td>
    <td><?php echo $graph;?></td>
  </tr>
 </table>
 </td>
 </tr>
 <tr>
    <td colspan="2">
	<table class="tableS" cellpadding="0" cellspacing="0">
 <thead>
  <tr>
    <th align="left">&nbsp;</th>
	<th align="left">Wallet Deposits</th>
	<th align="left">Pending Withdrawals</th>
	<th align="left">Paid Withdrawals</th>
  </tr>
  </thead>
  <tr>
    <td align="left">Today</td>
    <td align="left">$<?php echo $wallet_deposits['day'];?></td>
	<td align="left">$<?php echo $pending_withdrawals['day'];?></td>
	<td align="left">$<?php echo $paid_withdrawals['day'];?></td>
  </tr>
   <tr>
    <td align="left">This Week</td>
	<td align="left">$<?php echo $wallet_deposits['week'];?></td>
	<td align="left">$<?php echo $pending_withdrawals['week'];?></td>
	<td align="left">$<?php echo $paid_withdrawals['week'];?></td>
  </tr>
   <tr>
    <td align="left">This Month</td>
	<td align="left">$<?php echo $wallet_deposits['month'];?></td>
	<td align="left">$<?php echo $pending_withdrawals['month'];?></td>
	<td align="left">$<?php echo $paid_withdrawals['month'];?></td>
  </tr>
   <tr>
    <td align="left">This Year</td>
	<td align="left">$<?php echo $wallet_deposits['year'];?></td>
	<td align="left">$<?php echo $pending_withdrawals['year'];?></td>
	<td align="left">$<?php echo $paid_withdrawals['year'];?></td>
  </tr>
  <tr>
    <td align="left">Total</td>
	<td align="left">$<?php echo $wallet_deposits['total'];?></td>
	<td align="left">$<?php echo $pending_withdrawals['total'];?></td>
	<td align="left">$<?php echo $paid_withdrawals['total'];?></td>
  </tr>
  </table>
   </td>
  </tr>
  <tr>
    <td width="50%">
	<table class="tableS" cellpadding="0" cellspacing="0">
    <thead>
  <tr>
    <th  colspan="2" align="left">Top 10 balances</th>
  </tr>
  </thead>
  <?php foreach($top_balances as $balance) { ?>
  <tr>
    <td align="left"><?php echo $balance["username"] ?></td>
    <td align="left">$<?php echo $balance["total"] ?></td>
  </tr>
  <?php } ?>
  </table>
   </td>
   <td>
	<table class="tableS" cellpadding="0" cellspacing="0">
    <thead>
  <tr>
    <th  colspan="2" align="left">Top 10 withdrawals</th>
  </tr>
  </thead>
  <?php foreach($top_withdrawals as $withdrawal) { ?>
  <tr>
    <td align="left"><?php echo $withdrawal["username"] ?></td>
    <td align="left">$<?php echo $withdrawal["total"] ?></td>
  </tr>
  <?php } ?>
  </table>
   </td>
  </tr>
</table>