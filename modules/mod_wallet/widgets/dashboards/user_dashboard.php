<?php
   global $system,$userinfo,$db,$settings;
   
   $wallet = $system->importClass('wallet');
   
   $sql = "SELECT SUM(amount) as amount FROM ".PREFIX."_wallet_withdrawals ";
   $sql.= "WHERE username = '".$userinfo['username']."'  ";
   $sql.= "AND status = 'Pending'  ";
   $res = $db->query_db($sql,$print = DEBUG);
   $pending_withdrawals = number_format($db->db_result($res,0,'amount'),2);
   $db->free_result($res);
   
   $sql = "SELECT SUM(amount) as amount FROM ".PREFIX."_wallet_withdrawals ";
   $sql.= "WHERE username = '".$userinfo['username']."'  ";
   $sql.= "AND status = 'Paid'  ";
   $res = $db->query_db($sql,$print = DEBUG);
   $paid_withdrawals = number_format($db->db_result($res,0,'amount'),2);
   $db->free_result($res);
   
   $total_withdrawals = $paid_withdrawals + $pending_withdrawals ;

   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'user_dashboard.php';
   
   $loader->setVar($data);
   
   $loader->loadOutput($tpl_file);
   
   $loader->displayOutput();
?>