<?php
global $system,$db,$settings,$admininfo;

$system->importClass('account')->checkPrivilege(2);

$payment = $system->importClass('payment');

$wallet = $system->importClass('wallet');

$var = $system->getVar();

$processors = $payment->processors('withdraw');

$processors2 = $payment->processors('masspay');

$processors3 = $payment->processors('masspayfile');

    if($var['update'] == 'y')
	{
	
	    $csv_array = array();
		
		for ($i = 0; $i < count ($var['list']); $i++)
		{
			
			$n 	= $i+1;
			$tid = $var['list'][$i];
			$sql = "SELECT * FROM ".PREFIX."_wallet_withdrawals WHERE id = '".$tid."'";
			$res = $db->query_db($sql,$print = DEBUG);
			if ($db->num_rows($res) > 0)
			{ 
                $row = $db->fetch_db_array($res);	
			    switch($var['submit'])
	            {
		           case"Update":
				   $sql = "UPDATE ".PREFIX."_wallet_withdrawals SET
                       status = '".$var['status'][$tid]."',
					   paydate = now(),
					   details = '".$var['details'][$tid]."'
                       WHERE id = '".$tid."' LIMIT  1";
					
			       $updated = $db->query_db($sql,$print = DEBUG);
				   if($updated) 
				   {
				       if($var['status'][$tid] == "Paid"){
				       $info = array(
					   '{date}'=>date("Y-m-d"),
					   '{amount}'=>$row["amount"],
					   '{method}'=>$processors[$row['proid']],
					   '{reference}'=>$var['details'][$tid]);
					   $system->email($row['username'],"Withdrawal Request Completed",$info);}
					   $success[] = "withdrwal request #".$tid." has been updated";
				   }
				   break;
				   
				   case"Set as Paid":
				   $sql = "UPDATE ".PREFIX."_wallet_withdrawals SET
                       status = 'Paid',
					   paydate = now(),
					   details = '".$var['details'][$tid]."'
                       WHERE id = '".$tid."' LIMIT  1";
					
			       $paid = $db->query_db($sql,$print = DEBUG);
				   $info = array(
					   '{date}'=>date("Y-m-d"),
					   '{amount}'=>$row["amount"],
					   '{method}'=>$processors[$row['proid']],
					   '{reference}'=>$var['details'][$tid]);
					$system->email($row['username'],"Withdrawal Request Completed",$info);
				   
				   if($paid) $success[] = "withdrwal request #".$tid." has been paid";
				   break;
				   case"Refund":
				   
				   $sql = "UPDATE ".PREFIX."_wallet_withdrawals SET
                       status = 'Refunded',
					   details = '".$var['details'][$tid]."'
                       WHERE id = '".$tid."' LIMIT  1";
					
			       $db->query_db($sql,$print = DEBUG);
				    //send update email
			       $description = 'Withdrawal Refund';
			       $total = $row['amount'] + $row['fee'];
			       $wallet->updateUserBal($row['username'],'Credit',$row['proid'],$total,0.00,"USD",$description);
				   $success[] = "withdrwal request #".$tid." has been refunded";
				   break;
				   case"Delete":
				   
				   $sql = "DELETE FROM ".PREFIX."_wallet_withdrawals  WHERE id = '".$tid."' LIMIT  1";
			       $db->query_db($sql,$print = DEBUG);
				    //send update email
				   $success[] = "withdrwal request #".$tid." has been deleted";
				   break;
				   case"Make Mass Payment":
				   
				   $sql = "SELECT account_id FROM ".PREFIX."_payment_user_accounts WHERE id = '".$row['proacc']."'  LIMIT 1";
                   $res2 = $db->query_db($sql,$print = DEBUG);
                   $acc = $db->fetch_db_array($res2);
				   
			       $acc["account_id"];
				   if($acc["account_id"] == null);
			       $memo = "";
				   
				   $result = $payment->autoPayment($var['masspay'],$acc["account_id"],$row['amount'],$row['currency'],$memo);
				   if($result[0] == true)
				   {
				    
					 $sql = "UPDATE ".PREFIX."_wallet_withdrawals SET
                             status = 'Paid',
							 paydate = now(),
					         details = '".$result[1]."'
                             WHERE id = '".$tid."' LIMIT  1";
			         $updated = $db->query_db($sql,$print = DEBUG);
					 $info = array(
					   '{date}'=>date("Y-m-d"),
					   '{amount}'=>$row["amount"],
					   '{method}'=>$processors[$row['proid']],
					   '{reference}'=>$result[1]);
					 $system->email($row['username'],"Withdrawal Request Completed",$info);
					 if($updated) $success[] = "withdrwal request #".$tid." has been updated";
                   }
				   else $errors[] = "error processing withdrwal request #".$tid.":".$result[1];
				   break;
				   case"Generate File":
				   
				   $sql = "SELECT account_id FROM ".PREFIX."_payment_user_accounts WHERE id = '".$row['proacc']."'  LIMIT 1";
                   $res2 = $db->query_db($sql,$print = DEBUG);
                   $acc = $db->fetch_db_array($res2);
				   $csv_array[] = array($var['masspay'],$acc["account_id"],$row['amount'],$row['currency'],$memo);
			      /* $acc["account_id"];
				   if($acc["account_id"] == null);
			       $memo = "";*/
				   
				  
				   
				   break;
			     }
			     $db->free_result($res);
				 if(count($csv_array) > 0 && $var['submit'] == 'Generate File')
				 {
				    $output = $payment->massPaymentFile($var['masspay'],$csv_array);
					if($output[0] == false) $errors[] = "error generating Generate File: ".$output[1];
				   
				 }
	       }	
		  
		}
		
		if(is_array($errors) && !empty($errors)) while (list($key,$value) = each($errors)) $return_msg.= $value.'<br>';	
		elseif(is_array($success) && !empty($success)) while (list($key,$value) = each($success)) $return_msg.= $value.'<br>';	
	
	}
	

$per_page = $settings['system']['rows_per_page'];	
	
$sql  = "SELECT COUNT(*) as total FROM ".PREFIX."_wallet_withdrawals ";

$sql .= "WHERE id != '' ";
	
if($var['submit'] == 'Search'){


         if(is_numeric($var['total_from']) || is_numeric($var['total_to'])){
	  
	     if(!is_numeric($var['total_to'])) $sql .= "AND amount > '".$var['total_from']."'  ";
	   
	     elseif(!is_numeric($var['total_from'])) $sql .= "AND amount < '".$var['total_to']."'  ";
	   
	     else $sql .= "AND amount BETWEEN '".$var['total_from']."' AND '".$var['total_to']."' "; 
		 
         }
		   
         if(isset($var['currency']) && $var['currency']!='A') $sql .= "AND currency = '".$var['currency']."' ";
	  

         if(isset($var['status']) && count($var['status'])> 0)
	     { 
	        $sql .= "AND ( ";
	        foreach($var['status'] as $status) { $sql .= "status = '".$status."' "; if($status != end($var['status'])) $sql .= "OR ";}
	        $sql .= ") ";
	      }
		  
         if(!empty($var['period']) && (!empty($var['time_from']) || !empty($var['time_to'])))
	     { 
	    
		    $time1 = explode("/",$var['time_from']);
		    $time_from = $time1[2]."-".$time1[0]."-".$time1[1];
		
		    $time2 = explode("/",$var['time_to']);
		    $time_to = $time2[2]."-".$time2[0]."-".$time2[1];
		
		    if(!checkdate($time2[0],$time2[1],$time2[2]) && checkdate($time1[0],$time1[1],$time1[2])) $sql .= "AND date = '".$time_from."'  ";
	   
	        elseif(checkdate($time2[0],$time2[1],$time2[2]) && !checkdate($time1[0],$time1[1],$time1[2])) $sql .= "AND date < '".$time_to."'  ";
	   
	        else  $sql .= "AND adddate BETWEEN date('".$time_from."') AND date('".$time_to."') ";
	 
	     }
     

          if(!empty($var['proid']))$sql .= "AND proid = '".$var['proid']."' ";
	  
	      if(!empty($var['username']))$sql .= "AND username = '".$var['username']."' ";

}


  
  $sql .= "ORDER BY id DESC";
  $result = $db->query_db($sql,$print = DEBUG);
  $num_rows = $db->db_result($result, 0, 'total');
  $page = $var['page'];
  $per_page = (is_numeric($var['per_page']) 
	             && $var['per_page'] <= $settings['system']['rows_per_page'])?
				 $var['per_page']:$settings['system']['rows_per_page'];		 	
  if(!empty($page)){$start = ($page - 1) * $per_page;}
  else { $start 	= 0; $page	= 1; }		
  $sql = str_replace("COUNT(*) as total", " * ",$sql);
  $sql .= " LIMIT $start, $per_page "; 
  $result = $db->query_db($sql,$print = DEBUG);
  if ($db->num_rows($result) > 0)
  {
	 	$rows = $start;
	 	while ($row = $db->fetch_db_array($result))
		{	
			$rows++;
			$sql  = "SELECT name FROM ".PREFIX."_payment_processors WHERE id = '".$row['proid']."'  LIMIT 1";
            $result2 = $db->query_db($sql,$print = DEBUG);
            $prow = $db->fetch_db_array($result2);
			$row["processor"] = $prow["name"];
			
			$sql  = "SELECT account_id FROM ".PREFIX."_payment_user_accounts WHERE id = '".$row['proacc']."'  LIMIT 1";
            $result2 = $db->query_db($sql,$print = DEBUG);
            $prow = $db->fetch_db_array($result2);
			$row["account"] = $prow["account_id"];
			
		    $withdrawals[$rows] = $row;			
		}
  }
 
	
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'withdrawals.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();  

?>
