<?php
global $system,$userinfo,$db,$settings;

$system->importClass('account')->checkPrivilege(2);

$payment = $system->importClass('payment');

$processors = $payment->processors('withdraw');

$var = $system->getVar();

if(!isset($var['wid'])) $system->invalidRequest();



   if($var['submit'])
   switch($var['submit'])
   {

     
      case('Update'):
      Validate::trim_post();
	  
	  if($var['wid'] == '') $errors[] =  "Please select a valid payment option";
	    
	  if(is_array($errors) && !empty($errors)) while (list($key,$value) = each($errors)) $return_msg.= $value.'<br>';	
		  
	  else 
	  {
	    
		    $sql = "SELECT id FROM ".PREFIX."_wallet_withdrawals WHERE id = '".$var['wid']."' ";
			$res = $db->query_db($sql,$print = DEBUG);
			if ($db->num_rows($res) > 0)
			{ 
			  
               $row = $db->fetch_db_array($res);
			   $paydate = ($row['paydate'] == '' || $row['paydate'] == '0000-00-00' )? date('Y-m-d'):$row['paydate'];
			   $sql = "UPDATE ".PREFIX."_wallet_withdrawals SET
			      	   status =  '".$var['status']."',
					   details =  '".$var['details']."',
					   paydate =  '".$paydate."',
					   payer_comments =  '".$var['comments']."'
                       WHERE id = '".$var['wid']."' LIMIT  1";
					
			   $db->query_db($sql,$print = DEBUG);
			   $db->free_result($res);
               //send update email
			   if($var['status'] == 'Paid')
			   {
				       $info = array(
					   '{date}'=>date("Y-m-d"),
					   '{amount}'=>$row["amount"],
					   '{method}'=>$processors[$row['proid']],
					   '{reference}'=>$var['details']);
					   $system->email($row['username'],"Withdrawal Request Completed",$info);
			   }
               $return_msg = "Withdrawal Updated";


	       }	   
			  
	  }
      break;
	  
	  
   }

 
  $per_page = $settings['system']['rows_per_page'];
  
  $sql  = "SELECT * FROM ".PREFIX."_wallet_withdrawals ";
  $sql .= "WHERE id = '".$var['wid']."'  LIMIT 1 ";
  	

  $result = $db->query_db($sql,$print = DEBUG);
  if ($db->num_rows($result) > 0) $withdrawal = $db->fetch_db_array($result);
  
 $sql  = "SELECT name FROM ".PREFIX."_payment_processors WHERE id = '".$withdrawal['proid']."'  LIMIT 1";
            $result2 = $db->query_db($sql,$print = DEBUG);
            $prow = $db->fetch_db_array($result2);
			$withdrawal["processor"] = $prow["name"];
			
			$sql  = "SELECT account_id,AES_DECRYPT(field_descriptions,'".ENCKEY."') as field_descriptions ,AES_DECRYPT(field_values,'".ENCKEY."') as field_values FROM ".PREFIX."_payment_user_accounts WHERE id = '".$withdrawal['proacc']."'  LIMIT 1";
            $result2 = $db->query_db($sql,$print = DEBUG);
            $prow = $db->fetch_db_array($result2);
			$withdrawal["account"]["id"] = $prow["account_id"];
			$withdrawal["account"]["description"] = unserialize(html_entity_decode($prow['field_descriptions']));
			$withdrawal["account"]["values"] = unserialize(html_entity_decode($prow['field_values']));
  
  
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'withdrawal_details.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();  
?>
