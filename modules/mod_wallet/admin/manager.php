<?php
global $system,$db,$settings,$admininfo;

$system->importClass('account')->checkPrivilege(3);

$payment = $system->importClass('payment');

$processors = $payment->processors();

$wallet = $system->importClass('wallet');

$var = $system->getVar();



    switch($var['Submit'])
	{
		
		case"Update":

        if($var['username'] == null || !Account::isUser($var['username']))
        {
                $errors[] =  "Add a valid username";
        }
        elseif(!is_numeric($var['amount']))
        {
                $errors[] =  "Add a valid amount";
        }
		elseif($var['processor'] == '') 
		{
		        $errors[] =  "Please select a valid processor";	
		}
        elseif($var['type'] == 'Debit' && $var['amount'] > $wallet->userBal($var['username'],$var['processor'],"USD"))
        {
               $errors[] =  "Amount is greater than user balance";
        }
        
        if(is_array($errors) && !empty($errors)) while (list($key,$value) = each($errors)) $return_msg.= $value.'<br>';	
        else 
	    {
	           $description = ($var['memo'] != null)? $var['memo'] : $var['type']." from administrator";//$settings['wallet']['default_memo'];
		       $wallet->updateUserBal($var['username'],$var['type'],$var['processor'],$var['amount'],0.0,"USD",$description,$var['balance_type']);
			   if($var['type'] == 'Credit')$wallet->addTransaction($var['username'],"Deposit",$description,$var['processor'],$var['amount'],0.0);
			   $sign = ($var['type'] == 'Credit') ? "+":"-";
			   $log_description = "updated ".$var['username'].' '.$processors[$var['processor']].' account balance: '.$sign.' $'.$var['amount'];
			   Account::logActivity("wallet", $admininfo['username'], "manager", $log_description);
               $return_msg = "User balance updated";	
	    }

        break;
 }
 
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'manager.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();
?>
