<h1>Wallet Manager</h1>

<p class="info">Here you can credit to and debit from members wallet balance</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

<form  method="post" action="">
<table cellpadding="0" cellspacing="0" class="tableS">
<thead>
  <tr >
    <th colspan="2" class="left" >Update User Balance</th>
  </tr>
  </thead>
   <tr>
     <td width="30%">Username</td>
     <td><input type="text" name="username" value="<?php echo $var['username'] ?>"></td>
  </tr>
  <tr>
    <td>Transaction Type</td>
    <td><select name="type">
           <option value="Credit" <?php if($var["type"] == 'Credit'){?> selected="selected" <?php } ?> >Credit</option>
           <option value="Debit"  <?php if($var["type"] == 'Debit'){?> selected="selected" <?php } ?>>Debit</option>
        </select> </td>
  </tr>
  <tr>
    <td>Processor</td>
    <td><select name="processor">
		  <option value="" >- Select -</option>
		  <?php foreach($processors as $id => $value) { ?>
		  <option value="<?php echo $id?>"  <?php if($var["processor"] == $id){?> selected="selected" <?php } ?>><?php echo $value?> balance</option>
		  <?php } ?>
	 </select>
  </td>
  </tr>
  <tr>
    <td>Balance Type</td>
    <td><select name="balance_type">
           <option value="internal"  <?php if($var["balance_type"] == 'internal'){?> selected="selected" <?php } ?> >Spendable</option>
           <option value="available"  <?php if($var["balance_type"] == 'available'){?> selected="selected" <?php } ?> >Withdrawable</option>
		   <option value="repurchase"  <?php if($var["balance_type"] == 'repurchase'){?> selected="selected" <?php } ?>>Repurchase</option>
        </select> 
	</td>
  <tr>
     <td>Amount</td>
     <td><input type="text" name="amount" value="<?php echo $var['amount'] ?>"></td>
  </tr>
  <tr>
     <td>Memo</td>
     <td><textarea name="memo"><?php echo $var['memo'] ?></textarea></td>
  </tr>
  <tr>
    <td colspan="2" class="left" ><input type="submit" name="Submit" value="Update"/></td>
  </tr>
  </table>
</form>


