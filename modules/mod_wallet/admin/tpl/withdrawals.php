<h1>Withdrawals</h1>

<p class="info">Here you can view withdrawals from members wallet</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

<div class="box" style="margin-bottom:10px">
<form action="index.php" method="get" >
<input  type="hidden" name="mod" value="wallet" />
<input  type="hidden" name="go" value="withdrawals" />
  <table cellpadding="0" cellspacing="0" class="utility">
    <tr style="background-color:transparent">
      <td style="position:relative">
	    Username: <input type="text" name="username" value="<?php echo $var["username"]?>"  />
		Amount: 
	    <input type="text" name="total_from" value="<?php echo $var["total_from"]?>" size="3" class="input-text-short" />&nbsp;-&nbsp;
	    <input type="text" name="total_to" value="<?php echo $var["total_to"]?>" size="3" class="input-text-short" />
		
	  </td>
       <td align="left"><table cellpadding="0" cellspacing="0" border="0"> <tr>
	   <td>Status:</td>
<td><input type="checkbox" class="html-checkboxes" name="status[]" value="Pending"<?php if(in_array('Pending',$var["status"])){ ?> checked="checked" <?php } ?>/>Pending</td>
<td><input type="checkbox" class="html-checkboxes" name="status[]" value="Paid"<?php if(in_array('Paid',$var["status"])){ ?> checked="checked" <?php } ?>/>Paid</label>&nbsp;</td>
<td><input type="checkbox" class="html-checkboxes" name="status[]" value="Refunded"<?php if(in_array('Refunded',$var["status"])){ ?> checked="checked" <?php } ?>/>Refunded&nbsp;</td>
<td><input type="checkbox" class="html-checkboxes" name="status[]" value="Canceled" <?php if(in_array('Canceled',$var["status"])){ ?> checked="checked" <?php } ?>/>Canceled&nbsp;</td>
</tr></table></td>
    </tr>
    <tr style="background-color:transparent">
     <td  class="border">
	 Currency:
	    <select name="currency">
	     <option value="A" selected="selected">All</option>
	     <?php foreach ($settings['payment']['currencies'] as $id => $data) {?>
		 <option value="<?php echo $id?>" <?php if( $var["currency"] == $id){ ?> selected="selected" <?php } ?> ><?php echo $data['name']?> (<?php echo $data['symbol']?>)</option>	
	     <?php } ?>
	    </select>
     Processor:
	    <select name="proid">
	    <option value="" >All</option>
	    <?php foreach ($processors as $id => $value) {?>
		<option value="<?php echo $id?>" <?php if( $var["proid"] == $id){ ?> selected="selected" <?php } ?> ><?php echo $value; ?></option>	 
	    <?php } ?>
	    </select></td>
     <td align="left" class="border">
	 Items Per Page: 
	 <select class="select" ><option selected="selected" value="10">10</option>
     <option value="25">25</option>
     <option value="50">50</option>
     <option value="100">100</option>
     </select>
     &nbsp;&nbsp;
     Go To: 
	 <select class="select" >
	 <option value="1" selected="selected">1</option>
     </select>
	 <input  class="button-alt-sml" type="button" name="clear"  value="Clear Filters" onclick="window.location.href='?mod=wallet&go=withdrawals'"  />
	 <input  class="button-alt-sml" type="submit" name="submit"  value="Search"  />
</td>
    </tr>
  </table></form>
</div>


<form  method="post" action="" id="withdrawals">
<input type="hidden" name="update" value="y" />
<table  cellpadding="0" cellspacing="0"  class="tableS">
 <thead>
  <tr>
    <th class="left">ID #</th>
	<th class="left">Date</th>
    <th class="left">Username</th>
    <th class="left">Amount</th>
    <!--<th class="left">Fee</th>-->
    <th class="left">Processor</th>
    <th class="left">Processor I.D</th>
    <th class="left">Details</th>
	<th >Status</th>
    <th >Action</th>
	<th >Select</th>
 
  </tr>
  </thead>
  
  <?php foreach ($withdrawals as $num => $row){ ?>
   
  <tr>
     
    <td align="center"><?php echo $row['id']; ?></td>
	<td align="left"><?php echo $row['adddate']; ?></td>
    <td align="left"><?php echo $row['username']; ?></td>
    <td align="left" nowrap="nowrap"><?php echo $settings['payment']['currencies'][$row['currency']]['symbol'] ?><?php echo number_format($row['amount'],2); ?></td>
    <!--<td align="left" nowrap="nowrap"><?php echo $settings['payment']['currencies'][$row['currency']]['symbol'] ?><?php echo number_format($row['fee'],2); ?></td>-->
    <td align="left"><?php echo $row['processor']; ?></td>
    <td align="left"><?php echo $row['account']; ?></td>
    <td align="left"><input name="details[<?php echo $row['id'] ?>]" type="text" class="input" size="10"   value="<? echo $row["details"]?>"/></td>
	<td align="center"><?php echo $row['status']; ?>
	<!--<select name="status[<?php echo $row['id'] ?>]">
           <option value="Pending" <?php if($row['status'] == 'Pending')  echo "selected"?>>Pending</option>
           <option value="Paid" <?php if($row['status'] == 'Paid')  echo "selected"?>>Paid</option>
           <option value="Refunded" <?php if($row['status'] == 'Refunded')  echo "selected"?>>Refunded</option>  
		   <option value="Deleted" <?php if($row['status'] == 'Deleted')  echo "selected"?>>Deleted</option>
		   <option value="Canceled" <?php if($row['status'] == 'Canceled')  echo "selected"?>>Canceled</option>   					  
    </select>-->
    </td>
	<td align="center"><a href="?mod=wallet&go=withdrawal_details&wid=<?php echo $row["id"]?>" target="_blank">Details</a></td>
	<td align="center">
	
	  <input type="checkbox" name="list[]" value="<? echo $row["id"]?>">
	  <input type="hidden" name="amount[]" id="amount_<? echo $row["id"]?>" value="<? echo $row["amount"]?>">
	  <input type="hidden" name="currency[]" id="currency_<? echo $row["id"]?>" value="<? echo $row["currency"]?>">
	  <input type="hidden" name="method[]" id="method_<? echo $row["id"]?>" value="<? echo $row["proid"]?>">
	  <input type="hidden" name="status[]" id="status_<? echo $row["id"]?>" value="<? echo $row["status"]?>">
	  
	</td>
  </tr>
	   <?php } ?>
 
       <?php if(count($withdrawals) == 0) { ?><tr> <td colspan="12"  align="center">No withdrawal found</td></tr><?php } ?>

     <tr>
	    <td colspan="11" align="center"> <?php echo $system->getPaginationString($page,$num_rows,$per_page,3,$system->curPageURL(),"page");?></td>
	 </tr>
          
 
  <tr>
    <td colspan="11"  align="center">
      <input type=button value="Check All" onclick="this.value=check(this.form.checkbox)" id="button" />
     <!-- <input type="submit" name="submit" value="Update" id="button"/>-->
	  <input type="submit" name="submit" value="Set as Paid" id="button"/>
	  <input type="submit" name="submit" value="Refund" id="button"/>
	  <input type="submit" name="submit" value="Delete"  onclick="return(confirm('Do you really want to delete?'))" id="button"/>
   </td>
  </tr>
 
  <thead>
  <tr>
    <th class="left" colspan="11"><a href="#" onclick="toggle_masspay()">Make Mass Payment</a></th>
  </tr>
  </thead>
  <tr id="r_masspay" class="hidden">
  <td colspan="11">
  <?php foreach($processors2 as $id => $name) { ?>
  <input type="submit" name="submit" value="Make Mass Payment" id="button" onclick="return(masspay(<?php echo $id;?>))"/> -->  with <?php echo $name;?> <br />
   
  <?php } ?>
  </td>
  <!-- <td class="left" colspan="6"></td>-->
  </tr>
 <!-- <thead>
  <tr>
    <th class="left" colspan="12">Mass Payment File</th>
  </tr>
  </thead>
  <?php foreach($processors3 as $id => $name) { ?>
  <tr>
    <td><?php echo $id;?></td>
    <td><?php echo $name;?></td>
    <td class="left" colspan="10"><input type="submit" name="submit" value="Generate File" id="button" onclick="return(masspayfile(<?php echo $id;?>))"/></td>
  </tr>
  <?php } ?>-->
  
  </table>
</form>
<script language="javascript">
<!-- Begin

    var checkflag = "false";

    function check(field) {
    var checks = document.getElementsByName('list[]');
    if (checkflag == "false") {
     for (i = 0; i < checks.length; i++){
      checks[i].checked = true;
     }
      checkflag = "true";
      return "Uncheck All";
   }
   else {
     for (i = 0; i < checks.length; i++) {
      checks[i].checked = false; }
      checkflag = "false";
      return "Check All"; 
  }
}
//  End -->

function toggle_masspay()
{ 
	var elm = document.getElementById('r_masspay');
	if (elm.className == 'hidden')
	{
		elm.className = "";
	}
	else
	{
		elm.className = "hidden";
	}
}


function masspay(processor) {
   
    var id;
	var amount;
	var method;
	var currency = '';
	var sum = 0;
	var checks = document.getElementsByName('list[]');
	var currencies = document.getElementsByName('currency[]');
	
	
    for (i = 0; i < checks.length; i++)
	{
       if(checks[i].checked == true){
	     id = checks[i].value;
	     amount = document.getElementById('amount_'+id).value;
	     method = document.getElementById('method_'+id).value;
		 status = document.getElementById('status_'+id).value;
		 if(currency == '') currency = document.getElementById('currency_'+id).value;
		 if(currency != document.getElementById('currency_'+id).value){alert('All currencies must be the same');return false;}
	     if(method != processor){alert('All methods must be the same'); return false;}
		 if(status != 'Pending'){alert('One or more of the rows selected is not a pending withdrawal'); return false;}
		 sum += parseFloat(amount);
	   } 
	   
	}
	 if(sum <= 0){alert('Nothing to payout');return false;}
	 if(confirm('Total amount that will be paid out is $'+sum))
	 {
	   var masspay = document.createElement("input");
       masspay.setAttribute("type","hidden");
       masspay.setAttribute("name","masspay");
       masspay.setAttribute("value",processor);
	   document.getElementById('withdrawals').appendChild(masspay);
	   return true;
	 }
     return false; 
}


function masspayfile(processor) {
   
    var id;
	var amount;
	var method;
	var currency = '';
	var sum = 0;
	var checks = document.getElementsByName('list[]');
	var currencies = document.getElementsByName('currency[]');
	
	
    for (i = 0; i < checks.length; i++)
	{
       if(checks[i].checked == true){
	     id = checks[i].value;
	     amount = document.getElementById('amount_'+id).value;
	     method = document.getElementById('method_'+id).value;
		 status = document.getElementById('status_'+id).value;
		 if(currency == '') currency = document.getElementById('currency_'+id).value;
		 if(currency != document.getElementById('currency_'+id).value){alert('All currencies must be the same');return false;}
	     if(method != processor){alert('All methods must be the same'); return false;}
		 if(status != 'Pending'){alert('One or more of the rows selected is not a pending withdrawal'); return false;}
		 sum += parseFloat(amount);
	   } 
	   
	}
	 if(sum <= 0){alert('Nothing to payout');return false;}
	 if(confirm('Total amount that will be paid out is $'+sum))
	 {
	   var masspayfile = document.createElement("input");
       masspayfile.setAttribute("type","hidden");
       masspayfile.setAttribute("name","masspayfile");
       masspayfile.setAttribute("value",processor);
	   document.getElementById('withdrawals').appendChild(masspayfile);
	   return true;
	 }
     return false; 
}

<?php if($var['masspay'] != ''){ ?> toggle_masspay(); <?php }?>

</script>

