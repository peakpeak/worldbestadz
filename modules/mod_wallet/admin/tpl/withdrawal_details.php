<h1>Withdrawal Details</h1>

<p class="info">Here you can view withdrawal details</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>
	
	
	<form  action="" method="post">
    <input type="hidden" name="wid" value="<?php echo $var['wid']?>" />
	<table cellpadding="0" cellspacing="0"   class="tableS">
	<thead>
	<tr>
		<th colspan="2" align="left">Withdrawal Details</th>	
	</tr>
	</thead>
	 <?php if(count($withdrawal) == 0) : ?>
	 
      <tr>
		<td colspan="2"> - no records found - </td>
	  </tr>
	  <?php else: ?> 
      <tr>
						
						<td width="30%">Username</td>
                        <td nowrap="nowrap"><?php echo $withdrawal["username"]?></td>
		</tr>
        <tr>
						
						<td>Amount</td>
                        <td nowrap="nowrap"><?php echo $settings['payment']['currencies'][ $withdrawal['currency']]['symbol']; ?><?php echo  $withdrawal['amount'] ?></td>
		</tr>
		 <tr>
						
						<td>Fee</td>
                        <td nowrap="nowrap"><?php echo $settings['payment']['currencies'][ $withdrawal['currency']]['symbol']; ?><?php echo  $withdrawal['fee'] ?></td>
		</tr>
        <tr>
						<td>Description</td>
                        <td><?php echo $withdrawal["description"]?></td>
	   </tr>
	  <tr>
						<td>Payment method</td>
                        <td><?php echo $withdrawal["processor"]?></td>
	   </tr>
	   <tr>
						<td>Account details</td>
                        <td><?php
						echo "Account ID:".$withdrawal["account"]["id"]."<br>"; 
						if(is_array($withdrawal["account"]["description"]))foreach($withdrawal["account"]["description"] as $id => $description)
						echo $description.":".$withdrawal["account"]["values"][$id]."<br>";?>
						</td>
	   </tr>
	   
						<td>Payee's comment</td>
                        <td><?php echo $withdrawal["payee_comments"]?></td>
	   </tr>
	    <tr>
		
						<td>Status</td>
                        <td>
                           <select name="status">
                               <option value="Pending" <?php if($withdrawal["status"] == 'Pending')  echo "selected"?>>Pending</option>
                               <option value="Paid" <?php if($withdrawal["status"] == 'Paid')  echo "selected"?>>Paid</option>
                               <option value="Refunded" <?php if($withdrawal["status"] == 'Refunded')  echo "selected"?>>Refunded</option>                           
							   <option value="Canceled" <?php if($withdrawal["status"] == 'Canceled')  echo "selected"?>>Canceled</option>
							   <option value="Deleted" <?php if($withdrawal['status'] == 'Deleted')  echo "selected"?>>Deleted</option>   						                         </select>
					</td>
	    </tr>
	   <tr>
	   
	   <tr>
						<td>Payment details</td>
                        <td><textarea rows="2"  name="details" ><?php echo $withdrawal["details"]?></textarea></td>
	   </tr>
       
		 <tr>
						<td>Admin comment</td>
                        <td><textarea rows="2"  name="comments"><?php echo $withdrawal["payer_comments"]?></textarea></td>
	   </tr>
        <tr>
            <td colspan="2" align="left"><input type="submit" name="submit" <?php echo ($withdrawal["status"] == 'Completed')? "disabled ='disabled'":"" ?>value="Update" class="button_link">	</td>
        </tr>
     <?php endif; ?> 
	</table>
   
</form>

	