<?php 
    
   global $system,$db,$userinfo,$settings;	

   $content = $system->importClass('content');

   $var = $system->getVar();

   $page = (isset($var['page']))? $var['page']:$content->homePage();

   $sql  = "SELECT *,AES_DECRYPT(content,'".ENCKEY."') as content FROM ".PREFIX."_content_pages ";

   $sql .= "WHERE name ='".$db->sql_quote($page)."' LIMIT 1";
 
   $result = $db->query_db($sql,$print = DEBUG);

   if ($db->num_rows($result) == 0 )$system->invalidRequest();

   $row = $db->fetch_db_array($result);
   
   if($row['privilege'] == 1)  $system->redirect(SITEURL.FS."members".FS."pages".FS.$page);
   
   $pageInfo['title'] = $row['title'];
   
   $pageInfo['meta'] = array(
                      'description'=> $row['description'],
		              'keywords'=>$row['keywords'], ); 
   
  
   $loader = new Loader($row['template']);
   
   $data = get_defined_vars();
   
   $tpl_file =  $row['content']; 
   
   $loader->setVar($data);
   
   $loader->mainHeader();
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter();
   
   $loader->displayOutput();
?>
