<?php
 //$mid,$css_class,$css_id
 global $system,$userinfo,$db,$settings;
 $content = $system->importClass('content');
 $current = $system->curPageURL();
 $menu = $content->getMenuArray($mid); 
?>

<ul id="<?php echo $css_id?>" class="<?php echo $css_class?> nav">

<?php  $i = 0; foreach($menu as $id => $data){ $i++;

      $active_class = ""; //($data["url"] == $current) ? "active ":"";
	  $last_class = ($i == (count($menu))) ? "last":"";
	  $class = ($active_class != "" || $last_class != "") ? "class = \"".$active_class . $last_class."\"":"";
	  
?>  
   <li <?php echo $class;?>>
     <a href="<?php echo $data["url"] ?>" title="<?php echo $data["title"] ?>"><span><?php echo $data["name"] ?></span></a>
   </li>
<?php }?>



</ul>

