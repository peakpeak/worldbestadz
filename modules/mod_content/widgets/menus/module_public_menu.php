<?php
 global $system,$userinfo,$db,$settings;
 $system->importClass('content');
 $menu = Content::getModuleMenu('public');
?>
<ul>
	<?php foreach ($menu as $title => $link) : ?>
	  <li><a href="<?php echo $link ?>"><?php echo $title ?></a></li> 
	<?php endforeach; ?>

</ul>
