<?php
   global $system,$userinfo,$db,$settings;

   $system->importClass('Content');
   
   $var = $system->getVar();
   
   if($var['mod'] == 'content'){
   
	   $list = Content::getPageParentTree($var['page']);
	   
	   foreach( $list as $name)$breadcrumbs[$name] = "?mod=content&page=$name";
 
   }
   else
   {
   
      $breadcrumbs = urldecode($breadcrumbs);
   
      $breadcrumbs = $system->createArray($breadcrumbs);
   
      $breadcrumbs = $breadcrumbs['breadcrums'];
   
   }
   
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'index.php';
   
   $loader->setVar($data);
   
   $loader->loadOutput($tpl_file);
   
   $loader->displayOutput();
?>