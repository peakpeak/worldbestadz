<span class="form_title">Questions</span>
<?php if($error_msg){ ?>
<span class="error_message"><?php echo $error_msg;?></span>
<? } ?>
<?php if($success_msg){ ?>
<span class="success_message"><?php echo $success_msg;?></span>
<? } ?>
<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td width="430" height="200" valign="top"><div class="white">
        <?php  foreach ($faqs as $num => $data) { ?>
        <a href="#Q<?php echo $num?>"><?php echo $data["question"] ?></a><br />
        <?php } ?>
        <?php if(count($faqs) == 0) { ?>
        - No questions found -
        <?php } ?>
      </div>
      <?php  foreach ($faqs as $num => $data) { ?>
      <div class="white"> <b><a name="Q<?php echo $num?>"><?php echo $data["question"] ?></a></b><br />
        <br />
        <?php echo $data["answer"] ?></a> </div>
      <?php } ?>
    </td>
  </tr>
</table>
