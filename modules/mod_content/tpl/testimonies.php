<span class="form_title">Testimonies</span>
<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td width="430" height="200" valign="top"><?php  $i = 0; foreach ($testimonies as $num => $data) { $i++;?>
      <div class="white"> <span class="floatimgleft"><img src="<?php echo $data['image']; ?>"></span> <b><?php echo $data['username']; ?></b><br>
        <?php echo $data['content']; ?>
        <p align="right"><i><?php echo $data['date']; ?></i></p>
      </div>
      <?php } ?>
      <?php if(count($testimonies) == 0) : ?>
      <div class="white">No record found</div>
      <?php endif; ?>
      <?php if($num_rows > $per_page) : ?>
      <div class="white"><?php echo $system->getPaginationString($page,$num_rows,$per_page,3,$system->curPageURL(),"page");?></div>
      <?php endif; ?>
    </td>
  </tr>
</table>
