<span class="form_title">Announcements</span>
<?php if($error_msg){ ?>
<span class="error_message"><?php echo $error_msg;?></span>
<? } ?>
<?php if($success_msg){ ?>
<span class="success_message"><?php echo $success_msg;?></span>
<? } ?>
<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td width="430" height="200" align="center" valign="top"><div class="white">
        <table cellspacing=1 cellpadding=2 border=0 width=100% align="center" class="table_2">
          <thead>
            <tr>
              <th>#</th>
              <th><b>Date</b></th>
              <th><b>Title</b></th>
            </tr>
          </thead>
          <?php $i = 0; foreach ($announcements as $num => $data) { $i++; ?>
          <tr>
            <td align="center">#<?php echo $i; ?></td>
            <td><?php echo $data['date']; ?></td>
            <td><a id="popup<?php echo $i?>" href="{#loader=system::url}/content/announcement_details/aid/<?php echo $data['id']; ?>"><?php echo $data['title']; ?></a></td>
          </tr>
          <?php } ?>
          <?php if(count($announcements) == 0) { ?>
          <tr>
            <td colspan=3 align=center>No announcement found!</td>
          </tr>
          <?php } ?>
          <?php if($num_rows > $per_page) { ?>
          <tr>
            <td colspan=3 align="center"><?php echo $system->getPaginationString($page,$num_rows,$per_page,3,$system->curPageURL(),"page");?></td>
          </tr>
          <?php } ?>
        </table>
      </div></td>
  </tr>
</table>
<script type="text/javascript">
 
   
	 var first = 0;
	 var last = <?php echo $i;?>;	
	  fancyjQuery(document).ready(function() {
			
			for (i = first; i  <= last; i++) {
			
			fancyjQuery("#popup"+i).fancybox({
				'width'				: '60%',
				'height'			: '100%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
          }
		
		});
  </script>
