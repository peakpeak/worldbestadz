<?php
  global $system,$db,$settings;

  $var = $system->getVar();
  
  $per_page = $settings['system']['rows_per_page'];

  $sql  = "SELECT * FROM ".PREFIX."_content_testimonies ";
  
  $sql .= "WHERE status = 'Approved' ";
  
  $sql .= "ORDER BY `date` DESC";
 
  $result = $db->query_db($sql,$print = DEBUG);


			$page		 = $system->getVar('page');	
			$num_rows 	 = $db->num_rows($result);
			if(!empty($page)) 
			{
				$start = ($page - 1) * $settings['system']['rows_per_page']; 
			}			
			else
			{
				$start 	= 0;
				$page	= 1;
			}				

		
  $sql .= " LIMIT $start, $per_page "; 
  
  $result = $db->query_db($sql,$print = DEBUG);
  if ($db->num_rows($result) > 0)
  {
	 	$rows = $start;
	 	while ($row = $db->fetch_db_array($result))
		{	
			$rows++;
			$row["image"] = Account::userImageSmall($row["username"]);
		    $testimonies[$rows] = $row;	
					
		}
  }
  
   $pageInfo['title'] = 'Testimonies';
  
   $pageInfo['map'] = array('Testimonies' => '',);
   
   $loader = new Loader;
   
   $data= get_defined_vars();
   
   $loader->setVar($data);
   
   $loader->mainHeader();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'testimonies.php';
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter();
   
   $loader->displayOutput();
   
 ?>

 