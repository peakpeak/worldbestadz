<?php
global $system,$db,$settings,$validate;

$system->importClass('account')->checkPrivilege(3);

$content = $system->importClass('content');

$var = $system->getVar();


if(!isset($var['fid']))$system->invalidRequest();


if ($var['submit'] == 'Edit')
{
	
	//Validate
    if ($var['question'] == NULL)
	{
		$return_msg = "Please add a question";	
	}	
    elseif ($var['safe_input']['answer'] == NULL)
	{
		$return_msg = "Answer field cannot be blank";	
	}
	else
	{
      $answer = "AES_ENCRYPT('".$var['safe_input']['answer']."','".ENCKEY."')";
      $table = "_content_faqs";
	  $db->noquote = array(1);
      $db->noslash = array(1);
	  
      $update_array = array( 
	  'question'=>$var['question'],
	  'answer'=>$answer,
	  );
	  
	  
	  $result = $db->update_db($update_array,$table,"id",$var['fid']);
      if($result) $return_msg = "Faq Edited";

    }

}
$sql = "SELECT *, AES_DECRYPT(answer,'".ENCKEY."') as answer FROM ".PREFIX."_content_faqs WHERE id='".$var['fid']."' LIMIT 1";
$res = $db->query_db($sql,$print = DEBUG);  
if($db->num_rows($res) == 0)$system->invalidRequest();
$row = $db->fetch_db_array($res);


   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'edit_faq.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();
?>
