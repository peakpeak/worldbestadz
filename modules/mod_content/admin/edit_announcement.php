<?php
global $system,$db,$settings,$validate;

$system->importClass('account')->checkPrivilege(3);

$content = $system->importClass('content');

$var = $system->getVar();


if(!isset($var['aid']))$system->invalidRequest();


if ($var['submit'] == 'Edit')
{
	
	//Validate
   if ($var['title'] == NULL)
	{
		$return_msg = "Please add a title";	
	}	
    elseif ($var['safe_input']['content'] == NULL)
	{
		$return_msg = "Content field cannot be blank";	
	}
    elseif ($var['type'] == NULL)
	{
		$return_msg = "Please choose a type";	
	}
	else
	{
      $content = "AES_ENCRYPT('".$var['safe_input']['content']."','".ENCKEY."')";
	   
      $table = "_content_announcements";
	  $db->noquote = array(0);
      $db->noslash = array(0);
	  
      $update_array = array(
	  'content'=>$content,
	  'title'=>$var['title'],
	  'content'=> $content,
	  'type'=>$var['type'],
	  'comments'=>$var['comments'],
	  );
	  
	  
	  $result = $db->update_db($update_array,$table,"id",$var['aid']);
      if($result) $return_msg = "Announcement Edited";

    }

}
$sql = "SELECT *, AES_DECRYPT(content,'".ENCKEY."') as content FROM ".PREFIX."_content_announcements WHERE id='".$var['aid']."' LIMIT 1";
$res = $db->query_db($sql,$print = DEBUG);  
if($db->num_rows($res) == 0)$system->invalidRequest();
$row = $db->fetch_db_array($res);

   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'edit_announcement.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();
?>
