<?php
global $system,$db,$settings,$validate;

$system->importClass('account')->checkPrivilege(3);

$system->importClass('content');

$var = $system->getVar();

if(!isset($var['mid']))$system->invalidRequest();

$pages = Content::getPages();

$links = array();

if ($var['submit'])
{
	
	//Validate
	switch($var['submit']){
	
	case 'Add':
	
	if ($var['name'] == NULL)
	{
		$errors[] = "Please add a name";	
		
	}
	if ($var['title'] == NULL)
	{
		$errors[] = "Please add a title";	
	}
	if ($var['type'] == NULL)
	{
		$errors[] = "Please choose a type";	
	}
	if ($var['type'] == "page" && !is_numeric($var['page_id']))
	{
		$errors[] = "Please choose a page";	
	} 
	if ($var['type'] == "url" && $var['url'] == "")
	{
		$errors[] = "Please add a url";	
	}
	if(count($errors) > 0)
	{
	  while (list($key,$value) = each($errors)) $return_msg.= $value.'<br>';
	 
	}
	else
	{	
		if($var['type'] == "page") $link = $var['page_id']; elseif($var['type'] == "url") $link = $var['url'];
		$added = Content::addMenuLink($var['mid'],$var['name'],$var['title'],$var['after'],$link,$var['type']);	
		if($added) $return_msg = "Link added";   
       
	}
	
	break;
	case 'Edit':
	if ($var['name'] == NULL)
	{
		$errors[] = "Please add a name";	
		
	}
	if ($var['title'] == NULL)
	{
		$errors[] = "Please add a title";	
	}
	if ($var['type'] == NULL)
	{
		$errors[] = "Please choose a type";	
	}
	if ($var['type'] == "page" && !is_numeric($var['page_id']))
	{
		$errors[] = "Please choose a page";	
	} 
	if ($var['type'] == "url" && $var['url'] == "")
	{
		$errors[] = "Please add a url";	
	}
	if(count($errors) > 0)
	{
	  while (list($key,$value) = each($errors)) $return_msg.= $value.'<br>';
	 
	}
	else
	{	
		if($var['type'] == "page") $link = $var['page_id']; elseif($var['type'] == "url") $link = $var['url'];
		$edited = Content::editMenuLink($var['lid'],$var['name'],$var['title'],$var['after'],$link,$var['type']);	
		if($edited) { $return_msg = "Link edited"; unset($var['edit']);}   
       
	}
	
	break;
	}
	//switch($var['submit']){}
	
}


if($var['delete'] == 'y' && $var['lid'] != '')Content::deleteMenuLink($var['lid']);


$sql = "SELECT *  FROM ".PREFIX."_content_menus ";
$sql.= "WHERE id ='".$var['mid']."' LIMIT 1 ";
$result = $db->query_db($sql,$print = DEBUG);
if($db->num_rows($result) == 0)$system->invalidRequest();
$menu = $db->fetch_db_array($result);	


$sql = "SELECT *  FROM ".PREFIX."_content_menu_elements ";
$sql.= "WHERE menu_id ='".$var['mid']."' ORDER by `order` ASC ";
$res = $db->query_db($sql,$print = DEBUG);
if($db->num_rows($res) > 0) while ($row = $db->fetch_db_array($res)) {

 if($row["type"] == "page") {
	  $prow = $db->get_row("id",$row['page_id'],"_content_pages");
	  $row["url"] = SITEURL.FS."pages/".$prow["name"];
 }
 else{
        $tags = array('{websiteurl}'=>SITEURL);  
	    $row["url"] = $system->replaceTags($tags,$row["url"]);
 }
 $links[$row['id']] = $row;
}			
		
if($var['edit'] == 'y' && isset($var['lid'])) {

$sql = "SELECT * FROM ".PREFIX."_content_menu_elements WHERE id = '".$var['lid']."' LIMIT 1";
$res = $db->query_db($sql,$print = DEBUG);
if ($db->num_rows($res) > 0) $erow = $db->fetch_db_array($res);

}

   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $loader->setVar($data);
   
   $tpl_add_file = dirname(__FILE__).DS.'tpl'.DS.'add_link.php';
   
   $tpl_edit_file = dirname(__FILE__).DS.'tpl'.DS.'edit_link.php';
   
   $loader->placeHolder['form'] =  ($var['edit'] == 'y' && isset($var['lid'])) ?  $tpl_edit_file :  $tpl_add_file;
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'links.php';
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();
?> 
