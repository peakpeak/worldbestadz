<?php
global $system,$db,$settings,$validate;

$system->importClass('account')->checkPrivilege(3);

$content = $system->importClass('content');

$var = $system->getVar();


if ($var['submit'] == 'Add')
{
	
	//Validate
   if ($var['question'] == NULL)
	{
		$return_msg = "Please add a question";	
	}	
    elseif ($var['safe_input']['answer'] == NULL)
	{
		$return_msg = "Answer field cannot be blank";	
	}
   
	else
	{
      $answer = "AES_ENCRYPT('".$var['safe_input']['answer']."','".ENCKEY."')";
      $table = "_content_faqs";
	  $db->noquote = array(1);
      $db->noslash = array(1);
	  
      $insert_array = array( 
	  'question'=>$var['question'],
	  'answer'=>$answer,
	  );
	  
	  $result = $db->insert_into_db($insert_array,$table);
      if($result) $return_msg = "FAQ added";

    }

}

   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'add_faq.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();
?>
