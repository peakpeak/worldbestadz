<?php
global $system,$db,$settings,$validate;

$system->importClass('account')->checkPrivilege(3);

$content = $system->importClass('content');

$var = $system->getVar();


if ($var['submit'] == 'Add')
{
	
	//Validate
   if ($var['title'] == NULL)
	{
		$return_msg = "Please add a title";	
	}	
    elseif ($var['safe_input']['content'] == NULL)
	{
		$return_msg = "Content field cannot be blank";	
	}
    elseif ($var['type'] == NULL)
	{
		$return_msg = "Please choose a type";	
	}
	else
	{
      $content = "AES_ENCRYPT('".$var['safe_input']['content']."','".ENCKEY."')";
      $table = "_content_announcements";
	  $db->noquote = array(0,1);
      $db->noslash = array(0);
      $insert_array = array(
	  'content'=>$content,
	  'date'=>"now()",
	  'title'=>$var['title'],
	  'type'=>$var['type'],
	  'comments'=>$var['comments'],
	  );
	  $result = $db->insert_into_db($insert_array,$table);
      if($result) $return_msg = "Announcement added";

    }

}
   $menus = Content::getMenus();

   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'add_announcement.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();
   
?>
