<h1>Manage Announcements</h1>

<p class="info">Here you can manage announcements</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>
	  
<h2><span><a href="?mod=content&go=add_announcement" class="button-alt-sml" >Add Announcement</a></span><br /></h2>

      <table cellpadding="0" cellspacing="0"  class="tableS">
	  <thead>
       <tr>
         <th class="left">Date</th>
		 <th class="left">Title</th>
		 <th class="left">Type</th>
         <th class="right">&nbsp;</th> 
      </tr>
	  </thead>
	  
	 <?php $i = 0; foreach ($announcements as $num => $row){  $i++;?>
	  
       <tr>
          <td><?php echo $row['date']?></td>
		  <td>
		  <?php if($row['type'] == 'Public') { ?>
		  <a href="{#loader=system::url}/content/announcement_details/aid/<?php echo $row['id'];?>" id="popup<?php echo $i?>"><?php echo $row['title']?></a>
		  <?php } else {  ?>
		   <a href="{#loader=system::url}/members/content/announcement_details/aid/<?php echo $row['id'];?>" id="popup<?php echo $i?>"><?php echo $row['title']?></a>
		  <?php } ?>
		  </td>
		  <td><?php echo $row['type']?></td>
          <td class="right"><a href="?mod=content&go=edit_announcement&aid=<?php echo $row['id']; ?>"><img src="images/edit.png" class="tooltip img-wrap2"  alt="" title="Edit"/></a> <a href="?mod=content&go=announcements&delete=y&aid=<?php echo $row['id'];?>" onclick="return(confirm('Do you really want to delete?'))"><img src="images/delete.png" class="tooltip img-wrap2"  alt="" title="Delete"/></a></td>
      </tr>
      
	  <?php } ?>
	  
	  <?php if(count($announcements) == 0) { ?> <tr> <td colspan="4" align="center">No announcements found</td> </tr> <?php } ?>
	 
	 
	 
     </table>
 <script type="text/javascript">
 
   
	 var first = 0;
	 var last = <?php echo $i;?>;	
	  fancyjQuery(document).ready(function() {
			
			for (i = first; i  <= last; i++) {
			
			fancyjQuery("#popup"+i).fancybox({
				'width'				: '100%',
				'height'			: '100%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
          }
		
		});
  </script>




