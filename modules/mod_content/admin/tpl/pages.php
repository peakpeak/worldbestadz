<h1>Manage Pages</h1>

<p class="info">Here you can manage custom pages</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>
	  
<h2><span><a href="?mod=content&go=add_page" class="button-alt-sml">Add Page</a></span><br /></h2>

      <table cellpadding="0" cellspacing="0"  class="tableS">
	  <thead>
       <tr>
         <th class="left">Name</th>
		 <th class="left">Title</th>
         <th class="right">&nbsp;</th> 
      </tr>
	  </thead>
	  
	  <?php foreach ($pages as $num => $row){ ?>
      
       <tr>
          <td>
		     <?php if($row['privilege'] == 0) { ?>
		     <a href="{#loader=system::url}/pages/<?php echo $row['name']?>" target="_blank"><?php echo $row['name']?></a>
			 <?php } else {  ?>
			 <a href="{#loader=system::url}/members/pages/<?php echo $row['name']?>" target="_blank"><?php echo $row['name']?></a>
			 <?php } ?>
		  </td>
		  <td>
		     <?php if($row['privilege'] == 0) { ?>
			 <a href="{#loader=system::url}/pages/<?php echo $row['name']?>" target="_blank"><?php echo $row['title']?></a>
		     <?php } else {  ?>
			 <a href="{#loader=system::url}/members/pages/<?php echo $row['name']?>" target="_blank"><?php echo $row['title']?></a>
			 <?php } ?>  
		  </td>
          <td class="right"><a href="?mod=content&go=edit_page&pid=<?php echo $row['id']; ?>" id="popup<?php echo $i?>"><img src="images/edit.png" class="tooltip img-wrap2"  alt="" title="Edit"/></a> <a href="?mod=content&go=pages&delete=y&pid=<?php echo $row['id'];?>" onclick="return(confirm('Do you really want to delete?'))"><img src="images/delete.png" class="tooltip img-wrap2"  alt="" title="Delete"/></a></td>
      </tr>
	  
      <?php } ?>
	  
      <?php if(count($pages) == 0) { ?>   <tr>  <td colspan="3" align="center">No pages found</td>  </tr> <?php } ?>
	  
     </table>
