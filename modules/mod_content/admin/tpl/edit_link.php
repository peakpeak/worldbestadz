<form  method="post" action="">
<input type="hidden" name="edit" value="y" />
<input type="hidden" name="lid" value="<?php echo $var['lid']?>" />
<input type="hidden" name="mid" value="<?php echo $var['mid']?>" />
<table  cellpadding="0" cellspacing="0"  class="tableS">
 <thead>
  <tr >
    <th colspan="2"  class="left"><a href="?mod=content&go=menus">Manage Menus</a> > <a href="?mod=content&go=links&mid=<?php echo $var['mid']?>"><?php echo $menu["name"]?></a> > Edit Link > <?php echo $erow['name']?></th>
  </tr>
  </thead>
   <tr>
    <td width="30%">Add Link</td>
	 <td><select name="after" >
	        <option value="index"  <?php if($erow['order'] == 1 ){ ?> selected="selected"<?php } ?>>At the top</option> 	
	        <?php foreach ($links as $num => $link) : ?> 
			<option value="<?php echo $link['name']?>" <?php if($erow['order']-1 == $link["order"]) { ?> selected="selected"<?php } ?>>After <?php echo $link['name']?></option> 
			<?php  endforeach; ?>
          </select>	 
	</td>
  </tr>
  <tr>
    <td>Name</td>
	<td><input type="text" name="name"  value="<?php echo $erow['name'] ?>" /></td>
  </tr>
  <tr>
    <td>Title</td>
	<td><input type="text" name="title"   value="<?php echo $erow['title'] ?>"/></td>
  </tr>
  <tr>
    <td>Type</td>
	  <td><select name="type" id="type" onchange="set_link()">	
			<option value="page" <?php if($erow['type'] == "page") {?> selected="selected" <?php }?>> Custom Page </option>
            <option value="url" <?php if($erow['type'] == "url") {?> selected="selected" <?php }?>> URL </option>
         </select>
	 </td>
  </tr>
   <tr id="page">
    <td>Choose Page</td>
	  <td><select name="page_id" >	
	  		<option value=""> - Select - </option>
            <?php foreach ($pages as $id => $name) : ?> 
			<option value="<?php echo $id?>"  <?php if($erow['page_id'] == $id) {?> selected="selected" <?php }?>><?php echo $name; ?></option> 
			<?php endforeach; ?>
         </select>
	 </td>
  </tr>
	<tr id="url">
    <td>URL</td>
	<td><input type="text" name="url"  value="<?php echo $erow['url'] ?>" /></td>
  </tr>
  
  <tr >
    <td colspan="2"  class="left"><input type="submit" name="submit" value="Edit" /></td>
  </tr>

  </table>
</form>
