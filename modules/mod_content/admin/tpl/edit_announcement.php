<h1>Manage Announcements</h1>

<p class="info">Here you can edit an announcement</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

<form name="addpage" action="" method="post">
<input type="hidden" name="aid"  value="<?php echo $row['id'] ?>" />
<table cellpadding="0" cellspacing="0"  class="tableS" >
  <thead>
  <tr>
    <th colspan="2" class="left"><a href="?mod=content&go=announcements">Manage Announcements</a> > Edit Announcment</th>
  </tr>
  </thead>
 <tr>
     <td>Type</td>
     <td><select name="type" >	
	  		<option value = "">-Select-</option>
            <option value="Public" <?php if($row['type'] == "Public")  echo "selected"?>>Public</option> 	
            <option value="private" <?php if($row['type'] == "Private")  echo "selected"?>>Private</option> 			
         </select>	  </td>
    </tr>
  <tr>
     <td>Allow Comments</td>
     <td><select name="comments" >	
            <option value="Y" <?php if($row['comments'] == "Y")  echo "selected"?>>Yes</option> 	
            <option value="N" <?php if($row['comments'] == "N")  echo "selected"?>>No</option> 			
         </select>	  </td>
    </tr>
  <tr>
    <td>Title </td>
	<td><input type="text" name="title" style="width:350px" value="<?php echo  $row['title']?>"/></td>
  </tr>
  <tr>
    <td align="left" colspan="2">
      <textarea name="safe_input[content]" rows="20" cols="20" style="width:100%; height:550px"  class="ckeditor" id="ckeditor"><?php echo  $row['content']?></textarea>
   </td>
  </tr>
   <tr>
    <td align="center" colspan="2">
	   <input type="button" name="preview" value="Preview"  onclick="" />
	   <input type="submit" name="submit" value="Edit"   />	  
   </td>
  </tr>
</table>
</form>

<script src="<?php echo SITEURL.FS.$settings['system']['plugins_directory']?>/ckeditor/ckeditor.js"></script>
<script src="<?php echo SITEURL.FS.$settings['system']['plugins_directory']?>/ckeditor/custom.js"></script>
<script type="text/javascript">CKEDITOR.replace( 'ckeditor', {toolbar: advanced});</script>
