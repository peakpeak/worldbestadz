<h1>Testimonies</h1>

<p class="info">Here you can manage testimonies added by members</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

<div class="box" style="margin-bottom:10px">
<form action="index.php" method="get">
<input  type="hidden" name="mod" value="content" />
<input  type="hidden" name="go" value="testimonies" />
  <table cellpadding="0" cellspacing="0" class="utility">
	<tr style="background-color:transparent">
     <td class="border">Username: <input type="text" name="username" value="<?php echo $var["username"]?>"  /></td>
	 <td align="left"><table cellpadding="0" cellspacing="0" border="0"> <tr>
	   <td>Status:</td>
	   <td><input type="checkbox" class="html-checkboxes" name="status[]" value="Pending"<?php if(in_array('Pending',$var["status"])){ ?> checked="checked" <?php } ?>/>Pending</td>
       <td><input type="checkbox" class="html-checkboxes" name="status[]" value="Approved"<?php if(in_array('Approved',$var["status"])){ ?> checked="checked" <?php } ?>/>Approved</td>    <td><input type="checkbox" class="html-checkboxes" name="status[]" value="Disapproved"<?php if(in_array('Disapproved',$var["status"])){ ?> checked="checked" <?php } ?>/>Dispproved</td>
</tr></table></td>
    </tr>
    <tr style="background-color:transparent">
     <td  class="border"> Content contains : <input type="text" name="content" value="<?php echo $var["content"]?>" size="60"  /></td>
     <td align="left" class="border">
	 Items Per Page: 
	 <select name="per_page">
	 <option value="10" <?php if( $var["per_page"] == '10'){ ?> selected="selected" <?php } ?>>10</option>
     <option value="25" <?php if( $var["per_page"] == '25'){ ?> selected="selected" <?php } ?>>25</option>
     <option value="50" <?php if( $var["per_page"] == '50'){ ?> selected="selected" <?php } ?>>50</option>
     <option value="100"<?php if( $var["per_page"] == '100'){ ?> selected="selected" <?php } ?>>100</option>
     </select>
     &nbsp;&nbsp;
     <!--Go To: 
	 <select class="select" >
	 <option value="1" selected="selected">1</option>
     </select>-->
	 <input  class="button-alt-sml" type="button" name="clear"  value="Clear Filters" onclick="window.location.href='?mod=content&go=testimonies'"  />
	 <input  class="button-alt-sml" type="submit" name="submit"  value="Search"  />
</td>
    </tr>
  </table></form>
</div>


<form  method="post" action="">
<table  cellpadding="0" cellspacing="0"  class="tableS">
 <thead>
  <tr>
    <th >No</th>
	<th class="left">Date</th>
    <th class="left">Username</th>
    <th class="left">Content</th>
	<th class="left">Status</th>
	<th >Select</th>
 
  </tr>
  </thead>
   <?php $i = 0; foreach ($testimonies as $num => $row){ $i++; ?> 
  <tr>
     
    <td align="center"><?php echo $num; ?></td>
	<td ><?php echo $row['date']; ?></td>
    <td ><?php echo $row['username']; ?></td>
    <td ><a href="?mod=content&go=testimony_details&tid=<? echo $row["id"]?>" id="popup<?php echo $i?>"><?php echo $row['content']; ?></a></td>
	<td ><?php echo $row['status'];?></td>
	<td align="center"><input type="checkbox" name="list[]" value="<? echo $row["id"]?>"></td>
  </tr>


  <?php } ?>
 
  <?php if(count($testimonies) == 0) { ?><tr><td colspan="8"  align="center">No testimonies found</td></tr><?php } ?> 
  	 
     <tr>
	    <td colspan="6" align="center"> <?php echo $system->getPaginationString($page,$num_rows,$per_page,3,$system->curPageURL(),"page");?></td>
	 </tr>

        

 
  <tr>
    <td colspan="8" ><div align="center">
      <input type=button value="Check All" onclick="this.value=check(this.form.checkbox)" id="button" />
	  <input type="submit" name="Submit" value="Approve" id="button"/>
	  <input type="submit" name="Submit" value="Disapprove" id="button"/>
	  <input type="submit" name="Submit" value="Delete" id="button" onclick="return(confirm('Do you really want to delete?'))"/>
	  
     </div>
   </td>
  </tr>
  </table>
</form>
<script language="javascript">
<!-- Begin

    var checkflag = "false";

    function check(field) {
    var checks = document.getElementsByName('list[]');
    if (checkflag == "false") {
     for (i = 0; i < checks.length; i++){
      checks[i].checked = true;
     }
      checkflag = "true";
      return "Uncheck All";
   }
   else {
     for (i = 0; i < checks.length; i++) {
      checks[i].checked = false; }
      checkflag = "false";
      return "Check All"; 
  }
}
//  End -->
</script>

<script type="text/javascript">
 
   
	 var first = 0;
	 var last = <?php echo $i;?>;	
	  fancyjQuery(document).ready(function() {
			
			for (i = first; i  <= last; i++) {
			
			fancyjQuery("#popup"+i).fancybox({
				'width'				: '100%',
				'height'			: '100%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
          }
		
		});
  </script>




