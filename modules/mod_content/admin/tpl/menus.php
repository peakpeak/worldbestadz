<h1>Manage Menus</h1>

<p class="info">Here you can manage website menus</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

{#include=form}

<br />

<form  method="post" action="">
<table  cellpadding="0" cellspacing="0"  class="tableS">
 <thead>
 <tr>
    <th class="left">Name</th>
    <th class="left">Code</th>
	<th></th>
  </tr>
  </thead>
  
  <?php foreach ($menus as $num => $row){ ?>
  
  <tr >
     <td width="30%"><?php echo $row['name'] ?></td>
	 <td><input  type="text" value="{&#35;widget=content::menu?mid:<?php echo $row["id"]?>}" size="60"/><img src="images/tooltip.png" alt="Tip" class="tooltip" title="Add this code anywhere in your theme or pages to display this menu" /></td>
	 <td align="right">
	 <a href="?mod=content&go=links&mid=<?php echo $row["id"]?>"><img src="images/view.png" class="tooltip img-wrap2"  alt="" title="View Links"/></a> 
	 <a href="?mod=content&go=menus&edit=y&mid=<?php echo $row["id"]?>"><img src="images/edit.png" class="tooltip img-wrap2"  alt="" title="Edit"/></a> 
	 <a href="?mod=content&go=menus&delete=y&mid=<?php echo $row["id"]?>" onclick="return(confirm('Do you really want to delete?'))"><img src="images/delete.png" class="tooltip img-wrap2"  alt="" title="Delete"/></a> </td>
  </tr>
 	 
 <?php } ?>


  <?php if(count($menus) == 0) { ?> <tr> <td colspan="3"  align="center">No Menus found</td> </tr> <?php } ?>

  
  </table>
</form>
