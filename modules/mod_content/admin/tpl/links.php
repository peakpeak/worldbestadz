<h1>Manage Menus</h1>

<p class="info">Here you can manage menu links</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

{#include=form}

<br />

<form  method="post" action="">
<table  cellpadding="0" cellspacing="0"  class="tableS">
 <thead>
 <tr>
    <th class="left">Name</th>
    <th class="left">Link</th>
	 <th class="left">&nbsp;</th>
  </tr>
 </thead>
  
  <?php $i = 0; foreach ($links as $num => $row){ $i++; ?>
  
  <tr >
     <td ><?php echo $row['name'] ?></td>
	 <td > <a href="<?php echo $row['url'] ?>" id="popup<?php echo $i?>"><?php echo $row['url'] ?></a></td>
	 <td  align="right"><a href="?mod=content&go=links&edit=y&lid=<?php echo $row["id"]?>&mid=<?php echo $row["menu_id"]?>"><img src="images/edit.png" class="tooltip img-wrap2"  alt="" title="Edit"/></a> <a href="?mod=content&go=links&delete=y&lid=<?php echo $row["id"]?>&mid=<?php echo $row["menu_id"]?>" onclick="return(confirm('Do you really want to delete?'))"><img src="images/delete.png" class="tooltip img-wrap2"  alt="" title="Delete"/></a></td>
  </tr>
 
 <?php } ?>


<?php if(count($links) == 0) { ?> <tr> <td colspan="3"  align="center">No Links found</td> </tr> <?php } ?>

<!--  <tr>
    <td colspan="3"  align="center">
      <input type=button value="Check All" onclick="this.value=check(this.form.checkbox)" id="button" />
      <input type="submit" name="Submit" value="Save" id="button"/>
   </td>
  </tr>-->
  </table>
</form>

<script>
function set_link()
{ 
   
	var index = document.getElementById('type').selectedIndex;
	if(document.getElementById('type').options[index].value == 'page'){	
	
        document.getElementById('page').className = "";	
		document.getElementById('url').className = "hidden";	
		
	}else{
	  
	   document.getElementById('url').className = "";	
        document.getElementById('page').className = "hidden";
	}
	
}
set_link();		
</script>
<script type="text/javascript">
 
   
	 var first = 0;
	 var last = <?php echo $i;?>;	
	  fancyjQuery(document).ready(function() {
			
			for (i = first; i  <= last; i++) {
			
			fancyjQuery("#popup"+i).fancybox({
				'width'				: '100%',
				'height'			: '100%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
          }
		
		});
  </script>