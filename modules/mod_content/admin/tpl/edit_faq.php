<h1>Frequently Asked Questions</h1>

<p class="info">Here you can edit a faq</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

<form  action="" method="post">
<input type="hidden" name="fid"  value="<?php echo $row['id'] ?>" />
<table cellpadding="0" cellspacing="0"  class="tableS" >
  <thead>
   <tr>
    <th colspan="2" class="left"><a href="?mod=content&go=faqs">FAQs</a> > Edit FAQ > <?php echo  $row['question']?></th>
  </tr>
  </thead>
  <tr>
    <td>Question </td>
	<td><input type="text" name="question" style="width:350px" value="<?php echo  $row['question']?>"/></td>
  </tr>
  <tr>
    <td align="left" colspan="2">
      <textarea name="safe_input[answer]" rows="20" cols="20" style="width:100%; height:550px" class="ckeditor" id="ckeditor"><?php echo  $row['answer']?></textarea>
   </td>
  </tr>
   <tr>
    <td align="center" colspan="2">
	   <!--<input type="button" name="preview" value="Preview"/>-->
	   <input type="submit" name="submit" value="Edit"   />	  
   </td>
  </tr>
</table>
</form>

<script src="<?php echo SITEURL.FS.$settings['system']['plugins_directory']?>/ckeditor/ckeditor.js"></script>
<script src="<?php echo SITEURL.FS.$settings['system']['plugins_directory']?>/ckeditor/custom.js"></script>
<script type="text/javascript">CKEDITOR.replace( 'ckeditor', {toolbar: advanced});</script>
