<h1>Frequently Asked Questions</h1>

<p class="info">Here you can manage FAQs for your website</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>
	  
<h2><span><a href="?mod=content&go=add_faq" class="button-alt-sml">Add FAQ</a></span><br /></h2>

      <table cellpadding="0" cellspacing="0"  class="tableS">
	  <thead>
       <tr>
         <th class="left">Question</th>
         <th class="right">&nbsp;</th> 
      </tr>
	  </thead>
	  
       <?php foreach ($faqs as $num => $row){ ?>
      
       <tr>
		  <td><?php echo $row['question']?></td>
          <td class="right"><a href="?mod=content&go=edit_faq&fid=<?php echo $row['id']; ?>"><img src="images/edit.png" class="tooltip img-wrap2"  alt="" title="Edit"/></a> <a href="?mod=content&go=faqs&delete=y&fid=<?php echo $row['id'];?>" onclick="return(confirm('Do you really want to delete?'))"><img src="images/delete.png" class="tooltip img-wrap2"  alt="" title="Delete"/></a></td>
      </tr>
     
	  <?php } ?>


      <?php if(count($faqs) == 0) { ?>  <tr> <td colspan="2" align="center">No faqs found</td> </tr>  <?php } ?>
	  
     </table>
	 
     <script type="text/javascript">
			
	  fancyjQuery(document).ready(function() {
			
			fancyjQuery("#popup").fancybox({
				'width'				: '100%',
				'height'			: '100%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});

		
		});
  </script>