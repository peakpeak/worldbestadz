<form  method="post" action="">
<input type="hidden" name="mid" value="<?php echo $var['mid']?>" />
<table  cellpadding="0" cellspacing="0" class="tableS">
  <thead>
  <tr >
    <th colspan="2"  class="left"><a href="?mod=content&go=menus">Manage Menus</a> > <?php echo $menu["name"]?> > New Link</th>
  </tr>
  </thead>
 <tr>
    <td width="30%">Add Link</td>
	 <td><select name="after" >
	        <option value="index">At the top</option> 	
	        <?php foreach ($links as $num => $plan) : ?> 
			<option value="<?php echo $plan['name']?>">After <?php echo $plan['name']?></option> 
			<?php  endforeach; ?>
          </select>	 
	</td>
  </tr>
  <tr>
    <td>Name</td>
	<td><input type="text" name="name"  /></td>
  </tr>
  <tr>
    <td>Title</td>
	<td><input type="text" name="title"  /></td>
  </tr>
  <tr>
    <td>Type</td>
	  <td><select name="type" id="type" onchange="set_link()">	
			<option value="page"> Custom Page </option>
            <option value="url"> URL </option>
         </select>
	 </td>
  </tr>
   <tr id="page">
    <td>Choose Page</td>
	  <td><select name="page_id" >	
	  		<option value=""> - Select - </option>
            <?php foreach ($pages as $id => $name) : ?> 
			<option value="<?php echo $id?>"><?php echo $name; ?></option> 
			<?php endforeach; ?>
         </select>
	 </td>
  </tr>
	<tr id="url">
    <td>URL</td>
	<td><input type="text" name="url"  /></td>
  </tr>
  <tr >
    <td colspan="2"  class="left"><input type="submit" name="submit" value="Add" /></td>
  </tr>
  </table>
</form>
