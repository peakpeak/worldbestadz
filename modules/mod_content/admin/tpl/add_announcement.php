<h1>Manage Announcements</h1>

<p class="info">Here you can add a new announcement</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>


<form name="addpage" action="" method="post">

<table cellpadding="0" cellspacing="0"  class="tableS" >
  <thead>
  <tr>
    <th colspan="2" class="left"><a href="?mod=content&go=announcements">Manage Announcements</a> > New Announcment</th>
  </tr>
  </thead>
 <tr>
     <td>Type</td>
     <td><select name="type" >	
	  		<option value = "">-Select-</option>
            <option value="Public">Public</option> 	
            <option value="private">Private</option> 			
         </select>	  </td>
    </tr>
  <tr>
     <td>Allow Comments</td>
     <td><select name="comments" >	
            <option value="Y">Yes</option> 	
            <option value="N">No</option> 			
         </select>	  </td>
    </tr>
  <tr>
    <td>Title </td>
	<td><input type="text" name="title" style="width:350px" value="<?php echo  $var['title']?>"/></td>
  </tr>
  <tr>
    <td align="left" colspan="2">
      <textarea name="safe_input[content]" rows="20" cols="20" style="width:100%; height:550px" class="ckeditor" id="ckeditor" ><?php echo  $var['safe_input']['content']?></textarea>
   </td>
  </tr>
   <tr>
    <td align="center" colspan="2">
	   <input type="button" name="preview" value="Preview"  onclick="" />
	   <input type="submit" name="submit" value="Add"   />	  
   </td>
  </tr>
</table>
</form>
<script src="<?php echo SITEURL.FS.$settings['system']['plugins_directory']?>/ckeditor/ckeditor.js"></script>
<script src="<?php echo SITEURL.FS.$settings['system']['plugins_directory']?>/ckeditor/custom.js"></script>
<script type="text/javascript">CKEDITOR.replace( 'ckeditor', {toolbar: advanced});</script>