<h1>Manage Pages</h1>

<p class="info">Here you can add a new page</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

<form name="addpage" action="" method="post">

<table cellpadding="0" cellspacing="0"  class="tableS" >
  <thead>
  <tr>
    <th colspan="2" class="left"><a href="?mod=content&go=pages">Manage Pages</a> > Add New Page</th>
  </tr>
  </thead>
  <tr>
   <td>
   <table cellpadding="0" cellspacing="0">
    <tr>
     <td>Make HomePage</td><td><input type="checkbox" name="index" value="1" /></td>
    </tr>
    <tr>
     <td>Access Level</td>
     <td><select name="privilege" >	
	  		<option value = "">-Select-</option>
            <option value="0">Public</option> 	
            <option value="1">Private</option> 			
         </select><img src="images/tooltip.png" alt="Tip" class="tooltip" title="Select how you want this page to be accessed. Selecting private means it will only be accessible by users who are logged in" />
	  </td>
    </tr>
<tr>
 <td>Theme</td>
    <td><select name="template" >
	        <option value="">-Select-</option>
	        <?php foreach($templates as $template){ ?>
	  		<option value="<?php echo $template;?>"><?php echo $template;?></option>
			<?php } ?>
         </select><img src="images/tooltip.png" alt="Tip" class="tooltip" title="Select a theme for this page" />
	 </td>
</tr>
 <tr>
    <td>Add To Menu</td>
	  <td><select name="menu" >	
	  		<option value=""> - None - </option>
            <?php foreach ($menus as $id => $name) : ?> 
			<option value="<?php echo $id?>"><?php echo $name; ?></option> 
			<?php endforeach; ?>
         </select><img src="images/tooltip.png" alt="Tip" class="tooltip" title="You can choose a menu from which this page accessible" />
	 </td>
  </tr>
	
	 <tr>
    <td>Name </td>
	<td><input type="text" name="name"  style="width:350px" value="<?php echo  $var['name']?>"/></td>
  </tr>
  <tr>
    <td>Page Title </td>
	<td><input type="text" name="title" style="width:350px" value="<?php echo  $var['title']?>"/></td>
  </tr>
 
	</table>
	</td>
	
	
	<td width="50%" valign="top">
	<table cellpadding="0" cellspacing="0">
	 <tr>
      <td>Discription</td>
      <td align="left" colspan="2"><textarea name="meta" rows="2" cols="40" ><?php echo  $var['meta']?></textarea><img src="images/tooltip.png" alt="Tip" class="tooltip" title="Add a page description" /></td>
     </tr>
    <tr>
     <td>Keywords</td>
     <td align="left" colspan="2"><textarea name="keywords" rows="2" cols="40" ><?php echo  $var['keywords']?></textarea><img src="images/tooltip.png" alt="Tip" class="tooltip" title="Enter keywords that describe this page seperated by a comma. Example: promotions,discounts,deals" /></td>
   </tr>
  </table></td>
  </tr>
  <tr>
    <td align="left" colspan="2">
      <textarea name="safe_input[content]" rows="20" cols="20" style="width:100%; height:550px"  class="ckeditor" id="ckeditor"><?php echo  $var['safe_input']['content']?></textarea>
   </td>
  </tr>
   <tr>
    <td align="center" colspan="2">
	  <!-- <input type="button" name="preview" value="Preview"/>-->
	   <input type="submit" name="submit" value="Add"   />	  
   </td>
  </tr>
</table>
</form>

<script src="<?php echo SITEURL.FS.$settings['system']['plugins_directory']?>/ckeditor/ckeditor.js"></script>
<script type="text/javascript">CKEDITOR.replace( ckeditor, {allowedContent: true} );</script>