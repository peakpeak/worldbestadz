<?php
global $system,$db,$settings;

$system->importClass('account')->checkPrivilege(3);

$content = $system->importClass('content');

$var = $system->getVar();

if ($var['delete']== 'y' &&  isset($var['pid']))Content::deletePage($var['pid']);

$sql = "SELECT * FROM ".PREFIX."_content_pages ORDER by category,`order` ASC";
$res = $db->query_db($sql,$print = DEBUG); 
if ($db->num_rows($res) > 0) while ($row = $db->fetch_db_array($res)) $pages[$row["id"]] = $row;

   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'pages.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();
      
?>
 