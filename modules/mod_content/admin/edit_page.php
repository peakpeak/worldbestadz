<?php
global $system,$db,$settings,$validate;

$system->importClass('account')->checkPrivilege(3);

$content = $system->importClass('content');

$var = $system->getVar();

$templates = $system->scanDirectory(ROOT.DS.$settings['system']['templates_directory']);

if(!isset($var['pid']))$system->invalidRequest();

if ($var['submit'] == 'Edit')
{
	
	//Validate
	if ($var['name'] == NULL)
	{
		$return_msg = "Please add a page name";	
	}	
    elseif($db->if_exist_in_db("name",$var['name'],"_content_pages") && $var['pid'] != $db->get_item("id","name",$var['name'],"_content_pages") ) 
    {
        $return_msg = "A page with this name already exist";
    }
    elseif ($var['safe_input']['content'] == NULL)
	{
		$return_msg = "Content field cannot be blank";	
	}
    elseif ($var['privilege'] == NULL)
	{
		$return_msg = "Please add access level";	
	}
	else
	{
      $row = $db->get_row("id",$var['pid'],"_content_pages");
     
      $content = "AES_ENCRYPT('".$db->sql_quote($var['safe_input']['content'])."','".ENCKEY."')";

      $table = "_content_pages";
     
      $update_array = array(
	  'name'=>$var['name'],
	  'title'=>$var['title'],
	  'meta'=>$var['meta'],
	  'keywords'=>$var['keywords'],	  	  	  
	  'template'=>($var['template'] == "")?"default":$var['template'],
     // 'category'=>$var['category'],
	  'order'=>$order,
	  'alias'=>$var['alias'],
	  'content'=>$content,
	  'privilege'=>$var['privilege'],
      'status'=>$var['status'],
	  
	  );

	  $db->noquote = array(7);
      $db->noslash = array(7); 

	  $result = $db->update_db($update_array,$table,"id",$var['pid']);
      if($var['index'] == 1) Content::makeHomePage($var['pid']);
      if($result) $return_msg = "Page Edited";

    }

}


$sql = "SELECT *, AES_DECRYPT(content,'".ENCKEY."') as content FROM ".PREFIX."_content_pages WHERE id='".$var['pid']."' LIMIT 1";
$res = $db->query_db($sql,$print = DEBUG);  
if($db->num_rows($res) == 0)$system->invalidRequest();
$row = $db->fetch_db_array($res);
$menus = Content::getMenus();

   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'edit_page.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();
?>
