<?php
global $system,$db,$settings;

$system->importClass('account')->checkPrivilege(3);

$content = $system->importClass('content');

$var = $system->getVar();

if ($var['delete'] == 'y' &&  isset($var['fid']))Content::deleteFaq($var['fid']);

$sql = "SELECT * FROM ".PREFIX."_content_faqs ORDER BY `order` ASC";
$res = $db->query_db($sql,$print = DEBUG); 
if ($db->num_rows($res) > 0) while ($row = $db->fetch_db_array($res)) $faqs[$row["id"]] = $row;

   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'faqs.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();      
 ?>
 
 