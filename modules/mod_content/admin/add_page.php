<?php
global $system,$db,$settings,$validate;

$system->importClass('account')->checkPrivilege(3);

$content = $system->importClass('content');

$var = $system->getVar();

$templates = $settings['system']['themes'];

if ($var['submit'] == 'Add')
{
	
	//Validate
   if ($var['name'] == NULL)
	{
		$return_msg = "Please add a page name";	
	}	
    elseif($db->if_exist_in_db("name",$var['name'],"_content_pages")) 
    {
        $return_msg = "This page already exist";
    }
    elseif ($var['safe_input']['content'] == NULL)
	{
		$return_msg = "Content field cannot be blank";	
	}
    elseif ($var['privilege'] == NULL)
	{
		$return_msg = "Please add access level";	
	}
	else
	{

     
      
      $content = "AES_ENCRYPT('".$db->sql_quote($var['safe_input']['content'])."','".ENCKEY."')";

      $table = "_content_pages";
     
      $insert_array = array(
	  'name'=>$var['name'],
	  'title'=>$var['title'],
	  'meta'=>$var['meta'],
	  'keywords'=>$var['keywords'],	  	  	  
	  'template'=>($var['template'] == "")?"default":$var['template'],
      //'category'=>$var['category'],
	  'order'=>$order,
	  'alias'=>$var['alias'],
	  'content'=>$content,
	  'privilege'=>$var['privilege'],
      'status'=>$var['status'],
	  
	  );

	  $db->noquote = array(7);
      $db->noslash = array(7);

	  $result = $db->insert_into_db($insert_array,$table);
	  $page_id = $db->id_db();
	  $db->noquote = array();
      $db->noslash = array();
      if($var['index'] == 1) Content::makeHomePage($db->id_db());
	  
	  if($var['menu']!= "")Content::addMenuLink($var['menu'],$var['name'],$var['title'],"index",$page_id,"page");
      if($result) $return_msg = "New Page Added";

    }

}
   $menus = Content::getMenus();

   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'add_page.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();
?>
