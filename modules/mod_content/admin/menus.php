<?php
global $system,$db,$settings,$validate;

$system->importClass('account')->checkPrivilege(3);

$system->importClass('content');

$var = $system->getVar();

$menus = array();

if ($var['submit'])
{
	
	//Validate
	switch($var['submit']){
	
	case 'Add':
	if ($var['name'] == NULL)
	{
		$return_msg = "Please add a name";	
	}
	
	else
	{	
		$added = Content::addMenu($var['name']);		
		if($added) $return_msg = "Menu added";   
       
	}
	
	break;
	case 'Edit':
	if ($var['name'] == NULL)
	{
		$return_msg = "Please add a name";	
	}
	
	else
	{	
		$edited = Content::editMenu($var['mid'],$var['name']);	
			
		if($edited) $return_msg = "Menu edited";   
       
	}
	
	break;
	}
	//switch($var['submit']){}
	
}


if($var['delete'] == 'y' && $var['mid'] != '')Content::deleteMenu($var['mid']);


$sql = "SELECT *  FROM ".PREFIX."_content_menus ";
$result = $db->query_db($sql,$print = DEBUG);
if($db->num_rows($result) > 0) while ($row = $db->fetch_db_array($result)) $menus[$row['id']] = $row;			

if($var['edit'] == 'y' && isset($var['mid'])) {

$sql = "SELECT * FROM ".PREFIX."_content_menus WHERE id = '".$var['mid']."' LIMIT 1";
$res = $db->query_db($sql,$print = DEBUG);
if ($db->num_rows($res) > 0) $erow = $db->fetch_db_array($res);

}

   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $loader->setVar($data);
   
   $tpl_add_file = dirname(__FILE__).DS.'tpl'.DS.'add_menu.php';
   
   $tpl_edit_file = dirname(__FILE__).DS.'tpl'.DS.'edit_menu.php';
   
   $loader->placeHolder['form'] =  ($var['edit'] == 'y' && isset($var['mid'])) ?  $tpl_edit_file :  $tpl_add_file;
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'menus.php';
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();
?>
