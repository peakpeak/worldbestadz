<?php
  global $system,$db,$settings;

   $pageInfo['title'] = '404';
 
   $loader = new Loader;
   
   $data= get_defined_vars();
   
   $loader->setVar($data);
   
   $loader->mainHeader();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'404.php';
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter();
   
   $loader->displayOutput();
   
 ?>

 