<?php
   global $system,$db,$userinfo,$settings,$plugins;
   
   $content = $system->importClass('content');	
   
   $var = $system->getVar();


   if($var['add_testimony'])
   {

      Validate::trim_post();
	   
	  if($var['content']=='') $errors[] =  "Please add testimony content";
	  
	  elseif(strlen($var['content']) > 255)$errors[] =  "Testimony should not exceed 255 characters";
	  
	  if(is_array($errors) && !empty($errors)) while (list($key,$value) = each($errors)) $error_msg.= $value.'<br>';	 
	  else 
	  { 
		  $content->addTestimony($userinfo['username'],$var["content"]);
		  $success_msg = "Thanks for your testimony. It has been added and awaiting admin approval";
	  }
	 
   }
 

   
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'testimony.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('private');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('private');
   
   $loader->displayOutput();

?>