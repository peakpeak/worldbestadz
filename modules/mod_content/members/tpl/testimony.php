<span class="form_title">Add Testimony</span>
<?php if($error_msg){ ?>
<span class="error_message"><?php echo $error_msg;?></span>
<? } ?>
<?php if($success_msg){ ?>
<span class="success_message"><?php echo $success_msg;?></span>
<? } ?>
<table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td width="430" height="200" align="center" valign="top"><div align="justify">Please leave a testimony or review for our program. </div>
      <br>
      <div class="white">
        <form action="" method="post" class="form_1">
          <table cellspacing=0 cellpadding=0 border=0 width="350" class="table_1">
            <thead>
              <tr>
                <th colspan="2">Testimony Form</th>
              </tr>
            </thead>
            <tr>
              <td colspan="2"><textarea name="content"  rows=8 style="width:100%;"><?php echo $var["content"];?></textarea>
            </tr>
            <tr>
              <td colspan="2" align="center"><input type="submit" name="add_testimony" value="Leave Testimony"></td>
            </tr>
          </table>
        </form>
      </div></td>
  </tr>
</table>
