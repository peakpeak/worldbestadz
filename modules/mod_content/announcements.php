<?php
  global $system,$db,$userinfo,$settings;
  
  $system->importClass('content');

  $var = $system->getVar();
  
  $per_page = $settings['system']['rows_per_page'];

  $sql  = "SELECT *,date(date) as date FROM ".PREFIX."_content_announcements ";
  
  $sql .= "WHERE type = 'Public' ";
  
  $sql .= "ORDER BY date DESC";
 
  $result = $db->query_db($sql,$print = DEBUG);


			$page		 = $system->getVar('page');	
			$num_rows 	 = $db->num_rows($result);
			if(!empty($page)) 
			{
				$start = ($page - 1) * $settings['system']['rows_per_page']; 
			}			
			else
			{
				$start 	= 0;
				$page	= 1;
			}				

		
  $sql .= " LIMIT $start, $per_page "; 
  
  $result = $db->query_db($sql,$print = DEBUG);
  if ($db->num_rows($result) > 0)
  {
	 	$rows = $start;
	 	while ($row = $db->fetch_db_array($result))
		{	
			
		    $announcements[$rows] = $row;
			$rows++;	
					
		}
  }
   $pageInfo['title'] = 'Accouncements';
  
   $pageInfo['map'] = array( 'My Announcements' => '', );
   
   $loader = new Loader;
   
   $data= get_defined_vars();
   
   $loader->setVar($data);
   
   $loader->mainHeader();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'announcements.php';
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter();
   
   $loader->displayOutput();
   
 ?>

 