<h1>Send Mass Email</h1>

<p class="info">Here you can send mass email to all members</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

<form  action="" method="post">

<table cellpadding="0" cellspacing="0" class="tableS">
 <thead>
  <tr>
    <th colspan="2" align="left">Send Mass Email</th>
  </tr>
  </thead>
  <tr>
   <td colspan="2">
	  <p class="info"><b>Available Tags </b></p>				
	  <h4>Use the folowing tags below to include user data in your newsletter</h4>
	  <ol>
	    <li>{username} - User username</li>
		<li>{firstname} - User First Name</li>
	    <li>{lastname} - User Last Name</li>
		<li>{email} - User Email Address</li>			 
	  </ol>
	 </td>                  
  </tr>
  <tr>
    <td >To</td>
	  <td><select name="list" id="list" >
	         <option value=""> - Select - </option>
			<?php foreach($lists as $id => $list){ ?>
			  <option value="<?php echo $id?>" <?php if($var['list'] == $id){ ?> selected="selected" <?php }?>><?php echo $list['description'];?></option>
			<?php } ?>
          </select>
	 </td>
  </tr>
  <tr>
    <td>Subject</td>
	<td><input type="text" name="subject" size="100" value="<?php echo $var['subject']; ?>"/></td>
  </tr>
  <tr>
    <td align="left" colspan="2">
      <textarea name="safe_input[message]" rows="20" cols="20" style="width:100%; height:550px" class="ckeditor"><?php echo $var['safe_input']['message']; ?></textarea>
   </td>
  </tr>
   <tr>
    <td align="center" colspan="2">
	  <!-- <input type="button" name="test" value="Send Test" onclick="test_mail()"/>-->
	   <input type="submit" name="submit" value="Send Now"   onclick="send_mail()"/>	  
   </td>
  </tr>
</table>
</form>
<script src="<?php echo SITEURL.FS.$settings['system']['plugins_directory']?>/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
//-->

function test_mail()
{ 
  if (confirm('Do you want to test? This email will be sent to admin email only as test'))return true;
  return false;
}

function send_mail()
{ 
  if (confirm('Send now?'))return true;
  return false;
}

</script>