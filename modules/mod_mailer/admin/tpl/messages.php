<h1>Email Messages</h1>

<p class="info">Here you can view your messages</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

<div class="box" style="margin-bottom:10px">
<form action="index.php" method="get">
<input  type="hidden" name="mod" value="mailer" />
<input  type="hidden" name="go" value="messages" />
  <table cellpadding="0" cellspacing="0" class="utility">
	<tr style="background-color:transparent">
     <td class="border">Username: <input type="text" name="username" value="<?php echo $var["username"]?>"  /></td>
	 <td align="left"><table cellpadding="0" cellspacing="0" border="0"> <tr>
	   <td>Status:</td>
	  <!-- <td><input type="checkbox" class="html-checkboxes" name="status[]" value="Draft"<?php if(in_array('Draft',$var["status"])){ ?> checked="checked" <?php } ?>/>Draft</td>-->
	   <td><input type="checkbox" class="html-checkboxes" name="status[]" value="Pending"<?php if(in_array('Pending',$var["status"])){ ?> checked="checked" <?php } ?>/>Pending</td>
       <td><input type="checkbox" class="html-checkboxes" name="status[]" value="Sending"<?php if(in_array('Sending',$var["status"])){ ?> checked="checked" <?php } ?>/>Sending</td>
	   <td><input type="checkbox" class="html-checkboxes" name="status[]" value="Paused"<?php if(in_array('Paused',$var["status"])){ ?> checked="checked" <?php } ?>/>Paused</td>    <td><input type="checkbox" class="html-checkboxes" name="status[]" value="Completed"<?php if(in_array('Completed',$var["status"])){ ?> checked="checked" <?php } ?>/>Completed</td>
</tr></table></td>
    </tr>
    <tr style="background-color:transparent">
     <td  class="border"> Subject contains : <input type="text" name="subject" value="<?php echo $var["subject"]?>" size="60"  /></td>
     <td align="left" class="border">
	 Items Per Page: 
	 <select name="per_page">
	 <option value="10" <?php if( $var["per_page"] == '10'){ ?> selected="selected" <?php } ?>>10</option>
     <option value="25" <?php if( $var["per_page"] == '25'){ ?> selected="selected" <?php } ?>>25</option>
     <option value="50" <?php if( $var["per_page"] == '50'){ ?> selected="selected" <?php } ?>>50</option>
     <option value="100"<?php if( $var["per_page"] == '100'){ ?> selected="selected" <?php } ?>>100</option>
     </select>
     &nbsp;&nbsp;
     <!--Go To: 
	 <select class="select" >
	 <option value="1" selected="selected">1</option>
     </select>-->
	 <input  class="button-alt-sml" type="button" name="clear"  value="Clear Filters" onclick="window.location.href='?mod=mailer&go=messages'"  />
	 <input  class="button-alt-sml" type="submit" name="submit"  value="Search"  />
</td>
    </tr>
  </table></form>
</div>


<form  method="post" action="">
<table  cellpadding="0" cellspacing="0"  class="tableS">
 <thead>
  <tr>
    <th >No</th>
	<th class="left">Date</th>
    <th class="left">Sender</th>
	<th class="left">Subject</th>
	<th class="left">Status</th>
	<th >Select</th>
 
  </tr>
  </thead>
   <?php $i = 0; foreach ($messages as $id => $row){ $i++; ?> 
  <tr>
     
    <td align="center"><?php echo $id; ?></td>
	<td ><?php echo $row['date']; ?></td>
    <td ><?php echo $row['owner']; ?></td>
    <td ><a href="#inline<?php echo $i;?>" id="popup<?php echo $i?>"><?php echo $row['subject']; ?></a>
	<div style="display: none;">
		<div id="inline<?php echo $i;?>" style="width:700px;height:400px;overflow:auto;"><?php echo $row['message']; ?></div>
	</div></td>
	<td ><?php echo $row['status'];?></td>
	<td align="center"><input type="checkbox" name="list[]" value="<? echo $row["id"]?>"></td>
  </tr>


  <?php } ?>
 
  <?php if(count($messages) == 0) { ?><tr><td colspan="8"  align="center">No messages found</td></tr><?php } ?> 
  	 
     <tr>
	    <td colspan="6" align="center"> <?php echo $system->getPaginationString($page,$num_rows,$per_page,3,$system->curPageURL(),"page");?></td>
	 </tr>

        

 
  <tr>
    <td colspan="8" ><div align="center">
      <input type=button value="Check All" onclick="this.value=check(this.form.checkbox)" id="button" />
	  <input type="submit" name="Submit" value="Pause" id="button"/>
	  <input type="submit" name="Submit" value="Unpause" id="button"/>
	  <input type="submit" name="Submit" value="Delete" id="button" onclick="return(confirm('Do you really want to delete?'))"/>
	  
     </div>
   </td>
  </tr>
  </table>
</form>
<script language="javascript">
<!-- Begin

    var checkflag = "false";

    function check(field) {
    var checks = document.getElementsByName('list[]');
    if (checkflag == "false") {
     for (i = 0; i < checks.length; i++){
      checks[i].checked = true;
     }
      checkflag = "true";
      return "Uncheck All";
   }
   else {
     for (i = 0; i < checks.length; i++) {
      checks[i].checked = false; }
      checkflag = "false";
      return "Check All"; 
  }
}
//  End -->
</script>

<script type="text/javascript">
 
   
	 var first = 0;
	 var last = <?php echo $i;?>;	
	  fancyjQuery(document).ready(function() {
			
			for (i = first; i  <= last; i++) {
			
			fancyjQuery("#popup"+i).fancybox({
				'titlePosition'		: 'inside',
				'transitionIn'		: 'none',
				'transitionOut'		: 'none'
			});
			
          }
		
		});
  </script>




