<?php
set_time_limit(0);

global $system,$db,$settings,$validate,$admininfo;

$system->importClass('account')->checkPrivilege(3);

$var = $system->getVar();

$lists = array();
$sql  = "SELECT * FROM ".PREFIX."_mailer_lists ";
$res = $db->query_db($sql,$print = DEBUG); 
if ($db->num_rows($res) > 0)while ($row = $db->fetch_db_array($res)) $lists[$row['id']] = $row;

			
if ($var['submit'] == 'Send Now')
{
	
	//Validate
	if ($var['safe_input']['message'] == NULL)
	{
		$return_msg = "Message field cannot be blank";	
	}
	elseif ($var['list'] == NULL)
	{
		$return_msg = "You must select recipients";	
	}
	else
	{	
		
		 $table = "_mailer_messages";
		 $insert_array = array('owner' => $admininfo['username'],
							   'list_id' => $var['list'],
							   'date' => date('Y-m-d H:i:s'),
							   'subject' => $var['subject'],
							   'message' => $var['safe_input']['message'],
		 );
		 $added = $db->insert_into_db($insert_array,$table); 
         if($added) $return_msg = "Message has been queued";	

	}
}


   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'send_email.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();
?>
