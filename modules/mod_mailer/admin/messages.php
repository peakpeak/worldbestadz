<?php
global $system,$db,$settings,$admininfo;
$system->importClass('account')->checkPrivilege(2);
$mailer = $system->importClass('mailer');
$var = $system->getVar();

$r = count ($var['list']);
$list = $var['list'];

    switch($var['Submit'])
	{
		
		case"Pause":
		for ($i=0; $i < $r; $i++)
		{
			$n 	= $i+1;
			$tid = $list[$i];
			$sql = "SELECT * FROM ".PREFIX."_mailer_messages WHERE id = '".$tid."' ";
			$sql.= "AND status = 'Sending' ";
			$res = $db->query_db($sql,$print = DEBUG);
			if ($db->num_rows($res) > 0) $mailer->updateMessageStatus($tid,'Paused');

		}   if($n > 0) $return_msg ="Message(s) Paused";
		break;
		
		case"Unpause":
		for ($i=0; $i < $r; $i++)
		{
			$n 	= $i+1;
			$tid = $list[$i];
			$sql = "SELECT * FROM ".PREFIX."_mailer_messages WHERE id = '".$tid."' ";
			$sql.= "AND status = 'Paused' ";
			$res = $db->query_db($sql,$print = DEBUG);
			if ($db->num_rows($res) > 0) $mailer->updateMessageStatus($tid,'Pending');

		}   if($n > 0) $return_msg ="Message(s) Unpaused";
		break;
		
		case"Delete":
		for ($i=0; $i < $r; $i++)
		{
			
			$n 	= $i+1;
			$tid = $list[$i];
			$sql = "SELECT * FROM ".PREFIX."_mailer_messages WHERE id = '".$tid."' ";
			$res = $db->query_db($sql,$print = DEBUG);
			if ($db->num_rows($res) > 0) $mailer->deleteMessage($tid);
		  
		}  if($n > 0) $return_msg ="Message(s) Deleted";
		break;
		
	}

	
  $sql  = "SELECT COUNT(*) as total FROM ".PREFIX."_mailer_messages ";

  $sql .= "WHERE id != '' ";
	
  if($var['submit'] == 'Search')
  {
	   
      if(isset($var['status']) && count($var['status'])> 0)
	  { 
	    $sql .= "AND ( ";
	    foreach($var['status'] as $status) { $sql .= "status = '".$status."' "; if($status != end($var['status'])) $sql .= "OR ";}
	    $sql .= ") ";
	  }
    
	  if(!empty($var['username']))$sql .= "AND owner = '".$var['username']."' ";
	  
	  if(!empty($var['subject']))$sql .= "AND subject LIKE '%".$var['subject']."%' ";

  }


  
  $sql .= "ORDER BY id DESC";
  $result = $db->query_db($sql,$print = DEBUG);
  $num_rows = $db->db_result($result, 0, 'total');
  $page = $var['page'];
  $per_page = (is_numeric($var['per_page']) 
	             && $var['per_page'] <= $settings['system']['rows_per_page'])?
				 $var['per_page']:$settings['system']['rows_per_page'];		 	
  if(!empty($page)){$start = ($page - 1) * $per_page;}
  else { $start 	= 0; $page	= 1; }		
  $sql = str_replace("COUNT(*) as total", " * ",$sql);
  $sql .= " LIMIT $start, $per_page "; 
  $result = $db->query_db($sql,$print = DEBUG);
  
  if ($db->num_rows($result) > 0)
  {
	 	$rows = $start;
	 	while ($row = $db->fetch_db_array($result))
		{	
			$rows++;
			$row["content"] = (strlen($row["subject"]) <= 50)?$row["subject"]:substr($row['subject'],0,50).'...' ;
		    $messages[$rows] = $row;			
		}
  }
 
	
   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'messages.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();

?>
