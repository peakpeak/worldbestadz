<?php
defined('MAILER') || exit('No direct script access allowed');
$users = array();
$sql  = "SELECT username FROM ".PREFIX."_account_users as user ";
$sql .= "WHERE id != '' ";
//select clause
$sql .= "AND user.status = 'Active' ";

//mailer clause
$sql .= "AND NOT EXISTS (SELECT log.receiver FROM ".PREFIX."_mailer_log as log WHERE log.receiver = user.username AND log.message_id = '".MID."') ";
$sql .= "LIMIT ".LIMIT;


$res = $db->query_db($sql,$print = DEBUG);  
if ($db->num_rows($res) == 0) return $users;
while($row = $db->fetch_db_array($res)) $users[] = $row['username'];

return $users;

?>