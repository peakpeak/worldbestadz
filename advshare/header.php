<html>
<head>
<title>Admin Panel</title>
<link rel="stylesheet" type="text/css" href="<?php echo SITEURL.FS.$settings['system']['admin_directory']?>/css/admin.css" />
<?php $system->importPlugin("fancybox");?>
<?php $system->importPlugin("jquery-ui");?>
<script type="text/javascript" src="<?php echo SITEURL.FS.$settings['system']['admin_directory']?>/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo SITEURL.FS.$settings['system']['admin_directory']?>/js/admin.js"></script>
<script type="text/javascript" >

function post_to_url(path, params, method) {
    method = method || "post";
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);
            form.appendChild(hiddenField);
         }
    }

    document.body.appendChild(form);
    form.submit();
}
</script>
</head>
<body>

<table width="100%" height="100%" cellpadding=0 cellspacing=0>
	<tr>
		<td height="50" colspan=2>
		
			<div id="logoHeader">
				<div id="adminHeading"><a href="?mod=mailer&go=send_email" style="color: white; text-decoration:none; font-size: 17px; font-weight: normal;"><img src="<?php echo SITEURL.FS.$settings['system']['admin_directory']?>/images/icons/email.png"> Newsletter</a></div>
				<div id="adminHeading"><a href="?mod=admin&go=settings" style="color: white; text-decoration:none; font-size: 17px; font-weight: normal;"><img src="<?php echo SITEURL.FS.$settings['system']['admin_directory']?>/images/icons/settings.png"> Configuration</a></div>
				<div id="adminHeading"><a href="index.php" style="color: white; text-decoration:none; font-size: 17px; font-weight: normal;"><img src="<?php echo SITEURL.FS.$settings['system']['admin_directory']?>/images/icons/home.png"> Home</a></div>
				<div id="agptHeading"><a href="../members/"><img src="<?php echo SITEURL.FS.$settings['system']['admin_directory']?>/images/icons/back.png" alt="back"></a> <a href="<?php echo SITEURL?>/logout.php"><img src="<?php echo SITEURL.FS.$settings['system']['admin_directory']?>/images/icons/logout.png" alt="logout"></a> Welcome, {#data=account::username}</div>
			</div>
		</td>
	</tr>
	
	<td width="180" valign="top">
	<div id="menuMainCont-wrap">
		 <div id="menuMainCont">
	        <div class="mainMenu">

				    <?php // global $db,$system;
					
                       $sql  = "SELECT * FROM ".PREFIX."_core_modules ";
                       $sql .= "WHERE installed ='Yes' ";
					   $sql .= "AND  status ='Enabled' ";
                       $sql .= "ORDER BY `order` ASC";
                       $result = $db->query_db($sql,$print = DEBUG);

                       if ($db->num_rows($result) > 0){
	                      $sub = 50; 
	 	                  while ($row = $db->fetch_db_array($result)){ 
                          $system->importClass($row['name']); 
						  if(method_exists($row['controller_classname'],"adminMenu")) {
						  //$menu = $row['controller_classname']::adminMenu();
						  $tempClass = new $row['controller_classname'];
						  $menu = $tempClass->adminMenu();
						  unset($tempClass); 
						  } else $menu = "";                        
                          //$menu = ( method_exists($row['controller_classname'],"adminMenu") )?$row['controller_classname']::adminMenu():"";	
						  if($menu == "" || count($menu) == 0) continue;
                   ?>
				   
				   
<div class="menuMainItem"><?php if(is_file(ROOT.DS."modules".DS."mod_".$row['name'].DS."icon.gif")) {?><img src="<?php  echo SITEURL.FS."modules".FS."mod_".$row['name'].FS."icon.gif" ?>" align="absmiddle"><?php } ?><a href="#" onClick="return toggleSub('<?php echo $row['id']?>')"><?php echo strtoupper($row['title'])?></a></div>
      
	 <div id="dropCont<?php echo $row['id']?>" class="dropContDiv">
	 
	 
            <?php if(count($menu) > 0) { foreach ($menu as $title => $link) { ?>
			   
	        <?php if(is_array($link)){ ?>
			
		    <div class="menuMainSubHead">&nbsp;&nbsp;&nbsp;-><a href="#" onClick="return toggleSub('<?php echo $row['id']?><?php echo $sub?>')"><?php echo $title ?></a></div>
			<div id="dropCont<?php echo $row['id']?><?php echo $sub?>" class="dropContDiv">
			
			<?php foreach ($link as $subtitle => $sublink) {$sub++; ?>
			   
			<div class="menuMainSub">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo SITEURL.FS.$settings['system']['admin_directory']?>/images/menuSubItem.gif">&nbsp;<a href="<?php  echo SITEURL.FS.$settings['system']['admin_directory'].FS.$sublink?>" ><?php echo $subtitle ?></a></div>
				
			<?php } ?> </div> <?php  } else { ?>
			
			<div class="menuMainSubHead">&nbsp;&nbsp;&nbsp;-><a href="<?php echo SITEURL.FS.$settings['system']['admin_directory'].FS.$link; ?>"><?php echo $title ?></a></div>
			   
		     <?php } } }  ?>
     </div>
			  

<?php } } ?>	


  <div class="menuMainItem"><img src="<?php echo SITEURL.FS.$settings['system']['admin_directory']?>/images/icons/applications.gif" align="absmiddle"><a href="" onClick="return toggleSub('104')">SYSTEM</a></div>
					
						  
						  <div id="dropCont104" class="dropContDiv">
						   <div class="menuMainSubHead">&nbsp;&nbsp;&nbsp;-><a href="?mod=admin&go=settings">Configuration</a></div>
						   <div class="menuMainSubHead">&nbsp;&nbsp;&nbsp;-><a href="?mod=admin&go=modules">Modules</a></div>
						   <div class="menuMainSubHead">&nbsp;&nbsp;&nbsp;-><a href="?mod=admin&go=updates">Updates</a></div>
        				   <div class="menuMainSubHead">&nbsp;&nbsp;&nbsp;-><a href="<?php echo SITEURL?>/logout.php"><b>LOGOUT</b></a></div>
						 </div>  
					              
	       </div>
	  </div>
	</div>
	</td>
	<td width="796" valign="top">	

	 <div class="wrap">
    <div id="contentMainCont-wrap">
    <div id="contentMainCont">
	
	