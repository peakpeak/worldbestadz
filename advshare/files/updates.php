<?php

   global $system,$db,$settings;

   $system->importClass('account')->checkPrivilege(3);

   $var = $system->getVar();
   
   if($var['install'] == 'y' && isset($var['update']))
   {
	 $updated = $system->installUpdate($var['update']);
	 if($updated[0] == true)  $return_msg = "Update Installed";
	 else  $return_msg = $updated[1];
   }
	
   $updates = array();
   $files = scanDirectory( ROOT.DS.'updates' );
   foreach($files as $file)
   {
	   $updates[] = $file;
	   $installed[] = $db->get_row("version",$file,"_core_updates") ? 'Yes':'No';
	   $changelog[] = SITEURL.FS.'updates'.FS.$file.FS.'CHANGELOG.txt';
    }
 
   $data = get_defined_vars();
	
   $loader = new Loader;
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'updates.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();

?>

