<?php
set_time_limit(0);

global $system,$db,$settings,$validate;

$system->importClass('account')->checkPrivilege(3);

$var = $system->getVar();

if ($var['submit'] == 'Send')
{
	
	//Validate
    if($var['reciever'] == null || !Account::isUser($var['reciever']))
    {
            $return_msg = "Add a valid username";
    }
	elseif ($var['safe_input']['message'] == NULL)
	{
		    $return_msg = "Message field cannot be blank";	
	}
	else
	{		
	        $sql  = "SELECT username FROM ".PREFIX."_account_users ";
			$sql .= "WHERE username = ".$db->sql_quote($var['reciever'])."' LIMIT 1 ";
			$res = $db->query_db($sql,$print = DEBUG);  
			if ($db->num_rows($res) > 0)
			{
				$row = $db->fetch_db_array($res);
				$var['subject'] = ($var['subject']=='')? "[No Subject]" : $var['subject'];
				$msg_data = array(
				 
				 "{subject}" =>  $var['subject'],
				 "{body}" => $var['safe_input']['message'],
				 
				);
				$system->email($row['username'],"Newsletter",$msg_data);
				/*if($var['to'] == "User")$system->email($row['username'],"Newsletter",$msg_data);
				elseif(Account::getSettings($row['username'],"system","receive_newsletter") && $var['to'] == "All")$system->email($row['username'],"Newsletter",$msg_data);*/

				$return_msg = "Message sent";
				$db->free_result($res);	
			}
			else
			{
				$return_msg = "No data found";//No User data Found
			}

	}
}


   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'send_email.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();
?>
