<h1>Email Templates</h1>

<p class="info">Here you can edit email templates</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

<form action="" method="post" >
<input type="hidden" name="tid" value="<?php echo $erow['id'];?>" />

<table cellpadding="0" cellspacing="0"  class="tableS">
<thead>
  <tr>
    <th colspan="2" align="left"><a href="?mod=admin&go=email_templates">Email Templates</a> > Edit Email Template > <?php echo $erow['name'];?></th>  
  </tr>
  </thead>
  <tr>
    <td width="30%">Subject</td>
	<td ><input type="text" name="value[]" class="input"  size="100" value="<?php echo $erow['subject'];?>"/></td>
  </tr>
  <tr>
     <td colspan="2"> <textarea name="safe_input[body]"  rows="20" cols="20" style="width:100%; height:550px"  class="ckeditor"><?php echo $erow['body'];?></textarea></td>
  </tr>
  <tr>
    <td>Status</td>
	  <td ><? echo $db->make_select("value[]",array("Yes"=>"Active","No"=>"Inactive"),$erow["send"],"input")?> </td>
  </tr>
  <tr>
    <td align="left" colspan="2"><input type="submit" name="submit" value="Edit Now" /></td>
   </tr>   
</table>
</form>

<script src="<?php echo SITEURL.FS.$settings['system']['plugins_directory']?>/ckeditor/ckeditor.js"></script>