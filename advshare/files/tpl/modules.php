<h1>Installed Modules</h1>

<p class="info">You can mange modules installed in your system from here</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

<!--<h2><span><a href="?mod=admin&go=wizard" class="button-alt-sml" >Install New Module</a></span><br /></h2>-->

<form  method="post" action="">
<input  type="hidden" name="update" value="y" />
<table   cellpadding="0" cellspacing="0" class="tableS">
 <thead>
  <tr>
    <th  width="5%"></th>
    <th class="left">Name</th>
	<th class="right">Sort Order</th>
  </tr>
  </thead>
  
  <?php foreach ($modules as $num => $row){ ?>
  
  <tr >
     <td align="center" ><input type="checkbox" name="list[]" value="<?php echo $row["id"]?>"></td>
     <td align="left"><?php echo $row['title'] ?> <!--v<?php echo $system->getModuleInfo($row['name'],"info:version");?>--></td>
     <td align="right">
	     <select name="status[<?php echo $row['id'] ?>]">
           <option value="Enabled" <?php if($row['status'] == 'Enabled')  echo "selected"?>>Enabled</option>
           <option value="Disabled" <?php if($row['status'] == 'Disabled')  echo "selected"?>>Disabled</option>
        </select> 
		<input type="text" name="order[<?php echo $row['id'] ?>]" value="<?php echo $row["order"]?>" size="5">
     </td>
  </tr>
  <?php } ?>
  <?php if(count($modules) == 0) { ?><tr> <td colspan="5" align="center" >No module found</td> </tr> <?php } ?>
  <tr>
    <td colspan="3" align="center">
      <input type=button value="Check All" onclick="this.value=check(this.form.checkbox)" id="button" />
      <input type="submit" name="submit" value="Save" id="button"/>
   </td>
  </tr>
  </table>
</form>

<script language="javascript">
<!-- Begin
var checkflag = "false";
function check(field) {
var checks = document.getElementsByName('list[]');
if (checkflag == "false") {
for (i = 0; i < checks.length; i++) {
checks[i].checked = true;}
checkflag = "true";
return "Uncheck All"; }
else {
for (i = 0; i < checks.length; i++) {
checks[i].checked = false; }
checkflag = "false";
return "Check All"; }
}
//  End -->
</script>

