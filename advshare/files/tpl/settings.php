<h1>Settings</h1>

<p class="info">Here you can update system and module settings and configuration</p>

<?php if($return_msg){ ?><p id="mes"><? echo $return_msg;?></p><?php } ?>


<form  method="post" action="">
<table  cellpadding="0" cellspacing="0"  class="tableS">
  <!--<thead>
  <tr>
    <th colspan="2" class="left">Settings & Configuration</th>
  </tr>
  </thead>-->
 
 <?php
 $sql = "SELECT * FROM ".PREFIX."_core_settings ORDER BY `group_name` ASC, id ASC ";
 $res = $db->query_db($sql,$print = DEBUG);
 if ($db->num_rows($res) > 0)
 {
   $group = "" ;
   while ($row = $db->fetch_db_array($res))
   {
     
	 $opt = explode("::",$row["options"]);
	 $options = ($row["options"] == "")? $settings['system']['bool_no_yes']: array(''=>'Select') + $settings[$opt[0]][$opt[1]];
     if($row["group_name"] != $group){ $group = $row["group_name"]; ?>
	<thead>
    <tr>
      <th colspan="2"  align="left"><b><?php echo $row["group_name"]; ?></b></th>
    </tr>
	<thead>
   <?php } ?>
     <tr>
       <td  width="30%"><?php echo $row['description'] ?></td>
     <td >
	
	 <?php if($row["input_type"] == "select") { ?> <?php echo $db->make_select("settings[".$row['id']."]",$options,$row["value"],"")?> 
	 <?php } elseif($row["input_type"] == "textarea") { ?><textarea name="settings[<?php echo $row['id'];?>]"><?php echo $row["value"]?></textarea> 
	 <?php } elseif($row["input_type"] == "multi_select" ) { ?> <input type="hidden" name="settings[<?php echo $row['id'];?>]" value="" />
	 <?php foreach( $options as $k => $v) { ?> <input type="checkbox" name="settings[<?php echo $row['id'];?>][]" value="<?php echo $k;?>"<?php if(stristr($row['value'],",$k,")) { ?> checked="checked"<?php }?>/><?php echo $v;?> <?php  }?>
	 <?php } else { ?>  <input type="text" name="settings[<?php echo $row['id'];?>]" value="<?php echo $row["value"]?>"> <?php } ?>
	 <?php if($row["hint"] != "") { ?><img src="images/tooltip.png" alt="Tip" class="tooltip" title="<?php echo $row["hint"]?>" /> <?php } ?>
	 </td>
  </tr>
  <?php } } else { ?> <tr> <td colspan="2" align="center">No Settings found</td> </tr><?php } ?>
  <tr>
    <td colspan="2" class="left"><input type="submit" name="submit" value="Save" /></td>
  </tr>
 </table>
</form>
