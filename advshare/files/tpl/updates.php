<h1>Install Updates</h1>

<p class="info">Make sure you backup your website before installing update</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

  <table  cellpadding="0" cellspacing="0" class="tableS">
    <thead>
	  <tr>
	     <th colspan="2" align="left">Uploaded Updates</th>            
	   </tr>
	 </thead>
	 <?php  $j = 0; foreach ($updates as $i => $update){ $j++;?>
     <tr>
      <td  align="left">Version <?php echo $update ?> - <a href="<?php echo $changelog[$i];?>" id="popup<?php echo $j?>">CHANGELOG</a></td>
      <td  align="right"><?php if($installed[$i] == 'No') { ?><a href="?mod=admin&go=updates&install=y&update=<?php echo $update ?>" onclick="return(confirm('Do you really want to install?'))">Install</a><?php } else { ?> Installed <?php } ?></td>
     </tr>
     <?php } ?>
     <?php if(count($updates) == 0) { ?><tr> <td colspan="2" align="center" >No updates found</td> </tr><?php } ?>
 </table> 

 
 <script type="text/javascript">
 
   
	 var first = 0;
	 var last = <?php echo $j;?>;	
	  fancyjQuery(document).ready(function() {
			
			for (i = first; i  <= last; i++) {
			
			fancyjQuery("#popup"+i).fancybox({
				'width'				: '70%',
				'height'			: '100%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
          }
		
		});
  </script>
 