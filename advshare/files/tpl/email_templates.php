<h1>Email Templates</h1>

<p class="info">Here you can edit email templates</p>

<?php if($return_msg){ ?><p id="mes"><?php echo $return_msg;?></p><?php } ?>

<form  method="post" action="">
<input  type="hidden" name="update" value="y" />
<table  cellpadding="0" cellspacing="0"  class="tableS">
 <thead>
  <tr>
    <th  width="2%">#</th>
	<th class="left">Template</th>
    <th class="right">Action</th>
  </tr>
  </thead>
  
 <?php $i = 0; foreach ($templates as $num => $row){ $i++; ?>
  
  <tr >
     <td ><?php echo $num ?></td>
     <td ><?php echo $row['name'] ?></td>
     <td align="right"><a href="?mod=admin&go=edit_email_template&tid=<?php echo $row['id']?>"><img src="images/edit.png" class="tooltip img-wrap2"  alt="" title="Edit"/></a ></td>
  </tr>
  
  <?php } ?>

  <?php if(count($templates) == 0) { ?> <tr> <td colspan="3"  align="center">No template found</td> </tr>  <?php } ?>

  <!--<tr>
    <td colspan="3" ><div align="center">
      <input type=button value="Check All" onclick="this.value=check(this.form.checkbox)" id="button" />
      <input type="submit" name="submit" value="Save" id="button"/>
     </div>
   </td>
  </tr>-->
  </table>
</form>
