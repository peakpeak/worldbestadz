<?php
global $system,$db,$settings,$validate;

$system->importClass('account')->checkPrivilege(3);

$var = $system->getVar();


if (!isset($var['tid'])) header('location: index.php');

       if ($var['submit'] == 'Edit Now')
       {
	      
	       $db->key_f = "id";
	       $db->table_n = "_core_email_templates";
	       $db->keyid  = $var['tid'];
	       $db->fields = array ('subject','send','body');
		   array_push($var['value'],$var['safe_input']['body']);
		   $db->values = $var['value'];
	       $updated = $db->up_record();
		   if($updated)$return_msg = "Template updated";

      }

$sql = "SELECT * FROM ".PREFIX."_core_email_templates WHERE id='".$var['tid']."' LIMIT 1";
$res = $db->query_db($sql,$print = DEBUG);  
if ($db->num_rows($res) == 0) header('location: index.php');
$erow = $db->fetch_db_array($res);


   $loader = new Loader;
   
   $data = get_defined_vars();
   
   $tpl_file =  dirname(__FILE__).DS.'tpl'.DS.'edit_email_template.php';
   
   $loader->setVar($data);
   
   $loader->mainHeader('admin');
   
   $loader->loadOutput($tpl_file);
   
   $loader->mainFooter('admin');
   
   $loader->displayOutput();
?>
