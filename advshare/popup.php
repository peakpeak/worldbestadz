<?php
session_start();

define( 'EXE', 1 );

define('ROOT', realpath(dirname('../index.php')) );

define( 'DS', DIRECTORY_SEPARATOR );

define( 'FS', '/' );

require_once ( ROOT .DS.'includes'.DS.'system.inc.php' );

$system = new Main;

$system->systemEnv="admin";

$admininfo = $system->importClass('account')->islogged(true);

$mod = $system->getVar('mod');

$go = $system->getVar('go');

if(!isset($mod)) $mod = $settings['system']['default_module'];

$output = $system->loadModule($mod,$go);

if(!$output)$system->invalidpage();

if(DEBUG)$system->systemDebug();

?>