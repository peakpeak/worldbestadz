<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>{#loader=page::title}</title>
<meta property="og:title" content="AdvShare" />
<meta property="og:description" content="AdvShare is an Advertisement Platform to promote your business and make money while doing it." />
<meta property="og:image" content="http://advshare.com/templates/default-home/styles/images/imgctn1.png" />

<link rel="shortcut icon" href="{#loader=template::url}images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="{#loader=template::url}css/styles.css" type="text/css">
<link rel="stylesheet" href="{#loader=template::url}css/base.css" type="text/css" >
<link rel="shortcut icon" href="{#loader=system::url}/favicon.png" type="image/x-icon">
<link rel="stylesheet" href="{#loader=template::url}styles/bootstrap.min.css" type="text/css" >
<link rel="stylesheet" href="{#loader=template::url}styles/custom.css" type="text/css" >
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,100,300,500,600,700,900' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="{#loader=template::url}js/jquery.min.js"></script>
<script type="text/javascript" src="{#loader=template::url}js/functions.js"></script>
<script type="text/javascript" src="{#loader=template::url}styles/bootstrap.min.js"></script>
<?php $system->importPlugin("vkeyboard");?>
<?php $system->importPlugin("fancybox");?>
<?php $system->importPlugin("jquery-ui");?>
<?php $system->importPlugin("jquery-cycle");?>
</head>

<body leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0">
	<div class="headerContainer">
		<div class="headerInner">
			<a href="{#loader=system::url}/" id="logo"></a>
			<div class="mainNavRight">
				<div class="navbar">
					<div class="navbar-inner">
						<ul class="nav">
							<li><a href="{#loader=system::url}/">Home</a></li>
							<li><a href="{#loader=system::url}/pages/How">how it works</a></li>
							<li><a href="{#loader=system::url}/pages/about">About</a></li>
							<li><a href="{#loader=system::url}/pages/plans">plans</a></li>
							<li><a href="{#loader=system::url}/content/faqs">FAQ</a></li>
							<li><a href="{#loader=system::url}/account/support">support</a></li>
							<li><a class="login" href="{#loader=system::url}/account/login"><i></i>Login</a></li>
							<li><a class="register" href="{#loader=system::url}/account/register"><i></i>Register</a></li>	
						</ul>				
					</div>
				</div>
			</div>
		</div>
	</div> <!-- end headerContainer -->

<?php if($_SERVER['REQUEST_URI'] == "" OR $_SERVER['REQUEST_URI'] == "/" OR $_SERVER['REQUEST_URI'] == "/index.php" OR preg_match("/(\&|\?)ref\=/i", $_SERVER['REQUEST_URI']) ) { ?>

<div class="Warrper">
		<div class="bannerContainer">
			<div class="bannerInner">
				<div class="bannerLeft">
					<h3>get paid for advertising</h3>
					<p>An Advertising Platform to Headstart your Business, while making money every second..!</p>
						<a href="{#loader=system::url}/account/register">free signup</a>
				</div>
				<div class="bannerRight">
					<div class="bannerPartRight1">
						<p><span>+10%</span> affiliate income</p>
						<div></div>
					</div>
					<div class="bannerPartRight2">
						<p>cashback up to <span>130%</span></p>
						<div></div>
					</div>
					<div class="bannerPartRight3">
						<p>purchased ad pack</p>
						<div></div>
					</div>
				</div>
			</div>
		</div><!-- end bannerContainer -->
		<div class="jetadContainer">
			<div class="jetadInner">
				<center>{#widget=ads::banner?pid:3&display:vertical&limit:1}</center>
			</div>
		</div>
	</div><!-- end Warrper -->

<?php }?>	
	

