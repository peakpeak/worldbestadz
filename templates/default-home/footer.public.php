	<div class="advertiseContainer">
		<div class="advertiseInner">
			{#widget=ads::banner?pid:2&display:vertical&limit:1}
			{#widget=ads::banner?pid:2&display:vertical&limit:1}
		</div>
	</div> <!-- end advertiseContainer -->
	<div class="footerContainer">
		<div class="footerInner">
			<div class="footerLeft">
				<div class="footerLeftTop">
					<p>&#169; 2015 AdvShare All Rights reserved.</p>
				</div>
				<div class="footerLeftBot"> 
					<a href="{#loader=system::url}/pages/terms">Terms and Conditions</a>
					<span>|</span>
					<a href="{#loader=system::url}/pages/privacy">Privacy Policy</a>
					<span>|</span>
					<a href="{#loader=system::url}/pages/about">About Us</a>
					<span>|</span>
					<a href="{#loader=system::url}/pages/support">Support</a>
					<span>|</span>
					<a href="{#loader=system::url}/content/faqs">F.A.Q.</a>
				</div>
			</div>
			<div class="footerRight">
				<a class="solid" href="https://www.solidtrustpay.com"></a>
				<a class="bitc" href="https://www.bitcoin.org"></a>
				<a class="perf" href="https://www.perfectmoney.is"></a>
				<a class="payz" href="https://www.payza.com"></a>
			</div>
		</div>
	</div><!--  end footerContainer -->
</body>
</html>



