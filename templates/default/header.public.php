<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>{#loader=page::title}</title>
<meta property="og:title" content="AdvShare" />
<meta property="og:description" content="AdvShare an Advertisement Platform to promote your business" />
<meta property="og:image" content="http://advshare.com/templates/default-home/styles/images/imgctn1.png" />
<link rel="shortcut icon" href="{#loader=template::url}images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="{#loader=template::url}css/styles.css" type="text/css">
<link rel="stylesheet" href="{#loader=template::url}css/base.css" type="text/css" >
<link rel="shortcut icon" href="{#loader=system::url}/favicon.png" type="image/x-icon">
<link rel="stylesheet" href="{#loader=template::url}styles/bootstrap.min.css" type="text/css" >
<link rel="stylesheet" href="{#loader=template::url}styles/custom.css" type="text/css" >
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,100,300,500,600,700,900' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="{#loader=template::url}js/jquery.min.js"></script>
<script type="text/javascript" src="{#loader=template::url}js/functions.js"></script>
<script type="text/javascript" src="{#loader=template::url}styles/bootstrap.min.js"></script>

<?php $system->importPlugin("vkeyboard");?>
<?php $system->importPlugin("fancybox");?>
<?php $system->importPlugin("jquery-ui");?>
<?php $system->importPlugin("jquery-cycle");?>
</head>

<body leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0">
	<div class="headerContainer">
		<div class="headerInner">
			<a href="{#loader=system::url}/" id="logo"></a>
			<div class="mainNavRight">
				<div class="navbar">
					<div class="navbar-inner">
						<ul class="nav">
						<li><a href="{#loader=system::url}/">Home</a></li>
							<li><a href="{#loader=system::url}/pages/How">how it works</a></li>
							<li><a href="{#loader=system::url}/pages/about">About</a></li>
							<li><a href="{#loader=system::url}/pages/plans">plans</a></li>
							<li><a href="{#loader=system::url}/content/faqs">FAQ</a></li>
							<li><a href="{#loader=system::url}/account/support">support</a></li>
							<li><a class="login" href="{#loader=system::url}/account/login"><i></i>Login</a></li>
							<li><a class="register" href="{#loader=system::url}/account/register"><i></i>Register</a></li>	
						</ul>				
					</div>
				</div>
			</div>
		</div>
	</div> <!-- end headerContainer -->

	<div class="logintitleContainer">
		<div class="logintitleInner">
	
		</div>
	</div><!-- end logintitleContainer -->
	
<div class="subContainer">
		<div class="subInner">

