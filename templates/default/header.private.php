<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>{#loader=page::title}</title>

<meta property="og:title" content="AdvShare" />
<meta property="og:description" content="AdvShare an Advertisement Platform to promote your business" />
<meta property="og:image" content="http://advshare.com/templates/default-home/styles/images/imgctn1.png" />
<link rel="shortcut icon" href="{#loader=template::url}images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="{#loader=template::url}css/styles.css" type="text/css">
<link rel="stylesheet" href="{#loader=template::url}css/base.css" type="text/css" >
<link rel="shortcut icon" href="{#loader=system::url}/favicon.png" type="image/x-icon">
<link rel="stylesheet" href="{#loader=template::url}styles/bootstrap.min.css" type="text/css" >
<link rel="stylesheet" href="{#loader=template::url}styles/custom.css" type="text/css" >
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,100,300,500,600,700,900' rel='stylesheet' type='text/css'>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="{#loader=template::url}js/functions.js"></script>
<script type="text/javascript" src="{#loader=template::url}styles/bootstrap.min.js"></script>
	<script type="text/javascript" src="{#loader=template::url}styles/jquery.jcarousel.min.js"></script>
	<script type="text/javascript" src="{#loader=template::url}styles/jcarousel.responsive.js"></script>
<?php $system->importPlugin("vkeyboard");?>
<?php $system->importPlugin("fancybox");?>
<?php $system->importPlugin("jquery-ui");?>

</head>

<body leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0">
	<div class="headerContainer">
		<div class="headerInner">
			<a href="{#loader=system::url}/" id="logo"></a>
			<div class="mainNavRight">
				<div class="navbar">
					<div class="navbar-inner">
						<ul class="nav">
							<li><a href="{#loader=system::url}/members/ads/rotator">Start Surfing</a></li>
							<!--li><a href="{#loader=system::url}/instant/deposits">My Deposits</a></li-->
							<li><a href="{#loader=system::url}/content/testimony">Add Testimomy</a></li>
							<li><a href="{#loader=system::url}/account/support">support</a></li>
							<li><a class="login" href="{#loader=system::url}/members"><i></i>Account</a></li>
							<li><a class="register" href="{#loader=system::url}/logout.php"><i></i>Logout</a></li>	
						</ul>				
					</div>
				</div>
			</div>
		</div>
	</div> <!-- end headerContainer -->
	<div class="payheaderContainer">
		<div class="payheaderInner">
			<div class="payheaderLeft">
				<p>Welcome, <span><?php echo $userinfo["username"]?>!</span></p>
				<div>
					
					{#widget=wallet::user_dashboard}
				
				</div>
			</div>
			<div class="payheaderMid">
				
				 {#widget=account::user_dashboard}

			</div>
			<div class="payheaderRight">
				{#widget=ads::banner?pid:2&display:vertical&limit:1}
			</div>
		</div>
	</div><!-- end payheaderContainer -->

	<div class="header2Container">
		<div class="header2Inner">
			<div class="mainNavRight">
				<div class="navbar">
					<div class="navbar-inner">
						<ul class="nav">                                                                                 
							<li class="dropdown">
								<a href="#" id="dropdown-toggle" data-toggle="dropdown" class="myaccount"><i></i>my account <i class="HDACMT"></i></a>
								<ul class="dropdown-menu">
									<li><a href="{#loader=system::url}/members">Account Overview</a></li>
									<li><a href="{#loader=system::url}/members/account/settings">General Settings</a></li>
									<li><a href="{#loader=system::url}/members/payment/accounts">Cashout Settings</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" id="dropdown-toggle" data-toggle="dropdown" class="mywallet"><i></i>my wallet <i class="HDACMT"></i></a>
								<ul class="dropdown-menu">
										<li><a href="{#loader=system::url}/members/wallet">Wallet Balance</a></li>
										<li><a href="{#loader=system::url}/members/wallet/fund">Add Money</a></li>
										<!--li><a href="{#loader=system::url}/members/wallet/transfer">Transfer Money</a></li-->
										<li><a href="{#loader=system::url}/members/wallet/withdraw">Withdraw Money</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" id="dropdown-toggle" data-toggle="dropdown" class="adshare"><i></i>adshare <i class="HDACMT"></i></a>
								<ul class="dropdown-menu">
										<li><a href="{#loader=system::url}/members/instant">Summary</a></li>
										<li><a href="{#loader=system::url}/members/instant/deposit">Buy Adshares</a></li>
										<li><a href="{#loader=system::url}/members/instant/deposits">My Adshares</a></li>
										<li><a href="{#loader=system::url}/members/instant/repurchase">Repurchase</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" id="dropdown-toggle" data-toggle="dropdown" class="manageads"><i></i>manage ads <i class="HDACMT"></i></a>
								<ul class="dropdown-menu">
										<li><a href="{#loader=system::url}/members/ads/websites">My Websites</a></li>
										<li><a href="{#loader=system::url}/members/ads/banners">My Banners</a></li>
										<li><a href="{#loader=system::url}/members/ads/textads">My Text Ads</a></li>
										<li><a href="{#loader=system::url}/members/ads/loginads">My Login Ads</a></li>
										<li><a href="{#loader=system::url}/members/ads/listings">My Listings</a></li>
										<li><a href="{#loader=system::url}/members/ads/buy_credits">Buy Ad Credits</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" id="dropdown-toggle" data-toggle="dropdown" class="history"><i></i>history <i class="HDACMT"></i></a>
								<ul class="dropdown-menu">
										<li><a href="{#loader=system::url}/members/wallet/history">Transactions</a></li>
										<li><a href="{#loader=system::url}/members/promo/commissions">Commissions</a></li>
										<li><a href="{#loader=system::url}/members/payment/deposits">Deposits</a></li>
										<li><a href="{#loader=system::url}/members/wallet/withdrawals">Withdrawals</a></li>
										<li><a href="{#loader=system::url}/members/payment/subscriptions">Subscriptions</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" id="dropdown-toggle" data-toggle="dropdown" class="referrals"><i></i>referrals program <i class="HDACMT"></i></a>
								<ul class="dropdown-menu">
											<li><a href="{#loader=system::url}/members/instant/referrals">Referrals</a></li>
											<li><a href="{#loader=system::url}/members/promo/tools">Promotion tools</a></li>
								</ul>
							</li>
						</ul>							
					</div>
				</div>
			</div>
		</div>
	</div><!-- end header2Container -->
	
<div class="contentTopACContainer">
		<div class="contentTopACInner">
