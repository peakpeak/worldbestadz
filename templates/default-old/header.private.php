<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>{#loader=page::title}</title>
<link rel="shortcut icon" href="{#loader=template::url}images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="{#loader=template::url}css/styles.css" type="text/css">
<link rel="stylesheet" href="{#loader=template::url}css/base.css" type="text/css" >
<script type="text/javascript" src="{#loader=template::url}js/jquery.min.js"></script>
<script type="text/javascript" src="{#loader=template::url}js/functions.js"></script>
<?php $system->importPlugin("vkeyboard");?>
<?php $system->importPlugin("fancybox");?>
<?php $system->importPlugin("jquery-ui");?>
<?php $system->importPlugin("jquery-cycle");?>
</head>
<body leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0">
<div id="shadow">
<table width="980" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="280" align="center" valign="top" bgcolor="#EFEFEF"><div class="slideshow"> <img src="{#loader=template::url}images/sliders/slide1.png" width="954" height="280"> <img src="{#loader=template::url}images/sliders/slide2.png" width="954" height="280"> <img src="{#loader=template::url}images/sliders/slide3.png" width="954" height="280"> </div></td>
  </tr>
  <tr>
    <td height="33" valign="top" bgcolor="#EFEFEF"><div id="topmenu">{#widget=content::menu?mid:2}</div></td>
  </tr>
</table>
<table width="980" height="400" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#EFEFEF">
<!--DWLayoutTable-->
<tr>
  <td width="687" height="400" align="center" valign="top"><div style="padding-top:13px; padding-bottom:13px;">
    <!-- Banner space -->
    <table width="661" height="300" border="0" cellpadding="0" cellspacing="0" background="{#loader=template::url}images/bbox_02.gif">
    <!--DWLayoutTable-->
    <tr>
      <td width="661" height="290" valign="top" class="bbox"><div style="padding-left:20px;padding-right:20px;">
