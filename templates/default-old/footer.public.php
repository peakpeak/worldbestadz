</div>
</td>
</tr>
<tr>
  <td height="10" valign="top"><img src="{#loader=template::url}images/bbox_04.gif" width="661" height="10"></td>
</tr>
</table>
</div>
</td>
<td width="293" valign="top"><div style="padding-top:13px;">
    <table width="280" border="0" cellpadding="0" cellspacing="0">
      <!--DWLayoutTable-->
      <tr>
        <td width="280" height="38" valign="middle" class="box2t">Members Login</td>
      </tr>
      <tr>
        <td height="100" valign="middle" bgcolor="#FFFFFF"><div style="padding-top:12px; padding-bottom:12px;">
            <form method="post" action="{#loader=system::url}/account/login">
              <input type="hidden" name="submit"  value="yes" />
              <table width="250" cellpadding="0" cellspacing="0" border="0" class="loginform" align="center">
                <tr>
                  <td height="30" align="right">Username:&nbsp;&nbsp;</td>
                  <td><input type="text" name="username" class="input" style="width:170px;height:22px;"></td>
                </tr>
                <tr>
                  <td height="30" align="right" valign="middle">Password:&nbsp;&nbsp;</td>
                  <td valign="middle"><input type="password" name="password" class="input" style="width:170px;height:22px;"></td>
                </tr>
                <?php if($settings['account']['use_captcha'] == true) { ?>
                <tr>
                  <td height="30" align="right" valign="middle">Code:&nbsp;&nbsp;</td>
                  <td valign="middle"><input  type="text" name="captcha" value="" class="input" style="width:100px;height:22px;"></td>
                </tr>
                <tr>
                  <td height="30" align="right" valign="middle">&nbsp;&nbsp;</td>
                  <td valign="middle"><img src="<?php echo $system->showCaptcha(); ?>" id='captcha' width="150" height="60" onclick="this.src='<?php echo $system->showCaptcha(); ?>?'+Math.random();"></td>
                </tr>
                <?php } ?>
               <tr align="center">
                  <td width="250" height="25" colspan="2" valign="bottom"><input type=image src="{#loader=template::url}images/log.gif"  value="Login">
                    &nbsp;&nbsp;<a href="{#loader=system::url}/account/reset"><img src="{#loader=template::url}images/but2.gif" width="128" height="18" border="0"></a></td>
                </tr>
              </table>
            </form>
          </div></td>
      </tr>
      <tr>
        <td><img src="{#loader=template::url}images/box2_03.gif" width="280" height="5"></td>
      </tr>
    </table>
  </div>
  <div style="padding-top:13px;">
    <table width="280" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="280" height="38" valign="middle" class="box2t">Sponsored Ads</td>
      </tr>
      <tr>
        <td width="280" height="100" bgcolor="#FFFFFF" valign="top"><div style=" margin:10px 10px 0 10px;" align="center"> {#widget=ads::banner?pid:1&display:horizontal&limit:2}
            {#widget=ads::banner?pid:1&display:horizontal&limit:2} </div></td>
      </tr>
      <tr>
        <td><img src="{#loader=template::url}images/box2_03.gif" width="280" height="5"></td>
      </tr>
    </table>
  </div>
  <div style="padding-top:13px;">
    <table width="280"border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="280" height="38" valign="middle" class="box2t">Statistics</td>
      </tr>
      <tr>
        <td width="280" height="100" bgcolor="#FFFFFF" valign="top"><div  style=" margin:10px 10px 10px 10px;">{#widget=instant::statistics}</div></td>
      </tr>
      <tr>
        <td><img src="{#loader=template::url}images/box2_03.gif" width="280" height="5"></td>
      </tr>
    </table>
  </div>
  <div style="padding-top:13px;">
    <table width="280"border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="280" height="38" valign="middle" class="box2t">Recent Members</td>
      </tr>
      <tr>
        <td width="280" height="100" bgcolor="#FFFFFF" valign="top"><div  style=" margin:10px 10px 10px 10px;">{#widget=account::recent_members}</div></td>
      </tr>
      <tr>
        <td><img src="{#loader=template::url}images/box2_03.gif" width="280" height="5"></td>
      </tr>
    </table>
  </div>
  <div style="padding-top:13px;">
    <table width="280"border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="280" height="38" valign="middle" class="box2t">Deposits</td>
      </tr>
      <tr>
        <td width="280" height="100" bgcolor="#FFFFFF" valign="top"><div  style=" margin:10px 10px 10px 10px;">{#widget=instant::deposits}</div></td>
      </tr>
      <tr>
        <td><img src="{#loader=template::url}images/box2_03.gif" width="280" height="5"></td>
      </tr>
    </table>
  </div>
  <div style="padding-top:13px;">
    <table width="280"border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="280" height="38" valign="middle" class="box2t">Payouts</td>
      </tr>
      <tr>
        <td width="280" height="100" bgcolor="#FFFFFF" valign="top"><div  style=" margin:10px 10px 10px 10px;">{#widget=instant::payouts}</div></td>
      </tr>
      <tr>
        <td><img src="{#loader=template::url}images/box2_03.gif" width="280" height="5"></td>
      </tr>
    </table>
  </div>
  <div style="padding-top:13px;">
    <table width="280" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="280" height="38" valign="middle" class="box2t">Latest News</td>
      </tr>
      <tr>
        <td width="280" height="100" bgcolor="#FFFFFF" valign="top"><div  style=" margin:10px 10px 10px 10px;">{#widget=content::announcements?type:public}</div></td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF" align="right"><div  style="margin-right:10px;" align="right"> <a href="{#loader=system::url}/content/announcements">View All</a> </div></td>
      </tr>
      <tr>
        <td><img src="{#loader=template::url}images/box2_03.gif" width="280" height="5"></td>
      </tr>
    </table>
  </div>
  <div style="padding-top:13px; padding-bottom:13px;">
    <table width="280" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="280" height="38" valign="middle" class="box2t">Calculator</td>
      </tr>
      <tr>
        <td width="280" height="100" bgcolor="#FFFFFF" valign="top"><div  style=" margin:10px 10px 10px 10px;"> {#widget=instant::calculator}</div></td>
      </tr>
      <tr>
        <td><img src="{#loader=template::url}images/box2_03.gif" width="280" height="5"></td>
      </tr>
    </table>
  </div>
  </td>
</tr>
</table>
<table width="980" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#EFEFEF">
  <tr>
    <td width="980" height="67" valign="top"><img src="{#loader=template::url}images/index_19.gif" width="980" height="67" border="0" usemap="#Map"></td>
  </tr>
  <tr>
    <td height="90" valign="top" class="footer"><div style="padding-top:25px;"> <a href="{#loader=system::url}">Home</a>&nbsp;&nbsp;|&nbsp;&nbsp; <a href="{#loader=system::url}/pages/about">About Us</a>&nbsp;&nbsp;|&nbsp;&nbsp; <a href="{#loader=system::url}/account/register">Register Account</a>&nbsp;&nbsp;|&nbsp;&nbsp; <a href="{#loader=system::url}/content/faqs">F.A.Q.</a>&nbsp;&nbsp;|&nbsp;&nbsp; <a href="{#loader=system::url}/pages/terms">Terms and Conditions</a>&nbsp;&nbsp;|&nbsp;&nbsp; <a href="{#loader=system::url}/pages/privacy">Privacy Policy</a>&nbsp;&nbsp;|&nbsp;&nbsp; <a href="{#loader=system::url}/account/support">Support</a></div>
      <div align="right" style="padding-top:5px;">Copyright � <?php echo date("Y") ?> <a href="http://2gosoft.com" target="_blank">2gosoft Solutions</a></div></td>
  </tr>
  <map name="Map">
    <area shape="rect" coords="806,13,957,53" href="{#loader=system::url}/account/register">
  </map>
</table>
</div>
</body></html>