</div>
</td>
</tr>
<tr>
  <td height="10" valign="top"><img src="{#loader=template::url}images/bbox_04.gif" width="661" height="10"></td>
</tr>
</table>
</div>
</td>
<td width="293" valign="top"><div style="padding-top:13px;">
    <table width="280" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="280" height="38" valign="middle" class="box2t">&nbsp;</td>
      </tr>
      <tr>
        <td valign="middle"><div id="sidemenu">
            <ul>
              <li><span><span>My Account</span></span>
                <ul style="display: none;">
                  <li><a href="{#loader=system::url}/members">Account Overview</a></li>
                  <li><a href="{#loader=system::url}/members/account/settings">General Settings</a></li>
                  <li><a href="{#loader=system::url}/members/payment/accounts">Cashout Settings</a></li>
                </ul>
              </li>
              <li><span><span>My Wallet</span></span>
                <ul>
                  <li><a href="{#loader=system::url}/members/wallet">Wallet Balance</a></li>
                  <li><a href="{#loader=system::url}/members/wallet/fund">Add Money</a></li>
                  <li><a href="{#loader=system::url}/members/wallet/transfer">Transfer Money</a></li>
                  <li><a href="{#loader=system::url}/members/wallet/withdraw">Withdraw Money</a></li>
                </ul>
              </li>
              <li><span><span>Adshare</span></span>
                <ul style="display: none;">
				  <li><a href="{#loader=system::url}/members/instant">Summary</a></li>
                  <li><a href="{#loader=system::url}/members/instant/deposit">Buy Adshares</a></li>
                  <li><a href="{#loader=system::url}/members/instant/deposits">My Adshares</a></li>
				  <li><a href="{#loader=system::url}/members/instant/repurchase">Repurchase</a></li>
                </ul>
              </li>
			   <li><span><span>Manage Ads</span></span>
                <ul>
                  <li><a href="{#loader=system::url}/members/ads/websites">My Websites</a></li>
                  <li><a href="{#loader=system::url}/members/ads/banners">My Banners</a></li>
                  <li><a href="{#loader=system::url}/members/ads/textads">My Text Ads</a></li>
				  <li><a href="{#loader=system::url}/members/ads/loginads">My Login Ads</a></li>
                  <li><a href="{#loader=system::url}/members/ads/listings">My Listings</a></li>
				  <li><a href="{#loader=system::url}/members/ads/buy_credits">Buy Ad Credits</a></li>
                </ul>
              </li>
              <li><span><span>History</span></span>
                <ul>
                  <li><a href="{#loader=system::url}/members/wallet/history">Transactions</a></li>
                  <li><a href="{#loader=system::url}/members/promo/commissions">Commissions</a></li>
                  <li><a href="{#loader=system::url}/members/payment/deposits">Deposits</a></li>
                  <li><a href="{#loader=system::url}/members/wallet/withdrawals">Withdrawals</a></li>
                  <li><a href="{#loader=system::url}/members/payment/subscriptions">Subscriptions</a></li>
                </ul>
              </li>
              <li><span><span>Referrals Program</span></span>
                <ul>
                  <li><a href="{#loader=system::url}/members/instant/referrals">Referrals</a></li>
                  <li><a href="{#loader=system::url}/members/promo/tools">Promotion tools</a></li>
                </ul>
              </li>
            </ul>
          </div></td>
      </tr>
    </table>
  </div>
  <div style="padding-top:13px;">
    <table width="280" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="280" height="38" valign="middle" class="box2t">Sponsored Ads</td>
      </tr>
      <tr>
        <td width="280" height="100" bgcolor="#FFFFFF" valign="top"><div style=" margin:10px 10px 0 10px;" align="center"> {#widget=ads::banner?pid:1&display:horizontal&limit:2}
            {#widget=ads::banner?pid:1&display:horizontal&limit:2} </div></td>
      </tr>
      <tr>
        <td><img src="{#loader=template::url}images/box2_03.gif" width="280" height="5"></td>
      </tr>
    </table>
  </div>
  <div style="padding-top:13px; padding-bottom:13px;">
    <table width="280" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="280" height="38" valign="middle" class="box2t">Latest News</td>
      </tr>
      <tr>
        <td width="280" height="100" bgcolor="#FFFFFF" valign="top"><div  style=" margin:10px 10px 10px 10px;"> {#widget=content::announcements?type:private} </div></td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF" align="right"><div  style="margin-right:10px;" align="right"> <a href="{#loader=system::url}/members/content/announcements">View All</a></div></td>
      </tr>
      <tr>
        <td><img src="{#loader=template::url}images/box2_03.gif" width="280" height="5"></td>
      </tr>
    </table>
  </div>
  
  
  </td>
</tr>
</table>
<table width="980" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#EFEFEF">
   <tr>
    <td width="980" height="67" valign="top" align="center">{#widget=ads::banner?pid:2&display:horizontal&limit:2}</td>
  </tr>
  <tr>
    <td width="980" height="67" valign="top"><img src="{#loader=template::url}images/index_19.gif" width="980" height="67" border="0" usemap="#Map"></td>
  </tr>
  <tr>
    <td height="90" valign="top" class="footer"><div style="padding-top:25px;"> <a href="{#loader=system::url}">Home</a>&nbsp;&nbsp;|&nbsp;&nbsp; <a href="{#loader=system::url}/pages/about">About Us</a>&nbsp;&nbsp;|&nbsp;&nbsp; <a href="{#loader=system::url}/account/register">Register Account</a>&nbsp;&nbsp;|&nbsp;&nbsp; <a href="{#loader=system::url}/content/faqs">F.A.Q.</a>&nbsp;&nbsp;|&nbsp;&nbsp; <a href="{#loader=system::url}/pages/terms">Terms and Conditions</a>&nbsp;&nbsp;|&nbsp;&nbsp; <a href="{#loader=system::url}/pages/privacy">Privacy Policy</a>&nbsp;&nbsp;|&nbsp;&nbsp; <a href="{#loader=system::url}/account/support">Support</a></div>
      <div align="right" style="padding-top:5px;">Copyright � <?php echo date("Y") ?> <a href="http://2gosoft.com" target="_blank">2gosoft Solutions</a></div></td>
  </tr>
  <map name="Map">
    <area shape="rect" coords="806,13,957,53" href="{#loader=system::url}/account/register">
  </map>
</table>
</div>
</body></html>