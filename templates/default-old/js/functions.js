

function toggleMenu() {

	if($(this).parent('li').hasClass('active')) {

		$(this).parent('li').removeClass('active');

		$(this).parent('li').children('ul').slideUp(600);

	}else

	{

		$(this).parent('li').addClass('active');

		$(this).parent('li').children('ul').slideDown(600);

	}

};

$(document).ready(function(){

	$('#sidemenu li span').click(toggleMenu);
	cyclejQuery('.slideshow').cycle({fx: 'fade',timeout:  20000 });

});