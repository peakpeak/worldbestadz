var none    = [];

var basic    = [
			[ 'Bold', 'Italic', '-',
			  'NumberedList', 'BulletedList','-',
			  'Link', 'Unlink' , '-',
			  'Undo', 'Redo' 
			]
];


var advanced = [
			[ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-',
			  'RemoveFormat'
			],
			
			[ 'NumberedList', 'BulletedList','-',
			  'Outdent', 'Indent', '-',
			  'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock','-',
			  'Blockquote'
			],
			
			[ 
			  'Image', 'Link', 'Unlink' , '-',
			  'Table', 'HorizontalRule','-',
			  'Undo', 'Redo' 
			]
];

