-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jan 04, 2016 at 10:50 PM
-- Server version: 5.5.46-cll
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `advshare_main`
--

-- --------------------------------------------------------

--
-- Table structure for table `_account_activity`
--

CREATE TABLE IF NOT EXISTS `_account_activity` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `date` datetime NOT NULL,
  `mod` text NOT NULL,
  `type` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `url` text NOT NULL,
  `ipaddress` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=98 ;

--
-- Dumping data for table `_account_activity`
--

INSERT INTO `_account_activity` (`id`, `username`, `date`, `mod`, `type`, `description`, `url`, `ipaddress`) VALUES
(1, 'advshare', '2015-12-10 05:11:00', 'account', 'login', 'successful login to account', '', '182.74.195.30'),
(2, 'advshare', '2015-12-10 05:11:34', 'account', 'settings', 'updated security question', '', '182.74.195.30'),
(3, 'advshare', '2015-12-10 05:13:17', 'account', 'login', 'successful login to account', '', '182.74.195.30'),
(7, 'advshare', '2015-12-10 05:38:59', 'account', 'login', 'successful login to account', '', '182.74.195.30'),
(8, 'advshare', '2015-12-10 07:12:35', 'account', 'login', 'successful login to account', '', '124.41.220.87'),
(9, 'advshare', '2015-12-10 07:25:42', 'account', 'login', 'successful login to account', '', '78.26.162.63'),
(10, 'advshare', '2015-12-10 07:57:19', 'wallet', 'manager', 'updated checksite1 Bitcoin account balance: + $1000', '', '124.41.220.87'),
(11, 'advshare', '2015-12-10 08:01:46', 'account', 'login', 'failed login attempt', '', '124.41.220.87'),
(13, 'advshare', '2015-12-10 08:06:24', 'account', 'login', 'successful login to account', '', '124.41.220.87'),
(15, 'advshare', '2015-12-10 08:27:45', 'account', 'login', 'successful login to account', '', '78.26.162.63'),
(16, 'advshare', '2015-12-10 14:50:47', 'account', 'login', 'successful login to account', '', '124.41.220.87'),
(18, 'advshare', '2015-12-10 15:07:51', 'wallet', 'manager', 'updated checksite1 Bitcoin account balance: + $1000', '', '124.41.220.87'),
(20, 'advshare', '2015-12-11 08:53:09', 'account', 'login', 'successful login to account', '', '78.26.162.63'),
(22, 'advshare', '2015-12-11 20:42:05', 'account', 'login', 'successful login to account', '', '78.26.162.63'),
(24, 'advshare', '2015-12-12 10:03:27', 'account', 'login', 'successful login to account', '', '78.26.162.63'),
(25, 'advshare', '2015-12-12 10:19:59', 'account', 'login', 'successful login to account', '', '78.26.162.63'),
(26, 'advshare', '2015-12-12 12:12:07', 'account', 'login', 'successful login to account', '', '78.26.162.63'),
(27, 'advshare', '2015-12-12 13:18:37', 'account', 'login', 'successful login to account', '', '78.26.162.63'),
(28, 'advshare', '2015-12-12 13:41:05', 'account', 'login', 'successful login to account', '', '78.26.162.63'),
(29, 'advshare', '2015-12-12 13:44:51', 'account', 'login', 'successful login to account', '', '78.26.162.63'),
(31, 'advshare', '2015-12-12 13:55:25', 'account', 'login', 'successful login to account', '', '78.26.162.63'),
(34, 'advshare', '2015-12-12 17:43:33', 'account', 'login', 'successful login to account', '', '124.41.220.3'),
(36, 'advshare', '2015-12-12 20:07:38', 'account', 'login', 'successful login to account', '', '78.26.162.63'),
(38, 'advshare', '2015-12-13 07:19:19', 'account', 'login', 'successful login to account', '', '124.41.220.3'),
(40, 'advshare', '2015-12-14 07:42:41', 'account', 'login', 'successful login to account', '', '78.26.162.63'),
(42, 'advshare', '2015-12-14 08:57:00', 'account', 'login', 'successful login to account', '', '78.26.162.63'),
(45, 'advshare', '2015-12-14 12:56:53', 'account', 'login', 'successful login to account', '', '78.26.162.63'),
(47, 'advshare', '2015-12-14 17:23:23', 'account', 'login', 'successful login to account', '', '124.41.220.3'),
(48, 'advshare1', '2015-12-14 17:26:47', 'account', 'login', 'successful login to account', '', '124.41.220.3'),
(49, 'advshare', '2015-12-15 06:18:35', 'account', 'login', 'failed login attempt', '', '117.197.151.232'),
(50, 'advshare1', '2015-12-15 06:19:09', 'account', 'login', 'successful login to account', '', '117.197.151.232'),
(51, 'advshare1', '2015-12-15 06:19:40', 'account', 'login', 'successful login to account', '', '117.197.151.232'),
(52, 'advshare', '2015-12-15 06:20:51', 'account', 'login', 'failed login attempt', '', '124.41.194.179'),
(53, 'advshare', '2015-12-15 06:21:22', 'account', 'login', 'successful login to account', '', '124.41.194.179'),
(54, 'advshare', '2015-12-15 06:23:28', 'account', 'login', 'successful login to account', '', '117.197.151.232'),
(55, 'advshare', '2015-12-15 17:07:12', 'account', 'login', 'successful login to account', '', '210.56.106.148'),
(56, 'advshare', '2015-12-16 07:39:56', 'account', 'login', 'successful login to account', '', '59.89.94.104'),
(57, 'advshare1', '2015-12-20 00:18:10', 'account', 'login', 'successful login to account', '', '124.41.194.145'),
(58, 'advshare1', '2015-12-30 07:01:19', 'account', 'login', 'successful login to account', '', '124.41.220.10'),
(59, 'advshare', '2015-12-30 07:04:30', 'account', 'login', 'failed login attempt', '', '124.41.220.10'),
(60, 'advshare', '2015-12-30 07:05:55', 'account', 'login', 'failed login attempt', '', '124.41.220.10'),
(61, 'advshare1', '2015-12-30 07:06:51', 'account', 'login', 'successful login to account', '', '124.41.220.10'),
(62, 'advshare', '2015-12-30 07:09:53', 'account', 'login', 'failed login attempt', '', '124.41.220.10'),
(63, 'advshare', '2015-12-30 07:10:40', 'account', 'login', 'successful login to account', '', '124.41.220.10'),
(64, 'advshare', '2015-12-30 17:25:31', 'account', 'login', 'successful login to account', '', '210.56.102.164'),
(65, 'advshare1', '2016-01-01 16:00:16', 'account', 'login', 'successful login to account', '', '124.41.218.215'),
(66, 'advshare', '2016-01-01 16:16:30', 'account', 'login', 'successful login to account', '', '124.41.218.215'),
(67, 'advshare', '2016-01-01 16:22:00', 'account', 'login', 'successful login to account', '', '125.62.106.19'),
(68, 'advshare1', '2016-01-01 16:46:35', 'account', 'login', 'failed login attempt', '', '124.41.218.215'),
(69, 'advshare', '2016-01-01 16:52:10', 'wallet', 'manager', 'updated advshare1 SolidTrust Pay account balance: + $1000', '', '124.41.218.215'),
(70, 'advshare1', '2016-01-01 16:52:38', 'account', 'login', 'successful login to account', '', '124.41.218.215'),
(71, 'advshare1', '2016-01-02 07:14:54', 'account', 'login', 'failed login attempt', '', '124.41.218.170'),
(72, 'advshare1', '2016-01-02 07:15:11', 'account', 'login', 'successful login to account', '', '124.41.218.170'),
(73, 'advshare', '2016-01-02 08:00:28', 'account', 'login', 'successful login to account', '', '117.197.142.168'),
(74, 'advshare', '2016-01-02 08:22:40', 'account', 'login', 'successful login to account', '', '117.197.142.168'),
(75, 'advshare1', '2016-01-02 09:57:20', 'account', 'login', 'successful login to account', '', '124.41.220.66'),
(76, 'advshare', '2016-01-02 09:59:27', 'account', 'login', 'successful login to account', '', '117.197.142.168'),
(77, 'advshare', '2016-01-02 10:03:03', 'account', 'login', 'successful login to account', '', '124.41.220.66'),
(78, 'advshare', '2016-01-02 10:07:33', 'wallet', 'manager', 'updated advshare2 SolidTrust Pay account balance: + $1000', '', '124.41.220.66'),
(79, 'advshare2', '2016-01-02 10:08:04', 'account', 'login', 'successful login to account', '', '124.41.220.66'),
(80, 'advshare1', '2016-01-02 10:36:48', 'account', 'login', 'successful login to account', '', '124.41.220.66'),
(81, 'advshare', '2016-01-02 11:27:40', 'account', 'login', 'successful login to account', '', '117.197.142.168'),
(82, 'advshare', '2016-01-02 11:40:57', 'account', 'login', 'successful login to account', '', '117.197.142.168'),
(83, 'advshare1', '2016-01-03 07:57:36', 'account', 'login', 'successful login to account', '', '124.41.220.66'),
(84, 'advshare', '2016-01-03 08:01:29', 'account', 'login', 'successful login to account', '', '124.41.220.66'),
(85, 'advshare1', '2016-01-03 08:05:01', 'account', 'login', 'successful login to account', '', '124.41.220.66'),
(86, 'advshare2', '2016-01-03 09:09:48', 'account', 'login', 'successful login to account', '', '124.41.220.66'),
(87, 'advshare1', '2016-01-03 09:11:34', 'account', 'login', 'successful login to account', '', '124.41.220.66'),
(88, 'advshare', '2016-01-03 09:11:56', 'account', 'login', 'failed login attempt', '', '124.41.220.66'),
(89, 'advshare', '2016-01-03 09:15:12', 'account', 'login', 'failed login attempt', '', '124.41.220.66'),
(90, 'advshare', '2016-01-03 09:15:41', 'account', 'login', 'successful login to account', '', '124.41.220.66'),
(91, 'advshare', '2016-01-03 09:55:17', 'account', 'login', 'successful login to account', '', '59.91.247.169'),
(92, 'advshare', '2016-01-04 08:26:08', 'account', 'login', 'successful login to account', '', '59.89.92.108'),
(93, 'advshare', '2016-01-04 11:36:59', 'account', 'login', 'successful login to account', '', '124.41.218.228'),
(94, 'advshare', '2016-01-04 11:52:06', 'account', 'login', 'successful login to account', '', '106.192.44.159'),
(95, 'advshare1', '2016-01-04 12:20:42', 'account', 'login', 'successful login to account', '', '124.41.218.228'),
(96, 'advshare', '2016-01-04 12:23:41', 'account', 'login', 'successful login to account', '', '124.41.218.228'),
(97, 'advshare', '2016-01-04 12:35:12', 'account', 'login', 'successful login to account', '', '59.89.92.108');

-- --------------------------------------------------------

--
-- Table structure for table `_account_settings`
--

CREATE TABLE IF NOT EXISTS `_account_settings` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `mod` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `_account_settings_details`
--

CREATE TABLE IF NOT EXISTS `_account_settings_details` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `mod` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `hint` text NOT NULL,
  `group_name` text NOT NULL,
  `default` varchar(100) NOT NULL,
  `input_type` varchar(100) NOT NULL DEFAULT 'text',
  `data_type` varchar(100) NOT NULL,
  `options` text NOT NULL,
  `required` enum('Yes','No') NOT NULL DEFAULT 'No',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `_account_settings_details`
--

INSERT INTO `_account_settings_details` (`id`, `mod`, `name`, `description`, `hint`, `group_name`, `default`, `input_type`, `data_type`, `options`, `required`) VALUES
(1, 'account', 'pin_authentication_on_ip_change', 'Require login code on IP change', 'If enabled, a pin will be sent to your email if system notices a change in the IP used to access your account ', 'Security', '', 'select', 'bool', 'system::bool_no_yes', 'Yes'),
(2, 'ads', 'credit_low_notification', 'Notify me when any of my ads credit is running low', '', 'Email Preferences', '1', 'select', 'bool', 'system::bool_no_yes', 'Yes'),
(3, 'promo', 'new_referral_notification', 'Notify me when i have a new referral', '', 'Email Preferences', '1', 'select', 'bool', 'system::bool_no_yes', 'Yes'),
(4, 'promo', 'referral_commission_notification', 'Notify me when i earn commissions', '', 'Email Preferences', '1', 'select', 'bool', 'system::bool_no_yes', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `_account_users`
--

CREATE TABLE IF NOT EXISTS `_account_users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `address1` text NOT NULL,
  `address2` text NOT NULL,
  `city` varchar(250) NOT NULL,
  `state` varchar(250) NOT NULL,
  `country` varchar(50) NOT NULL,
  `country_code` varchar(10) NOT NULL,
  `zipcode` varchar(35) NOT NULL,
  `telephone` varchar(35) NOT NULL,
  `image` varchar(35) NOT NULL,
  `bio` text NOT NULL,
  `interests` text NOT NULL,
  `question` varchar(100) NOT NULL,
  `answer` varchar(100) NOT NULL,
  `regdate` datetime NOT NULL,
  `lastvisit` datetime DEFAULT NULL,
  `usertype` varchar(50) NOT NULL,
  `activation` varchar(50) DEFAULT NULL,
  `referrer` varchar(50) DEFAULT NULL,
  `regip` varchar(50) NOT NULL,
  `lastip` varchar(50) NOT NULL,
  `status` enum('Active','Pending','Suspended','Deleted') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`,`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `_account_users`
--

INSERT INTO `_account_users` (`id`, `username`, `password`, `email`, `firstname`, `lastname`, `gender`, `address1`, `address2`, `city`, `state`, `country`, `country_code`, `zipcode`, `telephone`, `image`, `bio`, `interests`, `question`, `answer`, `regdate`, `lastvisit`, `usertype`, `activation`, `referrer`, `regip`, `lastip`, `status`) VALUES
(1, 'advshare', '6916fd790e662a8709bfaa36dae8af66', 'alexrayads@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'What is your friend name?', 'robin', '2015-12-09 22:09:02', '2016-01-04 12:35:11', 'Super Admin', NULL, NULL, '210.56.105.207', '59.89.92.108', 'Active'),
(3, '33brushes', '05a671c66aefea124cc08b76ea6d30bb', 'leasystyle@gmail.com', 'Elisabeth', 'Dobkevich', '', 'Akradia', '', 'Odessa', 'Odessa', 'Ukraine', 'ua', '', '', '', '', '', 'What sity where you born?', 'Nikolaev', '2015-12-12 13:55:00', '2015-12-12 13:55:00', 'Basic User', '', 'advshare', '78.26.162.63', '78.26.162.63', 'Active'),
(4, 'advshare1', 'b60d8e698c9d8db35ae4b5649d0a7a69', 'alexrayearn@gmail.com', 'adv', 'share', 'Male', 'Vantaa', '01300', 'Vantaa', 'Vantaa', 'Finland', 'fi', '', '', '', '', '', 'r u ok?', 'yes', '2015-12-14 17:25:07', '2016-01-04 12:20:42', 'Basic User', '', 'advshare', '124.41.220.3', '124.41.218.228', 'Active'),
(5, 'putralon', '9127a2652f6e90e48d2f3e57462b727d', 'fabiocontraco1@gmail.com', 'putra', 'muslim', '', 'Jl.A.Majid Ibrahim', 'No.1 - OJ kopi', 'sigli', 'aceh', 'Indonesia', 'id', '', '', '', '', '', 'klub bola kesukaan anda?', 'lapendos', '2015-12-24 13:38:25', '2015-12-24 13:38:25', 'Basic User', '', 'advshare', '36.76.110.96', '36.76.110.96', 'Active'),
(6, 'advshare2', 'b60d8e698c9d8db35ae4b5649d0a7a69', 'bgmgharti@gmail.com', 'check', 'sites', '', 'india', 'india', 'india', 'india', 'India', 'in', '', '', '', '', '', 'r u ok?', 'yes', '2016-01-02 10:06:11', '2016-01-03 09:09:48', 'Basic User', '', 'advshare1', '124.41.220.66', '124.41.220.66', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `_ads_ad_categories`
--

CREATE TABLE IF NOT EXISTS `_ads_ad_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `_ads_ad_categories`
--

INSERT INTO `_ads_ad_categories` (`id`, `name`, `order`) VALUES
(1, 'Business', 0),
(2, 'Health', 1),
(3, 'Sports', 2),
(4, 'Education', 3),
(5, 'Others', 4);

-- --------------------------------------------------------

--
-- Table structure for table `_ads_banners`
--

CREATE TABLE IF NOT EXISTS `_ads_banners` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `name` text NOT NULL,
  `title` text NOT NULL,
  `url` text NOT NULL,
  `image` text NOT NULL,
  `html` text NOT NULL,
  `category` text NOT NULL,
  `geo_target` text NOT NULL,
  `country` text NOT NULL,
  `credits` float(10,2) NOT NULL DEFAULT '0.00',
  `credits_used` float(10,2) NOT NULL DEFAULT '0.00',
  `credits_today` float(10,2) NOT NULL DEFAULT '0.00',
  `credits_per_day` float(10,2) NOT NULL DEFAULT '0.00',
  `display_days` varchar(50) NOT NULL DEFAULT '',
  `last_visit` text,
  `added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `startdate` date NOT NULL DEFAULT '0000-00-00',
  `views` int(11) NOT NULL,
  `hits` int(11) NOT NULL,
  `package` int(11) NOT NULL,
  `status` enum('Active','Inactive','Pending','Suspended','Completed') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `_ads_banners`
--

INSERT INTO `_ads_banners` (`id`, `username`, `name`, `title`, `url`, `image`, `html`, `category`, `geo_target`, `country`, `credits`, `credits_used`, `credits_today`, `credits_per_day`, `display_days`, `last_visit`, `added`, `startdate`, `views`, `hits`, `package`, `status`) VALUES
(1, 'advshare1', 'trafficncash', 'trafficncash', 'https://www.trafficncash.com', 'https://www.trafficncash.com/upload/banners/08431502092015.gif', '', '', '', '', 0.00, 1000.00, 98.00, 0.00, ',Sun,,Mon,,Tue,,Wed,,Thu,,Fri,,Sat,', '1451808547', '2016-01-02 07:22:52', '2016-01-02', 1000, 2, 2, 'Active'),
(2, 'advshare1', 'trafficncash1', 'trafficncash1', 'https://www.trafficncash.com', 'https://www.trafficncash.com/upload/banners/04421331102015.gif', '', '', '', '', 488.00, 512.00, 65.00, 0.00, ',Sun,,Mon,,Tue,,Wed,,Thu,,Fri,,Sat,', '1451923912', '2016-01-02 07:24:50', '2016-01-02', 512, 0, 1, 'Active'),
(3, 'advshare1', 'trafficncash2', 'trafficncash2', 'https://www.trafficncash.com', 'http://triplethr3at.net/banners/ban8.gif', '', '', '', '', 278.00, 722.00, 123.00, 0.00, ',Sun,,Mon,,Tue,,Wed,,Thu,,Fri,,Sat,', '1451927539', '2016-01-02 07:25:09', '2016-01-02', 722, 1, 3, 'Active'),
(4, 'advshare1', 'trafficncash4', 'trafficncash4', 'https://www.trafficncash.com/', 'https://www.trafficncash.com/upload/banners/10481319102015.jpg', '', '', '', '', 487.00, 513.00, 80.00, 0.00, ',Sun,,Mon,,Tue,,Wed,,Thu,,Fri,,Sat,', '1451923908', '2016-01-02 07:26:56', '2016-01-02', 513, 1, 1, 'Active'),
(5, 'advshare1', 'trafficncash5', 'trafficncash5', 'https://www.trafficncash.com/', 'https://www.trafficncash.com/upload/banners/11063004122015.gif', '', '', '', '', 521.00, 479.00, 67.00, 0.00, ',Sun,,Mon,,Tue,,Wed,,Thu,,Fri,,Sat,', '1451923912', '2016-01-02 07:27:12', '2016-01-02', 479, 0, 1, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `_ads_banner_packages`
--

CREATE TABLE IF NOT EXISTS `_ads_banner_packages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `default` enum('Y','N') NOT NULL DEFAULT 'N',
  `name` text NOT NULL,
  `type` enum('PPV','PPC','PPP') NOT NULL DEFAULT 'PPV',
  `length` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `credits_per_exposure` int(11) NOT NULL DEFAULT '1',
  `credits_per_click` int(11) NOT NULL DEFAULT '1',
  `cost_per_period` float(10,2) NOT NULL,
  `duration` int(11) NOT NULL,
  `period` enum('H','D','W','M','Y') NOT NULL DEFAULT 'D',
  `spots` int(11) NOT NULL DEFAULT '0',
  `default_url` text NOT NULL,
  `default_image` text NOT NULL,
  `default_html` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `_ads_banner_packages`
--

INSERT INTO `_ads_banner_packages` (`id`, `default`, `name`, `type`, `length`, `height`, `credits_per_exposure`, `credits_per_click`, `cost_per_period`, `duration`, `period`, `spots`, `default_url`, `default_image`, `default_html`) VALUES
(1, 'N', 'Small Banner Ads', 'PPV', 125, 125, 1, 1, 0.00, 0, 'D', 0, 'http://2gosoft.com', 'http://2gosoft.com/banners/125.gif', ''),
(2, 'N', 'Medium Banner Ads', 'PPV', 468, 60, 1, 1, 0.00, 0, 'D', 0, 'http://2gosoft.com', 'http://2gosoft.com/banners/468.gif', ''),
(3, 'N', 'Large Banner Ads', 'PPV', 728, 90, 1, 1, 0.00, 0, 'D', 0, 'http://2gosoft.com', 'http://2gosoft.com/banners/728.gif', '');

-- --------------------------------------------------------

--
-- Table structure for table `_ads_banner_statistics`
--

CREATE TABLE IF NOT EXISTS `_ads_banner_statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_id` int(11) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL DEFAULT '',
  `views` int(11) NOT NULL,
  `hits` int(11) NOT NULL,
  `date` date NOT NULL DEFAULT '0000-00-00',
  `time` time NOT NULL DEFAULT '00:00:00',
  `credits` float(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `_ads_banner_statistics`
--

INSERT INTO `_ads_banner_statistics` (`id`, `banner_id`, `username`, `views`, `hits`, `date`, `time`, `credits`) VALUES
(1, 1, 'advshare1', 902, 1, '2016-01-02', '23:54:26', 902.00),
(2, 2, 'advshare1', 331, 0, '2016-01-02', '22:35:57', 331.00),
(3, 3, 'advshare1', 394, 0, '2016-01-02', '23:54:26', 394.00),
(4, 4, 'advshare1', 339, 1, '2016-01-02', '23:54:26', 339.00),
(5, 5, 'advshare1', 296, 0, '2016-01-02', '23:54:26', 296.00),
(6, 5, 'advshare1', 116, 0, '2016-01-03', '18:43:11', 116.00),
(7, 3, 'advshare1', 204, 1, '2016-01-03', '22:53:39', 204.00),
(8, 2, 'advshare1', 116, 0, '2016-01-03', '19:15:42', 116.00),
(9, 1, 'advshare1', 98, 1, '2016-01-03', '14:36:10', 98.00),
(10, 4, 'advshare1', 94, 0, '2016-01-03', '19:15:42', 94.00),
(11, 3, 'advshare1', 124, 0, '2016-01-04', '17:12:19', 124.00),
(12, 4, 'advshare1', 80, 0, '2016-01-04', '16:11:48', 80.00),
(13, 2, 'advshare1', 65, 0, '2016-01-04', '16:11:52', 65.00),
(14, 5, 'advshare1', 67, 0, '2016-01-04', '16:11:52', 67.00);

-- --------------------------------------------------------

--
-- Table structure for table `_ads_credit_packages`
--

CREATE TABLE IF NOT EXISTS `_ads_credit_packages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `website_credits` float(10,2) NOT NULL DEFAULT '0.00',
  `banner_credits` float(10,2) NOT NULL DEFAULT '0.00',
  `textad_credits` float(10,2) NOT NULL DEFAULT '0.00',
  `loginads_credits` float(10,2) NOT NULL DEFAULT '0.00',
  `costs` text NOT NULL,
  `hidden` enum('Y','N') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `_ads_credit_packages`
--

INSERT INTO `_ads_credit_packages` (`id`, `name`, `website_credits`, `banner_credits`, `textad_credits`, `loginads_credits`, `costs`, `hidden`) VALUES
(1, 'Text ads credits', 0.00, 0.00, 10000.00, 0.00, '1:5,2:5', 'N'),
(2, 'Surf ads credits', 10000.00, 0.00, 0.00, 0.00, '1:10,2:10', 'N'),
(3, 'Login credits', 0.00, 0.00, 0.00, 1000.00, '1:5,2:5', 'N'),
(4, 'Banners ads credits', 0.00, 10000.00, 0.00, 0.00, '1:5,2:5', 'N'),
(5, 'Advertising credits', 300.00, 1000.00, 500.00, 100.00, '1:,2:', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `_ads_credit_purchases`
--

CREATE TABLE IF NOT EXISTS `_ads_credit_purchases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `username` varchar(50) NOT NULL DEFAULT '',
  `package_id` int(11) NOT NULL,
  `method` int(11) NOT NULL DEFAULT '0',
  `amount` double(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `_ads_directory`
--

CREATE TABLE IF NOT EXISTS `_ads_directory` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `name` text NOT NULL,
  `title` text NOT NULL,
  `url` text NOT NULL,
  `image` text NOT NULL,
  `description` text NOT NULL,
  `category` text NOT NULL,
  `geo_target` text NOT NULL,
  `country` text NOT NULL,
  `added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `views` int(11) NOT NULL,
  `hits` int(11) NOT NULL,
  `package` int(11) NOT NULL,
  `status` enum('Active','Inactive','Pending','Suspended','Completed') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `_ads_loginads`
--

CREATE TABLE IF NOT EXISTS `_ads_loginads` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `name` text NOT NULL,
  `title` text NOT NULL,
  `url` text NOT NULL,
  `credits` float(10,2) NOT NULL DEFAULT '0.00',
  `credits_used` float(10,2) NOT NULL DEFAULT '0.00',
  `credits_today` float(10,2) NOT NULL DEFAULT '0.00',
  `credits_per_day` float(10,2) NOT NULL DEFAULT '0.00',
  `display_days` varchar(50) NOT NULL DEFAULT '',
  `last_visit` text,
  `added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` enum('Active','Inactive','Pending','Suspended','Completed') NOT NULL DEFAULT 'Active',
  `hits` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `_ads_loginads`
--

INSERT INTO `_ads_loginads` (`id`, `username`, `name`, `title`, `url`, `credits`, `credits_used`, `credits_today`, `credits_per_day`, `display_days`, `last_visit`, `added`, `status`, `hits`) VALUES
(1, 'advshare1', 'trafficncash6', 'trafficncash6', 'https://www.trafficncash.com/', 492.00, 8.00, 1.00, 0.00, ',Sun,,Mon,,Tue,,Wed,,Thu,,Fri,,Sat,', '1451912072', '2016-01-02 07:30:12', 'Active', 8),
(2, 'advshare1', 'trafficncash8', 'trafficncash8', 'https://www.mypayingads.com/', 488.00, 12.00, 4.00, 0.00, ',Sun,,Mon,,Tue,,Wed,,Thu,,Fri,,Sat,', '1451911229', '2016-01-02 07:30:59', 'Active', 12);

-- --------------------------------------------------------

--
-- Table structure for table `_ads_loginads_statistics`
--

CREATE TABLE IF NOT EXISTS `_ads_loginads_statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL DEFAULT '',
  `hits` int(11) NOT NULL,
  `date` date NOT NULL DEFAULT '0000-00-00',
  `credits` float(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `_ads_loginads_statistics`
--

INSERT INTO `_ads_loginads_statistics` (`id`, `site_id`, `username`, `hits`, `date`, `credits`) VALUES
(1, 2, 'advshare1', 6, '2016-01-02', 6.00),
(2, 1, 'advshare1', 4, '2016-01-02', 4.00),
(3, 2, 'advshare1', 2, '2016-01-03', 2.00),
(4, 1, 'advshare1', 3, '2016-01-03', 3.00),
(5, 2, 'advshare1', 4, '2016-01-04', 4.00),
(6, 1, 'advshare1', 1, '2016-01-04', 1.00);

-- --------------------------------------------------------

--
-- Table structure for table `_ads_reports`
--

CREATE TABLE IF NOT EXISTS `_ads_reports` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `username` varchar(50) NOT NULL,
  `wid` bigint(20) DEFAULT NULL,
  `user_comment` text NOT NULL,
  `admin_comment` text NOT NULL,
  `status` enum('Pending','Completed') NOT NULL DEFAULT 'Pending',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `_ads_rotator_packages`
--

CREATE TABLE IF NOT EXISTS `_ads_rotator_packages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `default` enum('Y','N') NOT NULL DEFAULT 'N',
  `name` text NOT NULL,
  `surf_timer` int(11) NOT NULL DEFAULT '15',
  `cr_per_exposure` int(11) NOT NULL DEFAULT '1',
  `cr_per_view` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `_ads_rotator_packages`
--

INSERT INTO `_ads_rotator_packages` (`id`, `default`, `name`, `surf_timer`, `cr_per_exposure`, `cr_per_view`) VALUES
(1, 'Y', 'Global Rotator', 15, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `_ads_surf_statistics`
--

CREATE TABLE IF NOT EXISTS `_ads_surf_statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `surfed` float(10,2) NOT NULL DEFAULT '0.00',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `time` time NOT NULL DEFAULT '00:00:00',
  `credits` float(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `_ads_surf_statistics`
--

INSERT INTO `_ads_surf_statistics` (`id`, `username`, `surfed`, `date`, `time`, `credits`) VALUES
(1, 'advshare', 1.00, '2015-12-14', '19:51:20', 1.00),
(2, 'advshare1', 1.00, '2016-01-01', '16:05:31', 1.00);

-- --------------------------------------------------------

--
-- Table structure for table `_ads_textads`
--

CREATE TABLE IF NOT EXISTS `_ads_textads` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `name` text NOT NULL,
  `title` text NOT NULL,
  `body` text NOT NULL,
  `url` text NOT NULL,
  `category` text NOT NULL,
  `geo_target` text NOT NULL,
  `country` text NOT NULL,
  `credits` float(10,2) NOT NULL DEFAULT '0.00',
  `credits_used` float(10,2) NOT NULL DEFAULT '0.00',
  `credits_today` float(10,2) NOT NULL DEFAULT '0.00',
  `credits_per_day` float(10,2) NOT NULL DEFAULT '0.00',
  `display_days` varchar(50) NOT NULL DEFAULT '',
  `last_visit` text,
  `added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `startdate` date NOT NULL DEFAULT '0000-00-00',
  `views` int(11) NOT NULL,
  `hits` int(11) NOT NULL,
  `package` int(11) NOT NULL,
  `status` enum('Active','Inactive','Pending','Suspended','Completed') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `_ads_textads`
--

INSERT INTO `_ads_textads` (`id`, `username`, `name`, `title`, `body`, `url`, `category`, `geo_target`, `country`, `credits`, `credits_used`, `credits_today`, `credits_per_day`, `display_days`, `last_visit`, `added`, `startdate`, `views`, `hits`, `package`, `status`) VALUES
(1, 'advshare', 'This is a test', 'This is a test title', 'This is a test description', 'http://www.google.com', '', '', '', 5999575.00, 425.00, 10.00, 0.00, ',Sun,,Mon,,Tue,,Wed,,Thu,,Fri,,Sat,', '1451912677', '2015-12-12 15:58:52', '2015-12-03', 425, 0, 2, 'Active'),
(2, 'advshare1', 'very nice site', 'advshare', 'awesome revenue share program that pays instantly', 'http://www.cashking.me', '', '', '', 928.00, 72.00, 14.00, 0.00, ',Sun,,Mon,,Tue,,Wed,,Thu,,Fri,,Sat,', '1451912677', '2016-01-02 07:19:27', '2016-01-02', 72, 0, 2, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `_ads_textads_statistics`
--

CREATE TABLE IF NOT EXISTS `_ads_textads_statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `textad_id` int(11) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL DEFAULT '',
  `views` int(11) NOT NULL,
  `hits` int(11) NOT NULL,
  `date` date NOT NULL DEFAULT '0000-00-00',
  `time` time NOT NULL DEFAULT '00:00:00',
  `credits` float(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `_ads_textads_statistics`
--

INSERT INTO `_ads_textads_statistics` (`id`, `textad_id`, `username`, `views`, `hits`, `date`, `time`, `credits`) VALUES
(1, 1, 'advshare', 203, 0, '2015-12-12', '20:55:37', 203.00),
(2, 1, 'advshare', 33, 0, '2015-12-13', '15:47:06', 33.00),
(3, 1, 'advshare', 96, 0, '2015-12-14', '19:49:19', 96.00),
(4, 1, 'advshare', 3, 0, '2015-12-15', '17:07:37', 3.00),
(5, 1, 'advshare', 3, 0, '2015-12-16', '07:40:14', 3.00),
(6, 1, 'advshare', 6, 0, '2015-12-20', '00:18:49', 6.00),
(7, 1, 'advshare', 9, 0, '2015-12-30', '07:07:38', 9.00),
(8, 1, 'advshare', 18, 0, '2016-01-01', '18:11:37', 18.00),
(9, 1, 'advshare', 37, 0, '2016-01-02', '10:38:18', 37.00),
(10, 2, 'advshare1', 50, 0, '2016-01-02', '10:38:18', 50.00),
(11, 1, 'advshare', 7, 0, '2016-01-03', '10:15:51', 7.00),
(12, 2, 'advshare1', 8, 0, '2016-01-03', '10:15:51', 8.00),
(13, 2, 'advshare1', 14, 0, '2016-01-04', '13:04:37', 14.00),
(14, 1, 'advshare', 10, 0, '2016-01-04', '13:04:37', 10.00);

-- --------------------------------------------------------

--
-- Table structure for table `_ads_textad_packages`
--

CREATE TABLE IF NOT EXISTS `_ads_textad_packages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `default` enum('Y','N') NOT NULL DEFAULT 'N',
  `name` text NOT NULL,
  `type` enum('PPV','PPC','PPP') NOT NULL DEFAULT 'PPV',
  `length` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `credits_per_exposure` int(11) NOT NULL DEFAULT '1',
  `credits_per_click` int(11) NOT NULL DEFAULT '1',
  `cost_per_period` float(10,2) NOT NULL,
  `duration` int(11) NOT NULL,
  `period` enum('H','D','W','M','Y') NOT NULL DEFAULT 'D',
  `spots` int(11) NOT NULL DEFAULT '0',
  `max_title` int(11) NOT NULL,
  `max_body` int(11) NOT NULL,
  `default_url` text NOT NULL,
  `default_text` text NOT NULL,
  `default_html` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `_ads_textad_packages`
--

INSERT INTO `_ads_textad_packages` (`id`, `default`, `name`, `type`, `length`, `height`, `credits_per_exposure`, `credits_per_click`, `cost_per_period`, `duration`, `period`, `spots`, `max_title`, `max_body`, `default_url`, `default_text`, `default_html`) VALUES
(1, 'N', 'Front Page', 'PPV', 728, 90, 1, 1, 0.00, 0, 'D', 0, 20, 50, '', '', ''),
(2, 'N', 'Members Area', 'PPV', 728, 90, 1, 1, 0.00, 0, 'D', 0, 20, 50, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `_ads_users`
--

CREATE TABLE IF NOT EXISTS `_ads_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `website_credits` float(10,2) NOT NULL DEFAULT '0.00',
  `banner_credits` float(10,2) NOT NULL DEFAULT '0.00',
  `textad_credits` float(10,2) NOT NULL DEFAULT '0.00',
  `loginads_credits` float(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `_ads_users`
--

INSERT INTO `_ads_users` (`id`, `username`, `website_credits`, `banner_credits`, `textad_credits`, `loginads_credits`) VALUES
(1, 'checksite1', 10400.00, 10350.00, 10350.00, 1030.00),
(2, '33brushes', 0.00, 0.00, 0.00, 0.00),
(3, 'advshare', 1.00, 0.00, 0.00, 0.00),
(4, 'advshare1', 9001.00, 5000.00, 9000.00, 0.00),
(5, 'putralon', 0.00, 0.00, 0.00, 0.00),
(6, 'advshare2', 10000.00, 10000.00, 10000.00, 1000.00);

-- --------------------------------------------------------

--
-- Table structure for table `_ads_websites`
--

CREATE TABLE IF NOT EXISTS `_ads_websites` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `name` text NOT NULL,
  `title` text NOT NULL,
  `url` text NOT NULL,
  `category` text NOT NULL,
  `geo_target` text NOT NULL,
  `country` text NOT NULL,
  `credits` float(10,2) NOT NULL DEFAULT '0.00',
  `credits_used` float(10,2) NOT NULL DEFAULT '0.00',
  `credits_today` float(10,2) NOT NULL DEFAULT '0.00',
  `credits_per_day` float(10,2) NOT NULL DEFAULT '0.00',
  `display_days` varchar(50) NOT NULL DEFAULT '',
  `last_visit` text,
  `added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `startdate` date NOT NULL DEFAULT '0000-00-00',
  `status` enum('Active','Inactive','Pending','Suspended','Completed') NOT NULL DEFAULT 'Active',
  `totalhits` int(11) NOT NULL,
  `memhits` int(11) NOT NULL,
  `anohits` int(11) NOT NULL,
  `package` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `_ads_websites`
--

INSERT INTO `_ads_websites` (`id`, `username`, `name`, `title`, `url`, `category`, `geo_target`, `country`, `credits`, `credits_used`, `credits_today`, `credits_per_day`, `display_days`, `last_visit`, `added`, `startdate`, `status`, `totalhits`, `memhits`, `anohits`, `package`) VALUES
(1, 'advshare1', 'trafficncash', 'trafficncash', 'http://www.trafficncash.com', '', '', '', 999.00, 1.00, 1.00, 300.00, ',Sun,,Mon,,Tue,,Wed,,Thu,,Fri,,Sat,', '1451719290', '2016-01-02 07:21:27', '2016-01-02', 'Active', 1, 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `_ads_website_statistics`
--

CREATE TABLE IF NOT EXISTS `_ads_website_statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL DEFAULT '',
  `totalhits` int(11) NOT NULL,
  `memhits` int(11) NOT NULL,
  `anohits` int(11) NOT NULL,
  `date` date NOT NULL DEFAULT '0000-00-00',
  `time` time NOT NULL DEFAULT '00:00:00',
  `credits` float(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `_ads_website_statistics`
--

INSERT INTO `_ads_website_statistics` (`id`, `site_id`, `username`, `totalhits`, `memhits`, `anohits`, `date`, `time`, `credits`) VALUES
(1, 1, 'advshare1', 1, 1, 0, '2016-01-02', '07:21:30', 1.00);

-- --------------------------------------------------------

--
-- Table structure for table `_content_announcements`
--

CREATE TABLE IF NOT EXISTS `_content_announcements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `type` varchar(50) NOT NULL,
  `title` text,
  `content` longblob,
  `comments` enum('Y','N') NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `_content_faqs`
--

CREATE TABLE IF NOT EXISTS `_content_faqs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` text,
  `answer` longblob,
  `order` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `_content_faqs`
--

INSERT INTO `_content_faqs` (`id`, `question`, `answer`, `order`) VALUES
(4, 'What is cashking.me?', 0x4f145938e48b1424910499b33d3c0f71602f88674ecdac12d1e5f7e1ce2401364c341f8ae484151be66efc663740b17e9978cfce7f52fe23162841c47dd6ecf7f0f2d69903d634a7c083187eec3f702db1c515573acf2969bf2ae95494c9a9b2b045e4384053d3c8ad58e628ff6ea9981e5436d7a5bdb9ef1d5f29807202cfb6e8dfae0304e5a26c5f0fbd5c96f082244014007f37ca4b5533486c3d20bec3ffbbc244a703d29f953d524066f0ae65619358bbad2124d974d516c5916401b5655d20a18b114bf802d4d7d083a553e373c935a97898eda2bf853a702ac148c4d1cffc854c5c1e7491f2f0208490b415a50fefe2f6d5feefc6736b2f7de3e9f567ce39c4463bf322ad02314b8a6600b1de7bb09f2fe864d0bc4f6c5b08e374bc3ae6d778a618b5d22a1150e468762b4c0d, 0),
(5, 'How do I register and start?', 0x4f7156f7996c775e7a268b4ee7de6160431da3d9f466f00b08e620cda7807ef6eda0e84403575d911acc8ebf1fa1860cd8d93b507aa07f634fed2d10f9b2390a525b99d7aa463aae3d2c442638cac4689462cf1f05b03b57a189acb60764cc96b4dbc8aa4c992aa83375b6f7830ad12fa9c0568981a765ed998a8249f96fb04de2321c8b11ba0111047fcbcfa849c2d383ef81ce8db9c0f75af2797e972d14ea1fcdbd11946509d29c428f4305b402d30520e45af6d2bdb861c044d82432c486a5c418a42a60770071b895a09a8a68def00415451eb874726b0eb6eb8e76cc1b2ab45c75d679c36687dbfd4ac6d9afb3f902b6ee26f71c9585aef4450d85a51d36be385c21571187e39a7787481204a588211fdd9515c3d1ee9bb2074bef664c36030e9af8382af0532389a550e44b2a, 0),
(6, 'Is Advshare a hyip, ponzi, pyramid scheme, or illegal?', 0xd18e06fd5152d4a1e809381f7aa4f01e46c781ce1023b307fb58649b3db922a285e348f4b0424c2a7ec408d0ad5d07bde2840a395af0044116d4c7042dbeba87e06c338d46d83d8a3705a1090babbb1b870359910f071b9b50cfdf9b071ed44a0f12d1d4810c4dae15c7126cc4d5a4c26b1ce18dc0642a1c96a88a794e595d6389c708f9be631edb759028507a2cbfcf9219ecd564489077fb1972108abf00d57ba1b98615be27252aa20bda1c150f539712720a5fc60a9433bc6f530b12a7798fba9c3b6dae43e77f55690a13bb4e692286f1b0f68a02353bf36abcae55f068d334f14f1311c3a398dacfb048187ea0acf228729c464cb517994b5531e18986cc55613a9db81a51f109b3704837c6d28358a471ceafe92f6f259b5fa823159722cce44a9efb42e705d98b12a358c427, 0),
(7, 'How do i make money with Advshare?', 0x431c4b66b71879876ed9903cef5e5dba9d00083692f12c95d08f8b4a10dbeacac65189946016617474c2478a3a04a8473e7c317c42df60bd9806c77856d2065a70d349ae14085d4c6a05dd3bbca3115b8fa6d0bc499a8b29e751a6f304d38ff0b6ee56f9baf171e3fc072f5e14062317af123ea88b9e28f890e92e7c32dfe829e7ef29ea86f3937f17af3e9f9c557d146edeac09bd6663a1e98abc17b7b24f628758361cc6ea821f39b87f9d9bc66e4b5806794bc07a5867276d0cf1a6db8f8e9ceac9b48201444634338504dc399a46860cb0eea62858c6620f1a308f4d7ae79dbd6ff5b7ea9dbed08ac2dd1ce26d3d8fc90bd5f2bf152e40ee79d5156e4322fc42986d90a48d1b77aad4906fc7989c185ca018b54371adfd4fd936f4860342, 0),
(8, 'How much cost for each ad pack?', 0x29b1c8078ba2cc72d26ba685b3300c97ec6da7dba848a8a1c7f8daf53f664ae7a312d4b2a0080d97c183c657582227f5b4d9a470df62070da2a1afe43ed17c4f, 0),
(9, 'Anything else i have to do for earnings ?', 0x6dce7f53f0b16a3704a9979239391b50b3a1fc510241eba55c08c42234260edfe618047abfd6d3370057afd31d212db496228feaaa7149f1c33cef944e6ee36f, 0),
(10, 'How many Ads credits i get for $10 adpack?', 0x05abe78dbc5e657ae2d0708626fa134413ea7a5e94ed17f6b82756fcc26a1b99400165b424f7bd6b6d0eb4d1672e63612dd18179dfdd9a8da49fbea40e60c0c98e7cbafee3809063514557e82ba186683e85dd768b77b377652b255e1ceb0e10, 0),
(11, 'What is the repurchase balance?', 0x275eaed167fb1a2cdf550cad28c1b724b48a6a961e2bf5d8c002ce27d09cedab78e0c3db1e4f2c57735bd2bfeaad8b0f7c25c361b7fec9487774905f56b093d1b86d8df719680d2df34279a7fb3894d2408e58ebd186394e41c527c138eda253a614837a7a75d07603e371d3ec47a909ce8c6f0f7c162786965d8926ddade4aab9d8d5e0abfceacd4f153ad9fceca5802979a584c77cfb83e3af840acf8066dd82f8b77e52ba7615823db4634f7a0282ff48f84333bd20a9eca915043aafc890daeb9366d0835288edbff722502a2f39a28aa76bbc3a35cb4fc470dbac106a322b28799e7a27c55ff109d8c2b96f3a81, 0),
(12, 'How long does it take for my purchase to be added to my account?', 0x1e53233e01ef3374874043bcbffcc732bb74b2d0f70a5507bbe6f3b58549edbd4d4876d91af56fe3f793b507bd5eead1, 0),
(13, 'How to withdraw money?', 0x0984f9b6d03a49e782deaa7654a533ae3845c4df75740e24d60445b14e239c2a6f93d9a7fa9f0d4a141c6dc05f5e4684e2d48a3c6422c8c3582c49223e9da05b32ae6ce83d88542550624f2900da0e098cc44401d055758605498e63a84c7c6ba07002c439bf2673031abeed4f5c228ec9959a67dc1f025c7f27d29fdae234c5e93e73d0e5e51a7705cc8f7ef09af254148314046a676e320ed5c40ef1655480636afc21c0598a9711c59717ded049725af9ccbe55415359374e5500b6982a1f72f6897903d7e33c2be8439db5d1ccadccc142e76f674e5e53b11eb39c27951c185ca018b54371adfd4fd936f4860342, 0),
(14, 'What is the minimum withdrawal amount?', 0xf8a4ea9716acc26a9cdf54c63346edc888f64539f90c19a806a2fab0f2c2f88e9610cc14dbd862c4ec54e7311c9aaa3c, 0),
(15, 'What is the maximum withdrawal amount?', 0x0c9983e6b4885e0867b87f56927e285888f64539f90c19a806a2fab0f2c2f88e39edda47aef140052190e9acd658eb42e79870b9c24bacd08ff29f5ef3a38db8, 0),
(16, 'What payment processors can be used to fund a wallet?', 0xa1d4ffa72517c64c54d57915ffccf3bd328d98650a055b637063e21c04e7b80c8077cbac3d6da030e467c55f471a9af24d569aaec39e497f323f1f14caa3b7e1, 0),
(17, 'Can i withdraw in any other electronic currency?', 0x626ba475c9f722924180e226fe383b2a1de78fea354dc243c71e31587432e47f9e2312c3ccba5c7d78569614fb680f5a3ab64e86efa779dc2e27cc6fa10ad323b4d9a470df62070da2a1afe43ed17c4f, 0),
(18, 'How much is referral commision?', 0x52cd5cadd4ebdbe11712e8c889212f94b3516ba2b44e447e4a30efcb2cc9c53dab23d8fe662d91b61deaf5ad52a3492a, 0),
(19, 'Where can i collect my commision?', 0x7e6298359ad37cdb5457b07a1e30ff3f53365c9f49591c5cdc78f8b14f83f3201e4044b70fed5794081e1a1c87971b11e988ffd5011b563b3a5b199dbc2f484cc3f524b594937890e2156901d74f916a7c35412ee79c59ee0dcf24b892dcb1c0c06e820a6154a198569cd70e41b7a5d1, 0),
(20, 'Do i need to make deposit to participate in affiliate program?', 0x8ae32ec8ddeb32985590d0b5eaa1d0b0555c5c40d8b7c2a6eb2271b4b0be1037687c97ebe90e74f6ced766824ab1d5cc889e338610c1c0c56808785040693e040034fe8421206c81e2c68b2c7b95f6ba11f52cd6ffbce34c628dafc69a79aeaa22cce44a9efb42e705d98b12a358c427, 0),
(21, 'Is Advshare a worldwide opportunity?', 0xa5f0f8a0cc2950142105330d70fcb14472a637b44768a57c3f38ee6093bbb09c1ed278260a5d036b933538bb1db037cbd9b7e04dde266ba1175aaefcf01c9d96d84ae1b47423b014cb2c8ed7261588ab2b2c00573fd5c48b6ac67917d68e6034da5c9c2ebd0cc6c865dcd0bcd13bbe69b7c0881ab0c3fe3a235623b731da443b, 0),
(23, 'Can members compound their earnings?', 0x510d5f4c9ffc3be5307cd8489f771df331ecc12b0b282494729713fcf850b1dbe1e0e2d7dfbaed5057af257021e4247d905e43a4d466241a41b25c40ad7fc823a10649abeb93309f7f267a3b6410c911ad751a56db024ee36163c52daad669825acaec8385797acb6808770c45ff84fe531abf8859317ab34ae35d87a81900af, 0),
(24, 'How do you surf daily?', 0xa1677bf7b60def4047e230eb8f692f1ea72298d2ef72f46f5a52c1c15a9f245bc5594a5d77b70229f820801d0252041e96a060ee02a989a05f04425f9e22d4ddff28e1bd2989ff94a839f796bef13b7f107d10b6a3fb26a46850a48b95cc6f72619883c24a53bdbb674d6a73e02a95be52c130c53b5af6ff25bebe085d89def216282bd7b83e8c131f8ab42e65b52c848030d263d9fbee690219889a5ae17d20, 0),
(25, 'Why has my account been deleted?', 0x32d461d4db77a1e637e511d3fffbceee2ff8b24931d65f11fb18e4831a952ad7101f9d87fe7712ba8a131593e9723ceed5451448132f895549fe28c8af0ab9f92d8f6ccaec315838aa374721b88df1ffac4c99e7f7141d83ff03729228b0c4af91dedf9a3fee57928f2d6506816407a69f5325a6bd9bc68eb247afceb9e4de781a987b455b4fbbe6d91285b4517d3f074274ad77b7ec7a24d0e19c2a59a2a809101ccb553ae02f6d8effe5bbfb065943ec3b0d5436cc814aa4608b096bf6d396360c5375d385bc1c20b215ed53a1487918bd8c61c99fc3df02a0eda1951602ec4201a27a31782f2389ebf325b44b04de1cb91bfa75cb42c670f9673b95b44fd0d9e1fe7793015072431964d188d152ae26087cf22beddbb25977408711d8634cf50a55ac2cb04564700cc0cdfbf262d252118aceab00fa4d65eb82c1caa41464, 0),
(26, 'How long does it take for an Ad Pack to cap out at?', 0x44bc870b350e5149cc6269fc543bcb4fb3c03940bf788f42021297c574228143f27174a5d381938d307dfafc8ca8477e506efa5eabbe21adf06be1ea9c88de38d346cf1c85b92a141fa66feaabc41a39a3ff6444a6f01b3594ec4f0d02a8c8783c1acd3147a529351f96c544a874759f143fd8205f559daa0b0396df314dc557e9d8ccee19ce7d9793c3d59c5011b903cb88af6558ea79981e2894cd65c191db28d519b519178c7c88fb590b91c6ce7396ec56005e8af62c84c360ac8b162415b8ccd66465ff519610fa052d38c362c1d54fdefc0de5ac5096812c64945ef62e, 0),
(27, 'This site is really nice. i want to setup more than 1 account. can i do that?', 0x88d33872ae4bdb2b807cef45b0b99c9c7f8d04fab70174582fd06e8dff9068e63e48de8093c1977825c27e46df73c8e95dbe979b6a48690dae921b8a8d3bdfa10c71fff2594aad59583de0960936ab0092c6617f9e6cc3b78011249842928facfebfef3ad24904b734ae46ccdc407fa03ca244950d11688502416ae519235b8bf82a66280f9c51a99bb560c497f2c4451dd75c4fd22aab8efb2f90ebc2098ea1ea0b3e1e14cf9986699bce106ebcc96ca6c9e1460db3185c866e9e48f4991af5a0b7eb1968233679102a27b7c1d71cd7, 0),
(28, 'Can I refer my family member or friend to this site?', 0xbe14c6ef6e375e44d69ee1988c70ff55cc6ea73ab427bc0dc654bf815b021e4215d959dedf7001eb5623e6a978becea89c2cdd5ea56d7a46f4b9f902d1d8651d535caa449a22d3d71513a139350ca2612f6c4610c4fc5c81df6c85dc90bc270222cce44a9efb42e705d98b12a358c427, 0),
(29, 'What kind of sites do you allow?', 0x21f9764c3b44c48ad604fbd5b0a5a6fb0414d7b0b31f09120c701fa437b64f8cc53d1df808f4bb7b8d4d7f703d212e631f08fe712f1a4cf4136ca8343bab9f3db9a9ea535afffe7cadaf5caded62e31b2979f3cfa12abebfd0b4d11f165e333cd31326271f910dfd1181ca0b9b6377626a717db11c08c93ebaa34be63c97435f724dd644ecb879ed3deee65c1273b141a7ff9cfc072e38361842298a504c6b16fed02c982279df66d0de67092ace795c2d64094f59839f07a3636606916064b3abbdda4b1efc584cf914daf50b10ee0e0d49d21eb3deb6cadba9051059d07d4e8015aa05aa09d4f58f2c6f3dfd8be0fbe174660359b58cb34b3796ead0278f0f899ce34f5217662b623b3a1b8659d137edb19867054bbd7a135c7b27b840644bfa8ff1efb953eb5c344fe0b91ca4fa678e2a59f12e757b7f5c25d45bc60087d89519853519f5aa2fc34b221dd1269d07a4eecdfa875926d712c090c6f742d963, 0),
(30, 'When is revenue shared from purchases?', 0x8a17a74083e573cf2bd3166d64a4f2392bd74ac37a22d616c6441b6541be4834ddccc8a005183559f6ddc10d34806ea8d92f2ac7fe2fdc099a14fea570b7670d8752e57381c617d01124792f47be9faf, 0),
(31, 'If I choose to quit using your services after I''ve made a purchase, can I get a refund?', 0x843c2053a5ce314cd113423d26dbb37674cc1bb0e302c4d1652255d6368abf42b5af8533f0374e601012ad5b53adc6a2e58ef93915e096a430321c193be666cbac958f6b8ced0bc3b26224cddfe6ce5dd510b1599782e8b493f9ef4dcb0129e91a941ab834fcabbd96eafdfa06fdd11f5537fa6b863a0b7456b1c5d1bd2c2c9722cce44a9efb42e705d98b12a358c427, 0),
(32, 'What does &quot;share revenue up to $13&quot; mean?', 0x156e21e4614c1c675e15e6613d76169940cc28d27d9fe89df88e1b05eba104258e8e6ff7b0a5743f2708296dc5079879b4a758528000d2e4c6b886f57209681ced7d82d8f58907b0ab0d5b726e244ff857e8c969a7f7ac29a7de480709f850948a844c61472ae6d683a1c39e6e5e83fe39cf78590b21aa3f9f1413ba3a002f56eb2a88424187ed38141810d937adb4fe4fd6fda00ba674097a879770b0db691efda7fb556412596cab4c27044a35f0211efdd4a2e658a833b16f2b75d63f14dc947fae4754d1c7f3e23382f184194477724c8d05258f64a6e258fd6a9d2677e6e4b81469f51e00aa1131e13fcecf51c3adc2a2542b29f197de9abc510c1898ea980d99c41f4b7ad24aaa3b6f9d879065f87838c30753686904ef2d40442559f4bc6775532959ab18ea7290cbda3c4dd168032efb070ee6baeb9ca10d77254ff91cba087218a21e3892b2c5ed77947cf3ee3c361d67dcf262e8fca86595bd73542c64ba447ef3ec317b2daff4801e71c89e81987ae6afec14afe720e0b5add3154ea8965bcd58905db1e91a11ca4a3173e6a58fb04b21599b4298eacbdab8b519fda7fb556412596cab4c27044a35f02125bb404dbb1dd95ab63c36e5c440de859b67fa996a0257a2ef96b4b82f4948554d7d06e7af9dc607362db562bcc2f097df50bed38513e782dcd52f7ffa1ebcbc, 0),
(33, 'Can i change the sponsor?', 0x06a65929f5f84202841cbb72014799ebc3af78d9a3676d3c5ff6a53c65ce22e8, 0),
(34, 'I still have some more questions.', 0x0726997ade8458da4d5fd955e91418159afe79f9db79f8fd29f8c5e7fdb610f2d60b396941bd1ec311da9ffe7d8bba656d51e21f0282ca8a729750e745f8f9e76e101deb879f8fc0d0d1ac8c2a0f47fac06e820a6154a198569cd70e41b7a5d1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `_content_menus`
--

CREATE TABLE IF NOT EXISTS `_content_menus` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `_content_menus`
--

INSERT INTO `_content_menus` (`id`, `name`) VALUES
(1, 'Public Menu'),
(2, 'Private Menu');

-- --------------------------------------------------------

--
-- Table structure for table `_content_menu_elements`
--

CREATE TABLE IF NOT EXISTS `_content_menu_elements` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) DEFAULT NULL,
  `name` text,
  `title` text NOT NULL,
  `page_id` int(10) DEFAULT NULL,
  `url` text NOT NULL,
  `type` enum('page','url') NOT NULL DEFAULT 'page',
  `properties` text,
  `order` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `_content_menu_elements`
--

INSERT INTO `_content_menu_elements` (`id`, `menu_id`, `name`, `title`, `page_id`, `url`, `type`, `properties`, `order`) VALUES
(1, 1, 'Home', 'Home', 0, '{websiteurl}/index.php', 'url', NULL, 0),
(2, 1, 'Register', 'Create an Account', 0, '{websiteurl}/account/register', 'url', NULL, 3),
(3, 1, 'FAQs', 'Questions', 0, '{websiteurl}/content/faqs', 'url', NULL, 4),
(4, 1, 'Ads Directory', 'Directory', 0, '{websiteurl}/ads/directory', 'url', NULL, 5),
(5, 1, 'Payouts', 'Recent Payouts', 0, '{websiteurl}/wallet/payouts', 'url', NULL, 6),
(6, 1, 'Testimonies', 'Testimonies', 0, '{websiteurl}/content/testimonies', 'url', NULL, 7),
(7, 1, 'Support', 'My Account', 0, '{websiteurl}/account/support', 'url', NULL, 8),
(8, 2, 'Start Surfing', 'Start Surfing', 0, '{websiteurl}/members/ads/rotator', 'url', NULL, 0),
(9, 2, 'Fund Wallet', 'Fund Wallet', 0, '{websiteurl}/members/wallet/fund', 'url', NULL, 1),
(10, 2, 'New Deposit', 'New Deposit', 0, '{websiteurl}/members/instant/deposit', 'url', NULL, 2),
(11, 2, 'My Deposits', 'My Deposits', 0, '{websiteurl}/members/instant/deposits', 'url', NULL, 3),
(12, 2, 'Submit Testimony', 'Submit Testimony', 0, '{websiteurl}/members/content/testimony', 'url', NULL, 4),
(13, 2, 'Support', 'Support', 0, '{websiteurl}/account/support', 'url', NULL, 5),
(14, 2, 'Logout', 'Logout', 0, '{websiteurl}/logout.php', 'url', NULL, 6),
(15, 1, 'How', 'How it works', 6, '', 'page', '', 2),
(16, 1, 'Plans', 'Our Plans', 7, '', 'page', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `_content_pages`
--

CREATE TABLE IF NOT EXISTS `_content_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `title` text,
  `meta` text,
  `keywords` text,
  `template` varchar(50) NOT NULL DEFAULT 'default',
  `category` varchar(50) NOT NULL,
  `index` int(10) NOT NULL DEFAULT '0',
  `order` int(10) NOT NULL DEFAULT '0',
  `alias` varchar(50) NOT NULL,
  `content` longblob,
  `privilege` int(10) NOT NULL DEFAULT '0',
  `status` enum('Active','Pending') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `_content_pages`
--

INSERT INTO `_content_pages` (`id`, `name`, `title`, `meta`, `keywords`, `template`, `category`, `index`, `order`, `alias`, `content`, `privilege`, `status`) VALUES
(1, 'home', 'Welcome to Our Company', '', '', 'default-home', '', 1, 0, '', 0xd34bb223cbcb0ff44797d7a3f7082ff8e34a673b9542886d60aec38a6725b280e5c9390adf3184bfd609ecd28a62523ee2aae4faa82279ff717e5810e7b601dcd03d01e6505bfb8c8895cf1713a9c2ec4c12623e6b2b2fac447062bfb2c621a5ad36066ce65f4aaec233ce80979170c207c1691acc12d3d224c3ac4fd9d0c3e5a09c30b648eb230a2b66e3f63dafc8e889824affb572c4d2a4655a1752fafc3a8e995cdb53e01e3615fe53f4c9bd7c6cdbd221a5fe890d699c5b23d20c4dbb232254380b422aff5a1f2df6ca9fde451bd48d8f241f3c333e4bd054ae778607016e9b2132b8072cc7cc1e5a9cd58de0a3f5186c4179628829d22500366afa8127e12f6eae4d7f7574b6a1cdc190667c4cf311b95a4258703831ec1740731191f4b1a429b3c160bbe68d8bbe64ab1d279d24d781035782d8b45aee0359f0b903181352e41465afefbb07e09b95bd985001b8fb5ea9593623c0b86c698ca0ba7e2bf8c8ee5789e6b80a7559102034e71adda081b2ade71547470e02dadc7ef32fbf657d3177f1da12fcea8713765cc42232c1b8e917a134d8070f372c543a625610ce72f70065c5023791f4b3f6c5113ee1314fedf15bd87dc844181aa345b2832b32c127e3c1219e66849b47fc0649c11fea85c2f20a6f4582a63fb91be04b1c8293a8929a08ab3c74fed0d9825e71c687cf51a1e13489e16bfb82aef5b571ad8a8826b2c16fdf9c58d80f6e6f25a101160a5f1e9513ab8bf4414e8ee5f1b958f6d5106e4244e541f130ddd3a41d544a5aa6c4a153f697586708a95e6269119957c809b0b34b341fd88c6928ca31894ca25a2d412df54554faec0d6acd2eb60fe689fbaa87959ad8dbef3d455be6470e534a9950c06855f807abedacd91b3ae2b2910b81ebdbdc27d26e2710af17f4adb8c510faec706c889e852da90f531f66010658bb10ddb3eb6c564c15c343551095ebb42f7b29aad207c687d762c846149825f093446fa73f687fdeb5e2ac6cfe2693150c110a9d11e0f460c7e7b5531ca5fff55d2031786eb830653c2968557409548afcb9bade93c88cbfffc8def0e10d25971ee9bbe5bc5d376628992a58cb89aeeaa1cbaa53f7cd44fe128dec0debcbcbe9d25bbf085128988fd7a4539761b47448a172c0d12c3ad16c1866594076a79027218603ae9cc4510e7a6c22ab63968c37cc7289304458e8eda1628f8a152a326d08cac3274980124410081b0715b1c2424ba6aabbf17b72b52724a33094fe8e1e3cf6986878caab3c2adac291048bc53cfc29c59a66c82882e320895834e715e4973a7e380fd34a20c34bffcc34cd27cecc7579336098ab1e8c685359138a60ba306af7901fa1ddfc5bbdc7071a3703f177a9c4423c262c3ff524fb209a679abc4d95137e1a27b7ad20ac57fa0c0b3b7deeba648755631fcca3da94bd05df150a638bf32b18fd2b221ce92f09995f61daab4ad1af6d22cf8aed1a628dcfd6f2a47c08874db34c120ac44e48154af292fc14021019fddd731dac6f207c71ec0abe94814a62f5ba89a4feb00921870aa32185f3e4fcaa20c3ae698d412840f98565c9d0180225089ab79f7cc440cb1c76d504e2083ff9604876a561b579834aab739bf35c980915dc9e05ebc978164179d05d73b56c60aa7df99dda51b0364e241a3cbce415dd125b157edd77529c3d6f17475c4aff189d9a943d23597b19336dde97f9758cf11efae5eaec1a0f85b066ad6426f86e08ae6f8794e67b9221b3a24a15df066e04a86f386db27af3579ce2740a0d608317ac372cc9902c7759285c9fc64b02b8d44a961df0fcb55b8dd28b2e49358b10c1ca285011c933bcb28355179a62e1d01f8e438ddd47817a5755b72c6ae222c2a92cb80a427377c3b991dc0e626136cd48b013aa8d677be7ff672b09ff76b52f52d2973c657542877dfabc7f97d6dd57d677a52e35a0643d9e73fa4d34f6d63901b821a09a146493fb4932ff42ebb3099c02f4752b93f9cf8837451f43499f2321ab72cde208770b87bade4c4ce3f7b22a00a0639fc3f441cc00cf52a4f58b8c2c3765bb3087db629d41b77a093417adf5d76003e10cb2eef3877709bab4dfcb4bdbf9cac9ffa62faf4f0b15711cc3e24e13aadf8d3c9833485544a65a9af5a60ac056a5124667c4a0f617da264a9ebfbbe9da2d530164f6c15de0a69f749d7e74b6e8dfdc7388878332f231a76988eb082769c3d23f7fdf86a35ee0c96350fa87f8c8a1282049e14518663c79ff606e9b3876ad81c6be162fce6535c7ce5c67e4e03959afae26040b7d13d433a93bb981d0b1e0e330e0bae744aa78e78cb4102c57432d320cdc632c3ca78b0734deff5fe2df3bfb2d22690c66bf0104a036b36056b8fcf7e034fd871c95b1c9e47441f3461c36501e931beabd0abd05f5486e1b7ca92f96497b07d065a7f4fdf2cfb9d72eb77fd5e7eff7a49af0f208598a402f42ab8faf69d9ca7d03c8dd0494fcce97069fb7ac03429d18e516bb3820c46f24e975e7ee73850b2cb0a1e694e975cd31cb455ce34607920676feb9ffa5a6bfbdb9b79d6cd1039bc2c37deedcf154a38475ea356144da58f9ae556cc9f609a04e9b02390f69b0e48c3f50f15fb61b55a1fa3a9ab869a925f01520b08e55e3e053ea731327dd3630efc9fecd5cbd09b4337ebb0ca3c2880e9b4ac22b61f68920c157ccc762960bc2e257843c8293ba5a29d3ae9a84eb40c5ff3bcf115298893c08cbdc2e8c9658fb6639dc0e626136cd48b013aa8d677be7ff674d9330a53e81bd4914b886e2157602066d8dc77818babd3acc81a4e26f3dbd36f2d3116200958bfd6a57accf526d4018c076614514b15955768a01df775f3860a986464f790502cd53ed30abbed4ba256598116153e5f55744fa8541f827825bc51abcb7e02d7e96247916da6032f541db3de88d8892a109068052fea1738a0648267f34e7fd49bd55787bb397e3360d8c25b807fa669c5f4e75732427b2cda3452ce51849f874d47dd0fdc0584e9c7872ccdd48c906bbb103cd48c9f575d9738b1a98aec827803715814dee5ade03d88b468554203c6c5caed0e32416d65ae655bb82b80209861702736a9ae63b7362cfdd003f4bafdff5196ee2a96c9f709483edd7b4fe16d45b50e703f4dbffa17e5af6666c66d2b4b34bb764f7600a70e90f39ae7df8014d7bf33f743cf3df3d6767505a4c7a6a0783b1439103c345aedd8fb05c8b7c1653aaee4ecdcb6a4c0285d427cd0624077a2b75316317888de9285f01ca4150902c677189b5cd507b88effdfb485240a562691c8dd5f04dc6fcfef036d0eec2ee5f55e0fb00ca75a6878bff618bc0ed1c126bf8fe696cca979b6679bd55410b8cf392dbd2a26fe711ae174f2c5b342818523afabb18c1bb81154b887717acd8ea86c4ddcf9c0dc6742bbcdbd221a5fe890d699c5b23d20c4dbb23acf2e65c503b58b1475867c36d8419ae558f4e7d7e5296831af7eba37ec43586de26409c80fb414d8c0fc9509930394e5c1ca78cca90f9549ecd634e29ac970dc1251825b1217d04b9d5c9c4d86f494e5e9e5333b489cf46a9ab70b0a01a123d7043cb20728c46f4569b03ab7435b53fd8e428b6c2d093d62e50391ad026608d4eaf19bec4d3a45b85d4b5176cdbd49bb0e4a9a674aa26d3a6204c3d0bf6a2673c031b73c6c5da9cf6cc49950ba403d6835b426fb8733996a6504669c47eacdfc80ecc11ffbaaa683b3b30a55cc487cec48b2f95ad754a5d068b500c3cbde10875588542ebff905b1bf7e6cd7e3836c36b868264f847b29107dd34f996e1f486e329cb00470861ab60cd15098ae2c493630006a460dd8301fc17c2c26b2114b511d08a0da90cbf80dc0c434e2500e56599fd0b8e42db0c2823d67abda87bfd01c059d2f1cb59b96e0408c35f17d7c69f4c659e66a0394bf25e35197277f8f5d195acf360215529bfc3dd3e9fbe2e01aae95444dccada4beb44767dbc3cbf43df6b438459b4f04a7564e68bc260d57ea1333ce83e0466088e1e794e424daed5953230d23ec8b5efe6cf98302429dff81f6fc7ba7bbe8081dec9b2565d44ee8ac1a09c30b648eb230a2b66e3f63dafc8e820652c969985a974422390fa43d74566465e4995ea3e25398eb8cb6cbea047cc2865fdb6d307c656c1050ff4567a58c004663005b9e1d7fa04d7ec7c7ff4b0cf6f79d031d4f3cea6db4eb2ca186cec4a32207d42522b790de58bc5abff0ab6468601f602b23ad9fef3d4e74b89b273b817ec94663b6bcdc991ad11580e5d02c282ba7ab707d0e72f0038e1786a6466e906cd3074182c80864efdf9a431d4f8f58b050ccb96a51667e512673811a777b04fb8aebbffac6c1150a4e66b293d022d1dc7af08db6d0748a9244ce9e199d73530c744e14329125991e6a64baf4521803bc90592d5b09ead789f0bfb009d29dd5ca78b85ce8488a7dd7c740445558d3c4d4876d91af56fe3f793b507bd5eead1, 0, ''),
(2, 'about', 'About Our Company', '', '', 'default', '', 0, 0, '', 0xf1f9e77d01dd17043bf79aab9e8c7502156f96d0372374f282948a0dc21f525037507ae665d1f214b26909dd560e4008f7a9b1f4fdad37942208c27a30ef26a525f093446fa73f687fdeb5e2ac6cfe2650341e67fd8bb4e84b538deb45eb1e781e70d87f37018141fd6b780362771325ba71283ee67485e43d7a0b3a1e0cc69f6cfb66a0cbf4f6b8711d81a946677bbdde08c81064655983870070bc38a49dbfcee028bf64d59f6a15fb35fec40e66e5353c78a0ea49dbf64caa705ed334acb56b9bb3c0350eae4dba0c16c9166174856bb109afd72faf70cf5e8355974ae822fc9ed28d6f6d8115fe3cd5e05f82069150f96546a55eb8e34c8c9bd99583907f77b3009af66cc576b7a865a10ef4b75fa9dfb1a3548774743f058d9664569426b08bb8ec0f416581797f642464fc997e43f1ad60750a9697b20e65116d18df965b135ca3fd6522af64da632439ed5d1df89a8bf27dd29d7ac7befab5c2a23814e4afe2154897c5a04e2c0830f39720212c5cc5920cfba67819cd2dd1437a191b50d0e9712d13faa96dd82ef18bd44ce5d1a08b8744ff9e9f1777654c074409a55a3b777c7e0a003aa0e4ba025efe672020f38215fcbf002f2214a039279ae5a8b1621dd57b86d37b24a99b84be85a340965210855f9180eed8292e35d3d73a0e08bc21012427f2f56dd975d39830825c3a4f5990b35f7b00164b7051fd61fc22b5e295767e616a581f11241fd8031294f66221cfa4087d4fab2f8299d03a472f2f71330a39a9356c9f11e49b05eb7f71a8447babad652b4a17e70021b825528a9202f1876a8f7c058bfb13bf9017d02ad05f96a5465f73f7d1ddeb0e285576eefe83a5ef8b7a768ed69db895448f8538663e874a13b138c47282f66a393b7292a58e9f87960e6e77540e6252d4151adf2f8e5b18027dbabc384ec0a8941f4c3fa07d34b70a20af5c3f5a62daa09201b6673d20edfe894d6b1bdc29c791ad3ac7bfff36f56b08f02ced9accc0344af89817d4430c5cc7d67dd4c04c53a52226b8a241d3388a8c20340530f685f49a2aaeee1f0f1ddf18a140c88dc77b5362893837378eff59cfd71582116dd0e4549e46c9ce806ddf81fdb3fdc23f1e4a2deb98352120f6e2c40ac6507e13f5b7e67c674e8d2f5853c74745fe0f4ab46f67b397b08bebbc07c283c1b2d551a17c51f440a472bb5e876c4f10b631c70cd007db8c4842cf1e06f6d193e8b4365e8a8e9fd5cde857a1adb4e9816c0859c032ad753c02fe8cbd217362120e542d15b0adde6da8e93a3f58962d8a9fc94d1b27a77737d6f99b66aad9ed412402ce55ac51ebfed96872de3191f806fe744bc12bcd708b7c0c95e10787a8651839c5fd51696e03bfd85368bdf738fd559991139c93148cf7dfd4e961bcc9adb20abaf11b8d2a90528c7330ff9afdff6c7665d86c634a2d8408292d5ceb82fc0da07cc905701aae7cfaf414990c34c07fcb0cab908b74ed904bbd194c107b41838c5356305ca515bb9c1c9757eb929370450ef564287fbea06d9915822b0624de328f3977146de8d879cc0bc424bfbbdb8fc24a6a5f35f5bc9be41a74acd9f4ef5f73d8ba22258d357019a790e153d3ac29455ab18f5de3b36e1d1fd8939f10ae29df4eb8a22ed5ccaf59ea252a7b6b094e15fb5c1114dc049736fb4b77afa688c4a12b1562b9d7fad5d095b0986b19d326a8d382dfaae896e84ac9ea012814c6c2ffad061f1ad8b5c76dfdc74f1f4a43de36ca88a6a11627138e52098adcd999c5bcf5beea7d02e1ffabf4882ed9e8f94693d2b80f59ce85ecb386d56559403c58f1c32556feb32d89ac4cfab8e261123fb951d21e89f18fc843b018e26525eb9256465bc0a167fe1034f7c09e92f7a4047c23d72400ce00242e970e8411b6b78e2aaa7c4e836ce724263943c97e2234980ddb1d57b60c605286918b29fff9da06b6245e4f89f29da4384a81fd50147b7078959347a28a3b5f1d0d0d362e731653bd69096a9045ef0175474cf4c8765021d5335b5ae8b426fb52b41266d8fb22bb70d8b1cf23187d3c7ea67e2f8d82882c610898548f44cda865f943c331bf580be9fbd78414532f14f3e9b134030eca7663eb40d08f3a7ec4cb0baf79430df64c90d12213457e2a885a02cf8975a47147fcdf5f6026f3eae180d40b85f8ddbdc268f9efb5969fb2fd84cac7ad21d34dc52bd0fda4b071acb44e3caa9e18dd8498692036d53994, 0, ''),
(7, 'Plans', 'Our Plans', '', '', 'default', '', 0, 0, '', 0xf1f9e77d01dd17043bf79aab9e8c75027ce982af587d21b68ccd91bbef6caa6d2766f917c0e7d86719daedcefcc1db38d8bc5b6f99a4fba8386fca303916fad5efef7b7df2e1432049358e05e94e8abc28c681818a89390d66f3da1372cee5928e11038d2bd125498c9d010c6eac20c98f139f136b9e31daf67cb6aa99e4e6ea03f726419d85c3822439f27d003fe926043df88ed48166923112014fbd7beb6a52192623f0f4717b54b989f25d2fc27ab232aff159ab0b64dd78236a2790aceac17a10dd174447fb7a676b9f75b640667b104188e9d2e49a40dc57d4ee53bc9b2baf888cfb6cf8c2b2b40380aa2aa3a3eef648c081453044b459c5efe2dc6feb92fc14021019fddd731dac6f207c71ec0411798fae338000d8b7ee76eae9089c255964268491f307176a9f2e268af2100411798fae338000d8b7ee76eae9089c3d1b5d6c82573516ed7f1ff0bbaa3ce6596344beccff5b8110c0a0a936a1372be32c71cc269eb97b9f01f6baeddb098da844ad129cf3fdc91fddcad6765177a6b099addfc97e35d05c6cd8d6883634402ad525581cebf5574a02c4e06ac673373a83abaa0a805fea6513c46b8dee417b222ec03d19aecb227fc2f612d42d18abd7e06d75c96c27c1cad1eaa0fb5706b5364fc5df136ba87714dd120d8a837e76a69dd07d5656fa4fdd176207a7fe7b2129ef1446ea5c37a7ad94196df016b620e612090dad8047d7544453d3a0d52f6e7d779cb3c24d51b487bd1f1ae7e546ee855ae654254c4da4f46908fd323148ddbf171b083c1e964dbbf4d5396e80de06679cab196d85cec38f9191222a94c570e301e2c4df20e43b65188f5f4b66af68f86878be321f7bca65f4a407087e7b95d7bc1034aad46e1985a5b21bfe29cea50f2d9442d977a9761443549aa88d145f16b481102590d6f9caef0f35cf4abd15a916dacde1d61011d6acb9b8db085ed4972a57a28a14492f9ac2afc3e96298e26523d7ac9c6c63e8864ea0b7515c807fd4156b230ef69eb919d3f3f7cdcd19225569357e31a9708f9417bf24980e142fce3030e6cdbb182df9a5371ff7998e4f615d74d739c31cb41fc5ef413614fb5275222f1c2d27655f4055103244ced00f52e84dcb3790393abc5a12e7a7565e55680ae79cfdc58a99732f5c412d4b1b07341674bd1385b8d6ea688dc1d19f87b7f5aa2cbd0fda5722de912aa4def5554eedc7735a8c6391e8ac8d16b8c18187821eb2cf506e39f40d795cfb05055f406312f517d83e43098ec685c6c84f4c8a7525807a78e68358bec76278994a8641fac2b1d3a08bdc0554f9a7fbc8541c10c3801120f8e7911cffc8c9a6b39685eb3c0c4b4cea90e177c9b21881c99fa68b63850b9e5031582f58bb53f8aa72cd640ea41aa3344370ddad0d756614ab2dacdd58cedb51d3d299b5008ad6e77422051eba38a209874ad757e3558d48e5b5cbb350ea1fbbce4ed81072bd80ab98bfe9a866ae0883110c3cbbe5508a60aaf3e3ff0f0c71deb5e09f4bf619e972f525331d6b3adf1f328f2ca695ea72aa6cf4500b57ba3b8ac40e5059be957ca93ccd3fbc6bfc00b612a17ac110d6b28a3d95e3ebe83a6dd4c90baa8fe622c2828ad2052dc1190d0c487bb1ffa8e4a139b655d65b9128974275f850624f4a97e55000451c2ffb0ad891a3acda58268ae9811760c89f9b74d110177940e3757b19280a1c66c45e9d0b643f3a153773a5cb92cb76c7163f5025943e7875db4d5b8405c9115c58cedb51d3d299b5008ad6e77422051e9a9d25f40e901a595539e3118298004592385d6fb03ece45c9d2ce442ba1ec7dd7f70a84a610c3c4562f85721f160d652426e220844bb9b2607ccb92ed89b2fe835b426fb8733996a6504669c47eacdf1a08099fc54c5a7a8fe689fab0cb2aa5fbd197976d04424d7c73d2c9232873f2cd051c566e4935719b705df32fe55b22e25fcfd10396b0489e4030a599b8f804835b426fb8733996a6504669c47eacdf1a08099fc54c5a7a8fe689fab0cb2aa5b02f9dba1216b20edd13fb95d49705895c742915b8a99dba57f695066e982297bc91048b39960617929b7a44a6237601ec0712b1ab41945c5832a8f35ce1817c718f3edd2a2a80a39e3a9058ad5b69fc11d8dfebb8c88eb73663ae6f38446c3919d029c4c7010344b02279bc7d9ed8cbf3b54568ed287119d4135bb85b41f60683df89dcb66f3e46470428ea25def5fd0a4069922d952b97d0c6af78fe7407cf195a12cf708347edbd1d3890dd9b4d0521218d96494d2ad0b7ef880bb314dc88600ec48f6a518b4b6afe09bd574c819a1be69ab5618ce9a17433a62efb5c290e79de617ad858cfb8f3fbc236345237b8a24a15df066e04a86f386db27af3579ca24a15df066e04a86f386db27af3579cb079ab480fe4daa516fd97d3b8b15e55f5a65e16cae69e7cd85a4085e50591431e611295f33f216eae80e1b1182185c6, 0, ''),
(3, 'terms', 'Terms and Conditions', '', '', 'default', '', 0, 0, '', 0x46b98d3c9bad2c516385663026d92b56a1ccdae6de95c6ff126cbfbbe746e0fc439c4642decd8c9488a760dda797162588826d0715b5b74e3b97a594f6d1206d925ce576d552828b891ee200f53a7037bf7310b00c5022a112657d2ea73ba0eeb93282c50161478942fce17194a68b2ccfe57b4283d738b2b1746f065aa41639f9b24a1a1a1b63600fb9f4aa6ed17ba8ae0e60cc32d0ea404c21c0048633bc6de4788e0acb0f12c46b1d85b5151d9be3372360b2c56aed1c7f8582abce4d5669c9c4d36bdca3d5cf343072a352c7a46546c8d27a0725d7e737497321001ec6f3fa9f92c190e3eb657791a26133022b1283026d1e070e1e953a4c8d965fe96b9c6b2b4f386e3e3b52e33e81ee1bc3d8126bacff90ef87b83283e1f3c961224ae1adf4fdb6b57e0f4bdb0e90367960ab8b912de4c8bc624bfcb9572e7a90e9fc372e77982db63da0c94c5aadc122bee3d57ba184099694308a57bd2eda2abc51f026146474674f12654c78454ef8830e386c7d1f84b6f0b1f09bd2879b75f895f8561a76d372ade1fe45259bd5792b201fa419077170f82bb72c852de516a1e7befcb3786398a46f2298c25faafe18cea7fd8063fe86de1a41e6114e6a324611f7a49571d4d83dd4661198b6dff812fbbcab519594021c994f4b1cd67ff1c23af3e645f64745f70647615cc2e147f80684e6301fac01b5e3348c5061c9c0b8716aa6cec6809f85b09017bfbfdac62dda2415f29d231e520187c48752dcf98312de56727710df281fa770ebb9ab34661efcd248d00982ef351264549cc9c63e4e3eb01d7ebf117e64055b3da9f8ebbff962f0ffdb3535e0f1a388b372c3b72af689da3f52792ddc6a6ea38d4dca11d8768334fd308759db07ef2e29552426e1690ed1bc16d90461960694cd63b4b076bbd8a3034182ae93207c36f687cb8f34fed13baa141d19e6cfb4077924190dc068f550271551b8e7fe9a62ec5baca483f845886b347a6557d128622bf37cd881f0beb0ca8eb6728f8f0f854e9a1849cf5177161301bdc43450f1f4b9e35a4350a0319a966b2da3765344ad87fe2d5a0d5194ab21227509f742a29cfe40c4c8b8df7a1f16f8dad4a689c2be6cf517c7ae30e209fed225e633151b6fc3e573f5f96ca0981aa2ae18c34f9e7fc78c52415ae5911de69325d92e2182f3ed851e3e1dadca807b33810c8bd596578e45d5cc9f3fce2296b0a93b00ddc3d7c1607129dcc8505992207ee23a90ae61d42f5235d9cdc735e4cd7212376f7618413b2140a2b8daaebf854d5dc55baecbb62568056131d8330723619272e142c6f8c22cc1e4ce7804ea6e49fe31953dbd5f4e64e37fa4c6070795f6ae82e3ab2a7af058ae0c6148d2a139b270cb7ccf34a1fc874546298f1814ac61629deefcf2c8c98a22ca19d40522382675d57d33ea708780289cdc0ecdb94b6a70474871772fb713d631bb0806b76c3ff8b69031624423ace8754091b34bf13bf15deea76eb34d3f3dc90bf7e2bc9438c3fdaec78b4a964f6f9fb13acb6e26eb8abd1f95de83f9167e3dc685c088e8b40149051b80aef120c9b341445794b429c7c9617597bd8c32b5f94a36a3a5c2263d7bc48224bf69d5a060fc1581531f7ed37e2f6802b45a9ffdf5c7ac8cece89f2d005c8eb78181ced2565cb87eca8a5d4a2b5afab85be424313497fc4f7921a0ec797fd58b63c9dff9340c1e15035444f54260b1b93737f6fabe0a337ebf8f76aeae9f2791a59fa27a30db5dcad0db79bcc7f58c128c90aa329ae247a13546aa6e4f99cf2164576ccde650db6fcadd3d7a7227026b35742ce3ce56ca4014f70bec59f5b7af060db87a0e7249de97807be9924ab1917c64edbdde275a3ee06ca434eda53aacde9435906d780f50de65e76d2669d1e1a8bc98fefe52411e4babb0fac1dfa95ef0eae4b86ca23feadb699e244b89e1ec71ee4861d7f5ec83aa1cb770ecad584a277cb2a47cb248e2790a0c71cc79a3fa6537cb2fbe21d2f066c4864ddb50859063d7b11f514e50b4b4dcfafdbec9e492e97bfdc1ea972d5a7b3741e1f8cdec164200b45c70a1f5e7f8b92ec9ad4c8a2ebdde0a946d19ebfa3456f42d0fe0b2efeb2d436764640807d7f9709fa95a1f55d57b005e0a3cad3e1aeb03190c1d2901720e0143e7d597a7921563963dd47b030f7eed3d86ccde8b991b1e8dafd080848ba6123fdd9f4ef1a0fec55b36f0b8ea8085f432f3361392b9dc2e57d38a0eb617331d8e33e652ce15a642d6dcfd7f384620929a90441148a609eab114fa4b6a79d68304050c356a8d3d0eda3d2fb9f45352a3534ae987947a6d91a1fa2f4a6133dd74363f2a3ecbbc3166226ce9d22c448e4bc062048eed4e4504c3d1d6393f9511baff4bf2849a0d84a644dda7b8d912d5f2bc0524e4b79813707d9637b7adbc89b3484d5f4a9c453cd04e36425d361a4ecf89d57ca76b8e1190ca5490303f6649982734419efb69255f7b26bee34d9d56b369f282ce6bd57cf5284df4e46db695f62a5fb1d8a70fb4ac678e246861cff678ca95e37b875178244f46abc5c4d03d989c7513a45e1ba2ee581fded4da014f1eabb90c4c643f80bbb53b63712d5ecbe2c023092e05a7262301a9fb01423f2ce99785a6a2eeff5cf3e4f7e015d30417d18f42da7cd709e939549261e8f04230d9e97a041e262f78b50a776d08ae67bd222be4eeb79a991dceac38f47eb761b0caffc61071bbfec13bc4f8e8b37d71131b274843ae2dcea7459aa13fc8a4dc19a45f1653448e26806ac2252621a947c446274df081c92f40c21c84c3ed6d326ee30ef71619cb3c4b2f2a67b6a5c84232ba6a3eae64870af78f1561c1aac7b0b1280cb912075cc57b24ce697c1a87be28a278acb36924638aa985d1981b6e76c372b15a8df92c953f8c57c006d32ba82966c88516bb9cad8d743502f0dafdf697cb2589bb5de7095e73c0e2f4b4a7ed735fdfaa45f6d29ed0c65f6130f0ad9685f197f1fa76971202bfb2cd56249ef1ca214f2e5a0178f17c00763f94c8f4338ad62fb3ff4e48f8c340f87acf4e8cb01bd9e8e2a5914635e9f981f8e71f9cca2dab72f7ac36460e759f21a48fab02accc76218ff29dd9cfa9f0b9807e3c87e95d2e7a8e1d0c46762cdfe2a0706a9c8cc1c601317ac36f5c32e73196a6435479a66ff703644bce0c1752c2b4fc8cf94a12615e7e9abf35c211b9f9769cb1cd42417473cde02b2ccd984d16b3636e8d45e06665a74bba8dccd15a40f1ea29d909509f35fb5d8ca2494475738b09f16cb14bd5a91d6bc1022e0ab039da5ac89502cd08272b32e65df443fe8af2c95a7858283d79f5aa5a6b9d9f838bc31e4139fa7eca15ed4b4e9ef539e68d659fb3605f209f19ea65ef4f52ed4aee700b051bb65aa724785c74bf3435167d37377ec45aec67bf746638992b9c7a1cc7ec9e1cb6e3b60bc4621e67d0ff7266aabfe868b337cb21be90ae1e26ec0fe20e663538082e12fcb9f4fc927e193b599b8b882bacfcee91d87ecf1bc2e57ca609d7f374b2b76b2ee01c36166d16b0a5f7528ca55d464da9588b115e024d0f2cd1163b0bddf303af5587e450f7bdcbf49be0575f45755327863436f9166b9d90915094f59bcfc8860edffa40ccbef90cf3b40a8551d15fd780f152af00b3f36bf71704fb619cb1c64a0b6ca1b2eeccee0bfe9194eafd64a3b97daba6f7a09f07e66930aabd10e578b081db018c935641272982caadb04d3733a0b408d935178dc7592c44dfff22abcbea799de8d07d24961072eae8cce349311c0c8823f2dc0f38bc9f52b12cf1f26eb958ece90907059ed7afb263f76cce880bc635327e4fdae85da6886e34f10dcf91bb61a1b9c1731cdb80ccb58d6ff9935dc791df6893e7113a5f9daaf573c6fc3674d6ff8a882171ce64e43b87bb17e82151d850fdc75319d7c43cb13dc4b933b9b8db1e6eb8caa3eb6917669f2d22765aced9b34eabae6e19a3e4df39285251304f6e2f50f30e1ec5e5f6bff54db198cf08678f465002ff21eb7522dc65655e9ee46f5e83d9299b2b6d1e2ce30a948642daa5a2ec7a537df1903e40a84bbc45b45131371206657d02d543b546309adf9c6c85829df1f06ab01895d845f4f0471a6d9a23414c3f9e7f4d2f7c094b80b2fc941e73bef6186183b6c505556d2a50d8f8a33c4125c9e4e4af0348ad15b3c23f0b9a72f5211b943a948726456b29b347f5f002521147bd381ac9c6932b6ded4a229d459cc6f77c7da506ca92faeacb2fc9c15bdd439b332673d1598e9e5e57777f458226cd13a30cd751c63f361bbabc3561e12f590c75263726d2255e176f7b96eddabb5227d7e149c13a15128d89984596d9d7d49a380b2b62393671e8027bc459ef8c95edcae4bad7f04419c64a09bfbbd8299ae1bc38032b16d16188a2a9f63de7622ee0bc27fe4031b46e21ef6a736fc31be85ee9573a4f2f5f9d12ad95130f06ed4d464b2730c7a9371e1509096ac4fd86390c9eaba13bdfe00e9fd68b423c639cc6b34f81b385d1daac014c76448a0edb59e40c2ab450123d2679b81257bce79b9028c36ca9dbe19bd99b133a241f265e7394f2db7438c1db2b838779099335f9bb9d87aabeac9f1322a7061b4fd94ce58d17a7737104df50be92201f4b07a6943d0a76439e09e36ec51c913ec2d9f24540d6b65fc72a5e852214f4c9ed683d045878f4c8cb47504529c93a50c247cc0326b9247966b134efd03c1da02104a2989159c826a80d806a21b3739822bcf4746bcf8ad0078b02de53612c58e18efbadcc97b95b216c0db76f8735ef460c6947606621b73839f77ca6f093e897a3705632fa9cf95845e8da7d46189056928460b73de9f7e9f9a49e5f202d846ab127e313ba9b88a128d9baed82b0f9049fc28a98b1bdc7c4dd9946b6d9cb2603521978f5442e11f3aec5e5680e15ceb4bcde0ab0ba5aa7a92c07002534f156d193d97586eaf8a492d2273e430204b3807d694479bf3037f4d5eb280a6b0a33a130b454d882fb9bcabc8f7f28f924ad19a5595025c2d7b0dec0078315be6d3286d8e675aab9ccf94bd42d895feaf5f80fd9ebda8c25d1e9d0d0407d36414fa7e1657cb2dfd562f142211004ef0f43ef8aade7e22d6997c7fc8ea89f5612af9580f917ea43c337548f476b62570eebe88eb9c4d809228af6ac0c907741d5f96acd2acf810745e5dfa0882e3c1ecfbf0860e4eab5718c6c5b57451f114557e5d0fb2333898d3f01beedc43e5a985f28ecac93393dfb4810864223a3f0f4f64745b293cd4e91716301feff8766a54105dd0031ea730d6f9c1b7befef4277440960c9cdf8d2070e93eb58dda0edd0b7311b7ae61851c759e67ed3d82c9bf29d2e38dd3d8693f4f89f510d26f13efa6812ae96a73bae7c60751337c731316480fd52e2ebe45e0ccbebeee5685ba66d848b0b67282b36737defd6b63f28d2854028eca385e9cc222433d0032bda81d94bc10aa010d00f60070d6f0b23a7892d2e1e0d2556f054c1f33a531eac1e4cf2c86b763c9157e4ab5dc331d7ff2fe761b50d4ce73e0117e2b772d22b2181702e9550a3f61acca0f0c22e3d358d2f79ab8017aec49f7e4c5ac697b6d3815ace2622015a4ae16895c7e1fb68e36ef4ed2098f1119d8710c40cf349768940848bb76e27841e8cc65bd096c3dc7c8a039847ab603f4b01f037cdace5576d95bc95f4fdc6d5da744e3acb897831ab81a28e874c3883aecc7c900ad32a1de095a16be501f9fd97ae6e111024794cc9f84b47515f8fe4f466a3c95d78b3677793098d40898e2dbff4c99dab099238c4d0da68b8aaf13f0965799b92640f6d8454fe135169193b0f41bcf5b9abdd401fc5f19b12014005f35f11e05a097914fa13c14c2e27918ccec7d7b62a1df87b889a4e456388ce890a483119336da4156d404054bc218dbfb8d4932fc60839360d6ede8ae241f0d7c8c5eaa4aa1ec42df91470de97dbddaf5bbf9642adc2f889f09265ce801838eba05d0fd94bf83f45486e6a044ea3e2f5138fa3ead355a3b550ff1091779da4f191d08d24d28427ae67a7264b7bf66a543aacf0cab260625b01bc32fd2b9024d09b8477b49b9b3d562e2f2f0b58b4fcb0b5fb6644694ef0880b6935b919bc93aeb43004a14517e32851b79a851bb065954914940de5987be6b2b75f8351bb74a9f23855a59cf9aae7e56094ae3a6f09f368de47bb0cb21ec582e6786f36b25c30842d736a104ae429899c1008454d66ae12d7ca87a6e4c7c38d28bc083564f4fc2093a69d04cbe6090ba3a063b693934f99d653e01ab1f33e95920f92d3f52f45e390ffce2ef8f874f052fe9a966a652314c4412e35446efda826dfc17e1aba241addef93ff728a887451f93234c5742571d5b84d70ebf476e839e3519a9f495a5ad914def39d7f7e70d619121435dbbf6bab4e5e76013e6cae5d4537b8add19e5bb17be16c0c0cbfdb3b3a402493b2cf5858c0003a12365af50c0dcc42bdbd82b3b3016bb6f738d36cfc772bfb6ef0896bb584907c439e293a2bb345200d0324e6832bc01bf439f8c7281928a0f856ace104d502c3953e0ccba9769b4391097a4586cdfbc63021dd341bcbb360666e8c2792aa7a5e5ea7823238aa39d733257d8352b174c7c0d8dbe9d1bf11ba7a1a2c2dbbfd56cd5f4d1460bef9f942b41514f67a5d8aa4e2265858cdc392571a48be3358df17a98fc174fb11000ec5db3d024aaadb9af79f572cdaa201a9fa20e8700009fdc0069997a7b629460c9474373c4de242957c834efb80c9cdd691f99f6e6f8e00d298bc6df7b6d6740fdb78ccd687f0a5d88f574d7586276d6a9feb82e6b999818a6cb2fd6dc2a32d845cd16f74ad83e27734c7e64cbaa527d6fe44eefb0cc1b41c740ed364ba30498f4f5bc7ef66c245dd280ff7bd4dcb35059fef86a207cc8ca608d38702bf0de5c78d0611dc4d3af8aa6e6815c52b312da1c5c5967b93685fa8f47d347fc512acb52d0c2daa22596c2acf9df03fa16864d5543d9f3bae36b6e5e54a7301b89d1547e39cb6da0e6a18a348f227cb7c987ce817dd05b65573f3638112fa5dc59dd9ffb916bd0b0a52134b884469cba5541a9e14c6357263b4f9dbec2ce6bb92e63ceca7d6d390360367201a073923b6dd48a6e09ca0d2f87e3b1467eb13ca84080d54ef7be73ae4e6bd3a1859b59cdc48e0e75c6a11f9a731385fc85337a3c785eeac63536d8a8ca923a6bcee5c65e0bc59b163e18fe9fbf8b6adddb4a942735fee923a7cd358c2bb556053c54de100980065df18d3e7cd415a7f7db2961e30e6ab00894d2e998012dfb1b54d66c7c4476169736eaa2b353a1aeee5735bd12a8ecbf21b447c582b19d668dc5243fb80dbb0b11ce832e752b7f02d41e7d3fc83eee93a0ffb842bd5fa0aa50a00efa003d12e9a0243d24ae5fd4af1855254cf640641f758ff65facbb078068c77392563a8a606842b474dbf65fbaec047d44d047dd3812e59c430af9cc8fb475eacc1ba0aa39db21c705a10908ed6c2bd5cdfa8781f648f6b87050b2a68c4e4b0ad2094e03b65e4000d992f01d81932238bfac84ad31562be9f7ef56fb5b1cc1ddd6a537127dbbc6d837fc1a4fb279ac74a6c8aec7e923f4ea6593be0f1bc86b83422e081ba11cda7d4d15d2b38a66fe7a79f3d1d5a228d00739498fd8ab6b9c5ee833133cbf390d737a7a307329c3379658372b62fa38d5e58b9ddb645d80d1281f1cf6ecf6600941c9845913e86ac973d49ac614495ce863f95dcdb261f3bbd15b42f0b66fbcd8379f12ce4e467a48c77fb64ddc967e253a0121da3175b07faa3e65f9b77eba3a60780eed5ac006d08f03a915b41c8cf05e72c7abac310f0668ed471d53ac964a77305640815fb3d7d4308f5033ff7a9bb35b654ddffcd96b106452267ffbdc93a8339edddea6d7fa454c13da78e3455c57ca5fe535d7ece91b8998d3f4269eb87324025c4b4a19ff1c167fd236f33d8c05002c4318b6123b13162666155ab91b819f2040e56e0a14d5045e1c163af4db8cbd00a82324b3096be4eb57e541c2a4e81cd418e5c9c4fb6b5baf2ef22808e66e7e8e42164d94b18a1bea4097c7610f10b12e882954bfcd7bee68f366f4fa692913f682f7b24bff331dace3690c460b52c8e8a350d6ea6a3d709f67c54273b283313aeaf09d08050547886d002311310e418afcd1cda53960f881e47ca0f5a9c3528461e4c86e0cf80f7d027cdfa68457319e44e010b5b74e3dc0b8fbf8e8f100d05a6095d24d37b2f5822b3c3910f829575269b9bd475264ec3c27eb1a360fa7588a81c0e9c5bf64ac11aab4150cdc21108d4dc750e96adc3e5ff101979fe1c2cb0c07e4bc0d388d9b45ec5bed0105d6157d2f74c862cd6807cefd3e84168bb5b26866dba185bef5bb0f9bff48706686568569279e6cb0c6716430b6786039bbae89351eed8e479a1d4fc9b395fef620820ba8c0b49ee95bc28da066b9af9264e4f0a701c4e4e1a70f6a850b95641421fcb37cb0dc0a659fdfb3b9999ef1d52ca6af450b93fb18442f49f05f9c8f337c78d637037004062fdd6fcc0b64e44bd3183ce07cf7fed4e792c4e83c29c0dfd65d13842b01c3614b2bfd5701cba01cd2d0f838b23b79963b85eea2f3f28837f1b7532fe31849fa35ecb9766eb5d24e1409fa6181e0044bc40ee7e0da7f8c9c2f8688f8e0b43451fffa8c4bc71da3ad8902915ccdf1f3ea12f60f27ad41149041ec250a0a45c04f2b8c2628ebbdc90313ae83807ca1934b954c2d422141baeea67d0caf6132431d8fa795f8be752d06e0a361123cb82c37c6018354ac33fd411e3abe2a3dce222f4cd233a591a7c40eff95f9f2bdc189a9a93d1320e395eab51f6408ba3c151fd5ebf4521ef28aa10a5ac5b3ab386d1794a33b1d7535f8e1f4d4ce1873a89ac004a69770305e050fa8a698e35ed982d9b78923a44b66a6204e04114c0762a41cb1a7b1d7d1e2465d9c4efbabb33840458eda4e3bc552c5d360fc4728d0b8508f9ae1fc56c7f5c7bbfe049e5d9f87738ed102ecbb7b178101b31cc3f43dcfdb4e368672fc77ee61435dd25984a97bc645ce0c37abdc1f1158809463e66e717d39507ad2b3a2e7bc32e3ef9abca9190c4fdf14499534a6b8443614a097d8dcd4902e7caddf85792f02d3948dec88a3319082eb0ef1fd1cb6b69ae89828cf9d75e0a617d9e87c4d666e344d069fddce3e11de5b9d16be14af2eebc9c5e08cc396296a84b621ce115285a6c1535bfcbcf21e154f95c9f29fe860fac89031a90f98294430f1a088b342f45f08ac757fe949162fbd89b3cd5f5b3f11497a393de497c13bb67bc81c0b6d87dd3afca78d7ca87a6e4c7c38d28bc083564f4fc20969afd4bde07b510421e3819b2e9b7e8ed53fe7ffd5230b4f6808f469d77e888e89e6874e6f4e03ba0acdc986c5b8168c94906016b335302df6221abbbb1f269dc23811071332a692985f731e662b7c511815a3956a5fcbcd96cda0506ce004c2b64445d5346d60270fe8cba477c1816f6ffaf9e38840315c0d33d4538d60cfe576d0600ce0bd0180109e5baf5eac6cab5d2d986ab369a6ee73d4653ebbeadcfb444a1b8c9b46b1245fbef4c0f35dab431a1c55b1accf48eb869bffb5310f3eb675d4c053629888f2fa51ac95443cccf87f52a350db44999d63737759e693c9619ef28def6065e8ae34efadd1d55c9ec26a239b35ae03f4b6a9fbc9887580f963ac536af43f63568378f265caed31dcb83645316145cea75d9094a68ed2b5f47de85b61eb48a0738a2c771d94404f817ff276b9cc16f4eb9331b68de49a5546ab55c722c45af5c612c7896a0c0e36d560925345d7f69c817a5fc8eb07205d35c8c55691639616aa3516fca0b1ca66d746bfa5e85b3277916a7e3f7d87b9ff241f3e0a306ec59722ce5d2ab6cac1638d961e24b10ceea6640b3090c94150fcddb7e484f743a3cf43296f2e8f7c3f5e60c5b8792650491e241cc119a0c26b75fd0abac46a7f9a884b510d4ae8bacc88b4eaf0c6da3e732f3552ca9f278aa4c566c5b60fd463104f7769de01101ea84cb1e95977a704b15f5c7326853997ad2d9be6a3184be8745a80e55e8632dd0965d5d9283d26c6916c0dc035491b9974c33b3fb474bf70fb6656699c38c6c83881affab9e8986bc2c1d8221f513f8760b67f1dffc61f855915c9d44a2196d906998a7563d83fccc88eab47bf3e787f441311f1eefe453ebe31c00592ee3435949a8dbf636bf0e10855b05ff57ff52077402f67bfbf31126c8d8d4e7827f36d4a777fda3ce33b2f9618c428babfcc2fb18c141ab87b158a120477c4413e1277a3ba793c10fee58e009f7ec68d6a96be4ec41c689feebab4d9d3d4b6f62e6903b116288458ad2b5b73df6162c7fdba76d3fa9d1862038e490dc19e89bccad44df3cb8fba233eaae40541f0cece1e409b14a961788b6d916f7d7c641ea645118d10e146ad325007aa9b8f45fd7a604a36bad693321f6dc8bf444245d3de2d5f91c19e8da0c23e5c2f87bdd6de6df0f3396da9ce2083e2eac5ee203ffc0575d210360eb5b7916bec2f3814c4292315dab5a996a15af641c3ab7adafb5540b0c03bf2d0731173b5a80c06a81a096ba36f05cadae54cd9ebc8fa29187dd81951551b1c2452401e627b3e8c923350cbabdbecf73c9a18d9918c7ccb98fb6fbebed0d79528845274038912f441eefe3d5effcb0840384da5cc4cae4e1c97437ec8da70312c44d249654e451b285c41ab6c4d3e27f2fa1d07e1a20992abff7de6ed96fdf4920359d467e8306fc00162a3683373bea5300db548ac0e26b72dd4899d658bf9371b79043c18716fdf0653384da148b56078b3ebc54668e0761fe0efdb3169e7c645f3dc3731f52ac78fdc4daf3e40d41daf8453a91c879435f292a31a86fc3739f112e69f2d7f89d60a9ed44cc6f16a268d20c5919591f37397d6abeb6b37b0d8b6ae4ce9fb8a8bac6a85166c8db43f30073e447d579cd6f0fe667d9d9eda345d4bf5f73753f610e3274d443fc87e503c9195f17a52a912ffc13431a94ffd3bb598bc17987aa5b43c69d168c385a44e9688dcb5073a84f152446d2c31e90d73dc4e2d3830d1b1662804bc12690ade779aa2dc3187f0eca3931385f399bbc23a562be3c7d38ca6a7f970597ce040270e7b366ef7e6bec9142fcd16c210d23340b6548d77181b29ce0c7ca83c7792b3912588f0cafa21b5bb114720821c28898bb3cc0ce0f901ed6e3479aceaad51bf548a57c2e3947e911e9fbacf863256378acd3de8bbbd101c6de167ba4eb38da4993880c700f796a12dc528e24a7fde55f29a53f79f2bee9006a322e50f74c78ddccf169be29394b86ccf11769762df5eb48c82aec98d534c0c70d370246a22b29d1fa70466d8a10e45b15b566bcef24a19611537d59838f6ecedc71800bc6aca45d2c2d9ab1430d927b4a751d76161084b4bd12f0119e7d7fe0341b735ba5443307abd52196ddecbfc0a8a4ee149c0bb6b1493cf9a4818760a16ed0a85243dee3b41d85f1048ae9871fb6350d2542ab952850a05972f4556fb1cb0276be88063317e6c1b35f19353c4e02e1d3b6fa8b02a4c31b39e535bc8db1838d200f9c7cd0384a2b6f9ef8c991e8bf680a744642108245cc059140bd89527a766f8bd92299c7a3404550c3522bfb07744787ec9a7c765b061e7f62527051d9cf46bb09589e11a405366341822fc14f1b5ff3a09ff8bdb77a777f2a1631a4cc35e5fd7796edff3c1cedbfa9a82de8e63b1e0942c65ff929c174cc79ca7600daa04bbe0fb4edb8acf5bf247f8e9a5afcbee87351ae72ac24b6d33a6fba230d6f7996837715e9cf0c6c014a7f2ad7c2d710eeb32f83af031d018faa659d345d0e339712a8bdd4e2598006995b00fa85344411efdd09beafb3c71a98754fa5fe1596ca86a7a903151d05ab4b9a442ea7232f62dd32d69b82a163774073984aaa14d4c7ac9de06d05ff456aee6c13ec71b878308f87edb851f24f8e394ed5844c3d9d8739098ace7cc8b3ad96ae0364e7742d7fafabe808a60a2807018fadd10ba0f2ece25c088102c13cfc662f0b5bdc2b974afa6cefbd0ffc198b6014849649383086e6e84e0108bb8586b185788782876db69755640c800ce1a6fc1dad3caf7e64099a13b69b8c374fef1b59dc7ac483684be798b58940ac04c17f2b3a34f5bf0726c3d3538456a76f57c5b6eb639575e55b0723f6f457a003a994a6ef989faf07b0a0dc403e2a66bb344c6ab94e93e8dd8d20f512cac16274f62608f765c52a54913b1bbad6b3b61d501941ad5b12aaed8147f527dd45476806f0825dc2ae214791bb949beb74c0fad1aeb916df8c6b3c9232886e8482cb06086c0d4cb7be7943adfe769650caae6d269fcb0662025bcece517cccd19df844a25a840444b1c530850c510e03a844eccdbb367fda716d2e1ba9bde593eb35ad9075a4f8442127f2c30e2ab6a931dcc1377834852fb9f387e3344c4b0d8eb25f7f8ed325829ad5609afcda152ea3fe3457de1e1671bfeef849b90c0b8b79e102edbfa9a82de8e63b1e0942c65ff929c15ecc95ca172097c12067dd63806c6467639f169ff02a2fa43d8c8815508f762f92140153a4cfedc5fb9bbab56bf26a358f965cc97e9fca4518fff393ce0da9ec2aa3e849a9a6ff4459d0128828a229b4e1781112455a27999ef4e72af28b4568f87201e59725b277c65e2c35e38934efe6d92dca8c0e61b9af82bf12a9cb4f11f24934f317ca78f51e4435f2e891d92d8fdaef7afd187e1f8dcf1205cced348892bf9fcdea456e2ada54268e7e183ebdbfaef8debc11c21f1673b836e277ef9393a69d04cbe6090ba3a063b693934f993ecfd50f48dba09968f8730881d16c92989d168f24957cc9dca66b3f955d6aae26bb4626cb6d0fcfc6614ff244427ae1efab5788b6ca32e3556c0e49a2c74b882d276bb062e6df550eef4ffacd476beefa38a6e920cada67a9b2414f528751d24fc1b19426a16995de812a70070a33ba78091e8d68fca140dce5c67e9080e7dddd9e9107a74c970976c71b8f1a697f8f5a19577d725d9f39e5fc80650a68473209b11e523ff87da736173a57bbb49198a5344b2b8f7824d720233eaef28dd6fb2d292a371e9ce8b240111f5a502f08bec2e4f393740b35a6a55a01b8f6468695bd24efdefc1aad5902a6d237739fa422cbb9b6b1e0b626f2990bbd61db767b7c3dcfb2626c0a67ef9e005245d157e269205d66f905775e8be192257b4bb1a5b568bdaf550b2458e5e3feec0d1d4bac25f9a08f2e7bb7c89aeac12585b07526ded5c1f0e723f5ea0e3189bb43d1485f4fafa0288f4a0459e07bc0d9c60897dc8a4105b45112ee70a45e6d11105f67d4c37ab08a3f1d1b0da99e481e5676c0e3ad33e68f977d270993af2889a1576f8b67c0bf09d7a01586cb07d6e1676be0dc596b1e373d62829952146b06d9fe9abfcbbfcc9e24171e6b95153025678e57d73497e041ec25ce19066418d9e7f3889a5a5892271dfde9f49e71c0113702b59090cbd249ea156286cb86c3f725767be9656b88f42fbf8d356a0fbf6050907e891c397ebfb1fe066541832d4d9fbff0957c4a092e7d44569811597f2c181dab4f94059b4a38fc600c6e51262287304d3e19ef180d4a5662821c394e7043fbd11bb805c05fc2275643256e59c74a601114989d89294d6782db23a1239f6431c2b37602c2ab8210c847ef8e3bf2d5f4e4aaff4b3c67986dce320e8e1e093f636e205bc43e8eb1ec1eb55a60ea29f0df01fe006aebc6c8e701c46cdb330802ec5780e3ac22e12d437c6ba829c46810921faefd85a972c8b557621660107c5b5fdb0c22139e89b0185a77a5f8d3ee562da441b8bbb1ebc0a04fb40486b4b5457296206073e61ea4f0c58acd5c27fd131b963f74165f194ebdbd31c333bb9b7e4185a2a87dd714aa3a215cfd2b0870bb87fd989b7ecfbcaf480c89a51a0623663dac1922330453c2d52c9e16e42cdbbc4dffa2ec431232559932a86b4c01c27d8994eb984d3713363455bd9ffa65eb07c53eda377b94a12c79e28c8c584967e270bd41be9258022ca2ba11868fa626c05c33b5df27b1320e813dfa06a75d11eb914a374ac11b93dc707ad67a717747bcf78eec4be1cafb22b6f2f665148efce22168799da561949010ec582e8b2d4736f69b9d26b0f3f1ed2e2a2508a03392802549f5c9adb7a468fa886b90c929708e37d30aa3d8a03ae98a3d654e332f8b1b3e939f037c6c018a64834a20f880e31024de80f7abb3a7b9273d3b0296ef36a27592ebda511a594c23274525ff604a96c60e4a71b9624bd4f8f615ed156f7ad27cc0b8dc339d4759c1cad9239a748400dc83a5713f3766d79f7d3d0dca902f47842bab07d155b3c57aa2bcdc3073fed7077428d0814d7944c41d5ccb94e03856cd003526094bdd22a2bd48c93e3b0fe2a1936aba5afc35e35abfcd32eaed1f056acbff9e72e2c65b2915a6a6d4fa566e6be382b1013e5a7f8a01e09ff365f4d2353f3eb14bb4cff0e7b87435ea1bd38b4d42fd4e398ff39097c4dc3e17e26b3b0fee8d33d1c9f5891484f8e32db763bd75977ab3c5104179a76f67cb80a01c022eb188d1c685c3a59ced1422d4481e2ef20d2a948857961e6a3d91b634eb84430f0ac9ef8716ab98a14c48e4acbe8661230b19856e86d1476f53ac7c5f0bb4c373b1039548dd02c89e522964a7843243d29d052309cf290ac7ad13b1d50c7b215275db64eaf9fa6f4ab6d1eef172b05672322ee1c3704f1c2512913af3d769a4019fe020b8acc4edc5fcc9b3f58e05ac9e8bf09a36e983f9188edffbe2f1d7b120782e08db07db9bab4b5d0cab406ccc59ba244118fb6ead29389ae26728f623ce8371cc569aaadbf2583ae46eb2cde723ea742b8ff28dbec63a7882e292cc71c51e2e9c1978b199c175984c71f7652192fe0adec1aeca8e811bbd7d7f33ef51995e90b3305356ef5529fc57033ed9c3bbdec087a5f9ba7b994d34769fb9de415fd89b25d48f87461a23b5a0be5d731459b3e440016060d3629d9c4de2a1e7e4178d4cbafe36afbdd593170f5e478c41291f2166a35a6e410d1ac8eea57b552ae49ab094573d6fc214040dc2789eb49f3bd508ae3e686ed440eca3d5e5d80831b3c40286424a037725ad2981ec3998cfbc9741a802678f5893f1cf3bd34f8420a62ba74646e61fca2e3a0f8611dbe2a2ae50fd5cf9b25780912603f81530132a7d2bde1b6f7dadafb97b836122d8389e0760df4a4418f56e088303a71ffc9fbf51cc6d6c8346cdeedd49859ffa6c60b80eb2c88f952ebfa71b08dfed4c759b637f2f488b0ecd12c020ff6c66e0725fa05aa477ba34ab4e9cdeeaa68991c83b3106088f52cdbd6e8198c641531491dd741302d17e024b69f61c06df26492bcc7e8f665f1b84a5733aca37c2af23cc659e991dd7e09c516092d1eca91c2d61879845cf10837fda763d9255f99109640cf6a4dd5127d80d9e4394291e578de79e5e9c95f34bf8c6a5e2c1417ff715ef3ec697d1b7ee1d172f8f1f9264e11d33ebe83c3921721346e89a0dc100c9622794f04b8737b492c93f18a43490bd3c3d3689dcf954d5a3feb74aef36e2c3c1eea27e69a7cc88e58ee9637a01eb80960af60f8203bdd43d1858e76f4eccd4eb74d77a039f2bdd84761487465ec6c9d8d733dbe44b05b51e0ddace44f7f5989918802f01780a77e593fdb1aa5c9282f8744eaa1325ab913706453df314f006470682d190b3c3ae2431c74f27f57b140ce55ccf4afb090fd30a8389943138c24f4abd8a66d58b5bba87b15d1caa5016343232a2cd741f205dde91b7508f794e07023d022a497edbd5a88a25e72599b4c1a09c032a3b4ef0e098153d372be9fc7a48ef33166a1acc35d92b596ff7571418a72b1ce2c57b662f6436115ca0c627c36144d1f1e3fd9c7cff25b57f56532a8f79b4a648c483a151f1ba1c5d69e5d0a1e58e44eeaffa196c3db7839fa74f01d5da98be9d0b5e7f9c46c2b35dd98a366aa8345ba069fdda9637a05a21fc06b629e93e5c5aa12e1c55541e470389c554c50a5577f270e667f32963672fe6ad56677e33fa2868471a81b9538759cb33bfff6f8ee55b27e95160faef2e91518f8b0b1abacfc7391a83cd2b8df7cfb1102f1f338e621c70d3a4d18422b575ec29bbc9699de1d32821ed5432e30852122e618722564bf3ace752c9852987ea2a655dc0da3b2575dd3e6fa5adde7a0fa4c48194afbbc0f0e6d67d3ed9c5d63ee35e1d1fa616aa678510ef14243c32bced9f7a0cc5186b4310dae5bff49c265c19b395b076fad87c14020ca8c8e055eaba9e39c42969df60d55b17355782d8004c3ddc1fb055f6480e3652d6b517d27f42c4ee761bbe70e7ccd2dada460d8ed8c835a02a235df1f439b5800feb3183cadfcd007f5300f1dcafe479875d164971dbcd037f69071214c54a90c13e19d1da014e34f850c726faeb92b7d217ea6787d1389c67fb3f39e55a7386dbbe9075d784b203e73d1fa8524850a346de81609a781ca0be09a76d67d6b17d02b7c0b71f67453585a63ec568b8de193036e75f85827bf3d7cd1f3b77019816a988aa6024f51683934527be0c63a5c87f602c7488977e6188225a41ba0e25229e08fcd80bddc61959ff84cd3963d4e3288fc4b880df0d1b73c7b9d596b93310fc23db8b32f1ea96c6277551d28461c5cccae8c72638dd835e45cd5c4d5d0f1c7e8c1998999a99c6e25c62a4135b9b06601d88eaf5e2f81b46d43b85d106ec032165d3f033f15233dcd1a3f95ea2147246064b5aea4ba0c65dc62bcc075261db2a850314144094824b976510c3cb5963b1c02ae243bcc1d1a6526fd6da4ac3484067917c1164581aac5146641d7ded61704a1d5f37c8d6bccaa2abcac6a5ecf5b0b9d116a56ffacc46254f9a6ffdcfb693d40ba4ee79577fc754f6b3dd4afe4a89ce83b638eb7ff31df7db0a81ffc50ef7c7b8173c58434b003f6776ea87c547c18bb529ded4debe0674a57181918bab19fdc865f6dbf38a38fb8162051dbc4fbce7a7588a4d3349f60b43e98edf037bc0da79c469cbb0f46b8c085107b74b6be19e0420360f5cb3e0455d47001f21551186c6f553f36fd13ced342f15d1695d7e11340360ec4e107f1b1ad4f385c134d4922eeeb72e0e389d6da50ff38e50734faeba73ba3f3bee2d75b41386ab65a56471ae9a2f06c8c3b3bddec05154e0dc50fb0127390e3993a65d5df31974bbacd9ad4fd133e7fab9be2e3d03ea4f67b5f43a5dc9eea16dc0f4c904436dbdc67cc3af09113b478a63f9bcd909edbc5c4f3ab52b3ee8df8953ba16dcd880a6c1361fe7bd6fe0f8e0661d8c2ff8f03f726c49e3fb27dd845f5d96a6fd719b0e3fe1b2e9f250d34c0c1cdb311ed5c58a18b1401c1bda669a732af0cb3b5d8a7a98269a6464189253599b70c1db08b09bd4cb596141da16557592a744373d2f7326a40c2610ec88b5b1254523adfde8719a52f7467d2ab7a3108ab6d6f25a52fbbb015e6b7cb4ba8d4d906990ab7a71e8f65bfeb7e3b833a5a5672381ac836a20ff2d8970fcc0ab0e525b119e7cdb0328681ada6fadae6ce6828ee32b5c77c35f9768f312106d916a356466fa15c6a109973138f862a5b2615fd0bb7c32c40851785d544644d175fc1cecff0012384d5225cefb96fcfcec6eb9b02b63c2acf3915a9c210f958b57756b88a80839711bd6f37837747b191efb34d48297cfc2df456cfc6166e58ebbb3c6d8ffef181b0ad1bccc5517f32df7ab6ea9de2ee9eb4605c48abfd6214adf4f33ab2c21ba746b00c90428b43e9a98c62d53cd44d4d66121ca816f37286e44588241fa769253153760cf6e2578a9e4d76b93018c2d65965205c1f16d070eb9570c38226ec07210016eaaa4b2e7e7e65928c439bdb0e7d932a04e61be4f1bd6033c3550ad68445a8435565bda8cd2159d924b3a8df228e712aea318cc121280a23e5e94ad1b2aa906dc255e2ac28a45465e99ccd54d5885b612ee9b83f249ff1ea882d52101fca3855411a2a72e3adadfaa49d31aa71096a7339bc6f464062aab882355312efdf8d9dbe224c8f110f3ca9ca4f4ed6b2155730dd5591cd34446f0730f6698b695dff027aa2b5d23cd817f377cb6f2f33c4b26c2c736e4e3dc3b63bea5e441b9e25d127fb7ee05102d7286fd332b5435546f2042e0fc16472eafa7e28d348cbc9f7d76cd88bb57b146e367f94a1ce04b74545dd8a9580ad16d9548fc1f548cda586bfd25c3f8dd7dd977bbf2a9d113039b4ea2cdd7a751ca3e553bf16d2f87a225c3ca2c8c6c9ecfb5236aaf27d10b17070f1c2524c59734530f2c8c7fc8acde7e652134ccc25fb079f61122a7b371faec38acd838a651e36e22ca63a38a0a47b482f6817175cf5f0245e41d5238e136c69a2f0742802b74e077be99f502695d1dd10747463d18c4a4c3ebc1fad0d5e8c094c117793b023eb25df16ac38005399a23b2cc11141a2ef5d6909e5fe0c94774e209091318ddbd1a7676bc52a6a84de8f060e279fb79ecd71aa949f18400bafe3a365564145b6fd2dedf090f6241d97cfffe05405260692abe299bd888426863fe1779b7f658d0c43822beee1d4464545e0cb513d29441b81d2508804d18ad86f4c10b9e0d462338fa6972576db5ec50bfa5154c3fb46d07fb1d3034f8e1b5858ee936362e84a2ee1d647e7a3317460ab1b98038948e05a3318bc15f5ca093a9f90c23dd0f096203f0ac69af47576ab85b41690715ac9a25d235c942423d0b992ea46a9119a84a86c21f0e57003d46c55d2798818954c70d0adb7fa6b7d880ac89ab075b69e343d40154a8eb35bd4240f672069964a101254f497d90b6ad22a3e030292a1766ab743b7ff3f4d91ab28c97c46d2623b5b9d7187a3c59ff61012afa58d53be9d2e82c270805b2a9ca561c222cbe9e0df347c6ba98349f0ca0184d69019228ad61da2dc621a32ca9f7f73dbed2c0b29101fba4a7ea98862cc4d44fe6ad44ff6163470160256a06a6f42d9b9e612d1711c51ea8a549edba8ced611c5872dc56a95f903118626e2025938c9cce7be807f5e4454668d46fea38c4b4ea386017a1dbe09bd0f22b3237bf672a7ebf39597614bc85b53e5c739cde51c07c0ee305547f5612dbdb5bd5e4e0651de68175729201224dad6b3b20067479a4c89bc3318c80ee76a0dbfd725f1731ca0eb45d79f024111d8e940bb9aa78cec86e5ab3e6038a68ccc09adbe22ae508b376f76319809144f460e209e49af53f96faa2aaaf80d125f12ef6e677829f0dd5404aa70d5681f21e59f9b55261682801457a27c23d21df9721cb6f6ffb59049cda78fd0e9d7fe8a07d11ca27d8c45ded092cb93dde0b3723317fe631b21eeec4ec920335df6cc7891ea9a6ca3daacd1ca3b5261bee12da3a61d7555a9c0fc1f6fb84bfbae746b107876e9a4f5c38ad5c7554bad92fae824345141d1ccd35f83b28bf8d5e0dc25b0efa1a89a4fb024e197d7b4b90ad44b10c4deee11418cc99f6a72671179f71f64c4440b8bb64d8b5954fde36c4558f937ca7f22f5154f28644ff209601d505bade825e89026ba104c2ffa2194e20b0510796a4a02c3fa0da40d36ebfe4551f0c1c25553973a5edda25f84c4cdc2b0cf28de37777e3f6b9dea6a82e70bc14440252468e046e44826205ca1033783117dab11c8600ecbf6462a0bc63a2b6f8daad81d88dfc6cc4df383811060c225564406cc9f13f8e1ffea28a7216e19d73a3828111752bbc2f88f365320bb9dacd37068c8e7f24052525ccd79a1bd86f851c92c9656c6542d308471fccf41df634e7d7673e0e1e2623c43d07f46485bdc732d9edec68a8291b02e358866a36785290804bf658954c458408d2de74dd9b94a60631b6efd0cad306a745a5ac8d07c2d0678b845be61bb75d40cf25596df952557373369992dc8eef667a72b49e7fc3db72f5d1b0ff9d22612c3995ae634298b85d9841716a4438cf378f22165ba6fde7b99ad8f73052f365dcf4772b1d102cb2aaff983076a54837801d6bd5d59d2ff2c17b68b31e693458d3083821853ad45e33aeb86d09ef54d179cf6554aebf2d6fadb84f623e5f75c690a1959299b90688cd6a1c47a1064cc55ccd5bfab40c396aa74e78058b8f4e9120afd48f020d82ac67f1938cd2dbe81548b5c097cb3ef0d547389c18103e2a7307d766b5172217c9137e16c3f8b46ee3e633b25ca2b152082bd1eaff3696deb67d776bfb9077f445240bc8f724bd3b5d98f183224517d3fb4dfd8c936da400e0754792ac4db626de50f2ebe5e6b53d721645a1e2aa39896fac60b7e7def8772f270fae537ce712675a062128192a4220615124bf945ea7ae317641940bf30af3a41a3c2f8ee1c3b47a7c2e3d2346bd8546f099bb49c20138398fb2fb3f2b300a8845a25fbfcb9f1a11fdc75fce6a73ae17d518ca58736c952f3a70fa63bd9f654788037e736eaa801ebb6f61a08a74ebf107a10ec17ec4bc9dfcfb020b30cc88ac3cb81bbc8944efcda568c380fd33bda68aa5918d17274fbfc2df3721ed12f0645f6d37a6299b1c3cb02ab39fd4a302af31abff94bf3295ad31c130e3857b3bf6187b0cacc02a3071175f6d6c2ad345301b6263e2cfe29495d594870f684314c5935b042858093d6540328843789432375b290b3deb141360112b996e93a3024c400f36b42780d2651b1f1546489be4aab3eeaa44268a67aac7655a89490cc6e73b60dbd851833cd053f01af2904c93d82b8bdfabe0ebf5baaa83907cbc0f16605f188b84c7a05a168209885a64c681d356ad223039b76f44da141e205bb489696238567bc76f634ba9b47d29fe9d2e9587c1b383637571634212900871ca39462e31e08f47a3dbda1c08cd35fad4c9f18b5d6bbd3a4ab9ba850edbe516336c9ecff7a8340c00dd28887b8815cfce593782c8aeb2fdf766c83ac96d25cb7b8748bdd099fc99b9474f7ff059dffd8d5855948927e0bcd16243dca5d06621173b0df5e07028bfbd59189f5227dac9861cdf4a99386b26ed3a2db331933c1bf0acd081c8a1566f37de44e9c607ec20d73ed3b4dd12d2a19f6eef057b7a88dcfa105a77b0987f1c8cd8281418b9264bb815fb315d770ff68a9e7a715c4fc203509974b9e783fd509f7f0de45022499fcc2c87fffc30a197fef07ebaff1929f323cbde78d5c034a07655f5c9c7d63d3ac144f966c5124c99d75d41d72286b3c334f6d4f42e3ea338c1cad7ba0d41156ba1159560cd0d991e8d2e5ce10792de05bdd1e5ffa45aadd57a74b026aa0daa152af571bfdd7b0352ecd70ec591100aa2d5ea59ee4f4d579b4e72406ed278a4481db22d36304b79c2a4da94b499a644d691227acaca218e93765b5ad844d7cb6b30623f591ad16fe2ca4dde152a2117b59aa4ef7ba9b673e23434c0e97f8a1286e275254ab5305ef89497b0261f71afabf4e0d26f9d6863f762d10b8f0d96cc05af4f974e66085fe1fc41eac158ac0342eab7bfdbc663b47a7268d05ba84d7c2dd8142707c1c7d7db8d9b953e30154987ae620c6ef1b3d4a868570aa6560a63aafb877809b002e739aa0885c87def07ec3bdc6ac9f006e43aae1e2a271203aaf3983aebe03fabf72cbae4d4c9c9cb68c8e25d660af22f998b24f88393ec58d9294893b4f4f389aa7103606bc49716b5f5b710f544d4bc7b3c30284861ed7c7b7dd7f08345ed785a0e8e3432532beaf021ef43a0897595b0df40886e65d3792fd7fb4fd2cd8e63612092e1857c0b80785dfe47828df1aaf59b8ba077448e75f19deecf84c71ffbc69e9840e59945d23f2ceeb3d724c779d803bb0c32c5061174e586ad36f597dfb0cae431e0e5bd52b2d16ae1f944845580f40851ef94401a9243b3b70fedac9a46f8227ce9c4fa7d7a620ab0e61878a7ecd19685ac32783f9078d092eaa5ad46e6b0ad8cb60a5a02719f76f1a0f92b51ce4836772b89f15272bb06ecc68e6a0b5cc2f239f408ffe45776762ce7b1d602a5eb317305a5ce5352fa8405019e88c1d5d6f6bf1f3cb371f49405b277ed29c912adbb4f304c761baa4961f2b656bf02ce8729de4edc15cbd63ad89ecd38f9379d974d42000925d09139dd301c8822648d14781e611295f33f216eae80e1b1182185c6, 0, '');
INSERT INTO `_content_pages` (`id`, `name`, `title`, `meta`, `keywords`, `template`, `category`, `index`, `order`, `alias`, `content`, `privilege`, `status`) VALUES
(4, 'privacy', 'Privacy Policy', '', '', 'default', '', 0, 0, '', 0xcaa2e07a07bbc8c4998a3d8a9fcb86651bb65c1bf253a9c0a240903cbc426a98454919752b61b17331a66f69338673ca8a4a549519c6732e884f5a9f1d3f140f537bda730b40d6db93185f563aa39c43662b90eb970ad7ac0e239da383c41b4bed3dc069f877b64f57cfc00bcb9508c0a4391d5b9c3b0f6cb95d6033f61fff9c74cdfb4ec1f49d0b9899a18a2547d3ef7b106eff8bc47f5b9c6f16719f633bd408e5cbdbfcc85e155cf952df81a3230e670925fcac2e84b18913c4eec9b9c7a129f6ffafa46c6b4bf17b445d73179fb5b9cf94149038b3149702f675f89d8be5d4054a682d98597a9c15e149314d70dad5e8c14a87315106060f9c2634c515c186bce399021561f91ccd951f21c05b3cb31e908317d25d25fc73eee958232a442d7f5ae84869cfaaff3deae5089494398985f34f457c45f4cb7acf0c81b41daa978d12536ccce1c50fae34b30b39f202c541e3f024f845e18be9501e04040e3fd4200f447d2f65b53e98c238fb4398d5efdd58be2d6f80722dae63ac6f90db2fac3838de4e61bdd61f07ec962e29ab5d34d5efddafd52bbf9bd2f1a6f5cc89552e654b6eb2f3887bda4782c4212261b9ef2a2132b5bc8bb73559c9737da4c39d58457a28a637c07f6235bf2c1f427430c9b7a193699c334405007329e19691f9f1510dc9662383ecdc15232e5168e0031332de40de29099a143830dde153b959377e519bc9015389672cef984c53f3c9befef4e424972d65e63226621cc52bfa4be772c8bbb68d91a2f8080c815dba2af2d779b8a675821e07dcb8c7e48fb1172beea312c4dce7c70c5e8b95523f9ea124f1e2fb614d06cd102919264a354e9eb49c7feee262691dc25ae2e02455fb2addf19b35d5dc4e68c7a06471faa46cbf9bbc1d122d7f4089bb6adaf9607bfb73565dc6223985ba758716e49d85f2980674ade60ba41ffc337615e3a55dfa019e695afe0ca0e7b6f35932b2a530187352b0dc5d5f9d4af6f260c1507e1b8388b91766d81f4cf85e4ab40b416ebab39f307aa4fa385cbe6b0850e9fec079d36751a36a70adfaba4213e1a67fbff9fe200e3103320f02cdaba8e9d456c14f04292f54320668d948dd1fde5d4a6da3a81b93c8e967795f997c742a65e6106202d30210d18a95e41557308292d410840dbc8395a770da7833f5a96e72116dbf9ca9786e246115436102360d7bafd7497e181d0287db5a6f060425c22091d17f15674096d1d0e1ea5e715aa2686d5489ab55d8bfdc1c8d01844ce1d5112a0db6e1d68b082cb73aac3762dad62f590420c9208930734edfbb4ccfb32a6694a0513d8c64a1db46315df6895f610173354dc4214a05774e2658d62e3d72bec7379935279e8c552f1744b0b6282b23e91d3eeb0bf288e139dc28e5bc22b06b71ccc5dd27116a4bb23152e1c7671ba9570c7964c098cf594c26750cf6242c5aeff2a120393c2e499a27ed120f0ee7a875f972ef182bc21729963312119669dada07722e01a9eeae31dbfb3614aecf44f8c526a24eef9891748456d80bfe82ca3441a2cd00ae7cb7732bf3dc520984e3b9f02178cc95d9943bb9806038df6c27aa4af28afef62e19851dcd871ae110d5fa18a6762525a882d7af0b5521748fb187dec74fecc85887144a8aead705cff7899e3af01d299a4d1de8863ab5f66e331aff5ab3aa345b8896d2e7a5a7a2ee70ec8060b33c9d7aba35d746cd37cfe78b14510ee4154998f40bc52b91bf5aa9f58cb867decb1a62451d3ae301071e3f9400f8eac30700bfa8384b91f98ad1780199ecd1587fe79cfc5a0f8a80b38b5fb4dbf38ba5aa8475497448bba2d1bb2510a13c963814d8dd80349a5696444a51085f9705a70593e37c38d8d5f783bad2be8db800c3d825a3e4ff76f6fcc1fff3c44fad2e29425e6a6babb894021da0f0968904071947fc76d853ca607c20510c9faeb238cc4fb14158f578d8c229425c2e8cf9ee704f18f3c5a3ef5b390387da10679861c848e4aba7ee24807f92a213836dd738e1bb29161f9cf7205787ddf301b4bb6dd480efa6b77b958683905e501aa6e6160712b4da4d05f2347b53f5ac75d01b726fffa0e361b6158129fe7b1ba9764998339eba8f88a6da9a56e44162c7362ae47b622b5365f000b660acad643dde1ec29d26f427a7326cc45bbb40ef83dda7b732f96dd09d82edce26bd62d2a48b7b3a60e85c09d1a41c9b963d3d37bab38c845493ce3d3b4dc30d0ae95eb22ece0cc2964e517b6b98a57cf63718a4509c5cee348fda8571abefa02e3b226bde9e01c89dbcd3f68fde6a4f29539b6826afe110b4b7fbdfd9b784cdf0eeb396ed3cf586d11736f7ac39a3cad4fbb1583e673bc071ac0cc8d33b57e37d9de39a40e47fb52d3a966a3405b797821f66aa696d03e549ae14b278ee3de39807ae2d75a52a5976ec0308f48802b144f78488e2d69c5cdd46593ccd5d33b4bbf7e69b1192491a0397293fbb5c12db3b21abfa1907dc466039ff3382919be4e91715b5f64cd358d98b13f1d3da6c8a4689d12eac222302720c2a8c6064b0503f30f1b4be297d37051abb06825d8fac24883c5cc409a93d79055dd8ef86cb5d83937e1abd3fe6a2babd5771c626bd57796dcaf4a66267d34657811020ae8abd25eab7b88b3533baa019de71a0ef252ba2e4c929c2e6808bd6edde7b992ebc0e6b1dc08cf97912ecd86f90dc93acfdd579d3ce354eee427510e8993960bf6ffa37aec0415870f5cbef2844e510ecf4d8a9211b65b3e2cebde3390ecf0e7844a4d9857678bcbbe025346fea5b57e995eaf519095db1999a544015b8ba6a42e2c5087c4736970abce213cf002e8857a029d34faa214903e2825e5984ee2b270025fa9bf62c86d3bfef502281483ff7ecc0cf9101563f0e4a03ca554f2f70035f5440c7ad84d4746adcb5d1a61c940f35a2ee1ee1c3a67a07dcfa7793d1ebc84e355368a9a1b18aedb91d8b38338ef561159a7bb14913786caf3dad60d05673b9202f5256d512b748bb170e2007e6331d97da8170dac8520ad0dab7892bb8bec9f14ff1a463f7c98143ea864368df9d1ae3b1c1cdb264e5abd8b071cd8f6a4d748121f7c6e402b93a5334f68cf8ad604f944805ca540f566b3e73e41ab5d1c66d69cb7e566ba0a8a4d567fc0dd0e7cab671f9a7d0db98de2a57e0db9956119c9ab14519f87b234caf2315e5ec392ca8e5c6c2613200d65b3bffb25b5164ca5c334f87e987f9cb7c04760607508c6ca78f38f73bb86c5effe652b421e9e3aa21c2bfc0c06edb44b127c876ff8ade26b5a77644facba062969839c0211fc02a041001d3944c1e520173f4f0dd7ea84b62820fb2e60ca76dbc08ba02882c78660d3c26166b421bff965db936337401743e567c9c33ad686820c4a0a25eef15a75326dc86a6876c4dd5fd6a8cd58801c51cfee5dab64be03293020f93d6ca1add8d3ce85f5ec7d7a2dfdce93236cd31aadfd4e2e834d93838bab381a9f7b20f2b51fcd4423190791758563b8233e0b5c61579d053ad0ed9aeb1e637b5dc038af3c30063eacffca0976d4013f92173f10239f824ee707587aaa03070739bc3fe2914d4a611e017ca94776a9e8df981f52a22b1f62c73ae5c107f74f7ce2c0e945e299cd937cb7c8d8d96b8b3d5de8aad07a384f39f59760bde7d32854dc78ce5b98ac80540f50445e0bc0f6a05ea4f02d569b9c3002073bb00163140a1c73d34715c5324b5a8d5d44565bf354e621eb0ca9cb0a59853529cede8af56d4b610c3ebcced3cdae87a724f94195a4c68f74d5dba62e4ddf9b680a7bc62a6715073233748795e918c14d4f2da396cbf37ab6ed4621fc122fe38b0c0593e3f7aa624918956badb560003a9f365c2fd7f4827eafc54d7645c548e2c1ae8fd979fe87649736285d858d68978b5506f21185e55524938cc505777ac46e3302ac4724c5fa086eae3e287db457f38576339e3eb982d4539ab29f677286a70cb187eeec254a1db0f2d79d9f3f431e5bd17338b4a30c4207c46fd5e36c9e54101c6d4fc53895c70237e34ecf0f5fe9ac5d7c6efe4a3d5af174890ebdbe083c294502bb6aa96c24f1712fbb65a72553e8d9076898f56ee2fa5bcd35209dd63e0cb19f5b5f85747c8a4a90883378ce12821626166caec110543f2fb36825532811704f449ce27101e593df39bb26cf4bb5d21f1e71bc797c738f960c5abfd3d6a1f3113097060d81a1c04cdc5edcdd3a90a0edd59454d6886aaab9eb8153f26ce4bb413dff46f89a8d66103eeaf631b0dddea1e1f38d67b22726dca33fee835d34c7d78566283849f10e3bad3a2fe80e5d7908274d15998b22a5c0346fce851d977d90c302e22f93af2699e0516a888a2642a8961b3009735eb694f605bf9026669b0f50fbbd3bd9c1b92bd9dc5ef85b72b1847e59b517e7e652944f37b91af85ccf8b16e6eb6ad26287c57d5a9dd20e9d1b49855497a9061cb6d674206b766e98210a612afdce730fd803dd2c3344a21e0677f033e040fa3938c8fad160f0ca82f140a99f1a8e01771694ed7842580fe41065d021785c423f73d6caeab4387571e517a94c99ea4c92d1cbe6ff2f0d83b19c61b1b07a77fcd4430df32a40903e3f0055340bdc6be0a27f2dd078f270f8448ab8b405fcfcb358a31df8bd54202f4a0ca18450bc6b0d3ae09dcc4b419484d6c7ba747233d6d554e6e082d49d9cbf09b1cc961cb61d8c15f76548f4961abc7e55571e09d23cb6d6bf9c93c9b291a0c2b06efaa524961f59a57cd93efe6e8cb852742918d934451dc77b47b799be67a73794cf5211b5fa05a62ed3243f29b47be2db3ddca2dd9ffbae2025224e35018d3e1eeaba95f6466b2bfa229717206e4668abe17a93faa8d67fa1cd2b0309ce67650de4ee3b082f0ef897b103dbf277429cf3d8eaea56516c17bdff6ef897a4c2ff1594aa149253fff9b4f06cdcbf2dc7173e5e8f8d69af11724815b16b8a76e543b620ef8f56864e28ea5028ead819839840c26d6a7a6a576b3b680d806bdf32a6f2e5d34409134a77dc8b7bae800a43b2ef8b6575d9c5ca7f1b3c164b5963a40b41dec7936cd6cb03717523cad2d257a0610d394900be231f2d174c0304a488e710b6d65cec5b85e12e1212c80e742ff68869d9e19d6bb00d8b8551b4fe3dc1574792a3649d9e7027f7c41a8949dc5cb3e0a3080bfe984cd74b60bbbe499cace184175d670de817f65fc6df66a9aa5cd12ba05f40d8c8b648532c4028342eb12b4b87c81f73483919a69cf2a060ed3f5003bc17feed5524270c79bb07a3b4227f1818f275b79fbf0f1882e08773896b315fbaf960e9f1c22050722bd998595ba6ad003af3130ca2a29bf2a8c9eb1ba7a494b62a2ebd8949c1b82b4962051f725b6cdbf6e311ec014743f4bf1c0c1fa13509cc1e97af4e12c8c21f0c39b028d631b9ed428be0288b3252e286081f94b3c990207169444a0e8522947c1bfa5eb17204c463c438f44a07616b82aa5037d80b0b89862e2386360260215f651dcf9dd01513baa866ad7a62fcb69fa8541ee13ded668411b6718aa24064934fe47e1cac7e3505ab88392d933e62667ec99cd124a32221e8a9114297f33d4fe498dc261417473ed8a79efb640dc7ced471e95311e252d6e8e57d98f618905ce70d6a96a28cd45f186670a4d18ca6af6060ca8cdfe5e9735a55d71fdf27b004c62f1f700585a829fdf4c7b8a7a6b73bcfa0283eb4707fb7ab83d846a37c053c8a03c12592c054ce58d8b519282bb326bd94e76f70dd05277d027a797b50a3b977ffc7bbef63453a17af9a2b25d1dd6edbab4c792670b58bd3c327ab3a16842fd7a334795ccb9a2d4d879fc0e6d8518341c9842c36f9ab8ce97a9dbad6011d18aa0130106d9fe02e881cfa3a259b6f96adb78cd1e3459e405df47d2eac71c71004c3e989b463eeac9f9caa84a30b6e6d4129c0c0b46006eae499bf10ca62ccb9a5d6b8f15344500074034f6d1700dbbdd788c47ea9c885c47efe4c096da22cc7c552304a72abaf259b090fb4a162c367c91fb30e57dabf1c9f3ff260c356bcfd37576f6273f8a18e86bb5ab02ae46e520ab2c1b1c28f9d8a42947e28b4c20d0755f93cb59895868de566268e6c45db43164f9a6368b0d205afdffd5244eb54dc8ceb677fa2bd46f1122d4aeb3c356d6249301fb4df79dba6cca37b04f2102de8f6a9a5cbb076b68d93cd398f76b9967660c800e76df8cb904467838138773f772f626d6b734e4fc60af66d93cf84bc5d32e5908a1ba7997dead01831e9206863916f8f7b40041cb5eca7413315daf1b9419e03499bec3a0b3f9fa070bc0d23669e85ae98bb7e0b57654a25ae6019ce4fc337c6c6deb2c61c864f5705098b1472841e2e0a83a2a9dbda31502e41ab81df9036be996173d3807daa72b134d2d35ffb0cbe40a54f55bfb1914d89d0469aaa1ab5f0cd95d82770653d24b72019fe20f03686baba5b07c334da8f6260c93d1bea508c4a270b134b086a8127181b80d6422ccf7adc966bc0973060d3a02273a0d94e24789b6cf2652c59ec0a0d3f22b12861a494295409b90903671eac13651f0552ed4ffc8707a2603e815ef56fc954ad2edf59b909d9fdc5ba3f11685800e686def5af814cc797b64bf426576575592e5440037deb855a73cc2c41e65105750b8c03ae0465cae5fb7b09335937df8c8e77eb508a3a9d6d7405eadbd78f8fa5a58aee2292565887a029d3bdd0bf232bbb03fa84ce899df348ad24837a91bf7fc680102e6dd7fa746b16f9f212d347517f6d24cb387bbf48ad6d2717894b14db2cfe639275bc1fb7015d2b3e6475d37d1a146f96b2a35eceff76d97172e146b7a25de4771e0826674c71ba8b16294d00c9f4e6ed0634c68bea2f54ca1f738ff47d12f0cd811dc2ee8a27797d080e935f6111f922a56f67e98131184d8c329421df99ee24c09402e124daaeccb59add3983f527dfd2f10f35e667f1089589102f1c0cf52541b533e42e33bae47c6d3c964369cbe8bb9e20372521615ad01ca1b1b315402a4068cfcca0fb5e701e0159c4c3922ab3dbf801c112daf7f0701b1526f0f667d82616fb43596cd8a67a29d21102aad66a0aa8334e15bd73df5df2ad8e1f54083e79cacf30cc347051f88b43995d7455b01e08fbbaafe45dc368a65130b2ad04652f3aa5f1892c4eaf8787441b02174ca8353c59d747aa18ca91579173358ec5b20b222923b2328360266714d9126266aa3c06836b1a5a4ba351fd45bffdfe4d53cfdb6846dd3ca52f1163ae27477190255ff5f2e2bbb330b485853812e4b815e9c6474d6acbba527dd4e07df9bc73621640d918bf992544c66fefd82859359e6415af31e0b22552ac75e98a33fd2094a6765b40bc67eb47da878a614d7f5b2f40a93b39b35f41362d1561a7fd4d72eaac56f027abe2b5b1baa9a248e3e196481d87f3fde64b34f0acf271eb68f0a04c3fb2a1043a3efb3a595f1c278050b0b48e896ec295929b41614cda00f336865d462164743e4207746fc2abded2f00b4b5b3ebe3386fcaeb52d8a17646840df3dbf821bc035b83af8ac8d112d79ed2723d15a5fd469fe2ccf75931180a676581b52824f8339022021a55c2ff9fe35d5b0a431c0e2f7f6752a7e90b2eff0bf899bdd02f5548b07e2ea531e9942a6d9ad36b1a8d70d6b530c1227481c91679846d7f892c00e5f08e57e156b900d23c32fe429dea6bc54cf76ae79ec51cc6da544c7e1e70c2927ba9675883f9c810c11687b95a3e71d69ee7c7a55a5d983e817bf696ce2209b28d3737f13887e09b447c6b7a7f82389e0aa5f0b8f9cd67a05c2458ba59803eab2dd3a2ef946b307eeef7100e91177480a17ef47f91f69479b7e7c0844a59b2854bcf3c731b78a2de46fe8942da25a3dad2d459c8371d16d9f4f4d3c6a3f6c54391c06d3800dfdf4dee7421fb91c4ade4c22fadb863c14141e66f62183ac01956af3bc7a55a5d983e817bf696ce2209b28d3719af3919950468cbe8b9535bca186afd93777d77714c830cb920f2eafbe8dd8657e156b900d23c32fe429dea6bc54cf7dec73e109b242d46fb5c36f5ff85fcb199e1447fe6eb3988cc62ed83cfdaf576f061eced531305c9ce0df563af50f9b8e9171f46be3c03f4c78805c9f493876ff39a91b67eaff33035256b6e4421bd1bed45b5651bbf5b9e58bc8a9fcc5090a762875d4b68cf78a1735b14cf91687cf2ebe1be4baf65f76de04d733345534d5aa6e0a9a3c3dc47e6bf4aabd776eda5038fdae59b54dc63edfcff1ec9dec795767a63afa3e993fa389a3a4b023161d24a3794251adba1f2aceba02592bf9a2586f5445d064f9d258ddec3647bbd27a16976dc54b9fac44b7a73ca2db98efe27330d0f32217e76ade73f811e21d065d8f4b49e15a2e27dfdb577389ae50bb38b11ec2e596971eb54eaeae3e4875b0b79374453720d7e6b19465000e46612492198e8ee6200abe0d0cf8e54cde29db470b7534fc19532e30fb37450b263fa7cacd0a7b5f43308d5553714cd336dea3986c5f98173988b3a531b32166bb47beeaa29e3ea95d0fa2f15e308868f7f896184243ffb0ff63f49c1f604afb88d5bc4bb863dbe7e5bd982fcac4e5adab3334feca11425f24b21e5e3ecedaf061a98e6915c2cd53e56d64d10d826cfe04df2117573eb64161207aef35d54acbcdf084df9dbbac1bb62d38f29c8f132b1f4df176f11ea8f39dc2036211c5e71227f22a09e57618517f9f70ff1badfb6108728e81126d49a99cb23d20430d6b9f139cf090984a982bc1295778ecd587ffc3b5b79006657d13bcf15ca4ee7a2573494ffd880b75c75ad2e95362d85d1382fe3b9a82050065f10f47f32748fd736ce1c915858e8191f46d63f98eb0a7ac021b07d908f0aad6d003cc393502c9ad772a2e4074365e274a4255f0032f96cf28c1cad7efebd052e256750c71cf8b2c73135fdc8fea33157b9b0e8f19bd789f12093bb2c7e2ad9181db84daeb0dca617b4c6f478756bc25413f39a626d4d7894bb1d6d590668, 0, ''),
(5, 'rateus', 'Rate Our Company', NULL, NULL, 'default', '', 0, 0, '', 0xdf97b1acae16039af2f864b9c2258b8616e69133f0bdf3c2644c74a2e955a841e99375f5ad6246a025503e31a18f24d0b0bee67cd1de95b651b2bbd2e9a6650926f4758777036d091a8ddd7a579f0b8556e05e2f43f732aa6a9c235b591ae0c8c7f63081f615d52be5bf627c6ef9b3f0208a54c126ee8e3d0b2b57db2ed0062fd61dc8d57127c45cd9ee476d3f7688c1a67ea1ba4de1544317ce34ca6120e4b46fca91856092094e81fd57c648839303b3a0e2026ebbdf372c3b2ae2b5861cc1e60ec04a333db71471f91988cb37386e54cb4d04f3326f7f751fe93b1fb90fc6d0137bdf00009e748ffd651d02a1072a2884c9f57499da23a92557d8316c84aa1c01b5da68df8f5859f959996131d31d5f773adf4485e61193f3bdfca93d9b83bf892edf5524b56ed616c9b49f5bc21549b43f0d228045fd5de29d8f87606b9aa200a07c37244597c01f81eb79c65921e7eb1e440752d26d6603cffb1cfe2f983f67f472dc04adf5e5b06ec535d9a9bd6bcdb2825d6172402cb75b2d72676de8d9cc7c71b16366c8e29fc2882905700b7dc9412143b9e3d1368980800e17795dd5e8677969140efc21fb7a2070f9c20f2b47578968df800ea354ce1b96ca2a5fa5496cdeca3d18ebea3decdc792137fb738d08431dea67d99141e6a823cf46cbb711db1a8551ac376f81869080bd6193a552de93e11e066a16cfd17dc28e0a8f7b0fdd62660c9d2b62677db5b0e9ca385b8ad11d64930fb642f13b8ca22db890005114591a59c4899fc0834b5d8aa4d58e2af0c5611281511c76b51d75d59fb2fa1e73e6b8cdb1f02eae3d75a2892f3b32dc87fdf145dcfaf6a3ab6c5da759051952328152274e7609fe98f38bf85e21369066721583e2338c982d10fbdeb190af8e9bd254f8be31dbccfc1c0d78af5a6da05c587fc3efeaf7c7ea976af0fd87bebd08b09e72ec6651be87bd65d8ab44df03fd33eb5f0d980c50106629e28de106a96d0fe5d12ac137bfb6e5be24d8c2860a5da3d5977318f3e7660e2aa5c7a9ab07d7349852bb2bfdc3deed1f2948d5ed4cdced98334ccb6288011bae95ac40bbf6ded6b32808a06724494a549a7bd3043fb852adc36d343dc1db4207659fabcf0ccc8031688600d04f184a4e1900f25f0c263d197c245576a04205f37d9f15957decbcec78dbc33a762e28503c5e40cb869ba2c2a560a3157a5d9f52b555fee414dd372b061cd0659b512f7487b27003cf8a3dab8f88dd1f1eaaa20878a8c16d77aae24f11d38872b76a2e0fd353aefcd26abd36e11d0399f434f4d529343eebc154a2078f760bfde711b5a5a7b9cc48aa9be8c36ea6ebb7d756d6aa2f57213d3b8d915c378e6e934049a715e5b869110ccac2e1c749f74821f4f21262d4273ae67a039f8942d602a2702b3a31a139e32b7eb8f1ce0a808acde22e6070d66aecae2a28e65a5fa435d4f18d9da630e870b3fe18999a7b90ca7bba2acdd4ae62c219aee8cefd5cce4c6d185f791cea6bcedabe2551bd55a6ab497587936f0a9cf8365c167117bb4fef99814c253a285fe9954d956c23460262999d92c0c0e0cf5e847293bdd9a9323ea2baeeaadf7af44113f14fba5aa814aa06034d0f6742cfdd6f66c55fed450b6e43b89b526bf33a996a39dbd8967be3a3a6f203806867a651755706ee95ecd6bf365ac5ee2e6109f8d0c0f446dd89cd521164dd8f7734036609750a2305a9e7f28bf9594ff1699d5824b6f27ad2fdd0236d7bdbd8a7498054983a2a0ef69458c598a6b5620484cea7fde2b691011ec9ed238bd0bc22f061994c10bb3110d4529e03911a56378aae0c910790dcc6d659b088be1c309c994122aea4bddb85884a842e2f38075ca550b44f6f35f31a7e40be643d33461b234f6c1c1743e69183d126debb67845f4481697b865c6144cd495f028a048513a6691e3c4d944d286c8250b7083725f32d091e3271b4e97e23c52cbaa24cffb181e5948a5efbdd6368e219d3d32a45f25c408e8278783cd8fc71a089dbaefabf017e834b4fe01ca6f82cf1c96fa4eb7905aaf092899495eab13b96ffff11bf78bdc5715b8ac5513236a085f0c6dc2ee6e98edc240f8f2f04d9fc351a592a08dd95d2f2e920d65299ff55749da3d9befd010ef85768ac6e2a8787be972d53227af795578573457a33573e99ef6eb951673ba2b012e22b5757c9baaa435b25eadebbb5268cc64af0b1ca47ca659054957473ec9fe90686fa7eb31c965ca555a25e25d4ff6bd95ea79aa919378a364a6e11ebf09ebbce1952f2b639de2c8bab80f3e7e93a882c514f68667ad85e098ba0c8efb7f04d6a991f182770cc4d6286dfe45fb9a96bf7426c93cbd5eaa0d95c417c99233dd9a545c401e4b6745ed15c4e9efb75192ac2be08220b5261f25502a2e388b72e234ec71d3762e60819d4ef55c68696617db6c966754bb0815fc5deff727a802639e209fdb8b94d07e10d34b4c23448cdf00373369b9b6aa41ac4f8d7eaf3e800177d04fc343209d6182e0bb09d8ab2e9df0b11e21cff68292db3ef36d822bbb89cc6f4bd2d548765773dabf416c2123cc7a0f8f60f7730c27b469a0b285370e6da9d0f2a72e608163ad1d556df24e320bbc28f63bccb82eb4a912f051a9c66cedf237d4ef457e0ff67f721984c6d21e65f1770c882a48af32459e6a01e844efaf838364789e32ae8535712f339acc4cc5b4c8723da537fe21c822655ffceb011493f11ee5a6cb007311b30330daaa220c7fe20e7ed1e2b3e75ab6f6c3d8277fbf2789c7b8263931fddabd1898b178433245b1ed4c07664f4e5ff9ff429434d3b8fe57bf0be9cf76f0ea29cf9c968537deb1ab7c97a7cdb5fcdd9d9245d466cfbfb7a8cebd5f49e75962f616ece50cc2a2db8b84feac82bba3eaa187e175f1fffe83794d56f2deed948659f2cb1dfa04759ef0f4680dcaa7587c4d6dc2d2e40645a88ddf46eb64e60ba961e0ab15b59604e05db5e335964598f19afc8268f53d2adc37ec8c3e77ca1b249e81eb997932a4372ead2267579c2d681d0b9b2c03331d42ff34a0821140a0e9bf08ff9973416a719fd9e5244f9186ad6ea1806a350cd533b20fd97c23c2e42ef1ddc66fa887411d96ad9bf47a246c6ac7d75375bef68a02619bb8c8b021add04614c3af3002684ebbfe347d4dcbc70b926eb4c9f3d1203396d8dd7bed5377073027428cce93f3eac6dcbc819af28c8b28332a09021abfca674415548e220fbcbecd45bd9b05b60f810611ae5d13a48000152a46a677ee8ba81d322e47de4ada31c3de8dd1528d1ddd000c38c95298791864ece221ae8388ffe084104004145284763eb4514d76735a77f51d38576408fa39803eaf4d76e6194dacdb8e7b3c5c029848f38972c1b2da47c96d2f5bf7adfd5dbc1102bfc8d94a1523f5daa0c8b155ca0b1b167f9bf32967f88b80d85d82f51a73f43b87e9e699db27293466af1a72c93b2a9fc6728293812c22c74dfbb30751478c637882afa6aba23fe3cf4324e40c35138574f26523b17d2b01bd16ba523c4a1a0e72e8978e338a4bff4122a0613682b8e862dcf00a03314e63a603fe3e03ed4b9d5cc0bd5d026c7ca0590a49d8b60dd6aea55ec246b65ba2ae0f29cdee62ae25b8093f15771c110e15874be7f72a6b3980f24afa757434e27bb7e7b5c635a25c7516f1a11a589abfd9fc3015ef3a93320bf76e5a2b557c0a123c6501f5e777025cff42284e097d9ce228abfb25ae037827606a3142c51f757ac3ed294b774b0967c2ec1ef89af0b90db874ea5d3a6652f2b94732224fd1d6b7380fa25f70c7f9817d116b7a566ff538927ad8cdc5b72700e7c3b4bc9346d853228df2d0e02c2838f1010a0e75665280bee1112653809d3a73f96574f86f58ffbc7607c1499a7aed0a72f8e6c0464aa50c8300662155a05052147bc7a1dec0824b1b9f9a4a2a7c446b06bb7bbb3b747a030619edfe2a5768fbcb992138f271aca1606799c7a732c52b71a887bc585bb197aa704954472ddee, 0, 'Active'),
(6, 'How', 'How it works', '', '', 'default', '', 0, 0, '', 0xf1f9e77d01dd17043bf79aab9e8c7502b69071880b2daa8d321641eefb18fd394441840fb27399a76b4cf0b231414f5e83edd7b4fe16d45b50e703f4dbffa17ee9420552498ed69f8f4a68ae82b707265a8265cb81c4a1ed47e276fdb77e1aee8772222b91359d7d99ebebdae4de5df10054f6c528f6fe6aec5fa6dbffbfdadc80c82c62b3494dd74f98aee111d9dc09c578f6749d7a984fc6474c1b3635bddeaffdf9a4b74fbc5aacb5c8d4f33e61553054903e36c9ebccb20c40fccb5f2f7a56a92b13ab15719c2e6ea834cdb52e8488555841571a447148c86addc7f17a5837f5bfa498bcaeb82acc42caf14874a32f71330a39a9356c9f11e49b05eb7f71e52494166b9de559bda2e09381aa0c016ce7a9ecb4362849311c1ba10416cc21956bbbd4a99af23e8162e01ccf80a0e91484b9b9d3422842c3e19241ba2ac007cdf1d81b9d861ad9dec63915fca62322c404f975360344a1043124643e98fe9ae5b75b25ab4e11ca09303e88e06f383b51bca3b82eddd8ada4c59ed8f2e21ad8c28ce0d2aff3c0845863868bea20c5a6e3cb7dc62da95c7c4a906c67bfe49e499c691f3d8b52876c95e5ed1072e04f53213e81466854c32890e8b45b78cb8fa4dd5b236b2086ca02e3109a4665c169a87c8bbd77da98d1d75665e3109b2566604a9950c06855f807abedacd91b3ae2b283edd7b4fe16d45b50e703f4dbffa17e129bfdea9d8d7fc9db63de427ac60aaa49cc2390b1c4def770bdf537e34353b9c60ce31f7d37f178835dd840b7f8435b7fcee46771e465cbecb81ad96c16ae48b90ade69a604f11b3bafb0b85017c13841d1251c5fb3c8f858f002a0f7806a3cf6ab7a8099158a28c44eeaa248c9289f86c82fd5c4b2a871877306d22198d424fc78251b0c141f8aa5939dcecb3a8fef49601b2566782914aad30a745d1c9214d76a4e8ef6d48738da8bb33af4a83e5766fdd76f0d8ff674be99a5c6545a64049323b3c838918e6b479b57dca2e7c374a48d15af09fbd3c3c62d714f2a82a3a992fc14021019fddd731dac6f207c71ec9e46f2a41fc08508eba6ba0d9f71d4949f4f9a11cbb4cb18bc5286cdd1273e6213dee44a534f61280620993701288522d12deb4e9ec7a78ef8992d4edfa3f193a0529ed5977312d28ff77f4a1a5b13ad39bbebcd164eee2291ba573886841496f5ce7a7930456f4589936a30aff9baa1601c8aeedbadba7501fed42ccc277419c9e731edafc15f83d43f2a2a32a9a12ecb3f2a33907027a2f113420a07aa008d69e7b938549e81008160208be83707f444dfc43028212afa9a1f1b9658f32248df3c17053648bd68c2cada7cfff454ea379c69823e519c7c608705a4799c9c720ccabd2833571248e374184d967fcd0e8088d683bfb10ac3d8e141db40901a752ffdb7f244629a8c120a78f0c7028f752d25eb1e99db25229aa676e4b3827e6f456d8c34a1a733e2281939b8a71811eab0a648da252c6aba2acaf2ef638b827169a69df66a50dd4b4757d5025c2fdc36776837789b61ca21c0192099ba3650578c2b0138b6d357f10bd50acfdfdf6490bf1c078e872ab1749b5f719b1c46304f31e66c83f3ea0cb96825e8343494cdd486d228a3331a36684fd3720038012d3263f988b87b4821941872787483a8ccbc66cdb8fdd80539bce20a7adcc3dd3be5414c13973754c2ed71e238e4f52aabc986a3b9faacfb4cbac1de12d5079bbbbe9f8c7e23d7cc29e6d29cfb77f849e96c83edd7b4fe16d45b50e703f4dbffa17e965f423937511112933852654308a0339555a3b28d02c70e8fc4f98937a52d02ff33612a2b77a84136d112b6bfd0675b2cf10fa754bc56e862d1d24b2c20262c953dae86fdaa639ffcb823c7f02e5503835b426fb8733996a6504669c47eacdfe666f48aebcb7a6ed06e97f4de5820d338c5f51d7775169f2f2a0e98f675488e3e155293f473913b606ab489bde6f2b6d42bcd74cc49eb33cdc9f495cae850b083edd7b4fe16d45b50e703f4dbffa17efb2956dd1735995146cf3bcea3992120c809b0b34b341fd88c6928ca31894ca24b43f0a0bb916beb3fdb1d86c5776b36f022cb2f1464250b969b97377b1986b4f8a8ba21d481265c8f533a79638e8470b48cb6e96de59dcb0286a1930c411013c22b61f68920c157ccc762960bc2e257bfe7925be42e4f862821097b884925fca606f217f55c804004b7f2057dda167e, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `_content_testimonies`
--

CREATE TABLE IF NOT EXISTS `_content_testimonies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `date` date DEFAULT NULL,
  `content` longblob,
  `status` enum('Pending','Approved','Disapproved') NOT NULL DEFAULT 'Pending',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `_core_cronjobs`
--

CREATE TABLE IF NOT EXISTS `_core_cronjobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mod` varchar(50) NOT NULL,
  `interval` text NOT NULL,
  `last_cronjob` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `_core_cronjobs`
--

INSERT INTO `_core_cronjobs` (`id`, `mod`, `interval`, `last_cronjob`) VALUES
(1, 'ads', '3600', '1451925001'),
(2, 'instant', '0', '1451926802'),
(3, 'payment', '86400', '1451845802'),
(4, 'mailer', '0', '1451926802'),
(5, 'promo', '86400', '1451845802');

-- --------------------------------------------------------

--
-- Table structure for table `_core_email_templates`
--

CREATE TABLE IF NOT EXISTS `_core_email_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner` varchar(50) NOT NULL DEFAULT 'system',
  `name` text NOT NULL,
  `subject` text NOT NULL,
  `body` text NOT NULL,
  `send` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `_core_email_templates`
--

INSERT INTO `_core_email_templates` (`id`, `owner`, `name`, `subject`, `body`, `send`) VALUES
(1, 'system', 'Newsletter', '{subject}', '\r\n{body}\r\n<br><br>\r\n<center>You are recieveing this email because you are a member of {websitename}.<br>If you do not want to recieve emails from us anymore please log into your account to update your email preferences</center><br>\r\n{signature}', 'Yes'),
(2, 'account', 'Welcome Email', 'Welcome to {websitename}', '<p>Dear {firstname} {lastname},</p>\r\n<p><br>Thank you for your registration at {websitename}. Your personal information are as follow:</p>\r\n<p>\r\nUsername: {username}<br />\r\nPassword: **hidden**<br />\r\nEmail : {email}<br />\r\nYou can now login to your account. If you have any questions please contact us.\r\n</p>\r\n{signature}', 'Yes'),
(3, 'account', 'Account Activation Email', 'Please verify your email address', '<p>Dear {firstname} {lastname},</p>\r\n<p><br>Thank you for your registration at {websitename}. To complete your registration please click on the link below.</p>\r\n<p>{link}</p>\r\n<p>This is a auto-generated message please don''t reply it</p>\r\n{signature}', 'Yes'),
(4, 'account', 'Reset Password Email', 'Password reset requested', '<p>Dear {firstname} {lastname},</p>\r\n<p><br>You have requested to reset your account password for {websitename}. To complete this process please click on the link below.</p>\r\n<p>{link}</p>\r\n<p>This is a auto-generated message please don''t reply it</p>\r\n{signature}', 'Yes'),
(5, 'account', 'Login Verification Email', 'Please verify account login', '<p>Dear {firstname} {lastname},</p>\r\n<p><br>It appears that there has been an attempt to access your account at {websitename} from IP address {ipaddress}. If this is you trying to access your account from a different IP, enter this code <b>{code}</b> to complete login, otherwise , login to your account and change your password.</p>\r\n<p>For security reasons, do not share your password with anyone. If you have any questions feel free to contact our support team.</p>\r\n<p>This is a auto-generated message please don''t reply it</p>\r\n{signature}', 'Yes'),
(6, 'ads', 'Website Approved', 'Your Website has been Approved', '<p>Dear {firstname} {lastname},</p>\r\n<p><br>Your Website,{website} added on {added} at {websitename} has been approved and is now in rotation. To view the traffic statistics please log into you account.</p>\r\n<p>This is a auto-generated message please don''t reply it<br>\r\n{signature}', 'Yes'),
(7, 'ads', 'Website Disapproved', 'Your Website has been disapproved', '<p>Dear {firstname} {lastname},</p>\r\n<p><br>Your Website,{website} added on {added} at {websitename} has been disapproved. This may be because it does not conform to the terms stated in our terms and conditions page. please review our terms and conditions to see why your website was disapproved.</p>\r\n<p>This is a auto-generated message please don''t reply it<br>\r\n{signature}', 'Yes'),
(8, 'instant', 'New Deposit', 'Deposit Successfull', '<p>Dear {firstname} {lastname},</p>\r\n<p><br>This email is to notify you that your deposit on {date} has been completed successfully. \r\nTransaction details are as follows:</p>\r\n<p><b><i>TRANSACTION DETAILS</i></b></p>\r\n<ul>\r\n<li>Date: {date}</li>\r\n<li>Amount Paid: ${amount}</li>\r\n</ul></p>\r\n<p>This is a auto-generated message please don''t reply it<br>\r\n{signature}', 'Yes'),
(9, 'instant', 'Deposit Expiry', 'Your Deposit is now Expired', '<p>Dear {firstname} {lastname},</p>\r\n<p><br>This email is to notify you that your deposit on {date} has now expired. \r\nTransaction details are as follows:</p>\r\n<p><b><i>TRANSACTION DETAILS</i></b></p>\r\n<ul>\r\n<li>Date: {date}</li>\r\n<li>Amount Paid: ${amount}</li>\r\n<li>Total Earned: ${earned}</li>\r\n</ul></p>\r\n<p>This is a auto-generated message please don''t reply it<br>\r\n{signature}', 'Yes'),
(10, 'wallet', 'New Deposit', 'Your deposit has been completed', '<p>Dear {firstname} {lastname},</p>\r\n<p><br>This email is to notify you that your deposit into your wallet balance has been completed. \r\nTransaction details are as follows:</p>\r\n<p><b><i>TRANSACTION DETAILS</i></b></p>\r\n<ul>\r\n<li>Date: {date}</li>\r\n<li>Amount Paid: ${amount}</li>\r\n<li>Payment Method:{method}</li>\r\n</ul></p>\r\n<p>This is a auto-generated message please don''t reply it<br>\r\n{signature}', 'Yes'),
(11, 'wallet', 'New Withdrawal Request', 'Your withdrawal request has been initiated', '<p>Dear {firstname} {lastname},</p>\r\n<p><br>This email is to notify you that your withdrawal requested on {date} has been initiated. \r\nTransaction details are as follows:</p>\r\n<p><b><i>TRANSACTION DETAILS</i></b></p>\r\n<ul>\r\n<li>Date: {date}</li>\r\n<li>Amount Withdrawn: ${amount}</li>\r\n<li>Payment Method:{method}</li>\r\n</ul></p>\r\n<p>This is a auto-generated message please don''t reply it<br>\r\n{signature}', 'Yes'),
(12, 'wallet', 'Withdrawal Request Completed', 'Your withdrawal request has been completed', '<p>Dear {firstname} {lastname},</p>\r\n<p><br>This email is to notify you that your withdrawal requested on {date} has been completed. \r\nTransaction details are as follows:</p>\r\n<p><b><i>TRANSACTION DETAILS</i></b></p>\r\n<ul>\r\n<li>Date: {date}</li>\r\n<li>Amount Paid: ${amount}</li>\r\n<li>Payment Method:{method}</li>\r\n<li>Reference:{reference}</li>\r\n</ul></p>\r\n<p>This is a auto-generated message please don''t reply it<br>\r\n{signature}', 'Yes'),
(13, 'promo', 'Tell a Friend', '{friend}, Look What I Found', '<p>Hi {friend},</p>\r\n<p><br>I found an amazing website that i think you will be interested in. Click on the link below to check it out. </p>\r\n<p>{link}</p>\r\n<p>Regards,<br>\r\n{firstname} {lastname}<br></p>', 'Yes'),
(14, 'promo', 'New Referral', 'You have a new referral', '<p>Dear {firstname} {lastname},</p>\r\n<p><br>This email is to notify you that someone has joined our website using your referral link. Here is your referral information: </p>\r\n<ul>\r\n<li>Username: {referral}</li>\r\n<li>Joined on: {date}</li>\r\n</ul>\r\n<p>This is a auto-generated message please don''t reply it<br>\r\n{signature}', 'Yes'),
(15, 'promo', 'Referral Commission', 'You have just earned referral commission', '<p>Dear {firstname} {lastname},</p>\r\n<p><br>This email is to notify you that you have just earned referral commission. Transaction details are as follows: </p>\r\n<p><b><i>TRANSACTION DETAILS</i></b></p>\r\n<ul>\r\n<li>Referral: {referral}</li>\r\n<li>Amount: ${amount}</li>\r\n<li>Description: {description}</li>\r\n</ul></p>\r\n<p>This is a auto-generated message please don''t reply it<br>\r\n{signature}', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `_core_events`
--

CREATE TABLE IF NOT EXISTS `_core_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mod` varchar(50) NOT NULL,
  `event` tinytext NOT NULL,
  `task` tinytext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `_core_events`
--

INSERT INTO `_core_events` (`id`, `mod`, `event`, `task`) VALUES
(1, 'ads', 'on_register', 'modules/mod_ads/sys/on_register.php'),
(2, 'ads', 'on_delete_user', 'modules/mod_ads/sys/on_delete_user.php'),
(3, 'instant', 'on_delete_user', 'modules/mod_instant/sys/on_delete_user.php'),
(4, 'wallet', 'on_register', 'modules/mod_wallet/sys/on_register.php'),
(5, 'wallet', 'on_delete_user', 'modules/mod_wallet/sys/on_delete_user.php'),
(6, 'payment', 'on_delete_user', 'modules/mod_payment/sys/on_delete_user.php'),
(7, 'promo', 'on_start_session', 'modules/mod_promo/sys/on_start_session.php'),
(8, 'promo', 'on_register', 'modules/mod_promo/sys/on_register.php'),
(9, 'promo', 'on_delete_user', 'modules/mod_promo/sys/on_delete_user.php');

-- --------------------------------------------------------

--
-- Table structure for table `_core_hooks`
--

CREATE TABLE IF NOT EXISTS `_core_hooks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mod` varchar(50) NOT NULL,
  `hook` tinytext NOT NULL,
  `install` tinytext NOT NULL,
  `uninstall` tinytext NOT NULL,
  `status` enum('Installed','Uninstalled') NOT NULL DEFAULT 'Uninstalled',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `_core_modules`
--

CREATE TABLE IF NOT EXISTS `_core_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  `order` int(11) NOT NULL,
  `access` int(11) NOT NULL,
  `root_directory` text NOT NULL,
  `members_directory` text NOT NULL,
  `admin_directory` text NOT NULL,
  `controller_directory` text NOT NULL,
  `controller_filename` text NOT NULL,
  `controller_classname` text NOT NULL,
  `installed` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  `status` enum('Enabled','Disabled') NOT NULL DEFAULT 'Enabled',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `_core_modules`
--

INSERT INTO `_core_modules` (`id`, `name`, `title`, `order`, `access`, `root_directory`, `members_directory`, `admin_directory`, `controller_directory`, `controller_filename`, `controller_classname`, `installed`, `status`) VALUES
(1, 'account', 'Account Manager', 0, 1, 'mod_account', 'mod_account/members', 'mod_account/admin', 'mod_account/inc', 'controller.inc.php', 'Account', 'Yes', 'Enabled'),
(2, 'content', 'Content Manager', 0, 1, 'mod_content', 'mod_content/members', 'mod_content/admin', 'mod_content/inc', 'controller.inc.php', 'Content', 'Yes', 'Enabled'),
(3, 'ads', 'Ads Manager', 0, 1, 'mod_ads', 'mod_ads/members', 'mod_ads/admin', 'mod_ads/inc', 'controller.inc.php', 'Ads', 'Yes', 'Enabled'),
(4, 'instant', 'Instant AdShare', 0, 1, 'mod_instant', 'mod_instant/members', 'mod_instant/admin', 'mod_instant/inc', 'controller.inc.php', 'Instant', 'Yes', 'Enabled'),
(5, 'wallet', 'Wallet Manager', 0, 1, 'mod_wallet', 'mod_wallet/members', 'mod_wallet/admin', 'mod_wallet/inc', 'controller.inc.php', 'Wallet', 'Yes', 'Enabled'),
(6, 'payment', 'Payment Manager', 0, 1, 'mod_payment', 'mod_payment/members', 'mod_payment/admin', 'mod_payment/inc', 'controller.inc.php', 'Payment', 'Yes', 'Enabled'),
(7, 'mailer', 'Mass Mailer', 0, 1, 'mod_mailer', 'mod_mailer/members', 'mod_mailer/admin', 'mod_mailer/inc', 'controller.inc.php', 'Mailer', 'Yes', 'Enabled'),
(8, 'promo', 'Promotions Manager', 0, 1, 'mod_promo', 'mod_promo/members', 'mod_promo/admin', 'mod_promo/inc', 'controller.inc.php', 'Promo', 'Yes', 'Enabled');

-- --------------------------------------------------------

--
-- Table structure for table `_core_settings`
--

CREATE TABLE IF NOT EXISTS `_core_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mod` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `hint` text NOT NULL,
  `group_name` text NOT NULL,
  `value` varchar(100) NOT NULL,
  `input_type` varchar(100) NOT NULL DEFAULT 'text',
  `data_type` varchar(100) NOT NULL,
  `options` text NOT NULL,
  `required` enum('Yes','No') NOT NULL DEFAULT 'No',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=61 ;

--
-- Dumping data for table `_core_settings`
--

INSERT INTO `_core_settings` (`id`, `mod`, `name`, `description`, `hint`, `group_name`, `value`, `input_type`, `data_type`, `options`, `required`) VALUES
(1, 'system', 'websitename', 'Website Name', '', 'General Settings', 'AdvShare', 'text', 'string', '', 'Yes'),
(2, 'system', 'websiteurl', 'Website URL', '', 'General Settings', 'http://cashking.me', 'text', 'string', '', 'Yes'),
(3, 'system', 'default_pagetitle', 'Website Title', '', 'General Settings', 'Get Paid for Advertising ', 'text', 'string', '', 'Yes'),
(4, 'system', 'default_meta_description', 'Website Description', 'This is the description of your website as you want it to appear on search engines', 'General Settings', 'Website Description Here', 'textarea', 'string', '', 'No'),
(5, 'system', 'default_meta_keywords', 'Website Keywords', 'These are keywords that will be used to search for your website in search engines', 'General Settings', 'Default Keywords Here', 'textarea', 'string', '', 'No'),
(6, 'system', 'default_theme', 'Default Theme', '', 'General Settings', 'default', 'select', 'string', 'system::themes', 'Yes'),
(7, 'system', 'admin_email', 'Support Email', 'Set email to recieve all contact and inquiry', 'General Settings', 'admin@yourdomain.com', 'text', 'string', '', 'Yes'),
(8, 'system', 'noreply_email', 'System Email', 'this is the email that will appear in all system notification emails', 'General Settings', 'noreply@yourdomain.com', 'text', 'string', '', 'Yes'),
(9, 'system', 'adminsignature', 'Admin Signature', 'This will be the signature for all your newsletters and messages', 'General Settings', 'Amin Signature Here', 'textarea', 'string', '', 'Yes'),
(10, 'account', 'allow_registration', 'Allow Registration', 'Disabling this will disable users from creating an account on your website', 'Account Registration', '1', 'select', 'bool', '', 'Yes'),
(11, 'account', 'disallow_duplicate_ip', 'Disallow Duplicate IP', 'Disallowing duplicate IP will disallow users from creating multiple accounts from the same computer or same ip address', 'Account Registration', '0', 'select', 'bool', '', 'Yes'),
(12, 'account', 'confirm_email', 'Enable Confirmation Email', 'If enabled, a confirmation email will be sent to users on signup for account activation', 'Account Registration', '0', 'select', 'bool', '', 'Yes'),
(13, 'account', 'use_security_question', 'Enable Security Question', 'If enabled, users will be required to create a security question for security purposes', 'Account Registration', '1', 'select', 'bool', '', 'Yes'),
(14, 'account', 'use_captcha', 'Enable Captcha Image', 'We suggest that this be enabled to prevent spam', 'Account Registration', '1', 'select', 'bool', '', 'Yes'),
(15, 'account', 'virtual_keyboard', 'Enable Virtual Keyboard', 'Enabling this will let users have the option of entering their password with a virtual keyboard', 'Account Registration', '1', 'select', 'bool', '', 'Yes'),
(16, 'account', 'user_contact_info', 'User Contact Info', 'Setting this to required will make the system require user contact info on signup. Passive, will make it passive. Remove will remove contact fields from the Registration form', 'Account Registration', 'Required', 'select', 'string', 'account::contact_info_options', 'Yes'),
(17, 'account', 'min_username_length', 'Minimum Username Length', '', 'Account Registration', '4', 'text', 'integer', '', 'Yes'),
(18, 'account', 'max_username_length', 'Maximum Username Length', '', 'Account Registration', '10', 'text', 'integer', '', 'Yes'),
(19, 'account', 'min_password_length', 'Minimum Password Length', '', 'Account Registration', '4', 'text', 'integer', '', 'Yes'),
(20, 'account', 'max_password_length', 'Maximum Password Length', '', 'Account Registration', '10', 'text', 'integer', '', 'Yes'),
(21, 'account', 'referrer_required', 'Referrer Required', 'All users joining website must be referred by an existing user', 'Account Registration', '0', 'select', 'bool', '', 'Yes'),
(22, 'account', 'default_referrer', 'Default Referrer', 'This user will used as the default referrer on the registration form. Leave blank if not needed. User should be a valid user if used', 'Account Registration', 'advshare', 'text', 'string', '', 'No'),
(23, 'account', 'disallowed_usernames', 'Disallowed Username', 'Comma seperated list of usernames that cannot be used during registration', 'Account Registration', 'administrator,support,webmaster', 'textarea', 'string', '', 'No'),
(24, 'account', 'disallowed_email_domains', 'Disallowed Email Domains', 'Comma seperated list of email domains that cannot be used during registration.', 'Account Registration', '', 'textarea', 'string', '', 'No'),
(25, 'account', 'disallowed_ip_adresses', 'Disallowed IP adresses', 'Comma seperated list of IP adresses that cannot be allowed during registration. ', 'Account Registration', '', 'textarea', 'string', '', 'No'),
(26, 'ads', 'activate_website_on_add', 'Activate websites automatically once added', 'If enabled, a members website will autmatically approved when added to system', 'Ads Settings', '1', 'select', 'bool', '', 'Yes'),
(27, 'ads', 'activate_banner_on_add', 'Activate banners automatically once added', 'If enabled, a members banner will autmatically approved when added to system', 'Ads Settings', '1', 'select', 'bool', '', 'Yes'),
(28, 'ads', 'activate_textad_on_add', 'Activate text ad automatically once added', 'If enabled, a members text ad will autmatically approved when added to system', 'Ads Settings', '1', 'select', 'bool', '', 'Yes'),
(29, 'ads', 'activate_listing_on_add', 'Activate directory listing once added', 'If enabled, a members listing will autmatically approved when added to system', 'Ads Settings', '1', 'select', 'bool', '', 'Yes'),
(30, 'ads', 'website_credits_on_join', 'Free Website Credits on Signup', 'This is the number of free website credits a user gets when they signup', 'Ads Settings', '', 'text', 'integer', '', 'No'),
(31, 'ads', 'banner_credits_on_join', 'Free Banner Credits on Signup', 'This is the number of free banner credits a user gets when they signup', 'Ads Settings', '', 'text', 'integer', '', 'No'),
(32, 'ads', 'textad_credits_on_join', 'Free TextAds Credits on Signup', 'This is the number of free text ads credits a user gets when they signup', 'Ads Settings', '', 'text', 'integer', '', 'No'),
(33, 'ads', 'max_websites_1', 'Max websites for free members', 'Maximum websites free members can add', 'Ads Settings', '5', 'text', 'integer', '', 'Yes'),
(34, 'ads', 'max_banners_1', 'Max banners for free members', 'Maximum banners free members can add', 'Ads Settings', '5', 'text', 'integer', '', 'Yes'),
(35, 'ads', 'max_textads_1', 'Max textads for free members', 'Maximum textads free members can add', 'Ads Settings', '5', 'text', 'integer', '', 'Yes'),
(36, 'ads', 'max_listings_1', 'Max directory listings for free members', '', 'Ads Settings', '5', 'text', 'integer', '', 'Yes'),
(37, 'ads', 'max_websites_2', 'Max websites for upgraded members', 'Maximum websites upgraded members can add', 'Ads Settings', '5', 'text', 'integer', '', 'Yes'),
(38, 'ads', 'max_banners_2', 'Max banners for upgraded members', 'Maximum banners upgraded members can add', 'Ads Settings', '5', 'text', 'integer', '', 'Yes'),
(39, 'ads', 'max_textads_2', 'Max text ads for upgraded members', 'Maximum text ads upgraded members can add', 'Ads Settings', '5', 'text', 'integer', '', 'Yes'),
(40, 'ads', 'max_listings_2', 'Max directory listings for upgraded members', '', 'Ads Settings', '10', 'text', 'integer', '', 'Yes'),
(41, 'ads', 'default_website', 'Default Website', 'Website that will be displayed on surf rotator in absence of any active website', 'Ads Settings', 'http://cashking.me', 'text', 'string', '', 'Yes'),
(42, 'wallet', 'allow_deposit', 'Allow Deposit', 'Enable this to allow users deposit money their wallet. ', 'Wallet Settings', '1', 'select', 'bool', '', 'Yes'),
(43, 'wallet', 'deposit_withdrawable', 'Deposits are withdrawable anytime', 'If this is set to yes, users will be able to withdraw their deposits from their wallet anytime. If not, they will only be able to spend it within the system and withdraw only after deposits have been used to purchase share(s) ', 'Wallet Settings', '0', 'select', 'bool', '', 'Yes'),
(44, 'wallet', 'min_deposit_allowed', 'Minimum Deposit Allowed', 'Minimum amount allowed for funding wallet(Set to 0 for no limit)', 'Wallet Settings', '5', 'text', 'integer', '', 'Yes'),
(45, 'wallet', 'max_deposit_allowed', 'Maximum Deposit Allowed', 'Minimum amount allowed for funding wallet(Set to 0 for no limit) ', 'Wallet Settings', '10000', 'text', 'integer', '', 'Yes'),
(46, 'wallet', 'allow_withdrawal', 'Allow Withdrawal', 'If this is enabled, users will be able to withdraw money from their wallet', 'Wallet Settings', '1', 'select', 'bool', '', 'Yes'),
(47, 'wallet', 'allow_auto_withdrawal', 'Allow Auto Withdrawal', 'If enabled withdrawals that do not exceed maximum amount for auto withdrawal will be processed automatically (You also have to enable auto withdrawal settings for each payment processor and set up API information for this to work)', 'Wallet Settings', '1', 'select', 'bool', '', 'Yes'),
(48, 'wallet', 'max_auto_withdraw_allowed', 'Maximum Amount Allowed For Auto Withdrawal', 'If a withdrawal amount exceeds this amount it will be processed manually instead of automatically (Set to 0 for no limit)', 'Wallet Settings', '1000', 'text', 'integer', '', 'Yes'),
(49, 'wallet', 'min_withdraw_allowed', 'Minimum Withdrawal Allowed', '(Set to 0 for no limit)', 'Wallet Settings', '5', 'text', 'integer', '', 'Yes'),
(50, 'wallet', 'max_withdraw_allowed', 'Maximum Withdrawal Allowed', 'Maximum amount a member can withdraw per day (Set to 0 for no limit) ', 'Wallet Settings', '10000', 'text', 'integer', '', 'Yes'),
(51, 'wallet', 'withdrawal_days', 'Withdrawal Days', 'Choose days when members can make a withdrawal', 'Wallet Settings', ',Mon,,Tue,,Wed,,Thu,,Fri,', 'multi_select', 'varchar', 'system::days_options', 'No'),
(52, 'wallet', 'num_withdraw_allowed', 'Number of Withdrawals Allowed Per Day', '', 'Wallet Settings', '1', 'text', 'integer', '', 'Yes'),
(53, 'wallet', 'allow_transfer', 'Allow Internal Transfer', 'If this is enabled, users will be able to transfer money from their wallet to another users wallet', 'Wallet Settings', '0', 'select', 'bool', '', 'Yes'),
(54, 'wallet', 'min_transfer_allowed', 'Minimum Transfer Allowed', '', 'Wallet Settings', '5', 'text', 'integer', '', 'Yes'),
(55, 'wallet', 'max_transfer_allowed', 'Maximum Transfer Allowed', '', 'Wallet Settings', '10000', 'text', 'integer', '', 'Yes'),
(56, 'wallet', 'transfer_fee', 'Transfer Fee', 'This is the transaction fee that will be charged for transfers', 'Wallet Settings', '5', 'text', 'integer', '', 'Yes'),
(57, 'wallet', 'free_cash_on_signup', 'Free Cash on Signup', 'Bonus cash on signup (Users will only be able to spend it within the system - cannot be withdrawn)', 'Wallet Settings', '0', 'text', 'integer', '', 'No'),
(58, 'wallet', 'free_cash_method', 'Payment method for free cash', 'Select what payment method you want the free cash to be paid in', 'Wallet Settings', '', 'select', 'integer', 'payment::processors', 'No'),
(59, 'payment', 'default_currency', 'Default Payment Currency', '', 'Payment Settings', 'USD', 'select', 'string', 'payment::currency_options', 'Yes'),
(60, 'mailer', 'send_per_run', 'Number of mails sent per batch', '', 'Mailer', '100', 'text', 'integer', '', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `_core_updates`
--

CREATE TABLE IF NOT EXISTS `_core_updates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(50) NOT NULL,
  `installed` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `_instant_deposits`
--

CREATE TABLE IF NOT EXISTS `_instant_deposits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL DEFAULT '',
  `purchase_date` datetime NOT NULL,
  `expire_date` datetime NOT NULL,
  `last_processed_date` datetime NOT NULL,
  `deposit_method` int(11) NOT NULL DEFAULT '0',
  `deposit_amount` double(10,2) NOT NULL DEFAULT '0.00',
  `earned_amount` text,
  `paid_amount` text,
  `status` enum('Active','Inactive','Expired') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `_instant_deposits`
--

INSERT INTO `_instant_deposits` (`id`, `username`, `purchase_date`, `expire_date`, `last_processed_date`, `deposit_method`, `deposit_amount`, `earned_amount`, `paid_amount`, `status`) VALUES
(1, 'checksite1', '2015-12-10 08:03:57', '0000-00-00 00:00:00', '2016-01-04 00:00:00', 6, 1000.00, '764.153751399', '32.191615281', 'Active'),
(2, 'checksite1', '2015-12-10 15:10:00', '0000-00-00 00:00:00', '2016-01-04 00:00:00', 6, 1000.00, '754.4364178', '22.4706937196', 'Active'),
(3, 'checksite1', '2015-12-12 07:21:53', '0000-00-00 00:00:00', '2016-01-04 00:00:00', 6, 10.00, '7.01987217796', NULL, 'Active'),
(4, 'checksite1', '2015-12-14 08:49:14', '0000-00-00 00:00:00', '2016-01-04 00:00:00', 6, 20.00, '12.7558105596', NULL, 'Active'),
(5, 'advshare1', '2016-01-01 16:53:36', '0000-00-00 00:00:00', '2016-01-04 00:00:00', 3, 1000.00, '70.1794444444', NULL, 'Active'),
(6, 'advshare2', '2016-01-02 10:12:14', '0000-00-00 00:00:00', '2016-01-04 00:00:00', 3, 100.00, '47.8199768519', NULL, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `_instant_roi_logs`
--

CREATE TABLE IF NOT EXISTS `_instant_roi_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `roi` double(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `_instant_roi_logs`
--

INSERT INTO `_instant_roi_logs` (`id`, `date`, `roi`) VALUES
(1, '2015-12-12', 3.10),
(2, '2015-12-15', 3.05),
(3, '2016-01-01', 3.10),
(4, '2016-01-02', 3.10),
(5, '2016-01-03', 3.00),
(6, '2016-01-04', 4.00);

-- --------------------------------------------------------

--
-- Table structure for table `_instant_roi_override`
--

CREATE TABLE IF NOT EXISTS `_instant_roi_override` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `roi` double(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `_instant_roi_override`
--

INSERT INTO `_instant_roi_override` (`id`, `date`, `roi`) VALUES
(1, '2015-12-13', 3.10),
(2, '2015-12-14', 3.15),
(3, '2015-12-15', 3.05),
(4, '2015-12-16', 3.20),
(5, '2015-12-17', 3.00),
(6, '2015-12-18', 3.20),
(7, '2016-01-04', 4.00),
(8, '2016-01-05', 2.97),
(9, '2016-01-06', 3.05),
(10, '2016-01-07', 2.85),
(11, '2016-01-03', 3.00);

-- --------------------------------------------------------

--
-- Table structure for table `_instant_settings`
--

CREATE TABLE IF NOT EXISTS `_instant_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_cost` double(10,2) NOT NULL,
  `min_unit` double(10,2) NOT NULL,
  `max_unit` double(10,2) NOT NULL,
  `min_deposit` double(10,2) NOT NULL,
  `max_deposit` double(10,2) NOT NULL,
  `min_withdrawal` double(10,2) NOT NULL,
  `repurchase` double(10,2) NOT NULL,
  `roi_per_day` float(10,8) NOT NULL,
  `max_roi` float(10,2) NOT NULL,
  `referral_commission` text NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `description` text NOT NULL,
  `ads_packages` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `_instant_settings`
--

INSERT INTO `_instant_settings` (`id`, `unit_cost`, `min_unit`, `max_unit`, `min_deposit`, `max_deposit`, `min_withdrawal`, `repurchase`, `roi_per_day`, `max_roi`, `referral_commission`, `status`, `description`, `ads_packages`) VALUES
(1, 10.00, 1.00, 10000.00, 0.00, 0.00, 10.00, 30.00, 3.09999990, 130.00, '10', 'Active', '', '3');

-- --------------------------------------------------------

--
-- Table structure for table `_instant_transactions`
--

CREATE TABLE IF NOT EXISTS `_instant_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `username` varchar(50) NOT NULL DEFAULT '',
  `type` text NOT NULL,
  `description` text NOT NULL,
  `method` int(11) NOT NULL,
  `amount` double(10,2) NOT NULL DEFAULT '0.00',
  `fee` double(10,2) NOT NULL DEFAULT '0.00',
  `status` enum('Completed','Pending') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `_instant_transactions`
--

INSERT INTO `_instant_transactions` (`id`, `date`, `username`, `type`, `description`, `method`, `amount`, `fee`, `status`) VALUES
(1, '2015-12-10 08:03:57', 'checksite1', 'Deposit', 'Adshare purchase by checksite1', 6, 1000.00, 0.00, 'Completed'),
(2, '2015-12-10 15:10:00', 'checksite1', 'Deposit', 'Adshare purchase by checksite1', 6, 1000.00, 0.00, 'Completed'),
(3, '2015-12-10 16:30:30', 'checksite1', 'Transfer', 'Transfer of accruals from deposit #1', 6, 11.45, 0.00, 'Completed'),
(4, '2015-12-11 08:09:20', 'checksite1', 'Transfer', 'Transfer of accruals from deposit #2', 6, 22.47, 0.00, 'Completed'),
(5, '2015-12-11 08:09:30', 'checksite1', 'Transfer', 'Transfer of accruals from deposit #1', 6, 20.74, 0.00, 'Completed'),
(6, '2015-12-12 07:21:53', 'checksite1', 'Deposit', 'Adshare purchase by checksite1', 6, 10.00, 0.00, 'Completed'),
(7, '2015-12-14 08:49:14', 'checksite1', 'Deposit', 'Adshare purchase by checksite1', 6, 20.00, 0.00, 'Completed'),
(8, '2016-01-01 16:53:39', 'advshare', 'Commission', 'AdShare Commissions from level 1 downline - advshare1', 3, 100.00, 0.00, 'Completed'),
(9, '2016-01-01 16:53:39', 'advshare1', 'Deposit', 'Adshare purchase by advshare1', 3, 1000.00, 0.00, 'Completed'),
(10, '2016-01-02 10:12:18', 'advshare1', 'Commission', 'AdShare Commissions from level 1 downline - advshare2', 3, 100.00, 0.00, 'Completed'),
(11, '2016-01-02 10:12:18', 'advshare2', 'Deposit', 'Adshare purchase by advshare2', 3, 1000.00, 0.00, 'Completed');

-- --------------------------------------------------------

--
-- Table structure for table `_mailer_lists`
--

CREATE TABLE IF NOT EXISTS `_mailer_lists` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `owner` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text,
  `filter` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `_mailer_lists`
--

INSERT INTO `_mailer_lists` (`id`, `owner`, `name`, `description`, `filter`) VALUES
(1, 'account', 'all_users', 'All users', 'modules/mod_mailer/sys/filters/all_users.php'),
(2, 'account', 'all_active_users', 'All active users', 'modules/mod_mailer/sys/filters/all_active_users.php');

-- --------------------------------------------------------

--
-- Table structure for table `_mailer_log`
--

CREATE TABLE IF NOT EXISTS `_mailer_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `message_id` int(10) NOT NULL,
  `receiver` varchar(50) NOT NULL,
  `status` enum('Sent','Failed') NOT NULL DEFAULT 'Sent',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `_mailer_messages`
--

CREATE TABLE IF NOT EXISTS `_mailer_messages` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `owner` varchar(50) NOT NULL,
  `list_id` int(10) NOT NULL,
  `date` datetime DEFAULT NULL,
  `subject` text,
  `message` text,
  `status` enum('Draft','Pending','Sending','Paused','Completed') NOT NULL DEFAULT 'Pending',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `_payment_deposits`
--

CREATE TABLE IF NOT EXISTS `_payment_deposits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `type` enum('Auto','Manual') NOT NULL DEFAULT 'Manual',
  `mod` varchar(50) NOT NULL DEFAULT '',
  `username` varchar(50) NOT NULL DEFAULT '',
  `method` int(11) NOT NULL DEFAULT '0',
  `details` text,
  `reference` varchar(50) NOT NULL,
  `amount` float(10,2) NOT NULL DEFAULT '0.00',
  `fee` float(10,2) NOT NULL DEFAULT '0.00',
  `currency` varchar(50) NOT NULL DEFAULT 'USD',
  `description` text,
  `payer_comments` text,
  `payee_comments` text,
  `eid_on_generate` text,
  `eid_on_confirm` text,
  `eid_on_approve` text,
  `eid_on_cancel` text,
  `status` enum('Pending','Open','Canceled','Declined','Approved') NOT NULL DEFAULT 'Pending',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `_payment_execute`
--

CREATE TABLE IF NOT EXISTS `_payment_execute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `refno` varchar(20) NOT NULL,
  `date` datetime NOT NULL,
  `caller` varchar(80) NOT NULL DEFAULT '',
  `execute` blob NOT NULL,
  `custom` blob NOT NULL,
  `status` enum('Pending','Open','Completed') NOT NULL DEFAULT 'Pending',
  PRIMARY KEY (`id`,`refno`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `_payment_processors`
--

CREATE TABLE IF NOT EXISTS `_payment_processors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `payee` blob NOT NULL,
  `classdir` varchar(50) NOT NULL DEFAULT '',
  `classfile` varchar(50) NOT NULL DEFAULT '',
  `classname` varchar(50) NOT NULL DEFAULT '',
  `status` enum('Active','Pending') NOT NULL DEFAULT 'Active',
  `deposit` enum('Y','N') NOT NULL DEFAULT 'Y',
  `deposit_type` enum('Auto','Manual') NOT NULL DEFAULT 'Auto',
  `withdraw` enum('Y','N') NOT NULL DEFAULT 'Y',
  `subscribe` enum('Y','N') NOT NULL DEFAULT 'N',
  `autopay` enum('Y','N') NOT NULL DEFAULT 'N',
  `masspay` enum('Y','N') NOT NULL DEFAULT 'Y',
  `masspay_file` enum('Y','N') NOT NULL DEFAULT 'Y',
  `currencies` text NOT NULL,
  `dfee` float(6,2) NOT NULL,
  `wfee` float(6,2) NOT NULL,
  `sfee` float(6,2) NOT NULL,
  `var_1` blob NOT NULL,
  `var_2` blob NOT NULL,
  `var_3` blob NOT NULL,
  `var_4` blob NOT NULL,
  `var_5` blob NOT NULL,
  `var_6` blob NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `_payment_processors`
--

INSERT INTO `_payment_processors` (`id`, `name`, `payee`, `classdir`, `classfile`, `classname`, `status`, `deposit`, `deposit_type`, `withdraw`, `subscribe`, `autopay`, `masspay`, `masspay_file`, `currencies`, `dfee`, `wfee`, `sfee`, `var_1`, `var_2`, `var_3`, `var_4`, `var_5`, `var_6`) VALUES
(1, 'PayPal', '', 'paypal', 'paypal.class.php', 'PayPal', 'Active', 'Y', 'Auto', 'Y', 'Y', 'N', 'N', 'Y', ',USD,', 0.00, 0.00, 0.00, '', '', '', '', '', ''),
(2, 'Payza', '', 'payza', 'payza.class.php', 'Payza', 'Active', 'Y', 'Auto', 'Y', 'Y', 'Y', 'Y', 'Y', ',USD,', 0.00, 0.00, 0.00, '', '', '', '', '', ''),
(3, 'SolidTrust Pay', '', 'solidtrustpay', 'solidtrustpay.class.php', 'SolidTrustPay', 'Active', 'Y', 'Auto', 'Y', 'N', 'Y', 'Y', 'Y', ',USD,', 0.00, 0.00, 0.00, '', '', '', '', '', ''),
(4, 'Payeer', '', 'payeer', 'payeer.class.php', 'Payeer', 'Active', 'Y', 'Auto', 'Y', 'N', 'Y', 'Y', 'N', ',USD,', 0.00, 0.00, 0.00, '', '', '', '', '', ''),
(5, 'Okpay', '', 'okpay', 'okpay.class.php', 'Okpay', 'Active', 'Y', 'Auto', 'Y', 'N', 'N', 'Y', 'Y', ',USD,', 0.00, 0.00, 0.00, '', '', '', '', '', ''),
(6, 'Bitcoin', '', 'coinbase', 'coinbase.class.php', 'CoinbasePayments', 'Active', 'Y', 'Auto', 'Y', 'N', 'N', 'N', 'N', ',USD,', 3.00, 1.00, 0.00, 0x456ef0daf98551e7cb463e0afc726d02, 0xa7de29ff09153f70f81e137aa964ead1, '', '', '', ''),
(7, 'Perfect Money', '', 'perfectmoney', 'perfectmoney.class.php', 'PerfectMoney', 'Active', 'Y', 'Auto', 'Y', 'N', 'Y', 'Y', 'N', ',USD,', 0.00, 0.00, 0.00, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `_payment_subscriptions`
--

CREATE TABLE IF NOT EXISTS `_payment_subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `type` enum('Auto','Manual') NOT NULL DEFAULT 'Manual',
  `mod` varchar(50) NOT NULL DEFAULT '',
  `key` varchar(100) NOT NULL DEFAULT '',
  `username` varchar(50) NOT NULL DEFAULT '',
  `method` int(11) NOT NULL DEFAULT '0',
  `accountid` varchar(50) NOT NULL,
  `amount` float(10,2) NOT NULL DEFAULT '0.00',
  `fee` float(10,2) NOT NULL DEFAULT '0.00',
  `currency` varchar(50) NOT NULL DEFAULT 'USD',
  `description` text,
  `bill_duration` int(11) NOT NULL DEFAULT '0',
  `bill_period` enum('Days','Months','Years') NOT NULL,
  `lastupdate_date` date NOT NULL DEFAULT '0000-00-00',
  `lastbill_date` date NOT NULL DEFAULT '0000-00-00',
  `nextbill_date` date NOT NULL DEFAULT '0000-00-00',
  `expire_date` date NOT NULL DEFAULT '0000-00-00',
  `runtime` int(11) NOT NULL,
  `eid_on_generate` text,
  `eid_on_renew` text,
  `eid_on_suspend` text,
  `eid_on_cancel` text,
  `status` enum('Pending','Active','Cancelled') NOT NULL DEFAULT 'Pending',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `_payment_user_accounts`
--

CREATE TABLE IF NOT EXISTS `_payment_user_accounts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL DEFAULT '',
  `pro_id` int(11) NOT NULL DEFAULT '0',
  `account_id` text NOT NULL,
  `field_descriptions` blob NOT NULL,
  `field_values` blob NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `_promo_banners`
--

CREATE TABLE IF NOT EXISTS `_promo_banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  `width` int(5) DEFAULT NULL,
  `height` int(5) DEFAULT NULL,
  `status` enum('Active','Pending') DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `_promo_referrals`
--

CREATE TABLE IF NOT EXISTS `_promo_referrals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL DEFAULT '',
  `referral` varchar(50) NOT NULL DEFAULT '',
  `source` text NOT NULL,
  `refdate` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `_promo_referrals`
--

INSERT INTO `_promo_referrals` (`id`, `username`, `referral`, `source`, `refdate`) VALUES
(1, 'advshare', '33brushes', 'http://cashking.me/account/register', '2015-12-12'),
(2, 'advshare', 'advshare1', 'http://cashking.me/account/register', '2015-12-14'),
(3, 'advshare', 'putralon', 'http://cashking.me/account/register', '2015-12-24'),
(4, 'advshare1', 'advshare2', 'http://cashking.me/account/register', '2016-01-02');

-- --------------------------------------------------------

--
-- Table structure for table `_promo_referral_commissions`
--

CREATE TABLE IF NOT EXISTS `_promo_referral_commissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL DEFAULT '0000-00-00',
  `username` varchar(50) NOT NULL DEFAULT '',
  `downline` varchar(50) NOT NULL DEFAULT '',
  `source` text NOT NULL,
  `method` int(11) NOT NULL,
  `amount` double(10,2) NOT NULL DEFAULT '0.00',
  `currency` varchar(50) NOT NULL DEFAULT 'USD',
  `description` text NOT NULL,
  `status` enum('Confirmed','Pending') DEFAULT 'Confirmed',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `_promo_referral_commissions`
--

INSERT INTO `_promo_referral_commissions` (`id`, `date`, `username`, `downline`, `source`, `method`, `amount`, `currency`, `description`, `status`) VALUES
(1, '2016-01-01', 'advshare', 'advshare1', 'instant', 3, 100.00, 'USD', 'AdShare Commissions from level 1 downline - advshare1', 'Confirmed'),
(2, '2016-01-02', 'advshare1', 'advshare2', 'instant', 3, 100.00, 'USD', 'AdShare Commissions from level 1 downline - advshare2', 'Confirmed');

-- --------------------------------------------------------

--
-- Table structure for table `_promo_referral_contests`
--

CREATE TABLE IF NOT EXISTS `_promo_referral_contests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `start_date` date NOT NULL DEFAULT '0000-00-00',
  `end_date` date NOT NULL DEFAULT '0000-00-00',
  `positions` int(11) NOT NULL,
  `prize_description` text NOT NULL,
  `disallowed_usernames` text NOT NULL,
  `status` enum('Pending','Active','Completed') DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `_promo_referral_contests_winners`
--

CREATE TABLE IF NOT EXISTS `_promo_referral_contests_winners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL DEFAULT '',
  `cid` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `referrals` int(11) NOT NULL,
  `prize_status` enum('Pending','Paid') DEFAULT 'Pending',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `_promo_splashpages`
--

CREATE TABLE IF NOT EXISTS `_promo_splashpages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  `status` enum('Active','Pending') DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `_wallet_balance`
--

CREATE TABLE IF NOT EXISTS `_wallet_balance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `method` int(11) NOT NULL,
  `available_balance` double(10,4) NOT NULL DEFAULT '0.0000',
  `internal_balance` double(10,4) NOT NULL DEFAULT '0.0000',
  `pending_balance` double(10,4) NOT NULL DEFAULT '0.0000',
  `repurchase_balance` double(10,4) NOT NULL DEFAULT '0.0000',
  `currency` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `_wallet_balance`
--

INSERT INTO `_wallet_balance` (`id`, `username`, `method`, `available_balance`, `internal_balance`, `pending_balance`, `repurchase_balance`, `currency`) VALUES
(1, 'advshare', 1, 0.0000, 0.0000, 0.0000, 0.0000, 'USD'),
(2, 'advshare', 2, 0.0000, 0.0000, 0.0000, 0.0000, 'USD'),
(3, 'advshare', 3, 100.0000, 0.0000, 0.0000, 0.0000, 'USD'),
(4, 'advshare', 4, 0.0000, 0.0000, 0.0000, 0.0000, 'USD'),
(5, 'advshare', 5, 0.0000, 0.0000, 0.0000, 0.0000, 'USD'),
(6, 'advshare', 6, 0.0000, 0.0000, 0.0000, 0.0000, 'USD'),
(7, 'advshare', 7, 0.0000, 0.0000, 0.0000, 0.0000, 'USD'),
(8, 'advshare1', 1, 0.0000, 0.0000, 0.0000, 0.0000, 'USD'),
(9, 'advshare1', 2, 0.0000, 0.0000, 0.0000, 0.0000, 'USD'),
(10, 'advshare1', 3, 100.0000, 0.0000, 0.0000, 0.0000, 'USD'),
(11, 'advshare1', 4, 0.0000, 0.0000, 0.0000, 0.0000, 'USD'),
(12, 'advshare1', 5, 0.0000, 0.0000, 0.0000, 0.0000, 'USD'),
(13, 'advshare1', 6, 0.0000, 0.0000, 0.0000, 0.0000, 'USD'),
(14, 'advshare1', 7, 0.0000, 0.0000, 0.0000, 0.0000, 'USD'),
(15, 'advshare2', 3, 0.0000, 0.0000, 0.0000, 0.0000, 'USD'),
(16, 'advshare2', 1, 0.0000, 0.0000, 0.0000, 0.0000, 'USD'),
(17, 'advshare2', 2, 0.0000, 0.0000, 0.0000, 0.0000, 'USD'),
(18, 'advshare2', 4, 0.0000, 0.0000, 0.0000, 0.0000, 'USD'),
(19, 'advshare2', 5, 0.0000, 0.0000, 0.0000, 0.0000, 'USD'),
(20, 'advshare2', 6, 0.0000, 0.0000, 0.0000, 0.0000, 'USD'),
(21, 'advshare2', 7, 0.0000, 0.0000, 0.0000, 0.0000, 'USD');

-- --------------------------------------------------------

--
-- Table structure for table `_wallet_balance_history`
--

CREATE TABLE IF NOT EXISTS `_wallet_balance_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `username` varchar(50) NOT NULL DEFAULT '',
  `transaction` text NOT NULL,
  `amount` double(10,4) NOT NULL DEFAULT '0.0000',
  `fee` float(10,4) NOT NULL DEFAULT '0.0000',
  `pre_balance` double(10,4) NOT NULL DEFAULT '0.0000',
  `post_balance` double(10,4) NOT NULL DEFAULT '0.0000',
  `method` int(11) NOT NULL,
  `currency` varchar(15) NOT NULL,
  `type` enum('Credit','Debit') NOT NULL DEFAULT 'Credit',
  `status` enum('Pending','Completed') NOT NULL DEFAULT 'Completed',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `_wallet_balance_history`
--

INSERT INTO `_wallet_balance_history` (`id`, `date`, `username`, `transaction`, `amount`, `fee`, `pre_balance`, `post_balance`, `method`, `currency`, `type`, `status`) VALUES
(1, '2016-01-01 16:52:10', 'advshare1', 'Credit from administrator', 1000.0000, 0.0000, 0.0000, 1000.0000, 3, 'USD', 'Credit', 'Completed'),
(2, '2016-01-01 16:53:36', 'advshare1', 'Adshare purchase by advshare1', 1000.0000, 0.0000, 1000.0000, 0.0000, 3, 'USD', 'Debit', 'Completed'),
(3, '2016-01-01 16:53:36', 'advshare', 'AdShare Commissions from level 1 downline - advshare1', 100.0000, 0.0000, 0.0000, 100.0000, 3, 'USD', 'Credit', 'Completed'),
(4, '2016-01-02 10:07:33', 'advshare2', 'Credit from administrator', 1000.0000, 0.0000, 0.0000, 1000.0000, 3, 'USD', 'Credit', 'Completed'),
(5, '2016-01-02 10:12:14', 'advshare2', 'Adshare purchase by advshare2', 1000.0000, 0.0000, 1000.0000, 0.0000, 3, 'USD', 'Debit', 'Completed'),
(6, '2016-01-02 10:12:14', 'advshare1', 'AdShare Commissions from level 1 downline - advshare2', 100.0000, 0.0000, 0.0000, 100.0000, 3, 'USD', 'Credit', 'Completed');

-- --------------------------------------------------------

--
-- Table structure for table `_wallet_transactions`
--

CREATE TABLE IF NOT EXISTS `_wallet_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `username` varchar(50) NOT NULL DEFAULT '',
  `type` text NOT NULL,
  `description` text NOT NULL,
  `method` int(11) NOT NULL,
  `amount` double(10,4) NOT NULL DEFAULT '0.0000',
  `fee` double(10,4) NOT NULL DEFAULT '0.0000',
  `status` enum('Completed','Pending') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `_wallet_transactions`
--

INSERT INTO `_wallet_transactions` (`id`, `date`, `username`, `type`, `description`, `method`, `amount`, `fee`, `status`) VALUES
(1, '2016-01-01 16:52:10', 'advshare1', 'Deposit', 'Credit from administrator', 3, 1000.0000, 0.0000, 'Completed'),
(2, '2016-01-02 10:07:33', 'advshare2', 'Deposit', 'Credit from administrator', 3, 1000.0000, 0.0000, 'Completed');

-- --------------------------------------------------------

--
-- Table structure for table `_wallet_withdrawals`
--

CREATE TABLE IF NOT EXISTS `_wallet_withdrawals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL DEFAULT '',
  `amount` float(10,4) NOT NULL DEFAULT '0.0000',
  `fee` float(10,4) NOT NULL DEFAULT '0.0000',
  `currency` varchar(50) NOT NULL DEFAULT 'USD',
  `proid` int(11) NOT NULL,
  `proacc` int(11) NOT NULL,
  `details` text,
  `payer_comments` text,
  `payee_comments` text,
  `adddate` date NOT NULL DEFAULT '0000-00-00',
  `paydate` date NOT NULL DEFAULT '0000-00-00',
  `description` text NOT NULL,
  `status` enum('Pending','Paid','Refunded','Deleted') NOT NULL DEFAULT 'Pending',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
