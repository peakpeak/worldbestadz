<?php 

class Main
{
    public $systemEnv;
    public $debuginfo = array();
    public $execStartTime;
    public $execStartMemory;

    public function Main($environment = NULL)
    {
        $mtime = microtime();
        $mtime = explode(" ", $mtime);
        $mtime = $mtime[1] + $mtime[0];
        $this->execStartTime = $starttime = $mtime;
        $this->execStartMemory = memory_get_usage();
        $this->systemEnv = $environment;
        $this->validateBaseLicense(LICENSE_KEY_2);
        $_SESSION["system"] = array();
    }

    public function getPageInfo($module, $page = NULL, $item = NULL)
    {
        global $settings;
        global $db;
        $row = $db->get_row("name", $module, "_core_modules");
        if( $row == NULL ) 
        {
            return false;
        }

        switch( $this->systemEnv ) 
        {
            case "members":
                $environment = $row["members_directory"];
                break;
            case "admin":
                $environment = $row["admin_directory"];
                break;
            default:
                $environment = $row["root_directory"];
                break;
        }
        if( !is_dir(ROOT . DS . "modules" . DS . $row["root_directory"] . DS) ) 
        {
            return false;
        }

        $page = ($page == NULL ? "index" : $page);
        if( !is_file(ROOT . DS . "modules" . DS . $environment . DS . "xml" . DS . $page . ".xml") ) 
        {
            return false;
        }

        $xml = file_get_contents(ROOT . DS . "modules" . DS . $environment . DS . "xml" . DS . $page . ".xml");
        if( $item != NULL ) 
        {
            return $this->getElement($xml, $item);
        }

        return $xml;
    }

    public function getModuleInfo($module, $item = NULL)
    {
        global $db;
        $row = $db->get_row("name", $module, "_core_modules");
        if( $row == NULL ) 
        {
            return false;
        }

        if( !file_exists(ROOT . DS . "modules" . DS . $row["root_directory"] . DS . "info.xml") ) 
        {
            return false;
        }

        $xml = file_get_contents(ROOT . DS . "modules" . DS . $row["root_directory"] . DS . "info.xml");
        if( $item != NULL ) 
        {
            return $this->getElement($xml, $item);
        }

        return $xml;
    }

    public function getContent($xml, $tag)
    {
        $content = $xml;
        $tree = explode(":", $tag);
        foreach( $tree as $get ) 
        {
            $content = $this->extractElement($content, $get);
        }
        return $content;
    }

    public function getElement($xml, $element)
    {
        $content = $xml;
        $tree = explode(":", $element);
        foreach( $tree as $tag ) 
        {
            $content = $this->extractElement($content, $tag);
        }
        return $content;
    }

    public function extractZip($source, $destination)
    {
        $zip = new ZipArchive();
        $res = $zip->open($source);
        if( $res ) 
        {
            $zip->extractTo($destination);
            $zip->close();
            return true;
        }

        return false;
    }

    public function extractElement($xml, $element)
    {
        $elementlen = strlen($element) + 2;
        if( ($startat = strpos($xml, "" . "<" . $element . ">")) === false ) 
        {
            return "";
        }

        if( ($endat = strpos($xml, "" . "</" . $element . ">")) === false ) 
        {
            return "";
        }

        $value = trim(substr($xml, $startat + $elementlen, $endat - ($startat + $elementlen)));
        return $value;
    }

    public function arrayToXml($data, $rootNodeName = "data", $xml = null)
    {
        if( ini_get("zend.ze1_compatibility_mode") == 1 ) 
        {
            ini_set("zend.ze1_compatibility_mode", 0);
        }

        if( $xml == null ) 
        {
            $xml = simplexml_load_string("" . "<?xml version='1.0' encoding='utf-8'?><" . $rootNodeName . " />");
        }

        foreach( $data as $key => $value ) 
        {
            if( is_numeric($key) ) 
            {
                $key = "unknownNode_" . (bool) $key;
            }

            $key = preg_replace("/[^a-z]/i", "", $key);
            if( is_array($value) ) 
            {
                $node = $xml->addChild($key);
                self::arraytoxmll($value, $rootNodeName, $node);
            }
            else
            {
                $value = htmlentities($value);
                $xml->addChild($key, $value);
            }

        }
        return $xml->asXML();
    }

    public function _struct_to_array($values, &$i)
    {
        $child = array();
        if( isset($values[$i]["value"]) ) 
        {
            array_push($child, $values[$i]["value"]);
        }

        while( $i++ < count($values) ) 
        {
            switch( $values[$i]["type"] ) 
            {
                case "cdata":
                    array_push($child, $values[$i]["value"]);
                    break;
                case "complete":
                    $name = $values[$i]["tag"];
                    if( !empty($name) ) 
                    {
                        $child[$name] = ($values[$i]["value"] ? $values[$i]["value"] : "");
                        if( isset($values[$i]["attributes"]) ) 
                        {
                            $child[$name] = $values[$i]["attributes"];
                        }

                    }

                    break;
                case "open":
                    $name = $values[$i]["tag"];
                    $size = (isset($child[$name]) ? sizeof($child[$name]) : 0);
                    $child[$name][$size] = $this->_struct_to_array($values, $i);
                    break;
                case "close":
                    return $child;
            }
        }
        return $child;
    }

    public function createArray($xml)
    {
        $values = array();
        $index = array();
        $array = array();
        $parser = xml_parser_create();
        xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
        xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
        xml_parse_into_struct($parser, $xml, $values, $index);
        xml_parser_free($parser);
        $i = 0;
        $name = $values[$i]["tag"];
        $array[$name] = (isset($values[$i]["attributes"]) ? $values[$i]["attributes"] : "");
        $array[$name] = $this->_struct_to_array($values, $i);
        return $array;
    }

    public function autolink($string, $newtab = true)
    {
        $content_array = explode(" ", $string);
        $output = "";
        foreach( $content_array as $content ) 
        {
            if( substr($content, 0, 7) == "http://" || substr($content, 0, 8) == "https://" || substr($content, 0, 4) == "www." ) 
            {
                $link = "<a href=\"" . $content . "\"  ";
                if( $newtab ) 
                {
                    $link .= " target=\"_blank\" ";
                }

                $link .= ">" . $content . "</a>";
                $content = $link;
            }

            $output .= " " . $content;
        }
        $output = trim($output);
        return $output;
    }

    public function pathToUrl($path)
    {
        $url = str_replace(ROOT, SITEURL, $path);
        $url = str_replace(DS, FS, $url);
        return $url;
    }

    public function scanDirectory($path)
    {
        $temp = array();
        $handle = opendir($path);
        if( !handle ) 
        {
            return $temp;
        }

        while( $h = readdir($handle) ) 
        {
            if( is_dir($path . DS . $h) && $h != "." && $h != ".." ) 
            {
                $temp[$h] = $h;
            }

        }
        return $temp;
    }

    public function randomString($len)
    {
        $string = NULL;
        for( $i = 0; $i < $len; $i++ ) 
        {
            $char = chr(rand(48, 122));
            while( !ereg("[a-zA-Z0-9]", $char) ) 
            {
                if( $char == $lchar ) 
                {
                    continue;
                }

                $char = chr(rand(48, 90));
            }
            $string .= $char;
            $lchar = $char;
        }
        return $string;
    }

    public function replaceTags($data_array, $text)
    {
        $st_vari = array();
        $st_rep = array();
        foreach( $data_array as $var => $value ) 
        {
            $st_vari[] = $var;
            $st_rep[] = $value;
        }
        $return_text = str_replace($st_vari, $st_rep, $text);
        return $return_text;
    }

    public function findDate($time_difference, $time_type, $future_or_past, $date = NULL)
    {
        $date = ($date == NULL ? date("Y-m-d") : $date);
        if( $future_or_past == "future" ) 
        {
            $timestamp = strtotime("" . $date . " +" . $time_difference . " " . $time_type);
            $fdate = strftime("%Y-%m-%d", $timestamp);
            return $fdate;
        }

        if( $future_or_past == "past" ) 
        {
            $timestamp = strtotime("" . $date . " " . $time_difference . " " . $time_type . " ago");
            $fdate = strftime("%Y-%m-%d", $timestamp);
            return $fdate;
        }

    }

    public function dateDiff($period, $start, $end = "NOW")
    {
        $sdate = strtotime($start);
        $edate = strtotime($end);
        if( $sdate === false || $sdate < 0 || $edate === false || $edate < 0 || $edate < $sdate ) 
        {
            return 0;
        }

        $time = $edate - $sdate;
        switch( $period ) 
        {
            case "seconds":
                $diff = $time;
                break;
            case "minutes":
                $diff = $time / 60;
                break;
            case "hours":
                $diff = $time / 3600;
                break;
            case "days":
            default:
                $diff = $time / 86400;
                break;
        }
        return intval($diff);
    }

    public function stringDateDiff($start, $end = "NOW")
    {
        $sdate = strtotime($start);
        $edate = strtotime($end);
        if( $sdate === false || $sdate < 0 || $edate === false || $edate < 0 || $edate < $sdate ) 
        {
            return 0;
        }

        $time = $edate - $sdate;
        if( 0 <= $time && $time <= 59 ) 
        {
            $timeshift = $time . " seconds ";
        }
        else
        {
            if( 60 <= $time && $time <= 3599 ) 
            {
                $pmin = ($edate - $sdate) / 60;
                $premin = explode(".", $pmin);
                $presec = $pmin - $premin[0];
                $sec = $presec * 60;
                $timeshift = $premin[0] . " min " . round($sec, 0) . " sec ";
            }
            else
            {
                if( 3600 <= $time && $time <= 86399 ) 
                {
                    $phour = ($edate - $sdate) / 3600;
                    $prehour = explode(".", $phour);
                    $premin = $phour - $prehour[0];
                    $min = explode(".", $premin * 60);
                    $presec = "0." . $min[1];
                    $sec = $presec * 60;
                    $timeshift = $prehour[0] . " hrs " . $min[0] . " min " . round($sec, 0) . " sec ";
                }
                else
                {
                    if( 86400 <= $time ) 
                    {
                        $pday = ($edate - $sdate) / 86400;
                        $preday = explode(".", $pday);
                        $phour = $pday - $preday[0];
                        $prehour = explode(".", $phour * 24);
                        $premin = $phour * 24 - $prehour[0];
                        $min = explode(".", $premin * 60);
                        $presec = "0." . $min[1];
                        $sec = $presec * 60;
                        $timeshift = $preday[0] . " days " . $prehour[0] . " hrs " . $min[0] . " min " . round($sec, 0) . " sec ";
                    }

                }

            }

        }

        return $timeshift;
    }

    public function isWeekend($date)
    {
        return 6 <= date("N", strtotime($date));
    }

    public function getVar($item)
    {
        global $db;
        $var = array();
        $this->clean();
        foreach( $_GET as $key => $value ) 
        {
            $var[$key] = $value;
        }
        foreach( $_POST as $key => $value ) 
        {
            $var[$key] = $value;
        }
        if( $item != "" ) 
        {
            return $var[$item];
        }

        return $var;
    }

    public function getRealIP()
    {
        if( $_SERVER["HTTP_X_FORWARDED_FOR"] != "" ) 
        {
            $client_ip = (!empty($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : (!empty($_ENV["REMOTE_ADDR"]) ? $_ENV["REMOTE_ADDR"] : "unknown"));
            $entries = split("[, ]", $_SERVER["HTTP_X_FORWARDED_FOR"]);
            reset($entries);
            while( list(, $entry) = each($entries) ) 
            {
                $entry = trim($entry);
                if( preg_match("/^([0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+)/", $entry, $ip_list) ) 
                {
                    $private_ip = array( "/^0\\./", "/^127\\.0\\.0\\.1/", "/^192\\.168\\..*/", "/^172\\.((1[6-9])|(2[0-9])|(3[0-1]))\\..*/", "/^10\\..*/" );
                    $found_ip = preg_replace($private_ip, $client_ip, $ip_list[1]);
                    if( $client_ip != $found_ip ) 
                    {
                        $client_ip = $found_ip;
                        break;
                    }

                }

            }
        }
        else
        {
            $client_ip = (!empty($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : (!empty($_ENV["REMOTE_ADDR"]) ? $_ENV["REMOTE_ADDR"] : "unknown"));
        }

        return $client_ip;
    }

    public function ipToLocation($ip = null)
    {
        $url = "http://freegeoip.net/json/" . $ip;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        $result = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if( $code != 200 ) 
        {
            error_log("CURLINFO_HTTP_CODE: " . $code);
        }

        return json_decode($result, true);
    }

    public function getBaseUrl()
    {
        return SITEURL;
    }

    public function getBasePath()
    {
        return SITEPATH;
    }

    public function curPageURL($pagestrings = array(  ))
    {
        $pageURL = "http";
        if( $_SERVER["HTTPS"] == "on" ) 
        {
            $pageURL .= "s";
        }

        $pageURL .= "://";
        if( $_SERVER["SERVER_PORT"] != "80" ) 
        {
            $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
        }
        else
        {
            $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
        }

        if( strstr($pageURL, "?") != false ) 
        {
            $targetpageparts = explode("?", $pageURL);
            if( $targetpageparts[1] != "" ) 
            {
                $targetpageparts2 = explode("&", $targetpageparts[1]);
                foreach( $pagestrings as $string ) 
                {
                    foreach( $targetpageparts2 as $parts ) 
                    {
                        if( strstr("?" . $parts, $string) != false ) 
                        {
                            $pageURL = str_replace("?" . $parts, "", $pageURL);
                        }

                        if( strstr("&" . $parts, $string) != false ) 
                        {
                            $pageURL = str_replace("&" . $parts, "", $pageURL);
                        }

                    }
                }
            }

        }

        return $pageURL;
    }

    public function clean($vals = NULL)
    {
        $myFilter = new InputFilter();
        if( $vals == NULL ) 
        {
            $_GET = $myFilter->process($_GET);
            $_POST = $myFilter->process($_POST);
            $_SESSION = $myFilter->process($_SESSION);
            $_COOKIE = $myFilter->process($_COOKIE);
        }
        else
        {
            $vals = $myFilter->process($vals);
            return $vals;
        }

    }

    public function secure($type, $vals, $private, $public)
    {
        unset($temp);
        switch( $type ) 
        {
            case "en":
                $Secrypt = new Secrypt();
                $temp = array();
                foreach( $vals as $v ) 
                {
                    $EncryptedData = $Secrypt->Encrypt($v, $private, $public);
                    $temp[] = $EncryptedData;
                }
                return $temp;
            case "dc":
                $Secrypt = new Secrypt();
                $temp = array();
                foreach( $vals as $v ) 
                {
                    $DecryptedData = $Secrypt->Decrypt($v, $private, $public);
                    $temp[] = $DecryptedData;
                }
                return $temp;
            default:
                break;
        }
    }

    public function getPaginationString($page = 1, $totalitems, $limit = 15, $adjacents = 1, $targetpage = "/", $pagestring = "page")
    {
        if( !$adjacents ) 
        {
            $adjacents = 1;
        }

        if( !$limit ) 
        {
            $limit = 15;
        }

        if( !$page ) 
        {
            $page = 1;
        }

        if( !$targetpage ) 
        {
            $targetpage = "/";
        }

        if( strstr($targetpage, "?") != false ) 
        {
            $targetpageparts = explode("?", $targetpage);
            if( $targetpageparts[1] != "" ) 
            {
                $pagestring = "&" . $pagestring . "=";
                $targetpageparts2 = explode("&", $targetpageparts[1]);
                foreach( $targetpageparts2 as $parts ) 
                {
                    if( strstr("?" . $parts, $pagestring) ) 
                    {
                        $targetpage = str_replace("?" . $parts, "", $targetpage);
                    }

                    if( strstr("&" . $parts, $pagestring) ) 
                    {
                        $targetpage = str_replace("&" . $parts, "", $targetpage);
                    }

                }
            }
            else
            {
                $pagestring = $pagestring . "=";
            }

        }
        else
        {
            $pagestring = "?" . $pagestring . "=";
        }

        $prev = $page - 1;
        $next = $page + 1;
        $lastpage = ceil($totalitems / $limit);
        $lpm1 = $lastpage - 1;
        $pagination = "";
        if( 1 < $lastpage ) 
        {
            $pagination .= "<div class=\"pagination\"";
            if( $margin || $padding ) 
            {
                $pagination .= " style=\"";
                if( $margin ) 
                {
                    $pagination .= "" . "margin: " . $margin . ";";
                }

                if( $padding ) 
                {
                    $pagination .= "" . "padding: " . $padding . ";";
                }

                $pagination .= "\"";
            }

            $pagination .= ">";
            if( 1 < $page ) 
            {
                $pagination .= "" . "<a href=\"" . $targetpage . $pagestring . $prev . "\"><< prev</a>";
            }
            else
            {
                $pagination .= "<span class=\"disabled\"><< prev</span>";
            }

            if( $lastpage < 7 + $adjacents * 2 ) 
            {
                for( $counter = 1; $counter <= $lastpage; $counter++ ) 
                {
                    if( $counter == $page ) 
                    {
                        $pagination .= "" . "<span class=\"current\">" . $counter . "</span>";
                    }
                    else
                    {
                        $pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "" . "\">" . $counter . "</a>";
                    }

                }
            }
            else
            {
                if( 7 + $adjacents * 2 <= $lastpage ) 
                {
                    if( $page < 1 + $adjacents * 3 ) 
                    {
                        for( $counter = 1; $counter < 4 + $adjacents * 2; $counter++ ) 
                        {
                            if( $counter == $page ) 
                            {
                                $pagination .= "" . "<span class=\"current\">" . $counter . "</span>";
                            }
                            else
                            {
                                $pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "" . "\">" . $counter . "</a>";
                            }

                        }
                        $pagination .= "...";
                        $pagination .= "<a href=\"" . $targetpage . $pagestring . $lpm1 . "" . "\">" . $lpm1 . "</a>";
                        $pagination .= "<a href=\"" . $targetpage . $pagestring . $lastpage . "" . "\">" . $lastpage . "</a>";
                    }
                    else
                    {
                        if( $page < $lastpage - $adjacents * 2 && $adjacents * 2 < $page ) 
                        {
                            $pagination .= "<a href=\"" . $targetpage . $pagestring . "1\">1</a>";
                            $pagination .= "<a href=\"" . $targetpage . $pagestring . "2\">2</a>";
                            $pagination .= "...";
                            for( $counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++ ) 
                            {
                                if( $counter == $page ) 
                                {
                                    $pagination .= "" . "<span class=\"current\">" . $counter . "</span>";
                                }
                                else
                                {
                                    $pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "" . "\">" . $counter . "</a>";
                                }

                            }
                            $pagination .= "...";
                            $pagination .= "<a href=\"" . $targetpage . $pagestring . $lpm1 . "" . "\">" . $lpm1 . "</a>";
                            $pagination .= "<a href=\"" . $targetpage . $pagestring . $lastpage . "" . "\">" . $lastpage . "</a>";
                        }
                        else
                        {
                            $pagination .= "<a href=\"" . $targetpage . $pagestring . "1\">1</a>";
                            $pagination .= "<a href=\"" . $targetpage . $pagestring . "2\">2</a>";
                            $pagination .= "...";
                            for( $counter = $lastpage - (1 + $adjacents * 3); $counter <= $lastpage; $counter++ ) 
                            {
                                if( $counter == $page ) 
                                {
                                    $pagination .= "" . "<span class=\"current\">" . $counter . "</span>";
                                }
                                else
                                {
                                    $pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "" . "\">" . $counter . "</a>";
                                }

                            }
                        }

                    }

                }

            }

            if( $page < $counter - 1 ) 
            {
                $pagination .= "<a href=\"" . $targetpage . $pagestring . $next . "\">next >></a>";
            }
            else
            {
                $pagination .= "<span class=\"disabled\">next >></span>";
            }

            $pagination .= "</div>\n";
        }

        return $pagination;
    }

    public function pluginsPath()
    {
        global $settings;
        return ROOT . FS . $settings["system"]["plugins_directory"];
    }

    public function pluginsUrl()
    {
        global $settings;
        return SITEURL . FS . $settings["system"]["plugins_directory"];
    }

    public function importClass($module)
    {
        global $db;
        $row = $db->get_row("name", $module, "_core_modules");
        if( $row == NULL ) 
        {
            return false;
        }

        if( $row["controller_classname"] == "" || $row["controller_filename"] == "" || $row["controller_directory"] == "" ) 
        {
            return false;
        }

        if( !include_once(ROOT . DS . "modules" . DS . $row["controller_directory"] . DS . $row["controller_filename"]) ) 
        {
            return false;
        }

        $object = new $row["controller_classname"]();
        if( !is_object($object) ) 
        {
            return false;
        }

        $dependents = $object->dependents;
        $flagged = false;
        $missing_dependents = array();
        if( is_array($dependents) ) 
        {
            foreach( $dependents as $dependent ) 
            {
                if( !self::moduleinstalled($dependent) ) 
                {
                    $missing_dependents[] = $dependent;
                    $flagged = true;
                }

            }
        }

        if( $flagged ) 
        {
            $message = "The following modules are needed: ";
            foreach( $missing_dependents as $dependents ) 
            {
                $message .= $dependents . ", ";
            }
            return false;
        }

        return $object;
    }

    public function index()
    {
        ($this->systemEnv == "members" || $this->systemEnv == "admin" ? header("location:../index.php") : header("location:index.php"));
    }

    public function invalidRequest()
    {
        global $settings;
        switch( $this->systemEnv ) 
        {
            case "members":
                $location = SITEURL . FS . "members" . FS . "content/404";
                break;
            case "admin":
                $location = SITEURL . FS . $settings["system"]["admin_directory"] . FS . "content/404";
                break;
            default:
                $location = SITEURL . FS . "content/404";
                break;
        }
        header("location:" . $location);
        exit( "Problem Redirecting. Please contact technical support" );
    }

    public function redirect($url)
    {
        header("location:" . $url);
        exit();
    }

    public function loadModule($module, $page = NULL)
    {
        global $settings;
        global $db;
        if( $module == "admin" ) 
        {
            if( $page != NULL ) 
            {
                if( !include_once(ROOT . DS . $settings["system"]["admin_directory"] . DS . "files" . DS . $page . ".php") ) 
                {
                    return false;
                }

            }
            else
            {
                if( !include_once(ROOT . DS . $settings["system"]["admin_directory"] . DS . "files" . DS . "index.php") ) 
                {
                    return false;
                }

            }

            return true;
        }

        $row = $db->get_row("name", $module, "_core_modules");
        if( $row == NULL ) 
        {
            return false;
        }

        if( $row["status"] != "Enabled" ) 
        {
            return false;
        }

        switch( $this->systemEnv ) 
        {
            case "members":
                $environment = $row["members_directory"];
                break;
            case "admin":
                $environment = $row["admin_directory"];
                break;
            default:
                $environment = $row["root_directory"];
                break;
        }
        if( !is_dir(ROOT . DS . "modules" . DS . $row["root_directory"] . DS) ) 
        {
            return false;
        }

        if( $page != NULL ) 
        {
            if( !include_once(ROOT . DS . "modules" . DS . $environment . DS . $page . ".php") ) 
            {
                return false;
            }

        }
        else
        {
            if( !include_once(ROOT . DS . "modules" . DS . $environment . DS . "index.php") ) 
            {
                return false;
            }

        }

        return true;
    }

    public function dependentsInstalled($module)
    {
    }

    public function initialize($module, $page = NULL)
    {
        global $settings;
        global $db;
        if( $module == "admin" ) 
        {
            if( $page != NULL ) 
            {
                if( !include_once(ROOT . DS . $settings["system"]["admin_directory"] . DS . "files" . DS . $page . ".php") ) 
                {
                    return false;
                }

            }
            else
            {
                if( !include_once(ROOT . DS . $settings["system"]["admin_directory"] . DS . "files" . DS . "index.php") ) 
                {
                    return false;
                }

            }

            return true;
        }

        $row = $db->get_row("name", $module, "_core_modules");
        if( $row == NULL ) 
        {
            return false;
        }

        if( $row["status"] != "Enabled" ) 
        {
            return false;
        }

        switch( $this->systemEnv ) 
        {
            case "members":
                $environment = $row["members_directory"];
                break;
            case "admin":
                $environment = $row["admin_directory"];
                break;
            default:
                $environment = $row["root_directory"];
                break;
        }
        if( !is_dir(ROOT . DS . "modules" . DS . $row["root_directory"] . DS) ) 
        {
            return false;
        }

        if( $page != NULL ) 
        {
            if( !include_once(ROOT . DS . "modules" . DS . $environment . DS . $page . ".php") ) 
            {
                return false;
            }

        }
        else
        {
            if( !include_once(ROOT . DS . "modules" . DS . $environment . DS . "index.php") ) 
            {
                return false;
            }

        }

        return true;
    }

    public function getDb()
    {
    }

    public function ajaxCall($module, $page = NULL)
    {
        global $settings;
        global $db;
        if( $module == "admin" ) 
        {
            if( $page != NULL ) 
            {
                if( !include_once(ROOT . DS . $settings["system"]["admin_directory"] . DS . "files" . DS . $page . ".php") ) 
                {
                    return false;
                }

            }
            else
            {
                if( !include_once(ROOT . DS . $settings["system"]["admin_directory"] . DS . "files" . DS . "index.php") ) 
                {
                    return false;
                }

            }

            return true;
        }

        $row = $db->get_row("name", $module, "_core_modules");
        if( $row == NULL ) 
        {
            return false;
        }

        if( $row["status"] != "Enabled" ) 
        {
            return false;
        }

        $environment = $row["root_directory"] . DS . "ajax";
        if( !is_dir(ROOT . DS . "modules" . DS . $row["root_directory"] . DS) ) 
        {
            return false;
        }

        if( $page != NULL ) 
        {
            if( !include_once(ROOT . DS . "modules" . DS . $environment . DS . $page . ".php") ) 
            {
                return false;
            }

        }
        else
        {
            if( !include_once(ROOT . DS . "modules" . DS . $environment . DS . "index.php") ) 
            {
                return false;
            }

        }

        return true;
    }

    public function ajaxOutput($success, $result = array(  ))
    {
        $output = array( "success" => $success );
        $output = array_merge($output, $result);
        $output = json_encode($output);
        exit( $output );
    }

    public function authenticateApiKey($key)
    {
        global $settings;
        if( !$settings["system"]["api_enabled"] ) 
        {
            self::apioutput(false, array( "message" => "Api disabled" ));
        }
        else
        {
            if( $settings["system"]["api_key"] == null ) 
            {
                self::apioutput(false, array( "message" => "Api disabled" ));
            }
            else
            {
                if( $settings["system"]["api_key"] != $key ) 
                {
                    self::apioutput(false, array( "message" => "Invalid Api Key" ));
                }
                else
                {
                    return true;
                }

            }

        }

    }

    public function apiOutput($success, $result = array(  ))
    {
        $output = array( "success" => $success );
        $output = array_merge($output, $result);
        $output = json_encode($output);
        exit( $output );
    }

    public function str_replace_once($find, $replace, $string)
    {
        $pos = strpos($string, $find);
        if( $pos !== false ) 
        {
            $newstring = substr_replace($string, $replace, $pos, strlen($find));
        }
        else
        {
            $newstring = $string;
        }

        return $newstring;
    }

    public function route()
    {
        global $settings;
        $alt = 0;
        $key_name = "";
        $get = array();
        $req_uri = $_SERVER["REQUEST_URI"];
        $path_info = pathinfo($_SERVER["PHP_SELF"]);
        $dir = $path_info["dirname"];
        if( $dir != "/" ) 
        {
            $uri = $this->str_replace_once($dir, "", $req_uri);
        }
        else
        {
            $uri = $req_uri;
        }

        $uri = trim($uri, "/");
        if( strstr($uri, "?") ) 
        {
            $parts = explode("?", $uri);
            $l_part = $parts[0];
            $r_part = $parts[1];
        }
        else
        {
            $l_part = $uri;
            $r_part = "";
        }

        $l_part = trim($l_part, "/");
        $params = explode("/", $l_part);
        if( $l_part != null ) 
        {
            foreach( $params as $i => $value ) 
            {
                if( $i == 0 ) 
                {
                    if( $value == "pages" ) 
                    {
                        $_GET["mod"] = "content";
                        $_GET["go"] = "index";
                        if( $params[1] != null ) 
                        {
                            $_GET["page"] = $params[1];
                        }

                        break;
                    }

                    $_GET["mod"] = ($value == "index.php" ? "" : $value);
                    continue;
                }

                if( $i == 1 ) 
                {
                    $_GET["go"] = ($value == "index.php" ? "" : $value);
                    continue;
                }

                if( $i % 2 == 0 ) 
                {
                    $_GET[$value] = $params[$i + 1];
                }

            }
        }

        $r_part_parts = explode("&", $r_part);
        if( $r_part != null ) 
        {
            foreach( $r_part_parts as $parts ) 
            {
                $params = explode("=", $parts);
                $_GET[$params[0]] = $params[1];
            }
        }

    }

    public function installModule($zip_source)
    {
        define("INSTALLER", 1);
        $zip = zip_open($zip_source);
        if( !$zip ) 
        {
            return array( false, "Cannot open zip file" );
        }

        $info_file_found = false;
        while( $zip_entry = zip_read($zip) ) 
        {
            if( zip_entry_name($zip_entry) == "info.xml" ) 
            {
                $info_file_found = true;
                $contents = zip_entry_read($zip_entry);
                $module_name = Main::getelement($contents, "info:name");
                $script_version = Main::getelement($contents, "info:version");
                $key = Main::getelement($contents, "info:key");
                $zip_destination = ROOT . DS . "modules" . DS . "mod_" . strtolower($module_name);
                if( is_dir($zip_destination) && self::moduleinstalled($module_name) ) 
                {
                }

                $extracted = self::extractzip($zip_source, $zip_destination);
                $tables = $zip_destination . DS . "install" . DS . "tables.sql.php";
                $data = $zip_destination . DS . "install" . DS . "data.sql.php";
                ob_start();
                if( !include_once(ROOT . DS . "includes" . DS . "system.inc.php") ) 
                {
                    return array( false, "There was a problem installing module" );
                }

                if( is_file($tables) ) 
                {
                    include_once($tables);
                }

                if( is_file($data) ) 
                {
                    include_once($data);
                }

                self::installhooks($module_name);
                $message = ob_get_contents();
                ob_end_clean();
                break;
            }

        }
        zip_entry_close($zip_entry);
        if( !$info_file_found ) 
        {
            return array( false, "Info file not found" );
        }

        return array( true, $message );
    }

    public function isModule($module)
    {
        global $db;
        $row = $db->get_row("name", $module, "_core_modules");
        if( $row == NULL ) 
        {
            return false;
        }

        return true;
    }

    public function moduleInstalled($module)
    {
        global $settings;
        global $db;
        $row = $db->get_row("name", $module, "_core_modules");
        if( $row == NULL || $row["installed"] != "Yes" ) 
        {
            return false;
        }

        return true;
    }

    public function installUpdate($update)
    {
        global $db;
        define("INSTALLER", 1);
        $directory = ROOT . DS . "updates" . DS . strtolower($update);
        if( !file_exists($directory . DS . "info.php") ) 
        {
            return array( false, "Update info not found" );
        }

        if( !include_once($directory . DS . "info.php") ) 
        {
            return array( false, "There was a problem installing update" );
        }

        $base_version = $this->appVersion();
        $update_version = $version;
        $dependent_version = $dependent;
        $install = $directory . DS . "install.php";
        if( $version == null || $dependent == null ) 
        {
            return array( false, "Invalid update" );
        }

        $bv = str_replace(".", "", $base_version);
        $bv = (int) $bv;
        $uv = str_replace(".", "", $update_version);
        $uv = (int) $uv;
        $dv = str_replace(".", "", $dependent_version);
        $dv = (int) $dv;
        if( $db->get_row("version", $update_version, "_core_updates") ) 
        {
            return array( false, "Update already installed" );
        }

        if( $bv == $uv ) 
        {
            return array( false, "You are already using this version" );
        }

        if( $uv < $bv ) 
        {
            return array( false, "Invalid update version" );
        }

        $sql = "SELECT * FROM " . PREFIX . "_core_updates ";
        $sql .= "WHERE `version` = '" . $db->sql_quote($dependent_version) . "' ";
        $sql .= "AND installed = 'Yes' ";
        $result = $db->query_db($sql, $print = DEBUG);
        if( $db->num_rows($result) == 0 && $bv != $dv ) 
        {
            return array( false, "You must install update for v" . $dependent_version . " before proceeding" );
        }

        ob_start();
        if( !include_once(ROOT . DS . "includes" . DS . "system.inc.php") ) 
        {
            return array( false, "There was a problem installing update" );
        }

        if( is_file($install) ) 
        {
            include_once($install);
        }

        $message = ob_get_contents();
        ob_end_clean();
        if( mysql_error() ) 
        {
            return array( false, mysql_error() );
        }

        return array( true, "Update Installed Sucessfully" );
    }

    public function installHooks($mod)
    {
        global $db;
        $sql = "SELECT * FROM " . PREFIX . "_core_hooks ";
        $sql .= "WHERE `mod` ='" . $db->sql_quote($mod) . "' ";
        $sql .= "AND status != 'Installed' ";
        $result = $db->query_db($sql, $print = DEBUG);
        if( 0 < $db->num_rows($result) ) 
        {
            while( $row = $db->fetch_db_array($result) ) 
            {
                if( !self::moduleinstalled($row["mod"]) || !self::moduleinstalled($row["hook"]) ) 
                {
                    continue;
                }

                self::systemexec($row["install"], get_defined_vars());
                $sql = "UPDATE " . PREFIX . "_core_hooks ";
                $sql .= "SET status = 'Installed' ";
                $sql .= "WHERE id = '" . $row["id"] . "' ";
                $db->query_db($sql, $print = DEBUG);
            }
        }

        $sql = "SELECT * FROM " . PREFIX . "_core_hooks ";
        $sql .= "WHERE hook ='" . $db->sql_quote($mod) . "' ";
        $sql .= "AND status != 'Installed' ";
        $result = $db->query_db($sql, $print = DEBUG);
        if( 0 < $db->num_rows($result) ) 
        {
            while( $row = $db->fetch_db_array($result) ) 
            {
                if( !self::moduleinstalled($row["mod"]) || !self::moduleinstalled($row["hook"]) ) 
                {
                    continue;
                }

                self::systemexec($row["install"], get_defined_vars());
                $sql = "UPDATE " . PREFIX . "_core_hooks ";
                $sql .= "SET status = 'Installed' ";
                $sql .= "WHERE id = '" . $row["id"] . "' ";
                $db->query_db($sql, $print = DEBUG);
            }
        }

    }

    public function UnInstallHooks($mod)
    {
        global $db;
        $sql = "SELECT * FROM " . PREFIX . "_core_hooks ";
        $sql .= "WHERE `mod` ='" . $db->sql_quote($mod) . "' ";
        $sql .= "AND status != 'Uninstalled' ";
        $result = $db->query_db($sql, $print = DEBUG);
        if( 0 < $db->num_rows($result) ) 
        {
            while( $row = $db->fetch_db_array($result) ) 
            {
                if( !self::moduleinstalled($row["mod"]) || !self::moduleinstalled($row["hook"]) ) 
                {
                    continue;
                }

                self::systemexec($row["uninstall"], get_defined_vars());
                $sql = "UPDATE " . PREFIX . "_core_hooks ";
                $sql .= "SET status = 'Uninstalled' ";
                $sql .= "WHERE id = '" . $row["id"] . "' ";
                $db->query_db($sql, $print = DEBUG);
            }
        }

        $sql = "SELECT * FROM " . PREFIX . "_core_hooks ";
        $sql .= "WHERE hook ='" . $db->sql_quote($mod) . "' ";
        $sql .= "AND status != 'Uinstalled' ";
        $result = $db->query_db($sql, $print = DEBUG);
        if( 0 < $db->num_rows($result) ) 
        {
            while( $row = $db->fetch_db_array($result) ) 
            {
                if( !self::moduleinstalled($row["mod"]) || !self::moduleinstalled($row["hook"]) ) 
                {
                    continue;
                }

                self::systemexec($row["uninstall"], get_defined_vars());
                $sql = "UPDATE " . PREFIX . "_core_hooks ";
                $sql .= "SET status = 'Uninstalled' ";
                $sql .= "WHERE id = '" . $row["id"] . "' ";
                $db->query_db($sql, $print = DEBUG);
            }
        }

    }

    public function systemExec($file, $varsC, $return_content = false)
    {
        $SYSEXE = true;
        foreach( $varsC as $varC => $contentC ) 
        {
            ${$varC} = $contentC;
        }
        unset($output);
        ob_start();
        if( !include_once(ROOT . DS . "includes" . DS . "system.inc.php") ) 
        {
            return false;
        }

        if( !include_once(ROOT . DS . $file) ) 
        {
            return false;
        }

        if( $return_content ) 
        {
            $output = ob_get_contents();
        }

        ob_end_clean();
        return (!empty($output) ? $output : true);
    }

    public function __autoload($class_name)
    {
        include($class_name . ".php");
    }

    public function handleEvent($event, $vars = array(  ))
    {
        global $db;
        $sql = "SELECT * FROM " . PREFIX . "_core_events ";
        $sql .= "WHERE event ='" . $db->sql_quote($event) . "' ";
        $result = $db->query_db($sql, $print = DEBUG);
        if( 0 < $db->num_rows($result) ) 
        {
            while( $row = $db->fetch_db_array($result) ) 
            {
                self::systemexec($row["task"], $vars);
            }
        }

    }

    public function send_email($to, $from, $subject, $message, $host, $type)
    {
        $mail = $this->importPlugin("phpmailer", array( "PHPMailerAutoload.php" ));
        $mail = new PHPMailer();
        $mail->setFrom($from[0], $from[1]);
        $mail->addReplyTo($from[0], $from[1]);
        $mail->addAddress($to[0], $to[1]);
        $mail->Subject = $subject;
        $mail->Body = $message;
        $mail->IsHTML($type);
        if( !$mail->send() ) 
        {
            return false;
        }

        return true;
    }

    public function email($userName, $emailName, $dataArray = array(  ))
    {
        global $db;
        global $settings;
        global $system;
        $table = "_core_email_templates";
        $key_f = "name";
        $key_id = $emailName;
        $template = $db->get_row($key_f, $key_id, $table);
        if( $userName == null ) 
        {
            return false;
        }

        if( $template["send"] != "Yes" ) 
        {
            return false;
        }

        $userinfo = $this->importClass("account")->getUserData($userName);
        $user_tags = array( "{websitename}" => $settings["system"]["websitename"], "{websiteurl}" => $settings["system"]["websiteurl"], "{signature}" => $settings["system"]["adminsignature"], "{firstname}" => $userinfo["firstname"], "{lastname}" => $userinfo["lastname"], "{username}" => $userinfo["username"], "{password}" => "**hidden**", "{email}" => $userinfo["email"] );
        $dataArray = array_merge($user_tags, $dataArray);
        $mail_subject = $this->replaceTags($dataArray, $template["subject"]);
        $mail_subject = $this->replaceTags($dataArray, $mail_subject);
        $mail_body = $this->replaceTags($dataArray, $template["body"]);
        $mail_body = $this->replaceTags($dataArray, $mail_body);
        $to = array( $userinfo["email"], $userinfo["username"] );
        $from = array( $settings["system"]["noreply_email"], $settings["system"]["websitename"] );
        if( DEMO == 0 ) 
        {
            self::send_email($to, $from, $mail_subject, $mail_body, $settings["system"]["websiteurl"], true);
        }

        if( DEBUG ) 
        {
            $system->debuginfo[] = $mail_subject . "\n\n" . $mail_body;
        }

        return true;
    }

    public function showCaptcha()
    {
        global $settings;
        $path = SITEURL . FS . $settings["system"]["plugins_directory"] . FS . "cool-php-captcha" . FS . "captcha.php";
        return $path;
    }

    public function validateCaptcha($captcha)
    {
        global $settings;
        if( empty($_SESSION["captcha"]) || trim(strtolower($captcha)) != $_SESSION["captcha"] ) 
        {
            return false;
        }

        return true;
    }

    public function importPlugin($name, $file_path = null)
    {
        global $settings;
        if( $file_path != null ) 
        {
            foreach( $file_path as $path ) 
            {
                $goto .= $path . DS;
            }
            $goto = rtrim($goto, DS);
        }
        else
        {
            $goto = "index.php";
        }

        if( !include_once(ROOT . DS . $settings["system"]["plugins_directory"] . DS . $name . DS . $goto) ) 
        {
            return false;
        }

        return get_defined_vars();
    }

    public function importSystemClass($file_path = null)
    {
        global $settings;
        if( is_array($file_path) ) 
        {
            foreach( $file_path as $path ) 
            {
                $goto .= $path . DS;
            }
            $goto = rtrim($goto, DS);
        }
        else
        {
            $goto = $file_path;
        }

        if( !include(ROOT . DS . "includes" . DS . "classes" . DS . $goto) ) 
        {
            return false;
        }

        return get_defined_vars();
    }

    public function debug()
    {
        global $db;
        echo "<b>SQL QUERIES:</b>";
        if( count($db->sqlout) == 0 ) 
        {
            echo "None";
        }

        echo "<br>";
        foreach( $db->sqlout as $id => $info ) 
        {
            echo $id . ":" . $info . "<br>";
        }
        echo "<b>SQL ERRORS:</b>";
        if( count($db->sqlerror) == 0 ) 
        {
            echo "None";
        }

        echo "<br>";
        foreach( $db->sqlerror as $id => $info ) 
        {
            echo $id . ":" . $info . "<br>";
        }
        echo "<b>DEBUG INFO:</b>";
        if( count($this->debuginfo) == 0 ) 
        {
            echo "None";
        }

        echo "<br>";
        foreach( $this->debuginfo as $id => $info ) 
        {
            echo $id . ":" . $info . "<br>";
        }
        echo "<b>LOAD TIME:</b>";
        echo "This page was created in " . self::executiontime() . " seconds<br>";
        echo "<b>MEMORY USAGE:</b><br>";
        echo "Start Memory: " . $this->execStartMemory . "<br> ";
        echo "End Memory: " . memory_get_usage() . "<br> ";
        echo "Peak Memory: " . memory_get_peak_usage() . "<br> ";
    }

    public function executionTime()
    {
        $mtime = microtime();
        $mtime = explode(" ", $mtime);
        $mtime = $mtime[1] + $mtime[0];
        $endtime = $mtime;
        $totaltime = $endtime - $this->execStartTime;
        return $totaltime;
    }

    public function cronjobs()
    {
        global $db;
        $sql = "SELECT * FROM " . PREFIX . "_core_cronjobs";
        $res = $db->query_db($sql, $print = DEBUG);
        if( 0 < $db->num_rows($res) ) 
        {
            while( $row = $db->fetch_db_array($res) ) 
            {
                if( $row["interval"] <= time() - $row["last_cronjob"] ) 
                {
                    if( $this->isModule($row["mod"]) ) 
                    {
                        $mclass = $this->importClass($row["mod"]);
                    }

                    if( is_object($mclass) && method_exists($mclass, "cronjob") ) 
                    {
                        $mclass->cronjob();
                        unset($mclass);
                    }

                    $sql = "UPDATE " . PREFIX . "_core_cronjobs SET last_cronjob  = '" . time() . "' WHERE id = '" . $db->sql_quote($row["id"]) . "' LIMIT 1";
                    $db->query_db($sql, $print = DEBUG);
                }

            }
        }

    }

    public function logError($error)
    {
    }

    public function getLicenseDetails($key)
    {
        include_once("keys.php");
        $a = new App();
        $url = "http://www.2gosoft.com/client/index.php/api/guest/servicelicense/check";
        $params = array();
        $params["license"] = $key;
        $params["host"] = (substr($_SERVER["HTTP_HOST"], 0, 4) == "www." ? substr($_SERVER["HTTP_HOST"], 4) : $_SERVER["HTTP_HOST"]);
        $params["path"] = dirname(__FILE__);
        $params["key"] = $a->getKey("43097432");
        $params["name"] = $a->getName();
        $params["version"] = $a->getVersion();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        $result = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if( $code != 200 ) 
        {
            error_log("CURLINFO_HTTP_CODE: " . $code);
        }

        return json_decode($result, true);
    }

    public function isCron()
    {
        if( php_sapi_name() == "cli" ) 
        {
            return true;
        }

        if( !isset($_SERVER["HTTP_HOST"]) ) 
        {
            return true;
        }

        return false;
    }

    public function validateRemoteLicense($key)
    {
        $result = self::getlicensedetails($key);
        if( $result["error"] ) 
        {
            return true;
        }

        return true;
    }

    public function validateBaseLicense($key)
    {
        if( self::iscron() ) 
        {
            return true;
        }

        include_once("keys.php");
        $a = new App();
        $password = $a->getKey("43097432");
        $host = $_SERVER["HTTP_HOST"];
        $host = (substr($host, 0, 4) == "www." ? substr($host, 4) : $host);
        $validKey = md5($password) . ":" . $host;
        $validKey = md5($validKey);
        if( $validKey == $key ) 
        {
            return true;
        }

        return true;
    }

    public function appName()
    {
        include_once("keys.php");
        $a = new App();
        return $a->getName();
    }

    public function appVersion()
    {
        include_once("keys.php");
        $a = new App();
        return $a->getVersion();
    }

    public function logMessage($type, $message)
    {
        if( is_array($message) ) 
        {
            foreach( $message as $n_message ) 
            {
                $_SESSION["system"]["messages"][$type][] = $n_message;
            }
        }
        else
        {
            $_SESSION["system"]["messages"][$type][] = $message;
        }

    }

    public function displayMessage($type)
    {
        $string = "";
        foreach( $_SESSION["system"]["messages"][$type] as $message ) 
        {
            $string .= $message . "<br>";
        }
        unset($_SESSION["system"]["messages"][$type]);
        return $string;
    }

}
