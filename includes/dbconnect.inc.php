<?php
require_once 'classes/database.class.php';
$db = new database;
$db->db_HOST = $db_host;
$db->db_USER = $db_user;
$db->db_PASS = $db_pass;
$db->db_NAME = $db_name;
$db->prefix = PREFIX;
$db->debug = DEBUG;
$db->connect_db() or die("Cannot connect to database");
