<?php

$settings = array(

'system' => array(

		  
		   'default_module'=>'content',
           //system directory paths
           'plugins_directory' => 'plugins',
           'templates_directory' => 'templates',
           'admin_directory'=>'advshare',
           'rows_per_page'=>50 ,
		   //select options
		   'bool_no_yes'=> array('No','Yes'),
		   'bool_false_true'=> array('False','True'),
		   'days_options'=> array('Sun'=>'Sun','Mon'=>'Mon','Tue'=>'Tue','Wed'=>'Wed','Thu'=>'Thu','Fri'=>'Fri','Sat'=>'Sat'),
		   //system api
		   'api_enabled'=>false,
		   'api_key'=>'',


),



);

 $sql  = "SELECT * FROM ".PREFIX."_core_settings ";
 $sql .= "ORDER BY `mod` ASC";
 $result = $db->query_db($sql,$print = DEBUG);
 if ($db->num_rows($result) > 0) while($row = $db->fetch_db_array($result))$settings[$row['mod']][$row['name']] = $row['value'];
 $settings['system']['themes'] = scanDirectory(ROOT.DS.$settings['system']['templates_directory']);
 
 function scanDirectory($path)
 {
       $temp = array();
	   $handle = (opendir($path));
	   if(!handle) return $temp;
       while($h = readdir($handle)) 
	   if(is_dir($path.DS.$h) && $h != "." && $h != "..") $temp[$h] = $h;	
	   return $temp;
 }
 
 ?>
