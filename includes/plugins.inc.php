<?php

$plugins = array(

    'totalvirus' => array(
	    'active'=>false,
        'apikey' => '',       
    ),
	
	'facebook' => array(
	
	 //facebook connect integration
	     'active'=>false,
		 'facebook_connect' => false,
         'facebook_appID' => '',	
		 'facebook_appSecret' =>'',	 
	),
	
	'phplist' => array(
	  //phplist plugin
	     'active'=>false,
		 'list_id'=>'',
		 'directory'=>'',
		 'admin_directory'=>'',

     ),
	 
	 'mailchimp' => array(
	  //mailchimp plugin
		 'active'=>false,
		 'apikey'=>'',
		 'list_id'=>'',

     ),



);